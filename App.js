import 'react-native-gesture-handler';
import React, {useEffect, useCallback, useState} from 'react';
import SplashScreen from 'react-native-splash-screen';
import {StatusBar, Platform, Alert, DeviceInfo} from 'react-native';
import JailMonkey from 'jail-monkey';
import NetInfo from '@react-native-community/netinfo';
import {EventRegister} from 'react-native-event-listeners';
import { Text, TextInput } from 'react-native';

import {Provider} from 'react-redux';
//import ReduxThunk from 'redux-thunk';
import configureStore from './src/store/Store.js';

import * as DeviceAction from './src/store/actions/deviceAction';
import * as badgeCountAction from './src/store/actions/badgeCountAction';

import {PersistGate} from 'redux-persist/integration/react';

import AuthNavigator from './src/navigation/AuthNavigator';

import DeviceConfigScreen from './src/screens/DeviceConfigScreen';

import colors from './src/config/colors';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-async-storage/async-storage';
import appConstant from './src/utility/appConstant.js';
import apiUtility from './src/api/apiUtility.js';
import AppUpdateConfigScreen from './src/screens/AppUpdateConfigScreen .js';

import {
  GoogleAnalyticsSettings,
  GoogleAnalyticsTracker,
} from 'react-native-google-analytics-bridge';

import NOTIFICATION_API from './src/api/apiNotifications';

const {store, persistor} = configureStore();
// export const tracker =
// new GoogleAnalyticsTracker('UA-171278332-1',{CD_A: 1, CD_B: 2});
console.log("Start app...")
const App = () => {
  const [enableForceUpdate, setEnableForceUpdate] = useState(false);
  
  useEffect(() => {
    deviceHandler();
    checkPermission();
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false; 
    TextInput.defaultProps = TextInput.defaultProps || {};
    TextInput.defaultProps.allowFontScaling = false;
    
    messaging().onMessage(async (remoteMessage) => {
      console.log('A new FCM message arrived!', JSON.stringify(remoteMessage));

      getUserBadgeCount();
    });

    messaging().setBackgroundMessageHandler(async (remoteMessage) => {
      console.log('Message handled in the background!', remoteMessage);

      openPushNotification(remoteMessage);
    });
  }, [deviceHandler]);

  const openPushNotification = (notification) => {
    // New Notifications
    /* 1 || 2 || 3 || 5 || 6 || 8 || 10 || 11 */

    let userId = '';
    let isCustomer = '';
    if (
      store.getState() &&
      store.getState().loginInfo &&
      store.getState().loginInfo.loginResponse &&
      store.getState().loginInfo.loginResponse.data
    ) {
      if (store.getState().loginInfo.loginResponse.data.userid) {
        userId = store.getState().loginInfo.loginResponse.data.userid;
      } else {
        userId = '';
      }

      if (store.getState().loginInfo.loginResponse.data.is_customer) {
        isCustomer = store.getState().loginInfo.loginResponse.data.is_customer;
      } else {
        isCustomer = '';
      }
    } else {
      userId = '';
      isCustomer = '';
    }
    // console.log('Notification:: userId: ', userId);
    // console.log('Notification:: isCustomer: ', isCustomer);

    // console.log('Notification:: openPushNotification: ', notification);

    try {
      if (
        userId &&
        notification &&
        notification.data &&
        notification.data.notification_id
      ) {
        readNotification(userId, notification.data.notification_id);

        if (isCustomer && isCustomer == '1') {
          setTimeout(() => {
            // console.log(
            //   'Notification:: openPushNotification:: POST:: ',
            //   notification,
            // );
            EventRegister.emit(
              'notificationRedirectionEventPost',
              notification.data,
            );
          }, 1200);
        } else {
          setTimeout(() => {
            // console.log(
            //   'Notification:: openPushNotification:: PRE:: ',
            //   notification,
            // );
            EventRegister.emit(
              'notificationRedirectionEventPre',
              notification.data,
            );
          }, 1200);
        }
      }
    } catch (error) {
      console.log('Push Notification Redirection Error:: ', error);
    }
  };

  const readNotification = (userId, notificationId) => {
    const param = {
      user_id: userId,
      notification_id: notificationId,
    };
    NOTIFICATION_API.readNotification(param).then((data) => {
      if (data && data.data && data.data.status && data.data.status == '200') {
        changeNotificationCount(userId);
      }
    });
  };

  const changeNotificationCount = (userId) => {
    NOTIFICATION_API.notificationCount({user_id: userId}).then((data) => {
      if (data && data.data && data.data.status && data.data.status == '200') {
        getUserBadgeCount();
      }
    });
  };

  useEffect(() => {
    const eventListener = EventRegister.addEventListener(
      'wishlistUpdateEvent',
      (data) => {
        getUserBadgeCount();
      },
    );

    return () => {
      EventRegister.removeEventListener(eventListener);
    };
  }, []);

  const getUserBadgeCount = async () => {
    let userId = '';
    if (
      store.getState() &&
      store.getState().loginInfo &&
      store.getState().loginInfo.loginResponse &&
      store.getState().loginInfo.loginResponse.data &&
      store.getState().loginInfo.loginResponse.data.userid
    ) {
      userId = store.getState().loginInfo.loginResponse.data.userid;
    } else {
      userId = '';
    }

    try {
      if (userId) {
        const requestData = {
          user_id: userId,
        };

        const userBadgeCount = await apiUtility.apiUserBadgeCount(requestData);

        const response = userBadgeCount.data;

        let notificationCountData = '';
        let wishlistCountData = '';
        let lastAccessedData = '';

        if (response && response.status == '200' && response.data) {
          if (response.data.unread_notifications_count) {
            notificationCountData =
              Number(response.data.unread_notifications_count) > 99
                ? '99+'
                : response.data.unread_notifications_count;
          }

          if (response.data.wishlist_count) {
            wishlistCountData =
              Number(response.data.wishlist_count) > 99
                ? '99+'
                : response.data.wishlist_count;
          }

          if (response.data.last_accessed) {
            lastAccessedData = response.data.last_accessed
              ? appConstant.formatDate(response.data.last_accessed)
              : '';
          }

          const total =
            Number(wishlistCountData ? wishlistCountData : '0') +
            Number(notificationCountData ? notificationCountData : '0');

          store.dispatch(
            badgeCountAction.setBadgeData({
              lastAccessed: lastAccessedData,
              wishlistCount: wishlistCountData,
              notificationCount: notificationCountData,
              totalCount: total > 0 ? '' + total : '',
            }),
          );
        }
      }
    } catch (error) {
      console.log('App Version Checking Error: ', error);
    }
  };

  const deviceHandler = useCallback(async () => {
    try {
      store.dispatch(DeviceAction.getUniqueId());
    } catch (error) {
      console.log('Error Device Info Fetch: ', error);
    }
  }, [store.dispatch]);

  const isNetworkAvailable = async () => {
    try {
      const response = await NetInfo.fetch();
      // return response.isConnected && response.isInternetReachable;
      return response.isConnected;
    } catch (error) {
      console.log('Internet Connection Checking Error: ', error);
      return false;
    }
  };

  if (JailMonkey.isJailBroken()) {
    return <DeviceConfigScreen deviceConfig="JailBroken" />;
  }

  if (!isNetworkAvailable) {
    return <DeviceConfigScreen deviceConfig="internet" />;
  }

  const checkPermission = async () => {
    const enabled = await messaging().hasPermission();
    if (enabled) {
      getToken();
    } else {
      requestPermission();
    }
  };

  const getToken = async () => {
    try {
      if (!messaging().isDeviceRegisteredForRemoteMessages) {
        await messaging().registerDeviceForRemoteMessages();
      }
      let fcmToken = await messaging().getToken();
      if (fcmToken) {
        console.log(fcmToken);
        await AsyncStorage.setItem('fcmToken', fcmToken);
        checkAppVersion();
      }
    } catch (error) {
      console.log(error);
    }
  };

  const requestPermission = async () => {
    try {
      await messaging().requestPermission();
      getToken();
    } catch (error) {
      console.log(error);
    }
  };

  const checkAppVersion = async () => {
    try {
      let userId = '';
      if (
        store.getState() &&
        store.getState().loginInfo &&
        store.getState().loginInfo.loginResponse &&
        store.getState().loginInfo.loginResponse.data &&
        store.getState().loginInfo.loginResponse.data.userid
      ) {
        userId = store.getState().loginInfo.loginResponse.data.userid;
      } else {
        userId = '';
      }

      let deviceId = '';
      try {
        deviceId = await DeviceInfo.syncUniqueId();
      } catch (errorDeviceId) {
        deviceId = '';
      }

      const requestData = {
        app_version: appConstant.buildInstance.appVersion,
        build_type:
          appConstant.buildInstance.buildType != ''
            ? appConstant.buildInstance.buildType
            : 'STORE',
        device_id: deviceId ? deviceId : '',
        device_type:
          Platform.OS === 'android' ? 'Android' : Platform.OS.toUpperCase(),
        user_id: userId,
      };

      const checkAppVersionResult = await apiUtility.apiVersionCheck(
        requestData,
      );

      const response = checkAppVersionResult.data;

      SplashScreen.hide();

      if (
        response &&
        response.status == '200' &&
        response.data &&
        response.data.force_update &&
        response.data.force_update == '1'
      ) {
        console.log(
          'App Version Checking:: force update:: ',
          response.data.force_update,
        );
        setEnableForceUpdate(true);
      } else {
        setEnableForceUpdate(false);
      }
    } catch (error) {
      console.log('App Version Checking Error: ', error);
      SplashScreen.hide();
      setEnableForceUpdate(false);
    }
    getUserBadgeCount();
  };

  return (
    <PersistGate loading={null} persistor={persistor}>
      {enableForceUpdate ? <AppUpdateConfigScreen /> : <AuthNavigator />}
    </PersistGate>
  );
};

const AppWrapper = () => {
  return (
    <Provider store={store}>
      <App />
      <StatusBar
        backgroundColor={colors.jaguar}
        barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'}
      />
    </Provider>
  );
};

export default AppWrapper;
