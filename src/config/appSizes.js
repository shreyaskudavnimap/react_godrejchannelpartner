export const fontSizes = {
  smallFontSize: 12, 
    mediumFontSize: 13,
    normalFontSize: 14,
    largeFontSize: 16,
    largeBold: 18,
    largeBoldx: 19,
    xlargeFontSize: 20,
    xxlargeFontSize: 22,
    xxxlargeFontSize: 25
};





