export default {
    RobotoBold: "Roboto-Bold",
    RobotoLight: "Roboto-Light",
    RobotoMedium: "Roboto-Medium",
    RobotoRegular: "Roboto-Regular",
    SourceSansProBold: 'SourceSansPro-Bold',
    SourceSansProLight: 'SourceSansPro-Light',
    SourceSansProRegular: "SourceSansPro-Regular",
    SourceSansProSemiBold: "SourceSansPro-SemiBold",
    smallFontSize: 12, 
    mediumFontSize: 13,
    normalFontSize: 14,
    largeFontSize: 16,
    largeBold: 18,
    largeBoldx: 19,
    xlargeFontSize: 20,
    xxlargeFontSize: 22,
    xxxlargeFontSize: 25
  };