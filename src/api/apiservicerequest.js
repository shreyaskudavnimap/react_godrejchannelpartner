import apiClient from './client';

const CREATE_SERVICE_REQUEST='service_request/save.json';
const SERVICE_REQUEST_CLOSED='service_request.json'
const SERVICE_REQUEST_OPEN='service_request.json'
const FLAT_CODE='service_request/get_flat_code.json'

const api_CREATE_SERVICE_REQUEST = (requestData) => {
    return apiClient.post(CREATE_SERVICE_REQUEST, requestData);
};

const api_SERVICE_REQUEST_CLOSED = (requestData) => {
    return apiClient.post(SERVICE_REQUEST_CLOSED, requestData);
};

const api_SERVICE_REQUEST_OPEN = (requestData) => {
    return apiClient.post(SERVICE_REQUEST_OPEN, requestData);
};
const api_FLAT_CODE = (requestData) => {
    return apiClient.post(FLAT_CODE, requestData);
};

export default {
    api_CREATE_SERVICE_REQUEST,
    api_SERVICE_REQUEST_CLOSED,
    api_SERVICE_REQUEST_OPEN,
    api_FLAT_CODE
};