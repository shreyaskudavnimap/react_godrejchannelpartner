import apiClient from './client';
const epGetFaq = 'faq.json'
/**
 * 
 * @param {object} param 
 */
const apiGetFaq = (param) => {
  return apiClient.post(epGetFaq,param);
};

export default {
  apiGetFaq
};