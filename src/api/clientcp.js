import {create} from 'apisauce';
import base64 from 'react-native-base64'
import appConstant from '../utility/appConstant';

const apiUrl = "https://cp.godrejproperties.com//webapi/"
// export const cpClient = create({
//   baseURL: appConstant.buildInstance.baseUrl,
// });

const postRequest = (apiName, requestData, requestType) => {
  if(requestType == "FormData")
  {
    let options = {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST'
    };
    options.body = new FormData();
    options.body.append("Login", base64.encode(JSON.stringify(requestData)));
    return fetch(apiUrl + apiName, options)
    .then((response) => response.json())
    .then((responseJson) => {
      console.log("postResquest", responseJson.data)
        return responseJson.data;
    });
  }
};

const getRequestProfile = (url) => {
  return fetch(url, {
    method: 'GET'
  })
  .then((response) => response.json())
  .then((responseJson) => {
      return responseJson.data[0];
  });
}

const getRequestData = (url) => {
  return fetch(url, {
    method: 'GET'
  })
  .then((response) => response.json())
  .then((responseJson) => {
      return responseJson.data;
  });
}

const getRequest = (url) => {
  return fetch(url, {
    method: 'GET'
  })
  .then((response) => response.json())
  .then((responseJson) => {
      return responseJson;
  });
}

export default {
  postRequest,
  getRequestData,
  getRequestProfile,
  getRequest
};
