import apiClient from './client';

const epUpdateProjectInfo = 'update_history.json'
const epProjectDetailsInfo = 'get_project_details_info.json';
const epProjectDetailsGallery = 'project_galleries_rest';
const epProjectFeaturedMenuInfo = 'get_project_featuredmenu.json';
const epProjectGalleryViw = 'get_project_gallery_bytype.json';

const epProjectCMSData = 'get_property_cms.json';

const epPropertyLocationConnectivity = 'get_property_location_connectivity.json';
const epPropertyNearbyPoints = 'get_property_nearby_points.json';
const epNearbyPointsDetails = 'get_nearby_points_details.json';

const apiUpdateProjectInfo = (requestData) => {
  return apiClient.post(epUpdateProjectInfo, requestData);
};

const apiProjectDetailsInfo = (requestData) => {
  return apiClient.post(epProjectDetailsInfo, requestData);
};

const apiProjectDetailsGallery = (projectId) => {
  return apiClient.get(`${epProjectDetailsGallery}/${projectId}`);
};

const apiProjectFeaturedMenuInfo = (requestData) => {
  return apiClient.post(epProjectFeaturedMenuInfo, requestData);
};

const apiProjectCMS = (requestData) => {
  return apiClient.post(epProjectCMSData, requestData);
};

const apiPropertyLocationConnectivity = (requestData) => {
  return apiClient.post(epPropertyLocationConnectivity, requestData);
};

const apiPropertyNearbyPoints = (requestData) => {
  return apiClient.post(epPropertyNearbyPoints, requestData);
};

const apiNearbyPointsDetails = (requestData) => {
  return apiClient.post(epNearbyPointsDetails, requestData);
};

const apiProjectGalleryView = (requestData) => {
  console.log(requestData)
  return apiClient.post(epProjectGalleryViw, requestData);
};

export default {
  apiUpdateProjectInfo,
  apiProjectDetailsInfo,
  apiProjectDetailsGallery,
  apiProjectFeaturedMenuInfo,
  apiProjectCMS,
  apiPropertyLocationConnectivity,
  apiPropertyNearbyPoints,
  apiNearbyPointsDetails,
  apiProjectGalleryView
};
