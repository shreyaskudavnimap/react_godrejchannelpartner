import apiClient from './client';

const epBookingPropertyInfo = 'get_eoi_booking_property_info.json';
const epSourceControl = 'get_source_control_info.json';
const epEoiFoyrFilterInfo = 'get_eoi_foyr_filter_info.json';
const epEoiStockStatus = 'get_eoi_stock_status.json';
const epBookingAddData = 'res_eoi_booking_chk_add.json';
const epCheckBookingStatus = 'get_booking_confirmation_status.json';
const epEoiBookingConfirmationData = 'eoi_booking_confirmation_data.json';

const apiBookingPropertyInfo = (requestData) => {
  return apiClient.post(epBookingPropertyInfo, requestData);
};

const apiSourceControl = (requestData) => {
  return apiClient.post(epSourceControl, requestData);
};

const apiEoiFoyrFilterInfo = (requestData) => {
  return apiClient.post(epEoiFoyrFilterInfo, requestData);
};

const apiBookingAddData = (requestData) => {
  return apiClient.post(epBookingAddData, requestData);
};

const apiEoiStockStatus = (requestData) => {
  return apiClient.post(epEoiStockStatus, requestData);
};

const apiCheckBookingStatus = (requestData) => {
  return apiClient.post(epCheckBookingStatus, requestData);
};

const apiEoiBookingConfirmationData = (requestData) => {
  return apiClient.post(epEoiBookingConfirmationData, requestData);
};

const apiEoiBookingTerms = (projectId) => {
  return apiClient.get(`term_of_use_eoi_payment/${projectId}`);
};

export default {
  apiBookingPropertyInfo,
  apiSourceControl,
  apiEoiFoyrFilterInfo,
  apiEoiStockStatus,
  apiBookingAddData,
  apiCheckBookingStatus,
  apiEoiBookingConfirmationData,
  apiEoiBookingTerms,
};
