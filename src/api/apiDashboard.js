import apiClient from './client';

const epRMInfo = 'get_rm_info.json';
const epAccountSummary = 'account_summary_api.json';
const epSingleAccountSummary = 'single_account_summary_api.json';
const propertyGallery = 'project_galleries_rest';
const epConstructionDateRange = 'getDateRange.json';
const epConstructionStatus = 'construction_status_images.json';
const epPostloginTerms = 'term_of_use_login';

const apiGetRMInfo = (requestData) => {
  return apiClient.post(epRMInfo, requestData);
};

const apiAccountSummary = (requestData) => {
  return apiClient.post(epAccountSummary, requestData);
};

const apiSingleAccountSummary = (requestData) => {
  return apiClient.post(epSingleAccountSummary, requestData);
};

const getPropertyGallery = (propertyID) => {
  return apiClient.get(propertyGallery + '/' + propertyID);
};

/**
 * @param {object} requestData
 * @description get constructor date range
 */
const getConstructionDateRange = (requestData)=>{
  return apiClient.post(epConstructionDateRange, requestData);
}
/**
 * 
 * @param {object} requestData 
 * @description get construction status
 */
const getConstructionStatus = (requestData)=>{
  return apiClient.post(epConstructionStatus, requestData);
}

const getPostLoginTerms = () => {
  return apiClient.get(epPostloginTerms);
};

export default {
  apiGetRMInfo,
  apiAccountSummary,
  apiSingleAccountSummary,
  getPropertyGallery,
  getConstructionDateRange,
  getConstructionStatus,
  getPostLoginTerms
};
