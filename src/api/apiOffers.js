import apiClient from './client';

const epOffers = 'get_offers.json';

const apiOffers = (requestData) => {
  return apiClient.post(epOffers, requestData);
};

export default {
  apiOffers,
};
