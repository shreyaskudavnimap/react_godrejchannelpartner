import apiClient from './client';

const RM_INFO='get_rm_info.json'

const api_RM_INFO = (requestData) => {
    return apiClient.post(RM_INFO, requestData);
};

export default {
    api_RM_INFO,
  
};