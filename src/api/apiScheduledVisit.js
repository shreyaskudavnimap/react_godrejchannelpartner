import apiClient from './client';

const getScheduledVisit = 'get_schedule_visit.json';
const deleteScheduleVisit = 'delete_schedule_visit.json';
const editScheduleVisit = 'edit_schedule_visit.json'
const getScheduleVisitDetails = 'schedule_visit_form_data.json'
const getRMVisitDates = 'get_schedule_dates.json'
const getAvailableSlots = 'avaliable_slots.json'
const getVisitTypes = 'schedule_taxonomy_list.json'
const saveScheduleVisit = 'schedule_visit_save_data.json'
const getTermData = 'term_and_condition_schedule_visit'
const updateScheduleVisit = 'edit_schedule_visit.json'

const apiGetScheduledVisitList = (requestData) => {
    return apiClient.post(getScheduledVisit, requestData)
}

const apiDeleteScheduleVisit = (requestData) => {
    return apiClient.post(deleteScheduleVisit, requestData)
}

const apiEditScheduleVisit = (requestData) => {
    return apiClient.post(editScheduleVisit, requestData)
}

const apiGetScheduleVisitDetails = (requestData) => {
    return apiClient.post(getScheduleVisitDetails, requestData)
}

const apiGetRMVistDates = (requestData) => {
    return apiClient.post(getRMVisitDates, requestData)
}

const apiGetAvailableSlots = (requestData) => {
    return apiClient.post(getAvailableSlots, requestData)
}

const apiGetVisitTypes = (requestData) => {
    return apiClient.post(getVisitTypes, requestData)
}

const apiSaveScheduleVisit = (requestData) => {
    return apiClient.post(saveScheduleVisit, requestData)
}

const apiUpdateScheduleVisit = (requestData) => {
    return apiClient.post(updateScheduleVisit, requestData)
}

const apiGetTermData = (purposeId, purposeAlias) => {
    const url = `${getTermData}/${purposeAlias}`
    return apiClient.get(url)
}

export default {
    apiGetScheduledVisitList,
    apiDeleteScheduleVisit,
    apiEditScheduleVisit,
    apiGetScheduleVisitDetails,
    apiGetRMVistDates,
    apiGetAvailableSlots,
    apiGetVisitTypes,
    apiSaveScheduleVisit,
    apiGetTermData,
    apiUpdateScheduleVisit
};
