import apiClient from './client';

const propertyFilterInfo = 'property_filter_info.json';
const lastSearchData = 'property_api_search.json';

const apiPropertyFilterInfo = (requestData) => {
  return apiClient.post(propertyFilterInfo, requestData);
};

const apiPropertyLastFilterInfo = (requestData) => {
  return apiClient.post(lastSearchData, requestData);
};

export default {
  apiPropertyFilterInfo,
  apiPropertyLastFilterInfo,
};
