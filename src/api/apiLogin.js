import apiClient from './client';
import cpClient from './clientcp';

const lookupUserByPan = 'fetch_mobile_by_pan.json';
const sentOtp = 'send_otp.json';
const loginUser = 'postsale_customer_login.json';
const loginUser2 = 'GetEmplLoginDetails.ashx';
const loginNewUser = 'presale_user_login.json';
const lookupByMobile = 'fetch_user_by_mobile.json';
const validateOtp = 'otp_verify.json';
const signUpUser = 'presale_user_register.json';
const forgetPwdByMobile = 'forgot_password_by_mobile.json';
const updatePassword = 'user_new_pass.json';
const activateUserSfid = 'postsale_activate_user.json';
const forgetPwdByPan = 'forgot_password_by_pan.json';
const checkDuplicateEmail = 'presale_check_email.json';
const getUserData = 'get_user_data_api.json';
const updateDeviceToken = 'update_device_token.json';
const updateProfileData = 'update_profile_data.json';
const updateUserpass = 'update_userpass_api.json';
const updateUserApi = 'update_user_api.json';
const verifyUserdataApi = 'verify_userdata_api.json';
const epPreLoginTerms = 'term_of_use_prelogin';
const epLogout = 'user_custom_logout.json';

const apiLandingLookupUserByPan = (requestData) => {
  return apiClient.post(lookupUserByPan, requestData);
};

const apiLoginUserByPanNPass = (requestData) => {
  return apiClient.post(loginUser, requestData);
};

const apiLandingSendOtp = (requestData) => {
  return apiClient.post(sentOtp, requestData);
};

const apiLoginUserByMobileNPass = (requestData) => {
  return apiClient.post(loginNewUser, requestData);
};

const apiForlookupByMobile = (requestData) => {
  return apiClient.post(lookupByMobile, requestData);
};

const apiForValidateOtp = (requestData) => {
  return apiClient.post(validateOtp, requestData);
};

const apiForSignUpUser = (requestData) => {
  return apiClient.post(signUpUser, requestData);
};

const apiForUpdateDeviceToken = (requestData) => {
  return apiClient.post(updateDeviceToken, requestData);
};

const apiForForgetPwdByMobile = (requestData) => {
  return apiClient.post(forgetPwdByMobile, requestData);
};

const apiForUpdatePassword = (requestData) => {
  return apiClient.post(updatePassword, requestData);
};

const apiForActivateUserSfid = (requestData) => {
  return apiClient.post(activateUserSfid, requestData);
};

const apiForForgetPwdByPan = (requestData) => {
  return apiClient.post(forgetPwdByPan, requestData);
};

const apiForChkDuplicateEmail = (requestData) => {
  return apiClient.post(checkDuplicateEmail, requestData);
};

const apiForGetUserData = (requestData) => {
  return apiClient.post(getUserData, requestData);
};

const apiForUpdateProfileData = (requestData) => {
  return apiClient.post(updateProfileData, requestData);
};

const apiForUpdateUserPassword = (requestData) => {
  return apiClient.post(updateUserpass, requestData);
};

const apiForUpdateUserProfile = (requestData) => {
  return apiClient.post(updateUserApi, requestData);
};

const apiForVerifyUserData = (requestData) => {
  return apiClient.post(verifyUserdataApi, requestData);
};

const getPreloginTermsData = () => {
  return apiClient.get(epPreLoginTerms);
};

const apiLogout = (requestData) => {
  return apiClient.post(epLogout, requestData);
};

// My Logic
const apiLoginUser = (requestData) => {
  return cpClient.postRequest(loginUser2, requestData, "FormData");
};

const apiGetProfile = (apiUrl) => {
  return cpClient.getRequestProfile(apiUrl);
};

const apiGetRequestData = (apiUrl) => {
  return cpClient.getRequestData(apiUrl);
};

const apiGetRequest = (apiUrl) => {
  return cpClient.getRequest(apiUrl);
};

export default {
  apiLandingLookupUserByPan,
  apiLandingSendOtp,
  apiLoginUser,
  apiLoginUserByMobileNPass,
  apiForlookupByMobile,
  apiForValidateOtp,
  apiForSignUpUser,
  apiForForgetPwdByMobile,
  apiForUpdatePassword,
  apiForActivateUserSfid,
  apiForForgetPwdByPan,
  apiForChkDuplicateEmail,
  apiForGetUserData,
  apiForUpdateDeviceToken,
  apiForUpdateProfileData,
  apiForUpdateUserPassword,
  apiForUpdateUserProfile,
  apiForVerifyUserData,
  getPreloginTermsData,
  apiLogout,
  apiLoginUserByPanNPass,
  apiGetProfile,
  apiGetRequestData,
  apiGetRequest
};
