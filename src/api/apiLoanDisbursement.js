import apiClient from './client';

const epLoanDetails = 'get_loan_details.json';
const epTermText = 'disclaimer_of_loan_inquiry';
const epLoanEnquiry = 'loan_enquiry.json';

const epCheckBankDetailsExist = "check_bank_details_exist.json";
const epSaveBankDetails = "save_bank_details.json";
const epUpdateBankDetails = "update_bank_details_data.json";
const epGetBankDetails = "get_bank_details.json";

const epAccountSummary = "account_summary_api.json";
const epSingleAccountSummary = "single_account_summary_api.json";
const epGetPropertyAddress = "get_property_address.json";
const epLoanDisclaimerContent = "get_loan_disclaimer_content.json";
const epSaveLoanDisbursement = "save_loan_disbursement.json";
const epExistLoanDisbursementData = "check_loan_disbursement_data_exist.json";
const epCheckCoverLetterExist = "check_cover_letter_exist.json";
const epUpdateCoverLetter = "update_cover_letter_data.json";
const epGetInvoiceReceipt = "get-invoice-receipt.json";
const epSendEmailToBanker = "send_email_to_banker.json";
const epGetArchitectureCertificate = "get_architecture_certificate.json";
const epCheckApplicantInfo = "check_applicant_info.json";
const epAddSplitPaymentDetails = "split_payment_details_add.json";
const epGetPaymentStatusInfo = 'get_postsale_payment_confirmation_status_v2.json';

const getLoanDetails = (requestData) => {
    return apiClient.post(epLoanDetails, requestData);
};

const getTermsText = () => {
    return apiClient.get(epTermText);
};

const loanEnquiry = (requestData) => {
    return apiClient.post(epLoanEnquiry, requestData);
};

const checkBankDetails = (requestData) => {
    return apiClient.post(epCheckBankDetailsExist, requestData);
}

const saveBankDetails = (requestData) => {
    return apiClient.post(epSaveBankDetails, requestData);
}

const updateBankDetails = (requestData) => {
    return apiClient.post(epUpdateBankDetails, requestData);
}

const getBankDetails = (requestData) => {
    return apiClient.post(epGetBankDetails, requestData);
}

const accountSummary = (requestData) => {
    return apiClient.post(epAccountSummary, requestData);
}

const singleAccountSummary = (requestData) => {
    return apiClient.post(epSingleAccountSummary, requestData);
}

const getPropertyAddress = (requestData) => {
    return apiClient.post(epGetPropertyAddress, requestData);
}

const loanDisclaimerContent = (requestData) => {
    return apiClient.post(epLoanDisclaimerContent, requestData);
}

const saveDisbursementDetails = (requestData) => {
    return apiClient.post(epSaveLoanDisbursement, requestData);
}

const existDisbursementDetails = (requestData) => {
    return apiClient.post(epExistLoanDisbursementData, requestData);
}

const getSignedCoverLetter = (requestData) => {
    return apiClient.post(epCheckCoverLetterExist, requestData);
}

const uploadSignedCoverLetter = (requestData) => {
    return apiClient.post(epUpdateCoverLetter, requestData);
}

const getInvoiceReceiptList = (requestData) => {
    return apiClient.post(epGetInvoiceReceipt, requestData);
}

const getArchitectureCertificate = (requestData) => {
    return apiClient.post(epGetArchitectureCertificate, requestData);
}

const sendBankerEmail = (requestData) => {
    return apiClient.post(epSendEmailToBanker, requestData);
}

const checkApplicantInfo = (requestData) => {
    return apiClient.post(epCheckApplicantInfo, requestData);
}

const addSplitPaymentDetails = (requestData) => {
    return apiClient.post(epAddSplitPaymentDetails, requestData);
}

const getPaymentStatus = (requestData) => {
    return apiClient.post(epGetPaymentStatusInfo, requestData);
}

export default {
    getLoanDetails,
    getTermsText,
    loanEnquiry,
    checkBankDetails,
    saveBankDetails,
    updateBankDetails,
    getBankDetails,
    accountSummary,
    singleAccountSummary,
    getPropertyAddress,
    loanDisclaimerContent,
    saveDisbursementDetails,
    existDisbursementDetails,
    getSignedCoverLetter,
    uploadSignedCoverLetter,
    getInvoiceReceiptList,
    getArchitectureCertificate,
    sendBankerEmail,
    checkApplicantInfo,
    addSplitPaymentDetails,
    getPaymentStatus
};
