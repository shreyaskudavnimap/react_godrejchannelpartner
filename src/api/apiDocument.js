import apiClient from './client';

const GET_USER_DOCUMENT_LIST = "get_user_document.json";//14
const Add_DOCUMENTS = "document_save199999.json";//28


// ionic api

const LIST_DOCUMENT = "V2/get_user_document.json";
const GET_DOCUMENT = "get_document_data.json";
const CREATE_DOCUEMNT = "create_document_api.json";
const PROPERTY_DOCUMENT_LIST = "api_property_document_list.json";
const BOOKED_PROPERTY = "get_user_document_property.json";
const DOCUMENT_DATA_LIST = "V2/get_user_document.json";

const api_GET_USER_DOCUMENT_LIST = (requestData) => {
    return apiClient.post(GET_USER_DOCUMENT_LIST, requestData);
};
const api_Add_DOCUMENTS = (requestData) => {
    // return apiClient.post(Add_DOCUMENTS, requestData);
    const apiUrl = "https://0d5af0c8e517.ngrok.io/webapi/"
    let options = {
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        method: 'POST'
    };
    options.body = new FormData();
    options.body.append("Files", requestData);
    return fetch(apiUrl + "GenericFileUpload.ashx", options)
        .then((response) => response.json())
        .then((responseJson) => {
            return responseJson;
        });
};

// ionic api function
const api_LIST_DOCUMENT = (requestData) => {
    return apiClient.post(LIST_DOCUMENT, requestData);
};

export default {
    api_GET_USER_DOCUMENT_LIST,
    api_Add_DOCUMENTS,
    api_LIST_DOCUMENT

};