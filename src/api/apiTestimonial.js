import apiClient from './client';

const epGetTestimonial = 'get_testimonials_data.json'
const epAddTestimonial = 'add_testimonial.json';
const epTestimonialDiclaimer = 'disclaimer_of_use_testimonial';

/**
 * 
 * @param {object} requestData 
 * @description Get all testimonial data
 */
const apiTestimonialList = (requestData) => {
  return apiClient.post(epGetTestimonial, requestData);
};

/**
 * 
 * @param { object } epAddTestimonial 
 */
const apiAddTestimonial = (requestData)=>{
  return apiClient.post(epAddTestimonial,requestData);
}

/**
 * 
 * @description Get testimonal terms and conditions
 */
const getTestimonialDisclaimer = ()=>{
  return apiClient.get(epTestimonialDiclaimer)
}
export default {
  apiTestimonialList,
  apiAddTestimonial,
  getTestimonialDisclaimer
};