import apiClient from './client';

const accSummary = 'account_summary_api.json';
const singleAccSummary = 'single_account_summary_api.json';
const propOverview = 'api_property_overview.json';
const accStatement = 'account_summary_account_statement.json';
const interestStatement = 'account_summary_interest_statement.json';
const receiptAndInvoice = 'get-invoice-receipt-direct.json';
const interestStatementDetails = 'get_statement_summery.json';
const userJourney = 'my-journey.json';

const apiAccSummary = (requestData) => {
  return apiClient.post(accSummary, requestData);
};

const apiSingleAccSummary = (requestData) => {
  return apiClient.post(singleAccSummary, requestData);
};

const apiPropOverview = (requestData) => {
  return apiClient.post(propOverview, requestData);
};

const apiAccStatement = (requestData) => {
  return apiClient.post(accStatement, requestData);
};

const apiInterestStatement = (requestData) => {
  return apiClient.post(interestStatement, requestData);
};

const apiReceiptAndInvoice = (requestData) => {
  return apiClient.post(receiptAndInvoice, requestData);
};

const apiInterestSummaryDetails = (requestData) => {
  return apiClient.post(interestStatementDetails, requestData);
};

const apiUserJourney = (requestData) => {
  return apiClient.post(userJourney, requestData);
};

const apiUserJourneyCTA = (urlFrag, requestData) => {
  return apiClient.post(urlFrag, requestData);
};

export default {
  apiAccSummary,
  apiSingleAccSummary,
  apiPropOverview,
  apiAccStatement,
  apiInterestStatement,
  apiReceiptAndInvoice,
  apiInterestSummaryDetails,
  apiUserJourney,
  apiUserJourneyCTA,
};
