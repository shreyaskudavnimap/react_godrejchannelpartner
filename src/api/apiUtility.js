import apiClient from './client';

const epVersionCheck = 'app_version_check.json';
const epUserBadgeCount = 'get_user_badge_count_api.json';

const apiVersionCheck = (requestData) => {
  return apiClient.post(epVersionCheck, requestData);
};

const apiUserBadgeCount = (requestData) => {
  return apiClient.post(epUserBadgeCount, requestData);
};

export default {
  apiVersionCheck,
  apiUserBadgeCount
};
