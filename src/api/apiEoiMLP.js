import apiClient from './client';

const epEoiFiltersData = 'get_eoi_filters.json';

const apiEoiFiltersData = (requestData) => {
  return apiClient.post(epEoiFiltersData, requestData);
};

const apiCheckProjectAvailability = (projectId) => {
  return apiClient.get(`ProjectStatus/${projectId}/?_format=json`);
}

export default {
    apiEoiFiltersData,
    apiCheckProjectAvailability
};
