import apiClient from './client';

const SNAG_LIST = 'view-saved-snag-list.json'
const ADD_SNAG_ITEM = 'add-snag-item.json'
const GET_SNAG_ITEM_TO_EDIT = 'get-snag-item-for-edit.json'
const GET_VIEW_SNAG_LIST = "view-snag-list.json"
const DELETE_SNAG_ITEM = 'delete-snag-item.json'
const SUBMIT_SNAG_LIST = 'submit-snag-list.json'

const getSavedSnagList = (requestData) => {
    return apiClient.post(SNAG_LIST, requestData);
};

const addSnagItem = (requestData) => {
    return apiClient.post(ADD_SNAG_ITEM, requestData);
}

const getSnagItemToEdit = (requestData) => {
    return apiClient.post(GET_SNAG_ITEM_TO_EDIT, requestData);  
} 

const getViewSnagList = (requestData) => {
    return apiClient.post(GET_VIEW_SNAG_LIST, requestData);  
} 

const deleteSnagItem = (requestData) => {
    return apiClient.post(DELETE_SNAG_ITEM, requestData);  
} 

const submitSnagList = (requestData) => {
    return apiClient.post(SUBMIT_SNAG_LIST, requestData);  
} 

export default {
    getSavedSnagList,
    addSnagItem,
    getSnagItemToEdit,
    getViewSnagList,
    deleteSnagItem,
    submitSnagList
};