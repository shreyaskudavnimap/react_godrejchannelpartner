import apiClient from './client';

const landingPageVideoInfo = 'get_landing_info.json';
const landingPageContinueExpoloringInfo = 'get_recent_searched_project.json';
const landingPageLivingExperienceInfo =
  'get_experience_wise_projects_list.json';
const landingPageCityWisePropertyInfo = 'get_featured_project_citywise.json';
const landingPageProjectByTypeProperty = 'get_project_bytype.json';
const landingPageAskExpert = 'ask_expert_phone.json';

const apiLandingPageVideoInfo = (requestData) => {
  return apiClient.post(landingPageVideoInfo, requestData);
};

const apiLandingPageContinueExpoloringInfo = (requestData) => {
  return apiClient.post(landingPageContinueExpoloringInfo, requestData);
};

const apiLandingPageLivingExperienceInfo = (requestData) => {
  return apiClient.post(landingPageLivingExperienceInfo, requestData);
};

const apiLandingPageCityWiseProperty = (requestData) => {
  return apiClient.post(landingPageCityWisePropertyInfo, requestData);
};

const apiProjectByTypeProperty = (requestData) => {
  return apiClient.post(landingPageProjectByTypeProperty, requestData);
};

const apiLandingPageAskExpert = () => {
  return apiClient.get(landingPageAskExpert);
};

export default {
  apiLandingPageVideoInfo,
  apiLandingPageContinueExpoloringInfo,
  apiLandingPageLivingExperienceInfo,
  apiLandingPageCityWiseProperty,
  apiProjectByTypeProperty,
  apiLandingPageAskExpert
};
