import apiClient from './client';

const epWishlistItems = 'res_wishlist_items.json'
const epAddRemoveWishlist = 'res_wishlist.json'

/**
 * 
 * @param { object } requestData 
 * @description Get wish list data
 */

const apiWishlistItems = (requestData) => {
  return apiClient.post(epWishlistItems, requestData);
};

/**
 * 
 * @param {object} requestData 
 * @description remove project from wishlist
 */
const apiAddRemoveWishlist = (requestData) => {
  return apiClient.post(epAddRemoveWishlist, requestData);
};

export default {
  apiWishlistItems,
  apiAddRemoveWishlist
};
