import apiClient from './client';

const epModuleList = 'get_module_list.json';
const epSaveUserAuthorizationData = 'save_user_authorization_data.json';

const apiModuleList = (requestData) => {
  return apiClient.post(epModuleList, requestData);
};

const apiSaveUserAuthorizationData = (requestData) => {
  return apiClient.post(epSaveUserAuthorizationData, requestData);
};

export default {
  apiModuleList,
  apiSaveUserAuthorizationData,
};
