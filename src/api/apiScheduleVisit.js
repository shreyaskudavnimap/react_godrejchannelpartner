import apiClient from './client';

const epGetProjectsList = 'get_project_list.json'
const epScheduleVisit = 'book_visit.json'

const apiProjectsList = () => {
  return apiClient.get(epGetProjectsList);
};

const apiScheduleVisit = (requestData) => {
  return apiClient.post(epScheduleVisit, requestData);
}

export default {
  apiProjectsList,
  apiScheduleVisit
};
