import apiClient from './client';

const epNotificationItems = "get_user_notifications_api.json";
const epNotificationCount = "change_notification_count_api.json";
const epReadNotification = 'change_read_status_notification_api.json';

/**  
 * 
 * @param { object } requestData 
 * @description Get notification data
 */

const apiNotificationListItems = (requestData) => {
    return apiClient.post(epNotificationItems, requestData);
};

/**
 * 
 * @param {object} requestData 
 * @description Count notification
 */
const notificationCount = (requestData) => {
    return apiClient.post(epNotificationCount, requestData);
}

/**
 * 
 * @param {object} requestData 
 * @description Read notification
 */
const readNotification = (requestData) => {
    return apiClient.post(epReadNotification, requestData);
}

export default {
    apiNotificationListItems,
    notificationCount,
    readNotification
};
