import {create} from 'apisauce';

import appConstant from './../utility/appConstant';

const apiClient = create({
  baseURL: appConstant.buildInstance.baseUrl,
  headers: {
    'Content-Type': 'application/json',
    'X-CSRF-TOKEN': '',
    'x-requested-with': '',
    Accept:
      appConstant.buildInstance.buildType != ''
        ? appConstant.buildInstance.buildType +
          '|' +
          appConstant.buildInstance.appVersion
        : 'STORE' + '|' + appConstant.buildInstance.appVersion,
  },
});

export default apiClient;
