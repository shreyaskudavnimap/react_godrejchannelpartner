import apiClient from './client';

const epPaymentPlan = 'V2/get_payment_plan.json';
const epPaymentPlanDetails = 'get_payment_plan_details.json';
const epCostSheet = 'res_booking_cross_breakup.json';
const epBookingPropertyInfo = 'V2/get_booking_property_info.json';
const epSourceControl = 'get_source_control_info.json';
const epBookingAddData = 'V2/res_booking_chk_add.json';
const epCheckBookingStatus = 'get_booking_confirmation_status.json';
const epBookingConfirmationData = 'booking_confirmation_data.json';

const apiPaymentPlan = (requestData) => {
  return apiClient.post(epPaymentPlan, requestData);
};

const apiPaymentPlanDetails = (requestData) => {
  return apiClient.post(epPaymentPlanDetails, requestData);
};

const apiCostSheet = (requestData) => {
  return apiClient.post(epCostSheet, requestData);
};

const apiBookingPropertyInfo = (requestData) => {
  return apiClient.post(epBookingPropertyInfo, requestData);
};

const apiSourceControl = (requestData) => {
  return apiClient.post(epSourceControl, requestData);
};

const apiBookingAddData = (requestData) => {
  return apiClient.post(epBookingAddData, requestData);
};

const apiCheckBookingStatus = (requestData) => {
  return apiClient.post(epCheckBookingStatus, requestData);
};

const apiBookingConfirmationData = (requestData) => {
  return apiClient.post(epBookingConfirmationData, requestData);
};

const apiBookingTerms = (projectId) => {
  return apiClient.get(`term_of_use_payment/${projectId}`);
};

export default {
  apiPaymentPlan,
  apiPaymentPlanDetails,
  apiCostSheet,
  apiBookingPropertyInfo,
  apiSourceControl,
  apiBookingAddData,
  apiCheckBookingStatus,
  apiBookingConfirmationData,
  apiBookingTerms,
};
