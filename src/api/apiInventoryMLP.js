import apiClient from './client';

const epTypologyList = 'get_typology_info.json';
const epTowerList = 'get_tower_list.json';
const epFloorList = 'get_floor_list.json';
const epInventoryList = 'get_inv_list.json';
const epCheckInventoryStatus = 'check_inventory_status.json';
const epInventoryInfo = 'get_inv_info.json';

const apiTypologyList = (requestData) => {
  return apiClient.post(epTypologyList, requestData);
};

const apiTowerList = (requestData) => {
  return apiClient.post(epTowerList, requestData);
};

const apiFloorList = (requestData) => {
  return apiClient.post(epFloorList, requestData);
};

const apiInventoryList = (requestData) => {
  return apiClient.post(epInventoryList, requestData);
};

const apiCheckProjectAvailability = (projectId) => {
  return apiClient.get(`ProjectStatus/${projectId}/?_format=json`);
}

const apiCheckInventoryStatus = (requestData) => {
  return apiClient.post(epCheckInventoryStatus, requestData);
}

const apiInventoryInfo = (requestData) => {
  return apiClient.post(epInventoryInfo, requestData);
}

export default {
  apiTypologyList,
  apiTowerList,
  apiFloorList,
  apiInventoryList,
  apiCheckProjectAvailability,
  apiCheckInventoryStatus,
  apiInventoryInfo
};
