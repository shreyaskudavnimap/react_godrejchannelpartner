import apiClient from './client';

const epGetProjectsList = 'get_project_list.json'
const epRequestCallBack = 'request_callback.json'

const apiProjectsList = () => {
  return apiClient.get(epGetProjectsList);
};

const apiRequestCallBack = (requestData) => {
  return apiClient.post(epRequestCallBack, requestData);
}

export default {
  apiProjectsList,
  apiRequestCallBack
};
