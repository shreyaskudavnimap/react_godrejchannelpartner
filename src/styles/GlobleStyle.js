import { StyleSheet } from 'react-native';
import appFonts from "../config/appFonts"

export default StyleSheet.create({
  container: {
    flex: 1
  },
  welcome: {
    fontSize: appFonts.xlargeFontSize
  },
  loginTabTextActive:{
      color:'#fff',
      fontWeight:'bold',
      fontSize:appFonts.largeFontSize,
      paddingBottom:'10%',
    alignSelf:'stretch',
    justifyContent:'center',
    alignItems:'center',
  },
  loginTabTextInActive:{
    color:'#fff',
    fontSize:appFonts.largeFontSize,
    paddingBottom:'10%',
    alignSelf:'stretch',
    justifyContent:'center',
    alignItems:'center',
},
loginTabActive:{
    width:'100%',
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'stretch',
    borderBottomColor:"#fff",
    borderBottomWidth:2,
    position: 'absolute',
    bottom:0,
},
loginTabInActive:{
    width:'100%',
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'stretch',
    borderBottomColor:"gray",
    borderBottomWidth:1,
    position: 'absolute',
    bottom:0,
},
forgetPwdText:{
    color:'#fff',
    fontSize:appFonts.largeFontSize,
    fontWeight:'bold'
},
footerMenu: {
  flexDirection: "row",
  position: "absolute",
  bottom: 0,
  marginBottom: 0,
  height: 50,
  // margin:'1%',
  justifyContent: "center",
  width: "100%",
  backgroundColor: "white"
},
featuredResidences: {
  fontWeight:'bold',
  color:'gray',
  fontSize:appFonts.largeFontSize
},
projectName:{
  color:'#000', 
  fontWeight: 'bold',
  fontSize: appFonts.xlargeFontSize,
  textAlign:'center' ,
  marginTop:'1%',
  marginBottom:'1%'
},
projectLocation:{
  color:'gray', 
  fontWeight: 'bold',
  fontSize: appFonts.normalFontSize,
  textAlign:'center' ,
  marginBottom:'1%'
},
headingProjectLocation:{
    color:'#000', 
    fontWeight: 'bold',
    fontSize: appFonts.xlargeFontSize,
    marginTop:'1%',
    marginBottom:'1%'
}

});