import { Dimensions, Platform, PixelRatio } from 'react-native';

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT
} = Dimensions.get('window');

const scaleWidth = SCREEN_WIDTH / 380;
const scaleHeight = SCREEN_HEIGHT / 736;

export const normalizeWidth = (size) => {
    const newSize = size * scaleWidth;
    if(Platform.OS == 'ios'){
      return Math.round(PixelRatio.roundToNearestPixel(newSize))
    }
    return Math.round(PixelRatio.roundToNearestPixel(newSize))
};

export const normalizeHeight = (size) => {
    const newSize = size * scaleHeight;
    if(Platform.OS == 'ios'){
        return Math.round(PixelRatio.roundToNearestPixel(newSize))
    }
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
};