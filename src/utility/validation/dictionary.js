export const validationDictionary = {
  bool: {
    presence: {
      allowEmpty: false,
      message: '^This is required',
    },
  },

  day: {
    presence: {
      allowEmpty: false,
      message: '^This is required',
    },
    numericality: {
      greaterThan: 0,
      lessThanOrEqualTo: 31,
      message: '^Must be valid',
    },
  },

  email: {
    presence: {
      allowEmpty: false,
      message: '^This is required',
    },
    email: {
      message: '^Email address must be valid',
    },
  },

  generic: {
    presence: {
      allowEmpty: false,
      message: '^This is a required field',
    },
  },

  integer: {
    presence: {
      allowEmpty: false,
      message: '^This is required',
    },
    numericality: {
      greaterThan: 0,
      onlyInteger: true,
      message: '^Must be valid',
    },
  },

  month: {
    presence: {
      allowEmpty: false,
      message: '^This is required',
    },
    numericality: {
      greaterThan: 0,
      lessThanOrEqualTo: 12,
      message: '^Must be valid',
    },
  },

  password: {
    presence: {
      allowEmpty: false,
      message: '^This is required',
    },
    length: {
      minimum: 6,
      message: '^Password must be at least 6 characters long',
    },
  },

  phone: {
    presence: {
      allowEmpty: false,
      message: '^This is required',
    },
    format: {
      pattern: /^[2-9]\d{2}-\d{3}-\d{4}$/,
      message: '^Phone number must be valid',
    },
  },

  zip: {
    presence: {
      allowEmpty: false,
      message: '^This is required',
    },
    length: {
      is: 5,
      message: '^Zip must be 5 digits long',
    },
  },

  houseNo: {
    presence: {
      allowEmpty: false,
      message: '^House No. is required',
    },
    format: {
      pattern: "[a-zA-Z0-9.%$#!@(),/'‘’.:\\-_ ]+",
      message: "^Special characters allowed are ._%$#!@,/':-()",
    },
    length: {
      maximum: 40,
      message: '^Maximum character limit is 40',
    },
  },

  street: {
    presence: {
      allowEmpty: false,
      message: '^Street is required',
    },
    format: {
      pattern: "[a-zA-Z0-9.%$#!@(),/'‘’.:\\-_ ]+",
      message: "^Special characters allowed are ._%$#!@,/':-()",
    },
    length: {
      maximum: 40,
      message: '^Maximum character limit is 40',
    },
  },

  locality: {
    presence: {
      allowEmpty: false,
      message: '^Locality is required',
    },
    format: {
      pattern: "[a-zA-Z0-9.%$#!@(),/'‘’.:\\-_ ]+",
      message: "^Special characters allowed are ._%$#!@,/':-()",
    },
    length: {
      maximum: 40,
      message: '^Maximum character limit is 40',
    },
  },

  indPincode: {
    presence: {
      allowEmpty: false,
      message: '^Pincode is required',
    },
    format: {
      pattern: '^[0-9]*$',
      message: '^Not a valid Pincode',
    },
    length: {
      is: 6,
      message: '^Pincode must be 6 characters long',
    },
  },

  nriPincode: {
    presence: {
      allowEmpty: false,
      message: '^Pincode is required',
    },
    format: {
      pattern: /^[ A-Za-z0-9']*$/,
      message: '^Not a valid Pincode',
    },
  },

  city: {
    presence: {
      allowEmpty: false,
      message: '^City is required',
    },
    format: {
      pattern: /^[ A-Za-z']*$/,
      message: '^City should be alphabets only',
    },
    length: {
      maximum: 25,
      message: '^Maximum character limit is 25',
    },
  },

  state: {
    presence: {
      allowEmpty: false,
      message: '^State is required',
    },
    format: {
      pattern: /^[ A-Za-z']*$/,
      message: '^State should be alphabets only',
    },
    length: {
      maximum: 25,
      message: '^Maximum character limit is 25',
    },
  },

  commHouseNo: {
    presence: {
      allowEmpty: false,
      message: '^Communication House No. is required',
    },
    format: {
      pattern: "[a-zA-Z0-9.%$#!@(),/'‘’.:\\-_ ]+",
      message: "^Special characters allowed are ._%$#!@,/':-()",
    },
    length: {
      maximum: 40,
      message: '^Maximum character limit is 40',
    },
  },

  commStreet: {
    presence: {
      allowEmpty: false,
      message: '^Communication Street is required',
    },
    format: {
      pattern: "[a-zA-Z0-9.%$#!@(),/'‘’.:\\-_ ]+",
      message: "^Special characters allowed are ._%$#!@,/':-()",
    },
    length: {
      maximum: 40,
      message: '^Maximum character limit is 40',
    },
  },

  commLocality: {
    presence: {
      allowEmpty: false,
      message: '^Communication Locality is required',
    },
    format: {
      pattern: "[a-zA-Z0-9.%$#!@(),/'‘’.:\\-_ ]+",
      message: "^Special characters allowed are ._%$#!@,/':-()",
    },
    length: {
      maximum: 40,
      message: '^Maximum character limit is 40',
    },
  },

  commIndPincode: {
    presence: {
      allowEmpty: false,
      message: '^Communication Pincode is required',
    },
    format: {
      pattern: '^[0-9]*$',
      message: '^Not a valid Pincode',
    },
    length: {
      is: 6,
      message: '^Pincode must be 6 characters long',
    },
  },

  commNriPincode: {
    presence: {
      allowEmpty: false,
      message: '^Communication Pincode is required',
    },
    format: {
      pattern: /^[ A-Za-z0-9']*$/,
      message: '^Not a valid Pincode',
    },
  },

  commCity: {
    presence: {
      allowEmpty: false,
      message: '^Communication City is required',
    },
    format: {
      pattern: /^[ A-Za-z']*$/,
      message: '^Communication City should be alphabets only',
    },
    length: {
      maximum: 25,
      message: '^Maximum character limit is 25',
    },
  },

  commState: {
    presence: {
      allowEmpty: false,
      message: '^Communication State is required',
    },
    format: {
      pattern: /^[ A-Za-z']*$/,
      message: '^Communication State should be alphabets only',
    },
    length: {
      maximum: 25,
      message: '^Maximum character limit is 25',
    },
  },

  panNo: {
    presence: {
      allowEmpty: false,
      message: '^PAN Number is required',
    },
    format: {
      pattern: '^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$',
      message: '^Not a valid PAN Number',
    },
  },

  passportNo: {
    presence: {
      allowEmpty: false,
      message: '^Passport Number is required',
    },
    format: {
      pattern: '^(?!^0+$)[a-zA-Z0-9]{3,20}$',
      message: '^Not a valid Passport Number',
    },
  },

  genericIDProof: {
    presence: {
      allowEmpty: false,
      message: '^Please select ID Proof document type',
    },
  },

  genericAddressProof: {
    presence: {
      allowEmpty: false,
      message: '^Please select Address Proof document type',
    },
  },

  genericPurposeOfPurchase: {
    presence: {
      allowEmpty: false,
      message: '^Please select Purpose of Purchase',
    },
  },

  genericDob: {
    presence: {
      allowEmpty: false,
      message: '^Date of Birth is required',
    },
  },

  indMobile: {
    presence: {
      allowEmpty: false,
      message: '^Mobile Number is required',
    },
    format: {
      pattern: '^(?=.*[0-9])[0-9]+$',
      message: '^Please enter a valid Mobile Number',
    },
    length: {
      is: 10,
      message: '^Mobile Number should be 10 digits',
    },
  },

  nriMobile: {
    presence: {
      allowEmpty: false,
      message: '^Mobile Number is required',
    },
    format: {
      pattern: '^(?=.*[0-9])[0-9]+$',
      message: '^Please enter a valid Mobile Number',
    },
  },

  genericPrefferedDate: {
    presence: {
      allowEmpty: false,
      message: '^Preferred Date is required',
    },
  },

  genericPrefferedTime: {
    presence: {
      allowEmpty: false,
      message: '^Preferred Time is required',
    },
  },

  genericName: {
    presence: {
      allowEmpty: false,
      message: '^Name is required',
    },
    format: {
      pattern: /^[ A-Za-z']*$/,
      message: '^Name should be alphabets only',
    },
  },

  genericEmail: {
    presence: {
      allowEmpty: false,
      message: '^Email is required',
    },
    format: {
      pattern:
        '^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\\-+)|([A-Za-z0-9]+\\.+)|([A-Za-z0-9]+\\++))*[A-Za-z0-9]+@([a-zA-Z0-9-]+[.]){1,2}[a-zA-Z]{2,10}$',
      message: '^Please enter a valid Email',
    },
  },
};
