import Snackbar from 'react-native-snackbar';
import colors from '../config/colors';
import appFonts from '../config/appFonts';

const onShowSnakBar = (text, duration = 'SHORT') => {
  Snackbar.show({
    text: text,
    duration:
      duration === 'SHORT' ? Snackbar.LENGTH_SHORT : Snackbar.LENGTH_LONG,
    fontFamily: appFonts.SourceSansProRegular,
    textColor: colors.primary,
    backgroundColor: colors.secondaryDark,
  });
};

export default {
  onShowSnakBar,
};

// const onShowSnakBar = () => {
//   appSnakBar.onShowSnakBar('Test Snakbar..');
// };
