const landingScreenCurrencyFormat = (value) => {
  const divisibleBy = 10000000;
  let originalValue;
  let amount;
  originalValue = Math.abs(value ? value : 0);
  if (originalValue >= divisibleBy) {
    amount = originalValue / divisibleBy;
    originalValue = amount.toFixed(2) + ' Cr. onwards';
  } else if (originalValue <= divisibleBy) {
    amount = (originalValue / divisibleBy) * 100;
    if (amount >= 2) {
      originalValue = amount.toFixed(2) + ' Lakh onwards';
    } else {
      originalValue = amount.toFixed(2) + ' Lakh onwards';
    }
  }
  return originalValue;
};

const getSearchCurrencyFormat = (value) => {
  const divisibleBy = 10000000;
  let originalValue;
  if (value == '' || value == null || value == 0) {
    return (originalValue = '0');
  } else {
    originalValue = Math.abs(value ? value : 0);
    if (originalValue >= divisibleBy) {
      originalValue = (originalValue / divisibleBy).toFixed(2) + ' Crore';
    } else if (originalValue <= divisibleBy) {
      originalValue =
        ((originalValue / divisibleBy) * 100).toFixed(2) + ' Lakh';
    }
    return originalValue;
  }
};

const getRoundedCurrencyFormat = (value) => {
  const divisibleBy = 10000000;
  let originalValue;
  if (value == '' || value == null || value == 0) {
    return (originalValue = '0');
  } else {
    originalValue = Math.abs(value ? value : 0);
    if (originalValue >= divisibleBy) {
      const calculateValueCr = (originalValue / divisibleBy).toFixed(2);
      originalValue = Math.ceil(Number(calculateValueCr)) + ' Cr.';
    } else if (originalValue <= divisibleBy) {
      const calculateValueLk = ((originalValue / divisibleBy) * 100).toFixed(2);
      originalValue = Math.ceil(Number(calculateValueLk)) + ' Lakh';
    }
    return originalValue;
  }
};

const getIndianCurrencyFormat = (price) => {
  if (typeof price == undefined || price == "" || price == null) {
    return "0.00";
  } else {
    const indianPrice = price.toString();
    if (typeof indianPrice == undefined || indianPrice == "") {
      return "0.00";
    } else {
      try {
        let preVal = "";
        let deciVal = "";

        if (indianPrice.includes(".")) {
          preVal = indianPrice.split(".")[0];
          deciVal = indianPrice.split(".")[1].substring(0, 2);
          if (deciVal.length == 1) {
            deciVal = deciVal + "0";
          }
        } else {
          preVal = indianPrice;
          deciVal = "00";
        }

        let lastThree = preVal.substring(preVal.length - 3);
        let otherNumbers = preVal.substring(0, preVal.length - 3);
        if (otherNumbers != "") lastThree = "," + lastThree;
        let res =
          otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

        if (typeof res == undefined) {
          return indianPrice;
        } else {
          return res + "." + deciVal;
        }
      } catch (error) {
        return "0.00";
      }
    }
  }
}

const getIndianCurrencyFormatWithoutDecimal = (price) => {
  if (typeof price == undefined || price == "" || price == null) {
    return "0";
  } else {
    const indianPrice = price.toString();
    if (typeof indianPrice == undefined || indianPrice == "") {
      return "0";
    } else {
      try {
        let preVal = "";

        if (indianPrice.includes(".")) {
          preVal = indianPrice.split(".")[0];
        } else {
          preVal = indianPrice;
        }

        let lastThree = preVal.substring(preVal.length - 3);
        let otherNumbers = preVal.substring(0, preVal.length - 3);
        if (otherNumbers != "") lastThree = "," + lastThree;
        let res =
          otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

        if (typeof res == undefined) {
          return indianPrice;
        } else {
          return res;
        }
      } catch (error) {
        return "0";
      }
    }
  }
}

const getPriceInWord = (value) => {
  const divisibleBy = 10000000;
  const divisibleByLakh = 100000;
  let originalValue;
  if (value == "" || value == null || value == 0) {
    return (originalValue = "0");
  } else {
    originalValue = Math.abs(value ? value : 0);
    if (originalValue >= divisibleBy) {
      originalValue = (originalValue / divisibleBy).toFixed(2) + " Cr.";
    } else if (originalValue <= divisibleBy) {
      if (originalValue >= divisibleByLakh) {
        originalValue =
          ((originalValue / divisibleBy) * 100).toFixed(2) + " Lakh";
      } else if (originalValue <= divisibleByLakh) {
        originalValue =
          ((originalValue / divisibleByLakh) * 100).toFixed(2) + " Thousand";
      }
    }

    let returnOrgValue = originalValue;
    let splitOriginalValueSpace = originalValue.split(" ");
    if (
      splitOriginalValueSpace != null &&
      splitOriginalValueSpace.length > 0
    ) {
      if (
        splitOriginalValueSpace[0].split(".") != null &&
        (splitOriginalValueSpace[0].split(".")[1] === "0" ||
          splitOriginalValueSpace[0].split(".")[1] === "00")
      ) {
        returnOrgValue =
          Number(splitOriginalValueSpace[0]).toFixed(0) +
          " " +
          splitOriginalValueSpace[1];
      }
    }

    return returnOrgValue;
  }
}

export default {
  landingScreenCurrencyFormat,
  getSearchCurrencyFormat,
  getRoundedCurrencyFormat,
  getIndianCurrencyFormat,
  getIndianCurrencyFormatWithoutDecimal,
  getPriceInWord
};
