import * as AddCalendarEvent from 'react-native-add-calendar-event';
import moment from 'moment';

const utcDateToString = (momentInUTC) => {
	let s = moment.utc(momentInUTC).format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
	return s;
};

const addCalenderEvent = (title, startDateUTC, endDateUTC, location = null, allDay = true, url = null, notes = null) => {
	const eventConfig = {
		title: title,
		startDate: utcDateToString(startDateUTC),
		endDate: utcDateToString(endDateUTC),
		location: location,
		allDay: allDay,
		// url: url,
		notes: notes,
		navigationBarIOS: {
			tintColor: 'orange',
			backgroundColor: 'green',
			titleColor: 'blue',
		},
	};

	AddCalendarEvent.presentEventCreatingDialog(eventConfig)
		.then((eventInfo) => {
			// handle success - receives an object with `calendarItemIdentifier` and `eventIdentifier` keys, both of type string.
			// These are two different identifiers on iOS.
			// On Android, where they are both equal and represent the event id, also strings.
			// when { action: 'CANCELED' } is returned, the dialog was dismissed
			// console.warn(JSON.stringify(eventInfo));
		}).catch((error) => {
			// handle error such as when user rejected permissions
			// console.warn(error);
		});
};

const editCalendarEventWithId = (eventId, title, startDateUTC, endDateUTC, location = null, allDay = true, url = null, notes = null) => {
	if (!eventId) {
		alert('Please Insert Event Id');
		return;
	}
	
	const eventConfig = {
		eventId,
		title: title,
		startDate: utcDateToString(startDateUTC),
		endDate: utcDateToString(endDateUTC),
		location: location,
		allDay: allDay,
		// url: url,
		notes: notes,
		navigationBarIOS: {
			tintColor: 'orange',
			backgroundColor: 'green',
			titleColor: 'blue',
		},
	};

	AddCalendarEvent.presentEventEditingDialog(eventConfig)
		.then((eventInfo) => {
			// alert('eventInfo -> ' + JSON.stringify(eventInfo));
		}).catch((error) => {
			// alert('Error -> ' + error);
		});
};

const showCalendarEventWithId = (eventId) => {
	if (!eventId) {
		alert('Please Insert Event Id');
		return;
	}
	const eventConfig = {
		eventId,
		allowsEditing: true,
		allowsCalendarPreview: true,
		navigationBarIOS: {
			tintColor: 'orange',
			backgroundColor: 'green',
		},
	};

	AddCalendarEvent.presentEventViewingDialog(eventConfig)
		.then((eventInfo) => {
			// alert('eventInfo -> ' + JSON.stringify(eventInfo));
		}).catch((error) => {
			// alert('Error -> ' + error);
		});
};

export default { addCalenderEvent };

// Green Glades at Godrej Garden City, Ahmedabad || latitude: 23.115972 || longitude: 72.555944
// Godrej Zest, Bangalore || latitude: 12.8570439 || longitude: 77.5418356