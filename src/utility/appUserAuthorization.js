import moment from 'moment';
import apiUserAuthorization from './../api/apiUserAuthorization';

const validateUserAuthorization = async (requestData) => {

  const apiModuleListRequest = {device_id: requestData.deviceId};
  try {
    const moduleListResult = await apiUserAuthorization.apiModuleList(
      apiModuleListRequest,
    );
    const resultDataModuleList = moduleListResult.data;

    if (resultDataModuleList && resultDataModuleList.status == '200') {
      const selectedModule = resultDataModuleList.data.find(
        (e) => e.name === requestData.moduleName,
      );
      if (selectedModule) {
        const authorizationRequest = {
          user_id: requestData.userId,
          //   user_name: requestData.customerName,
          //   user_email_id: requestData.emailId,
          //   user_mobile_no: requestData.mobileNo,
          field_device_id: requestData.deviceId,
          field_device_platform: requestData.devicePlatform
            ? requestData.devicePlatform.toUpperCase()
            : '',
          field_latitude: requestData.latiTude,
          field_longitude: requestData.longiTude,
          field_module_id: selectedModule.id,
          field_type: selectedModule.type,
          field_alias: selectedModule.url_alias,
        //   date_time: moment(new Date()).format('DD-MM-YYYY hh:mm:ss'),
        };
        const authorizationResult = await apiUserAuthorization.apiSaveUserAuthorizationData(
          authorizationRequest,
        );
      }
    }
  } catch (error) {
    console.log('apiError: ', error);
  }
};

export default {
  validateUserAuthorization,
};
