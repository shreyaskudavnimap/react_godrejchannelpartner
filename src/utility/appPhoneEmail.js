import {Linking} from 'react-native';

const openDeviceEmail = (mailTo, subject, body) => {
  Linking.openURL(`mailto:${mailTo}?subject=${subject}&body=${body}`)
};

export default {openDeviceEmail};
