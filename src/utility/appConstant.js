const IMAGE_PLACEHOLDER = 'https://via.placeholder.com/200?text=No+image';

const qaInstance = {
  appVersion: 'Q-2.0.77',
  baseUrl: 'http://43.242.212.209/gpl-project/gpl-api/',
  buildType: 'QA',
  googleAnalyticsTrackingID: 'UA-170777927-1',
  plumb5Key: 'p5m1a2i3sdk400',
  draftAgreementStage: '27',
  powerOfAttorneyStage: '28',
};

const prodInstance = {
  appVersion: 'P-2.0.79',
  baseUrl: 'https://customercare.godrejproperties.com/gpl-api/',
  buildType: 'P',
  googleAnalyticsTrackingID: 'UA-171278332-1',
  plumb5Key: 'p5m1a2i3sdk500',
  draftAgreementStage: '27',
  powerOfAttorneyStage: '28',
};

const storeInstance = {
  appVersion: '2.0.76',
  baseUrl: 'https://customercare.godrejproperties.com/gpl-api/',
  buildType: '',
  googleAnalyticsTrackingID: 'UA-171278332-1',
  plumb5Key: 'p5m1a2i3sdk500',
  draftAgreementStage: '27',
  powerOfAttorneyStage: '28',
};

// const buildInstance = qaInstance;
const buildInstance = prodInstance;  
// const buildInstance = storeInstance;

const PROJECT_SFDC_ID_LIST = ['a1l2s000000XmaMAAS'];

const isPlotProject = (sfdcId) => {
  const plotProjectIdArr = PROJECT_SFDC_ID_LIST;
  let is_plot_project = false;
  if (plotProjectIdArr instanceof Array) {
    plotProjectIdArr.forEach(function (item) {
      if (item == sfdcId) {
        is_plot_project = true;
      }
    });
  }
  return is_plot_project;
};

const formatDate = (last_visited) => {
  if (last_visited != '') {
    let dob = new Date(last_visited.split('T')[0]);
    let dobArr = dob.toDateString().split(' ');
    let dobFormat = dobArr[2] + ' ' + dobArr[1] + ' ' + dobArr[3];

    let timesplit = last_visited.split('T')[1].split(':');
    let hours = timesplit[0];
    let minutes = timesplit[1];

    let ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'

    if (minutes == '00' || minutes.charAt(0) == '0') {
      minutes = minutes < 10 ? minutes : minutes;
    } else {
      minutes = minutes < 10 ? '0' + minutes : minutes;
    }

    let time = hours + ':' + minutes + ' ' + ampm;
    return dobFormat + ' ' + time;
  } else {
    return '';
  }
};

const timeSinceAgo = (date) => {

  var seconds = Math.floor((new Date() - new Date(date)) / 1000);

  var interval = seconds / 31536000;

  if (interval > 1) {
    return Math.floor(interval) + (Math.floor(interval)>1?" years":" year")+ " ago";
  }
  interval = seconds / 2592000;
  if (interval > 1) {
    return Math.floor(interval) + (Math.floor(interval)>1?" months":" month")+ " ago";
  }
  interval = seconds / 86400;
  if (interval > 1) {
    return Math.floor(interval) + (Math.floor(interval)>1?" days":" day")+ " ago";
  }
  interval = seconds / 3600;
  if (interval > 1) {
    return Math.floor(interval) + (Math.floor(interval)>1?" hours":" hour")+ " ago";
  }
  interval = seconds / 60;
  if (interval > 1) {
    return Math.floor(interval) + (Math.floor(interval)>1?" minutes":" minute")+ " ago";
  }
  return Math.floor(seconds) + (Math.floor(interval)>1?" seconds":" second")+ " ago";
};

const appMessage = {
  NO_DATA: 'No Data Available',
  APP_GENERIC_ERROR: 'Something went wrong. Please try again',
  NO_BOOKING_AVAILABLE_ALERT_MESSAGE:
    'Online booking for this project is currently unavailable',
  NO_BOOKING_AVAILABLE_ALERT_TITLE: 'Oops!',
  NO_BOOKING_AVAILABLE_ALERT_POSITIVE_BUTTON: 'OK',
  LOAN_ENQUIRY_ALERT_TITLE: 'Terms & Conditions',
  LOAN_ENQUIRY_ALERT_POSITIVE_BUTTON: 'I accept',
  LOAN_ENQUIRY_ALERT_NEGATIVE_BUTTON: 'Cancel',
  LOAN_ENQUIRY_ALERT_CLOSE_BUTTON: 'Close',
  COST_SHEET_BREAKUP_STATUS_ERROR:
    'Something went wrong. Try with some other unit',
  COST_SHEET_BREAKUP_CURL_REPONSE_ERROR:
    'Something went wrong. Try with some other unit',
  /***** BANK DETAILS **********/
  BANK_NAME_REQUIRED: 'Bank Name is required',
  BANKER_EMAIL_REQUIRED: 'Banker Email Id is required',
  LOAN_AMOUNT_REQUIRED: 'Loan Amount is required',
  AMOUNT_INVALID: 'Invalid amount',
  LOAN_AC_NO_REQUIRED: 'Loan Account Number is required',
  LOAN_AC_NO_INVALID: 'Loan Account Number is invalid',
  EMAIL_INVALID: 'Not a valid Email ID',
  APP_UPDATE_TITLE: 'Update Available',
  APP_UPDATE_MSG:
    'A new version of Godrej Customer Portal mobile application is available in store. Please update to the new version.',
    PROMO_CODE_REMOVE: 'Coupon removed successfully',
    PROMO_CODE_ADD: 'Coupon applied successfully and payment plan has been updated!',
};

const getExtention = (filename) => {
  return /[.]/.exec(filename) ? /[^.]+$/.exec(filename) : undefined;
};

const getFileName = (fileUrl) => {
  let mediaArr = fileUrl.split('/');
  return mediaArr[mediaArr.length - 1]
    ? mediaArr[mediaArr.length - 1]
    : undefined;
};

const fileNameSanitize = (fileName) => {
  let fNameArr = fileName.split('.');
  let fName = fNameArr[0];
  let fileExt = fNameArr[fNameArr.length - 1];
  let lengthFname = fName.length;
  let sanitizeFileName = fileName;
  if (lengthFname > 25) {
    sanitizeFileName =
      fName.substr(0, 18) + '...' + fName.substr(-5) + '.' + fileExt;
  }
  return sanitizeFileName;
};

const getJsonFromUrl = (url) => {
  if (!url) {
    url = location.href;
  }
  let question = url.indexOf('?');
  let hash = url.indexOf('#');
  if (hash == -1 && question == -1) {
    return {};
  }
  if (hash == -1) {
    hash = url.length;
  }
  let query =
    question == -1 || hash == question + 1
      ? url.substring(hash)
      : url.substring(question + 1, hash);
  let result = {};
  query.split('&').forEach(function (part) {
    if (!part) {
      return;
    }
    part = part.split('+').join(' '); // replace every + with space, regexp-free version
    let eq = part.indexOf('=');
    let key = eq > -1 ? part.substr(0, eq) : part;
    let val = eq > -1 ? decodeURIComponent(part.substr(eq + 1)) : '';
    let from = key.indexOf('[');
    if (from == -1) {
      result[decodeURIComponent(key)] = val;
    } else {
      let to = key.indexOf(']', from);
      let index = decodeURIComponent(key.substring(from + 1, to));
      key = decodeURIComponent(key.substring(0, from));
      if (!result[key]) {
        result[key] = [];
      }
      if (!index) {
        result[key].push(val);
      } else {
        result[key][index] = val;
      }
    }
  });
  return result;
};

const googleMapKey = {
  key: 'AIzaSyBg2UIaZE_0hhQAM6cib4XabNi73Y3ReRk',
};

export default {
  qaInstance,
  prodInstance,
  storeInstance,
  appMessage,
  getExtention,
  getFileName,
  getJsonFromUrl,
  googleMapKey,
  buildInstance,
  PROJECT_SFDC_ID_LIST,
  isPlotProject,
  fileNameSanitize,
  formatDate,
  timeSinceAgo
};

export {IMAGE_PLACEHOLDER};
