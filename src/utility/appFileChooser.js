import ImagePicker from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';

import RNFS, {stat} from 'react-native-fs';

const onLaunchCamera = (callback) => {
  try {
    ImagePicker.launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
      },
      (response) => {
        callback(response);
      },
    );
  } catch (error) {
    console.log('onLaunchCamera : ', error);
    callback(null);
  }
};

const onLaunchImageLibrary = (callback) => {
  let options = {
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
  try {
    ImagePicker.launchImageLibrary(options,
      // {
      //   mediaType: 'photo',
      //   includeBase64: false,
      //   maxHeight: 3500,
      //   maxWidth: 3500,
      // },
      (response) => {
        console.log("response", response);
        response.name = response?.fileName;
        callback(response);
      },
    );
  } catch (error) {
    console.log('Error onLaunchImageLibrary : ', error);
    callback(null);
  }
};

const onLaunchCameraVideo = (callback) => {
  try {
    ImagePicker.launchCamera({mediaType: 'video'}, (response) => {
      callback(response);
    });
  } catch (error) {
    console.log('Error onLaunchCameraVideo : ', error);
    callback(null);
  }
};

const onLaunchImageLibraryVideo = (callback) => {
  try {
    ImagePicker.launchImageLibrary(
      {mediaType: 'video', includeBase64: false, maxHeight: 250, maxWidth: 250},
      (response) => {
        // console.log('video response : ', response);
        callback(response);
      },
    );
  } catch (error) {
    console.log('Error onLaunchImageLibraryVideo : ', error);
    callback(null);
  }
};

const onSelectFilePdf = () =>
  DocumentPicker.pick({
    type: [DocumentPicker.types.pdf, DocumentPicker.types.images],
  });

const onSelectFile = () =>
  DocumentPicker.pick({
    type: [
      DocumentPicker.types.pdf,
      DocumentPicker.types.images,
      DocumentPicker.types.video,
    ],
    //There can me more options as well
    // DocumentPicker.types.allFiles
    // DocumentPicker.types.images
    // DocumentPicker.types.plainText
    // DocumentPicker.types.audio
    // DocumentPicker.types.pdf
  });

const validateFileSize = (fileSize, callback) => {
  const bytes = Number(fileSize);
  if (bytes && bytes < 5120000) {
    callback(true);
  } else {
    callback(false);
  }
};

const toBase64 = (filePath) =>
  RNFS.readFile(filePath, 'base64').then((res) => {
    return res;
  });

const getVideoDetails = async (filePath) => {
  return await stat(filePath);
};

export default {
  onLaunchCamera,
  onLaunchImageLibrary,
  onLaunchCameraVideo,
  onLaunchImageLibraryVideo,
  onSelectFilePdf,
  onSelectFile,
  validateFileSize,
  toBase64,
  getVideoDetails,
};
