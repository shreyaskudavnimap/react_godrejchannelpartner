import { Alert } from 'react-native';

const showAppAlert = (title, msg) => {
    Alert.alert(
        title,
        msg,
        [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
        ]
    );
};

export default {
    showAppAlert,
};
