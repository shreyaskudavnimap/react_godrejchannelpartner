import {Linking} from 'react-native';

const openDeviceCaller = (phoneNumber) => {
  Linking.openURL(`tel:${phoneNumber}`);
};

export default {openDeviceCaller};
