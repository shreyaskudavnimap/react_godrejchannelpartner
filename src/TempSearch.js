import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  FlatList,
  Modal,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppButton from '../components/ui/AppButton';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import AppText from '../components/ui/ AppText';
import AppSearchFilterListItem from '../components/ui/AppSearchFilterListItem';
import SelectLocationModalScreen from './modals/SelectLocationModalScreen';

import appCurrencyFormatter from './../utility/appCurrencyFormatter';

import {useDispatch, useSelector} from 'react-redux';

import * as propertySearchAction from './../store/actions/propertySearchAction';
import appSnakBar from '../utility/appSnakBar';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
var constSec;
var constFirst;
var selected_data = '';
var remains_city_data = [];
var selected_typology = '';
var selected_possesion = '';
var city_term_menu = '';
const SearchFilterScreen = ({navigation}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState();
  const [selectModalVisible, setSelectModalVisible] = useState(false);
  const [selectedCity, setSelectedCity] = useState([]);
  const [displayCity, setDisplayCity] = useState('Select Location');
  const [multiSliderValue, setMultiSliderValue] = useState(['990000.00', '30000000.00']);
  const [multiSliderUpdate, setmultiSliderUpdate] = useState(['990000.00', '30000000.00']);

  const onSelectedItemsChange = (selectedItems) => {
    setSelectedItems(selectedItems);
  };

  const dispatch = useDispatch();

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );

  const propertySearchData = useSelector((state) => state.propertySearch);

  const propertyTypeTermMenu = propertySearchData.propertyTypeTermMenu;
  const cityTermMenu = propertySearchData.cityTermMenu;
  const subLocationTermMenu = propertySearchData.subLocationTermMenu;
  const projectStatus = propertySearchData.projectStatus;
  const typologyTermMenu = propertySearchData.typologyTermMenu;
  const budgetData = propertySearchData.budgetData;
  const priceRange = propertySearchData.priceRange;
  const lastSearch = propertySearchData.lastSearch;
  const lastDesect = propertySearchData.lastDesect;

  // setMultiSliderValue([budgetData.min,budgetData.max])

  useEffect(() => {
    if (deviceUniqueId) {
      setDisplayCity('Select Location');
      getPropertySearchData(
        '',
        deviceUniqueId,
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        lastSearch,
        lastDesect,
      );
    }
  }, [dispatch, deviceUniqueId]);

  useEffect(() => {
      setMultiSliderValue([budgetData.min,budgetData.max])
      setmultiSliderUpdate([priceRange.min,priceRange.max])
  }, [budgetData,priceRange]);

  const getPropertySearchData = useCallback(
    async (
      userId,
      deviceId,
      propertyTypeTermMenu,
      cityTermMenu,
      subLocation,
      projectStatus,
      typologyTermMenu,
      minPrice,
      maxPrice,
      lastSearch,
      lastDesect,
    ) => {
      setIsLoading(true);
      setError(null);
      try {
        await dispatch(
          propertySearchAction.getPropertySearchData(
            userId,
            deviceId,
            propertyTypeTermMenu,
            cityTermMenu,
            subLocation,
            projectStatus,
            typologyTermMenu,
            minPrice,
            maxPrice,
            lastSearch,
            lastDesect,
          ),
        );
        setIsLoading(false);
      } catch (err) {
        appSnakBar.onShowSnakBar(err.message, 'LONG');
        navigation.goBack();
        setError(err.message);
        setIsLoading(false);
      }
    },
    [dispatch, setIsLoading, setError, isLoading, deviceUniqueId],
  );

  const onHandleOpenLocationModal = () => {
    if (cityTermMenu && cityTermMenu.length > 0) {
      setSelectedCity([]);
      setSelectModalVisible(true);
    } else {
      appSnakBar.onShowSnakBar('No location found', 'LONG');
    }
  };

  const onSelectCity = (city) => {
    
    console.warn('city..///',city)
    cityTermMenu.map((cityItem) => {
      if (cityItem.id === city.id) {
        cityItem.selected = !cityItem.selected;
      }
    });
    setSelectedCity([...selectedCity, city]);
    // dispatch(propertySearchAction.setCityTermMenu(cityTermMenu));
  };

  const onSelectCityUpdate = () => {
    let displayText = '';
    cityTermMenu.map((cityItem) => {
      if (cityItem.selected) {
        
        if (displayText === '') {
          displayText = cityItem.name;
        } else {
          displayText = displayText + ', ' + cityItem.name;
        }
      }
    });


    
    for(let data of cityTermMenu){
      if(data.selected){
        var item = data.id;
        
        selected_data+= item+',';
      }  else {
        remains_city_data.push(data)
      }
    }


    let property_type_term_menu = '1';
    
    let sub_location = '';
    let project_status = selected_possesion
    let typology_term_menu = selected_typology;
    let min_price = multiSliderValue[0];
    let max_price = multiSliderValue[1];

    city_term_menu = selected_data.toString();

    getTermeSelectData(
      '',
      deviceUniqueId,
      property_type_term_menu,
      city_term_menu,
      sub_location,
      project_status,
      typology_term_menu,
      min_price,
      max_price,
      'location',
      remains_city_data
    );

    
    setDisplayCity(displayText);
    setSelectModalVisible(false);
  };




  const getTermeSelectData = useCallback(
    async (
      userId,
      deviceId,
      propertyTypeTermMenu,
      cityTermMenu,
      subLocation,
      projectStatus,
      typologyTermMenu,
      minPrice,
      maxPrice,
      lastSearch,
      lastDesect,
    ) => {
      setIsLoading(true);
      setError(null);
      try {
        await dispatch(
          propertySearchAction.setCityTermMenu(
            userId,
            deviceId,
            propertyTypeTermMenu,
            cityTermMenu,
            subLocation,
            projectStatus,
            typologyTermMenu,
            minPrice,
            maxPrice,
            lastSearch,
            lastDesect,
          ),
        );
        setIsLoading(false);
      } catch (err) {
        appSnakBar.onShowSnakBar(err.message, 'LONG');
        navigation.goBack();
        setError(err.message);
        setIsLoading(false);
      }
    },
    [dispatch, setIsLoading, setError, isLoading, deviceUniqueId],
  );


  const onCancelCityUpdate = () => {
    setSelectModalVisible(false);
    console.warn('{{{{{{{{{{{{{{{');
    cityTermMenu.map((cityMainItem) => {
      selectedCity.map((citySelectedItem) => {
        if (cityMainItem.id === citySelectedItem.id) {
          cityMainItem.selected = false;
        }
      });
    });
    // dispatch(propertySearchAction.setCityTermMenu(cityTermMenu));
  };

  const multiSliderValuesChange = (values) => {
    constFirst = values[0].toString()
    constSec = values[1].toString()
    let price_value =[];
    price_value.push(constFirst,constSec)
    
    setMultiSliderValue(price_value);
    
  }

  const onSelectProjectStatus = (item, index) => {
    
    setmultiSliderUpdate([priceRange.min,priceRange.max])
    let data= [];
    let data_dupe = data.concat(projectStatus)
    let lastDesect_data = data_dupe.splice(index,1)
    let property_type_term_menu = '1';
    let sub_location = '';
    let min_price = multiSliderValue[0];
    let max_price = multiSliderValue[1];
    getPossesionSelectData(
      '',
      deviceUniqueId,
      property_type_term_menu,
      city_term_menu,
      sub_location,
      item,
      selected_typology,
      min_price,
      max_price,
      'possession',
      data_dupe,
    );  

    for(let data of lastDesect_data){
      var item = data.id;
      
      selected_possesion+= item+',';
  }

  };

  const getPossesionSelectData = useCallback(
    async (
      userId,
      deviceId,
      propertyTypeTermMenu,
      cityTermMenu,
      subLocation,
      projectStatus,
      typologyTermMenu,
      minPrice,
      maxPrice,
      lastSearch,
      lastDesect,
    ) => {
      setIsLoading(true);
      setError(null);
      try {
        await dispatch(
          propertySearchAction.setProjectStatus(
            userId,
            deviceId,
            propertyTypeTermMenu,
            cityTermMenu,
            subLocation,
            projectStatus,
            typologyTermMenu,
            minPrice,
            maxPrice,
            lastSearch,
            lastDesect,
          ),
        );
        setIsLoading(false);
      } catch (err) {
        appSnakBar.onShowSnakBar(err.message, 'LONG');
        navigation.goBack();
        setError(err.message);
        setIsLoading(false);
      }
    },
    [dispatch, setIsLoading, setError, isLoading, deviceUniqueId],
  );




  const onSelectTypology = (item, index) => {
    
    let data= [];
    let data_dupe = data.concat(typologyTermMenu)
    let lastDesect_data = data_dupe.splice(index,1)
    let property_type_term_menu = '1';
    let sub_location = '';
    let min_price = multiSliderValue[0];
    let max_price = multiSliderValue[1];
    
    
    getTopologySelectData(
      '',
      deviceUniqueId,
      property_type_term_menu,
      city_term_menu,
      sub_location,
      selected_possesion,
      item,
      min_price,
      max_price,
      'typology',
      data_dupe,
    ); 

    for(let data of lastDesect_data){
        var item = data.id;
        
        selected_typology+= item+',';
    }
  };

  const getTopologySelectData = useCallback(
    async (
      userId,
      deviceId,
      propertyTypeTermMenu,
      cityTermMenu,
      subLocation,
      projectStatus,
      typologyTermMenu,
      minPrice,
      maxPrice,
      lastSearch,
      lastDesect,
    ) => {
      setIsLoading(true);
      setError(null);
      try {
        await dispatch(
          propertySearchAction.setTypologyTermMenu(
            userId,
            deviceId,
            propertyTypeTermMenu,
            cityTermMenu,
            subLocation,
            projectStatus,
            typologyTermMenu,
            minPrice,
            maxPrice,
            lastSearch,
            lastDesect,
          ),
        );
        setIsLoading(false);
      } catch (err) {
        appSnakBar.onShowSnakBar(err.message, 'LONG');
        navigation.goBack();
        setError(err.message);
        setIsLoading(false);
      }
    },
    [dispatch, setIsLoading, setError, isLoading, deviceUniqueId],
  );




  const onSearchFilterData = () => {

    getLastSelectData(
      '',
      deviceUniqueId,
      '1',
      '',
      '',
      '',
      multiSliderValue[0],
      multiSliderValue[1],
      selected_possesion.toString(),
      selected_typology.toString(),
      '',
      0,
    );
    navigation.replace('SearchResult')
  };


  const getLastSelectData = useCallback(
    async (
      userId,
      deviceId,
      project_status,
      location,
      lat,
      lng,
      minPrice,
      maxPrice,
      possession,
      typology,
      sublocation,
      page
    ) => {
      setIsLoading(true);
      setError(null);
      try {
        await dispatch(
          propertySearchAction.setLastSearch(
            userId,
            deviceId,
            project_status,
            location,
            lat,
            lng,
            minPrice,
            maxPrice,
            possession,
            typology,
            sublocation,
            page
          ),
        );
        setIsLoading(false);
      } catch (err) {
        appSnakBar.onShowSnakBar(err.message, 'LONG');
        navigation.goBack();
        setError(err.message);
        setIsLoading(false);
      }
    },
    [dispatch, setIsLoading, setError, isLoading, deviceUniqueId],
  );



  return (
    <Screen>
      <AppHeader />

      {isLoading ? (
        <View style={styles.container}></View>
      ) : (
        <ScrollView>
          <View style={styles.container}>
            <AppTextBold style={styles.pageTitle}>SEARCH</AppTextBold>

            <TouchableWithoutFeedback
              onPress={() => onHandleOpenLocationModal()}>
              <View style={styles.itemContainer}>
                <AppText
                numberOfLines = {1}
                  style={[
                    styles.itemTxt,
                    {
                      fontFamily: appFonts.SourceSansProSemiBold,
                      fontSize: appFonts.largeBold,
                    },
                  ]}>
                  {displayCity}

                </AppText>
                <Image
                  style={styles.arrow}
                  source={require('./../assets/images/down-icon-b.png')}
                />
              </View>
            </TouchableWithoutFeedback>

            {projectStatus && projectStatus.length > 0 && (
              <View style={styles.possesionsView}>
                <AppText style={styles.possesionsTitle}>Possession</AppText>
                <FlatList
                  showsHorizontalScrollIndicator={false}
                  horizontal
                  data={projectStatus}
                  renderItem={({item, index}) => (
                    <AppSearchFilterListItem
                      title={item.name}
                      index={index}
                      color={item.selected ? 'jaguar' : 'primary'}
                      textColor={item.selected ? 'primary' : 'jaguar'}
                      onPress={() => onSelectProjectStatus(item, index)}
                    />
                  )}
                  keyExtractor={(item) => item.id}
                />
              </View>
            )}
            {projectStatus && projectStatus.length <= 0 && (
              <View style={styles.possesionsView}>
                <AppText style={styles.possesionsTitle}>No Possession Found</AppText>
              </View>
            )}

            {typologyTermMenu && typologyTermMenu.length > 0 && (
              <View style={styles.typologyView}>
                <AppText style={styles.typologyTitle}>Typology</AppText>
                <FlatList
                  showsHorizontalScrollIndicator={false}
                  horizontal
                  data={typologyTermMenu}
                  renderItem={({item, index}) => (
                    <AppSearchFilterListItem
                      title={item.name}
                      index={index}
                      color={item.selected ? 'jaguar' : 'primary'}
                      textColor={item.selected ? 'primary' : 'jaguar'}
                      onPress={() => onSelectTypology(item,index)}
                    />
                  )}
                  keyExtractor={(item) => item.id}
                />
              </View>
            )}

            <View style={styles.priceRangeView}>
              <AppText style={styles.priceRangeTitle}>Price Range</AppText>

              <View style={styles.placeHolderView}>
                {/* {budgetData.min ? (
                  <AppText style={styles.placeHolderAmount}>
                    ₹
                    {appCurrencyFormatter.getSearchCurrencyFormat(
                      multiSliderValue[0],
                    )}
                  </AppText>
                ) : null} */}
                <AppText style={styles.placeHolderAmount}>
                    ₹
                    {appCurrencyFormatter.getSearchCurrencyFormat(
                      multiSliderValue[0],
                    )}
                  </AppText>
                {budgetData.max ? (
                  <AppText style={styles.placeHolderAmount}>
                    ₹
                    {appCurrencyFormatter.getSearchCurrencyFormat(
                      multiSliderValue[1],
                    )}
                  </AppText>
                ) : null}
              </View>

              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <MultiSlider
                  trackStyle={{backgroundColor: colors.lightGray, height: 5}}
                  selectedStyle={{backgroundColor: colors.jaguar, height: 8}}
                  twoMarkerValue={2}
                  oneMarkerLeftPosition={2}
                  sliderLength={windowWidth * 0.88}
                  enabledTwo={true}
                  // values={[multiSliderUpdate[0], multiSliderUpdate[1]]}
                  values={[parseInt(multiSliderUpdate[0]), parseInt(multiSliderUpdate[1])]}
                  onValuesChange={multiSliderValuesChange}
                  markerStyle={styles.markerStyle}
                  min={parseInt(multiSliderValue[0])}
                  max={parseInt(multiSliderValue[1])}
                  step= {1000}
                />
              </View>
            </View>

            <View style={styles.searchBtn}>
              <AppButton
                title="SEARCH"
                onPress={onSearchFilterData}
              />
            </View>
          </View>
        </ScrollView>
      )}

      <Modal
        animationType="fade"
        transparent={true}
        visible={selectModalVisible}
        onRequestClose={() => {
          setSelectModalVisible(!selectModalVisible);
        }}>
        <SelectLocationModalScreen
          cities={cityTermMenu}
          onSelectCity={(city) => onSelectCity(city)}
          onCancel={onCancelCityUpdate}
          onUpdate={onSelectCityUpdate}
        />
      </Modal>

      <AppOverlayLoader isLoading={isLoading} />

      <AppFooter activePage={2} isPostSales={false} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.jaguar,
    textTransform: 'uppercase',
  },

  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    marginTop: 24,
    marginVertical: 20,
    backgroundColor: '#f7f7f7',
  },
  itemTxt: {
    color: colors.jaguar,
    marginRight: 5
  },
  arrow: {height: 16, width: 16},
  placeHolderView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    fontFamily: 'SourceSansPro-SemiBold',
  },
  placeHolderAmount: {
    fontSize:appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
    marginTop: 15,
  },
  possesionsView: {
    marginVertical: 20,
  },
  possesionsTitle: {
    color: colors.jaguar,
    fontSize: appFonts.xlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    marginBottom: 8,
  },
  typologyView: {
    marginVertical: 20,
  },
  typologyTitle: {
    color: colors.jaguar,
    fontSize: appFonts.xlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    marginBottom: 8,
  },
  priceRangeView: {
    marginVertical: 20,
  },
  priceRangeTitle: {
    color: colors.jaguar,
    fontSize: appFonts.xlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    marginBottom: 8,
  },
  markerStyle: {
    backgroundColor: colors.primary,
    height: 18,
    width: 18,
    borderWidth: 1,
    borderColor: colors.gray,
    top: 2,
  },
  searchBtn: {
    marginVertical: 20,
  },
});

export default SearchFilterScreen;
