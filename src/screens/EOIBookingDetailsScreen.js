import React, {useState} from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import AppExpandButton from '../components/ui/AppExpandButton';
import ApplicantDetailsComponent from '../components/ApplicantDetailsComponent';
import AppBookingPropertyDetails from '../components/ui/AppBookingPropertyDetails';

import appFonts from '../config/appFonts';
import colors from '../config/colors';

const windowWidth = Dimensions.get('window').width;

const propertyDetails = {
  proj_name: 'Godrej SE7EN',
  sample_unit: 'U01',
  unit_type: '1 BHK',
};

const paymentDetails = {
  starting_price: '34.55 Lakh',
  token_amount: '50,000',
  booking_number: '999999',
};

const ApplicantDetails = [
  {
    id: '1',
    title: 'Nationality',
    detail: 'Indian',
  },
  {
    id: '2',
    title: 'Full Name',
    detail: 'Atul Kocchar',
  },
  {
    id: '3',
    title: 'Mobile Number',
    detail: '9876543210',
  },
  {
    id: '4',
    title: 'Email',
    detail: 'atul_kocchar@gmail.com',
  },
  {
    id: '5',
    title: 'Date of Birth',
    detail: '30 Jan 2020',
  },
  {
    id: '6',
    title: 'Permanent Account Number (PAN)',
    detail: 'APSHR8527L',
  },
  {
    id: '7',
    title: 'Uploaded',
    detail: 'Aadhaar Card',
  },
  {
    id: '8',
    title: 'Uploaded',
    detail: 'PANCard',
  },
];

const PermanentAddress = [
  {
    id: '1',
    title: 'Address line 1',
    detail: '1323 A Wing, Wadhwa Solitaire',
  },
  {
    id: '2',
    title: 'Address line 2',
    detail: 'Opp Powai Lake, JVLR, Powai',
  },
  {
    id: '3',
    title: 'City',
    detail: 'Mumbai',
  },
  {
    id: '4',
    title: 'Pin code',
    detail: '400076',
  },
];
const CommunicationAddress = [
  {
    id: '1',
    title: 'Address line 1',
    detail: 'Plot 143. Novel township',
  },
  {
    id: '2',
    title: 'Address line 2',
    detail: 'Cantonment road, Devlali',
  },
  {
    id: '3',
    title: 'City',
    detail: 'Nasik',
  },
  {
    id: '4',
    title: 'Pin code',
    detail: '422401',
  },
];

const EOIBookingDetailsScreen = (props) => {
  const [isExpandedPropertyDetails, setIsExpandedPropertyDetails] = useState(
    false,
  );
  const [isExpandedPaymentDetails, setIsExpandedPaymentDetails] = useState(
    false,
  );
  const [isExpandedBookingSource, setIsExpandedBookingSource] = useState(false);
  const [isExpandedApplicantDetails, setIsExpandedApplicantDetails] = useState(
    false,
  );

  return (
    <Screen>
      <AppHeader />

      <ScrollView>
        <View style={styles.container}>
          <AppTextBold style={styles.pageTitle}>
            PRE-BOOKING DETAILS
          </AppTextBold>

          {isExpandedPropertyDetails ? (
            <View style={styles.viewContainer}>
              <View style={styles.subTitle}>
                <TouchableWithoutFeedback
                  onPress={() => setIsExpandedPropertyDetails(false)}>
                  <View style={styles.titleLine}>
                    <AppText style={styles.title}>Unit Preference</AppText>
                    <Image
                      source={require('./../assets/images/up-icon-b.png')}
                      style={styles.icon}
                    />
                  </View>
                </TouchableWithoutFeedback>
              </View>
              <View style={{paddingHorizontal: 25}}>
                <AppTextBold>{propertyDetails.proj_name}</AppTextBold>
                {propertyDetails.sample_unit ? (
                  <AppBookingPropertyDetails
                    label="Sample Unit"
                    value={propertyDetails.sample_unit}
                  />
                ) : null}

                {propertyDetails.unit_type ? (
                  <AppBookingPropertyDetails
                    label="Unit Type"
                    value={propertyDetails.unit_type}
                  />
                ) : null}

                <AppText style={styles.info}>
                  The mentioned Sample Unit is only to represent the Typology
                  preference selected by you.
                </AppText>
                <AppText style={styles.info}>
                  Actual unit is subject to change and availability, and will be
                  allotted as per final selection .
                </AppText>
              </View>
            </View>
          ) : (
            <AppExpandButton
              title="Unit Preference"
              onPress={() => {
                setIsExpandedPropertyDetails(true);
              }}
            />
          )}

          {isExpandedPaymentDetails ? (
            <View style={styles.viewContainer}>
              <View style={styles.subTitle}>
                <TouchableWithoutFeedback
                  onPress={() => setIsExpandedPaymentDetails(false)}>
                  <View style={styles.titleLine}>
                    <AppText style={styles.title}>Payment Details</AppText>
                    <Image
                      source={require('./../assets/images/up-icon-b.png')}
                      style={styles.icon}
                    />
                  </View>
                </TouchableWithoutFeedback>
              </View>
              <View style={{paddingHorizontal: 25}}>
                {paymentDetails.starting_price ? (
                  <AppBookingPropertyDetails
                    label="Starting Price"
                    value={paymentDetails.starting_price}
                  />
                ) : null}
                {paymentDetails.token_amount ? (
                  <AppBookingPropertyDetails
                    label="Token Amount"
                    value={paymentDetails.token_amount}
                  />
                ) : null}
                {paymentDetails.booking_number ? (
                  <AppBookingPropertyDetails
                    label="Starting Price"
                    value={paymentDetails.booking_number}
                  />
                ) : null}
              </View>
            </View>
          ) : (
            <AppExpandButton
              title="Payment Details"
              onPress={() => {
                setIsExpandedPaymentDetails(true);
              }}
            />
          )}

          {isExpandedBookingSource ? (
            <View style={styles.viewContainer}>
              <View style={styles.subTitle}>
                <TouchableWithoutFeedback
                  onPress={() => setIsExpandedBookingSource(false)}>
                  <View style={styles.titleLine}>
                    <AppText style={styles.title}>Booking Source</AppText>
                    <Image
                      source={require('./../assets/images/up-icon-b.png')}
                      style={styles.icon}
                    />
                  </View>
                </TouchableWithoutFeedback>
                <AppText style={styles.sourceType}>
                  Existing Godrej Properties' customer
                </AppText>
                <AppText style={styles.source}>Pankaj</AppText>
              </View>
            </View>
          ) : (
            <AppExpandButton
              title="Booking Source"
              onPress={() => {
                setIsExpandedBookingSource(true);
              }}
            />
          )}

          {isExpandedApplicantDetails ? (
            <ApplicantDetailsComponent
              onPress={() => setIsExpandedApplicantDetails(false)}
              ApplicantDetails={ApplicantDetails}
              PermanentAddress={PermanentAddress}
              CommunicationAddress={CommunicationAddress}
            />
          ) : (
            <AppExpandButton
              title="Primary Applicant Details"
              onPress={() => {
                setIsExpandedApplicantDetails(true);
              }}
            />
          )}
        </View>
      </ScrollView>

      <AppFooter activePage={0} isPostSales={true} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 10,
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    paddingHorizontal: 25,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: '7%',
    textTransform: 'uppercase',
  },
  viewContainer: {
    width: windowWidth,
    paddingBottom: 20,
    marginBottom: 25,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  titleLine: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize:appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
    color: colors.jaguar,
  },
  icon: {
    width: 16,
    height: 16,
  },
  info: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
    marginVertical: 10,
  },
  subTitle: {
    paddingHorizontal: 25,
  },
  sourceType: {
    fontSize: appFonts.xlargeFontSize,
    marginBottom: 10,
  },
  source: {fontSize:appFonts.largeBold, color: colors.gray},
});

export default EOIBookingDetailsScreen;
