import React, { useState, useEffect } from 'react';
import {
  View,
  Image,
  TextInput,
  StyleSheet,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';

import { ProgressBar } from 'react-native-paper';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppFooter from '../components/ui/AppFooter';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppButton from '../components/ui/AppButton';
import AppNotePink from '../components/ui/AppNotePink';
import { validationDictionary } from '../utility/validation/dictionary';
import validatejs from 'validate.js';
import { useSelector } from 'react-redux';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment'
import { TouchableOpacity } from 'react-native-gesture-handler';
import apiLoanDisbursement from '../api/apiLoanDisbursement';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';
import useFileDownload from '../hooks/useFileDownload';
import AppProgressBar from '../components/ui/AppProgressBar';
import { NavigationContainer } from '@react-navigation/native';
import Orientation from 'react-native-orientation';


const LoanDisbursementForm2 = ({ navigation, route }) => {
  const [isLoading, setIsLoading] = useState(false);
  const routeParams = route.params;
  const booking_id = routeParams && routeParams.booking_id ? routeParams.booking_id : '';
  const [bookingId, setBookingId] = useState(booking_id);
  const disburseStep1 = routeParams && routeParams.disburseStep1 ? routeParams.disburseStep1 : '';
  console.log("disburseStep1", disburseStep1);
  const [printClicked, isPrintClicked] = useState(false);
  const loginvalue = useSelector(
    (state) => state.loginInfo.loginResponse,
  );
  const userId = typeof loginvalue.data !== 'undefined' ? loginvalue.data.userid : '';
  // console.log("DATE,", new Date(disburseStep1.payable_date));
  const [inputs, setInputs] = useState({
    payable_at_location: {
      type: "generic",
      value: disburseStep1.payable_location,
    },
    payable_date: {
      type: "generic",
      value: disburseStep1.payable_date ? new Date(disburseStep1.payable_date) : new Date(),
    },
    disbursement_date: {
      type: "generic",
      value: disburseStep1.disbursement_date ? new Date(disburseStep1.disbursement_date) : new Date(),
    },
    disbursement_time: {
      type: "generic",
      value: disburseStep1.disbursement_date ? new Date(disburseStep1.disbursement_date) : new Date(),
    },
    present_stage_construction: {
      type: "generic",
      value: disburseStep1.present_stage_construct,
    }
  });
  const [mode, setMode] = useState('date');

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  // const [dateClicked, isDateClicked] = useState(false);
  // const [timeClicked, isTimeClicked] = useState(false);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [showDisburseDatePicker, setShowDisburseDatePicker] = useState(false);
  const [showTimePicker, setShowTimePicker] = useState(false);
  const openDatepicker = () => {
    console.log("Payable Date", showDatePicker);
    setShowDatePicker(true);
    setMode('date');
  };

  const openDisburseDatePicker = () => {
    console.log("Showdatepicker", showDisburseDatePicker);
    setShowDisburseDatePicker(true);
    setMode('date');
  };
  const [payableDate, setPayableDate] = useState(disburseStep1.payable_date ? moment(new Date(disburseStep1.payable_date)).format("DD MMM YYYY") : "");
  const [disbursementDate, setDisbursementDate] = useState(disburseStep1.disbursement_date ? moment(new Date(disburseStep1.disbursement_date)).format("DD MMM YYYY") : "");
  const [disburseTime, setDisburseTime] = useState(disburseStep1.disbursement_date ? moment(new Date(disburseStep1.disbursement_date)).format("hh:mm A") : "");

  const onPayableDateChange = (event, selectedDate) => {
    let { type } = event
    console.log("selectedDate============", selectedDate);
    console.log("type============", type);
    setShowDatePicker(false);
    if (typeof selectedDate !== 'undefined') {
      selectedDate = type === "set" ? selectedDate : new Date();
      // setShowDatePicker(Platform.OS === 'ios');
      if (type === "set") {
        setPayableDate(moment(selectedDate).format("DD MMM YYYY"));
        onInputChange('payable_date', selectedDate ? selectedDate : new Date());
      }
    }
  };
  const onDisburseDateChange = (event, selectedDate) => {

    setShowDisburseDatePicker(false);
    let { type } = event;
    if (typeof selectedDate !== 'undefined') {
      selectedDate = type === "set" ? selectedDate : new Date();
      // setShowDatePicker(Platform.OS === 'ios');
      if (type === "set") {
        setDisbursementDate(moment(selectedDate).format("DD MMM YYYY"));
        onInputChange('disbursement_date', selectedDate ? selectedDate : new Date());
      }


    }
  };

  const showTimepicker = () => {
    setShowTimePicker(true);
    setMode('time');
  };

  const onTimeChange = (event, selectedDate, field) => {
    setShowTimePicker(false);
    let { type } = event;
    // console.log("selectedTime============", selectedDate)
    if (typeof selectedDate !== 'undefined') {
      selectedDate = type === "set" ? selectedDate : new Date();
      // setShowTimePicker(Platform.OS === 'ios');
      if (type === "set") {
        setDisburseTime(moment(selectedDate).format("hh:mm A"))
        onInputChange('disbursement_time', selectedDate ? selectedDate : new Date());
      }
    }

  };

  const getInputValidationState = ({ input, value }) => {
    return {
      ...input,
      value,
      errorLabel: input.optional
        ? null
        : validateInput({ type: input.type, value }),
    };
  };

  const validateInput = ({ type, value }) => {
    const result = validatejs(
      {
        [type]: value,
      },
      {
        [type]: validationDictionary[type],
      },
    );

    if (result) {
      return result[type][0];
    }

    return null;
  };

  const getFormValidation = () => {
    const updatedInputs = {};

    for (const [key, input] of Object.entries(inputs)) {
      updatedInputs[key] = getInputValidationState({
        input,
        value: input.value,
      });
    }
    setInputs(updatedInputs);
  };

  const renderError = (id) => {
    if (inputs[id].errorLabel) {
      return <AppText style={styles.txtError}>{inputs[id].errorLabel}</AppText>;
    }
    return null;
  };

  const renderErrorDropdown = (id) => {
    if (inputs[id].errorLabel) {
      return (
        <View>
          <AppText style={styles.txtErrorDoc}>{inputs[id].errorLabel}</AppText>
        </View>
      );
    }
    return null;
  };

  const onInputChange = (id, value) => {
    setInputs({
      ...inputs,
      [id]: getInputValidationState({
        input: inputs[id],
        value,
      }),
    });
  };

  const handlePrintSave = async () => {
    setIsLoading(true);
    try {
      const requestData = {
        user_id: userId,
        booking_id: bookingId,
        borrower_name: disburseStep1.borrower_name,
        hdfc_file_number: disburseStep1.hdfc_file_number,
        loan_amount: disburseStep1.loan_amount,
        property_address: disburseStep1.property_details,
        selected_payment_mode: disburseStep1.selected_payment_mode,
        in_favour_of: '',
        account_number: '',
        bank_name: '',
        ifsc_code: '',
        branch_name: '',
        payable_at_location: inputs.payable_at_location.value,
        payable_date: moment(inputs.payable_date.value).format("YYYY-MM-DD"),
        avail_this_disbursement: '',
        present_stage_construction: inputs.present_stage_construction.value,
        disbursement_date: moment(inputs.disbursement_date.value).format("YYYY-MM-DD"),
        disbursement_time: moment(inputs.disbursement_time.value).format("hh:mm A"),
        splitPayment: disburseStep1.splitPayment,
        loan_amount_disbursed: disburseStep1.loan_amount_disbursed
      };
      const response = await apiLoanDisbursement.saveDisbursementDetails(requestData);
      const resultData = response.data
      if (resultData.status == "200") {
        setIsLoading(false);
        downloadCoverLetter();
        isPrintClicked(true);
      }
    } catch (error) {
      console.log(error);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  }

  const { progress, showProgress, error, isSuccess, checkPermission, } = useFileDownload();
  const downloadCoverLetter = () => {
    const fileName = bookingId + "_LoanDisbursementForm.pdf";
    const coverLetterUrl = encodeURI(
      appConstant.buildInstance.baseUrl + "loan_disbursement_hdfc_download/" + bookingId
    );
    if (fileName) {
      const ext = fileName.split('.')[0];
      if (ext) {
        downloadFile(coverLetterUrl, fileName, ext);
      }
    } else {
      let fileExt = appConstant.getExtention(coverLetterUrl);

      const fName = appConstant.getFileName(coverLetterUrl);
      if (fileExt[0] && fName) {
        downloadFile(REMOTE_IMAGE_PATH, fName, fileExt[0]);
      }
    }
  }

  const downloadFile = async (imgPath, fileName, fileExtension) => {
    try {
      await checkPermission(imgPath, fileName, fileExtension);
    } catch (error) {
      appSnakBar.onShowSnakBar(error.message, 'LONG');
    }
  };

  const handleConfirmProceed = () => {
    if (printClicked) {
      navigation.navigate("LoanDisbursementList", {
        booking_id: bookingId
      });
    } else {
      appSnakBar.onShowSnakBar(
        "Please Print/Save the Cover Letter in order to enable the Confirm to Proceed button",
        'LONG',
      );
    }
  }
  
  const onHandleRetryBtn = () => {
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />
      <ScrollView>
        <View style={styles.container}>
          <AppText style={styles.pageTitle}>Loan Disbursement</AppText>

          <View style={styles.coverLetterCon}>
            <AppText style={styles.coverText}>Covering Letter</AppText>
            <Image
              source={require('./../assets/images/hdfc-card.png')}
              style={styles.img}
            />
          </View>

          <AppText style={styles.updateText}>Update your details(2/2)</AppText>
          <ProgressBar
            style={{ marginBottom: 25 }}
            progress={1}
            color={colors.gray}
          />

          <AppTextBold style={{ fontSize: appFonts.xlargeFontSize }}>Bank Details</AppTextBold>

          <View style={styles.inputContainer}>
            <AppText>Payable at Location</AppText>
            <TextInput style={styles.input}
              value={
                inputs['payable_at_location'] && inputs['payable_at_location'].value ? inputs['payable_at_location'].value : ''
              }
              onChangeText={(value) => {
                onInputChange('payable_at_location', value);
              }} />
          </View>
          <View style={styles.inputContainer}>
            <AppText>Payable on Date</AppText>
            <TouchableOpacity
              style={styles.dateTimePickerStyle}
              onPress={() => {
                openDatepicker();
              }}
            >
              <AppText style={styles.input}>{payableDate}</AppText>
            </TouchableOpacity>
            {showDatePicker && (
              <DateTimePicker
                testID="datePicker1"
                value={
                  inputs['payable_date'] && inputs['payable_date'].value ? inputs['payable_date'].value : ''
                }
                mode={mode}
                // is24Hour={true}
                display="default"
                onChange={async (e, date) => {
                  await onPayableDateChange(e, date)
                }}
              />
            )}
          </View>
          <View style={styles.inputContainer}>
            <AppText>Disbursement Date</AppText>
            <TouchableOpacity
              style={styles.dateTimePickerStyle}
              onPress={() => {
                openDisburseDatePicker();
              }}
            >
              <AppText style={styles.input}>{disbursementDate}</AppText>
            </TouchableOpacity>
            {showDisburseDatePicker && (
              <DateTimePicker
                testID="datePicker2"
                value={
                  inputs['disbursement_date'] && inputs['disbursement_date'].value ? inputs['disbursement_date'].value : new Date()
                }
                mode={mode}
                // is24Hour={true}
                display="default"
                onChange={async (e, date) => {
                  await onDisburseDateChange(e, date)
                }}
              />
            )}
          </View>
          <View style={styles.inputContainer}>
            <AppText>Time</AppText>
            <TouchableOpacity
              style={styles.dateTimePickerStyle}
              onPress={() => {
                showTimepicker()
              }}
            >
              <AppText style={styles.input}>{disburseTime}</AppText>
            </TouchableOpacity>
            {showTimePicker && (
              <DateTimePicker
                testID="timePicker"
                value={inputs['disbursement_time'] && inputs['disbursement_time'].value ? inputs['disbursement_time'].value : new Date()}
                mode={mode}
                // is24Hour={true}
                display="default"
                onChange={async (e, date) => {
                  await onTimeChange(e, date)
                }}
              />
            )}
          </View>
          <View style={styles.inputContainer}>
            <AppText>Present stage of Construction</AppText>
            <TextInput style={styles.input}
              value={
                inputs['present_stage_construction'] && inputs['present_stage_construction'].value ? inputs['present_stage_construction'].value : ''
              }
              onChangeText={(value) => {
                onInputChange('present_stage_construction', value);
              }} />
          </View>

          <AppNotePink
            showNote={true}
            label="Please Print/Save the Cover Letter in order to enable the Confirm to Proceed button"
          />

          <AppButton color="primary" textColor="secondary" title="PRINT/SAVE"
            onPress={() => { handlePrintSave() }} />
          <AppButton title="CONFIRM TO PROCEED"
            onPress={() => { handleConfirmProceed() }} />
        </View>
      </ScrollView>
      <AppFooter isPostSales={true} />
      <AppOverlayLoader isLoading={isLoading} />
      {showProgress && progress && progress > 0 ? (
        <AppProgressBar progress={progress} />
      ) : null}
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '6%',
    justifyContent: 'center',
    marginBottom: 20,
  },
  pageTitle: {
    fontSize: appFonts.xxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    textTransform: 'uppercase',
    paddingTop: '5%',
    paddingBottom: '8%',
  },
  img: {
    width: 150,
    height: 30,
  },
  coverLetterCon: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  coverText: {
    fontSize:appFonts.largeBold,
    fontFamily: appFonts.SourceSansProBold,
  },
  updateText: {
    fontSize:appFonts.largeBold,
    marginTop: 25,
    paddingBottom: 5,
    alignSelf: 'center',
  },
  inputContainer: {
    marginTop: '5%',
  },
  input: {
    marginBottom: '3%',
    fontSize:appFonts.largeBold,
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGray,
  },
  dateTimePickerStyle: {
    // width: '100%',
    // fontSize: 18,
    // color: '#26262a',
    // marginBottom: 40,
    // borderBottomColor: '#d8d8d8',
    // borderBottomWidth: 1,
    // marginTop: 20,
    // fontFamily: appFonts.SourceSansProSemiBold,
  },
  dateTimePickerText: {
    width: '100%',
    fontSize: appFonts.largeBold,
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGray,
    marginTop: 20,
    fontFamily: appFonts.SourceSansProSemiBold
  },
});

export default LoanDisbursementForm2;
