import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity
} from 'react-native';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';

import AppText from '../components/ui/ AppText';
import AppButton from '../components/ui/AppButton';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import appFonts from '../config/appFonts';
import colors from '../config/colors'
import appSnakBar from '../utility/appSnakBar';
import RequestCreated from '../screens/modals/RequestCreated'
import AppTextBoldSmall from '../components/ui/AppTextBoldSmall';

import useFileDownload from '../hooks/useFileDownload';
import AppProgressBar from '../components/ui/AppProgressBar';
import appConstant from '../utility/appConstant';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import Orientation from 'react-native-orientation';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const ServiceRequestDetails = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const {
    progress,
    showProgress,
    error,
    isSuccess,
    checkPermission,
  } = useFileDownload();

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  const handleFileDownload = async (imgPath, isPdf) => {
    setIsLoading(true)
    let fileExt = isPdf ? 'pdf' : appConstant.getExtention(imgPath);
    let fName = appConstant.getFileName(imgPath);
    fName = fName + (isPdf ? '.pdf' : '')
    if (fileExt[0] && fName) {
      downloadFile(imgPath, fName, fileExt[0]);
    } else {
      setIsLoading(false)
    }
  };

  const downloadFile = async (imgPath, fileName, fileExtension) => {
    try {
      await checkPermission(imgPath, fileName, fileExtension);
      setIsLoading(false)
    } catch (error) {
      appSnakBar.onShowSnakBar(error.message, 'LONG');
      setIsLoading(false)
    }
  };

  let date = new Date(props.route.params.requetdetail.created_at * 1000);



  const onHandleRetryBtn = () => {
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />
      <ScrollView>
        <View style={styles.container}>
          <AppTextBold style={styles.pageTitle}> Write to Us </AppTextBold>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 1, alignSelf: "flex-start" }}>
              <AppTextBold> #{props.route.params.requetdetail.title} </AppTextBold>
            </View>
            <View />
            <View style={styles.open}>
              <AppTextBold>{props.route.params.requetdetail.status_readable}</AppTextBold>
              <AppText> {date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear()} </AppText>
            </View>
          </View>
          <View>
            <AppTextBold> {props.route.params.requetdetail.subject}</AppTextBold>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <View style={styles.textAreaContainer} >
                <AppTextBold>Description</AppTextBold>
                <AppText>{props.route.params.requetdetail.description}</AppText>


              </View>
            </View>
            {props.route.params.requetdetail.file_attached?.length > 0 && <View style={styles.textAreaContainer} >
              <AppTextBold>File Attached</AppTextBold>

              {props.route.params.requetdetail.file_attached.map((item, index) => {
                return <View key={index}>
                  <View style={styles.documentDoc}>
                    <View style={styles.textContainer}>
                      <AppTextBoldSmall > {item.name}</AppTextBoldSmall>
                    </View>
                    <TouchableOpacity onPress={() => handleFileDownload(item.path, false)} >
                      <Image
                        style={styles.iconDownload}
                        source={require('../assets/images/download-b.png')}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              })}

            </View>}
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <View style={styles.textAreaContainer} >
                <AppTextBold>RM Comments</AppTextBold>
                <AppText>{props.route.params.requetdetail.remarks != "" ? props.route.params.requetdetail.remarks : 'No remarks'}</AppText>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
      <AppFooter activePage={0} isPostSales={true} />
      {showProgress && progress && progress > 0 ? (
        <AppProgressBar progress={progress} />
      ) : null}
      <AppOverlayLoader isLoading={isLoading} />
    </Screen>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 18,
    // marginLeft: "3%",
    // marginRight: "3%"
  },
  pageTitle: {
    fontSize:appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.jaguar,
    marginBottom: "8%"
  },
  textAreaContainer: {
    width: '100%',
    marginTop: 5,
    marginBottom: 15,
    marginLeft: "2%",
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.7,
    shadowRadius: 1,
    elevation: 1,
    padding: 10,
    backgroundColor: colors.primary,
  },
  textArea: {
    height: windowHeight * 0.18,
    justifyContent: "flex-start",
    textAlignVertical: "top",
    padding: 10
  },
  open: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "flex-end"
  },
  documentDoc: {
    flex: 1,
    flexDirection: "row",
    marginTop: "2%",
    marginBottom: "2%",
  },
  iconImg: {
    height: 30,
    width: 25,
  },
  iconDownload: {
    height: 18,
    width: 18,
    top: 5,
  },
  textContainer: {
    width: "82%",
    marginBottom:0,
    marginTop:0,
    marginHorizontal: 10
  },
  listcontainer: {
    marginTop: 5,
    marginBottom: 5
  },
})

export default ServiceRequestDetails;