import React, { useState, useEffect } from 'react';
import {
	View,
	StyleSheet,
	ScrollView,
	TouchableOpacity,
	Image,
	Dimensions,
	Modal,
	Text,
	FlatList
} from 'react-native';
import { useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import Carousel from 'react-native-snap-carousel';
import Icon from 'react-native-vector-icons/Ionicons';
//components
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import GalleryModalScreen from './modals/GalleryModalScreen';
//config
import appFonts from '../config/appFonts';
import colors from '../config/colors';
//api 
import TESTIMONIAL_API from '../api/apiTestimonial';
import Orientation from 'react-native-orientation';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
function TestimonialsScreen(props) {
	const [stackNum, setStackNum] = useState(0);
	const [testiMonialList, setTestimonialList] = useState([]);
	const { loginInfo } = useSelector((state) => state);
	const [ modalVisible, setModalVisible ] = useState(false);
	const [ galleryData, setGalleryData ] = useState({});
	const [ mediaIndex, setMediaIndex] = useState(0);
	const [ carousalIndex, setCarousalIndex ] = useState(0);
	const [restoreCarousal,setRestoreCaraousal] = useState(true)
	const userId = loginInfo && loginInfo.loginResponse && loginInfo.loginResponse.data && loginInfo.loginResponse.data.userid ?loginInfo.loginResponse.data.userid:'';
	
	const scrollRef = React.useRef();
	const navigation = useNavigation();
	let previewCarousel;

	useEffect(() => {
		Orientation.lockToPortrait();
	}, []);
	useEffect(() => {
		setRestoreCaraousal(true)
	}, [stackNum])
	useEffect(() => {
		TESTIMONIAL_API.apiTestimonialList({ user_id: userId }).then((response) => {
			if (response && response.data && response.data.data) {
				setTestimonialList(response.data.data);

			}
		}).catch((error) => {
			console.log("error", error)
		})
	}, [])

	const createTwoDimentionalArray = (data) => {
		const arr = [];
		data.forEach((elem, index) => {
			if (index > 0) {
				if ((index + 1) % 2 === 0) {
					arr.push([data[index - 1], data[index]])
				}
				else {
					console.log('jgjgh', index, data.length)
					if (index == (data.length - 1)) {
						arr.push([elem])
					}
				}
			}
			else {
				if (data.length < 2) {
					arr.push([elem])
				}
			}
		})
		return arr;
	}
	/**
	 * 
	 * @description call only if testimonial list changes
	 */
	useEffect(() => {
		getGalleryData();


	}, [testiMonialList])

	/**
	 * 
	 * @param {string} url 
	 */
	const isVedio = (url) => {
		const regex = /\.(gif|jpe?g|tiff?|png|webp|bmp)$/i;
		return regex.test(url)?false:true
		//return url.includes('.mp4') ? true : false;
	}
	/**
	 * 
	 * @description Set the gallery detail from testimonial list
	 */
	const getGalleryData = () => {
		//field_media_type,field_gallery_image_url,field_gallery_thumb_image_url
		const regex = /\.(gif|jpe?g|tiff?|png|webp|bmp)$/i;
		const galleryList = {};
		testiMonialList.forEach((testimonial, index) => {
			const fileArray = [];
			testimonial.attached_files.forEach((data) => {
				const url = data.url;
				const field_media_type = regex.test(url) ? 'image' : 'video';
				fileArray.push({
					field_media_type,
					field_gallery_image_url: url,
					field_gallery_thumb_image_url: url
				})
				galleryList[index] = fileArray;
			})
		})
		setGalleryData({ ...galleryList })
	}

	/**
	 * 
	 * @description Change gallery modal visibility 
	 */
	const handleGalleryModal = (item) => {
		const mediaIndex = testiMonialList[stackNum].attached_files.indexOf(item);
		setMediaIndex(mediaIndex);
		setModalVisible(!modalVisible);
	}
	const caraousalArr = createTwoDimentionalArray(testiMonialList[stackNum] ? testiMonialList[stackNum].attached_files : []);
	const onHandleRetryBtn = () => {
		TESTIMONIAL_API.apiTestimonialList({ user_id: userId }).then((response) => {
			if (response && response.data && response.data.data) {
				setTestimonialList(response.data.data);

			}
		}).catch((error) => {
			console.log("error", error)
		})
	};

	return (
		<Screen onRetry={onHandleRetryBtn}>
			<AppHeader />
			<ScrollView nestedScrollEnabled={true}>
				<View style={styles.container}>
					<View style={{ flexDirection: 'row', flex: 1 }}>
						<View style={{ flex: 2 }}>
							<AppTextBold style={styles.pageTitle}> Testimonials </AppTextBold>
						</View>
						<View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', flex: 1, marginBottom: 11 }}>
							<TouchableOpacity onPress={() => navigation.navigate('AddTestimonials')}>
								<Image
									source={require('./../assets/images/plus-icon.png')}
									style={{ height: 25, width: 25 }}
								/>
							</TouchableOpacity>
						</View>
					</View>

					<View style={styles.infoContainer}>

						<View style={styles.imgContainer}>
							{
								testiMonialList[stackNum] && testiMonialList[stackNum].user_image ? <Image
									source={{ uri: testiMonialList[stackNum] ? testiMonialList[stackNum].user_image : '' }}
									style={styles.profile}
								/> : <Image
										source={require('./../assets/images/no-user-img.png')}
										style={styles.profile}
									/>
							}
							<AppTextBold>{testiMonialList[stackNum] ? testiMonialList[stackNum].user_name : ''}</AppTextBold>
							<AppText>{testiMonialList[stackNum] ? testiMonialList[stackNum].project_name : ''}, {testiMonialList[stackNum] ? testiMonialList[stackNum].location : ''}</AppText>
						</View>

						<View >
							<View style={{ ...styles.card }}>
								<ScrollView nestedScrollEnabled={true} >
									<AppText>{testiMonialList[stackNum] ? testiMonialList[stackNum].desc : ''}</AppText>
								</ScrollView>

							</View>
						</View>

						<View>

							{
								testiMonialList[stackNum] && testiMonialList[stackNum].attached_files && testiMonialList[stackNum].attached_files.length ?
									<View style={{
										...styles.card, flexDirection: "row",
										alignItems: "center",
										paddingLeft: 5
									}}>
										{restoreCarousal && <>
											<TouchableOpacity onPress={() => {
												previewCarousel.snapToPrev(true);
											}}>
												<Icon name={'chevron-back'} size={20} color={carousalIndex != 0 ? 'black' : 'grey'} ></Icon>
											</TouchableOpacity>
											{testiMonialList[stackNum] &&
												<Carousel
													layout={'default'}
													ref={(ref) => (previewCarousel = ref)}
													data={caraousalArr}
													sliderWidth={windowWidth}
													sliderHeight={windowHeight / 2}
													itemWidth={windowWidth}
													itemHeight={windowHeight / 2}
													removeClippedSubviews={false}
													onSnapToItem={(index) => {
														setCarousalIndex(index)
													}}
													renderItem={({ item, index }) => {
														return (
															<>
																<View style={{ flexDirection: 'row' }}>
																	{
																		item.map((testimonial, index1) => {
																			return (
																				<View key={index1}>
																					<TouchableOpacity onPress={() => handleGalleryModal(testimonial)}>
																						{!isVedio(testimonial.url) ? <Image
																							resizeMode="cover"
																							source={{ uri: testimonial.url }}
																							style={{ ...styles.img, borderRadius: 5 }}

																						/> : <View style={{ ...styles.img, backgroundColor: colors.LynxWhite, borderRadius: 5, alignItems: 'center' }}>
																								<Image
																									source={require('./../assets/images/video-icon.png')}
																									style={styles.img}
																								/>
																							</View>}
																					</TouchableOpacity>
																				</View>
																			)
																		})
																	}
																</View>
															</>
														);
													}}
												/>}
											<TouchableOpacity onPress={() => {
												previewCarousel.snapToNext(true);
											}}

											>
												<Icon name={'chevron-forward'} size={20} color={carousalIndex + 1 != caraousalArr.length ? 'black' : 'grey'}></Icon>
											</TouchableOpacity>
										</>}
									</View>
									:
									<View style={{ height: windowHeight * 0.2 }}></View>

							}
						</View>


						<View style={styles.stacknumber}>
							<View style={{ marginLeft: -22 }}>
								<TouchableOpacity onPress={() => {
									if (stackNum != 0) {
										setStackNum(stackNum - 1)
										setRestoreCaraousal(false)
									}

								}}>
									<View >
										<Icon name={'chevron-back'} size={30} color={stackNum != 0 ? 'black' : 'grey'} style={{ marginTop: 10 }}></Icon>
									</View>
								</TouchableOpacity>
							</View>
							<View >
								<AppText >{stackNum + 1}/ {testiMonialList.length} </AppText>
							</View>
							<View style={{ marginRight: -22 }}>
								<TouchableOpacity onPress={() => {
									if (stackNum + 1 != testiMonialList.length) {



										//previewCarousel.snapToItem(0,true);
										setStackNum(stackNum + 1)
										setRestoreCaraousal(false)


									}
								}}>
									<View >
										<Icon name={'chevron-forward'} size={30} color={stackNum + 1 != testiMonialList.length ? 'black' : 'grey'} style={{ marginTop: 10 }}></Icon>
									</View>
								</TouchableOpacity>
							</View>
						</View>
					</View>


				</View>
			</ScrollView>
			<Modal
				animationType="fade"
				transparent={true}
				visible={modalVisible}
				supportedOrientations={['portrait', 'landscape']}
				onRequestClose={() => {
					Orientation.lockToPortrait();
					setModalVisible(!modalVisible);
				}}>

				<GalleryModalScreen
					onPress={() => {
						Orientation.lockToPortrait();
						setModalVisible(!modalVisible);
					}}
					imgIndex={mediaIndex}
					galleryData={galleryData[stackNum]}
				/>
			</Modal>
			<AppFooter activePage={0} isPostSales={true} />
		</Screen>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: 20,
		paddingVertical: 10,
	},
	pageTitle: {
		fontSize: appFonts.xxxlargeFontSize,
		fontFamily: appFonts.SourceSansProBold,
		color: colors.jaguar,
		textTransform: 'uppercase',
	},
	profile: {
		height: 80,
		width: 80,
		borderRadius: 40
	},
	infoContainer: {
		alignItems: "center",
		marginTop: "10%"
	},
	imgContainer: {
		justifyContent: "center",
		alignItems: "center"
	},
	addedImg: {
		justifyContent: "center",
		margin: 15,
		flexDirection: "row",
	},
	img: {
		height: 90,
		width: windowWidth * 0.32,
		marginHorizontal: 10
	},
	icon: {
		height: 17,
		width: 13
	},
	stacknumber: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		width: "90%",
		marginTop: "10%",
		marginBottom: "10%",
	},
	card: {
		padding: 10,
		height: windowHeight * 0.2,
		width: windowWidth * 0.9,
		marginTop: "10%",
		borderWidth: 1,
		borderRadius: 1,
		borderColor: colors.lightGray,
		borderBottomWidth: 1,
		shadowColor: colors.lightGray,
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.7,
		shadowRadius: 3,
		elevation: 3,
		backgroundColor: colors.primary,
	},

})

export default TestimonialsScreen;