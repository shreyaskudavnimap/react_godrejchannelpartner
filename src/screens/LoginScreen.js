import React, {useState, useEffect, useCallback, useRef} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  TextInput,
  PermissionsAndroid,
  ActivityIndicator,
  Platform,
  ScrollView,
  Modal,
  StyleSheet,
  FlatList,
  Dimensions,
  SafeAreaView,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  ToastAndroid,
  LogBox
} from 'react-native';

import AppOverlayLoader from '../components/ui/AppOverlayLoader';

import {EventRegister} from 'react-native-event-listeners';
import GetLocation from 'react-native-get-location';
import appUserAuthorization from '../utility/appUserAuthorization';

import AppButton from '../components/ui/AppButton';
import globlaStyles from '../styles/GlobleStyle';
import * as normStyle from '../styles/StyleSize';
import apiProjectDetails from './../api/apiProjectDetails';
import Screen from '../components/Screen';
import apiLogin from './../api/apiLogin';
import useDevice from '../hooks/useDevice';
import useLocation from './../hooks/useLocation';
import {RadioButton} from 'react-native-paper';
import AppText from '../components/ui/ AppText';
import * as userLookUpAction from './../store/actions/lookupUserByPanAction';
import * as userLoginAction from './../store/actions/userLoginAction';
import {useDispatch, useSelector} from 'react-redux';
import ShowLoader from '../components/ui/ShowLoader';
import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CountryModal from '../components/CountryModal';
import PasswordShow from '../components/showPassword';
import {color} from 'react-native-reanimated';
import colors from '../config/colors';
import appSnakBar from '../utility/appSnakBar';
import {setLoginParam} from './../store/actions/userLoginAction';
import {CommonActions, StackActions} from '@react-navigation/native';
import {GoogleAnalyticsTracker} from 'react-native-google-analytics-bridge';
import LoginTermConditionModalScreen from './modals/LoginTermConditionModalScreen';
import * as preloginTermsAction from './../store/actions/preloginTermsAction';
import CountrySelectModalScreen from './modals/CountrySelectModalScreen';
import Orientation from 'react-native-orientation';
import appConstant from '../utility/appConstant';
import appFonts from '../config/appFonts';
import { result } from 'validate.js';

LogBox.ignoreLogs(['Animated:']);

var {height, width} = Dimensions.get('window');

let DATA = [
  {
    id: '1',
    number: '9999999999',
  },
  {
    id: '2',
    number: '9876543210',
  },
  {
    id: '3',
    number: '987654410',
  },
];

const Login = (props) => {
  const [existingCustomer, setexistingCustomer] = useState(true);
  const [isNewUser, setNewUser] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [panInputvalue, pansetInputValue] = useState('');
  const [passwordValue, setPasswordValue] = useState('');
  const [mobileInputvalue, setMobileInputvalue] = useState('');
  const [newUserPswdValue, setNewUserPswdValue] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isApiLoading, setApiIsLoading] = useState(false);
  const [error, setError] = useState();
  const [arrMobileNo, setArrMobileNo] = useState([
    {id: 1, number: '1234567890'},
    {id: 2, number: '1234567899'},
  ]);
  const [checkedMobile, setCheckedMobile] = useState('first');
  const [deviceType, setDeviceType] = useState('');
  const [loginNewUser, setLoginNewuser] = useState('');
  const [SoftInputOnFocus, setSoftInputOnFocus] = useState(false);
  const [SoftInput4ExistCust, setSoftInput4ExistCust] = useState(false);
  const [forgetPasswordSenario, SetForgetPasswordSenario] = useState(false);
  const [showCountryModal, setShowCountryModal] = useState(false);
  const [passwordError, SetPasswordError] = useState('');
  const ProceedWithOtp = useRef(false);
  const tempUserId = useRef('');
  const activationIdForPAN = useRef('');

  //const count = useSelector(state => state.counter.count);
  const dispatch = useDispatch();

  var [userDetail, setUserDetail] = useState('');

  const [countryName, setCountryName] = useState('IND');
  const [countryId, setCountryId] = useState('+91');
  const [countryFlag, setFlag] = useState(
    'https://www.countryflags.io/in/flat/64.png',
  );
  const device = useDevice();
  const location = useLocation();

  const [newMobile, SetNewMobile] = useState('');
  const [newEmailID, SetNewEmailID] = useState('');
  const [newFirstName, SetNewFirstName] = useState('');
  const [newLastName, SetNewLastName] = useState('');
  const [newPasswod, SetNewPassword] = useState('');
  const [newConfirmPswd, SetNewConfirmPswd] = useState('');
  
  const passwordRegex =
    "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d!@#$%^*?%/'._\\-,\\\\`~]{6,14}$";
  var tracker = new GoogleAnalyticsTracker('UA-171278332-1');
  useEffect(() => {
    if (
      Platform.OS === 'ios' ? setDeviceType('IOS') : setDeviceType('ANDROID')
    ) {
    }
  }, [dispatch, checkedMobile, lookupData, loginvalue]);

  const lookupData = useSelector((state) => state.lookupInfo.lookupUserPanInfo);

  const loginvalue = useSelector((state) => state.loginInfo.loginResponse);

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );

  const deviceOS = useSelector((state) => state.deviceInfo.deviceOS);

  const startLoginHere = async (cust_type) => {
    //console.log('token = '+ JSON.stringify(AsyncStorage.getItem('sessionId')));
    // const value = await AsyncStorage.getItem('sessionId');
    // console.log('token = '+value);
    console.log('>>>>>>>>>>>>>');
    let version = DeviceInfo.getVersion();
    if (cust_type == 'existcustomer') {
      var pan = panInputvalue;
      if (passwordValue == '' || pan == '') {
        appSnakBar.onShowSnakBar('Enter password and PAN for Login', 'LONG');
        return null;
      }
      if (
        pan != '' &&
        deviceUniqueId != '' &&
        deviceType != '' &&
        passwordValue != '' &&
        version != ''
      ) {
        callApi(version, pan, passwordValue, deviceUniqueId, deviceType);
      }
    }
    if (cust_type == 'newuser') {
      if (countryId == '+91') {
        if (mobileInputvalue.length != 10) {
          appSnakBar.onShowSnakBar('Mobile number must be 10 digits', 'LONG');
          return null;
        }
      }
      if (mobileInputvalue.trim() != '' && newUserPswdValue.trim() != '') {
        //alert(mobileInputvalue+" II "+newUserPswdValue);
        callNewUserLoginApi(
          version,
          countryId + mobileInputvalue,
          newUserPswdValue,
          deviceUniqueId,
          deviceType,
          'password',
        );
      } else {
        appSnakBar.onShowSnakBar('Enter Mobile Number and Password', 'LONG');
      }
    }
  };

  const callApi = useCallback(
    async (version, userValue, passwordValue, deviceUniqueId, deviceType) => {
      setIsLoading(true);
      try {
        if(userValue != "shreyaskudav@nimapinfotech.com" && userValue != "Rajesh.pandey@squareyards.com") {
          await dispatch(
          userLoginAction.getUserLoginInfo(
            version,
            userValue,
            passwordValue,
            deviceUniqueId,
            deviceType,
            'user_login',
          ),
        );
        }
        else {
          const request = {
            username: userValue,
            password: passwordValue
          };

          login_result = await apiLogin.apiLoginUser(request);

          if(login_result != null) {
            console.log("User LoggedIn : ", login_result);
            await AsyncStorage.setItem('isLogin', 'true');
            await AsyncStorage.setItem('userName', login_result.Name);
            await AsyncStorage.setItem('userDetail', JSON.stringify(login_result));
            _skip(); 
          }
          else
          {
            console.log("Login failed!")
          }
        }

      } catch (err) {
        console.log(err.message);
      }
      //setTimeout(() => {setIsLoading(false);}, 1000)
      setIsLoading(false);
      checkLoginStatus();
    },
    [dispatch, setIsLoading, setError, loginvalue],
  );

  const callNewUserLoginApi = useCallback(
    async (appVersion, username, password, deviceId, deviceType, loginType) => {
      setIsLoading(true);
      try {
        await dispatch(
          userLoginAction.getUserLoginInfo(
            appVersion,
            username,
            password,
            deviceId,
            deviceType,
            loginType,
          ),
        );
      } catch (err) {
        setError(err.message);
      }
      //setTimeout(() => {
      setIsLoading(false);
      checkLoginStatus();
    },
    [dispatch, setIsLoading, setError],
  );

  // const checkLoginStatus = async() => {
  //     const isLogin = await AsyncStorage.getItem('isLogin');
  //     //alert(value);
  //     if(isLogin == 'true'){
  //         props.navigation.navigate('AppStackRoute');
  //     }else{
  //         setTimeout(() => {appSnakBar.onShowSnakBar(`Invalid Password`, 'LONG');}, 500)
  //     }
  // };
  const checkLoginStatus = async () => {
    const isLogin = await AsyncStorage.getItem('isLogin');
    //alert(value);
    if (isLogin == 'true') {
      const params = props.route.params;

      let userDATA = await AsyncStorage.getItem('data');
      let tempArr = JSON.parse(userDATA);
      let userid = tempArr.userid;

      let fcmToken = await AsyncStorage.getItem('fcmToken');
      const request = {
        device_token: fcmToken,
        user_id: loginvalue.data ? loginvalue.data.userid : userid,
        device_type: deviceOS,
        device_id: deviceUniqueId,
      };

      await dispatch(userLoginAction.updateDeviceToken(request));
      console.log('Notification Payload++++++++', request);

      if (params && params.backRoute && params.action && params.screen) {
        EventRegister.emit('wishlistUpdateEvent', 'update');

        if (params.screen === 'GodrejHome') {
          EventRegister.emit('GodrejHomeUpdateEvent', 'updateList');
        } else if (params.screen === 'ProjectDetails') {
          if (params.action === 'wishlist_set') {
            EventRegister.emit('ProjectDetailsUpdateEvent', 'updateList');
          } else if (params.action === 'booking') {
            EventRegister.emit('ProjectDetailsUpdateEventOther', 'updateList');

            EventRegister.emit('ProjectDetailsUpdateEventBooking', params.item);
          } else if (params.action === 'booking_eoi') {
            EventRegister.emit('ProjectDetailsUpdateEventOther', 'updateList');

            EventRegister.emit(
              'ProjectDetailsUpdateEventBookingEOI',
              params.item,
            );
          }
        } else if (params.screen === 'FloorLayout') {
          // EventRegister.emit('GodrejHomeUpdateEventOther', 'updateList');
          EventRegister.emit('ProjectDetailsUpdateEventOther', 'updateList');

          if (params.action === 'wishlist_set') {
            EventRegister.emit('FloorLayoutUpdateEvent', params.item);
          } else if (params.action === 'booking') {
            EventRegister.emit('FloorLayoutUpdateEventBooking', params.item);
          }
        } else if (params.screen === 'EOIFloorLayout') {
          // EventRegister.emit('GodrejHomeUpdateEventOther', 'updateList');
          EventRegister.emit('ProjectDetailsUpdateEventOther', 'updateList');

          if (params.action === 'booking') {
            EventRegister.emit('EOIFloorLayoutUpdateEventBooking', params.item);
          } else if (params.action === 'booking_foyr') {
            EventRegister.emit(
              'EOIFloorLayoutUpdateEventBookingFOYR',
              params.item,
            );
          }
        } else if (params.screen === 'SearchFilter') {
          EventRegister.emit('SearchFilterUpdateEvent', 'updateList');
        }

        props.navigation.dispatch(StackActions.pop());

        // dispatch(setLoginParam('wishlist_set'));

        // dispatch(setLoginParam(params.action));
        // props.navigation.navigate(params.backRoute);

        // if (params.item) {
        //     props.navigation.navigate(params.backRoute, { screen: params.screen, params: { item: params.item } });
        // } else {
        //     props.navigation.navigate(params.backRoute, { screen: params.screen });
        // }
      } else {
        EventRegister.emit('wishlistUpdateEvent', 'update');

        props.navigation.dispatch(
          CommonActions.reset({
            index: 1,
            routes: [{name: 'AppStackRoute'}],
          }),
        );
        // props.navigation.navigate('AppStackRoute');
      }
    } 
    // else {
    //   setTimeout(() => {
    //     appSnakBar.onShowSnakBar('Invalid Password', 'LONG');
    //   }, 500);
    // }
  };

  const getLookupRecord = async (pan) => {
    const request = {
      pan: pan,
    };
    setIsLoading(true);
    const result = await apiLogin.apiLandingLookupUserByPan(request);
    setIsLoading(false);
    var status = result.data.status;
    //status = 201; // for testing
    console.log(result.data);
    if (status == '200') {
      setSoftInput4ExistCust(true);
      //setModalVisible(true); // for testing
      //setTimeout(() => {appSnakBar.onShowSnakBar(`Enter password for Login`, 'LONG');}, 500)
    }
    if (status == '201') {
      getMobileNoList(result.data.data);
    }
    if (status != '200' && status != '201') {
      setSoftInput4ExistCust(true);
      var msg = result.data.msg;
      setTimeout(() => {
        appSnakBar.onShowSnakBar(msg, 'LONG');
      }, 500);
    }
  };

  const getMobileverified = async (e) => {
    const request = {
      mobile: e,
    };
    console.log(request);
    setIsLoading(true);
    const result = await apiLogin.apiForlookupByMobile(request);
    setIsLoading(false);
    var status = result.data.status;
    console.log(result.data);
    Keyboard.dismiss();
    if (isNewUser) {
      setNewUser(false);
    }
    SetPasswordError('');
    if (status == 200) {
      setSoftInputOnFocus(true);
      setLoginNewuser('mobilelookupSet');
      if (ProceedWithOtp.current) {
        var user_id = result.data.user_id;
        ProceedWithOtp.current = false;
        props.navigation.navigate('Otp', {
          mobileNo: countryId + mobileInputvalue,
          userId: user_id,
          actionType: 'proceed_with_otp',
        });
      } else {
        setTimeout(() => {
          appSnakBar.onShowSnakBar('Enter Password', 'LONG');
        }, 500);
      }
    }
    if (status == 204) {
      ProceedWithOtp.current = false;
      setNewUser(true);
      SetForgetPasswordSenario(true);
      setTimeout(() => {
        appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
      }, 500);
    }
    if (status == 205) {
      ProceedWithOtp.current = false;
      setTimeout(() => {
        appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
      }, 300);
    }
    if (status == 201) {
      tempUserId.current = result.data.user_id;
      ProceedWithOtp.current = false;
      setNewUser(true);
      SetForgetPasswordSenario(false);
    }
  };

  const getMobileNoList = (mobileData) => {
    console.log('abcd = ' + JSON.stringify(mobileData));
    if (mobileData.length > 0 && mobileData != '') {
      let tempArr = [];
      DATA = [];
      for (let i = 0; i < mobileData.length; i++) {
        var temp = mobileData[i].split('_');
        tempArr.push({id: temp[1], number: temp[0].replace('+91', '')});
      }
      // setArrMobileNo(tempArr); not setting array using useState

      /*      REMOVING DUPLICATES      */
      for (let i = 0; i < tempArr.length; i++) {
        let isFirst = true;
        var a = tempArr[i].number;
        for (let j = 0; j < tempArr.length; j++) {
          var b = tempArr[j].number;
          if (a == b) {
            if (!isFirst) {
              console.log('j = ' + j);
              tempArr.splice(j, 1);
            }
            isFirst = false;
          }
        }
      }
      /*   END REMOVING DUPLICATES   */
      DATA = tempArr;
      setModalVisible(true);
    }
  };

  const handleChange = (e) => {
    pansetInputValue(e);
    //console.log(e);
    if (e.length == 10) {
      Keyboard.dismiss();
      var patt = new RegExp('^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
      var res = patt.test(e);
      if (res) {
        setCheckedMobile('first'); //unselect the previous selected number in pop-up
        getLookupRecord(e);
      } else {
        appSnakBar.onShowSnakBar('Invalid Email', 'LONG');
      }
    }
  };

  const handleMobileChange = (mobile_no) => {
    let validateMobile = mobileInputvalue.trim();
    if(mobile_no){
      validateMobile = mobile_no.trim();
    }
    if (loginNewUser == '') {
      if (!validateMobile.trim()) {
        appSnakBar.onShowSnakBar('Enter mobile number', 'LONG');
      }
      if (validateMobile.trim()) {
        if (countryId == '+91') {
          if (validateMobile.length < 10 || validateMobile.length > 10) {
            appSnakBar.onShowSnakBar('Mobile number digits must be 10', 'LONG');
          } else {
            getMobileverified(countryId + validateMobile);
          }
        } else {
          getMobileverified(countryId + validateMobile);
        }
      }
    }
    if (loginNewUser == 'mobilelookupSet') {
    }
  };

  const changeMobileNumber = (mobileNo) => {
    setMobileInputvalue(mobileNo);
    setSoftInputOnFocus(false);
    setLoginNewuser('');
    setNewUserPswdValue('');
    if(mobileNo.length == 10){
        handleMobileChange(mobileNo);
    }
    ProceedWithOtp.current = false;
  };

  const _selectMobile = (number, id) => {
    setCheckedMobile(number);
    activationIdForPAN.current = id;
  };

  const _goToOtpScreen = () => {
    setModalVisible(!modalVisible);
    if (checkedMobile == 'first') {
      setTimeout(() => {
        appSnakBar.onShowSnakBar('Select Mobile Number', 'LONG');
      }, 300);
    }
    if (
      checkedMobile != '' &&
      checkedMobile != 'first' &&
      activationIdForPAN.current != ''
    ) {
      props.navigation.navigate('Otp', {
        mobileNo: checkedMobile,
        firstName: activationIdForPAN.current, //firstName used for hold sfid in otp screen
        actionType: 'exist_cust_pan_login',
      });
    }
  };

  const checkStatusForMobileCode = async () => {
    const mobileVerificationStatus = await AsyncStorage.getItem(
      'lookupbymobileastatus',
    );
    //alert(mobileVerificationStatus);
  };

  const newUserOtpLogin = () => {
    //alert(mobileInputvalue);
    // if(mobileInputvalue.trim() != '' && loginNewUser == 'mobilelookupSet'){

    // }
    if (mobileInputvalue.trim() == '') {
      appSnakBar.onShowSnakBar('Enter Mobile Number', 'LONG');
    }
    if (mobileInputvalue.trim() != '' && !ProceedWithOtp.current) {
      if (countryId == '+91') {
        if (mobileInputvalue.length < 10 || mobileInputvalue.length > 10) {
          appSnakBar.onShowSnakBar('Mobile number digits must be 10', 'LONG');
          return null;
        } //else{alert('ok');}
      }
      ProceedWithOtp.current = true;
      getMobileverified(countryId + mobileInputvalue);
    }
  };

  const _setCustomer = (custType) => {
    if (custType == 'existing_cust') {
      setexistingCustomer(true);
      setNewUser(false);
      setMobileInputvalue('');
      setNewUserPswdValue('');
      SetNewEmailID('');
      SetNewFirstName('');
      SetNewLastName('');
      SetNewPassword('');
      SetNewConfirmPswd('');
      SetPasswordError('');
    }
    if (custType == 'new_user') {
      setexistingCustomer(false);
      pansetInputValue('');
      setPasswordValue('');
      SetPasswordError('');
    }
  };

  const validatePassword = (pwd) => {
    var patt = new RegExp(
      "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d!@#$%^*?%/'._\\-,\\\\`~]{6,14}$",
    );
    var res = patt.test(pwd);
    if (pwd.length < 6) {
      appSnakBar.onShowSnakBar(
        'Password should be minimum 6 characters long',
        'LONG',
      );
      return false;
    } else {
      if (pwd.length > 14) {
        appSnakBar.onShowSnakBar(
          'Password should be maximum 6 characters long',
          'LONG',
        );
        return false;
      } else {
        if (res) {
          return true;
        } else {
          appSnakBar.onShowSnakBar('Password should be alpha-numeric', 'LONG');
          return false;
        }
      }
    }
  };

  const validateEmail = (email) => {
    var patt = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
    var res = patt.test(email);
    console.log(res);
    if (res) {
      return res;
    } else {
      appSnakBar.onShowSnakBar('Enter valid Email-Id', 'LONG');
    }
  };

  const _chkDuplicateEmail4newUser = async (email) => {
    const request = {
      email: email,
    };
    console.log(request);
    setIsLoading(true);
    const result = await apiLogin.apiForChkDuplicateEmail(request);
    setIsLoading(false);
    var status = result.data.status;
    console.log(result.data);
    if (status == 200) {
      props.navigation.navigate('Otp', {
        mobileNo: countryId + mobileInputvalue,
        emailID: newEmailID,
        firstName: newFirstName,
        lastName: newLastName,
        password: newPasswod,
        countryId: countryId,
        actionType: 'new_signup',
      });
    } else {
      setTimeout(() => {
        appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
      }, 300);
    }
  };

  const _signUpNewUser = () => {
    tracker.trackEvent('Go To Page', 'Click', {label: 'Sign up', value: 4});
    if (validateEmail(newEmailID.trim())) {
      if (validatePassword(newPasswod.trim())) {
        if (
          mobileInputvalue.trim() != '' &&
          newEmailID.trim() != '' &&
          newFirstName.trim() != '' &&
          newLastName.trim() != '' &&
          newPasswod.trim() != '' &&
          newConfirmPswd.trim() != ''
        ) {
          if (newPasswod.trim() === newConfirmPswd.trim()) {
            _chkDuplicateEmail4newUser(newEmailID);
          } else {
            //appSnakBar.onShowSnakBar(`Password & Confirm Password Mismatch`, 'LONG');
            SetPasswordError('Password & Confirm Password Mismatch');
          }
        } else {
          appSnakBar.onShowSnakBar('All Field Are Mandatory', 'LONG');
        }
      }
    }
  };

  const _forgetPasswordSenarioNewUser = () => {
    if (validatePassword(newPasswod)) {
      if (newPasswod == newConfirmPswd) {
        props.navigation.navigate('Otp', {
          mobileNo: mobileInputvalue,
          userId: tempUserId.current,
          password: newPasswod,
          actionType: 'forget_pwd_senario_newuser',
          countryId: countryId,
        });
      } else {
        SetPasswordError('Password & Confirm Password Mismatch');
      }
    }
  };

  const _skip = () => {
    // props.navigation.navigate('AppStackRoute');

    props.navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{name: 'AppStackRoute'}],
      }),
    );
  };

  const preloginTerms = useSelector((state) => state.preloginTerms);
  // console.log("preloginTerms", preloginTerms);

  const [termsModalVisible, setTermsModalVisible] = useState(false);
  const [termsText, setTermsText] = useState('');

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
    if (!preloginTerms.termsSelected) {
      getPreloginTermsData();
    }
  }, []);

  const getPreloginTermsData = async () => {
    setIsLoading(true);
    try {
      const result = await apiLogin.getPreloginTermsData();
      const resultData = result.data;
      setIsLoading(false);
      let termsTxt =
        typeof resultData[0] !== 'undefined' ? resultData[0].body : '';
      if (termsTxt) {
        setTermsModalVisible(true);
        setTermsText(termsTxt);
      }
    } catch (error) {
      console.log(error);
      setTermsModalVisible(false);
      setIsLoading(false);
    }
  };

  const onHandleTermsSelection = async () => {
    dispatch(
      preloginTermsAction.setTermsData({
        termsSelected: 'Yes',
      }),
    );
    postAuthorization('Pre Login Terms');
    setTermsModalVisible(false);
  };

  const postAuthorization = (moduleName) => {
    const requestData = {
      deviceId: deviceUniqueId,
      devicePlatform: Platform.OS,
      userId: '',
      customerName: '',
      mobileNo: '',
      emailId: '',
      moduleName: moduleName,
    };
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
      .then(({latitude, longitude}) => {
        appUserAuthorization.validateUserAuthorization({
          ...requestData,
          latiTude: latitude,
          longiTude: longitude,
        });
      })
      .catch((error) => {
        appUserAuthorization.validateUserAuthorization(requestData);
      });
  };

  const onSelectCountry = (selectedCountry) => {
    console.log(selectedCountry.callingCodes);
    setCountryName(selectedCountry.alpha3Code);
    setCountryId(selectedCountry.callingCodes);
    setFlag(selectedCountry.flag);
    setShowCountryModal(false);
  };

  if (termsModalVisible && termsText) {
    return (
      <LoginTermConditionModalScreen
        onContinue={() => {
          onHandleTermsSelection();
        }}
        termsData={termsText}
      />
    );
  }

  const getAppVersion = () => {
    if (DeviceInfo.getVersion()) {
      return `${
        appConstant.buildInstance && appConstant.buildInstance.buildType
          ? appConstant.buildInstance.buildType + '-'
          : ''
      }${DeviceInfo.getVersion()}`;
    } else {
      return '';
    }
  };

  return (
    <>
      <ImageBackground
        source={require('../assets/images/loginbg.jpg')}
        resizeMode={'cover'}
        style={styles.imgBg}>
        <KeyboardAvoidingView
          behavior={Platform.OS == 'ios' ? 'padding' : 'height'}>
          <ScrollView
            keyboardShouldPersistTaps='always'>
            <SafeAreaView
              style={{
                width: width,
                height:
                  isNewUser == true ? height + 60 + height / 4 : height + 60,
              }}>
              <TouchableWithoutFeedback
                onPress={() => {
                  Keyboard.dismiss();
                }}>
                <View>
                  <View style={styles.viewContainer}>
                    <View />
                    <View style={styles.loginLogo}>
                      <Image
                        source={require('../assets/images/login-logo.png')}
                        style={styles.logp}
                      />
                    </View>
                    {/* <TouchableOpacity
                      onPress={() => {
                        _skip();
                        tracker.trackEvent('Go To Page', 'Click', {
                          label: 'Guest Login',
                          value: 5,
                        });
                      }}>
                      <Text style={styles.skip}>Skip</Text>
                    </TouchableOpacity> */}
                  </View>

                  <View style={styles.tabContainer}>
                    <View style={styles.tabView}>
                      <View style={{flex: 1.0}}>
                        <TouchableOpacity
                          style={{flex: 1}}
                          onPress={() => {
                            _setCustomer('existing_cust');
                          }}>
                          <View
                            style={
                              existingCustomer
                                ? globlaStyles.loginTabActive
                                : globlaStyles.loginTabInActive
                            }>
                            <Text
                              style={
                                existingCustomer
                                  ? globlaStyles.loginTabTextActive
                                  : globlaStyles.loginTabTextInActive
                              }
                              textBreakStrategy="simple"
                              ellipsizeMode="tail">
                              {" "}Channel Partner Login{" "}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                      {/* <View style={{flex: 0.5}}>
                        <TouchableOpacity
                          style={{flex: 1}}
                          onPress={() => {
                            _setCustomer('new_user');
                          }}>
                          <View
                            style={
                              !existingCustomer
                                ? globlaStyles.loginTabActive
                                : globlaStyles.loginTabInActive
                            }>
                            <Text
                              style={
                                !existingCustomer
                                  ? globlaStyles.loginTabTextActive
                                  : globlaStyles.loginTabTextInActive
                              }
                              textBreakStrategy="simple"
                              ellipsizeMode="tail">
                             {" "} New User Login{" "}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View> */}
                    </View>

                    {existingCustomer ? (
                      <View>
                        <TextInput
                          style={styles.input}
                          placeholder="Username"
                          autoCapitalize="characters"
                          underlineColorAndroid="transparent"
                          placeholderTextColor="white"
                          onTouchStart={() => {
                            //setModalVisible(true);
                          }}
                          onChangeText={(e) => {
                            handleChange(e);
                          }}
                          value={panInputvalue}
                        />
                        <View style={{marginTop: 10}}>
                          <PasswordShow
                            title={'Password'}
                            colorText="white"
                            showSoftInputOnFocus={SoftInput4ExistCust}
                            onChangeText={(e) => {
                              setPasswordValue(e);
                            }}
                            value={passwordValue}
                          />
                        </View>
                        <View style={styles.forgotPw}>
                          <TouchableOpacity
                            onPress={() => {
                              props.navigation.navigate('ForgetPwd', {
                                actionType: 'exist_cust_fotget_pwd',
                              });

                              tracker.trackEvent('Go To Page', 'Click', {
                                label: 'Forgot Password',
                                value: 3,
                              });
                            }}>
                            <Text style={styles.forgotPw1}>
                              Forgot Password ?
                            </Text>
                          </TouchableOpacity>
                        </View>

                        <AppButton
                          title="LOGIN"
                          color="primary"
                          textColor="secondary"
                          onPress={() => {
                            startLoginHere('existcustomer');
                            tracker.trackEvent('Go To Page', 'Submit', {
                              label: 'Login with PAN',
                              value: 6,
                            });
                          }}
                        />
                        
                        <Text style={styles.forgotPw1}>Don't have an account?</Text>
                        
                        <AppButton
                          title="REQUEST FOR EMPANELMENT"
                          color="primary"
                          textColor="secondary"
                          onPress={() => {
                            StackActions.replace('EmpanelmentRequest');
                          }}
                        />

                        <View style={styles.versionContainer}>
                          <AppText style={styles.versionText}>{getAppVersion()}</AppText>
                        </View>
                      </View>
                    ) : (
                      <View>
                        {isNewUser == false && (
                          <View>
                            <View style={styles.view1}>
                              <View style={{flex: 0.25}}>
                                <TouchableOpacity
                                  style={styles.countryFlagStyle}
                                  onPress={() => {
                                    setShowCountryModal(true);
                                  }}>
                                  <Image
                                    source={{uri: countryFlag}}
                                    style={styles.flagStyle}
                                  />
                                  <Text style={styles.countryIdstyle}>
                                    {countryName}
                                  </Text>
                                  <Image
                                    source={require('./../assets/images/down-icon.png')}
                                    style={styles.downImgstyle}
                                  />
                                </TouchableOpacity>
                              </View>
                              <View style={{flex: 0.18, flexDirection: 'row'}}>
                                <View style={{flex: 0.2}} />
                                <View style={styles.countryIdStyle}>
                                  <Text style={{color: colors.primary}}>
                                    {countryId}
                                  </Text>
                                </View>
                              </View>
                              <View style={styles.mobileInputStyle}>
                                <TextInput
                                  style={styles.userMobile}
                                  placeholder={'  Mobile Number'}
                                  underlineColorAndroid="transparent"
                                  placeholderTextColor="white"
                                  keyboardType="number-pad"
                                  color="white"
                                  value={mobileInputvalue}
                                  onTouchStart={() => {
                                    //setNewUser(true);
                                  }}
                                  onChangeText={(e) => {
                                    changeMobileNumber(e);
                                  }}
                                  onBlur={() => {
                                    handleMobileChange();
                                  }}
                                />
                              </View>
                            </View>
                            <View style={{marginTop: 10}}>
                              <PasswordShow
                                title={'Password'}
                                colorText="white"
                                showSoftInputOnFocus={SoftInputOnFocus}
                                onChangeText={(e) => {
                                  setNewUserPswdValue(e);
                                }}
                                value={newUserPswdValue}
                              />
                            </View>

                            <View style={styles.forgotPwNewUser}>
                              <TouchableOpacity
                                onPress={() => {
                                  props.navigation.navigate('ForgetPwd', {
                                    actionType: 'new_user_fotget_pwd',
                                  });
                                }}>
                                <Text style={styles.forgotPwText}>
                                  Forgot Password ?
                                </Text>
                              </TouchableOpacity>
                            </View>

                            <View style={styles.btnContainer}>
                              <AppButton
                                title="LOGIN"
                                color="primary"
                                textColor="secondary"
                                onPress={() => {
                                  startLoginHere('newuser');
                                }}
                              />
                              
                              <View style={styles.orContainer}>
                                <AppText style={styles.or}>OR</AppText>
                              </View>
                              <AppButton
                                title="PROCEED WITH OTP"
                                color="secondary"
                                textColor="primary"
                                onPress={() => {
                                  newUserOtpLogin();
                                }}
                              />
                              <View style={styles.versionContainer}>
                                <AppText style={styles.versionText}>{getAppVersion()}</AppText>
                              </View>
                            </View>
                          </View>
                        )}

                      </View>
                    )}
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </SafeAreaView>
          </ScrollView>
        </KeyboardAvoidingView>
      </ImageBackground>

      <View style={styles.modalContainer}>
        <Modal animationType="slide" transparent={true} visible={modalVisible}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={styles.modalClose}>
                <TouchableOpacity
                  onPress={() => {
                    setModalVisible(false);
                  }}>
                  <Text style={{fontSize: appFonts.xlargeFontSize}}>X</Text>
                </TouchableOpacity>
              </Text>
              <Text style={styles.modalText}>Mobile Number</Text>

              <FlatList
                data={DATA}
                renderItem={({item}) => (
                  <View style={styles.radioContainer}>
                    <RadioButton
                      value={item.number}
                      status={
                        checkedMobile === item.number ? 'checked' : 'unchecked'
                      }
                      onPress={() => _selectMobile(item.number, item.id)}
                    />
                    <View style={styles.numberContainer}>
                      <TouchableOpacity
                        onPress={() => {
                          _selectMobile(item.number, item.id);
                        }}>
                        <Text style={styles.number}>{item.number}</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                )}
              />
              {checkedMobile == 'first' ? (
                <View />
              ) : (
                <AppButton
                  title="Continue"
                  onPress={() => {
                    setModalVisible(!modalVisible);
                    _goToOtpScreen();
                  }}
                />
              )}
            </View>
          </View>
        </Modal>
      </View>

      <Modal
        animationType="fade"
        transparent={true}
        visible={showCountryModal}
        onRequestClose={() => {
          setShowCountryModal(false);
        }}>
        <CountrySelectModalScreen
          onCancelPress={() => {
            setShowCountryModal(false);
          }}
          searchData={''}
          onSelectCountry={(selectedCountry) => {
            onSelectCountry(selectedCountry);
          }}
          isShowCountryCode={true}
        />
      </Modal>
      {/* {isCountrySelected && <CountryModal visible={true} func={setCountryInfo} />} */}
      {modalVisible && <View style={styles.numberModal} />}

      {/* {
                (isLoading)
                    ?
                    <ShowLoader />
                    //<AppOverlayLoader isLoading={isLoading || isApiLoading} />
                    :
                    <View />
            } */}

      <AppOverlayLoader isLoading={isLoading} isZindex={true} />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  },
  newUserPw: {
    marginHorizontal: 0,
    marginBottom: 30,
    width: '100%',
  },
  orContainer: {
    backgroundColor: 'white',
    borderRadius: 15,
    height: 30,
    width: 30,
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  numberContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 5,
  },
  numberModal: {
    position: 'absolute',
    height,
    width,
    backgroundColor: 'black',
    opacity: 0.7,
    zIndex: 27,
  },
  containerViewMobile: {
    width: '69%',
    flexDirection: 'row',
    borderColor: colors.borderGrey,
    paddingHorizontal: 20,
    alignItems: 'center',
    borderBottomWidth: 2,
  },
  newUser: {flexDirection: 'row', marginTop: '5%'},
  skip: {color: colors.primary, marginTop: 20, fontSize:appFonts.largeBold},
  logo: {height: 60, width: 200, zIndex: 25},
  modalContainer: {
    alignSelf: 'center',
    height,
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 29,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  userMobile1: {
    marginRight: 20,
    textAlign: 'center',
    color: colors.primary,
    minHeight: 14,
    alignSelf: 'center',
  },
  userMobile: {
    width: '100%',
    paddingLeft: normStyle.normalizeWidth(20),
  },
  btnContainer: {top: '20%', alignItems: 'center'},
  flagIcon: {height: '40%', width: '40%', top: '20%'},
  countryName: {color: colors.borderGrey, marginLeft: 5, top: '20%'},
  number: {fontSize: appFonts.normalFontSize, fontWeight: 'bold', textAlign: 'center'},
  loginLogo: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 25,
  },
  newUserMobile: {
    borderColor: colors.borderGrey,
    borderBottomWidth: 2,
    height: '100%',
    width: '26%',
    marginRight: 15,
    flexDirection: 'row',
  },
  inputMobile: {
    borderColor: colors.borderGrey,
    borderBottomWidth: 2,
    width: '55%',
  },
  inputMobile1: {
    borderColor: colors.borderGrey,
    borderBottomWidth: 2,
    width: '15%',
    textAlignVertical: 'center',
    color: colors.primary,
  },
  or: {textAlign: 'center', textAlignVertical: 'center'},
  tabContainer: {
    height: '50%',
    marginLeft: '5%',
    marginRight: '5%',
    //    justifyContent: 'center',
    // marginTop: '10%',
    paddingVertical: 0
  },
  downIcon: {
    height: height * 0.05,
    width: width * 0.03,
    top: height * 0.003,
    marginLeft: 13,
  },
  radioContainer: {flex: 1, flexDirection: 'row'},
  mobileContainer: {flexDirection: 'row', marginTop: '5%'},
  pwContainer: {marginHorizontal: 20},
  imgBg: {
    width: width,
    height: height,
    zIndex: 25,
    resizeMode: 'cover',
    // paddingBottom: '20%',
    paddingTop: Platform.OS === 'ios' ? 25 : 0,
  },
  input: {
    height: 50,
    borderColor: colors.borderGrey,
    borderBottomWidth: 2,
    marginBottom: 10,
    marginTop: 10,
    color: 'white',
  },
  icon: {
    height: height * 0.03,
    width: width * 0.08,
    top: height * 0.02,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalView: {
    backgroundColor: 'white',
    width: '90%',
    borderRadius: 10,
    padding: 30,
  },
  forgotPwNewUser: {
    width: '100%',
    flexDirection: 'row-reverse',
    top: 60,
    marginBottom: 30,
  },
  forgotPwText: {
    fontWeight: 'bold',
    color: 'white',
    fontFamily: 'SourceSansPro-Bold',
  },
  tabView: {
    flexDirection: 'row',
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    height: 60,
  },
  confirmPw: {marginBottom: 40, marginTop: 30},
  countryContainer: {
    borderColor: colors.borderGrey,
    borderBottomWidth: 2,
    height: height * 0.07,
    width: '26%',
    marginRight: '5%',
    flexDirection: 'row',
  },
  forgotPw: {
    width: '100%',
    flexDirection: 'row-reverse',
    marginTop: '20%',
    marginBottom: '30%',
  },
  forgotPw1: {
    fontWeight: 'bold',
    color: 'white',
    fontFamily: 'SourceSansPro-Bold',
  },
  viewContainer: {
    height: '30%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: '5%',
    marginRight: '5%',
  },
  modalText: {
    marginBottom: 20,
    textAlign: 'center',
    fontSize: appFonts.xlargeFontSize,
    fontWeight: 'bold',
  },
  proceedOtp: {
    // marginTop: 10,
  },

  modalClose: {
    left: '95%',
    color: 'grey',
  },
  countryFlagStyle: {
    flex: 1,
    borderColor: colors.borderGrey,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: normStyle.normalizeWidth(2),
    paddingLeft: normStyle.normalizeWidth(5),
  },
  countryIdstyle: {
    fontSize: normStyle.normalizeWidth(14),
    marginLeft: normStyle.normalizeWidth(5),
    color: colors.primary,
  },
  downImgstyle: {
    width: normStyle.normalizeWidth(10),
    height: normStyle.normalizeWidth(20),
    marginLeft: normStyle.normalizeWidth(5),
  },
  countryIdStyle: {
    flex: 0.8,
    alignItems: 'flex-end',
    justifyContent: 'center',
    borderBottomWidth: 2,
    borderColor: colors.borderGrey,
  },
  mobileInputStyle: {
    flex: 0.58,
    borderBottomWidth: normStyle.normalizeWidth(2),
    borderColor: colors.borderGrey,
    alignItems: 'center',
    justifyContent: 'center',
  },
  view1: {
    width: '100%',
    flexDirection: 'row',
    height: normStyle.normalizeWidth(60),
  },
  flagStyle: {
    width: normStyle.normalizeWidth(30),
    height: normStyle.normalizeWidth(30),
  },

  versionContainer: {
    paddingHorizontal: 30,
    marginTop: 15,
    marginBottom: 15,
    alignItems: 'center',
    paddingVertical: 20,
  },
  versionText: {
    fontSize: appFonts.normalFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
    color: colors.primary,
  },
});

export default Login;
