import React, { useEffect, useState } from 'react';
import {
  View, StyleSheet, ScrollView, TextInput, Platform,
  Dimensions, Image, Text, TouchableOpacity, PermissionsAndroid
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import AppButton from '../components/ui/AppButton';
import AppHeader from '../components/ui/AppHeader';
import AppTextBold from '../components/ui/AppTextBold';
import AppText from '../components/ui/ AppText';
import Screen from '../components/Screen';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppFooter from '../components/ui/AppFooter';
import * as normStyle from '../styles/StyleSize';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import apiLogin from './../api/apiLogin';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import appSnakBar from '../utility/appSnakBar';
import { useNavigation } from '@react-navigation/native';
import DateTimePicker from '@react-native-community/datetimepicker';
import appAlert from '../utility/appAlert';
import Orientation from 'react-native-orientation';

const { height } = Dimensions.get('window');

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

const EditDateOfBirth = ({ props, route }) => {

  const routeParams = route.params;
  const dobValue =
    routeParams && routeParams.dobValue && routeParams.dobValue
      ? routeParams.dobValue
      : '';

  const userId =
    routeParams && routeParams.userId && routeParams.userId
      ? routeParams.userId
      : '';

  const [isApiLoading, setApiIsLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [dob, setDob] = useState("");
  const [docBase64, setDocBase64] = useState("");
  const [docId, setDocId] = useState("");
  const [docName, setDocName] = useState("");
  const [docSize, setDocSize] = useState("");
  const [docMime, setDocMime] = useState("");
  const [hideAttachment, setHideAttachment] = useState(true);
  const [disableButton, setDisableButton] = useState(true);
  const navigation = useNavigation();

  let restrictDate = new Date();

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);


  useEffect(() => {
    setDob(dobValue);
  }, [loginvalue]);

  const loginvalue = useSelector(
    (state) => state.loginInfo.loginResponse,
  );

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );
  const menuType = useSelector((state) => state.menuType.menuType);
  const goBack = () => {
    navigation.goBack();
  };

  const onChange = (event, selectedDate) => {
    setShow(false)

    if (event.type == "dismissed") {
      setDob(dob);
      console.log(dobValue + " " + dob);
      if (dobValue == dob) {
        setHideAttachment(true);
        setDisableButton(true);
      } else {
        setHideAttachment(false);
        setDisableButton(false);
      }
    } else {
      const currentDate = selectedDate || date;
      let d = new Date(currentDate);
      if(Platform.OS == 'ios'){
        d.setDate(d.getDate()-1);
      }
      let dt = d.getDate() + " " + monthNames[d.getMonth()] + ", " + d.getFullYear();
      setDob(dt);
      if (dobValue == dt) {
        setHideAttachment(true);
        setDisableButton(true);
      } else {
        setHideAttachment(false);
        setDisableButton(false);
      }
      //setHideAttachment(false);
    }
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const _selectDate = () => {
    showMode('date');
  };

  const chooseImage = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [
          DocumentPicker.types.images,
          DocumentPicker.types.pdf,
        ],
        readContent: true,
      });
      var fileType = (res.type).substring((res.type).lastIndexOf('/') + 1);
      console.log(fileType);
      if (fileType == 'png' || fileType == 'PNG' || fileType == 'jpeg' || fileType == 'JPEG' || fileType == 'pdf'
        || fileType == 'PDF' || fileType == 'JPG' || fileType == 'jpg') {
        setDocName(res.name);
        setDocSize(res.size);
        setDocMime((res.type).substring((res.type).lastIndexOf('/') + 1));
        _getBase64(res.uri);
      } else {
        appSnakBar.onShowSnakBar('Only image and pdf file is accepted', 'LONG');
      }

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  const _getBase64 = (uri) => {
    RNFetchBlob.fs.readFile(uri, 'base64')
      .then((files) => {
        setDocBase64(files);
      })
  };

  const _convertDob = (value) => {
    if (value) {
      let d = new Date(value);
      let ans = d.getFullYear() + "-" + [d.getMonth() + 1] + "-" + d.getDate();
      return ans;
    } else {
      return "";
    }

  };

  const _changeDOB = async () => {
    if (dobValue == dob) {
      return null;
    } else {
      if (docBase64 != '' && docName != '' && docSize != '' && docMime != '') {
        let old_Value = _convertDob(dobValue);
        let new_Value = _convertDob(dob);
        const request = {
          device_id: deviceUniqueId,
          new_value: new_Value,
          old_value: old_Value,
          type: "dob",
          user_id: userId,
          attachedFiles: {
            file: docBase64,
            file_id: 123,
            file_name: docName,
            file_size: docSize,
            mimeType: docMime,
          }
        };
        console.log(request);
        setIsLoading(true);
        const result = await apiLogin.apiForUpdateProfileData(request);
        setIsLoading(false);
        var status = result.data.status;
        console.log(result.data);
        if (status == 200) {
          appAlert.showAppAlert('Service Request', "Request submitted successfully. Your service request number is " + result.data.request_id);
          goBack();
        } else {
          appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
        }
      } else {
        appSnakBar.onShowSnakBar('Attach address proof document', 'LONG');
      }
    }
  };


  const _removeDoc = () => {
    setDocName(""); setDocBase64(""); setDocSize(""); setDocMime();
  };

  return (
    <Screen>
      <AppHeader />

    <ScrollView>
      <View style={styles.container}>

        <View style={{ width: '100%', alignItems: 'flex-start' }}>
          <AppText style={styles.pageTitle}>Edit Date of Birth</AppText>
        </View>

        <View style={{ width: '100%', alignItems: 'flex-start' }}>
          <AppText style={{ marginLeft: '5%', color: colors.gray }}>Date of Birth</AppText>
        </View>
        <TouchableOpacity onPress={() => { _selectDate(); }}>
          <View style={{ width: '100%', flexDirection: 'row' }}>

            <View style={{ flex: 0.05 }} />
            <View style={styles.dobStyle}>
              <AppText style={{ color: colors.secondaryDark }}>{dob}</AppText>
            </View>
            <View style={styles.editStyle}>
              {/* <TouchableOpacity onPress={() => { _selectDate(); }}> */}
              <Image
                source={require('./../assets/images/date-icon.png')}
                style={styles.editImg}
              />
              {/* </TouchableOpacity> */}
            </View>
            <View style={{ flex: 0.05 }} />
          </View>
        </TouchableOpacity>
        {
        (show)
          ?
          <DateTimePicker
            testID="dateTimePicker"
            timeZoneOffsetInMinutes={0}
            value={new Date()}
            mode={mode}
            display="default"
            style={{margin: 10, marginBottom: -10}}
            onChange={onChange}
            maximumDate={new Date(restrictDate.getFullYear(), restrictDate.getMonth(), restrictDate.getDate())}
          />
          :
          <View />
        }
        {
          (!hideAttachment)
            ?
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <View style={{ width: '100%', alignItems: 'flex-start', flexDirection: 'row', marginTop: normStyle.normalizeWidth(10) }}>
                <View style={{ flex: 0.8 }}>
                  <AppText style={{ marginLeft: '5%' }}>Please attach a copy of your PAN Card as proof of DOB.</AppText>
                </View>
                <View style={{ flex: 0.2, alignItems: 'center', justifyContent: 'center' }}>
                  <TouchableOpacity onPress={() => { chooseImage(); }}>
                    <Image
                      source={require('./../assets/images/attachment-icon.png')}
                      style={styles.cameraStyle}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              {
                (docName)
                  ?
                  <View style={styles.docStyle}>
                    <View style={{ flex: 0.8 }}>
                      <AppText style={{ color: colors.greenColor }}>{docName}</AppText>
                    </View>
                    <View style={{ flex: 0.2, alignItems: 'flex-end', justifyContent: 'center' }}>
                      <TouchableOpacity onPress={() => { _removeDoc(); }}>
                        <Image
                          source={require('./../assets/images/cancel.png')}
                          style={styles.cancelStyle}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                  :
                  <View />
              }
            </View>
            :
            <View />
        }

        
          <View style={styles.button}>
            <AppButton
              color={disableButton ? 'charcoal' : 'secondary'}
              disabled={disableButton}
              textColor = {disableButton ? 'primary' : 'primary'}
              title="Save"
              onPress={() => { _changeDOB() }}
            />
        
        </View>

      </View>
    </ScrollView>  
      <AppFooter activePage={0} isPostSales={menuType == 'postsale' ? true : false} />
      <AppOverlayLoader isLoading={isLoading || isApiLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  pageTitle: {
    fontSize: normStyle.normalizeWidth(20),
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginLeft: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(40),
  },
  button: {
    // width: normStyle.normalizeWidth(100),
    // height: normStyle.normalizeWidth(0),
    alignItems: 'center', justifyContent: 'center',
    marginHorizontal:15,
    paddingBottom: normStyle.normalizeWidth(40),
    paddingTop: normStyle.normalizeWidth(40),
  },
  dobStyle: {
    flex: 0.7, height: normStyle.normalizeWidth(50),
    borderBottomWidth: 0.5, alignItems: 'flex-start',
    justifyContent: 'center'
  },
  editImg: {
    width: normStyle.normalizeWidth(20),
    height: normStyle.normalizeWidth(20),
  },
  editStyle: {
    flex: 0.20, height: normStyle.normalizeWidth(50), borderBottomWidth: 0.5,
    alignItems: 'flex-end', justifyContent: 'center'
  },
  cameraStyle: {
    width: normStyle.normalizeWidth(25),
    height: normStyle.normalizeWidth(25),
  },
  cancelStyle: {
    width: normStyle.normalizeWidth(15),
    height: normStyle.normalizeWidth(15),
  },
  docStyle: {
    width: '90%', marginTop: normStyle.normalizeWidth(10), flexDirection: 'row',
    backgroundColor: '#ebebeb', padding: normStyle.normalizeWidth(10)
  }
});

export default EditDateOfBirth;
