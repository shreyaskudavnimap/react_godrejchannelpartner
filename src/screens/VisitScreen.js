import React, {useState} from 'react';
import {View, StyleSheet, TouchableOpacity, Text, Image, FlatList} from 'react-native';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';

const VisitScreen = (props) =>{
    return(
        <Screen>
            <AppHeader />
                <View style = {{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text>visit screen</Text>
                </View>
            <AppFooter activePage={4} isPostSales={false} />
        </Screen>
    );
};

export default VisitScreen;