import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
  Modal,
  TouchableWithoutFeedback,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import DropDownPicker from 'react-native-dropdown-picker';
//components
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import AppProgressBar from '../components/ui/AppProgressBar';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import GalleryModalScreen from './modals/GalleryModalScreen';
//config
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import appSnakBar from '../utility/appSnakBar';
//api
import DASH_BOARD_API from '../api/apiDashboard';
//hooks
import useFileDownload from '../hooks/useFileDownload';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import Orientation from 'react-native-orientation';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
let carousel;
const MonthList = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];
let propertyDropdownController;
let monthDropdownController;
const ConstructionStatus = (props) => {
  const {route} = props;
  const [activeIndex, setActiveIndex] = useState(0);
  const {loginInfo} = useSelector((state) => state);
  const dashboardData = useSelector((state) => state.dashboardData);
  const propertyData = dashboardData.rmData;
  const [bookingId, setBookingId] = useState(route.params.bookingId);
  const [months, setMonths] = useState([{label: 'Select Month', value: ''}]);
  const [currentMonth, setCurrentMonth] = useState('');
  const [constructionStatus, setConstructionStatus] = useState([]);
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [disableMonthDropDown,setDisableMonthDropDown] = useState(false);
  const userId =
    loginInfo &&
    loginInfo.loginResponse &&
    loginInfo.loginResponse.data &&
    loginInfo.loginResponse.data.userid
      ? loginInfo.loginResponse.data.userid
      : '';
  const [modalVisible, setModalVisible] = useState(false);
  const [galleryData, setGalleryData] = useState([]);
  const [monthNotFoundMessage, setMonthNotFoundMessage] = useState('');

  const [
    dynamicBottomPaddingProperty,
    setDynamicBottomPaddingProperty,
  ] = useState(0);

  const [dynamicBottomPaddingPeriod, setDynamicBottomPaddingPeriod] = useState(
    0,
  );

  const {
    progress,
    showProgress,
    error,
    isSuccess,
    checkPermission,
  } = useFileDownload();

  const dropDownPropertyData =
    propertyData && propertyData.length > 0
      ? propertyData.map((item) => {
          return {
            label: `${item.property_name}/${item.inv_flat_code}`,
            value: item,
          };
        })
      : [{label: 'Select Month', value: ''}];

  const selectedProperty =
    propertyData && propertyData.length > 0
      ? propertyData.find((element) => element.booking_id == bookingId)
      : '';

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
    
    getDateRange(bookingId);
    
  }, []);
  useEffect(() => {
    getDateRange(bookingId);
    getConstructionStatus();
  }, [ bookingId]);
  useEffect(()=>{
    getConstructionStatus();
  },[currentMonth])
  /**
   *
   * @param {string} fileName
   * @param {string} fileUrl
   * @description On download file check the validations
   */
  const onDownloadFile = (fileName, fileUrl) => {
    if (fileName && fileName != '' && fileUrl && fileUrl != '') {
      const ext = fileName.split('.')[0];
      if (ext) {
        downloadFile(fileUrl, fileName, ext);
      }
    }
  };
  /**
   *
   * @param {string} imgPath
   * @param {string} fileName
   * @param {string} fileExtension
   * @description Download  desire files
   */
  const downloadFile = async (imgPath, fileName, fileExtension) => {
    try {
      await checkPermission(imgPath, fileName, fileExtension);
    } catch (error) {
      appSnakBar.onShowSnakBar(error.message, 'LONG');
    }
  };
  /**
   *
   * @description Get construction status for desire month and year
   */
  const getConstructionStatus = () => {
    setIsShowLoader(true);
    const [month, year] = currentMonth.split(' ');
    const data = {
      user_id: userId,
      booking_id: bookingId,
      month: month,
      year: year,
    };
    DASH_BOARD_API.getConstructionStatus(data)
      .then((response) => {
        setIsShowLoader(false);
        if (response.data && response.data.data) {
          if(response.data.data.construction_status){
            setConstructionStatus(response.data.data.construction_status);
            getGalleryData(response.data.data.construction_status);
          }
          else{
            setConstructionStatus([]);
            getGalleryData([]);
            
          }
        }
      })
      .catch((err) => {
        setIsShowLoader(false);
        console.log('err', err);
      });
  };

  /**
   *
   * @description Set the gallery detail from construction image list
   */
  const getGalleryData = (data) => {
    if (!data) {
      return;
    }
    //field_media_type,field_gallery_image_url,field_gallery_thumb_image_url
    //	const regex = /\.(gif|jpe?g|tiff?|png|webp|bmp)$/i;
    const fileArray = [];
    data.forEach((element) => {
      const url = element.image;
      const field_media_type = 'image';
      fileArray.push({
        field_media_type,
        field_gallery_image_url: url,
        field_gallery_thumb_image_url: url,
      });
    });
    setGalleryData([...fileArray]);
  };
  /**
   *
   * @description Create a message if month not found or before six month of current month
   */
  // const monthNotFound=(month)=>{
  //   let message;
  //   console.log('month',month)
  //   if(!month.length){
  //     message = 'Construction and handover of this tower is already completed, hence no new updates are available';
  //     setMonthNotFoundMessage(message);
  //   }
  //   else{
  //     const constructionDate = month[0].split(' ');
  //     const constructionMonth = constructionDate[0];
  //     const constuctionYear = constructionDate[1];

  //     const currentDate = new Date();
  //     const currentMonth = currentDate.getMonth();
  //     const currentYear = currentDate.getFullYear();
  //     const yearDifference = (currentYear)-(+constuctionYear);
  //     const monthDifference = (currentMonth+yearDifference*12)-(MonthList.indexOf(constructionMonth));
  //     if(monthDifference>6){
  //       message = "No update available for your project, kindly contact your relationship manager."
  //       setMonthNotFoundMessage(message);
  //     }

  //   }
  // }
  /**
   *
   * @description Get date range for construction status
   */
  const getDateRange = (bookingId) => {
    const data = {
      user_id: userId,
      booking_id: bookingId,
    };
    
    setIsShowLoader(true);
    DASH_BOARD_API.getConstructionDateRange(data)
      .then((response) => {
        setIsShowLoader(false);
        console.log('response',response)
        if (response.data && response.data.status === '500') {
          if (response.data.msg) {
            setMonthNotFoundMessage(response.data.msg);
            setMonths([{label: 'Select Month', value: ''}])
            setCurrentMonth('')
            setDisableMonthDropDown(true)
            return;
          }
          
        }
        if (response.data && response.data.data && response.data.data.month) {
          //monthNotFound(response.data.data.month);
          setMonthNotFoundMessage('');
          const dropDownMonthArray = response.data.data.month.map((month) => {
            return {
              label: month,
              value: month,
            };
          });
          setMonths(dropDownMonthArray);
          setCurrentMonth(
            dropDownMonthArray.length ? dropDownMonthArray[0].value : null,
          );
          if(!dropDownMonthArray.length){
            setDisableMonthDropDown(true)
          }
          else{
            setDisableMonthDropDown(false)
          }
        } else {
          //monthNotFound([]);
        }
      })
      .catch((err) => {
        console.log('err', err);
        setIsShowLoader(false);
      });
  };
  /**
   *
   * @param {object} item
   * @description On change property set the current booking id
   */
  const changeProperty = (item) => {
    
    setBookingId(item.value.booking_id);
  };
  /**
   *
   * @param {object} item
   * @description On change month set the month
   */
  const onChangeMonth = (item) => {
    setCurrentMonth(item.value);
  };

  const downloadArchitechure = () => {
    if (
      constructionStatus && constructionStatus[activeIndex] &&
      constructionStatus[activeIndex].certificate
    ) {
      const cerificateFileArray = constructionStatus[
        activeIndex
      ].certificate.split('/');
      const fileName = cerificateFileArray[cerificateFileArray.length - 1];
      onDownloadFile(fileName, constructionStatus[activeIndex].certificate);
    }
  };
  /**
   *
   * @description On click outside close the dropdowns
   */
  const closeDropdown = () => {
    if (propertyDropdownController && propertyDropdownController.isOpen()) {
      propertyDropdownController.close();
    }
    if (monthDropdownController && monthDropdownController.isOpen()) {
      monthDropdownController.close();
    }
  };

  const isMessageFound = monthNotFoundMessage ? true : false;

  const onHandleRetryBtn = () => {
    getDateRange(bookingId);
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />
      <ScrollView>
        <TouchableWithoutFeedback onPress={() => closeDropdown()}>
          <View style={styles.container}>
            <AppTextBold style={styles.pageTitle}>
              Construction Status
            </AppTextBold>

            <View>
              <AppText style={{marginBottom: 5}}>Property</AppText>
              {dropDownPropertyData && dropDownPropertyData.length > 0 ? (
                <DropDownPicker
                  defaultValue={selectedProperty}
                  items={dropDownPropertyData}
                  style={styles.dropdownDocument}
                  placeholderStyle={styles.dropdownPlaceholder}
                  itemStyle={styles.dropDownItem}
                  labelStyle={styles.label}
                  onChangeItem={changeProperty}
                  controller={(instance) =>
                    (propertyDropdownController = instance)
                  }
                  onOpen={() => {
                    if (
                      monthDropdownController &&
                      monthDropdownController.isOpen()
                    ) {
                      monthDropdownController.close();
                    }
                    setDynamicBottomPaddingProperty(
                      dropDownPropertyData && dropDownPropertyData.length > 0
                        ? dropDownPropertyData.length * 65
                        : 65,
                    );
                  }}
                  onClose={() => {
                    setDynamicBottomPaddingProperty(0);
                  }}
                />
              ) : null}
              <View
                style={{paddingBottom: dynamicBottomPaddingProperty}}></View>

              <AppText style={{marginBottom: 5}}>Period</AppText>
              <DropDownPicker
                items={months}
                disabled={disableMonthDropDown}
                onChangeItem={onChangeMonth}
                defaultValue={currentMonth}
                style={styles.dropdownDocument}
                placeholderStyle={styles.dropdownPlaceholder}
                placeholder={'Select Month'}
                itemStyle={styles.dropDownItem}
                labelStyle={styles.label}
                controller={(instance) => (monthDropdownController = instance)}
                onOpen={() => {
                  if (
                    propertyDropdownController &&
                    propertyDropdownController.isOpen()
                  ) {
                    propertyDropdownController.close();
                  }
                  setDynamicBottomPaddingPeriod(
                    months && months.length > 0 ? months.length * 65 : 65,
                  );
                }}
                onClose={() => {
                  setDynamicBottomPaddingPeriod(0);
                }}
              />
              <View style={{paddingBottom: dynamicBottomPaddingPeriod}}></View>
            </View>

            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                margin: 10,
              }}>
              {constructionStatus.length && !isMessageFound ? (
                <Carousel
                  style={{justifyContent: 'center', alignItems: 'center'}}
                  layout={'default'}
                  ref={(ref) => (carousel = ref)}
                  data={constructionStatus}
                  sliderWidth={windowWidth}
                  itemWidth={windowWidth}
                  onSnapToItem={(index) => setActiveIndex(index)}   
                  renderItem={({item}) => (
                    <View>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <TouchableWithoutFeedback
                          onPress={() => setModalVisible(!modalVisible)}>
                          <Image
                            style={styles.image}
                            source={{uri: item.image}}
                          />
                        </TouchableWithoutFeedback>
                      </View>
                      <AppText style={styles.imgTitle}>
                        {item.description}
                      </AppText>
                      <View
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          // width: windowWidth * 0.8,
                        }}>
                        <Pagination
                          dotsLength={constructionStatus.length}
                          activeDotIndex={activeIndex}
                          dotStyle={{
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            backgroundColor: colors.secondary,
                          }}
                          inactiveDotOpacity={0.2}
                          inactiveDotScale={1}
                        />
                      </View>
                    </View>
                  )}
                />
              ) : (
                <View>
                  {!isMessageFound ? (
                    <View
                      style={{
                        ...styles.image,
                        backgroundColor: colors.lightGray,
                      }}></View>
                  ) : (
                    <View
                      style={{
                        backgroundColor: '#FFE6E7',
                        borderRadius: 5,
                        padding: 10,
                        marginHorizontal: -10,
                      }}>
                      <View>
                        <AppText>{monthNotFoundMessage}</AppText>
                      </View>
                    </View>
                  )}
                </View>
              )}
            </View>
            {constructionStatus && constructionStatus[activeIndex] &&
            constructionStatus[activeIndex].certificate != '' ? (
              <TouchableOpacity
                style={styles.downloadContainer}
                onPress={() => downloadArchitechure()}>
                <Image
                  style={styles.icon}
                  source={require('./../assets/images/download-b.png')}
                />

                <AppText>Architecture Certificate</AppText>
              </TouchableOpacity>
            ) : (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 5,
                }}>
                <Image
                  style={styles.icon}
                  source={require('./../assets/images/download-g2.png')}
                />
                <AppText style={{color: colors.gray, marginHorizontal: 10}}>
                  Architecture Certificate
                </AppText>
              </View>
            )}
          </View>
        </TouchableWithoutFeedback>
        <Modal
          animationType="fade"
          transparent={true}
          visible={modalVisible}
          supportedOrientations={['portrait', 'landscape']}
          onRequestClose={() => {
            Orientation.lockToPortrait();
            setModalVisible(!modalVisible);
          }}>
          <GalleryModalScreen
            onPress={() => {
              Orientation.lockToPortrait();
              setModalVisible(!modalVisible);
            }}
            mediaIndex={activeIndex}
            galleryData={galleryData}
          />
        </Modal>
      </ScrollView>
      {<AppOverlayLoader isLoading={isShowLoader} />}
      {showProgress ? <AppProgressBar progress={progress} /> : null}
      <AppFooter activePage={0} isPostSales={true} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginBottom: 28,
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.jaguar,
    textTransform: 'uppercase',
    marginBottom: '8%',
  },
  imgTitle: {
    marginTop: 10,
    marginHorizontal: 35,
    fontSize: appFonts.xlargeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  dropdownPlaceholder: {
    fontSize:appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  label: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '5%',
  },
  dropDownItem: {
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGray,
    // marginTop: '5%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropdownDocument: {
    height: '100%',
    width: '100%',
    marginBottom: '5%',
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderTopWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  downloadContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  icon: {height: 20, width: 20, marginHorizontal: 5},
  image: {
    height: windowHeight * 0.4,
    width: windowWidth * 0.88,
    resizeMode: 'stretch',
    // borderRadius: 1.8
  },
});

export default ConstructionStatus;
