import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList,
  ScrollView,
  TouchableWithoutFeedback,
  Alert,
  ActivityIndicator,
} from 'react-native';

import ImageModal from 'react-native-image-modal';
import {WebView} from 'react-native-webview';
import {useBackHandler} from '@react-native-community/hooks';
import Orientation from 'react-native-orientation';

import Screen from '../components/Screen';
import AppButton from '../components/ui/AppButton';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import appFonts from '../config/appFonts';
import AppHeader from '../components/ui/AppHeader';
import colors from '../config/colors';

import EOITypologyList from '../components/ui/EOITypologyList';
import EOITowerList from '../components/ui/EOITowerList';
import EOIInventoryListItem from '../components/ui/EOIInventoryList';

import {useDispatch, useSelector} from 'react-redux';
import * as eoiMPLAction from './../store/actions/eoiMPLAction';

import apiEoiMLP from './../api/apiEoiMLP';

import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import {EventRegister} from 'react-native-event-listeners';
import {StackActions} from '@react-navigation/native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const EOIFloorLayout = ({navigation, route}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [isInnerLoading, setIsInnerLoading] = useState(false);
  const [isFloorLoading, setIsFloorLoading] = useState(false);
  const [isJourneyLoading, setIsJourneyLoading] = useState(false);

  const [showWebview, setShowWebview] = useState(false);
  const [isFOYR, setIsFOYR] = useState(false);

  const routeParams = route.params;
  const projectId =
    routeParams && routeParams.item && routeParams.item.proj_id
      ? routeParams.item.proj_id
      : '';
  const foyrUrl =
    routeParams && routeParams.item && routeParams.item.field_foyr_url
      ? routeParams.item.field_foyr_url
      : '';

  const preferenceCombination =
    routeParams && routeParams.item && routeParams.item.preference_combination
      ? routeParams.item.preference_combination
      : 'typology_tower_floor';

  const projectSfdcId =
    routeParams && routeParams.item && routeParams.item.project_sfdc_id
      ? routeParams.item.project_sfdc_id
      : '';

  // typology
  // typology_tower
  // typology_tower_floor
  // const preferenceCombination = 'typology_tower_floor';

  const dispatch = useDispatch();

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );

  const loginUserData = useSelector((state) => state.loginInfo.loginResponse);
  const userId =
    loginUserData && loginUserData.data && loginUserData.data.userid
      ? loginUserData.data.userid
      : '';

  const eoiMLPData = useSelector((state) => state.eoiMLP);

  const typologyList = eoiMLPData.typologyList.data;
  const towerList = eoiMLPData.towerList.data;
  const floorBandList = eoiMLPData.floorBandList.data;

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  useEffect(() => {
    setShowWebview(false);
    if (deviceUniqueId) {
      getEoiFiltersDataTypology(userId, deviceUniqueId, projectId);
    }
  }, [dispatch, deviceUniqueId]);

  useEffect(() => {
    const eventListenerBooking = EventRegister.addEventListener(
      'EOIFloorLayoutUpdateEventBooking',
      (data) => {
        if (data) {
          if (userId) {
            goToEoiBookingPageAfterLogin(data);
          }
        }
      },
    );

    const eventListenerBookingFOYR = EventRegister.addEventListener(
      'EOIFloorLayoutUpdateEventBookingFOYR',
      (data) => {
        if (data) {
          if (userId) {
            goToEoiBookingPageAfterLogin(data);
          }
        }
      },
    );

    return () => {
      EventRegister.removeEventListener(eventListenerBooking);
      EventRegister.removeEventListener(eventListenerBookingFOYR);
    };
  }, [dispatch, userId]);

  const getEoiFiltersDataTypology = useCallback(
    async (userId, deviceId, projectId) => {
      setIsLoading(true);
      try {
        const eoiFiltersDataTypology = await dispatch(
          eoiMPLAction.getEoiFiltersData(
            userId,
            deviceId,
            projectId,
            'typology',
            '',
            '',
            '',
          ),
        );
        setIsLoading(false);

        if (
          eoiFiltersDataTypology &&
          eoiFiltersDataTypology.data &&
          eoiFiltersDataTypology.data.length > 0
        ) {
          if (
            preferenceCombination &&
            preferenceCombination != '' &&
            (preferenceCombination.toLowerCase() === 'typology_tower' ||
              preferenceCombination.toLowerCase() === 'typology_tower_floor')
          ) {
            getEoiFiltersDataTower(
              userId,
              deviceId,
              projectId,
              eoiFiltersDataTypology.data[0].id,
            );
          }
        } else {
          appSnakBar.onShowSnakBar(
            eoiFiltersDataTypology.msg
              ? eoiFiltersDataTypology.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
          navigation.goBack();
        }
      } catch (err) {
        appSnakBar.onShowSnakBar(err.message, 'LONG');
        navigation.goBack();
        setIsLoading(false);
      }
    },
    [
      dispatch,
      setIsLoading,
      isLoading,
      deviceUniqueId,
      typologyList,
      towerList,
      floorBandList,
    ],
  );

  const onSelectTypology = (typology) => {
    dispatch(eoiMPLAction.updateTypologyList(typology));

    if (
      preferenceCombination &&
      preferenceCombination != '' &&
      (preferenceCombination.toLowerCase() === 'typology_tower' ||
        preferenceCombination.toLowerCase() === 'typology_tower_floor')
    ) {
      getEoiFiltersDataTower(userId, deviceUniqueId, projectId, typology.id);
    }
  };

  const getFieldPropertyLayoutPlanImg = () => {
    if (typologyList && typologyList.length > 0) {
      const selectedTypologyIndex = typologyList.findIndex(
        (x) => x.selectedTypology == true,
      );
      return (
        typologyList[selectedTypologyIndex] &&
        typologyList[selectedTypologyIndex].field_eoi_plan_image_url
      );
    } else {
      return '';
    }
  };

  const getEoiFiltersDataTower = useCallback(
    async (userId, deviceId, projectId, typologyId) => {
      setIsInnerLoading(true);
      try {
        const eoiFiltersDataTower = await dispatch(
          eoiMPLAction.getEoiFiltersData(
            userId,
            deviceId,
            projectId,
            'tower',
            typologyId,
            '',
            '',
          ),
        );
        setIsInnerLoading(false);

        if (
          eoiFiltersDataTower &&
          eoiFiltersDataTower.data &&
          eoiFiltersDataTower.data.length > 0
        ) {
          console.log('Success...');
        } else {
          appSnakBar.onShowSnakBar(
            eoiFiltersDataTower.msg
              ? eoiFiltersDataTower.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      } catch (err) {
        appSnakBar.onShowSnakBar(err.message, 'LONG');
        navigation.goBack();
        setIsInnerLoading(false);
      }
    },
    [
      dispatch,
      setIsInnerLoading,
      isInnerLoading,
      deviceUniqueId,
      typologyList,
      towerList,
      floorBandList,
    ],
  );

  const onSelectTower = (tower) => {
    dispatch(eoiMPLAction.updateTowerList(tower));

    if (
      preferenceCombination &&
      preferenceCombination != '' &&
      preferenceCombination.toLowerCase() === 'typology_tower_floor'
    ) {
      if (tower.selectedTower) {
        if (typologyList && typologyList.length > 0) {
          const selectedTypologyIndex = typologyList.findIndex(
            (x) => x.selectedTypology == true,
          );

          getEoiFiltersDataFloorBand(
            userId,
            deviceUniqueId,
            projectId,
            typologyList[selectedTypologyIndex].id,
            tower.id,
          );
        }
      }
    }
  };

  const getEoiFiltersDataFloorBand = useCallback(
    async (userId, deviceId, projectId, typologyId, towerId) => {
      setIsFloorLoading(true);
      try {
        const eoiFiltersDataFloorBand = await dispatch(
          eoiMPLAction.getEoiFiltersData(
            userId,
            deviceId,
            projectId,
            'floor_band',
            typologyId,
            towerId,
            '',
          ),
        );
        setIsFloorLoading(false);

        if (
          eoiFiltersDataFloorBand &&
          eoiFiltersDataFloorBand.data &&
          eoiFiltersDataFloorBand.data.length > 0
        ) {
          console.log('Success...');
        } else {
          appSnakBar.onShowSnakBar(
            eoiFiltersDataFloorBand.msg
              ? eoiFiltersDataFloorBand.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
          navigation.goBack();
        }
      } catch (err) {
        appSnakBar.onShowSnakBar(err.message, 'LONG');
        setIsFloorLoading(false);
        navigation.goBack();
      }
    },
    [
      dispatch,
      setIsFloorLoading,
      isFloorLoading,
      deviceUniqueId,
      typologyList,
      towerList,
      floorBandList,
    ],
  );

  const onSelectFloorBand = (floorBand) => {
    dispatch(eoiMPLAction.updateFloorBandList(floorBand));
  };

  const onSelectionPlanImg = (floorBand) => {
    dispatch(eoiMPLAction.updateFloorBandPlan(floorBand));
  };

  useBackHandler(() => {
    if (showWebview) {
      Orientation.lockToPortrait();
      setShowWebview(false);
      return true;
    }
    return false;
  });

  const goToFOYR = () => {
    checkProjectAvailability('3DView');
  };

  const goToSiteVisitScreen = () => {
    navigation.navigate('PresaleVisitScreen', {projectId: projectId});
  };

  const goToBookingDetails = () => {
    checkProjectAvailability('Booking');
  };

  const checkProjectAvailability = async (action) => {
    setIsJourneyLoading(true);

    try {
      const projectAvailabilityResult = await apiEoiMLP.apiCheckProjectAvailability(
        projectId,
      );
      setIsJourneyLoading(false);

      if (
        projectAvailabilityResult.data[0].hasOwnProperty('status') &&
        projectAvailabilityResult.data[0].status != null &&
        projectAvailabilityResult.data[0].status == '1'
      ) {
        if (action == 'Booking') {
          checkGuestLogin('Booking');
        } else if (action == '3DView') {
          onHandleOpenWebviewFOYR();
        }
      } else {
        setIsJourneyLoading(false);
        projectAvailabilityError();
      }
    } catch (error) {
      console.log(error);
      setIsJourneyLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const onHandleOpenWebviewFOYR = () => {
    Orientation.lockToLandscape();
    setIsFOYR(true);
    setShowWebview(true);
  };

  const onCloseFOYRWebView = () => {
    setIsFOYR(false);
    Orientation.lockToPortrait();
    setShowWebview(false);
  };

  const handleWebViewNavigationStateChange = ({url}) => {
    if (isFOYR) {
      const pathClose = url.split('foyr-close=');
      if (pathClose && pathClose.length > 1 && pathClose[1] == 'true') {
        onCloseFOYRWebView();
      } else {
        const pathCloseFOYR = url.split('foyrTxnID=');
        if (pathCloseFOYR && pathCloseFOYR.length > 0) {
          const foyrData = appConstant.getJsonFromUrl(url);
          console.log('foyrData: ', foyrData);

          if (
            foyrData &&
            foyrData.flattype &&
            foyrData.tower &&
            foyrData.Floor_Band &&
            foyrData.foyrTxnID
          ) {
            onCloseFOYRWebView();
            checkGuestLogin('3DView', foyrData);
          } else {
            if (!showWebview)
              appSnakBar.onShowSnakBar(
                'Inventory selection facility is not available through FOYR',
                'LONG',
              );
          }
        }
      }
    }

    console.log('handleWebViewNavigationStateChange newNavState : ', url);
  };

  const ActivityIndicatorElement = () => {
    return (
      <AppOverlayLoader isLoading={true} isWhiteBackground={true} />
      // <ActivityIndicator
      //   color={colors.jaguar}
      //   size="small"
      //   style={styles.activityIndicatorStyle}
      // />
    );
  };

  const checkGuestLogin = (action, foyrData) => {
    if (userId && userId != '') {
      if (action === 'Booking') {
        goToEoiBookingPage();
      } else if (action === '3DView') {
        if (foyrData) goToEoiBookingPageFOYR(foyrData);
      }
    } else {
      if (action === 'Booking') {
        validateNextData('Booking');
      } else if (action === '3DView') {
        if (foyrData) validateNextData('3DView', foyrData);
      }
    }
  };

  const validateNextData = (action, foyrData) => {
    if (action === 'Booking') {
      let refundStaus = '';

      const typologyListData = eoiMLPData.typologyList;
      const towerListData = eoiMLPData.towerList;
      const floorBandListData = eoiMLPData.floorBandList;

      if (floorBandListData && Object.keys(floorBandListData).length > 0) {
        refundStaus = floorBandListData.refund_staus
          ? floorBandListData.refund_staus
          : '';
      } else {
        if (towerListData && Object.keys(towerListData).length > 0) {
          refundStaus = towerListData.refund_staus
            ? towerListData.refund_staus
            : '';
        } else {
          if (typologyListData && Object.keys(typologyListData).length > 0) {
            refundStaus = typologyListData.refund_staus
              ? typologyListData.refund_staus
              : '';
          } else {
            refundStaus = '';
          }
        }
      }

      const preferenceData = {
        data: {
          typology:
            typologyListData &&
            Object.keys(typologyListData).length > 0 &&
            typologyListData.data
              ? typologyListData.data
              : [],
          tower:
            towerListData &&
            Object.keys(towerListData).length > 0 &&
            towerListData.data
              ? towerListData.data
              : [],
          floor:
            floorBandListData &&
            Object.keys(floorBandListData).length > 0 &&
            floorBandListData.data
              ? floorBandListData.data
              : [],
        },
        refund_staus: refundStaus,
      };

      const dataProcess = {
        project_id: projectId,
        eoi_preference: '1',
        preference_data: preferenceData,
        preference_combination: preferenceCombination,
        project_sfdc_id: projectSfdcId,
        foyrTxnID: '',
      };
      validateLoginBooking(dataProcess);
    } else if (action === '3DView') {
      if (foyrData) {
        if (
          foyrData &&
          foyrData.flattype &&
          foyrData.tower &&
          foyrData.Floor_Band &&
          foyrData.foyrTxnID
        ) {
          const dataProcess = {
            project_id: projectId,
            eoi_preference: '2',
            preference_combination: preferenceCombination,
            project_sfdc_id: projectSfdcId,
            typology: foyrData.flattype ? foyrData.flattype : '',
            tower: foyrData.tower ? foyrData.tower : '',
            floor_band: foyrData.Floor_Band ? foyrData.Floor_Band : '',
            foyrTxnID: foyrData.foyrTxnID ? foyrData.foyrTxnID : '',
          };

          setIsJourneyLoading(true);
          setTimeout(() => {
            setIsJourneyLoading(false);
            validateLoginBookingFOYR(dataProcess);
          }, 600);
        }
      }
    }
  };

  const validateLoginBooking = async (generatedData) => {
    navigation.dispatch(
      StackActions.push('Login', {
        backRoute: 'AppStackRoute',
        action: 'booking',
        screen: 'EOIFloorLayout',
        item: generatedData,
      }),
    );
  };

  const validateLoginBookingFOYR = async (generatedData) => {
    navigation.dispatch(
      StackActions.push('Login', {
        backRoute: 'AppStackRoute',
        action: 'booking_foyr',
        screen: 'EOIFloorLayout',
        item: generatedData,
      }),
    );
  };

  const goToEoiBookingPageFOYR = (foyrData) => {
    if (
      foyrData &&
      foyrData.flattype &&
      foyrData.tower &&
      foyrData.Floor_Band &&
      foyrData.foyrTxnID
    ) {
      navigation.navigate('EOIBookingScreen', {
        project_id: projectId,
        eoi_preference: '2',
        preference_combination: preferenceCombination,
        project_sfdc_id: projectSfdcId,
        typology: foyrData.flattype ? foyrData.flattype : '',
        tower: foyrData.tower ? foyrData.tower : '',
        floor_band: foyrData.Floor_Band ? foyrData.Floor_Band : '',
        foyrTxnID: foyrData.foyrTxnID ? foyrData.foyrTxnID : '',
      });
    } else {
      appSnakBar.onShowSnakBar(
        'Preference selection facility is not available through FOYR',
        'LONG',
      );
    }
  };

  const goToEoiBookingPage = () => {
    let refundStaus = '';

    const typologyListData = eoiMLPData.typologyList;
    const towerListData = eoiMLPData.towerList;
    const floorBandListData = eoiMLPData.floorBandList;

    if (floorBandListData && Object.keys(floorBandListData).length > 0) {
      refundStaus = floorBandListData.refund_staus
        ? floorBandListData.refund_staus
        : '';
    } else {
      if (towerListData && Object.keys(towerListData).length > 0) {
        refundStaus = towerListData.refund_staus
          ? towerListData.refund_staus
          : '';
      } else {
        if (typologyListData && Object.keys(typologyListData).length > 0) {
          refundStaus = typologyListData.refund_staus
            ? typologyListData.refund_staus
            : '';
        } else {
          refundStaus = '';
        }
      }
    }

    const preferenceData = {
      data: {
        typology:
          typologyListData &&
          Object.keys(typologyListData).length > 0 &&
          typologyListData.data
            ? typologyListData.data
            : [],
        tower:
          towerListData &&
          Object.keys(towerListData).length > 0 &&
          towerListData.data
            ? towerListData.data
            : [],
        floor:
          floorBandListData &&
          Object.keys(floorBandListData).length > 0 &&
          floorBandListData.data
            ? floorBandListData.data
            : [],
      },
      refund_staus: refundStaus,
    };

    navigation.navigate('EOIBookingScreen', {
      project_id: projectId,
      eoi_preference: '1',
      preference_data: preferenceData,
      preference_combination: preferenceCombination,
      // preference_combination: 'typology',
      project_sfdc_id: projectSfdcId,
      foyrTxnID: '',
    });
  };

  const goToEoiBookingPageAfterLogin = (data) => {
    setIsJourneyLoading(true);
    setTimeout(() => {
      setIsJourneyLoading(false);
      navigation.navigate('EOIBookingScreen', data);
    }, 800);
  };

  const projectAvailabilityError = () => {
    Alert.alert(
      appConstant.appMessage.NO_BOOKING_AVAILABLE_ALERT_TITLE,
      appConstant.appMessage.NO_BOOKING_AVAILABLE_ALERT_MESSAGE,
      [
        {
          text:
            appConstant.appMessage.NO_BOOKING_AVAILABLE_ALERT_POSITIVE_BUTTON,
          onPress: () => console.log('OK Pressed'),
        },
      ],
      {cancelable: false},
    );
  };

  const onHandleRetryBtn = () => {
    if (deviceUniqueId) {
      getEoiFiltersDataTypology(userId, deviceUniqueId, projectId);
    }
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      {showWebview && foyrUrl ? (
        <WebView
          originWhitelist={['*']}
          source={{
            uri: foyrUrl,
          }}
          style={{flex: 1}}
          scalesPageToFit
          javaScriptEnabledAndroid={true}
          javaScriptEnabled={true}
          // domStorageEnabled={true}
          cacheEnabled={false}
          incognito={true}
          mediaPlaybackRequiresUserAction={true}
          startInLoadingState={true}
          renderLoading={ActivityIndicatorElement}
          onNavigationStateChange={handleWebViewNavigationStateChange}
        />
      ) : null}

      {!showWebview && (
        <AppHeader
          isShowUnitSwitch={false}
          isSqFt={false}
          changeUnit={() => {}}
        />
      )}

      {isLoading && !showWebview ? (
        <View style={styles.container}></View>
      ) : (
        <>
          {!showWebview && (
            <View style={styles.viewContainer}>
              <ScrollView>
                <View style={styles.container}>
                  <AppTextBold style={styles.pageTitle}>
                    FLOOR PLANS &amp; LAYOUTS
                  </AppTextBold>

                  <View style={styles.typologyContainer}>
                    {typologyList && typologyList.length > 0 && (
                      <FlatList
                        showsHorizontalScrollIndicator={false}
                        horizontal
                        data={typologyList}
                        renderItem={({item, index}) => (
                          <EOITypologyList
                            menuItem={item}
                            index={index}
                            color={
                              item.selectedTypology
                                ? item.scssTypology && colors[item.scssTypology]
                                  ? item.scssTypology
                                  : 'secondary'
                                : 'primary'
                            }
                            textColor={
                              item.selectedTypology
                                ? 'primary'
                                : item.scssTypology && colors[item.scssTypology]
                                ? item.scssTypology
                                : 'secondary'
                            }
                            borderColor={
                              item.selectedTypology
                                ? 'primary'
                                : item.scssTypology && colors[item.scssTypology]
                                ? item.scssTypology
                                : 'secondary'
                            }
                            onPress={() => {
                              if (!item.selectedTypology)
                                onSelectTypology(item);
                            }}
                          />
                        )}
                        keyExtractor={(item) => item.id}
                      />
                    )}

                    {!isInnerLoading && getFieldPropertyLayoutPlanImg() ? (
                      <View style={styles.img3d}>
                        <ImageModal
                          resizeMode="contain"
                          style={styles.img3dSource}
                          source={{
                            uri: getFieldPropertyLayoutPlanImg(),
                          }}
                        />
                      </View>
                    ) : null}

                    {!isInnerLoading &&
                    getFieldPropertyLayoutPlanImg() &&
                    foyrUrl ? (
                      <View style={styles.btn3DContainer}>
                        <TouchableOpacity
                          activeOpacity={0.8}
                          style={styles.btn3D}
                          onPress={goToFOYR}>
                          <AppText style={styles.text3d}>3D View</AppText>
                        </TouchableOpacity>
                      </View>
                    ) : null}

                    {!isInnerLoading &&
                      preferenceCombination &&
                      (preferenceCombination.toLowerCase() ===
                        'typology_tower' ||
                        preferenceCombination.toLowerCase() ===
                          'typology_tower_floor') && (
                        <View>
                          {towerList && towerList.length > 0 ? (
                            towerList.map((towerItem, indexTowerItem) => (
                              <View key={indexTowerItem}>
                                {!towerItem.selectedTower && (
                                  <EOITowerList
                                    menuItem={towerItem}
                                    onPress={() => {
                                      if (!towerItem.selectedTower)
                                        onSelectTower(towerItem);
                                    }}
                                  />
                                )}

                                {towerItem.selectedTower && (
                                  <>
                                    {preferenceCombination &&
                                    preferenceCombination.toLowerCase() ===
                                      'typology_tower_floor' ? (
                                      <View style={[styles.tdtContainer]}>
                                        <TouchableWithoutFeedback
                                          onPress={() => {
                                            if (towerItem.selectedTower)
                                              onSelectTower(towerItem);
                                          }}>
                                          <View
                                            style={
                                              styles.tdtHeaderItemContainer
                                            }>
                                            <AppText
                                              style={styles.tdtText}
                                              numberOfLines={1}>
                                              {towerItem.name}
                                            </AppText>

                                            <Image
                                              style={styles.tdtIcon}
                                              source={require('./../assets/images/cancel.png')}
                                            />
                                          </View>
                                        </TouchableWithoutFeedback>

                                        {floorBandList &&
                                        floorBandList.length > 0 ? (
                                          floorBandList.map(
                                            (floorBandItem, indexTowerItem) => (
                                              <EOIInventoryListItem
                                                key={indexTowerItem}
                                                menuItem={floorBandItem}
                                                onPress={() => {
                                                  onSelectFloorBand(
                                                    floorBandItem,
                                                  );
                                                }}
                                                onSelectionPlanImg={() => {
                                                  onSelectionPlanImg(
                                                    floorBandItem,
                                                  );
                                                }}
                                              />
                                            ),
                                          )
                                        ) : (
                                          <AppText style={styles.noDataMsg}>
                                            {isFloorLoading ||
                                            (preferenceCombination &&
                                              preferenceCombination.toLowerCase() ==
                                                'typology_tower')
                                              ? ''
                                              : 'Floor Band Not Found'}
                                          </AppText>
                                        )}
                                      </View>
                                    ) : (
                                      <EOITowerList
                                        menuItem={towerItem}
                                        onPress={() => {
                                          if (towerItem.selectedTower)
                                            onSelectTower(towerItem);
                                        }}
                                        selected={true}
                                      />
                                    )}
                                  </>
                                )}
                              </View>
                            ))
                          ) : (
                            <AppText style={styles.noDataMsg}>
                              {isInnerLoading ||
                              (preferenceCombination &&
                                preferenceCombination.toLowerCase() ==
                                  'typology')
                                ? ''
                                : 'Tower Not Found'}
                            </AppText>
                          )}
                        </View>
                      )}
                  </View>
                </View>
              </ScrollView>
              <View style={styles.btnContainer}>
                <AppButton
                  title={'SCHEDULE A VISIT '}
                  textColor="secondary"
                  color="primary"
                  onPress={goToSiteVisitScreen}
                />
                <AppButton
                  title="PROCEED TO PRE-BOOKING"
                  onPress={goToBookingDetails}
                />
              </View>
            </View>
          )}
        </>
      )}

      {!showWebview && <AppFooter activePage={0} isPostSales={false} />}

      <AppOverlayLoader
        isLoading={
          isLoading || isInnerLoading || isFloorLoading || isJourneyLoading
        }
      />
    </Screen>
  );
};
const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  btnContainer: {
    marginHorizontal: '5%',
    marginBottom: '2%',
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.jaguar,
    textTransform: 'uppercase',
  },
  img3d: {
    width: '100%',
    height: windowHeight / 3.8,
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: '#e1e4e8',
  },
  img3dSource: {
    width: windowWidth * 0.9,
    height: windowHeight / 3.8,
    alignSelf: 'center',
  },

  tdtContainer: {
    alignItems: 'center',
    padding: 20,
    flexDirection: 'column',
    width: '100%',
    marginVertical: 9,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  tdtHeaderItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 12,
  },
  tdtText: {
    flex: 1,
    fontSize:appFonts.largeBold,
    textTransform: 'capitalize',
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  tdtIcon: {
    width: 12,
    height: 12,
  },
  btn3DContainer: {
    marginTop: '5%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  typologyContainer: {
    marginTop: 15,
    justifyContent: 'center',
  },
  btn3D: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 14,
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
    marginVertical: 10,
  },
  text3d: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  typologyContainer: {
    justifyContent: 'center',
  },
  noDataMsg: {
    marginLeft: 20,
  },
});
export default EOIFloorLayout;
