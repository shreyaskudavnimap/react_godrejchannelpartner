
import React, { useState, useEffect } from 'react';
import {
	View,
	StyleSheet,
	Image,
	ScrollView,
	TouchableOpacity
} from 'react-native';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import Orientation from 'react-native-orientation';
import appSnakBar from '../utility/appSnakBar';
import useFileDownload from '../hooks/useFileDownload';
import AppProgressBar from '../components/ui/AppProgressBar';
import { stat } from 'react-native-fs';
import appConstant from '../utility/appConstant';

const INFO = [
	{
		id: 1,
		title: "Account Number",
		data: "5779999"
	},
	{
		id: 2,
		title: "Bank Name",
		data: "HDFC"
	},
	{
		id: 3,
		title: "Primary Account Holder Name",
		data: "Makarand"
	},
	{
		id: 4,
		title: "Account Holder PAN Number",
		data: "AZ36AVH"
	},
	{
		id: 5,
		title: "Transaction ID",
		data: "15723836"
	},
	{
		id: 6,
		title: "Date of Transaction",
		data: "2021-01-15"
	},
	{
		id: 7,
		title: "Amount",
		data: "879766.00"
	},
	{
		id: 8,
		title: "Mobile Number",
		data: "984367490"
	},

]

const StampDutyScreen = ({ navigation, route }) => {

	const routeParams = route.params;
	const page_data = routeParams && routeParams.pageData ? routeParams.pageData : '';
	const [pageData, setPageData] = useState(page_data);

	let documentName = "";
	if (page_data && typeof page_data[0].url !== 'undefined') {
		let fileUrl = decodeURI(page_data[0].url);
		documentName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1);
	}



	useEffect(() => {
		Orientation.lockToPortrait();
		// getFileInfo();
	}, []);

	const getFileInfo = async () => {
		console.log("=======", page_data[0].url);
		const statResult = await stat(page_data[0].url);
		console.log('file size: ' + statResult);
	}

	const { progress, showProgress, error, isSuccess, checkPermission, } = useFileDownload();

	const downloadDocument = () => {
		// const fileName = 'invoice_2018-07-10.pdf';
		let fileName = "";
		let fileUrl = pageData[0].url;
		if (fileName) {
			const ext = fileName.split('.')[0];
			if (ext) {
				downloadFile(fileUrl, fileName, ext);
			}
		} else {
			let fileExt = appConstant.getExtention(fileUrl);

			const fName = appConstant.getFileName(fileUrl);
			if (fileExt[0] && fName) {
				downloadFile(fileUrl, fName, fileExt[0]);
			}
		}
	}

	const downloadFile = async (imgPath, fileName, fileExtension) => {
		try {
			await checkPermission(imgPath, fileName, fileExtension);
		} catch (error) {
			appSnakBar.onShowSnakBar(error.message, 'LONG');
		}
	};

	return (
		<Screen>
			<AppHeader />
			<ScrollView>
				<View style={styles.container}>
					<AppTextBold style={styles.pageTitle}>Stamp Duty &amp; Registration</AppTextBold>
					{(pageData && typeof pageData[0].account_number !== 'undefined') ? (
						<View>
							<AppText style={styles.label}>Account Number</AppText>
							<AppTextBold style={styles.fieldValue}>{pageData[0].account_number}</AppTextBold>
						</View>
					) : null}
					{(pageData && typeof pageData[0].bank_name !== 'undefined') ? (
						<View>
							<AppText style={styles.label}>Bank Name</AppText>
							<AppTextBold style={styles.fieldValue}>{pageData[0].bank_name}</AppTextBold>
						</View>
					) : null}
					{(pageData && typeof pageData[0].account_holder_name !== 'undefined') ? (
						<View>
							<AppText style={styles.label}>Primary Account Holder Name</AppText>
							<AppTextBold style={styles.fieldValue}>{pageData[0].account_holder_name}</AppTextBold>
						</View>
					) : null}
					{(pageData && typeof pageData[0].pan_number !== 'undefined') ? (
						<View>
							<AppText style={styles.label}>Account Holder PAN Number</AppText>
							<AppTextBold style={styles.fieldValue}>{pageData[0].pan_number}</AppTextBold>
						</View>
					) : null}
					{(pageData && typeof pageData[0].transaction_id !== 'undefined') ? (
						<View>
							<AppText style={styles.label}>Transaction ID</AppText>
							<AppTextBold style={styles.fieldValue}>{pageData[0].transaction_id}</AppTextBold>
						</View>
					) : null}
					{(pageData && typeof pageData[0].transaction_date !== 'undefined') ? (
						<View>
							<AppText style={styles.label}>Date of Transaction</AppText>
							<AppTextBold style={styles.fieldValue}>{pageData[0].transaction_date}</AppTextBold>
						</View>
					) : null}
					{(pageData && typeof pageData[0].amount !== 'undefined') ? (
						<View>
							<AppText style={styles.label}>Amount</AppText>
							<AppTextBold style={styles.fieldValue}>{pageData[0].amount}</AppTextBold>
						</View>
					) : null}
					{(pageData && typeof pageData[0].mobile_number !== 'undefined') ? (
						<View>
							<AppText style={styles.label}>Mobile Number</AppText>
							<AppTextBold style={styles.fieldValue}>{pageData[0].mobile_number}</AppTextBold>
						</View>
					) : null}
					<TouchableOpacity
						onPress={() => {
							downloadDocument();
						}}>
						<View style={styles.downloadBlock}>
							<View style={styles.imgContainer}>
								<Image
									style={styles.icon}
									source={require('../assets/images/pdf_icon.png')} />
								<AppText style={styles.pdfInfo}>{documentName} {" "}
									{/* <AppText style={styles.size}>250kb</AppText> */}
								</AppText>
							</View>
							<Image style={styles.downloadIcon}
								source={require('../assets/images/download-b.png')}
								resizeMode="contain" />
						</View>
					</TouchableOpacity>
				</View>
			</ScrollView>
			<AppFooter activePage={0} isPostSales={true} />
			{showProgress && progress && progress > 0 ? (
				<AppProgressBar progress={progress} />
			) : null}
		</Screen>
	);
};
const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: 20,
		paddingVertical: 10,
		marginBottom: 10
	},
	pageTitle: {
		fontSize: appFonts.largeBold,
		fontFamily: appFonts.SourceSansProBold,
		color: colors.jaguar,
		// textTransform: 'uppercase',
		marginBottom: 30
	},
	label: {
		fontSize: appFonts.largeFontSize,
		marginBottom: -5
	},
	fieldValue: {
		marginBottom: 20,
		fontSize: appFonts.largeFontSize,
	},
	icon: {
		height: 30,
		width: 35
	},
	imgContainer: {
		flexDirection: "row",
		alignItems: "center"
	},
	pdfInfo: {
		// flexShrink: 1,
		width: "80%",
		marginHorizontal: 10,
		justifyContent: 'space-between',
	},
	size: {
		color: colors.gray,
		// width: "15%"
		paddingLeft: 15,
		marginLeft: 10
	},
	downloadIcon: {
		width: '5%',
		alignSelf: 'baseline',
		// marginRight: 25,
		marginBottom: 10
	},
	downloadBlock: {
		flex: 1,
		flexDirection: "row",
		alignItems: "center",
		paddingRight: 15

	}
})
export default StampDutyScreen;