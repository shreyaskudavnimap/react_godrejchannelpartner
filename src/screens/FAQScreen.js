import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import {
  View,
  Image,
  StyleSheet,
  ScrollView,
  TextInput,
  TouchableWithoutFeedback,
  TouchableOpacity
} from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { useNavigation } from '@react-navigation/native';
//api
import FAQ_API from '../api/apiFaq';
//components
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppFooter from '../components/ui/AppFooter';
import AppText from '../components/ui/ AppText';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import PowerOfAttornyAndDraft from '../components/PowerOfAttornyAndDraftModal';
import DropDownPicker from 'react-native-dropdown-picker';
import Orientation from 'react-native-orientation';

const ExpandQuestion = (props) => {
  const deepLinking = {
    dashboard: { screen: 'DashboardScreen', param: {} },
    my_property_overview: { screen: 'MyAccoutScreen', param: { isPropertyOverView: true } },
    pay_now: { screen: 'PayNowScreen', param: {} },
    my_property_account: { screen: 'MyAccoutScreen', param: {} },
    my_journey_registration: { screen: 'MyAccoutScreen', param: { isJourney: true } },
    enquire_for_home_loan: { screen: 'LoanEnquiryScreen', param: {} },
    loan_approval_details: { screen: 'LoanEnquiryScreen', param: { activeTab: 'LoanDetails' } },
    wishlist_project: { screen: 'WishlistScreen', param: {} },
    wishlist_inventory: { screen: 'WishlistScreen', param: { isInventory: true } },
    notification_my_task: { screen: 'NotificationsScreen', param: {} },
    notification_my_updates: { screen: 'NotificationsScreen', param: { isMyUpdate: true } },
    schedule_a_visit_registration: { screen: 'VisitScreen', param: {} },
    my_documents: { screen: 'DocumentScreen', param: {} },
    my_profile: { screen: 'Profile', param: {} },
    testimonials: { screen: 'Testimonial', param: {} },
    my_service_request: { screen: 'ServiceRequest', param: { flatCode: props.flatCode } },
    support: { screen: 'ReachUs', param: {} },
    construction_status: { screen: 'ConstructionStatus', param: {} },
    pay_now: { screen: 'PayNowScreen', param: {} },
    godrej_home: { screen: 'GodrejHome', param: {} },
    menu_selection: { screen: 'UserDetailsScreen', param: {} },
    draft_agreement: { screen: 'draft_agreement', param: {} },
    power_of_attorney: { screen: 'power_of_attorney', param: {} }
  }
  const [isClicked, setClicked] = useState(false);
  const formattedAnswer = props.answer.split('##');
  const { currentPropertyBookingId, setIsPowerOfAttornyDraftModalOpen, setIsPowerOfAttorny } = props
  const navigation = useNavigation();

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);
  /**
   * 
   * @description Check is link valid or not
   */
  const isLinkValid = () => {
    if (formattedAnswer.length > 1) {
      const linkArray = formattedAnswer[1].split('@@');
      if (deepLinking[linkArray[0]]) {
        return true;
      }
      return false
    }
    return false;
  }
  const link = () => {
    if (formattedAnswer.length > 1) {
      const linkArray = formattedAnswer[1].split('@@');
      const navigate = () => {
        if (deepLinking[linkArray[0]].screen == 'draft_agreement' || deepLinking[linkArray[0]].screen == 'power_of_attorney') {
          if (deepLinking[linkArray[0]].screen == 'draft_agreement') {
            setIsPowerOfAttorny(false);
          }
          else {
            setIsPowerOfAttorny(true);
          }
          setIsPowerOfAttornyDraftModalOpen(true);
        }
        else {
          navigation.navigate(deepLinking[linkArray[0]].screen, { ...deepLinking[linkArray[0]].param, bookingId: currentPropertyBookingId })
        }

      }
      if (deepLinking[linkArray[0]]) {
        return (
          <>
            {<TouchableWithoutFeedback onPress={() => navigate()}>
              <AppText style={styles.linkStyle}>
                {
                  linkArray.length > 0 ? linkArray[1] : null
                }
              </AppText>
            </TouchableWithoutFeedback>}
          </>
        )
      }
      else {
        return formattedAnswer[1];
      }

    }
    else {
      return null
    }
  }
  return (
    <View key={props.id} style={styles.buttonConatainer}>
      <TouchableWithoutFeedback onPress={() => setClicked(!isClicked)}>
        <View style={styles.button}>
          <View style={{ flex: 0.9 }}>
            <AppText style={styles.buttonText}>{props.question}</AppText>
          </View>
          <View style={styles.iconCon}>
            <Image
              source={
                isClicked === true
                  ? require('./../assets/images/down-icon-b.png')
                  : require('./../assets/images/arrow-back.png')
              }
              style={styles.arrow}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
      {isClicked && (
        <View style={styles.button}>
          {isLinkValid() ? <AppText style={[styles.buttonText, { color: colors.gray }]}>
            {/* {props.answer.replace("Please", <AppText>abc</AppText>)} */}
            {formattedAnswer.length > 0 && formattedAnswer[0]}
            {link()}
            {formattedAnswer.length > 2 && formattedAnswer[2]}
          </AppText> : <AppText style={[styles.buttonText, { color: colors.gray }]}>{props.answer}</AppText>}
        </View>
      )}
    </View>
  );
};


const FAQScreen = ({ navigation, route }) => {
  const { loginInfo } = useSelector((state) => state);
  const userId = loginInfo && loginInfo.loginResponse && loginInfo.loginResponse.data && loginInfo.loginResponse.data.userid ? loginInfo.loginResponse.data.userid : '';
  const [propertyWithQuestions, setPropertyWithQuestions] = useState({});
  const routeParams = route.params;
  // let booking_id = routeParams && routeParams.bookingId ? routeParams.bookingId : 0;
  const [propertyIndex, setPropertyIndex] = useState(0);
  const [questionTypeIndex, setQuestionTypeIndex] = useState(0);
  const [currentQuestionAnswer, setCurrentQuestionAnswer] = useState([]);
  const [questionTypeList, setQuestionTypeList] = useState({});
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [originalCurrentQuestionAnswer, setOriginalCurrentQuestionAnswer] = useState([]);
  const dashboardData = useSelector((state) => state.dashboardData);
  const propertyData = dashboardData.rmData;
  const [currentPropertyBookingId, setCurrentPropertyBookingId] = useState(propertyData.length ? propertyData[0].booking_id : null);
  const [isPowerOfAttornyDraftModalOpen, setIsPowerOfAttornyDraftModalOpen] = useState(false);
  const [isPowerOfAttorny, setIsPowerOfAttorny] = useState(false);

  const dropDownPropertyData = propertyData.map((item) => {
    return {
      label: `${item.property_name}/${item.inv_flat_code}`,
      value: item
    }
  })
  useEffect(() => {
    getFaq();
  }, [])

  useEffect(() => {
    getQuestions()
  }, [questionTypeList, questionTypeIndex, currentPropertyBookingId])
  /**
   * 
   * @description Fetch Faq detail
   */
  const getFaq = () => {
    setIsShowLoader(true)
    FAQ_API.apiGetFaq({ user_id: userId }).then((response) => {
      setIsShowLoader(false)
      if (response.data) {
        const data = response.data.data
        const keys = Object.keys(data);
        const newPropertyWithQuestions = { ...propertyWithQuestions };
        const newQuestionTypeList = { ...questionTypeList };
        keys.forEach((key) => {
          newPropertyWithQuestions[key] = data[key].tab_content;
          if (data[key].tab_content && data[key].tab_content.length) {
            newQuestionTypeList[key] = Object.keys(data[key].tab_content[0]).map((elem) => {
              return {
                label: elem,
                value: elem
              }
            })
            newQuestionTypeList[key].unshift({ label: 'All Question', value: 'All Question' })
          }
        })
        setPropertyWithQuestions(newPropertyWithQuestions);
        setQuestionTypeList(newQuestionTypeList);
      }

    })
  }
  /**
   * 
   * @param {object} itemValue 
   * @param {number} itemIndex 
   * @description On change of property this function is called
   */
  const onHandlePropertyChange = (itemValue, itemIndex) => {
    setCurrentPropertyBookingId(itemValue.value.booking_id)
    setPropertyIndex(itemIndex);
    //getQuestions()
  }
  /**
   * 
   * @param {object} itemValue 
   * @param {number} itemIndex 
   * @description On question type dropdown change this function is called
   */
  const onHandleQusetionTypeChange = (itemValue, itemIndex) => {
    setQuestionTypeIndex(itemIndex);
  }

  /**
   * 
   * @description Create list of questions for different question type
   */
  const getQuestions = () => {
    if (propertyWithQuestions && propertyWithQuestions[currentPropertyBookingId] && propertyWithQuestions[currentPropertyBookingId][0]) {
      let questionAnswerList = []
      const currentQuestionType = questionTypeList[currentPropertyBookingId][questionTypeIndex].value;
      const questionAnwer = propertyWithQuestions[currentPropertyBookingId][0];
      if (currentQuestionType == 'All Question' && propertyWithQuestions[currentPropertyBookingId]) {
        const keys = Object.keys(questionAnwer);
        keys.forEach((key) => {
          questionAnswerList.push(...questionAnwer[key])
        })
      }
      else {
        questionAnswerList = questionAnwer[currentQuestionType]
      }
      setCurrentQuestionAnswer(questionAnswerList);
      setOriginalCurrentQuestionAnswer([...questionAnswerList]);
    }
  }

  /**
   * 
   * @description Search question answers
   */
  const search = (text) => {
    const callBack = (elem) => {
      return elem.question.toLowerCase().includes(text.toLowerCase());
    }
    if (!text) {
      setCurrentQuestionAnswer([...originalCurrentQuestionAnswer])
    }
    else {

      const filteredQuestionAnswer = originalCurrentQuestionAnswer.filter(callBack)
      setCurrentQuestionAnswer(filteredQuestionAnswer);
    }

  }
  /**
   * 
   * @description Render property
   */
  const renderPropertyList = () => {
    return (
      <View style={styles.dropDownContainer}>

        {
          propertyData.length > 0 && (
            <View>
              {
                propertyData.length > 1 ? <View style={styles.itemContainer}>
                  <View style={{ flex: 1 }}>
                   
                    <DropDownPicker
                      defaultValue={propertyData[propertyIndex]}
                      items={dropDownPropertyData}
                      style={styles.dropdownDocument}
                      placeholderStyle={styles.dropdownPlaceholder}
                      itemStyle={styles.dropDownItem}
                      labelStyle={styles.label}
                      onChangeItem={onHandlePropertyChange}
                    //placeholder="Godrej Meridian"
                    />
                  </View>
                </View> : <View>
                    {
                      <AppText style={styles.propertyStyle}>
                        {`${propertyData[0].property_name}/${propertyData[0].inv_flat_code}`}
                      </AppText>
                    }
                  </View>
              }

            </View>
          )
        }

      </View>
    )
  }
  const onHandleRetryBtn = () => {
    getFaq();
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />
      <View style={{flex:1}}>
      <ScrollView >
        
        <View style={styles.container}>
          <AppText style={styles.pageTitle}>FAQs</AppText>
          {
            renderPropertyList()
          }
          <View style={styles.searchContainer}>
            {/* <Image
              source={require('./../assets/images/icon-search.png')}
              style={styles.searchIcon}
            /> */}
            <TextInput
              style={styles.searchInput}
              placeholder="Search"
              onChangeText={(text) => search(text)}
            />
          </View>

          {Object.keys(questionTypeList).length > 0 && <View style={{ flex: 1, marginBottom: 10, ...styles.dropDownContainer }}>
           

            <DropDownPicker
              defaultValue={currentPropertyBookingId && questionTypeList[currentPropertyBookingId] ? questionTypeList[currentPropertyBookingId][questionTypeIndex].value : null}
              items={questionTypeList[currentPropertyBookingId] ? questionTypeList[currentPropertyBookingId] : []}
              style={styles.dropdownDocument}
              placeholderStyle={styles.dropdownPlaceholder}
              itemStyle={styles.dropDownItem}
              labelStyle={styles.label}
              onChangeItem={onHandleQusetionTypeChange}
            />

          </View>}
          <View style={{ zIndex: 30}}>  
            <View style={{paddingBottom:!currentQuestionAnswer.length || currentQuestionAnswer.length==1 
              ?'35%':'0%'}}>
            {currentQuestionAnswer.map((data, index) => (
              <ExpandQuestion
                id={data.id}
                key={data.id}
                question={data.question}
                answer={data.answer}
                currentPropertyBookingId={currentPropertyBookingId}
                setIsPowerOfAttornyDraftModalOpen={setIsPowerOfAttornyDraftModalOpen}
                setIsPowerOfAttorny={setIsPowerOfAttorny}
                flatCode={propertyData.length ? propertyData[propertyIndex].inv_flat_code : null}
              />
            ))}
          </View>
          </View>
        </View>
        {<AppOverlayLoader isLoading={isShowLoader} />}
        {isPowerOfAttornyDraftModalOpen && <PowerOfAttornyAndDraft isPowerOfAttornyDraftModalOpen={isPowerOfAttornyDraftModalOpen} bookingId={currentPropertyBookingId} setIsShowLoader={setIsShowLoader} setIsPowerOfAttornyDraftModalOpen={setIsPowerOfAttornyDraftModalOpen} isPowerOfAttorny={isPowerOfAttorny}></PowerOfAttornyAndDraft>}
      
      </ScrollView>
      </View>         
      <AppFooter activePage={5} isPostSales={true} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '6%',
    justifyContent: 'center',
    marginBottom: 20,
  },
  pageTitle: {
    paddingVertical: '5%',
    fontSize: appFonts.xxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
  },
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
    //borderBottomWidth: 2,
    //borderBottomColor: colors.gray4,
  },
  searchIcon: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  searchContainer: {
    marginVertical: 20,
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: colors.LynxWhite,
  },
  buttonConatainer: {
    marginBottom: 20,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
    zIndex: 10

  },
  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  buttonText: {
    fontSize:appFonts.largeBold,
    color: colors.secondary,
  },
  arrow: {
    width: 12,
    height: 16,
  },
  searchInput: {
    flex: 1,
    fontSize: appFonts.xlargeFontSize,
  },
  iconCon: {
    flex: 0.1,
    alignItems: 'center',
  },
  propertyStyle: {
    color: colors.secondary,
    fontFamily: appFonts.SourceSansProRegular,
  },
  linkStyle: { color: colors.charcoal, fontWeight: 'bold' },
  label: {
    fontSize:appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
    justifyContent: "center", alignItems: "center",
    marginBottom: '5%'
  },
  dropdownDocument: {
    height: '100%',
    width: '100%',
    marginBottom: '5%',
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderTopWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 999
  },
  dropdownPlaceholder: {
    fontSize:appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  dropDownItem: {
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGray,
    // marginTop: '5%',
    justifyContent: "center", alignItems: "center"
  },
  dropDownContainer: {
    ...Platform.select({ ios: { zIndex: 1000 } })
  }
});

export default FAQScreen;
