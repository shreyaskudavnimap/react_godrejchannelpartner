import React, { useEffect, useState } from 'react';
import {
  View, StyleSheet, ScrollView, TextInput,
  Dimensions, Image, Text, TouchableOpacity, PermissionsAndroid
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import AppButton from '../components/ui/AppButton';
import AppHeader from '../components/ui/AppHeader';
import AppTextBold from '../components/ui/AppTextBold';
import AppText from '../components/ui/ AppText';
import Screen from '../components/Screen';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppFooter from '../components/ui/AppFooter';
import * as normStyle from '../styles/StyleSize';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import apiLogin from './../api/apiLogin';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import appSnakBar from '../utility/appSnakBar';
import { useNavigation } from '@react-navigation/native';
import Orientation from 'react-native-orientation';

const { height } = Dimensions.get('window');



const ChangeEmailId = ({ props, route }) => {

  const routeParams = route.params;
  const emailid =
    routeParams && routeParams.emailid && routeParams.emailid
      ? routeParams.emailid
      : '';

  const userId =
    routeParams && routeParams.userId && routeParams.userId
      ? routeParams.userId
      : '';

  const userName =
    routeParams && routeParams.userName && routeParams.userName
      ? routeParams.userName
      : '';

  const [emailId, SetEmailId] = useState("");
  const [isApiLoading, setApiIsLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [disableButton, setDisableButton] = useState(true);
  const navigation = useNavigation();

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);


  useEffect(() => {
    _getUserInfo();
  }, [loginvalue]);

  const loginvalue = useSelector(
    (state) => state.loginInfo.loginResponse,
  );

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );
  const menuType = useSelector((state) => state.menuType.menuType);
  const goBack = () => {

    navigation.goBack();
  };

  const _getUserInfo = () => {
    SetEmailId(emailid);
  };

  const validateEmail = (email) => {
    var patt = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
    var res = patt.test(email);
    console.log(res);
    if (res) {
      return res;
    } else {
      appSnakBar.onShowSnakBar(`Enter valid Email-Id`, 'LONG');
    }
  };

  const _changeEmailId = () => {
    if (emailId == '') {
      appSnakBar.onShowSnakBar(`Enter Email-id`, 'LONG');
      return null;
    }
    if (emailid == emailId) {
      return null;
    } else {
      if (validateEmail(emailId.trim())) {
        navigation.navigate('OtpForEmailScreen',
          {
            userId: userId,
            mobileNo: emailId,
            actionType: 'update_email_id',
            oldMobileValue: emailid,
            userName: userName,
          }
        );
      }
    }
  };

  const _onChangeEnail = (e) => {
    SetEmailId(e);
    if (emailid == e) {
      setDisableButton(true);
    } else {
      setDisableButton(false);
    }
  };


const onHandleRetryBtn = () => {
};

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />

    <ScrollView>
      <View style={styles.container}>

        <View style={{ width: '100%', alignItems: 'flex-start' }}>
          <AppText style={styles.pageTitle}>Edit email-id</AppText>
        </View>


        <View style={{ width: '100%', alignItems: 'flex-start' }}>
          <AppText style={{ marginLeft: '5%', color: colors.gray }}>Email-Id *</AppText>
        </View>
        <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
          <TextInput
            style={styles.nameStyle}
            placeholder={"enter email-id"}
            placeholderTextColor={colors.gray}
            value={emailId}
            color={colors.secondaryDark}
            onChangeText={e => {
              _onChangeEnail(e);
            }}

          />
        </View>

        {/* <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}> */}
          <View style={styles.button}>
            <AppButton
             color={disableButton ? 'charcoal' : 'secondary'}
             disabled={disableButton}
             textColor = {disableButton ? 'primary' : 'primary'}
              title="Submit"
              onPress={() => { _changeEmailId(); }}
            />
          </View>
        {/* </View> */}

      </View>
    </ScrollView>
      <AppFooter activePage={0} isPostSales={menuType == 'postsale' ? true : false} />
      <AppOverlayLoader isLoading={isLoading || isApiLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  pageTitle: {
    fontSize: normStyle.normalizeWidth(20),
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginLeft: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(40),
  },
  nameStyle: {
    width: '90%', height: normStyle.normalizeWidth(50),
    borderBottomWidth: 0.5, fontSize: normStyle.normalizeWidth(18),
    fontWeight: 'bold',
  },
  editImg: {
    width: normStyle.normalizeWidth(20),
    height: normStyle.normalizeWidth(20),
    marginLeft: normStyle.normalizeWidth(10),
  },
  cameraStyle: {
    width: normStyle.normalizeWidth(25),
    height: normStyle.normalizeWidth(25),
  },
  button: {
     // width: normStyle.normalizeWidth(100),
    // height: normStyle.normalizeWidth(0),
    alignItems: 'center', justifyContent: 'center',
    marginHorizontal:15,
    paddingBottom: normStyle.normalizeWidth(40),
    paddingTop: normStyle.normalizeWidth(40),
  }
});

export default ChangeEmailId;
