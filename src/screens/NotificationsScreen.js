import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Text,
  Image,
  Dimensions,
} from 'react-native';
import { useSelector } from 'react-redux';
//api
import NOTIFICATION_API from '../api/apiNotifications';
//components
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import NotificationsComponent from '../components/NotificationsComponent';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import appFonts from '../config/appFonts';
import colors from '../config/colors';

import Orientation from 'react-native-orientation';
import { EventRegister } from 'react-native-event-listeners';

const windowWidth = Dimensions.get('window').width;
const NotificationsScreen = ({ navigation, route }) => {
  /**
   *
   * @constant {boolean} isInventory It is come from FAQ screen to default selection of inventory
   *
   */
  // const isMyUpdate = route.params
  //   ? route.params.isMyUpdate
  //     ? true
  //     : false
  //   : false;

  const isMyUpdate = false;
  const [myPlans, setMyPlans] = useState(isMyUpdate ? false : true);
  const [myTasks, setMyTasks] = useState([]);
  const [myUpdate, setMyUpdate] = useState([]);
  const [selectedBtn, setSelectedBtn] = useState(
    isMyUpdate ? 'myupdate' : 'myplans',
  );
  const loginvalue = useSelector((state) => state.loginInfo.loginResponse);
  const userId =
    typeof loginvalue.data !== 'undefined' ? loginvalue.data.userid : '';
  const [isMyTaskArrayLength, setIsMyTaskArrayLength] = useState(true);
  const [isMyupdateArrayLength, setIsMyupdateArrayLength] = useState(true);
  const [isShowLoader, setIsShowLoader] = useState(true);
  const menuType = useSelector((state) => state.menuType.menuType);
  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
    if (isMyUpdate) {
      setNotification('General');
    } else {
      setNotification('Tasks');
    }
  }, []);

  const notificationType = {
    Tasks: setMyTasks,
    General: setMyUpdate,
  };
  /**
   *
   * @param {string} type
   * @description Set the task and general notifications
   */
  const setNotification = (type) => {
    setIsShowLoader(true);
    NOTIFICATION_API.apiNotificationListItems({
      user_id: userId,
      type: type,
      page: 1,
    }).then((response) => {
      setIsShowLoader(false);
      if (response.data && response.data.data) {
        if (type === 'Tasks') {
          if (!response.data.data.length) {
            setIsMyTaskArrayLength(false);
          }
        }
        if (type === 'General') {
          if (!response.data.data.length) {
            setIsMyupdateArrayLength(false);
          }
        }
        notificationType[type](response.data.data);
      }
    });
    NOTIFICATION_API.notificationCount({ user_id: userId }).then((data) => {
      // console.log('daaddaaddaa', data);
      if (data && data.data && data.data.status && data.data.status == '200') {
        EventRegister.emit('wishlistUpdateEvent', 'update');
      }
    });
  };
  const noDataFoundText = () => {
    if (myPlans) {
      if (!myTasks.length && !isMyTaskArrayLength) {
        return <Text>No task found</Text>;
      }
    } else {
      if (!myUpdate.length && !isMyupdateArrayLength) {
        return <Text>No updates found</Text>;
      }
    }
  };
  const onPressMyPlans = () => {
    setMyPlans(true);
    setSelectedBtn('myplans');
    setNotification('Tasks');
  };
  const onPressMyUpdates = () => {
    setMyPlans(false);
    setSelectedBtn('myupdate');
    setNotification('General');
  };

  const readNotification = (notificationId) => {
    const param = {
      user_id: userId,
      notification_id: notificationId,
    };
    NOTIFICATION_API.readNotification(param).then((data) => {
      // console.log('daaddaaddaa', data);
      if (data && data.data && data.data.status && data.data.status == '200') {
        EventRegister.emit('wishlistUpdateEvent', 'update');
      }
    });
  };

  const onHandleRetryBtn = () => {
    if (isMyUpdate) {
      setNotification('General');
    } else {
      setNotification('Tasks');
    }
  };
  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />

      <View style={styles.container}>
        <AppTextBold style={styles.pageTitle}>NOTIFICATIONS</AppTextBold>
        <View
          style={{
            // width: '65%',
            width: windowWidth * 0.35,
            paddingVertical: 12,
            flexDirection: 'row',
            justifyContent: 'flex-start',
          }}>
          {/* <TouchableOpacity
            style={
              selectedBtn == 'myplans' ? styles.btnPress : styles.btnNormal
            }
            onPress={onPressMyPlans}>
            <AppTextBold
              style={
                selectedBtn == 'myplans'
                  ? styles.activeTab
                  : styles.inactiveTab
              }
              textBreakStrategy="simple"
              ellipsizeMode="tail">
              My Tasks
            </AppTextBold>
          </TouchableOpacity>
          <TouchableOpacity
            style={
              selectedBtn === 'myupdate' ? styles.btnPress : styles.btnNormal
            }
            onPress={onPressMyUpdates}>
            <AppTextBold
              style={
                selectedBtn == 'myupdate'
                  ? styles.activeTab
                  : styles.inactiveTab
              }
              textBreakStrategy="simple"
              ellipsizeMode="tail">
              My Updates
            </AppTextBold>
          </TouchableOpacity> */}

        </View>
        {!isShowLoader ? (
          <View>
            {noDataFoundText()}
            <FlatList
              data={myPlans ? myTasks : myUpdate}
              renderItem={({ item }) => (
                <NotificationsComponent
                  readNotification={readNotification}
                  item={item}
                />
              )}
            />
          </View>
        ) : null}
      </View>
      <AppFooter isPostSales={menuType == 'postsale' ? true : false} />

      <AppOverlayLoader isLoading={isShowLoader} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 18,
    paddingVertical: 10,
    marginLeft: 10,
    marginBottom: 50,
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: '7%',
    textTransform: 'uppercase',
  },
  btnNormal: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 13,
    paddingVertical: 18,
    width: '100%',
    marginVertical: 9,
    marginRight: 15,
    borderRadius: 1,
    borderColor: colors.lightGray,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 5,
    elevation: 10,
    backgroundColor: colors.primary,
  },
  btnPress: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 13,
    paddingVertical: 18,
    width: '100%',
    marginVertical: 9,
    marginRight: 15,
    borderRadius: 1,
    backgroundColor: colors.jaguar,
  },
  activeTab: {
    color: colors.primary,
    fontSize: appFonts.largeBold,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inactiveTab: {
    color: colors.secondary,
    fontSize: appFonts.largeBold,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  }
});

export default NotificationsScreen;
