import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Modal,
  Text,
  TextInput,
  FlatList,
  Dimensions,
  Image,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
} from 'react-native';
import Screen from '../components/Screen';
import AppButton from '../components/ui/AppButton';
import AppTextBold from '../components/ui/AppTextBold';
import appFonts from '../config/appFonts';
import AppHeader from '../components/ui/AppHeader';
import {RadioButton} from 'react-native-paper';
import colors from '../config/colors';
import * as normStyle from '../styles/StyleSize';
import appSnakBar from '../utility/appSnakBar';
import apiLogin from './../api/apiLogin';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import {useNavigation} from '@react-navigation/native';
import ShowLoader from '../components/ui/ShowLoader';
import {useDispatch, useSelector} from 'react-redux';
import CountrySelectModalScreen from './modals/CountrySelectModalScreen';
import Orientation from 'react-native-orientation';

var {height, width} = Dimensions.get('window');

let DATA = [
  // {
  //     id: '1',
  //     number: '9999999999',
  // },
  // {
  //     id: '2',
  //     number: '9876543210',
  // },
];

const ForgotPassword = ({props, route}) => {
  const routeParams = route.params;
  const actionType =
    routeParams && routeParams.actionType && routeParams.actionType
      ? routeParams.actionType
      : '';

  const [existingCustomer, setexistingCustomer] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [isCountrySelected, setCountry] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isApiLoading, setApiIsLoading] = useState(false);
  const [error, setError] = useState();
  const [countryName, setCountryName] = useState('IND');
  const [countryId, setCountryId] = useState('+91');
  const [countryFlag, setFlag] = useState(
    'https://www.countryflags.io/in/flat/64.png',
  );
  const [checkedMobile, setCheckedMobile] = useState('first');
  const [mobilNumber, SetMobileNumber] = useState('');
  const [panNumber, SetPanNumber] = useState('');
  const [dynamicActionType, SetDynamicActionType] = useState('');
  const [showCountryModal, setShowCountryModal] = useState(false);
  const userType = useRef('');
  const activationIdForPAN = useRef('');
  const navigation = useNavigation();

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  useEffect(() => {
    SetDynamicActionType('exist_cust_fotget_pwd');
    console.log('countryId = ' + countryId);
  }, []);

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );

  const onSelectCountry = (selectedCountry) => {
    console.log(selectedCountry.callingCodes);
    setCountryName(selectedCountry.alpha3Code);
    setCountryId(selectedCountry.callingCodes);
    setFlag(selectedCountry.flag);
    setShowCountryModal(false);
};

  const newUserMobileNo = (mobile_no) => {
    userType.current = 'new_user';
    SetMobileNumber(mobile_no);
  };

  const _existingUser = (pan_no) => {
    userType.current = 'existing_type';
    SetPanNumber(pan_no);
    if (pan_no.length == 10) {
      Keyboard.dismiss();
      var patt = new RegExp('^([A-Z]){5}([0-9]){4}([A-Z]){1}?$');
      var res = patt.test(pan_no);
      if (res) {
        setCheckedMobile('first'); //unselect the previous selected number in pop-up
        getLookupRecord(pan_no);
      } else {
        appSnakBar.onShowSnakBar('Invalid PAN number', 'LONG');
      }
    }
  };

  const getLookupRecord = async (pan_no) => {
    console.log(pan_no);
    const request = {
      pan: pan_no,
    };
    setIsLoading(true);
    const result = await apiLogin.apiLandingLookupUserByPan(request);
    setIsLoading(false);
    var status = result.data.status;
    console.log(JSON.stringify(result.data));
    if (status == 200) {
      _sendOtpByPan(pan_no);
    }
    if (status == 201) {
      getMobileNoList(result.data.data);
    }
    if (status != 201 && status != 200) {
      appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
    }
  };

  const getMobileNoList = (mobileData) => {
    console.log('abcd = ' + JSON.stringify(mobileData));
    if (mobileData.length > 0 && mobileData != '') {
      let tempArr = [];
      DATA = [];
      for (let i = 0; i < mobileData.length; i++) {
        var temp = mobileData[i].split('_');
        tempArr.push({id: temp[1], number: temp[0].replace('+91', '')});
      }
      // setArrMobileNo(tempArr); not setting array using useState

      /*      REMOVING DUPLICATES      */
      for (let i = 0; i < tempArr.length; i++) {
        let isFirst = true;
        var a = tempArr[i].number;
        for (let j = 0; j < tempArr.length; j++) {
          var b = tempArr[j].number;
          if (a == b) {
            if (!isFirst) {
              console.log('j = ' + j);
              tempArr.splice(j, 1);
            }
            isFirst = false;
          }
        }
      }
      /*   END REMOVING DUPLICATES   */
      DATA = tempArr;
      setModalVisible(true);
    }
  };

  const _selectMobile = (number, id) => {
    setCheckedMobile(number);
    activationIdForPAN.current = id;
  };

  const _goToOtpScreen = () => {
    setModalVisible(!modalVisible);
    if (checkedMobile == 'first') {
      setTimeout(() => {
        appSnakBar.onShowSnakBar('Select Mobile Number', 'LONG');
      }, 300);
    }
    if (checkedMobile != '' && checkedMobile != 'first') {
      navigation.navigate('Otp', {
        mobileNo: checkedMobile,
        firstName: activationIdForPAN.current, //firstName used for hold sfid in otp screen
        actionType: 'forget_pwd_by_exist_cust_multiMob',
      });
    }
  };

  const _sendOtpByPan = async (pan_no) => {
    const request = {
      pan: pan_no,
      device_id: deviceUniqueId,
    };
    console.log(JSON.stringify(request));
    setIsLoading(true);
    const result = await apiLogin.apiForForgetPwdByPan(request);
    setIsLoading(false);
    var status = result.data.status;
    console.log(JSON.stringify(result.data));
    if (status == 200) {
      navigation.navigate('Otp', {
        userId: result.data.user_id,
        actionType: 'forget_pwd_by_exist_cust',
        panNo: pan_no,
      });
    } else {
      appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
    }
  };

  const _sendOtp = () => {
    if (userType.current == '') {
      appSnakBar.onShowSnakBar('Enter Mobile Number', 'LONG');
      return null;
    }
    if (userType.current != '' && userType.current == 'new_user') {
      _startNewUserProcess(); //alert('new');
    }
    // if(userType.current != '' && userType.current == 'existing_type'){
    //     alert('exist');
    // }
  };

  const _startNewUserProcess = async () => {
    //alert(mobilNumber.length);return null;
    if (!mobilNumber.trim()) {
      appSnakBar.onShowSnakBar('Enter Mobile Number', 'LONG');
      return null;
    }
    if (countryId == '+91') {
      if (mobilNumber.length > 10 || mobilNumber.length < 10) {
        console.log();
        appSnakBar.onShowSnakBar(
          'Mobile Number length must be 10 digits',
          'LONG',
        );
        return null;
      }
    }
    const request = {
      mobile: countryId + mobilNumber,
    };
    setIsLoading(true);
    const result = await apiLogin.apiForlookupByMobile(request);
    //setIsLoading(false);
    var status = result.data.status;
    console.log(JSON.stringify(result.data));
    if (status == 200 || status == 201) {
      _runForgetPwdAPI();
    } else {
      setIsLoading(false);
      appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
    }
  };

  const _runForgetPwdAPI = async () => {
    //setIsLoading(true);device_id: deviceId,
    const request = {
      mobile: countryId + mobilNumber,
      device_id: deviceUniqueId,
    };
    const result = await apiLogin.apiForForgetPwdByMobile(request);
    setIsLoading(false);
    var status = result.data.status;
    //status = 200; /* testing purpose */
    if (status == 200) {
      var userId = result.data.user_id;
      navigation.navigate('Otp', {
        mobileNo: countryId + mobilNumber,
        userId: userId,
        actionType: 'forget_password',
      });
    } else {
      appSnakBar.onShowSnakBar('No record found', 'LONG');
    }
  };

  const _existingCustomer = () => {
    alert(panNumber);
  };

  return (
    <Screen>
      <AppHeader />

      <View style={styles.container}>
        <AppTextBold style={styles.title}>FORGOT PASSWORD</AppTextBold>

        {/* <KeyboardAvoidingView behavior={'position'}> */}
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.container}>
              <View style={styles.viewContainer}>
                <View style={styles.tabContainer}>
                  <View
                    style={existingCustomer ? styles.active : styles.inactive}>
                      <TouchableOpacity
                      onPress={() => {
                        setexistingCustomer(true);
                        SetDynamicActionType('exist_cust_fotget_pwd');
                      }}
                      style={{flex:1}}
                      >
                    <View style={{flex:1, alignSelf:'center', justifyContent:'center'}}>
                      <Text
                        style={
                          existingCustomer ? styles.text : styles.textClicked
                        }>
                        Existing Customer
                      </Text>
                    </View>
                    </TouchableOpacity>
                  </View>
                  <View
                    style={!existingCustomer ? styles.active : styles.inactive}>
                      <TouchableOpacity
                      onPress={() => {
                        setexistingCustomer(false);
                        SetDynamicActionType('new_user_fotget_pwd');
                      }}
                      style={{flex:1}}
                      >
                    <View style={{flex:1, alignSelf:'center', justifyContent:'center'}}>
                      <Text
                        style={
                          !existingCustomer ? styles.text : styles.textClicked
                        }>
                        New User
                      </Text>
                    </View>
                    </TouchableOpacity>
                  </View>
                </View>
                {existingCustomer ? (
                  <View>
                    <View style={styles.panNumberView}>
                      <TextInput
                        style={styles.inputPan}
                        onTouchStart={() => {
                          //setModalVisible(true);
                        }}
                        placeholder="PAN Number"
                        autoCapitalize="characters"
                        underlineColorAndroid="transparent"
                        placeholderTextColor="black"
                        onChangeText={(e) => {
                          _existingUser(e);
                        }}
                        value={panNumber}
                      />
                    </View>
                  </View>
                ) : (
                  <View>
                    <View style={styles.newUser}>
                      <TouchableOpacity
                        style={styles.countryContainer}
                        onPress={() => {
                          setShowCountryModal(true);
                        }}>
                        {/* <Image
                          source={require('./../assets/images/flag-icon.png')}
                          style={styles.icon}
                        /> */}
                        <View style={{height: height * 0.03, width: width * 0.08, top: height * 0.02,}}>
                          <Image
                              source={{ uri: countryFlag }}
                              style={styles.flagStyle}
                          />
                        </View>
                        <Text style={styles.countryName}>{countryName}</Text>

                        <Image
                          source={require('./../assets/images/down-icon-b.png')}
                          style={styles.downIcon}
                        />
                      </TouchableOpacity>

                      <View style={styles.containerViewMobile}>
                        <Text style={styles.userMobile1}>{countryId}</Text>

                        <TextInput
                          style={styles.userMobile}
                          placeholder={'  Mobile Number'}
                          underlineColorAndroid="transparent"
                          placeholderTextColor="black"
                          keyboardType="number-pad"
                          onTouchStart={() => {
                            //setNewUser(true);
                          }}
                          onChangeText={(e) => {
                            newUserMobileNo(e);
                          }}
                          value={mobilNumber}
                        />
                      </View>
                    </View>
                  </View>
                )}
              </View>
            </View>
          </TouchableWithoutFeedback>
        {/* </KeyboardAvoidingView> */}
      </View>
      <View style={styles.btnContainer}>
        {dynamicActionType == 'exist_cust_fotget_pwd' ? (
          <View />
        ) : (
          // <AppButton
          //     title="SEND OTP"
          //     onPress={() => {_existingCustomer();}}
          // />
          <AppButton
            title="SEND OTP"
            onPress={() => {
              _sendOtp();
            }}
          />
        )}
      </View>

      <View style={styles.modalContainer}>
        <Modal animationType="slide" transparent={true} visible={modalVisible}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={styles.close}>
                <TouchableOpacity
                  onPress={() => {
                    setModalVisible(false);
                  }}>
                  <Text>X</Text>
                </TouchableOpacity>
              </Text>
              <Text style={styles.modalText}>Mobile Number</Text>

              <FlatList
                data={DATA}
                renderItem={({item}) => (
                  <View style={styles.itemcontainer}>
                    <RadioButton
                      value={item.number}
                      status={
                        checkedMobile === item.number ? 'checked' : 'unchecked'
                      }
                      onPress={() => _selectMobile(item.number, item.id)}
                    />
                    <View style={styles.modalTextContainer}>
                      <TouchableOpacity onPress={() => { _selectMobile(item.number, item.id)}}>
                          <Text style={styles.number}>{item.number}</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                )}
              />
              <View style={{height: 10}} />
              {
                (checkedMobile == 'first')
                ?
                <View/>
                :
                <AppButton
                title="Continue"
                style={styles.btncontinue}
                onPress={() => {
                  setModalVisible(!modalVisible);
                  _goToOtpScreen();
                }}
              />
              }
              
            </View>
          </View>
        </Modal>
      </View>
      <Modal
          animationType="fade"
          transparent={true}
          visible={showCountryModal}
          onRequestClose={() => {
            setShowCountryModal(false);
          }}>
          <CountrySelectModalScreen
            onCancelPress={() => {
              setShowCountryModal(false);
            }}
            searchData={''}
            onSelectCountry={(selectedCountry) => {
              onSelectCountry(selectedCountry);
            }}
            isShowCountryCode={true}
          />
      </Modal>
      {modalVisible && <View style={styles.modalVisibility} />}
      {/* {isCountrySelected && <CountryModal func={setCountryInfo} />} */}
      {isLoading ? (
        //<ShowLoader/>
        <AppOverlayLoader isLoading={isLoading || isApiLoading} />
      ) : (
        <View />
      )}
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5,
  },
  viewContainer: {
    height: '50%',
    marginLeft: '5%',
    marginRight: '5%',
    justifyContent: 'center',
  },
  title: {
    marginBottom: 15,
    fontSize:appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    alignItems: 'center',
  },
  newUser: {flexDirection: 'row', marginTop: 12, marginBottom: 20},
  btnContainer: {marginHorizontal: 20, marginBottom: '15%'},
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalVisibility: {
    position: 'absolute',
    height,
    width,
    backgroundColor: 'black',
    opacity: 0.7,
    zIndex: 27,
  },
  btncontinue: {
    marginTop: 80,
    padding: 10,
  },
  number: {
    fontSize: appFonts.normalFontSize,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  itemcontainer: {
    flex: 1,
    flexDirection: 'row',
  },
  close: {
    left: '95%',
    color: 'grey',
  },
  modalContainer: {
    alignSelf: 'center',
    height,
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 29,
  },
  modalView: {
    backgroundColor: 'white',
    width: '90%',
    borderRadius: 10,
    padding: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalText: {
    marginBottom: 20,
    textAlign: 'center',
    fontSize: appFonts.xlargeFontSize,
    fontWeight: 'bold',
  },
  inputPan: {
    height: 50,
    width: width * 0.9,
    borderColor: colors.darkBorderGrey,
    borderBottomWidth: 1,
    marginBottom: 10,
  },
  panNumberView: {height: 80, justifyContent: 'center'},
  containerViewMobile: {
    width: '69%',
    flexDirection: 'row',
    borderColor: colors.darkBorderGrey,
    paddingHorizontal: 20,
    alignItems: 'center',
    borderBottomWidth: 1,
  },
  userMobile1: {
    marginRight: 20,
    textAlign: 'center',
    color: '#000',
    minHeight: 14,
    alignSelf: 'center',
  },
  userMobile: {
    width: '100%',
  },
  text: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: appFonts.largeFontSize,
  },
  countryContainer: {
    borderBottomColor: colors.darkBorderGrey,
    borderBottomWidth: 1,
    height: 50,
    justifyContent: 'center',
    width: '27%',
    marginRight: 15,
    flexDirection: 'row',
  },
  icon: {height: '40%', width: '40%', top: '20%'},
  downIcon: {height: '20%', width: '14%', top: '25%', marginLeft: 13},
  countryName: {color: 'black', marginLeft: 5, top: '20%'},
  tabContainer: {
    flexDirection: 'row',
    width: '100%',
    height: 60,
  },
  textClicked: {
    color: 'black',
    fontSize: appFonts.largeFontSize,
  },
  active: {
    width: '55%',
    // justifyContent: 'center',
    // alignItems: 'center',
    borderBottomColor: 'black',
    borderBottomWidth: 2,
    //backgroundColor:'#800000',
  },

  inactive: {
    width: '50%',
    // justifyContent: 'center',
    // alignItems: 'center',
    borderBottomColor: 'grey',
    borderBottomWidth: 2,
    //backgroundColor:'#ff0000',
  },
  modalTextContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 5,
  },
  flagStyle:{
    width: normStyle.normalizeWidth(30),
    height: normStyle.normalizeWidth(30)
  }
});

export default ForgotPassword;
