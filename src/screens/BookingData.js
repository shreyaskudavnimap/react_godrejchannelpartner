import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Text,
  Image,
  Dimensions,
  ScrollView
} from 'react-native';
import { useSelector } from 'react-redux';
//api
import NOTIFICATION_API from '../api/apiNotifications';
//components
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import NotificationsComponent from '../components/NotificationsComponent';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import appFonts from '../config/appFonts';
import colors from '../config/colors';

import Orientation from 'react-native-orientation';
import { EventRegister } from 'react-native-event-listeners';

import AppTypologyListItem from '../components/ui/AppTypologyListItem';
import AppTowerListItem from '../components/ui/AppTowerListItem';

const windowWidth = Dimensions.get('window').width;



const BookingData = ({ navigation, route }) => {
  const [isSqFt, setIsSqFt] = useState(false);
  const typologyList = JSON.parse('{"msg":"Successful","status":"200","project_name":"Godrej Upavan","field_foyr_url":"https://customercare.godrejproperties.com/gpl-api/virtual_grid_mobile/a1l2s000000PJPmAAO","data":[{"floor_plan_id":"2307844","title":"Banglore","field_floor_price_range":"5.09 Net BV(Cr)","field_floor_size_square_foot":"367.59 to 427.22 sqft","field_floor_size_square_meter":"6 bookings","field_property_layout_plan_url":"https://dxdwcef76c2aw.cloudfront.net/sites/default/files/2021-02/Kalyan_MLP_1_BHK_Luxe.jpg","field_typology_tag":"329","selectedTypology":true,"scssTypology":"bhk_1"},{"floor_plan_id":"2307845","title":"Mumbai","field_floor_price_range":"15.56 Net BV(Cr)","field_floor_size_square_foot":"541.11 sqft","field_floor_size_square_meter":"13","field_property_layout_plan_url":"https://dxdwcef76c2aw.cloudfront.net/sites/default/files/2021-02/Kalyan_MLP_2_BHK_Premium.jpg","field_typology_tag":"330","selectedTypology":false,"scssTypology":"bhk_2"}]}');
  const towerList = JSON.parse('[{"id":"2301938","title":"Godrej 24","code":"KB08","filling_fast":"","inv_count":201,"selectedTower":false},{"id":"2301939","title":"Godrej Aqua","code":"KB09","filling_fast":"","inv_count":202,"selectedTower":false}]');

  const onSelectTypology = (typology) => {
    dispatch(inventoryMPLAction.updateTypologyList(typology));
    getTowerList(
      userId,
      deviceUniqueId,
      projectId,
      typology.field_typology_tag,
    );
  };

  return (
    <Screen>
      <AppHeader />

      <ScrollView>
        <View style={styles.container}>
          <AppTextBold style={styles.pageTitle}>
            BOOKING DATA
          </AppTextBold>

          {/* Typology List */}
          {typologyList &&
            typologyList.data &&
            typologyList.data.length > 0 && (
              <View style={styles.typologyContainer}>
                <FlatList
                  showsHorizontalScrollIndicator={false}
                  horizontal
                  data={typologyList.data}
                  renderItem={({ item, index }) => (
                    <AppTypologyListItem
                      menuItem={item}
                      index={index}
                      color={
                        item.selectedTypology
                          ? item.scssTypology && colors[item.scssTypology]
                            ? item.scssTypology
                            : 'secondary'
                          : 'primary'
                      }
                      textColor={
                        item.selectedTypology
                          ? 'primary'
                          : item.scssTypology && colors[item.scssTypology]
                            ? item.scssTypology
                            : 'secondary'
                      }
                      borderColor={
                        item.selectedTypology
                          ? 'primary'
                          : item.scssTypology && colors[item.scssTypology]
                            ? item.scssTypology
                            : 'secondary'
                      }
                      onPress={() => {
                        if (!item.selectedTypology)
                          onSelectTypology(item);
                      }}
                      isSqFt={isSqFt}
                    />
                  )}
                  keyExtractor={(item) => item.field_typology_tag}
                />
              </View>
            )}

          <View style={styles.towerListContainer}>
            {towerList.map((towerItem, indexTowerItem) => (
              <View key={indexTowerItem}>
              {!towerItem.selectedTower ? (
                <AppTowerListItem
                  menuItem={towerItem}
                  onPress={() => {
                    if (!towerItem.selectedTower)
                      onSelectTower(towerItem);
                  }}
                />
                ) : ('')}
              </View>
              
            ))}
          </View>
        </View>
      </ScrollView>

    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 10,
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    paddingHorizontal: 25,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: '7%',
    textTransform: 'uppercase',
  },
  viewContainer: {
    width: windowWidth,
    paddingBottom: 20,
    marginBottom: 25,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  titleLine: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  value: {
    fontSize: appFonts.largeBold,
    color: colors.expandText,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  icon: {
    width: 16,
    height: 16,
  },
  layoutImg: {
    width: '90%',
    marginVertical: 20,
  },
  detailsLine: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
  },
  checkboxContainer: {
    flexDirection: 'row',
    paddingBottom: '5%',
    alignItems: 'center',
  },
  checkboxLabel: {
    fontSize: appFonts.largeFontSize,
    color: colors.jaguar,
    margin: 8,
    fontFamily: appFonts.SourceSansProRegular,
  },
  checkbox: { height: 24, width: 24 },
  bottomContainer: { padding: 24 },
  txtMsg: {
    fontSize: appFonts.normalFontSize,
    marginBottom: 20,
    lineHeight: 20,
    textAlign: 'justify',
  },
  activityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default BookingData;
