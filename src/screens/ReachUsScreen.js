import React, { useEffect, useState } from 'react';
import { Linking, Alert, Platform, TouchableOpacity } from 'react-native';

import {
    View,
    StyleSheet,
    Image,
    Dimensions,
    ScrollView,
    LayoutAnimation,
    FlatList,
    TouchableWithoutFeedback
} from 'react-native';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppTextBoldSmall from '../components/ui/AppTextBoldSmall';
import AppProjectDetailsMenuItem from '../components/ui/AppProjectDetailsMenuItem';
import AppFooter from '../components/ui/AppFooter';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import apiReachUs from '../api/apiReachUs'

import AppClickableText from '../components/ui/AppClickableText';
import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';


import AppOverlayLoader from '../components/ui/AppOverlayLoader';

import { useSelector } from 'react-redux';
import Orientation from 'react-native-orientation';

const windowHeight = Dimensions.get('window').height;





var property_nameatzero = "";
var booking_idatzero = "";
const ReachUsScreen = (props) => {
    const [isLoading, setIsLoading] = useState(false);
    const [rmdata, setRmdata] = useState([])
    const [allrmdata, setAllrmdata] = useState([])
    const [propertyitems, setPropertyitems] = useState([]);



    const [isExpandedPropertyname, setIsExpandedPropertyname] = useState(false);

    const [selectedPropertyname, setSelectedPropertyname] = useState("")




    const loginvalue = useSelector((state) => state.loginInfo.loginResponse);

    //const userId = loginvalue.data.userid;
    var userId = '';

    useEffect(() => {
        Orientation.lockToPortrait();
    }, []);


    useEffect(() => {
        if (loginvalue.isLogin) {
            userId = loginvalue.data.userid;
            console.log(userId);
        }
        api_RM_INFO();
    }, [])




    const api_RM_INFO = async () => {
        try {
            setIsLoading(true)
            const requestData = { user_id: userId };
            const rmdata = await apiReachUs.api_RM_INFO(requestData);

            setAllrmdata(rmdata.data.data)
            setRmdata(rmdata.data.data[0])
            setPropertyitems(propertyOptions(rmdata.data.data))


            setIsLoading(false)
        } catch (error) {
            console.log(error);
            setIsLoading(false);
            appSnakBar.onShowSnakBar(
                appConstant.appMessage.APP_GENERIC_ERROR,
                'LONG',
            );
        }

    };


    const properties = [];
    const propertyOptions = (propert) => {

        for (let x in propert) {
            properties.push({
                name: propert[x].property_name + "/" + propert[x].inv_flat_code,
                id: propert[x].booking_id.toString()
            })
        }

        property_nameatzero = properties[0].name
        booking_idatzero = properties[0].booking_id
        console.log(properties)
        return properties;
    }

    const getpropertydetail = (name, id) => {
        console.log(name, id)
        setIsExpandedPropertyname(!isExpandedPropertyname);
        setSelectedPropertyname(name);

        for (let x in allrmdata) {
            if (allrmdata[x].booking_id == id) {
                setRmdata(allrmdata[x]);

            }
        }
    }
    const changePropertlistlayout = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        setIsExpandedPropertyname(!isExpandedPropertyname);

    };

    const callNumber = () => {

        const phone = rmdata.rm_mobile_no;
        console.log('callNumber ----> ', phone);
        let phoneNumber = phone;

        if (Platform.OS !== 'android') {
            phoneNumber = `telprompt:${phone}`;
        }
        else {
            phoneNumber = `tel:${phone}`;
        }
        Linking.canOpenURL(phoneNumber)
            .then(supported => {
                if (!supported) {
                    Alert.alert('Phone number is not available');
                } else {
                    return Linking.openURL(phoneNumber);
                }
            })
            .catch(err => console.log(err));

    }

    const sendemail = () => {
        const email = rmdata.rm_email;
        console.log(email);
        Linking.openURL(`mailto:${email}`)
    }

    const onHandleRetryBtn = () => {
        if (loginvalue.isLogin) {
            userId = loginvalue.data.userid;
            console.log(userId);
        }
        api_RM_INFO();
    };

    return (
        <Screen onRetry={onHandleRetryBtn}>
            <AppHeader />
            <ScrollView>
                <View style={styles.container}>
                    <AppTextBold style={styles.pageTitle}>Reach Us</AppTextBold>
                    <View>
                        <View style={styles.button}>
                            {allrmdata.length>1?
                            <TouchableOpacity style={{ width: "100%", marginTop: 10, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }} onPress={changePropertlistlayout}>
                                <AppText style={styles.buttonText}>{selectedPropertyname != "" ? selectedPropertyname : property_nameatzero}</AppText>

                                <Image
                                    source={allrmdata.length > 1 ?
                                        isExpandedPropertyname ? require('./../assets/images/up-icon-b.png') : require('./../assets/images/down-icon-b.png')
                                        : null

                                    }
                                    style={styles.icon}
                                />
                            </TouchableOpacity>: <AppText style={styles.buttonText}>{ property_nameatzero}</AppText>}
                        </View>
                        {/* show property name listing */}
                        <View
                            style={{
                                height: isExpandedPropertyname ? null : 0,
                                overflow: 'hidden',
                            }}>

                            <FlatList
                                data={propertyitems}
                                renderItem={({ item }) => (
                                    <View style={styles.button}>
                                        <TouchableWithoutFeedback onPress={(event) => getpropertydetail(item.name, item.id)}>
                                            <AppText style={styles.buttonText}>{item.name}</AppText>
                                        </TouchableWithoutFeedback>
                                    </View>
                                )}
                            />

                        </View>
                    </View>

                    <View style={styles.imgContainer}>
                        <View style={{justifyContent:"center",alignItems:"center"}}>
                        <Image
                            source={rmdata.rm_img != "" ? { uri: rmdata.rm_img } : require('./../assets/images/no-user-img.png')}
                            style={[styles.icon, { height: 104, width: 104, borderRadius: 52 }]}
                        />
                         {rmdata.rm_info != "" &&
                        <View style={styles.rmView}>
                            <TouchableOpacity
                                onPress={() => {
                                    props.navigation.navigate('RMDetailsScreen', {
                                        rmName: rmdata.rm_name,
                                        npsScore: rmdata.nps_score,
                                        npsYear: rmdata.nps_year,
                                        rmInfo: rmdata.rm_info,
                                        rmImg: rmdata.rm_img,
                                        npsMonth: rmdata.nps_month
                                    })
                                }}
                                style={styles.rmText}>
                                <AppTextBoldSmall>Know Your RM</AppTextBoldSmall>
                            </TouchableOpacity>
                            <View />

                        </View>
                    }
                        </View>
                        <View style={styles.rmName}>
                            <AppTextBold>{rmdata.rm_name}</AppTextBold>
                            {!isLoading && rmdata.nps_score != "" &&
                                <AppText>{rmdata.nps_month!= "" && rmdata.nps_year != ""? rmdata.nps_month + ", " : null}{rmdata.nps_year != "" ? rmdata.nps_year + " " : null}
                                Net Promoter Score: {rmdata.nps_score}%</AppText>
                            }
                        
                        </View>

                    </View>
                   

                    <View style={styles.mailContainer}>

                        <View style={styles.card}>
                            <View style={styles.containerMail}>
                                <TouchableOpacity onPress={callNumber} style={styles.mailContainer}>
                                    <Image
                                        source={require('./../assets/images/call-1.png')}
                                        style={styles.imgPhone}

                                    />
                                    <AppText>Call Your RM</AppText>
                                </TouchableOpacity>

                            </View>
                            <View style={styles.partition}>
                            </View>
                            <View style={styles.containerMail}>
                                <TouchableOpacity onPress={sendemail} style={styles.mailContainer}>
                                    <Image
                                        source={require('./../assets/images/black-mail-icon-new.png')}
                                        style={styles.imgMail}
                                    />
                                    <AppText>Email Your RM</AppText>
                                </TouchableOpacity>

                            </View>
                        </View>


                        <AppProjectDetailsMenuItem title="Book A Visit" onPress={() => { props.navigation.navigate('VisitScreen') }} />
                        <AppProjectDetailsMenuItem title="Write To Us" onPress={() => { props.navigation.navigate('ServiceRequest') }} />
                        <AppProjectDetailsMenuItem title="Raise a snag" onPress={() => { props.navigation.navigate('RaiseSnagScreen', { bookingId: rmdata.booking_id }) }} />
                    </View>

                </View>
            </ScrollView>
            <AppFooter activePage={6} isPostSales={true} />
            <AppOverlayLoader isLoading={isLoading} />
        </Screen>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginBottom: 30
    },
    rmView:{ flexDirection: "row", justifyContent: "space-between", alignItems: "center",marginTop:3 },
    rmText:{ borderBottomColor: "black", borderBottomWidth: 1, },
    imgContainer: {
        flexDirection: "row",
        // justifyContent: "space-between",
        padding: 10,
        alignItems: "center",
        marginTop: 10
    },
    rmName:{ margin: 20, width: "65%",bottom:12 },
    mailContainer:{ justifyContent: "center", alignItems: "center" },
    partition: { height: "90%", width: 1, backgroundColor: "black" },
    containerMail: { justifyContent: "center", alignItems: "center", },
    imgPhone: { height: 25, width: 25, margin: 10 },
    imgMail: { height: 25, width: 25, margin: 10 },
    card: {
        width: '100%',
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        padding: 8,
        height: windowHeight * 0.15,
        marginTop: 30,
        marginBottom: 10,
        borderWidth: 1,
        borderRadius: 1,
        borderColor: colors.lightGray,
        borderBottomWidth: 1,
        shadowColor: colors.lightGray,
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: 0.7,
        shadowRadius: 3,
        elevation: 3,
        backgroundColor: colors.primary,
    },

    pageTitle: {
        fontSize: appFonts.xxxlargeFontSize,
        fontFamily: appFonts.SourceSansProBold,
        color: colors.jaguar,
        textTransform: 'uppercase',
    },
    button: {
        height: 60,
        borderBottomWidth: 1, width: "100%", borderBottomColor: colors.borderGrey,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 25,
    },
    buttonText: {
        fontSize:appFonts.largeBold,
        fontFamily: appFonts.SourceSansProSemiBold,
        color: colors.secondary,

    },
    icon: {
        width: 15,
        height: 15,
        zIndex: 9999,
    },
});

export default ReachUsScreen;