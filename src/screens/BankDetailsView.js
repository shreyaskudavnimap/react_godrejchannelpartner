import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Image, FlatList, TouchableWithoutFeedback, TextInput, ScrollView, Picker, Modal, Alert, Dimensions } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation'
import colors from '../config/colors';
import appFonts from '../config/appFonts';
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppButton from '../components/ui/AppButton';
import AppFooter from '../components/ui/AppFooter';
import appSnakBar from '../utility/appSnakBar';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import { useSelector } from 'react-redux';
import apiLoanDisbursement from '../api/apiLoanDisbursement';
import appConstant from '../utility/appConstant';
import { useNavigation } from '@react-navigation/native';
import useFileDownload from '../hooks/useFileDownload';
import AppProgressBar from '../components/ui/AppProgressBar';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Orientation from 'react-native-orientation';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const BankDetailsView = (props) => {
    const bookingId = props.bookingId;
    const projectId = props.projectId;
    // const [isLoading, setIsLoading] = useState(false);
    const [successMsg, setSuccessMsg] = useState('');
    const [formData, setFormData] = useState([]);
    const [showPoupMsg, isShowPoupMsg] = useState(false);
    const [showNoteMsg, isShowNoteMsg] = useState(false);
    const [popupMsg, setPopupMsg] = useState('');

    const navigation = useNavigation();

    const loginvalue = useSelector(
        (state) => state.loginInfo.loginResponse,
    );
    const userId = typeof loginvalue.data !== 'undefined' ? loginvalue.data.userid : '';

    useEffect(() => {
        Orientation.lockToPortrait();
    }, []);


    useEffect(() => {
        getBankDetails();
    }, [projectId]);

    const getBankDetails = async () => {
        props.setLoader(true);
        try {
            const requestParam = { user_id: userId, booking_id: bookingId }
            const bankDetails = await apiLoanDisbursement.getBankDetails(requestParam);
            props.setLoader(false);
            if (bankDetails.data.status == 200) {
                const bankData = bankDetails.data.bank_details[0];
                setFormData(bankData);
                isShowNoteMsg(false);
                console.log("navigation",props.navigation)
                if (bankData.verification_status == 'Pending') {
                    Alert.alert(
                        "",
                        "Your submitted details are awaiting for RM approval",
                        [
                            {
                                text: "OK",
                                onPress: () => {
                                    if (formData.is_incomplete_information != 1) {
                                        isShowNoteMsg(true);
                                    }
                                },
                            }
                        ],
                        { cancelable: false },
                    );
                    setPopupMsg('Your submitted details are awaiting for RM approval');
                    // isShowPoupMsg(true);
                }
                let isIncompleteInfo = bankData.is_incomplete_information ? bankData.is_incomplete_information : 0;
                if (isIncompleteInfo == 1 && bankData.verification_status == 'Approved') {
                    Alert.alert(
                        "",
                        "Bank Details are partially filled, please enter complete details",
                        [
                            {
                                text: "OK",
                                onPress: () => {
                                    isShowNoteMsg(true);
                                },
                            }
                        ],
                        { cancelable: false },
                    );
                    setPopupMsg('Bank Details are partially filled, please enter complete details');
                    // isShowPoupMsg(true);
                }
            } else {
                navigator.navigate('LoanEnquiryScreen', { bookingId: bookingId, activeTab: "LoanDetails" });
            }
        } catch (error) {
            console.log(error);
            props.setLoader(false);
            appSnakBar.onShowSnakBar(
                appConstant.appMessage.APP_GENERIC_ERROR,
                'LONG',
            );
        }
    }

    const closePopup = () => {
        isShowPoupMsg(false);
        if (formData.is_incomplete_information != 1) {
            isShowNoteMsg(true);
        }
    }

    const { progress, showProgress, error, isSuccess, checkPermission, } = useFileDownload();

    const downloadSanctionLetter = (sanctionLetterName, sanctionLetterUrl) => {
        // const fileName = 'invoice_2018-07-10.pdf';
        let fileName = sanctionLetterName;

        if (fileName) {
            const ext = fileName.split('.')[0];
            if (ext) {
                downloadFile(sanctionLetterUrl, fileName, ext);
            }
        } else {
            let fileExt = appConstant.getExtention(sanctionLetterUrl);

            const fName = appConstant.getFileName(sanctionLetterUrl);
            if (fileExt[0] && fName) {
                downloadFile(REMOTE_IMAGE_PATH, fName, fileExt[0]);
            }
        }
    }

    const downloadFile = async (imgPath, fileName, fileExtension) => {
        try {
            await checkPermission(imgPath, fileName, fileExtension);
        } catch (error) {
            appSnakBar.onShowSnakBar(error.message, 'LONG');
        }
    };

    return (
        <>
            {!props.loading ? (
                <ScrollView>
                    <View style={styles.container}>
                        {/* <AppTextBold style={styles.pageTitle}>Your Bank Loan Details</AppTextBold> */}

                        <View style={styles.item}>
                            <AppText style={styles.label}>Bank</AppText>
                            <AppText style={styles.itemTxt}>{formData.bank_name}</AppText>
                        </View>

                        <View style={styles.item}>
                            <AppText style={styles.label}>Banker Email Id</AppText>
                            <AppText style={styles.itemTxt}>{formData.banker_email_id}</AppText>
                        </View>

                        <View style={styles.item}>
                            <AppText style={styles.label}>Loan Amount</AppText>
                            <AppText style={styles.itemTxt}>&#8377;{formData.bank_loan_amount}</AppText>
                        </View>

                        <View style={styles.item}>
                            <AppText style={styles.label}>Loan Account Number</AppText>
                            <AppText style={styles.itemTxt}>{formData.account_no}</AppText>
                        </View>
                        {formData.sanction_letter_name ? (
                            <View style={styles.item}>
                                <AppText style={styles.label}>Loan Sanction Letter</AppText>
                                <TouchableOpacity
                                    style={styles.downloadBlock}
                                    onPress={() => downloadSanctionLetter(formData.sanction_letter_name, formData.sanction_letter_url)}
                                >
                                    <AppText style={styles.itemTxt}>
                                        {appConstant.fileNameSanitize(formData.sanction_letter_name)}
                                    </AppText>
                                    <Image style={styles.downloadIcon}
                                        source={require('../assets/images/download-b.png')}
                                        resizeMode="contain" />
                                </TouchableOpacity>
                            </View>
                        ) : (null)}

                        {showNoteMsg ? (
                            <AppText style={styles.infoMsg}>{popupMsg}</AppText>
                        ) : (null)}

                        {formData.verification_status == 'Approved' ? (
                            <View style={styles.button}>
                                <AppButton title="Update"
                                    color="primary"
                                    textColor="secondary"
                                    onPress={e => {
                                        props.showUpdateForm();
                                    }} />
                            </View>
                        ) : null}
                    </View>
                </ScrollView>
            ) : null}


            {/* <View style={styles.modalContainer}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={showPoupMsg}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                           
                            <TouchableOpacity style={styles.close}
                                onPress={() => closePopup()}
                            >
                                <Icon name={'close'} color={colors.charcoal} size={25} />
                            </TouchableOpacity>
                            <View style={{ height: 70 }}>
                                <AppText>{popupMsg}</AppText>
                            </View>
                        </View>

                    </View>

                </Modal>

            </View> */}

            {showProgress && progress && progress > 0 ? (
                <AppProgressBar progress={progress} />
            ) : null}
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // paddingHorizontal: 20,
        paddingVertical: 10,
    },
    itemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 24,
        paddingVertical: 5,
        marginTop: 24,
        marginVertical: 20,
        backgroundColor: '#f7f7f7',
    },
    item: {
        borderColor: 'gray',
        borderBottomWidth: 1,
        marginBottom: 15,

    },
    label: {
        color: colors.gray3,
        fontSize: appFonts.normalFontSize,
    },
    itemTxt: {
        color: colors.jaguar,
        marginRight: 5,
        paddingBottom: 10

    },
    button: {
        marginVertical: '5%',
        // marginHorizontal: '5%',
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
    },
    modalContainer: {
        alignSelf: 'center',
        height: 20,
        justifyContent: 'center',
        position: 'absolute',
        zIndex: 29
    },
    modalView: {
        backgroundColor: "white",
        width: "90%",
        borderRadius: 10,
        padding: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    modalText: {
        marginBottom: 20,
        textAlign: "center",
        fontSize: appFonts.xlargeFontSize,
        fontWeight: 'bold'
    },
    close: {
        left: "95%",
        color: 'grey',
        marginTop: -10
    },
    infoMsg: {
        backgroundColor: colors.whiteSmoke,
        borderRadius: 10,
        padding: 15,
    },
    downloadBlock: {
        flex: 1,
        flexDirection: "row",
        alignItems: 'center',
        width: '95%',
        display: 'flex',
        justifyContent: 'space-between'

    },
    previewDownload: {
        flexShrink: 1,
        width: '80%',
        paddingRight: 15
    },
    downloadIcon: {
        width: '5%',
        alignSelf: 'baseline',
        marginRight: 15,
        marginBottom: 10
    }

})
export default BankDetailsView;