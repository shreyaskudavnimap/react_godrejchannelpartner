import React, { useEffect, useState,useRef } from 'react';
import {
	View,
	StyleSheet,
	TouchableOpacity,
	Image,
	TextInput,
	ScrollView,
	LayoutAnimation,
	FlatList,
	TouchableWithoutFeedback
}
	from 'react-native';



import Modal from 'react-native-modal';
import AppFileChooser from './../components/actionSheet/AppFileChooser';
import appFileChooser from './../utility/appFileChooser';
import { useSelector } from 'react-redux';
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppTextBoldSmall from '../components/ui/AppTextBoldSmall';
import appFonts from '../config/appFonts';
import AppFooter from '../components/ui/AppFooter';
import colors from '../config/colors';
import apiDocument from '../api/apiDocument';
import appSnakBar from '../utility/appSnakBar';
import useFileDownload from '../hooks/useFileDownload';
import AppProgressBar from '../components/ui/AppProgressBar';
import appConstant from '../utility/appConstant';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import DocumentPicker from 'react-native-document-picker';
import DocumentUploaded from '../screens/modals/DocumentUploaded'
import Orientation from 'react-native-orientation';
import Selecteddocumentlistmodal from './modals/Selecteddocumentlistmodal'


const Properties = [];
const DocumentType = [];
const DocumentSubtype = [];
var DocumenttypeAt_Zero = "";
var property_nameatzero = "";
var booking_idatzero = "";
var itemslength = "0";
var onkey = "";

const DocumentScreen = (props) => {


	const [isExpandedPropertyname, setIsExpandedPropertyname] = useState(false);
	const [isDocumentCatogery, setIsDocumentCatogery] = useState(false);
	const [isDocumentSubCategory, setIsDocumentSubCategory] = useState(false);
	const [isChildSubCategory, setIsChildSubCategory] = useState(false);
	const [isLoading, setIsLoading] = useState(false);
	const [checksearch, setChecksearch] = useState(false);
	const [active, setActive] = useState(false);
	
	const [selectedPropertyname, setSelectedPropertyname] = useState("")
	const [selectDocumentType, setSelectDocumentType] = useState("");
const input_reset=useRef();

	const [allDocuments, setAllDocuments] = useState([]);
	const [myUploadDocuments, setMyUploadDocuments] = useState([]);
	const [gplUploadDocuments, setGplUploadDocuments] = useState([]);
	const [childsubstatecatgory, setChildsubstatecatgory] = useState([])
	const [showData, setShowData] = useState([]);
	const [filterdata, setFilterdata] = useState([]);
	const [search, setSearch] = useState([]);
	const [filesToUpload, setFilesToUpload] = useState([]);

	// download states
	const [fileChooserResponse, setFileChooserResponse] = useState(null);
	const [fileChooser, setFileChooser] = useState(false);
	const [documentuploadedModal, setDocumentuploadedModal] = useState(false)
	const [documentselectedlistModal, setDocumentselectedlistModal] = useState(false)
	const [documentuploadedModalMSG, setDocumentuploadedModalMSG] = useState(false)
	const closeActionSheet = () => setFileChooser(false);

	const changePropertlistlayout = () => {
		LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
		setIsExpandedPropertyname(!isExpandedPropertyname);
	};
	const changeDocumentCatogerylayout = () => {
		LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
		setIsDocumentCatogery(!isDocumentCatogery);
		setIsDocumentSubCategory(false)
	};
	const changeDocumentSubcategorylayout = (id) => {
		if (id == 3) {
			LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
			setIsDocumentSubCategory(!isDocumentSubCategory)

		}
	};




	const loginvalue = useSelector(
		(state) => state.loginInfo?.loginResponse,
	);
	const userId = loginvalue.data?.userid;



	useEffect(() => {
		Orientation.lockToPortrait();
	}, []);

	// api call
	useEffect(() => {
		api_GET_USER_DOCUMENT_LIST();

	}, [])

	const api_GET_USER_DOCUMENT_LIST = async () => {
		try {
			setIsLoading(true);
			const requestData = { user_id: userId };
			const userdocument = await apiDocument.api_GET_USER_DOCUMENT_LIST(requestData);
			(Properties.length == 0) ? propertyOptions(userdocument?.data?.data?.property) : null;
			api_LIST_DOCUMENT(booking_idatzero);
			console.log(userdocument?.data?.data?.property)
			setIsLoading(false);
		} catch (error) {
			console.log(error);
			appSnakBar.onShowSnakBar(
				appConstant.appMessage.APP_GENERIC_ERROR,
				'LONG',
			);
		}
	};


	const propertyOptions = (propertynames) => {
		for (let x in propertynames) {
			Properties.push(
				{
					name: propertynames[x].name,
					booking_id: propertynames[x].booking_id
				}
			)
		}
		property_nameatzero = Properties[0].name,
			booking_idatzero = Properties[0].booking_id,
			console.log(Properties)

		return Properties;
	}


	var selectBookingid="";
	const getpropertydetail = (name, booking_id) => {
	selectBookingid=booking_id;
	console.log(selectBookingid+"oiuytrewertyuioiuytrertyuioiuytrtyuiopoiuytr")
		setSelectedPropertyname(name);
		setIsExpandedPropertyname(!isExpandedPropertyname);
		api_LIST_DOCUMENT(booking_id)
		DocumentType.length = 0;
		DocumentSubtype.length = 0;
		setSelectDocumentType("");


	}

	// api for documents 

	const api_LIST_DOCUMENT = async (booking_id) => {
		try {
			setIsLoading(true);
			console.log(booking_id + "booking id")
			const requestData = { user_id: userId, "booking_id": booking_id };
			const userdocument = await apiDocument.api_LIST_DOCUMENT(requestData);
			(DocumentType.length == 0) ? DocumentTypeOption(userdocument?.data?.data) : null;
			setdata(userdocument?.data?.data)
			DocumentSubtype.length == 0 ? Subcategorydata(userdocument?.data?.data) : null
			// console.log(JSON.stringify(userdocument.data.data[2].document_category))


			setIsLoading(false);
		} catch (error) {
			console.log(error);
			appSnakBar.onShowSnakBar(
				appConstant.appMessage.APP_GENERIC_ERROR,
				'LONG',
			);
		}
	};

	const setdata = (alldata) => {
		for (let i in alldata) {
			if (alldata[i].id == "1") {
				setAllDocuments(alldata[i]);
				setShowData(alldata[i]);

			}
			if (alldata[i].id == "2") {
				setMyUploadDocuments(alldata[i])
			}
			if (alldata[i].id == "3") {
				setGplUploadDocuments(alldata[i])
			}
		}

	}

	const DocumentTypeOption = (type) => {
		for (let parenttype in type) {
			DocumentType.push({
				id: type[parenttype].id,
				parent: type[parenttype].title
			})
		}
		DocumenttypeAt_Zero = DocumentType[0].parent;
		console.log(DocumenttypeAt_Zero)
	}
	const Subcategorydata = (alldata) => {

		for (let category in alldata[2].document_category) {


			DocumentSubtype.push({
				id: alldata[2].document_category[category].id,
				child: alldata[2].document_category[category].title
			})
		}
	}

	const changeDocumentType = (parent, id) => {
		setIsDocumentCatogery(!isDocumentCatogery);
		setIsDocumentSubCategory(false);
		setSelectDocumentType(parent)
		setShowData([]);
	
		if (id == "1") {
			setShowData(allDocuments);
			itemslength = allDocuments.file.length;
		console.log(allDocuments.file.length)
	
		
		}
		if (id == "2") {
			setShowData(myUploadDocuments);
			itemslength = myUploadDocuments.file.length;
			console.log(myUploadDocuments.file.length)
	
		}

		if (id == "3") {
			setShowData(gplUploadDocuments);
			
			itemslength = gplUploadDocuments.file.length;
			console.log(gplUploadDocuments.file.length)
		
		}
	}

	const changeChildSubcategorylayout = (id) => {

		LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
		setIsChildSubCategory(!isChildSubCategory)

		if (onkey == "") {


			onkey = id;
			setActive(true);
			setChildsubstatecatgory(gplUploadDocuments.document_category[onkey - 1].document_type)
			setIsChildSubCategory(true)

		}
		else if (onkey == id) {
			onkey = "";
			setIsChildSubCategory(!isChildSubCategory)
			setActive(false);
		}
		else {

			onkey = id;
			setActive(true);
			setChildsubstatecatgory(gplUploadDocuments.document_category[onkey - 1].document_type)
			setIsChildSubCategory(true)

		}
		console.log(onkey + "onkey ")
	}
	const child_sub_category = (id) => {
		setShowData(childsubstatecatgory[id - 1]);
		setIsDocumentCatogery(!isDocumentCatogery);
		setIsDocumentSubCategory(!isDocumentSubCategory);
		setSelectDocumentType(childsubstatecatgory[id - 1].title)
	}
	const subtypedata = (id, title) => {

		console.log(id + " " + title)
		setShowData(gplUploadDocuments.document_category[id - 1])
		setSelectDocumentType(gplUploadDocuments.document_category[id - 1].title)
		setIsDocumentCatogery(!isDocumentCatogery);
		setIsDocumentSubCategory(!isDocumentSubCategory);

	}


	// Searching
	const onsearch = (text) => {

		console.log(text)
		if (text) {
			setChecksearch(true);
			const newData = allDocuments.file.filter(
				function (item) {
					const itemData = item.title
						? item.title.toUpperCase()
						: ''.toUpperCase();
					const textData = text.toUpperCase();
					return itemData.indexOf(textData) > -1;
				}
			);
			setFilterdata(newData);

			setSearch(text);
		} else {
			setChecksearch(false);
			setFilterdata(allDocuments.file);
			setSearch(text);
		}

	}




	// uploading documents

	const onClickFileChooserItem = async (clickItem) => {
		switch (clickItem) {
			case 'Remove':
				setFileChooser(false);
				setFileChooserResponse(null);
				break;

			case 'Gallery':
				appFileChooser.onLaunchImageLibrary((fileChooserResponse) => {
					setFileChooser(false);
					if(fileChooserResponse && fileChooserResponse.uri){
					appFileChooser.toBase64(fileChooserResponse.uri).then((resp) => {
						base64(resp, fileChooserResponse.fileName ? fileChooserResponse.fileName : appConstant.getFileName(fileChooserResponse.uri));

					}).catch((err) =>
						console.log(err)
					);}
				});
				break;

			case 'Camera':
				appFileChooser.onLaunchCamera((fileChooserResponse) => {
					setFileChooser(false);
					if(fileChooserResponse && fileChooserResponse.uri){
					appFileChooser.toBase64(fileChooserResponse.uri).then((resp) => {
						base64(resp, fileChooserResponse.fileName ? fileChooserResponse.fileName : appConstant.getFileName(fileChooserResponse.uri));

					}).catch((err) =>
						console.log(err)
					);}
				});
				break;

			case 'Phone':
				try {
					const chooserResponse = await appFileChooser.onSelectFile();
					const data = await appFileChooser.toBase64(chooserResponse.uri);
					setFileChooser(false);
					base64(data, chooserResponse.name)
                } catch (error) {
					setFileChooser(false);
                    console.log('Error onSelectFile : ', error);
                }
                break;
		}

	};

	const base64 = (resp, name) => {
		// alert('File fetched successfully : ' + JSON.stringify(resp))
	    setIsLoading(true)
		resp = resp;
		name = name;
		const  files = filesToUpload;
		if(name == null || name == undefined || name == ''){
			name = appConstant.getFileName(imgPath);
		}
		files.push({ "file_base64_encoded": resp, "file_name": name })
		setFilesToUpload(files);
		for(let x in files){console.log(files[x].file_name)}
		setTimeout(() => {
			setDocumentselectedlistModal(true);
		}, 200);
		setIsLoading(false)	
	}

	const removeFile = (index) => {	
		let filesToUpload = [...filesToUpload];
		console.log(filesToUpload.length+"okepokrof")
		filesToUpload.splice(index,1)
		setFilesToUpload(filesToUpload);
		for(let x in filesToUpload){console.log(filesToUpload[x].file_name)}
	}
	const api_Add_DOCUMENTS = () => {
		const requestData = ({ "user_id": userId, "booking_id":selectBookingid!="" ?selectBookingid : booking_idatzero, "files_attached": filesToUpload })
		setIsLoading(true);
		
		apiDocument.api_Add_DOCUMENTS(requestData).then((response) => {
			setIsLoading(false)
			setDocumentselectedlistModal(false);
			console.log(selectBookingid+"qwertyuiopasdfghjklwertyuiopsdfghjk")
			
			;
			if (response.status == "200") {
				const msg = response && response.data && response.data.msg ? response.data.msg : 'Document has been submitted to your Relationship Manager for review and would appear under My Uploads on acceptance.';
				console.log(selectBookingid)
				console.log(booking_idatzero);
				console.log('Document uploaded successfully', msg)
				setDocumentuploadedModalMSG(msg);
				setDocumentuploadedModal(true)
				setFilesToUpload([]);
			}
			
		}).catch((err) => {
			setIsLoading(false);
			console.log("testing data for uploading", requestData, err);
			appSnakBar.onShowSnakBar(
				appConstant.appMessage.APP_GENERIC_ERROR,
				'LONG',
			);
		});
	}


	// DOwnloading
	//download file
	const {
		progress,
		showProgress,
		error,
		isSuccess,
		checkPermission,
	} = useFileDownload();
	
	
	const handleFileDownload = async (imgPath, isPdf) => {
		setIsLoading(true)
		let fileExt = isPdf ? 'pdf' : appConstant.getExtention(imgPath);
		let fName = appConstant.getFileName(imgPath);
		fName = fName + (isPdf ? '.pdf' : '')
		if (fileExt[0] && fName) {
			downloadFile(imgPath, fName, fileExt[0]);
		} else {
			setIsLoading(false)
		}
	};

	const downloadFile = async (imgPath, fileName, fileExtension) => {
		try {
			await checkPermission(imgPath, fileName, fileExtension);
			setIsLoading(false)
		} catch (error) {
			appSnakBar.onShowSnakBar(error.message, 'LONG');
			setIsLoading(false)
		}
	};


	const onHandleRetryBtn = () => {
		api_GET_USER_DOCUMENT_LIST();
	};


	return (
		<Screen onRetry={onHandleRetryBtn}>
			<AppHeader />
			<ScrollView>
				{/* heading */}
				<View style={styles.container}>
					<View style={styles.headerContainer}>
						<AppTextBold style={styles.pageTitle}> MY DOCUMENT</AppTextBold>
						<TouchableOpacity onPress={() => setFileChooser(true)} >
							<Image
								style={styles.iconAdd}
								source={require('../assets/images/plus-icon.png')}
							/>
						</TouchableOpacity>
					</View>
					{/* property selector */}
					<View>
						{Properties.length>1?
						<TouchableOpacity onPress={changePropertlistlayout}>
							<View style={styles.button}>
								<AppText style={styles.buttonText}>{selectedPropertyname != "" ? selectedPropertyname : property_nameatzero}</AppText>

								<Image
									source={Properties.length > 1 ?
										isExpandedPropertyname
											? require('./../assets/images/up-icon-b.png')
											: require('./../assets/images/down-icon-b.png')
										: null
									}
									style={styles.icon}
								/>

							</View>
						</TouchableOpacity>
						:<AppText style={styles.buttonText}>{property_nameatzero}</AppText>}
						{/* show property name listing */}
						<View
							style={{
								height: isExpandedPropertyname ? null : 0,
								overflow: 'hidden',
							}}>
							<FlatList
								data={Properties}
								renderItem={({ item }) => (
									<TouchableWithoutFeedback onPress={(event) => getpropertydetail(item.name, item.booking_id)}>
										<View style={styles.button}>

											<AppText style={styles.buttonText}>{item.name}</AppText>

										</View>
									</TouchableWithoutFeedback>
								)}
							/>

						</View>
					</View>

					{/* search */}
					<TextInput placeholder="Search"
						style={styles.search}
						onChangeText={(text) => onsearch(text)}
						ref={input_reset}

					/>

					{/* category */}
					<View style={styles.buttonConatainer}>
						<TouchableOpacity onPress={changeDocumentCatogerylayout}>
							<View style={styles.button}>
								<AppText style={styles.buttonText}>{selectDocumentType != "" ? selectDocumentType : DocumenttypeAt_Zero}</AppText>

								<Image
									source={
										isDocumentCatogery
											? require('./../assets/images/up-icon-b.png')
											: require('./../assets/images/down-icon-b.png')
									}
									style={styles.icon}
								/>
							</View>
						</TouchableOpacity>
						{/* document category listing */}
						<View
							style={{
								height: isDocumentCatogery ? null : 0,
								overflow: 'hidden',
							}}>
							{DocumentType.map((item, index) => {
								return <View key={index}>
									<TouchableOpacity onPress={(event) => changeDocumentSubcategorylayout(item.id)} hitSlop={{ top: 20, bottom: 20, left: 100, right: 50 }}>
										<View style={styles.button}>
										<TouchableOpacity onPress={(e) => changeDocumentType(item.parent, item.id)}>
											<AppText style={styles.buttonText}>{item.parent}</AppText>
											</TouchableOpacity>
											
												<Image
													source={
														(item.id == 3) ? isDocumentSubCategory
															? require('./../assets/images/up-icon-b.png')
															: require('./../assets/images/down-icon-b.png')
															: null}
													style={styles.icon}
												/>
											
										</View>
									</TouchableOpacity>
								</View>

							})
							}
						</View>

						{/* document subcategory list */}
						<View
							style={{
								height: isDocumentSubCategory ? null : 0,
								overflow: 'hidden',
							}}>

							{DocumentSubtype.map((item, index) => {
								return <View key={index}>
								<TouchableOpacity onPress={(event) => changeChildSubcategorylayout(item.id)} hitSlop={{ top: 20, bottom: 20, left: 100, right: 50 }}>
										<View style={styles.button}>
										<TouchableOpacity onPress={(e) => subtypedata(item.id, item.child)}>
											<AppText style={styles.buttonText1}>{item.child}</AppText>
											</TouchableOpacity>
											
												<Image
													source={
														onkey == item.id && active == true
															? require('./../assets/images/up-icon-b.png')
															: require('./../assets/images/down-icon-b.png')
													}
													style={styles.icon}
												/>
											

										</View>
									</TouchableOpacity>

									{active == true && onkey == item.id ?
										<View
											style={{
												height: isChildSubCategory ? null : 0,
												overflow: 'hidden',
											}}>

											{childsubstatecatgory.map((item, index) => {
												return <View key={index}>
													<TouchableOpacity onPress={(event) => child_sub_category(item.id)}>
														<View style={styles.button}>

															<AppText style={styles.buttonText2}>{item.title}</AppText>

															<Image
																source={

																	require('./../assets/images/up-icon-b.png')
																}
																style={[
																	styles.icon1,
																	{ transform: [{ rotate: '90deg' }] },
																]}
															/>


														</View>
													</TouchableOpacity>


												</View>
											})
											}
										</View>
										: null}
								</View>
							})
							}
						</View>


					</View>

					{/* show lists */}
				
					<View style={styles.listcontainer} >
						<View>
							<AppText>{checksearch ? filterdata.length : showData.file?.length} items</AppText>
							<FlatList
								data={checksearch ? filterdata : showData?.file}
								renderItem={({ item }) => (
									<View style={styles.documentDoc}>
										<Image
											style={styles.iconImg}
											source={item.type != "My Uploads" ? require('../assets/images/pdf_icon.png') : { uri: item.url }}
										/>
										<View style={styles.textContainer}>
											<AppTextBoldSmall style={{marginTop: '-1%'}}> {item.title}  </AppTextBoldSmall>
											<View style={{ flex: 1, flexDirection: 'row' }}>
												<AppText >{item.file_size} |</AppText>
												<AppText >{item.type}</AppText>
											</View>
										</View>
										<TouchableOpacity onPress={() => handleFileDownload(item.url, false)}>
											<Image
												style={styles.iconDownload}
												source={require('../assets/images/download-b.png')}
											/>
										</TouchableOpacity>
									</View>
								)}
							/>
						</View>
					</View>
					
				</View>
</ScrollView>
			
			<AppFooter activePage={0} isPostSales={true} />
			<AppOverlayLoader isLoading={isLoading} />
			{showProgress && progress && progress > 0 ? (
				<AppProgressBar progress={progress} />
			) : null}
			{/* {documentuploadedModal ? <DocumentUploaded msg={"Your data uploaded successfully, but it will show after rm approval."
			} func={setDocumentuploadedModal} /> : null} */}

			{documentuploadedModal ? <DocumentUploaded msg={documentuploadedModalMSG} func={setDocumentuploadedModal} /> : null}
			{documentselectedlistModal  && filesToUpload.length>=1? <Selecteddocumentlistmodal func={setDocumentselectedlistModal} filesToUpload={filesToUpload} fileslength={filesToUpload.length} choosefileagain={()=> {setDocumentselectedlistModal(false); setTimeout(() => {
				setFileChooser(true)	
			}, 0); }} /*removeFile={removeFile}*/ onsubmitdocument={()=> api_Add_DOCUMENTS()} aftercancelresetdata={setFilesToUpload} /> : null}
			<Modal
				isVisible={fileChooser}
				style={{
					margin: 0,
					justifyContent: 'flex-end',
				}}
				onBackButtonPress={closeActionSheet}
				onBackdropPress={closeActionSheet}
				backdropColor="rgba(0,0,0,0.5)">
				<AppFileChooser
					onPress={(clickItem) => onClickFileChooserItem(clickItem)}

				/>
			</Modal>
		</Screen>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: 20,
		paddingVertical: 10,
		
	},
	pageTitle: {
		fontSize: appFonts.xxxlargeFontSize,
		fontFamily: appFonts.SourceSansProBold,
		color: colors.jaguar,
		textTransform: 'uppercase',
		marginBottom: "10%",

	},
	headerContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
	},
	iconAdd: {
		height: 26,
		width: 26,
		marginTop: 15,
	},
	dropdownDocument: {
		height: '100%',
		width: '100%',
		marginBottom: "5%",
		borderRightWidth: 0,
		borderLeftWidth: 0,
		borderTopWidth: 0,
		flex: 1,
		justifyContent: 'flex-start',
	},
	label: {
		fontSize: appFonts.largeBold,
		fontFamily: appFonts.SourceSansProSemiBold,
		marginBottom: "5%"
	},
	dropDownItem: {
		borderBottomWidth: 1,
		borderBottomColor: colors.lightGray,
		marginTop: "5%",

		justifyContent: 'flex-start',

	},
	search: {
		width: "100%",
		borderBottomWidth: 1,
		borderBottomColor: colors.borderGrey,
		marginBottom: 12,
		marginTop: 12,
		left: 4,
		paddingBottom: 15,
		fontSize: appFonts.largeBold,
		fontFamily: appFonts.SourceSansProSemiBold,
		paddingLeft: 13,
	},
	documentDoc: {
		flex: 1,
		flexDirection: "row",
		paddingRight: 20,
		marginTop: 20,
		alignItems: 'flex-start'
	},
	iconImg: {
		height: 30,
		width: 25,
		
	},
	iconDownload: {
		height: 18,
		width: 18,
	},
	textContainer: {
		width: "82%",
		marginHorizontal: 10
	},
	listcontainer: {
		marginTop: 5,
		marginBottom: 5,
		
	},

	buttonConatainer: {
		marginBottom: 20,
		borderWidth: 1,
		borderRadius: 1,
		borderColor: colors.lightGray,
		borderBottomWidth: 1,
		shadowColor: colors.lightGray,
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.7,
		shadowRadius: 3,
		elevation: 3,
		backgroundColor: colors.primary,
	},
	button: {
		height: 60,
		borderBottomWidth: 1, width: "100%", borderBottomColor: colors.borderGrey,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingHorizontal: 25,
	},
	buttonText: {
		fontSize:appFonts.largeBold,
		fontFamily: appFonts.SourceSansProSemiBold,
		color: colors.secondary,

	},
	buttonText1: {
		fontSize:appFonts.largeBold,
		fontFamily: appFonts.SourceSansProSemiBold,
		color: colors.secondary,
		paddingLeft: 15
	},
	buttonText2: {
		fontSize:appFonts.largeBold,
		fontFamily: appFonts.SourceSansProSemiBold,
		color: colors.secondary,
		paddingLeft: 33
	},
	icon: {
		width: 15,
		height: 15,

		zIndex: 9999,
	},
	icon1: {
		width: 15,
		height: 15,

		zIndex: 9999,
	},
	expandButton: {
		top: 20,
		width: 40,
		height: 40,
		alignSelf: 'center',
		justifyContent: 'center',
		alignItems: 'center',
		borderWidth: 1,
		borderRadius: 50,
		borderColor: colors.lightGray,
		borderBottomWidth: 1,
		shadowColor: colors.lightGray,
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.7,
		shadowRadius: 3,
		elevation: 3,
		backgroundColor: colors.primary,
		zIndex: 999999,
	},
	data: {

		width: '90%',
		fontSize: appFonts.largeBold,
		fontWeight: 'bold',
		color: colors.secondary,
	}
})

export default DocumentScreen;	