import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Image, FlatList, TouchableWithoutFeedback, TextInput, ScrollView, Picker, Modal, Alert } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation'
import colors from '../config/colors';
import appFonts from '../config/appFonts';
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppButton from '../components/ui/AppButton';
import AppFooter from '../components/ui/AppFooter';
import appSnakBar from '../utility/appSnakBar';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import { useSelector } from 'react-redux';
import apiLoanDisbursement from '../api/apiLoanDisbursement';
import appConstant from '../utility/appConstant';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Orientation from 'react-native-orientation';

const BankDetailsPreview = ({ navigation, route }) => {
    const [isLoading, setIsLoading] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [successMsg, setSuccessMsg] = useState('');
    // const dashboardData = useSelector((state) => state.dashboardData);
    // const propertyData = dashboardData.rmData;
    const routeParams = route.params;
    const formData = routeParams && routeParams.formData ? routeParams.formData : null;
    const formMode = routeParams && routeParams.formMode ? routeParams.formMode : null;
    console.log('formData', formData);

    const loginvalue = useSelector(
        (state) => state.loginInfo.loginResponse,
    );
    var userId = typeof loginvalue.data !== 'undefined' ? loginvalue.data.userid : '';

    const sanctionLetter = formData.files_attached ? formData.files_attached[0].file_name : formData.sanction_letter_name;

    useEffect(() => {
        Orientation.lockToPortrait();
    }, []);

    const postBankDetails = async () => {
        setIsLoading(true);
        if (formMode == "Edit") {
            try {
                const bankDetails = await apiLoanDisbursement.updateBankDetails(formData);
                setIsLoading(false);
                // console.log(bankDetails.data.msg);
                if (bankDetails.data.status == 200) {
                    setSuccessMsg(bankDetails.data.msg);
                    setModalVisible(true);
                } else {
                    appSnakBar.onShowSnakBar(
                        bankDetails.data.msg,
                        'LONG',
                    );
                }

            } catch (error) {
                console.log(error);
                setIsLoading(false);
                appSnakBar.onShowSnakBar(
                    appConstant.appMessage.APP_GENERIC_ERROR,
                    'LONG',
                );
            }

        } else {
            try {
                const bankDetails = await apiLoanDisbursement.saveBankDetails(formData);
                setIsLoading(false);
                if (bankDetails.data.status == 200) {
                    setSuccessMsg(bankDetails.data.msg);
                    setModalVisible(true);
                } else {
                    appSnakBar.onShowSnakBar(
                        bankDetails.data.msg,
                        'LONG',
                    );
                }
            } catch (error) {
                console.log(error);
                setIsLoading(false);
                appSnakBar.onShowSnakBar(
                    appConstant.appMessage.APP_GENERIC_ERROR,
                    'LONG',
                );
            }
        }
    }

    const closeSuccessPopup = () => {
        setModalVisible(false);
        navigation.navigate('DashboardScreen');
    }

    const onHandleRetryBtn = async () => {
        setIsLoading(true);
        if (formMode == "Edit") {
            try {
                const bankDetails = await apiLoanDisbursement.updateBankDetails(formData);
                setIsLoading(false);
                // console.log(bankDetails.data.msg);
                if (bankDetails.data.status == 200) {
                    setSuccessMsg(bankDetails.data.msg);
                    setModalVisible(true);
                } else {
                    appSnakBar.onShowSnakBar(
                        bankDetails.data.msg,
                        'LONG',
                    );
                }

            } catch (error) {
                console.log(error);
                setIsLoading(false);
                appSnakBar.onShowSnakBar(
                    appConstant.appMessage.APP_GENERIC_ERROR,
                    'LONG',
                );
            }

        } else {
            try {
                const bankDetails = await apiLoanDisbursement.saveBankDetails(formData);
                setIsLoading(false);
                if (bankDetails.data.status == 200) {
                    setSuccessMsg(bankDetails.data.msg);
                    setModalVisible(true);
                } else {
                    appSnakBar.onShowSnakBar(
                        bankDetails.data.msg,
                        'LONG',
                    );
                }
            } catch (error) {
                console.log(error);
                setIsLoading(false);
                appSnakBar.onShowSnakBar(
                    appConstant.appMessage.APP_GENERIC_ERROR,
                    'LONG',
                );
            }
        }
    };

    return (
        <Screen onRetry={onHandleRetryBtn}>
            <AppHeader />
            <ScrollView>
                <View style={styles.container}>
                    <AppTextBold style={styles.pageTitle}>Confirm Your Bank Loan Details</AppTextBold>

                    <View style={styles.item}>
                        <AppText style={styles.label}>Property</AppText>
                        <AppText style={styles.itemTxt}>{formData.property_name}</AppText>
                    </View>

                    <View style={styles.item}>
                        <AppText style={styles.label}>Bank</AppText>
                        <AppText style={styles.itemTxt}>{formData.bank_name}</AppText>
                    </View>

                    <View style={styles.item}>
                        <AppText style={styles.label}>Banker Email Id</AppText>
                        <AppText style={styles.itemTxt}>{formData.banker_email_id}</AppText>
                    </View>

                    <View style={styles.item}>
                        <AppText style={styles.label}>Loan Amount</AppText>
                        <AppText style={styles.itemTxt}>&#8377;{formData.loan_amount}</AppText>
                    </View>

                    <View style={styles.item}>
                        <AppText style={styles.label}>Loan Account Number</AppText>
                        <AppText style={styles.itemTxt}>{formData.loan_account_number}</AppText>
                    </View>
                    {sanctionLetter ? (
                        <View style={styles.item}>
                            <AppText style={styles.label}>Loan Sanction Letter</AppText>
                            <AppText style={styles.itemTxt}>
                                {appConstant.fileNameSanitize(sanctionLetter)}</AppText>
                        </View>
                    ) : (null)}

                    <View style={styles.button}>
                        <AppButton title="Edit"
                            color="primary"
                            textColor="secondary"
                            onPress={e => {
                                navigation.goBack();
                            }} />
                        <AppButton title="Confirm"
                            onPress={e => {
                                postBankDetails()
                            }} />
                    </View>
                </View>
            </ScrollView>

            <View style={styles.modalContainer}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <TouchableOpacity style={styles.close}
                                onPress={() => closeSuccessPopup()}
                            >
                                <Icon name={'close'} color={colors.charcoal} size={25} />
                            </TouchableOpacity>
                            {/* <Text style={styles.close} onPress={() => {
                                closeSuccessPopup();
                            }}>X</Text> */}
                            {/* <Text style={styles.modalText}>Terms &amp; Condition</Text> */}
                            <View>
                                <AppText>Your Service Request is successfully created!</AppText>
                                <AppText style={{ marginVertical: 10 }}>{successMsg}</AppText>
                                <AppText>Your Relationship Manager will authenticate the details provided.</AppText>

                            </View>
                        </View>

                    </View>

                </Modal>

            </View>

            <AppFooter activePage={0} isPostSales={true} />
            <AppOverlayLoader isLoading={isLoading} />
        </Screen>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    itemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 24,
        paddingVertical: 5,
        marginTop: 24,
        marginVertical: 20,
        backgroundColor: '#f7f7f7',
    },
    item: {
        borderColor: 'gray',
        borderBottomWidth: 1,
        marginBottom: 15,

    },
    label: {
        color: colors.gray3,
        fontSize: appFonts.normalFontSize,
    },
    itemTxt: {
        color: colors.jaguar,
        marginRight: 5,
        paddingBottom: 10

    },
    button: {
        marginVertical: '5%',
        // marginHorizontal: '5%',
    },
    arrow: { height: 16, width: 16 },
    searchBox: { flex: 1, },
    searchIcon: {
        padding: 10,
        margin: 5,
        height: 16,
        width: 16,
        resizeMode: 'stretch',
        alignItems: 'center',
    },

    searchSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        borderColor: 'gray',
        borderBottomWidth: 2,
        marginBottom: 20,
    },
    bankListItem: {
        paddingBottom: 5,
        paddingTop: 10,
        marginVertical: 10,
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },
    bankListTitle: {
    },
    bankCount: { marginTop: '5%', borderBottomColor: colors.veryLightGray, borderBottomWidth: 1, paddingVertical: 10 },
    disclaimerTitle: {
        color: colors.gray3,
        marginTop: '5%',
        borderBottomColor: colors.veryLightGray,
        borderBottomWidth: 1,
        marginVertical: 10
    },
    disclaimerText: {
        color: colors.gray3
    },
    bulletsFlex: {
        width: 20,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    bullets: {
        color: '#136ca5',
        fontSize: 32,
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
        height: '100%',
        width: '100%',
        backgroundColor: 'rgba(255,255,255,0.8)'
    },
    modalContainer: {
        alignSelf: 'center',
        height: 20,
        justifyContent: 'center',
        position: 'absolute',
        zIndex: 29
    },
    modalView: {
        backgroundColor: "white",
        width: "90%",
        borderRadius: 10,
        padding: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    modalText: {
        marginBottom: 20,
        textAlign: "center",
        fontSize: appFonts.xlargeFontSize,
        fontWeight: 'bold'
    },
    close: {
        left: "95%",
        color: 'grey',
        marginTop: -10
    },
    tabName: {
        paddingBottom: '3%',
        marginBottom: '5%',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    activeTab: {
        width: '33%',
        paddingVertical: '5%',
        borderBottomWidth: 3,
        borderBottomColor: colors.gray,
        alignItems: 'center',
    },

})
export default BankDetailsPreview;