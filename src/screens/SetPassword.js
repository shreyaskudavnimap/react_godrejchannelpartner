import React, {useState, useEffect} from 'react';
import { View, StyleSheet, KeyboardAvoidingView, Text, Image } from 'react-native';

import Screen from '../components/Screen';
import AppButton from '../components/ui/AppButton';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import ShowLoader from '../components/ui/ShowLoader';
import appSnakBar from '../utility/appSnakBar';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import apiLogin from './../api/apiLogin';
import { useNavigation } from '@react-navigation/native';

import appFonts from '../config/appFonts';
import AppHeader from '../components/ui/AppHeader';
import ShowPassword from '../components/showPassword';
import Orientation from 'react-native-orientation';

const SetPassword = ({props, route}) => {

    const routeParams = route.params;
    const userId =
    routeParams && routeParams.userId && routeParams.userId
      ? routeParams.userId
      : '';

    const userSFId =
    routeParams && routeParams.userSFId && routeParams.userSFId
    ? routeParams.userSFId
    : '';

    const actionType =
    routeParams && routeParams.actionType && routeParams.actionType
    ? routeParams.actionType
    : '';

    const [isLoading, setIsLoading] = useState(false);
    const [isApiLoading, setApiIsLoading] = useState(false);
    const [error, setError] = useState();
    const [Password, SetPassword] = useState("");
    const [ConfirmPwd, SetConfirmPwd] = useState("");

    const navigation = useNavigation();

    useEffect(() => {
        Orientation.lockToPortrait();
    }, []);
    

    const validatePassword = (pwd) => {
        var patt = new RegExp("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d!@#$%^*?%/'._\\-,\\\\`~]{6,14}$");
        var res = patt.test(pwd);
        if(pwd.length < 6){
            appSnakBar.onShowSnakBar(`Password should be minimum 6 characters long`, 'LONG');
            return false;
        }else{
            if(pwd.length > 14){
                appSnakBar.onShowSnakBar(`Password should be maximum 6 characters long`, 'LONG');
                return false;
            }else{
                if(res){
                    return true;
                }else{
                    appSnakBar.onShowSnakBar(`Password should be alpha-numeric`, 'LONG');
                    return false;
                }
            }
        }
    };

    const _setPassword = async() => {
        if(validatePassword(Password)){
            if(Password == ConfirmPwd){
                if(actionType == 'new_user_forget_password')
                    changePassForForgetPassword(userId);
                if(actionType == 'exist_cust_pan_login')
                    activatePanUserBySfid();
                if(actionType == 'forget_pwd_by_exist_cust')
                    changePassForForgetPassword(userId);
                if(actionType == 'forget_pwd_by_exist_cust_multiMob')
                    activatePanUserBySfid();
            }else{
                appSnakBar.onShowSnakBar(`Password and Confirm Password mismatch`, 'LONG');
            }
        }
    };

    const changePassForForgetPassword = async (user_id) => {
        const request = {
            uid: user_id,
            pass: Password,
        };
        setIsLoading(true);
        const result = await apiLogin.apiForUpdatePassword(request);
        setIsLoading(false);
        var status = result.data.status;
        console.log(JSON.stringify(result.data));
        if(status == 200){
            navigation.navigate('PasswordResetted');
        }else{
            appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
        }
    };

    const activatePanUserBySfid = async () => {
        const request = {
            user_sfid: userSFId,
        };
        setIsLoading(true);
        const result = await apiLogin.apiForActivateUserSfid(request);
        setIsLoading(false);
        var status = result.data.status;
        console.log(JSON.stringify(result.data));
        if(status == 200){
            changePassForForgetPassword(result.data.user_id);
        }else{
            appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
        }
    };


    return (
        <Screen>
            <AppHeader />

            

            <View style={styles.container}>
                <AppTextBold style={styles.title}>SET PASSWORD</AppTextBold>

                <View style={styles.contentContainer}>

                    <AppText>
                        Password
                     </AppText>

                    <View style={styles.pwContainer}>
                        <ShowPassword
                            title="Enter New Password"
                            colorText="black"
                            onChangeText={(e)=>{SetPassword(e);}}
                            value={Password}
                        />
                    </View>

                    <AppText>
                    Confirm Password
                        </AppText>

                    <View style={styles.pwContainer}>
                        <ShowPassword
                            title="Re-Enter New Password"
                            colorText="black"
                            onChangeText={(e)=>{SetConfirmPwd(e);}}
                            value={ConfirmPwd}
                        />
                    </View>

                </View>
            </View>

            <View style={styles.btnContainer}>
                <AppButton title="CONFIRM" onPress={() => _setPassword()} />
            </View>
            {
                (isLoading)
                ?
                //<ShowLoader/>
                <AppOverlayLoader isLoading={isLoading || isApiLoading} />
                :
                <View/>
            }
        </Screen>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        margin: 5,
    },

    title: {
        marginBottom: 15, fontSize: appFonts.xxxlargeFontSize,
        fontFamily: appFonts.SourceSansProBold,
        alignItems: "center"
    },
    contentContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        marginTop: "10%"
    },
    pwContainer: { height: 80,  paddingLeft:0, paddingRight:0, width:300 },
    btnContainer:{ marginHorizontal: 20, marginBottom: "0%", },
    backIcon: {
        height: 22,
        width: 65,
    },
})

export default SetPassword;

