import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';
import {View, ScrollView, StyleSheet, TouchableOpacity} from 'react-native';
import Screen from '../components/Screen';
import ScheduledVisit from '../components/ScheduledVisitComponent';
import RescheduleVisitModal from './modals/ReschedulePopUp';
import AppHeader from '../components/ui/AppHeader';
import AppFooter from '../components/ui/AppFooter';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import apiScheduledVisit from './../api/apiScheduledVisit';
import ScheduleVisitAdd from '../components/ScheduleVisitAdd';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import Orientation from 'react-native-orientation';

const TAB_TYPES = {
  SCHEDULED_VISITS: 'SCHEDULED_VISITS',
  ADD_NEW: 'ADD_NEW',
};

const MyVisits = (props) => {
  const bookingId =  props && props.route && props.route.params && props.route.params.bookingId || ''
  const visitTypeList = props && props.route && props.route.params && props.route.params.visitTypeList || ''
  const [tabType, setTabType] = useState(bookingId ? TAB_TYPES.ADD_NEW : TAB_TYPES.SCHEDULED_VISITS);
  const [isApiLoading, setApiIsLoading] = useState(false);
  const [isReshceduleVisitModalOpen, setRescheduleVisitModalOpen] = useState(
    false,
  );
  const [selectedScheduleVisit, setSelectedScheduleVisit] = useState({});
  const [callGetScheduleVisit, setCallGetScheduleVisit] = useState(
    new Date().getTime(),
  );
  const loginUserData = useSelector((state) => state.loginInfo.loginResponse);
  var userId = typeof loginUserData.data !== 'undefined' ? loginUserData.data.userid : '';

  const handleReschedulePress = (item) => {
    setSelectedScheduleVisit(item);
    setRescheduleVisitModalOpen(true);
  };

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  return (
    <Screen>
      <AppHeader />
      <View style={styles.container}>
        <AppTextBold style={styles.pageTitle}>All Visits</AppTextBold>

        <View style={styles.tabName}>
          <TouchableOpacity
            style={
              tabType === TAB_TYPES.ADD_NEW
                ? styles.activeTab
                : [styles.activeTab, {borderBottomWidth: 0}]
            }
            onPress={() => {
              setTabType(TAB_TYPES.ADD_NEW);
            }}>
            <View>
              <AppText
                style={
                  tabType === TAB_TYPES.ADD_NEW
                    ? {fontWeight: 'bold', fontSize:appFonts.largeBold}
                    : {fontSize:appFonts.largeBold}
                }>
                New Visit
              </AppText>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={
              tabType === TAB_TYPES.SCHEDULED_VISITS
                ? styles.activeTab
                : [styles.activeTab, {borderBottomWidth: 0}]
            }
            onPress={() => {
              setTabType(TAB_TYPES.SCHEDULED_VISITS);
            }}>
            <View>
              <AppText
                style={
                  tabType === TAB_TYPES.SCHEDULED_VISITS
                    ? {fontWeight: 'bold', fontSize:appFonts.largeBold}
                    : {fontSize:appFonts.largeBold}
                }>
                Scheduled Visits
              </AppText>
            </View>
          </TouchableOpacity>
        </View>

        {tabType === TAB_TYPES.ADD_NEW ? (
          <View style={{height: '100%'}}>
            <ScheduleVisitAdd userId={userId} bookingId={bookingId} visitTypeList={visitTypeList} setLoading={setApiIsLoading} />
          </View>
        ) : (
          <View />
        )}

        {tabType === TAB_TYPES.SCHEDULED_VISITS ? (
          <ScheduledVisit
            setApiIsLoading={setApiIsLoading}
            userId={userId}
            handleReschedulePress={handleReschedulePress}
            callGetScheduleVisit={callGetScheduleVisit}
          />
        ) : (
          <View />
        )}

        {isReshceduleVisitModalOpen ? (
          <RescheduleVisitModal
            toggleModal={setRescheduleVisitModalOpen}
            isOpen={isReshceduleVisitModalOpen}
            userId={userId}
            selectedScheduleVisit={selectedScheduleVisit}
            setLoading={setApiIsLoading}
            setCallGetScheduleVisit={setCallGetScheduleVisit}
          />
        ) : (
          <View />
        )}
      </View>
      <AppFooter activePage={4} isPostSales={true} />
      <AppOverlayLoader isLoading={isApiLoading} />
    </Screen>
  );
};

export default MyVisits;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '6%',
    paddingBottom: '6%',
  },
  pageTitle: {
    fontSize: appFonts.xxlargeFontSize,
    color: colors.secondary,
    textTransform: 'uppercase',
  },
  tabName: {
    paddingBottom: '1%',
    marginBottom: '1%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  activeTab: {
    width: '50%',
    paddingVertical: '5%',
    borderBottomWidth: 3,
    borderBottomColor: colors.gray,
    alignItems: 'center',
  },
});
