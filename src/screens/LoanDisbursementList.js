import React, { useEffect, useState } from 'react';
import {
  View,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback, Modal as ModalNative, SafeAreaView, Alert
} from 'react-native';
import Modal from 'react-native-modal';
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppFooter from '../components/ui/AppFooter';
import AppText from '../components/ui/ AppText';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppButton from '../components/ui/AppButton';
import apiLoanDisbursement from '../api/apiLoanDisbursement';
import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';
import { useSelector } from 'react-redux';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import AppProgressBar from '../components/ui/AppProgressBar';
import useFileDownload from '../hooks/useFileDownload';
import { FlatList } from 'react-native-gesture-handler';
import AppFileChooser from './../components/actionSheet/AppFileChooser';
import appFileChooser from './../utility/appFileChooser';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Orientation from 'react-native-orientation';

const LoanDisbursementList = ({ navigation, route }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const routeParams = route.params;
  const booking_id = routeParams && routeParams.booking_id ? routeParams.booking_id : '';
  const [bookingId, setBookingId] = useState(booking_id);
  const [coverLetterTitle, setCoverLetterTitle] = useState("Unsigned Cover Letter");
  let unsignedLetter = encodeURI(appConstant.buildInstance.baseUrl + "loan_disbursement_hdfc_download/" + bookingId);
  const [coverLetterUrl, setCoverLetterUrl] = useState(unsignedLetter);
  const [architechtureCertificate, setArchitechtureCertificate] = useState(null);
  const [invoiceList, setInvoiceList] = useState([]);
  const [receiptList, setReceiptList] = useState([]);

  const loginvalue = useSelector(
    (state) => state.loginInfo.loginResponse,
  );
  const userId = typeof loginvalue.data !== 'undefined' ? loginvalue.data.userid : '';

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  useEffect(() => {
    getSignedCoverLetter(false);
    getArchitectureCertificate();
  }, []);

  const getSignedCoverLetter = async (hideLoader = true) => {
    setIsLoading(true);
    try {
      const requestData = {
        user_id: userId,
        booking_id: bookingId
      };
      const response = await apiLoanDisbursement.getSignedCoverLetter(requestData);
      const resultData = response.data;
      if (hideLoader) {
        setIsLoading(false);
      }
      if (resultData.status == "200" && resultData.file_url) {
        setCoverLetterTitle("Signed Cover Letter");
        setCoverLetterUrl(resultData.file_url);
      }
    } catch (error) {
      console.log(error);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  }

  const getArchitectureCertificate = async () => {
    setIsLoading(true);
    try {
      const monthList = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];
      const monthIndx = new Date().getMonth();
      const year = new Date().getFullYear();
      const month = monthList[monthIndx];
      const requestData = {
        user_id: userId,
        booking_id: bookingId,
        year: year,
        month: month,
      };
      const response = await apiLoanDisbursement.getArchitectureCertificate(requestData);
      const resultData = response.data;
      // setIsLoading(false);
      if (resultData.status == "200") {
        if (resultData.data.certificate_url) {
          setArchitechtureCertificate(resultData.data.certificate_url);
        }
      }
    } catch (error) {
      console.log(error);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
    getInvoiceReceiptList();
  }

  const getInvoiceReceiptList = async () => {
    // setIsLoading(true);
    try {
      const requestData = {
        user_id: userId,
        booking_id: bookingId
      };
      const response = await apiLoanDisbursement.getInvoiceReceiptList(requestData);
      const resultData = response.data;
      if (resultData.status == "200" && resultData.data) {
        let invoiceData = [];
        if (typeof resultData.data.invoice !== "undefined" && resultData.data.invoice.length > 0) {
          let invDate = resultData.data.invoice[0].date;
          resultData.data.invoice.map((item) => {
            if (invDate == item.date) {
              invoiceData.push({
                name: item.name,
                url: item.url,
              });
            }
          });
          setInvoiceList(invoiceData);
        }
        if (typeof resultData.data.receipt !== "undefined" && resultData.data.receipt.length > 0) {
          let receiptData = [];
          let receiptDate = resultData.data.receipt[0].date;
          resultData.data.receipt.map((item) => {
            if (receiptDate == item.date) {
              receiptData.push({
                name: item.name,
                url: item.url,
              });
            }
          });
          setReceiptList(receiptData);
        }
      }

      setIsLoading(false);

    } catch (error) {
      console.log(error);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }

  }

  const { progress, showProgress, error, isSuccess, checkPermission, } = useFileDownload();
  const downloadAttachment = (fileName, fileUrl) => {
    if (coverLetterTitle == "Unsigned Cover Letter") {
      fileName = "CoverLetter.pdf";
    }

    if (fileName) {
      const ext = fileName.split('.')[0];
      if (ext) {
        downloadFile(fileUrl, fileName, ext);
      }
    } else {
      let fileExt = appConstant.getExtention(fileUrl);

      const fName = appConstant.getFileName(fileUrl);
      if (fileExt[0] && fName) {
        downloadFile(fileUrl, fName, fileExt[0]);
      }
    }
  }

  const downloadFile = async (imgPath, fileName, fileExtension) => {
    try {
      await checkPermission(imgPath, fileName, fileExtension);
    } catch (error) {
      appSnakBar.onShowSnakBar(error.message, 'LONG');
    }
  };
  const [fileChooserResponse, setFileChooserResponse] = useState(null);

  const [fileChooser, setFileChooser] = useState(false);
  const closeActionSheet = () => setFileChooser(false);

  const onClickFileChooserItem = async (clickItem) => {
    switch (clickItem) {
      case 'Remove':
        setFileChooser(false);
        setFileChooserResponse(null);
        break;

      case 'Gallery':
        appFileChooser.onLaunchImageLibrary((fileChooserResponse) => {
          setFileChooser(false);
          if (fileChooserResponse.didCancel) {
            setFileChooserResponse(null);
          } else {
            setFileChooserResponse(fileChooserResponse);
          }
        });
        break;

      case 'Camera':
        appFileChooser.onLaunchCamera((fileChooserResponse) => {
          setFileChooser(false);
          if (fileChooserResponse.didCancel) {
            setFileChooserResponse(null);
          } else {
            setFileChooserResponse(fileChooserResponse);
          }
        });
        break;

      case 'Phone':
        try {
          const chooserResponse = await appFileChooser.onSelectFile();
          const dataUriResponse = await appFileChooser.toBase64(chooserResponse.uri);
          chooserResponse.data = dataUriResponse;
          setFileChooser(false);
          setFileChooserResponse(chooserResponse);

          // const dataUriResponse = await appFileChooser.toBase64(fileChooserResponse.uri);
          // console.log(dataUriResponse)
        } catch (error) {
          console.log('Error onSelectFile : ', error);
          setFileChooserResponse(null);
        }
        break;
    }

    uploadSignedCoverLetter();
  };

  const validateFileSize = (fileSize) => {
    let msg = '';
    appFileChooser.validateFileSize(fileSize, (fileChooserResponse) => {
      msg = fileChooserResponse
        ? ''
        : 'File size should  be less than 5MB';
    });
    return msg;
  };

  useEffect(() => {
    uploadSignedCoverLetter();
  }, [fileChooserResponse]);

  const uploadSignedCoverLetter = async () => {
    if (fileChooserResponse) {
      let fileSize = fileChooserResponse.fileSize ? fileChooserResponse.fileSize : fileChooserResponse.size
      let invalidSize = validateFileSize(fileSize);
      if (invalidSize) {
        appSnakBar.onShowSnakBar(
          invalidSize,
          'LONG',
        );
      } else {
        const requestData = {
          user_id: userId,
          booking_id: bookingId,
        };
        const fileBase64 = fileChooserResponse.data;
        const fileName = fileChooserResponse.name
          ? fileChooserResponse.name
          : fileChooserResponse.uri.split('/').pop();
        requestData["files_attached"] = [];
        requestData["files_attached"].push({
          file_base64_encoded: fileBase64,
          file_name: fileName,
        });
        setIsLoading(true);
        try {
          const response = await apiLoanDisbursement.uploadSignedCoverLetter(requestData);
          const resultData = response.data;
          setIsLoading(false);
          if (resultData.status == "200") {
            getSignedCoverLetter();
          }
        } catch (error) {
          console.log(error);
          appSnakBar.onShowSnakBar(
            appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      }
    }
  };

  const sendEmail = async () => {
    const requestData = {
      user_id: userId,
      booking_id: bookingId,
    };
    let emailAttachment = {
      coverletter: coverLetterUrl,
      architectureCertificate: architechtureCertificate,
    };
    let invoiceArr = [];
    invoiceList.forEach((file) => {
      invoiceArr.push(file.url);
    });
    let receiptArr = [];
    receiptList.forEach((file) => {
      receiptArr.push(file.url);
    });
    emailAttachment["invoiceList"] = invoiceArr;
    emailAttachment["receiptList"] = receiptArr;
    requestData["attachment"] = emailAttachment;
    console.log(requestData);
    setIsLoading(true);
    try {
      const response = await apiLoanDisbursement.sendBankerEmail(requestData);
      const resultData = response.data;
      setIsLoading(false);
      if (resultData.status == "200") {
        // setModalVisible(true);
        Alert.alert(
          '',
          "Email sent successfully!\nYour loan disbursement has been sent to your bank.",
          [
            ,
            {
              text: 'Close',
              onPress: () => {
                console.log('I Accept Pressed');
                navigation.navigate('DashboardScreen');
              },
            }
          ],
          { cancelable: false },
        );
      }
    } catch (error) {
      console.log(error);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }

  };

  const closeSuccessPopup = () => {
    setModalVisible(false);
    navigation.navigate('DashboardScreen');
  }


const onHandleRetryBtn = () => {
  getSignedCoverLetter(false);
  getArchitectureCertificate();
};

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />
      {!isLoading ? (
        <>
          <ScrollView>
            <View style={styles.container}>
              <AppText style={styles.pageTitle}>Loan Disbursement</AppText>

              <View style={{ paddingBottom: 20 }}>
                <View style={styles.coverLetterCon}>
                  <TouchableOpacity onPress={() => {
                    downloadAttachment("", coverLetterUrl);
                  }}>
                    <AppText style={{ fontSize:appFonts.largeBold }}>{coverLetterTitle}</AppText>
                  </TouchableOpacity>
                  <TouchableOpacity style={{ flexDirection: 'row' }}
                    onPress={() => {
                      setFileChooser(true)
                    }}>
                    <Image
                      source={require('./../assets/images/attachment-icon.png')}
                      style={styles.attachIcon}
                    />
                    <AppText style={{ color: '#000' }}>
                      Upload Signed Letter
                </AppText>
                  </TouchableOpacity>
                </View>
                <AppText style={{ color: colors.gray }}>
                  (pdf, jpg, jpeg, png under 5 MB)
            </AppText>
              </View>
              {architechtureCertificate ? (
                <TouchableOpacity onPress={() => {
                  downloadAttachment("Architecture_Certificate.pdf", architechtureCertificate);
                }}>
                  <AppText style={{ fontSize:appFonts.largeBold, paddingBottom: 20 }}>
                    Architecture Certificate
            </AppText>
                </TouchableOpacity>
              ) : null}

              <SafeAreaView style={{ flex: 1 }}>
                <FlatList
                  data={invoiceList}
                  renderItem={({ item, index }) => (
                    <TouchableOpacity onPress={() => {
                      downloadAttachment("", item.url);
                    }}>
                      <AppText style={{ fontSize: appFonts.largeBold, paddingBottom: 20 }}>
                        {item.name}
                      </AppText>
                    </TouchableOpacity>
                  )}
                />
              </SafeAreaView>
              
              <SafeAreaView style={{ flex: 1 }}>
                <FlatList
                  data={receiptList}
                  renderItem={({ item, index }) => (
                    <TouchableOpacity onPress={() => {
                      downloadAttachment("", item.url);
                    }}>
                      <AppText style={{ fontSize:appFonts.largeBold, paddingBottom: 20 }}>
                        {item.name}
                      </AppText>
                    </TouchableOpacity>
                  )}
                />
              </SafeAreaView>
              <AppButton title="PROCEED"
                onPress={() => { sendEmail(); }} />
            </View>
          </ScrollView>

          <AppFooter isPostSales={true} />
        </>
      ) : null}
      <AppOverlayLoader isLoading={isLoading} />
      {showProgress && progress && progress > 0 ? (
        <AppProgressBar progress={progress} />
      ) : null}
      <Modal
        isVisible={fileChooser}
        style={{
          margin: 0,
          justifyContent: 'flex-end',
        }}
        onBackButtonPress={closeActionSheet}
        onBackdropPress={closeActionSheet}
        backdropColor="rgba(0,0,0,0.5)">
        <AppFileChooser
          onPress={(clickItem) => onClickFileChooserItem(clickItem)}
        />
      </Modal>

      {/* <View style={styles.modalContainer}>
        <ModalNative
          animationType="slide"
          transparent={true}
          visible={modalVisible}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>              
              <TouchableOpacity style={styles.close}
                onPress={() => closeSuccessPopup()}
              >
                <Icon name={'close'} color={colors.charcoal} size={25} />
              </TouchableOpacity>
              
              <View>
                <AppText>Email Sent Successfully!</AppText>
                <AppText style={{ marginVertical: 10 }}>Your loan disbursement has been sent to your bank.</AppText>
              </View>
            </View>
          </View>
        </ModalNative>
      </View> */}
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '6%',
    justifyContent: 'center',
    marginBottom: 20,
  },
  pageTitle: {
    fontSize: appFonts.xxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    textTransform: 'uppercase',
    paddingTop: '5%',
    paddingBottom: '8%',
  },
  attachIcon: {
    width: 20,
    height: 20,
  },
  coverLetterCon: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  modalContainer: {
    alignSelf: 'center',
    height: 20,
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 29
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
    height: 450,
    width: '100%'
  },
  modalView: {
    backgroundColor: "white",
    width: "90%",
    borderRadius: 10,
    padding: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  modalText: {
    marginBottom: 20,
    textAlign: "center",
    fontSize: appFonts.xlargeFontSize,
    fontWeight: 'bold'
  },
  close: {
    left: "95%",
    color: 'grey',
  }
});

export default LoanDisbursementList;
