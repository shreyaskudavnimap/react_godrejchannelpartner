import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  Image,
  Modal,
} from 'react-native';
import HTML from 'react-native-render-html';

import {useDispatch, useSelector} from 'react-redux';

import * as projectCmsAction from './../store/actions/projectCmsAction';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppButton from '../components/ui/AppButton';
import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import AppFooter from '../components/ui/AppFooter';
import AppVideoThumbView from '../components/ui/AppVideoThumbView';
import AppMenuItemDownload from '../components/ui/AppMenuItemDownload';
import VideoModalScreen from './modals/VideoModalScreen';
import useFileDownload from '../hooks/useFileDownload';
import AppProgressBar from '../components/ui/AppProgressBar';
import Orientation from 'react-native-orientation';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const ProjectCmsScreen = ({navigation, route}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [errors, setError] = useState();
  const [videoUrl, setVideoUrl] = useState('');
  const [modalVisible, setModalVisible] = useState(false);

  const routeParams = route.params;
  const projectId =
    routeParams && routeParams.item && routeParams.item.proj_id
      ? routeParams.item.proj_id
      : '';

  const featuredMenuItem =
    routeParams && routeParams.featuredMenuItem
      ? routeParams.featuredMenuItem
      : '';

  const dispatch = useDispatch();

  const projectDetailsData = useSelector((state) => state.projectDetails);
  const projectDetails = projectDetailsData.projectDetails;

  const projectCmsData = useSelector(
    (state) => state.projectCMSData.cmsData[0],
  );

  const {
    progress,
    showProgress,
    error,
    isSuccess,
    checkPermission,
  } = useFileDownload();

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  useEffect(() => {
    getProjectCmsData();
  }, [dispatch]);

  const getProjectCmsData = useCallback(async () => {
    if (!projectId || !featuredMenuItem) {
      navigation.goBack();
      return;
    }

    setIsLoading(true);
    setError(null);
    try {
      await dispatch(
        projectCmsAction.getProjectCMSData(projectId, featuredMenuItem),
      );
      setIsLoading(false);
    } catch (err) {
      appSnakBar.onShowSnakBar(err.message, 'LONG');
      navigation.goBack();
      setError(err.message);
      setIsLoading(false);
    }
  }, [dispatch, setIsLoading, setError, isLoading, projectId]);

  const onHandleBookNow = () => {
    if (projectDetails) {
      if (projectDetails.field_is_available_for_booking == '0') {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.NO_BOOKING_AVAILABLE_ALERT_MESSAGE,
          'LONG',
        );
      } else if (projectDetails.field_is_available_for_booking == '1') {
        if (projectDetails.eoi_enabled == '0') {
          navigation.navigate('FloorScreen', {
            item: routeParams.item,
          });
        } else {
          navigation.navigate('EOIFloorScreen', {
            item: {
              ...routeParams.item,
              preference_combination: projectDetails['preference_combination']
                ? projectDetails['preference_combination']
                : '',
              project_sfdc_id: projectDetails['field_external_project_id']
                ? projectDetails['field_external_project_id']
                : '',
            },
          });
        }
      }
    }
  };

  const onHandleVideoModal = (videoUrl) => {
    setVideoUrl(videoUrl);
    setModalVisible(true);
  };

  const getFileName = (mediaUrl) => {
    let mediaName = '';
    if (mediaUrl !== '' && mediaUrl !== null) {
      let mediaArr = mediaUrl.split('/');
      mediaName = mediaArr[mediaArr.length - 1];
    }
    return mediaName;
  };

  const goToSiteVisitScreen = () => {
    navigation.navigate('PresaleVisitScreen', {projectId: projectId});
  };

  const onDownloadFile = (fileName, fileUrl) => {
    if (fileName && fileName != '' && fileUrl && fileUrl != '') {
      const ext = fileName.split('.')[0];
      if (ext) {
        downloadFile(fileUrl, fileName, ext);
      }
    }
  };

  const downloadFile = async (imgPath, fileName, fileExtension) => {
    try {
      await checkPermission(imgPath, fileName, fileExtension);
    } catch (error) {
      appSnakBar.onShowSnakBar(error.message, 'LONG');
    }
  };

  const onHandleRetryBtn = () => {
    setModalVisible(false);
    getProjectCmsData();
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />
      {isLoading ? (
        <View style={styles.container}></View>
      ) : (
        <ScrollView>
          <View style={styles.container}>
            {featuredMenuItem === 'why_project' ? (
              <>
                {projectCmsData && projectCmsData.project_title ? (
                  <AppText style={styles.pageTile}>
                    WHY {projectCmsData.project_title}?
                  </AppText>
                ) : null}
              </>
            ) : (
              <>
                {projectCmsData && projectCmsData.cms_title ? (
                  <AppText style={styles.pageTile}>
                    {projectCmsData.cms_title}
                  </AppText>
                ) : null}
              </>
            )}

            <View style={styles.contentViewContainer}>
              {projectCmsData && projectCmsData.cms_body ? (
                <HTML
                allowFontScaling={false} 
                  tagsStyles={{
                    p: {
                      fontSize: appFonts.largeFontSize,
                      lineHeight: 24,
                      textAlign: 'justify',
                      fontFamily: appFonts.SourceSansProRegular,
                    },
                  }}
                  ignoredStyles={['font-family']}
                  html={projectCmsData.cms_body}
                  imagesMaxWidth={windowWidth * 0.5}
                />
              ) : null}

              {projectCmsData &&
                projectCmsData.details &&
                projectCmsData.details.length > 0 &&
                projectCmsData.details.map((data, index) => (
                  <View style={styles.contentViewItem} key={index}>
                    <AppText style={styles.contentTile}>{data.title}</AppText>
                    <HTML
                    allowFontScaling={false} 
                      tagsStyles={{
                        p: {
                          fontSize: appFonts.largeFontSize,
                          textAlign: 'justify',
                          lineHeight: 28,
                          fontFamily: appFonts.SourceSansProRegular,
                        },
                      }}
                      ignoredStyles={['font-family']}
                      html={data.description}
                      imagesMaxWidth={windowWidth * 0.5}
                    />

                    {data.media_type &&
                    data.media_type == 'image' &&
                    data.media_url ? (
                      <View style={styles.imgContainer}>
                        <Image
                          style={styles.contentImage}
                          source={{
                            uri: data.media_url,
                          }}
                        />
                      </View>
                    ) : null}

                    {data.media_type &&
                    data.media_type == 'video' &&
                    data.media_url &&
                    data.media_thumb_url ? (
                      <View style={styles.contentVideo}>
                        <AppVideoThumbView
                          imageUri={data.media_thumb_url}
                          onPress={() => {
                            onHandleVideoModal(data.media_url);
                          }}
                        />
                      </View>
                    ) : null}

                    {data.media_type &&
                    data.media_type == 'pdf' &&
                    data.media_url &&
                    getFileName(data.media_url) ? (
                      <View style={styles.contentPdf}>
                        <AppMenuItemDownload
                          title={getFileName(data.media_url)}
                          onPress={() =>
                            onDownloadFile(
                              getFileName(data.media_url),
                              data.media_url,
                            )
                          }
                        />
                      </View>
                    ) : null}
                  </View>
                ))}
            </View>

            <View style={styles.btnContainer}>
              <AppButton
                title="SCHEDULE A VISIT"
                color="primary"
                textColor="secondary"
                onPress={goToSiteVisitScreen}
              />

              {projectDetails &&
                projectDetails.field_is_available_for_booking && (
                  <AppButton
                    opacity={
                      projectDetails.field_is_available_for_booking == '0'
                        ? 0.6
                        : 1
                    }
                    title={
                      projectDetails.eoi_enabled &&
                      projectDetails.eoi_enabled === '1'
                        ? 'Pre-book Now'
                        : 'Book Now'
                    }
                    onPress={onHandleBookNow}
                  />
                )}
            </View>
          </View>
        </ScrollView>
      )}

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        supportedOrientations={['portrait', 'landscape']}
        onRequestClose={() => {
          Orientation.lockToPortrait();
          setModalVisible(!modalVisible);
        }}>
        <VideoModalScreen
          onPress={() => {
            Orientation.lockToPortrait();
            setModalVisible(!modalVisible);
          }}
          projectName={
            projectDetails && projectDetails.title ? projectDetails.title : ''
          }
          videoUri={videoUrl}
          setIsLoading={setIsLoading}
        />
      </Modal>

      <AppFooter activePage={0} isPostSales={false} />

      {showProgress ? <AppProgressBar progress={progress} /> : null}

      <AppOverlayLoader isLoading={isLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 18,
    paddingVertical: 10,
  },
  pageTile: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: 5,
    textTransform: 'uppercase',
  },
  contentViewContainer: {
    marginTop: 25,
  },
  contentViewItem: {
    marginTop: 20,
    justifyContent: 'center',
    // alignItems: 'center',
  },
  contentTile: {
    fontSize: appFonts.xxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: 15,
    textTransform: 'uppercase',
    lineHeight: 32,
  },
  imgContainer: {
    backgroundColor: '#e1e4e8',
    marginTop: 25,
    width: '100%',
    height: windowHeight / 2.75,
  },
  contentImage: {
    height: windowHeight / 2.75,
    width: '100%',
  },
  contentVideo: {
    height: windowHeight / 2.75,
    width: '100%',
    marginTop: 25,
  },
  contentPdf: {
    width: '100%',
    marginTop: 25,
  },
  btnContainer: {
    marginTop: 30,
    marginBottom: 25,
  },
});

export default ProjectCmsScreen;
