import React, { useEffect, useState } from 'react';
import {
  View, StyleSheet, ScrollView, TextInput,
  Dimensions, Image, Text, TouchableOpacity, PermissionsAndroid
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import AppButton from '../components/ui/AppButton';
import AppHeader from '../components/ui/AppHeader';
import AppTextBold from '../components/ui/AppTextBold';
import AppText from '../components/ui/ AppText';
import Screen from '../components/Screen';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppFooter from '../components/ui/AppFooter';
import * as normStyle from '../styles/StyleSize';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import apiLogin from './../api/apiLogin';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import appSnakBar from '../utility/appSnakBar';
import { useNavigation } from '@react-navigation/native';
import appAlert from '../utility/appAlert';
import Orientation from 'react-native-orientation';

const { height } = Dimensions.get('window');



const ChangeUserName = ({ props, route }) => {

  const routeParams = route.params;
  const fname =
    routeParams && routeParams.fname && routeParams.fname
      ? routeParams.fname
      : '';

  const lname =
    routeParams && routeParams.lname && routeParams.lname
      ? routeParams.lname
      : '';

  const userId =
    routeParams && routeParams.userId && routeParams.userId
      ? routeParams.userId
      : '';

  const [firstName, SetFirstName] = useState("");
  const [lastName, SetlastName] = useState("");
  const [dob, SetDob] = useState("");
  const [isApiLoading, setApiIsLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [docBase64, setDocBase64] = useState("");
  const [docId, setDocId] = useState("");
  const [docName, setDocName] = useState("");
  const [docSize, setDocSize] = useState("");
  const [docMime, setDocMime] = useState("");
  const [hideAttachment, setHideAttachment] = useState(true);
  const [disableButton, setDisableButton] = useState(true);
  const navigation = useNavigation();

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);


  useEffect(() => {
    _getUserInfo();
  }, [loginvalue]);

  const loginvalue = useSelector(
    (state) => state.loginInfo.loginResponse,
  );

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );
  const menuType = useSelector((state) => state.menuType.menuType);
  const goBack = () => {

    navigation.goBack();
  };

  const _getUserInfo = () => {
    SetFirstName(fname);
    SetlastName(lname);
  };

  const chooseImage = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [
          DocumentPicker.types.images,
          DocumentPicker.types.pdf,
        ],
        readContent: true,
      });
      console.log(
        res.uri,
        res.type, // mime type
        res.name,
        res.size,
        res.fileCopyUri
      );
      var fileType = (res.type).substring((res.type).lastIndexOf('/') + 1);
      console.log(fileType);
      if (fileType == 'png' || fileType == 'PNG' || fileType == 'jpeg' || fileType == 'JPEG' || fileType == 'pdf'
        || fileType == 'PDF' || fileType == 'JPG' || fileType == 'jpg') {
        setDocName(res.name);
        setDocSize(res.size);
        setDocMime((res.type).substring((res.type).lastIndexOf('/') + 1));
        _getBase64(res.uri);
      } else {
        appSnakBar.onShowSnakBar('Only image and pdf file is accepted', 'LONG');
      }
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  const _setFirstName = (e, ln) => {
    SetFirstName(e);
    if (fname === e.trim() && lname === ln) {
      setHideAttachment(true);
      setDisableButton(true);
      setDocBase64("");
      setDocId("");
      setDocName("");
      setDocSize("");
      setDocMime("");
    } else {
      setHideAttachment(false);
      setDisableButton(false);
    }
  };

  const _setLastName = (e, fn) => {
    SetlastName(e);
    if (lname === e.trim() && fname === fn) {
      setHideAttachment(true);
      setDisableButton(true);
      setDocBase64("");
      setDocId("");
      setDocName("");
      setDocSize("");
      setDocMime("");
    } else {
      setHideAttachment(false);
      setDisableButton(false);
    }
  };

  const _getBase64 = (uri) => {
    RNFetchBlob.fs.readFile(uri, 'base64')
      .then((files) => {
        setDocBase64(files);
      })
  };

  const _changeName = async () => {
    if (firstName == '' || lastName == '') {
      appSnakBar.onShowSnakBar('Enter First and Last name', 'LONG');
      return null;
    }
    if (lname === lastName && fname === firstName) {
      return null;
    } else {
      if (docBase64 != '' && docName != '' && docSize != '' && docMime != '') {
        const request = {
          device_id: deviceUniqueId,
          new_value: firstName + " " + lastName,
          old_value: fname + " " + lname,
          type: "name",
          user_id: userId,
          attachedFiles: {
            file: 'docBase64',
            file_id: 123,
            file_name: docName,
            file_size: docSize,
            mimeType: docMime,
          }
        };
        console.log(request);
        setIsLoading(true);
        const result = await apiLogin.apiForUpdateProfileData(request);
        setIsLoading(false);
        var status = result.data.status;
        console.log(result.data);
        if (status == 200) {
          appAlert.showAppAlert("Service Request", "Request submitted successfully. Your service request number is " + result.data.request_id);
          goBack();
        } else {
          appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
        }
      } else {
        appSnakBar.onShowSnakBar('Attach address proof document', 'LONG');
      }
    }
  };

  const _removeDoc = () => {
    setDocName(""); setDocBase64(""); setDocSize(""); setDocMime();
  };


const onHandleRetryBtn = () => {
};

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />

    <ScrollView>
      <View style={styles.container}>

        <View style={{ width: '100%', alignItems: 'flex-start' }}>
          <AppText style={styles.pageTitle}>Edit Name</AppText>
        </View>


        <View style={{ width: '100%', alignItems: 'flex-start' }}>
          <Text style={{ marginLeft: '5%', color: colors.gray }}>First Name *</Text>
        </View>
        <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
          <TextInput
            style={styles.nameStyle}
            placeholder={"Enter first name"}
            placeholderTextColor={colors.gray}
            value={firstName}
            color={colors.secondaryDark}
            onChangeText={e => {
              _setFirstName(e, lastName);
            }}

          />
        </View>

        <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(20) }}>
          <Text style={{ marginLeft: '5%', color: colors.gray }}>Last Name *</Text>
        </View>
        <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
          <TextInput
            style={styles.nameStyle}
            placeholder={"Enter last name"}
            placeholderTextColor={colors.gray}
            value={lastName}
            color={colors.secondaryDark}
            //onBlur={() => {handleMobileChange();}}
            onChangeText={e => {
              _setLastName(e, firstName);
            }}

          />
        </View>

          <View style={styles.button}>
            <AppButton
               color={disableButton ? 'charcoal' : 'secondary'}
               disabled={disableButton}
               textColor = {disableButton ? 'primary' : 'primary'}
              title="Save"
              onPress={() => { _changeName() }}
            />
          </View>
        {/* </View> */}

      </View>
    </ScrollView>
      <AppFooter activePage={0} isPostSales={menuType == 'postsale' ? true : false} />
      <AppOverlayLoader isLoading={isLoading || isApiLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  pageTitle: {
    fontSize: normStyle.normalizeWidth(20),
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginLeft: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(40),
  },
  nameStyle: {
    width: '90%', height: normStyle.normalizeWidth(50),
    borderBottomWidth: 0.5, fontSize: normStyle.normalizeWidth(18),
    fontWeight: 'bold',
  },
  editImg: {
    width: normStyle.normalizeWidth(20),
    height: normStyle.normalizeWidth(20),
    marginLeft: normStyle.normalizeWidth(10),
  },
  cameraStyle: {
    width: normStyle.normalizeWidth(25),
    height: normStyle.normalizeWidth(25),
  },
  button: {
    // width: normStyle.normalizeWidth(100),
    // height: normStyle.normalizeWidth(0),
    alignItems: 'center', justifyContent: 'center',
    marginHorizontal:15,
    paddingBottom: normStyle.normalizeWidth(40),
    paddingTop: normStyle.normalizeWidth(40),
  },
  cancelStyle: {
    width: normStyle.normalizeWidth(15),
    height: normStyle.normalizeWidth(15),
  },
  docStyle: {
    width: '90%', marginTop: normStyle.normalizeWidth(10), flexDirection: 'row',
    backgroundColor: colors.gray, padding: normStyle.normalizeWidth(10)
  }
});

export default ChangeUserName;
