import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Linking,
  Dimensions,
  Alert,
  TouchableWithoutFeedback,
} from 'react-native';

import DeviceInfo from 'react-native-device-info';

import {USER_LOGIN, clearRMData} from '../store/actions/userLoginAction';
import {clearBookingEnquiryData} from '../store/actions/bookingEnquiryAction';
import * as badgeCountAction from './../store/actions/badgeCountAction';
import * as menuTypeAction from './../store/actions/menuTypeAction';

import Screen from '../components/Screen';
import AppText from '../components/ui/ AppText';
import AppFooter from '../components/ui/AppFooter';
import UserDetailsRowComponent from '../components/UserDetailsRowComponent';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import {useDispatch, useSelector} from 'react-redux';
import appSnakBar from '../utility/appSnakBar';
import {CommonActions, StackActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {OfflineImageStore} from 'react-native-image-offline';
import apiUtility from '../api/apiUtility';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import appConstant from '../utility/appConstant';
import {GoogleAnalyticsTracker} from 'react-native-google-analytics-bridge';
import Orientation from 'react-native-orientation';
import apiLogout from './../api/apiLogin';

const {width: windowWidth, height: windowHeight} = Dimensions.get('window');

// const getUserName = () => {
//   setUserName(userName);
// }


const UserDetailsScreen = (props) => {
  let _isMounted = true;  
  const [userName, setUserName] = useState('');

  AsyncStorage.getItem("userName").then((value) => {
    setUserName(value);
  });

  const dispatch = useDispatch();

  const loginvalue = useSelector((state) => state.loginInfo.loginResponse);
  const userId =
    loginvalue && loginvalue.data && loginvalue.data.userid
      ? loginvalue.data.userid
      : '';

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );

  let fullName = null;
  let userProfileImage = null;
  let countryCode = null;
  let mobileNumber = null;
  let emailAddress = null;
  let userDetails = null;
  if (loginvalue && loginvalue.data) {
    userDetails = loginvalue.data;
    if (userDetails.first_name) {
      fullName = userDetails.first_name;
    }
    if (userDetails.last_name) {
      fullName += ' ' + userDetails.last_name;
    }
    if (userDetails.user_image) {
      userProfileImage = userDetails.user_image;
    }
    if (userDetails.country_code) {
      countryCode = userDetails.country_code;
    }
    if (userDetails.mob_no) {
      mobileNumber = userDetails.mob_no;
    }
    if (userDetails.email) {
      emailAddress = userDetails.email;
    }
  }

  const details = {
    name: fullName,
    mobile: mobileNumber,
    emailId: emailAddress,
  };

  const [isLoading, setIsLoading] = useState(false);

  const badgeData = useSelector((state) => state.badgeCount);
  const wishlistCount =
    badgeData && badgeData.wishlistCount ? badgeData.wishlistCount : '';
  const notificationCount =
    badgeData && badgeData.notificationCount ? badgeData.notificationCount : '';
  const lastAccessed =
    badgeData && badgeData.lastAccessed ? badgeData.lastAccessed : '';
  const menuType = useSelector((state) => state.menuType.menuType);
  console.log('menuType', menuType);
  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  const handleMenuType = async () => {
    dispatch(
      menuTypeAction.setMenuData({
        menuType: 'presale',
      }),
    );
  };

  useEffect(() => {
    if (userId) {
      getUserBadgeCount();
    }

    return () => {
      _isMounted = false;
    };
  }, [userId]);

  var tracker = new GoogleAnalyticsTracker('UA-171278332-1');

  const getUserBadgeCount = async () => {
    setIsLoading(false);

    try {
      const requestData = {
        user_id: userId,
      };

      const userBadgeCount = await apiUtility.apiUserBadgeCount(requestData);

      const response = userBadgeCount.data;

      setIsLoading(false);

      let notificationCountData = '';
      let wishlistCountData = '';
      let lastAccessedData = '';

      if (response && response.status == '200' && response.data) {
        if (response.data.unread_notifications_count) {
          notificationCountData =
            Number(response.data.unread_notifications_count) > 99
              ? '99+'
              : response.data.unread_notifications_count;
        }

        if (response.data.wishlist_count) {
          wishlistCountData =
            Number(response.data.wishlist_count) > 99
              ? '99+'
              : response.data.wishlist_count;
        }

        if (response.data.last_accessed) {
          lastAccessedData = response.data.last_accessed
            ? appConstant.formatDate(response.data.last_accessed)
            : '';
        }

        const total =
          Number(wishlistCountData ? wishlistCountData : '0') +
          Number(notificationCountData ? notificationCountData : '0');

        dispatch(
          badgeCountAction.setBadgeData({
            lastAccessed: lastAccessedData,
            wishlistCount: wishlistCountData,
            notificationCount: notificationCountData,
            totalCount: total > 0 ? '' + total : '',
          }),
        );
      }
    } catch (error) {
      setIsLoading(false);

      console.log('App Version Checking Error: ', error);
    }
  };

  const logoutUser = async () => {
    Alert.alert(
      'Logout',
      'Are You sure you want to logout?',
      [
        {
          text: 'No',
          style: 'cancel',
          onPress: () => {},
        },
        {
          text: 'Yes',
          onPress: async () => {
            // if (
            //   loginvalue &&
            //   loginvalue.isLogin &&
            //   loginvalue.data &&
            //   loginvalue.data.userid
            // ) {
              // const userId = loginvalue.data.userid;

              // const requestParms = {
              //   user_id: userId,
              //   device_id: deviceUniqueId,
              // };
              setIsLoading(true);
              // apiLogout
              //   .apiLogout(requestParms)
              //   .then(async (data) => {
                  // if (
                  //   data &&
                  //   data.data &&
                  //   data.data.status &&
                  //   data.data.status == '200'
                  // ) {
                    setIsLoading(false);
                    try {
                      await AsyncStorage.removeItem('sessionId');
                      await AsyncStorage.removeItem('sessionName');
                      await AsyncStorage.removeItem('token');
                      await AsyncStorage.removeItem('userName');
                      await AsyncStorage.removeItem('status');
                      await AsyncStorage.removeItem('isLogin');
                      await AsyncStorage.removeItem('rmData');
                      dispatch(clearRMData());
                      dispatch(clearBookingEnquiryData());
                      // dispatch(
                      //   badgeCountAction.setBadgeData({
                      //     lastAccessed: '',
                      //     wishlistCount: '',
                      //     notificationCount: '',
                      //     totalCount: '',
                      //   }),
                      // );
                    } catch (exception) {
                      console.log(exception);
                    }
                    dispatch({type: USER_LOGIN, data: {isLogin: false}});

                    appSnakBar.onShowSnakBar(`Logged out successfully`,
                      'LONG',
                    );

                    props.navigation.dispatch(
                      CommonActions.reset({
                        index: 1,
                        routes: [{name: 'Login'}],
                      }),
                    );
                  // } else {
                  //   setIsLoading(false);
                  //   appSnakBar.onShowSnakBar(
                  //     data.data.msg ? data.data.msg : `No record found`,
                  //     'LONG',
                  //   );
                  // }
                // })
                // .catch((errorL) => {
                //   console.log('Logout API Error: ', errorL);
                //   setIsLoading(false);
                // });
            // }
          },
        },
      ],
      {cancelable: false},
    );
  };

  const goToNewBooking = () => {
    Alert.alert(
      'Disclaimer',
      "Booking is currently under process and will reflect under 'My Properties' section, on payment of entire Booking Amount and receipt of KYC Documentation. This process takes around 24-48 hours to complete. Please get in touch with your sales representative or connect with us via 'Contact' section, for any queries.",
      [
        {
          text: 'OK',
          onPress: () => {
            openBookingDetailsScreen();
          },
        },
      ],
      {cancelable: true},
    );
  };

  const openBookingDetailsScreen = () => {
    // props.navigation.navigate('BookingDetailsScreen', {
    //   projectId: projectId,
    //   bookingId: bookingId,
    //   userId: userId,
    // });

    const userId =
      loginvalue && loginvalue.data && loginvalue.data.userid
        ? loginvalue.data.userid
        : '';
    //357123 347699  bookingId: '347699', projectId: '347178' EOIBookingConfirmation

    // props.navigation.navigate('BookingDetailsScreen', {
    //   userId: userId,
    // });

    props.navigation.dispatch(
      StackActions.replace('BookingDetailsScreen', {
        userId: userId,
      }),
    );
  };

  const onHandleRetryBtn = () => {
    if (userId) {
      getUserBadgeCount();
    }
  };

  const getAppVersion = () => {
    if (DeviceInfo.getVersion()) {
      return `${
        appConstant.buildInstance && appConstant.buildInstance.buildType
          ? appConstant.buildInstance.buildType + '-'
          : ''
      }${DeviceInfo.getVersion()}`;
    } else {
      return '';
    }
  };

  const goToProfilePage = () => {
    if (loginvalue && loginvalue.isLogin) {
      props.navigation.dispatch(StackActions.replace('Profile'));
    }
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <TouchableOpacity onPress={goToProfilePage} style={styles.header} activeOpacity={0.8}>
        {userProfileImage ? (
          <Image source={{uri: userProfileImage}} style={styles.photo} />
        ) : (
          <Image
            source={require('./../assets/images/no-user-img.png')}
            style={styles.photo}
          />
        )}

        <View style={styles.details}>
          <AppText
            style={[
              styles.headerTxt,
              {fontFamily: appFonts.SourceSansProBold, fontSize: appFonts.largeBold},
            ]}
            numberOfLines={1}>
            {/* {loginvalue && loginvalue.isLogin ? details.name : 'Guest'} */}
            { userName }
          </AppText>
          {loginvalue && loginvalue.isLogin && (
            <>
              <View style={styles.mailContainer}>
                <AppText style={styles.headerTxt}>
                  {details.mobile}&nbsp;&nbsp;|&nbsp;&nbsp;
                </AppText>
                <AppText style={styles.headerTxt} numberOfLines={1}>
                  {details.emailId}
                </AppText>
              </View>
              {lastAccessed ? (
                <AppText style={styles.headerTxt} numberOfLines={1}>
                  Last Visited : {lastAccessed}
                </AppText>
              ) : null}
            </>
          )}
        </View>
      </TouchableOpacity>

      <ScrollView bounces={false} style={{height: '65%', marginBottom: 20}}>
        <View style={styles.container}>
          {/* {loginvalue && loginvalue.isLogin ? ( */}
            {true ? (
            <View>
              {userDetails &&
              userDetails.is_customer &&
              userDetails.is_customer == 1 &&
              menuType == 'postsale' ? (
                <View>
                  <UserDetailsRowComponent
                    title="My Accounts"
                    onPress={() => {
                      tracker.trackEvent('Menu', 'Click', {
                        label: 'My Account statements',
                        value: 18,
                      });

                      props.navigation.dispatch(
                        StackActions.replace('MyAccoutScreen'),
                      );

                      // props.navigation.navigate('MyAccoutScreen');
                    }}
                  />
                  <UserDetailsRowComponent
                    title="Home Loan"
                    onPress={() => {
                      tracker.trackEvent('Menu', 'Click', {
                        label: 'Home Loan Support - Loan Enquiry',
                        value: 19,
                      });

                      props.navigation.dispatch(
                        StackActions.replace('LoanEnquiryScreen'),
                      );
                      // props.navigation.navigate('LoanEnquiryScreen');
                    }}
                  />
                  <UserDetailsRowComponent
                    title="Book a Visit"
                    onPress={() => {
                      tracker.trackEvent('Menu', 'Click', {
                        label: 'Book A Visit',
                        value: 24,
                      });

                      props.navigation.dispatch(
                        StackActions.replace('VisitScreen'),
                      );
                      // props.navigation.navigate('VisitScreen');
                    }}
                  />
                  
                  <UserDetailsRowComponent
                    title="Profile"
                    onPress={() => {
                      props.navigation.dispatch(
                        StackActions.replace('Profile'),
                      );
                      // props.navigation.navigate('VisitScreen');
                    }}
                  />
                  <UserDetailsRowComponent
                    title="Write to Us"
                    onPress={() => {
                      // alert("Work in progress");

                      tracker.trackEvent('Menu', 'Click', {
                        label: 'My Service Request',
                        value: 29,
                      });

                      props.navigation.dispatch(
                        StackActions.replace('ServiceRequest'),
                      );
                      // props.navigation.navigate('ServiceRequest');
                    }}
                  />
                  <UserDetailsRowComponent
                    title="My Document"
                    onPress={() => {
                      // alert('Work in progress');
                      // props.navigation.navigate('DocumentScreen');
                      tracker.trackEvent('Menu', 'Click', {
                        label: 'My Documents',
                        value: 25,
                      });

                      props.navigation.dispatch(
                        StackActions.replace('DocumentScreen'),
                      );
                    }}
                  />
                  <UserDetailsRowComponent
                    title="Pay Now"
                    onPress={() => {
                      // props.navigation.navigate('PayNowScreen');

                      props.navigation.dispatch(
                        StackActions.replace('PayNowScreen'),
                      );
                    }}
                  />
                  <UserDetailsRowComponent
                    title="Explore our Projects"
                    onPress={() => {
                      tracker.trackEvent('Menu', 'Click', {
                        label: 'Godrej Home',
                        value: 15,
                      });
                      handleMenuType();

                      props.navigation.dispatch(
                        CommonActions.reset({
                          index: 1,
                          routes: [{name: 'GodrejHome'}],
                        }),
                      );
                    }}
                  />
                  <UserDetailsRowComponent
                    title="New Booking"
                    onPress={() => {
                      goToNewBooking();
                    }}
                  />
                  <UserDetailsRowComponent
                    title="My Wishlist"
                    // number={wishlistCount ? wishlistCount : 0}
                    onPress={() => {
                      // props.navigation.navigate('WishlistScreen');
                      tracker.trackEvent('Menu', 'Click', {
                        label: 'My Wishlist',
                        value: 22,
                      });

                      props.navigation.dispatch(
                        StackActions.replace('WishlistScreen'),
                      );
                    }}
                  />
                  <UserDetailsRowComponent
                    title="Testimonials"
                    onPress={() => {
                      // alert("Work in progress");

                      tracker.trackEvent('Menu', 'Click', {
                        label: 'Testimonials',
                        value: 28,
                      });
                      // props.navigation.navigate('Testimonial');

                      props.navigation.dispatch(
                        StackActions.replace('Testimonial'),
                      );
                    }}
                  />
                  <UserDetailsRowComponent
                    title="Notifications"
                    number={notificationCount ? notificationCount : 0}
                    onPress={() => {
                      // props.navigation.navigate('NotificationsScreen');

                      tracker.trackEvent('Menu', 'Click', {
                        label: 'Notifications',
                        value: 23,
                      });

                      props.navigation.dispatch(
                        StackActions.replace('NotificationsScreen'),
                      );
                    }}
                  />
                  <UserDetailsRowComponent
                    title="FAQ"
                    onPress={() => {
                      // props.navigation.navigate('FAQ');

                      tracker.trackEvent('Menu', 'Click', {
                        label: 'FAQ',
                        value: 27,
                      });

                      props.navigation.dispatch(StackActions.replace('FAQ'));
                    }}
                  />
                </View>
              ) : (
                // GPC Menu
                <View>
                <UserDetailsRowComponent
                  title="Notifications"
                  number={notificationCount ? notificationCount : 0}
                  onPress={() => {
                    // props.navigation.navigate('NotificationsScreen');

                    props.navigation.dispatch(
                      StackActions.replace('NotificationsScreen'),
                    );
                  }}
                />
                
                <UserDetailsRowComponent
                title="My Booking"
                // onPress={openBookingDetailsScreen}
                onPress={() => {
                  props.navigation.dispatch(
                      StackActions.replace('BookingData'),
                    );
                  }}
                />

                <UserDetailsRowComponent
                  title="My Walk-ins"
                  onPress={openBookingDetailsScreen}
                  />

                <UserDetailsRowComponent
                  title="My Profile"
                  onPress={() => {
                    // tracker.trackEvent('Menu', 'Click', {
                    //   label: 'Update Profile',
                    //   value: 26,
                    // });

                    props.navigation.dispatch(StackActions.replace('UpdateProfile'));
                  }}
                />
                
                <UserDetailsRowComponent
                  title="-- Empanel Form"
                  onPress={() => {
                    props.navigation.dispatch(StackActions.replace('EmpanelmentPan'));
                  }}
                />

                {/* 
                <UserDetailsRowComponent
                  title="My Wishlist"
                  // number={wishlistCount ? wishlistCount : 0}
                  onPress={() => {
                    // props.navigation.navigate('WishlistScreen');

                    props.navigation.dispatch(
                      StackActions.replace('WishlistScreen'),
                    );
                  }}
                />
                
                <UserDetailsRowComponent
                  title="Notifications"
                  number={notificationCount ? notificationCount : 0}
                  onPress={() => {
                    // props.navigation.navigate('NotificationsScreen');

                    props.navigation.dispatch(
                      StackActions.replace('NotificationsScreen'),
                    );
                  }}
                />
                <UserDetailsRowComponent
                  title="Schedule a Visit"
                  onPress={() => {
                    // props.navigation.navigate('PresaleVisitScreen');

                    props.navigation.dispatch(
                      StackActions.replace('PresaleVisitScreen'),
                    );
                  }}
                /> */}
                </View>
              )}
              {/* <UserDetailsRowComponent
                title="My Profile"
                onPress={() => {
                  // props.navigation.navigate('Profile');

                  tracker.trackEvent('Menu', 'Click', {
                    label: 'My Profile',
                    value: 26,
                  });

                  props.navigation.dispatch(StackActions.replace('Profile'));
                }}
              /> */}
              {/* <UserDetailsRowComponent
                    title="-- Explore our Projects"
                    onPress={() => {
                      tracker.trackEvent('Menu', 'Click', {
                        label: 'Godrej Home',
                        value: 15,
                      });
                      handleMenuType();

                      props.navigation.dispatch(
                        CommonActions.reset({
                          index: 1,
                          routes: [{name: 'GodrejHome'}],
                        }),
                      );
                    }}
                  /> */}
              <UserDetailsRowComponent
                title="Logout"
                onPress={() => {
                  logoutUser();

                  tracker.trackEvent('Menu', 'Click', {
                    label: 'Logout',
                    value: 30,
                  });
                }}
              />
            </View>
          ) 
          : (
            <UserDetailsRowComponent
              title="Login"
              onPress={() => {
                props.navigation.navigate('Login');
              }}
            />
          )
          }
        </View>
        
      </ScrollView>

      <View style={styles.versionContainer}>
        <AppText style={styles.versionText}>{getAppVersion()}</AppText>
      </View>

      <View style={styles.iconsContainer}>
        <TouchableWithoutFeedback
          onPress={() => {
            Linking.openURL('https://www.facebook.com/godrejproperties/');
          }}>
          <Image
            style={styles.icon}
            source={require('./../assets/images/facebook-icon.png')}
          />
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => {
            Linking.openURL('https://twitter.com/GodrejProp');
          }}>
          <Image
            style={styles.icon}
            source={require('./../assets/images/twitter-icon.png')}
          />
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => {
            Linking.openURL('https://www.instagram.com/godrejpropertiesltd/');
          }}>
          <Image
            style={styles.icon}
            source={require('./../assets/images/instagram-icon.png')}
          />
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => {
            Linking.openURL('https://www.youtube.com/godrejpropertiesIN');
          }}>
          <Image
            style={styles.icon}
            source={require('./../assets/images/youtube-icon.png')}
          />
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => {
            Linking.openURL(
              'https://www.linkedin.com/company/godrejproperties',
            );
          }}>
          <Image
            style={styles.icon}
            source={require('./../assets/images/linkedin-icon.png')}
          />
        </TouchableWithoutFeedback>
      </View>

      <AppFooter
        activePage={7}
        isPostSales={menuType == 'postsale' ? true : false}
      />

      <AppOverlayLoader isLoading={isLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  header: {
    width: windowWidth,
    flexDirection: 'row',
    backgroundColor: colors.jaguar,
    paddingLeft: 20,
    paddingVertical: 25,
    justifyContent: 'flex-start',
  },
  photo: {
    width: windowWidth / 6,
    height: windowWidth / 6,
    borderRadius: windowWidth / 12,
  },
  details: {
    flex: 1,
    justifyContent: 'center',
    marginLeft: 12,
  },
  headerTxt: {
    color: colors.primary,
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.smallFontSize,
  },

  container: {
    paddingHorizontal: 25,
  },

  versionContainer: {
    paddingHorizontal: 30,
    marginTop: 15,
    marginBottom: 12,
  },
  versionText: {fontSize: appFonts.normalFontSize, fontFamily: appFonts.SourceSansProSemiBold},
  iconsContainer: {
    flex: 1,
    paddingHorizontal: 25,
    paddingTop: 28,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  icon: {
    width: 30,
    height: 30,
    marginRight: 6,
  },
  dropDownImage: {
    width: 50,
    height: 50,
    marginBottom: 20,
  },
  dropDownImageCon: {
    flexDirection: 'row',
    justifyContent: 'center',
    // marginTop: 5,
  },
  nameText: {
    fontSize: appFonts.largeFontSize,
    // fontWeight: 'bold',
    textAlign: 'center',
    // marginHorizontal: 10,
  },
  containertext: {
    flex: 1,
    flexDirection: 'row',
    // width: 400,
    marginTop: 10,
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
    // marginBottom: 10,
  },
  sideBarSubMenu: {
    marginLeft: 30,
  },
  mailContainer: {flexDirection: 'row', flexWrap: 'wrap', marginVertical: 3},
});

export default UserDetailsScreen;
