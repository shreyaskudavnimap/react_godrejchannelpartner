import React, {useState, useEffect, useCallback, Fragment, useRef} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  Modal,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {EventRegister} from 'react-native-event-listeners';

import {WebView} from 'react-native-webview';
import {useBackHandler} from '@react-native-community/hooks';
import Orientation from 'react-native-orientation';

import AsyncStorage from '@react-native-async-storage/async-storage';
import {StackActions, useFocusEffect, useIsFocused} from '@react-navigation/native';

import {useSelector, useDispatch} from 'react-redux';

import {clearLoginParam, clearRMData} from '../store/actions/userLoginAction';

import * as projectDetailsAction from './../store/actions/projectDetailsAction';
import * as continueExploringAction from './../store/actions/homeContinueExploringAction';
import * as cityWiseProjectsAction from './../store/actions/homeCityWiseProjectsAction';
import * as newLaunchAction from './../store/actions/homeNewLaunchAction';
import * as greenLivingAction from './../store/actions/homeGreenLivingAction';
import * as awardWiningAction from './../store/actions/homeAwardWiningAction';

import Screen from '../components/Screen';
import appFonts from '../config/appFonts';
import AppHeader from '../components/ui/AppHeader';
import AppButton from '../components/ui/AppButton';
import AppText from '../components/ui/ AppText';
import colors from '../config/colors';
import AppProjectDetailsMenuItem from '../components/ui/AppProjectDetailsMenuItem';
import AppClickableText from '../components/ui/AppClickableText';
import AppProjectDetailsViewItem from '../components/ui/AppProjectDetailsViewItem';
import AppProjectDetailsViewImgItem from '../components/ui/AppProjectDetailsViewImgItem';

import VideoPlayer from 'react-native-video-player';
import GalleryModalScreen from './modals/GalleryModalScreen';
import VideoModalScreen from './modals/VideoModalScreen';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import appSnakBar from '../utility/appSnakBar';
import AppFooter from '../components/ui/AppFooter';

import apiProjectDetails from './../api/apiProjectDetails';
import appCurrencyFormatter from '../utility/appCurrencyFormatter';
import appConstant from '../utility/appConstant';

import apiInventoryMLP from './../api/apiInventoryMLP';
import apiWishlist from './../api/apiWishlist';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const ProjectDetailsScreen = ({navigation, route}) => {
  const [isVideoLoading, setIsVideoLoading] = useState(true);
  const [isLoading, setIsLoading] = useState(true);
  const [isInnerLoading, setIsInnerLoading] = useState(false);
  const [error, setError] = useState();
  const [readMore, setReadMore] = useState(true);

  const [modalVisible, setModalVisible] = useState(false);
  const [onlyVideos, setOnlyVideos] = useState(false);
  const [activeIndex, setActiveIndex] = useState(false);

  const [isViewLoading, setViewLoading] = useState(false);
  const [galleryView, setGalleryView] = useState([]);
  const [isShowGalleryView, setIsShowGalleryView] = useState(false);

  const [showWebview, setShowWebview] = useState(false);
  const [interactiveAssetUrl, setInteractiveAssetUrl] = useState('');
  const [isFOYR, setIsFOYR] = useState(false);
  const [foyrUrl, setFOYRUrl] = useState('');
  const webRefs = useRef();

  const [isVideoEnd, setIsVideoEnd] = useState(false);
  const videoRef = useRef();

  const routeParams = route.params;
  const projectId =
    routeParams && routeParams.item && routeParams.item.proj_id
      ? routeParams.item.proj_id
      : '';

  const dispatch = useDispatch();

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );

  const loginUserData = useSelector((state) => state.loginInfo.loginResponse);
  const userId =
    loginUserData && loginUserData.data && loginUserData.data.userid
      ? loginUserData.data.userid
      : '';
  const loginParam = useSelector((state) => state.loginInfo.loginParam);

  const projectDetailsData = useSelector((state) => state.projectDetails);
  const projectDetails = projectDetailsData.projectDetails;
  const projectGallery = projectDetailsData.projectGallery;
  const projectGallerySlice = projectDetailsData.projectGallerySlice;
  const projectFeaturedMenu = projectDetailsData.projectFeaturedMenu;
  // console.log(
  //   'projectDetailsData.projectGallery',
  //   projectDetailsData.projectGallery,
  // );

  const isFocused = useIsFocused();

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
    if (isVideoEnd) {
      if (typeof videoRef != 'undefined' && videoRef && videoRef.current) {
        setIsVideoEnd(false);

        console.log('videoRef: ', videoRef)

        videoRef.current.stop();
        setTimeout(() => {
          videoRef.current.resume();
        }, 250);
      }
    }
  }, [isVideoEnd]);

  useEffect(() => {
    dispatch(projectDetailsAction.resetProjectDetails());
  }, []);

  useEffect(() => {
    // console.log('useEffect(()...', isFocused)
    setShowWebview(false);
    Orientation.lockToPortrait();
    if (deviceUniqueId && isFocused) {
      onUpdateProjectInfo();

      if (loginParam) {
        if (loginParam === 'wishlist_set') {
          // Do some action
          console.log('Rou par', loginParam);
          console.log('Wishlist Resumed');
          dispatch(clearLoginParam());
        }
      }
    }
  }, [dispatch, deviceUniqueId, routeParams, projectId, isFocused]);

  useEffect(() => {
    const eventListener = EventRegister.addEventListener(
      'ProjectDetailsUpdateEvent',
      (data) => {
        if (data && data === 'updateList') {
          if (userId) {
            wishlistItems(false);
          }
        }
      },
    );

    const eventListenerBooking = EventRegister.addEventListener(
      'ProjectDetailsUpdateEventBooking',
      (data) => {
        if (data) {
          if (userId) {
            checkInventoryStatus(data);
          }
        }
      },
    );

    const eventListenerBookingEOI = EventRegister.addEventListener(
      'ProjectDetailsUpdateEventBookingEOI',
      (data) => {
        if (data) {
          if (userId) {
            goToEoiBookingPageAfterLogin(data);
          }
        }
      },
    );
    const eventListenerOther = EventRegister.addEventListener(
      'ProjectDetailsUpdateEventOther',
      (data) => {
        if (data && data === 'updateList') {
          if (userId) {
            wishlistItems(true);
          }
        }
      },
    );

    return () => {
      EventRegister.removeEventListener(eventListener);
      EventRegister.removeEventListener(eventListenerBooking);
      EventRegister.removeEventListener(eventListenerBookingEOI);
      EventRegister.removeEventListener(eventListenerOther);
    };
  }, [dispatch, userId]);

  // useFocusEffect(
  //   useCallback(() => {
  //     if (deviceUniqueId) {
  //       onUpdateProjectInfo();
  //     }
  //   }, [dispatch, deviceUniqueId, routeParams, projectId]),
  // );

  const onUpdateProjectInfo = async () => {
    if (!projectId) {
      navigation.goBack();
      return;
    }
    setIsLoading(true);
    try {
      const request = {
        user_id: userId,
        device_id: userId === '' ? deviceUniqueId : '',
        proj_id: projectId,
      };
      const result = await apiProjectDetails.apiUpdateProjectInfo(request);
      if (result.data.status == '200') {
        onlyLoadContinueExploringProperty();
      } else {
        getProjectDetals();
      }
    } catch (error) {
      console.log('Update Project Info Error: ', error);
      getProjectDetals();
    }
  };

  const onlyLoadContinueExploringProperty = useCallback(async () => {
    try {
      await dispatch(
        continueExploringAction.getContinueExpoloringProperty(
          userId,
          deviceUniqueId,
        ),
      );
      getProjectDetals();
    } catch (err) {
      setError(err.message);
      getProjectDetals();
    }
  }, [dispatch, setIsLoading, setError, isLoading, deviceUniqueId, userId, projectId]);

  const getProjectDetals = useCallback(async () => {
    setError(null);
    try {
      await dispatch(
        projectDetailsAction.getProjectDetals(
          userId,
          deviceUniqueId,
          projectId,
        ),
      );
      getProjectGallery();
    } catch (err) {
      appSnakBar.onShowSnakBar(err.message, 'LONG');
      navigation.goBack();
      setError(err.message);
      setIsLoading(false);
    }
  }, [dispatch, setIsLoading, setError, isLoading, deviceUniqueId, userId, projectId]);

  const getProjectGallery = useCallback(async () => {
    try {
      await dispatch(projectDetailsAction.getProjectGallery(projectId));
      getProjectFeaturedMenu();
    } catch (err) {
      setError(err.message);
      getProjectFeaturedMenu();
    }
  }, [dispatch, setIsLoading, setError, isLoading, deviceUniqueId, userId, projectId]);

  const getProjectFeaturedMenu = useCallback(async () => {
    try {
      await dispatch(
        projectDetailsAction.getProjectFeaturedMenu(
          userId,
          deviceUniqueId,
          projectId,
        ),
      );
      setIsLoading(false);
    } catch (err) {
      setError(err.message);
      setIsLoading(false);
    }
  }, [dispatch, setIsLoading, setError, , isLoading, deviceUniqueId, userId, projectId]);

  const onHandleRetryBtn = () => {
    setShowWebview(false);
    setModalVisible(false);
    setOnlyVideos(false);
    setIsShowGalleryView(false);
    onUpdateProjectInfo();
  };

  const onHandleOpenWebview = () => {
    Orientation.lockToLandscape();
    setShowWebview(true);
  };

  const checkProjectAvailability = async () => {
    setIsInnerLoading(true);

    try {
      const projectAvailabilityResult = await apiInventoryMLP.apiCheckProjectAvailability(
        projectId,
      );
      setIsInnerLoading(false);

      if (
        projectAvailabilityResult.data[0].hasOwnProperty('status') &&
        projectAvailabilityResult.data[0].status != null &&
        projectAvailabilityResult.data[0].status == '1'
      ) {
        onHandleOpenWebviewFOYR();
      } else {
        setIsInnerLoading(false);
        projectAvailabilityError();
      }
    } catch (error) {
      console.log(error);
      setIsInnerLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const projectAvailabilityError = () => {
    Alert.alert(
      appConstant.appMessage.NO_BOOKING_AVAILABLE_ALERT_TITLE,
      appConstant.appMessage.NO_BOOKING_AVAILABLE_ALERT_MESSAGE,
      [
        {
          text:
            appConstant.appMessage.NO_BOOKING_AVAILABLE_ALERT_POSITIVE_BUTTON,
          onPress: () => console.log('OK Pressed'),
        },
      ],
      {cancelable: false},
    );
  };

  const onHandleOpenWebviewFOYR = () => {
    Orientation.lockToLandscape();
    setFOYRUrl(projectDetails.field_foyr_url);

    // setFOYRUrl('https://customercare.godrejproperties.com/gpl-api/virtual_grid_mobile/a1l2s00000003VlAAI');
    // setFOYRUrl('https://customercare.godrejproperties.com/gpl-api/virtual_grid_mobile_eoi/a1l2s000000XoezAAC');

    setIsFOYR(true);
    setShowWebview(true);
  };

  const onCloseFOYRWebView = () => {
    setInteractiveAssetUrl('');
    setFOYRUrl('');
    setIsFOYR(false);
    Orientation.lockToPortrait();
    setShowWebview(false);
  };

  const handleWebViewNavigationStateChange = ({url}) => {
    if (isFOYR) {
      const pathClose = url.split('foyr-close=');
      if (pathClose && pathClose.length > 1 && pathClose[1] == 'true') {
        onCloseFOYRWebView();
      } else {
        const pathCloseFOYR = url.split('foyrTxnID=');
        if (pathCloseFOYR && pathCloseFOYR.length > 0) {
          const foyrData = appConstant.getJsonFromUrl(url);
          // console.log('foyrData: ', foyrData);

          if (projectDetails.eoi_enabled == '0') {
            if (foyrData && foyrData.inv_id && foyrData.foyrTxnID) {
              onCloseFOYRWebView();
              getMLPDataFromFOYR(foyrData.inv_id, foyrData.foyrTxnID);
            } else {
              if (!showWebview) {
                appSnakBar.onShowSnakBar(
                  'Inventory selection facility is not available through FOYR',
                  'LONG',
                );
              }
            }
          } else {
            console.log('foyrData: ', foyrData);
            if (
              foyrData &&
              foyrData.flattype &&
              foyrData.tower &&
              foyrData.Floor_Band &&
              foyrData.foyrTxnID
            ) {
              onCloseFOYRWebView();
              goToEoiBookingPageFoyr(
                foyrData.flattype,
                foyrData.tower,
                foyrData.Floor_Band,
                foyrData.foyrTxnID,
              );
            } else {
              if (!showWebview) {
                appSnakBar.onShowSnakBar(
                  'Preference selection facility is not available through FOYR',
                  'LONG',
                );
              }
            }
          }
        }
      }
    } else {
      const pathClose = url.split('interactive-asset-close=');
      if (pathClose && pathClose.length > 1 && pathClose[1] == 'true') {
        setInteractiveAssetUrl('');
        Orientation.lockToPortrait();
        setShowWebview(false);
      }
    }

    console.log('handleWebViewNavigationStateChange newNavState : ', url);
  };

  const goToEoiBookingPageFoyr = (typology, tower, floorBand, foyrTxnID) => {
    // const _URL = 'http://43.242.212.209/gpl-project/customer-login?foyrTxnID=4feda0a0-aa45-11ea-9d6f-4f4e56b508aa&Floor_Band=11th%20Floor%20and%20Above&flattype=2%20BHK&tower=T4C_2BHK-T1_202';
    // const foyrData = appConstant.getJsonFromUrl(_URL);
    // console.log('foyrData: ', foyrData);

    if (userId && userId != '') {
      navigation.navigate('EOIBookingScreen', {
        project_id: projectId,
        eoi_preference: '2',
        preference_combination: projectDetails['preference_combination']
          ? projectDetails['preference_combination']
          : '',
        project_sfdc_id: projectDetails['field_external_project_id']
          ? projectDetails['field_external_project_id']
          : '',
        typology: typology ? typology : '',
        tower: tower ? tower : '',
        floor_band: floorBand ? floorBand : '',
        foyrTxnID: foyrTxnID ? foyrTxnID : '',
      });
    } else {
      const data = {
        project_id: projectId,
        eoi_preference: '2',
        preference_combination: projectDetails['preference_combination']
          ? projectDetails['preference_combination']
          : '',
        project_sfdc_id: projectDetails['field_external_project_id']
          ? projectDetails['field_external_project_id']
          : '',
        typology: typology ? typology : '',
        tower: tower ? tower : '',
        floor_band: floorBand ? floorBand : '',
        foyrTxnID: foyrTxnID ? foyrTxnID : '',
      };
      setIsInnerLoading(true);
      setTimeout(() => {
        setIsInnerLoading(false);
        validateLoginBookingEOI(data);
      }, 600);
    }
  };

  const goToEoiBookingPageAfterLogin = (data) => {
    setIsInnerLoading(true);
    setTimeout(() => {
      setIsInnerLoading(false);
      navigation.navigate('EOIBookingScreen', data);
    }, 800);
  };

  const getMLPDataFromFOYR = async (inventoryId, foyrTxnID) => {
    try {
      setIsInnerLoading(true);

      const requestData = {
        inventory_id: inventoryId,
      };

      const inventoryStatusResult = await apiInventoryMLP.apiInventoryInfo(
        requestData,
      );

      setIsInnerLoading(false);

      if (inventoryStatusResult.data) {
        if (
          inventoryStatusResult.data.status &&
          inventoryStatusResult.data.status == '200'
        ) {
          const response = inventoryStatusResult.data;

          const typologyData = {
            title: response.data[0].typology_title,
          };
          const towerData = {
            id: response.data[0].towerid,
            title: response.data[0].tower,
          };
          const floorData = {
            title: response.data[0].floor_title,
          };
          const inventoryData = {
            carpet_area: response.data[0].carpet_area_sq_mt,
            carpet_area_sq_ft: response.data[0].carpet_area_sq_ft,
            floor_img: response.data[0].floor_img,
            floor_title: response.data[0].floor,
            id: response.data[0].id,
            inv_img: response.data[0].inv_img,
            inventory_price_amount: response.data[0].inventory_price_amount,
            typology_title: response.data[0].typology_title,
            unit_no: response.data[0].unit_no,
          };

          const enquiryFormData = {
            typologyData: typologyData,
            towerData: towerData,
            floorData: floorData,
            inventoryData: inventoryData,
            foyrTxnID: foyrTxnID,
            inventory_preference: '2',
          };
          checkGuestLogin(enquiryFormData, '3DView');
        } else {
          appSnakBar.onShowSnakBar(
            inventoryStatusResult.data.msg
              ? inventoryStatusResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      } else {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      setIsInnerLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const checkGuestLogin = (generatedData, action) => {
    if (userId && userId != '') {
      if (action === '3DView') {
        checkInventoryStatus(generatedData);
      } else if (action === 'Wishlist') {
        addOrRemoveWishlist();
      }
    } else {
      if (action === '3DView') {
        setIsInnerLoading(true);
        setTimeout(() => {
          setIsInnerLoading(false);
          validateLoginBooking(generatedData);
        }, 600);
      } else if (action === 'Wishlist') {
        validateLogin();
      }
    }
  };

  const onHandleWishlist = () => {
    checkGuestLogin(null, 'Wishlist');
  };

  const addOrRemoveWishlist = async () => {
    try {
      setIsInnerLoading(true);

      const requestData = {
        user_id: userId,
        proj_id: projectId,
        type: projectDetails.wishlist_status == '0' ? 'add' : 'delete',
      };
      const wishlistItem = await apiWishlist.apiAddRemoveWishlist(requestData);

      setIsInnerLoading(false);

      if (wishlistItem.data) {
        if (wishlistItem.data.status && wishlistItem.data.status == '200') {
          appSnakBar.onShowSnakBar(
            wishlistItem.data.msg ? wishlistItem.data.msg : 'Wishlist...',
            'LONG',
          );

          EventRegister.emit('wishlistUpdateEvent', 'update');

          dispatch(projectDetailsAction.updateProjetWishlist());

          dispatch(continueExploringAction.updateProjetWishlist(projectId));
          dispatch(cityWiseProjectsAction.updateProjetWishlist(projectId));
          dispatch(newLaunchAction.updateProjetWishlist(projectId));
          dispatch(greenLivingAction.updateProjetWishlist(projectId));
          dispatch(awardWiningAction.updateProjetWishlist(projectId));
        } else {
          appSnakBar.onShowSnakBar(
            wishlistItem.data.msg
              ? wishlistItem.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      } else {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      setIsInnerLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const wishlistItems = async (isNotShow) => {
    try {
      if (!isNotShow) {
        setIsInnerLoading(true);
      }

      const requestData = {
        user_id: userId,
      };
      const wishlistItems = await apiWishlist.apiWishlistItems(requestData);

      if (!isNotShow) {
        setIsInnerLoading(false);
      }

      if (wishlistItems.data) {
        if (wishlistItems.data.status && wishlistItems.data.status == '200') {
          const responseItems = wishlistItems.data.data;
          if (responseItems && responseItems.length > 0) {
            const indexWishlist = responseItems.findIndex(
              (x) => x.proj_id == projectId,
            );

            if (indexWishlist > -1) {
              dispatch(projectDetailsAction.updateProjetWishlist());
            }

            dispatch(
              continueExploringAction.updateProjetWishlistArr(responseItems),
            );
            dispatch(
              cityWiseProjectsAction.updateProjetWishlistArr(responseItems),
            );
            dispatch(newLaunchAction.updateProjetWishlistArr(responseItems));
            dispatch(greenLivingAction.updateProjetWishlistArr(responseItems));
            dispatch(awardWiningAction.updateProjetWishlistArr(responseItems));
          }
        } else {
          appSnakBar.onShowSnakBar(
            wishlistItems.data.msg
              ? wishlistItems.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      } else {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      if (!isNotShow) {
        setIsInnerLoading(false);
      }
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const checkInventoryStatus = async (enquiryFormData) => {
    try {
      setIsInnerLoading(true);

      const requestData = {
        user_id: userId,
        inventory_id: enquiryFormData.inventoryData.id,
      };
      const inventoryStatusResult = await apiInventoryMLP.apiCheckInventoryStatus(
        requestData,
      );

      setIsInnerLoading(false);

      if (inventoryStatusResult.data) {
        if (
          inventoryStatusResult.data.status &&
          inventoryStatusResult.data.status == '200'
        ) {
          const unitDetails = {
            project_id: projectId,
            unitData: enquiryFormData,
          };
          navigation.navigate('BookingScreen', unitDetails);
        } else if (
          inventoryStatusResult.data.status &&
          inventoryStatusResult.data.status == '500'
        ) {
          appSnakBar.onShowSnakBar(
            inventoryStatusResult.data.msg
              ? inventoryStatusResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        } else {
          appSnakBar.onShowSnakBar(
            inventoryStatusResult.data.msg
              ? inventoryStatusResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      } else {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      setIsInnerLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  useBackHandler(() => {
    if (showWebview) {
      setInteractiveAssetUrl('');
      setFOYRUrl('');
      Orientation.lockToPortrait();
      setShowWebview(false);
      return true;
    }
    return false;
  });

  const onHandleVideoModal = () => {
    setModalVisible(true);
    setOnlyVideos(true);
    setIsShowGalleryView(false);
  };

  const onHandleImageModal = (activeIndex) => {
    if (typeof activeIndex !== 'undefined') {
      console.log('activeIndex: ', activeIndex);
      setActiveIndex(activeIndex);
    } else {
      const aIndex = projectDetails.field_foyr_url
        ? projectGallerySlice.length - 1
        : projectGallerySlice.length;

      console.log('activeIndex: ', aIndex);

      setActiveIndex(aIndex);
    }

    setModalVisible(true);
    setOnlyVideos(false);
    setIsShowGalleryView(false);
  };

  const toogleReadMore = () => {
    setReadMore(!readMore);
  };

  const onHandleBookNow = () => {
    if (projectDetails) {
      if (projectDetails.field_is_available_for_booking == '0') {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.NO_BOOKING_AVAILABLE_ALERT_MESSAGE,
          'LONG',
        );
      } else if (projectDetails.field_is_available_for_booking == '1') {
        if (projectDetails.eoi_enabled == '0') {
          navigation.navigate('FloorScreen', {
            item: routeParams.item,
          });
        } else {
          navigation.navigate('EOIFloorScreen', {
            item: {
              ...routeParams.item,
              field_foyr_url: projectDetails['field_foyr_url']
                ? projectDetails['field_foyr_url']
                : '',
              preference_combination: projectDetails['preference_combination']
                ? projectDetails['preference_combination']
                : '',
              project_sfdc_id: projectDetails['field_external_project_id']
                ? projectDetails['field_external_project_id']
                : '',
            },
          });
        }
      }
    }
  };

  const onHandleMenuItemClick = (featuredMenuItem) => {
    if (featuredMenuItem.field_url_alias) {
      if (
        featuredMenuItem.field_url_alias == 'about_godrej' ||
        featuredMenuItem.field_url_alias == 'compliance' ||
        featuredMenuItem.field_url_alias == 'community' ||
        featuredMenuItem.field_url_alias == 'elderly' ||
        featuredMenuItem.field_url_alias == 'senior_living' ||
        featuredMenuItem.field_url_alias == 'entertainment' ||
        featuredMenuItem.field_url_alias == 'fitness_well' ||
        featuredMenuItem.field_url_alias == 'green_cover' ||
        featuredMenuItem.field_url_alias == 'interior_finishes' ||
        featuredMenuItem.field_url_alias == 'kids' ||
        featuredMenuItem.field_url_alias == 'pets' ||
        featuredMenuItem.field_url_alias == 'spend_day' ||
        featuredMenuItem.field_url_alias == 'sun_wind' ||
        featuredMenuItem.field_url_alias == 'why_project' ||
        featuredMenuItem.field_url_alias == 'disclaimer'
      ) {
        navigation.navigate('ProjectCms', {
          item: routeParams.item,
          featuredMenuItem: featuredMenuItem.field_url_alias,
        });
      } else if (featuredMenuItem.field_url_alias == 'location_connectivity') {
        navigation.navigate('LocationConnectivity', routeParams);
      } else if (featuredMenuItem.field_url_alias == 'payment_costsheet') {
      } else if (featuredMenuItem.field_url_alias == 'image_drone') {
        openGalleryView('drone_view');
      } else if (featuredMenuItem.field_url_alias == 'floor_plans') {
        onHandleBookNow();
      } else if (featuredMenuItem.field_url_alias == '3d_views') {
        openGalleryView('3d_views');
      } else if (
        featuredMenuItem.field_url_alias.indexOf('interactive') !== -1
      ) {
        try {
          const fieldUrlAliasInteractiveAsset = featuredMenuItem.field_url_alias.split(
            '_',
          );
          const fieldUrlAliasType = fieldUrlAliasInteractiveAsset[0];

          if (fieldUrlAliasType && fieldUrlAliasType == 'interactive') {
            if (
              projectDetails.field_interactive_asset_url &&
              projectDetails.field_interactive_asset_url != ''
            ) {
              const interactiveAssetURL =
                projectDetails.field_interactive_asset_url +
                '/' +
                featuredMenuItem.field_url_alias;

              setInteractiveAssetUrl(interactiveAssetURL);
              setIsFOYR(false);
              onHandleOpenWebview();
            }
          } else {
            appSnakBar.onShowSnakBar('No page found', 'LONG');
          }
        } catch (error) {
          // this.toast.show("No Page Found.");
          console.log('interactiveAssetURL error : ' + error);
        }
      } else {
        navigation.navigate('ProjectCms', {
          item: routeParams.item,
          featuredMenuItem: featuredMenuItem.field_url_alias,
        });
      }
    }
  };

  const openGalleryView = async (type) => {
    setViewLoading(true);
    setGalleryView([]);
    try {
      const request = {project_id: projectId, type: type};
      const result = await apiProjectDetails.apiProjectGalleryView(request);

      if (result.data && result.data.length > 0) {
        setIsShowGalleryView(true);
        setModalVisible(true);
        setOnlyVideos(false);
        setGalleryView(result.data);
      } else {
        appSnakBar.onShowSnakBar(appConstant.appMessage.NO_DATA, 'SHORT');
      }
      setViewLoading(false);
    } catch (error) {
      console.log('Open Gallery View Error: ', error);
      setViewLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const ActivityIndicatorElement = () => {
    return (
      <AppOverlayLoader isLoading={true} isWhiteBackground={true} />
      // <ActivityIndicator
      //   color={colors.jaguar}
      //   size="small"
      //   style={styles.activityIndicatorStyle}
      // />
    );
  };

  const validateLogin = async () => {
    await AsyncStorage.removeItem('sessionId');
    await AsyncStorage.removeItem('sessionName');
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('data');
    await AsyncStorage.removeItem('status');
    await AsyncStorage.removeItem('isLogin');
    await AsyncStorage.removeItem('rmData');
    dispatch(clearRMData());

    navigation.dispatch(
      StackActions.push('Login', {
        backRoute: 'AppStackRoute',
        action: 'wishlist_set',
        screen: 'ProjectDetails',
        item: routeParams.item,
      }),
    );
  };

  const validateLoginBooking = async (generatedData) => {
    await AsyncStorage.removeItem('sessionId');
    await AsyncStorage.removeItem('sessionName');
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('data');
    await AsyncStorage.removeItem('status');
    await AsyncStorage.removeItem('isLogin');
    await AsyncStorage.removeItem('rmData');
    dispatch(clearRMData());

    navigation.dispatch(
      StackActions.push('Login', {
        backRoute: 'AppStackRoute',
        action: 'booking',
        screen: 'ProjectDetails',
        item: generatedData,
      }),
    );
  };

  const validateLoginBookingEOI = async (generatedData) => {
    await AsyncStorage.removeItem('sessionId');
    await AsyncStorage.removeItem('sessionName');
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('data');
    await AsyncStorage.removeItem('status');
    await AsyncStorage.removeItem('isLogin');
    await AsyncStorage.removeItem('rmData');
    dispatch(clearRMData());

    navigation.dispatch(
      StackActions.push('Login', {
        backRoute: 'AppStackRoute',
        action: 'booking_eoi',
        screen: 'ProjectDetails',
        item: generatedData,
      }),
    );
  };

  const isPlotProject = (resultData) => {
    if (resultData && resultData.field_external_project_id) {
      return appConstant.isPlotProject(resultData.field_external_project_id);
    } else {
      return false;
    }
  };

  const goToSiteVisitScreen = () => {
    navigation.navigate('PresaleVisitScreen', {projectId: projectId});
  };

  const getMorePhotoTitle = () => {
    // `${projectGallery.length - 1}+ Photos ${projectGallerySlice.length}`

    const substractCount = projectDetails.field_foyr_url
      ? projectGallerySlice.length - 1 - 1
      : projectGallerySlice.length - 1;

    return projectGallery
      ? `+${projectGallery.length - 1 - substractCount} Photos`
      : `More Photos`;
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      {showWebview && (interactiveAssetUrl || foyrUrl) ? (
        <WebView
          ref={webRefs}
          originWhitelist={['*']}
          source={{
            uri: isFOYR ? foyrUrl : interactiveAssetUrl,
          }}
          style={{flex: 1}}
          scalesPageToFit
          javaScriptEnabledAndroid={true}
          javaScriptEnabled={true}
          // domStorageEnabled={true}
          cacheEnabled={false}
          incognito={true}
          mediaPlaybackRequiresUserAction={true}
          startInLoadingState={true}
          renderLoading={ActivityIndicatorElement}
          onNavigationStateChange={handleWebViewNavigationStateChange}
        />
      ) : null}

      {!showWebview && (
        <AppHeader
          selectionIconSource={
            !isLoading &&
            projectDetails &&
            projectDetails.wishlist_status &&
            projectDetails.wishlist_status == '1'
              ? require('./../assets/images/thumb-fill.png')
              : require('./../assets/images/thumb-line.png')
          }
          onWishlistPress={onHandleWishlist}
        />
      )}

      {isLoading && !showWebview ? (
        <View style={styles.container} />
      ) : (
        <>
          {!showWebview && (
            <ScrollView>
              <View style={styles.container}>
                <AppText style={styles.projectTitle} numberOfLines={2}>
                  {projectDetails && projectDetails.title
                    ? projectDetails.title
                    : ''}
                </AppText>

                <AppText style={styles.projectLocation} numberOfLines={1}>
                  {projectDetails && projectDetails.field_sub_location
                    ? projectDetails.field_sub_location + ', '
                    : ''}
                  {projectDetails && projectDetails.field_city
                    ? projectDetails.field_city
                    : ''}
                </AppText>

                <View style={styles.viewContainer}>
                  <View
                    style={styles.videoContainer}
                    onPress={() => {
                      setModalVisible(true);
                    }}>
                    {projectDetails &&
                      projectDetails.field_project_details_video_thum && (
                        <TouchableOpacity onPress={onHandleVideoModal}>
                          <VideoPlayer
                            ref={videoRef}
                            video={{
                              uri:
                                projectDetails.field_project_details_video_thum,
                            }}
                            style={{height: '100%', width: '100%'}}
                            thumbnail={{
                              uri:
                                projectDetails.field_project_details_video_thum,
                            }}
                            muted={true}
                            disableSeek={true}
                            autoplay={true}
                            loop={true}
                            hideControlsOnStart={true}
                            resizeMode="stretch"
                            disableControlsAutoHide={true}
                            customStyles={{
                              controls: {opacity: 0},
                              seekBar: {opacity: 0},
                            }}
                            // onLoadStart={() => {
                            //   setIsVideoLoading(true);
                            // }}
                            onLoad={() => {
                              setTimeout(() => {
                                setIsVideoLoading(false);
                              }, 500);
                            }}
                            onEnd={() => {
                              if (
                                typeof videoRef != 'undefined' &&
                                videoRef &&
                                videoRef.current
                              ) {
                                setIsVideoEnd(true);
                              }
                            }}
                          />
                          {isVideoLoading ? (
                            <View
                              style={{
                                height: '100%',
                                position: 'absolute',
                                left: 0,
                                right: 0,
                                top: 0,
                                bottom: 0,
                                backgroundColor: '#ffffff',
                                justifyContent: 'center',
                                alignItems: 'center',
                              }}>
                              <ActivityIndicator
                                color={colors.jaguar}
                                size="small"
                                style={{
                                  flex: 1,
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                }}
                              />
                            </View>
                          ) : null}
                        </TouchableOpacity>
                      )}
                  </View>

                  <View style={styles.imgContainer}>
                    <View style={styles.imgItem}>
                      {projectGallerySlice && projectGallerySlice.length > 0 ? (
                        projectGallerySlice.map((projectGalleryItem, index) => (
                          <Fragment key={index}>
                            {index != 4 ? (
                              <AppProjectDetailsViewImgItem
                                viewIndex={index}
                                viewItemLength={projectGallerySlice - 1}
                                containerStyle={styles.imgItemBtn}
                                imgSource={
                                  projectGalleryItem.field_gallery_thumb_image_url
                                }
                                onPress={() => onHandleImageModal(index)}
                              />
                            ) : (
                              <>
                                {projectDetails &&
                                projectDetails.field_foyr_url ? (
                                  <AppProjectDetailsViewItem
                                    containerStyle={styles.imgItemBtn}
                                    imgSource={require('./../assets/images/3d-view-icon-b.png')}
                                    title="3D View"
                                    onPress={checkProjectAvailability}
                                  />
                                ) : (
                                  <AppProjectDetailsViewImgItem
                                    viewIndex={index}
                                    viewItemLength={projectGallerySlice - 1}
                                    containerStyle={styles.imgItemBtn}
                                    imgSource={
                                      projectGalleryItem.field_gallery_thumb_image_url
                                    }
                                    onPress={() => onHandleImageModal()}
                                  />
                                )}
                              </>
                            )}
                          </Fragment>
                        ))
                      ) : (
                        <>
                          {projectDetails && projectDetails.field_foyr_url ? (
                            <AppProjectDetailsViewItem
                              containerStyle={styles.imgItemBtn}
                              imgSource={require('./../assets/images/3d-view-icon-b.png')}
                              title="3D View"
                              onPress={checkProjectAvailability}
                            />
                          ) : null}
                        </>
                      )}

                      {projectGallery && projectGallery.length > 4 && (
                        <AppProjectDetailsViewItem
                          containerStyle={styles.imgItemBtn}
                          imgSource={require('./../assets/images/photos-icon.png')}
                          title={getMorePhotoTitle()}
                          onPress={() => onHandleImageModal()}
                        />
                      )}
                    </View>
                  </View>
                </View>

                {!isPlotProject(projectDetails) ? (
                  <View style={styles.infoContainer}>
                    <View style={styles.infoItemPrice}>
                      <AppText style={styles.infoLabel}>INR</AppText>
                      {projectDetails && projectDetails.starting_price ? (
                        <AppText style={styles.infoValue}>
                          {appCurrencyFormatter.landingScreenCurrencyFormat(
                            projectDetails.starting_price,
                          )}
                        </AppText>
                      ) : (
                        <AppText style={styles.infoValue}>NA</AppText>
                      )}
                    </View>
                    <View style={styles.infoBorder} />
                    <View style={styles.infoItemPossession}>
                      <AppText style={styles.infoLabel}>
                        Possession Date
                      </AppText>
                      {projectDetails && projectDetails.possesion_dt ? (
                        <AppText style={styles.infoValue}>
                          {projectDetails.possesion_dt}
                        </AppText>
                      ) : (
                        <AppText style={styles.infoValue}>NA</AppText>
                      )}
                    </View>

                    <View style={styles.infoBorder} />

                    {/* <View style={styles.infoItemHome}>
                    <AppText style={styles.infoLabel}>Homes Left</AppText>
                    <AppText style={styles.infoValue}>07</AppText>
                  </View> */}
                    <View style={styles.infoItemHome}>
                      <AppText style={styles.infoLabel}>Typology</AppText>
                      {projectDetails &&
                      projectDetails.field_property_details_typology ? (
                        <AppText style={styles.infoValue} numberOfLines={3}>
                          {projectDetails.field_property_details_typology}
                        </AppText>
                      ) : (
                        <AppText style={styles.infoValue}>NA</AppText>
                      )}
                    </View>
                  </View>
                ) : (
                  <View style={styles.infoContainer}>
                    <View style={[styles.infoItemPrice, {flex: 1}]}>
                      <AppText style={styles.infoLabel}>INR</AppText>
                      {projectDetails && projectDetails.starting_price ? (
                        <AppText style={styles.infoValue}>
                          {appCurrencyFormatter.landingScreenCurrencyFormat(
                            projectDetails.starting_price,
                          )}
                        </AppText>
                      ) : (
                        <AppText style={styles.infoValue}>NA</AppText>
                      )}
                    </View>
                    <View style={styles.infoBorder} />
                    <View style={[styles.infoItemPossession, {flex: 1}]}>
                      <AppText style={styles.infoLabel}>
                        Possession Date
                      </AppText>
                      {projectDetails && projectDetails.possesion_dt ? (
                        <AppText style={styles.infoValue}>
                          {projectDetails.possesion_dt}
                        </AppText>
                      ) : (
                        <AppText style={styles.infoValue}>NA</AppText>
                      )}
                    </View>
                  </View>
                )}

                <View style={styles.descriptionContainer}>
                  {readMore ? (
                    <AppText style={styles.description}>
                      {projectDetails &&
                      projectDetails.field_project_details_excerpt
                        ? projectDetails.field_project_details_excerpt
                        : ''}
                      &nbsp;
                      <AppClickableText onPress={toogleReadMore}>
                        {projectDetails &&
                        projectDetails.field_project_details_excerpt
                          ? 'Read more'
                          : ''}
                      </AppClickableText>
                    </AppText>
                  ) : (
                    <AppText style={styles.description}>
                      {projectDetails &&
                      projectDetails.field_project_details_descriptio
                        ? projectDetails.field_project_details_descriptio
                        : ''}
                      &nbsp;
                      <AppClickableText onPress={toogleReadMore}>
                        {projectDetails &&
                        projectDetails.field_project_details_excerpt
                          ? 'Read Less'
                          : ''}
                      </AppClickableText>
                    </AppText>
                  )}
                </View>

                <View style={styles.btnContainer}>
                  <AppButton
                    title="SCHEDULE A VISIT"
                    color="primary"
                    textColor="secondary"
                    onPress={goToSiteVisitScreen}
                  />

                  {projectDetails &&
                    projectDetails.field_is_available_for_booking && (
                      <AppButton
                        opacity={
                          projectDetails.field_is_available_for_booking == '0'
                            ? 0.6
                            : 1
                        }
                        title={
                          projectDetails.eoi_enabled &&
                          projectDetails.eoi_enabled === '1'
                            ? 'Pre-book Now'
                            : 'Book Now'
                        }
                        onPress={onHandleBookNow}
                      />
                    )}
                </View>

                {projectFeaturedMenu &&
                  projectFeaturedMenu.length > 0 &&
                  projectFeaturedMenu.map((featuredMenu, index) => (
                    <View key={index} style={styles.menuContainer}>
                      {featuredMenu &&
                        featuredMenu.length > 0 &&
                        featuredMenu.map((featuredMenuItem, indexItem) => (
                          <AppProjectDetailsMenuItem
                            key={indexItem}
                            title={featuredMenuItem.name}
                            onPress={() =>
                              onHandleMenuItemClick(featuredMenuItem)
                            }
                          />
                        ))}
                    </View>
                  ))}
              </View>
            </ScrollView>
          )}
        </>
      )}

      {!showWebview && <AppFooter activePage={0} isPostSales={false} />}

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        supportedOrientations={['portrait', 'landscape']}
        onRequestClose={() => {
          Orientation.lockToPortrait();
          setModalVisible(!modalVisible);
        }}>
        {onlyVideos ? (
          <VideoModalScreen
            onPress={() => {
              Orientation.lockToPortrait();
              setModalVisible(!modalVisible);
            }}
            projectName={
              projectDetails && projectDetails.title ? projectDetails.title : ''
            }
            videoUri={
              projectDetails && projectDetails.proj_vid
                ? projectDetails.proj_vid
                : ''
            }
            setIsLoading={setIsLoading}
          />
        ) : (
          <GalleryModalScreen
            onPress={() => {
              Orientation.lockToPortrait();
              setModalVisible(!modalVisible);
            }}
            projectName={
              projectDetails && projectDetails.title ? projectDetails.title : ''
            }
            galleryData={isShowGalleryView ? galleryView : projectGallery}
            imgIndex={activeIndex}
          />
        )}
      </Modal>

      <AppOverlayLoader
        isLoading={isLoading || isViewLoading || isInnerLoading}
      />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 18,
    paddingVertical: 10,
  },
  projectTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: 5,
    textTransform: 'uppercase',
  },
  projectLocation: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.secondary,
  },
  descriptionContainer: {
    marginTop: 25,
  },
  description: {
    textAlign: 'justify',
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    lineHeight: 28,
  },
  btnContainer: {
    marginVertical: 25,
  },
  menuContainer: {
    marginVertical: 14,
  },
  infoContainer: {
    width: '100%',
    justifyContent: 'space-between',
    // alignItems: 'center',
    flexDirection: 'row',
    // paddingHorizontal: 2,
  },
  infoItemPrice: {flex: 1.3, alignItems: 'center'},
  infoItemPossession: {flex: 0.9, alignItems: 'center'},
  infoItemHome: {flex: 1, alignItems: 'center'},
  infoLabel: {fontSize: appFonts.mediumFontSize, marginBottom: 2},
  infoValue: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    textAlign: 'center',
  },
  infoBorder: {
    backgroundColor: colors.gray2,
    width: 1,
    height: '75%',
    marginHorizontal: 2,
    alignSelf: 'center',
  },
  viewContainer: {
    marginVertical: 25,
    height: windowHeight * 0.32,
    flexDirection: 'row',
  },
  videoContainer: {
    flex: 1,
    marginRight: 8,
  },
  imgContainer: {
    flex: 1,
    justifyContent: 'space-between',
  },
  imgItem: {
    height: '32%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  imgItemBtn: {
    width: '48%',
    height: '100%',
    backgroundColor: colors.secondary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  activityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ProjectDetailsScreen;
