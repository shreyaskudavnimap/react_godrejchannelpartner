import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Image, FlatList, TouchableWithoutFeedback, TextInput, ScrollView, Picker } from 'react-native';
import { useBackHandler } from '@react-native-community/hooks';
import Modal from 'react-native-modal';
import colors from '../config/colors';
import appFonts from '../config/appFonts';
import Screen from '../components/Screen';
import AppText from '../components/ui/ AppText';
import AppButton from '../components/ui/AppButton';
import { useDispatch, useSelector } from 'react-redux';
import AppFileChooser from './../components/actionSheet/AppFileChooser';
import appFileChooser from './../utility/appFileChooser';
import appConstant from '../utility/appConstant';
import { useNavigation } from '@react-navigation/native';
import appSnakBar from '../utility/appSnakBar';
import apiLoanDisbursement from '../api/apiLoanDisbursement';
import BankDetailsView from './BankDetailsView';
// import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import DropDownPicker from 'react-native-dropdown-picker';
import useFileDownload from '../hooks/useFileDownload';
import AppProgressBar from '../components/ui/AppProgressBar';
import Orientation from 'react-native-orientation';

const BankDetails = (props) => {
    let controller;
    const bookingId = props.bookingId;
    const [formMode, setFormMode] = useState('Add');
    const [bankDetailID, setBankDetailID] = useState('');
    const [sanctionLetterUrl, setSanctionLetterUrl] = useState('');
    const [sanctionLetterName, setSanctionLetterName] = useState('');

    const [bankerEmailClicked, isBankerEmailClicked] = useState(false);
    const [loanAmountClicked, isLoanAmountClicked] = useState(false);
    const [loanAccountNoClicked, isLoanAccountNoClicked] = useState(false);

    const [bankName, setBankName] = useState('HDFC Bank');
    const [bankerEmail, setBankerEmail] = useState('');
    const [loanAmount, setLoanAmount] = useState('');
    const [loanAccountNumber, setLoanAccountNumber] = useState('');

    const [bankerEmailError, setBankerEmailError] = useState('');
    const [loanAmountError, setLoanAmountError] = useState('');
    const [loanAccountNoError, setLoanAccountNoError] = useState('');
    const [sanctionLetterUrlError, setSanctionLetterUrlError] = useState('');


    const [projectId, setProjectId] = useState('');
    const navigation = useNavigation();
    const bankData = [{ label: "HDFC Bank", value: "HDFC Bank" }];

    const loginvalue = useSelector(
        (state) => state.loginInfo.loginResponse,
    );
    const userId = typeof loginvalue.data !== 'undefined' ? loginvalue.data.userid : '';

    const dashboardData = useSelector((state) => state.dashboardData);
    const propertyData = typeof dashboardData.rmData !== 'undefined' ? dashboardData.rmData : null;
    // console.log("propertyData",propertyData);
    const propertyIndex = propertyData.findIndex(
        (x) => x.booking_id == bookingId
    );
    var propertyName = "";
    if (typeof propertyData[propertyIndex] !== 'undefined') {
        propertyName = propertyData[propertyIndex].property_name + '/' + propertyData[propertyIndex].inv_flat_code;
    }

    const dispatch = useDispatch();

    const [fileChooserResponse, setFileChooserResponse] = useState(null);

    const [fileChooser, setFileChooser] = useState(false);
    const closeActionSheet = () => setFileChooser(false);

    const onClickFileChooserItem = async (clickItem) => {
        switch (clickItem) {
            case 'Remove':
                setFileChooser(false);
                setFileChooserResponse(null);
                break;

            case 'Gallery':
                appFileChooser.onLaunchImageLibrary((fileChooserResponse) => {
                    setFileChooser(false);
                    if (fileChooserResponse.didCancel) {
                        setFileChooserResponse(null);
                    } else {
                        setFileChooserResponse(fileChooserResponse);
                    }
                });
                break;

            case 'Camera':
                appFileChooser.onLaunchCamera((fileChooserResponse) => {
                    setFileChooser(false);
                    if (fileChooserResponse.didCancel) {
                        setFileChooserResponse(null);
                    } else {
                        setFileChooserResponse(fileChooserResponse);
                    }
                });
                break;

            case 'Phone':
                try {
                    const chooserResponse = await appFileChooser.onSelectFile();
                    const dataUriResponse = await appFileChooser.toBase64(chooserResponse.uri);
                    chooserResponse.data = dataUriResponse;
                    setFileChooser(false);
                    setFileChooserResponse(chooserResponse);

                    // const dataUriResponse = await appFileChooser.toBase64(fileChooserResponse.uri);
                    // console.log(dataUriResponse)
                } catch (error) {
                    console.log('Error onSelectFile : ', error);
                    setFileChooserResponse(null);
                }
                break;
        }

        setSanctionLetterName('');
        setSanctionLetterUrl('');

    };

    const validateFileSize = (fileSize) => {
        let msg = '';
        appFileChooser.validateFileSize(fileSize, (fileChooserResponse) => {
            msg = fileChooserResponse
                ? ''
                : 'File size should  be less than 5MB';
        });
        return msg;
    };

    const [showDetailView, isShowDetailView] = useState(false);
    const [preventBack, isPreventBack] = useState(false);
    const headerBack = props.showBankDetailView;
    console.log("headerBack", headerBack)

    useEffect(() => {
        Orientation.lockToPortrait();
    }, []);

    useEffect(() => {
        isShowDetailView(headerBack);
    }, [headerBack]);

    useEffect(() => {
        checkBankDetailsExist();
        setLoanAccountNumber('');
        setLoanAmount('');
        setBankerEmail('');
        setBankerEmailError('');
        setLoanAmountError('');
        setLoanAccountNoError('');
        setSanctionLetterUrlError('');
        setFileChooserResponse(null);
        setFormMode('Add');

    }, [bookingId]);

    // useEffect(() => {
    //     validateSanctionLetter();        
    // }, [fileChooserResponse]);

    const checkBankDetailsExist = async () => {
        props.setIsLoading(true);
        try {
            const requestData = { user_id: userId, booking_id: bookingId };
            const checkBankDetail = await apiLoanDisbursement.checkBankDetails(requestData);
            props.setIsLoading(false);
            if (checkBankDetail.data.status != 200) {
                isShowDetailView(true);
            } else {
                isShowDetailView(false);
            }
            setProjectId(bookingId);
        } catch (error) {
            console.log(error);
            props.setIsLoading(false);
            appSnakBar.onShowSnakBar(
                appConstant.appMessage.APP_GENERIC_ERROR,
                'LONG',
            );
        }
    };

    const onValueChangeBank = (value) => {
        setBankName(value);
    }

    const setLoader = (flag = false) => {
        props.setIsLoading(flag);
    }

    const validateBankDetails = () => {
        let validEmail = validateEmail();
        let validAmount = validateLoanAmount();
        let validAccNo = validateLoanAccountNo();
        if (!validEmail && !validAmount && !validAccNo) {
            return true;
        } else {
            return false;
        }
    }
    const validateEmail = () => {
        let error = true;
        let format = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let validEmail = format.test(bankerEmail);
        if (!bankerEmail) {
            setBankerEmailError(appConstant.appMessage.BANKER_EMAIL_REQUIRED);
        } else if (!validEmail) {
            setBankerEmailError(appConstant.appMessage.EMAIL_INVALID);
        } else {
            setBankerEmailError('');
            error = false;
        }
        return error;
    };

    const validateLoanAmount = () => {
        let error = true;
        let format = /^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/;
        let validAmount = format.test(loanAmount);
        if (!loanAmount) {
            setLoanAmountError(appConstant.appMessage.LOAN_AMOUNT_REQUIRED);
            // let frmErr = formError * true;
            // isFormError(frmErr);
        } else if (!validAmount) {
            setLoanAmountError(appConstant.appMessage.AMOUNT_INVALID);
            // let frmErr = formError * true;
            // isFormError(frmErr);
        } else {
            setLoanAmountError('');
            // let frmErr = formError * false;
            // isFormError(frmErr);
            error = false;
        }
        return error;
    };
    const validateLoanAccountNo = () => {
        let error = true;
        let pattern = /^[a-zA-Z0-9]{9,20}$/
        if (!loanAccountNumber) {
            setLoanAccountNoError(appConstant.appMessage.LOAN_AC_NO_REQUIRED);
            // let frmErr = formError * true;
            // isFormError(frmErr);
        } else if (!pattern.test(loanAccountNumber)) {
            setLoanAccountNoError(appConstant.appMessage.LOAN_AC_NO_INVALID);
        } else {
            setLoanAccountNoError('');
            // let frmErr = formError * false;
            // isFormError(frmErr);
            error = false;
        }
        return error;
    };

    const validateSanctionLetter = () => {
        let error = false;
        setSanctionLetterUrlError('');
        console.log("formMode", formMode);
        if (formMode != "Edit") {
            if (!sanctionLetterUrl) {
                if (!fileChooserResponse) {
                    setSanctionLetterUrlError("Please upload sanction letter");
                    error = true;
                } else {
                    let fileSize = fileChooserResponse.fileSize ? fileChooserResponse.fileSize : fileChooserResponse.size
                    let invalidSize = validateFileSize(fileSize);
                    if (invalidSize) {
                        setSanctionLetterUrlError(invalidSize);
                        error = true;
                    }
                }
            }
        }
        return error;
    }

    const handleSaveBankDetails = () => {
        let validate = validateBankDetails();
        let validSanctionLetter = validateSanctionLetter();
        console.log('formError', validate)
        if (validate && !validSanctionLetter) {
            let formData = {
                booking_id: bookingId,
                property_name: propertyName,
                bank_name: bankName,
                banker_email_id: bankerEmail,
                loan_account_number: loanAccountNumber,
                loan_amount: loanAmount,
                user_id: userId,
            }
            if (fileChooserResponse) {
                const fileBase64 = fileChooserResponse.data;
                const fileName = fileChooserResponse.name
                    ? fileChooserResponse.name
                    : fileChooserResponse.uri.split('/').pop();
                formData["files_attached"] = [];
                formData["files_attached"].push({
                    file_base64_encoded: fileBase64,
                    file_name: fileName,
                });
            }
            if (formMode == "Edit") {
                formData["bank_details_id"] = bankDetailID;
                formData['sanction_letter_name'] = sanctionLetterName;
            }
            // console.log(fileChooserResponse);
            console.log(formData);
            navigation.navigate('BankDetailsPreview', { formData, formMode });
        }

    }

    const showUpdateForm = () => {
        isShowDetailView(false);
        setFormMode("Edit");
        getBankDetails();
        props.isBankDetailsEditForm(true);
    }

    const getBankDetails = async () => {
        props.setIsLoading(true);
        isPreventBack(true);
        try {
            const requestParam = { user_id: userId, booking_id: bookingId }
            const bankDetails = await apiLoanDisbursement.getBankDetails(requestParam);
            props.setIsLoading(false);
            if (bankDetails.data.status == 200) {
                const formData = bankDetails.data.bank_details[0];
                setLoanAccountNumber(formData.account_no);
                setLoanAmount(formData.bank_loan_amount);
                setBankerEmail(formData.banker_email_id);
                setBankDetailID(formData.nid);
                setSanctionLetterName(formData.sanction_letter_name);
                setSanctionLetterUrl(formData.sanction_letter_url);
            }
        } catch (error) {
            console.log(error);
            props.setIsLoading(false);
            appSnakBar.onShowSnakBar(
                appConstant.appMessage.APP_GENERIC_ERROR,
                'LONG',
            );
        }
    }

    const { progress, showProgress, error, isSuccess, checkPermission, } = useFileDownload();

    const downloadSanctionLetter = () => {
        // const fileName = 'invoice_2018-07-10.pdf';
        let fileName = sanctionLetterName;

        if (fileName) {
            const ext = fileName.split('.')[0];
            if (ext) {
                downloadFile(sanctionLetterUrl, fileName, ext);
            }
        } else {
            let fileExt = appConstant.getExtention(sanctionLetterUrl);

            const fName = appConstant.getFileName(sanctionLetterUrl);
            if (fileExt[0] && fName) {
                downloadFile(sanctionLetterUrl, fName, fileExt[0]);
            }
        }
    }

    const downloadFile = async (imgPath, fileName, fileExtension) => {
        try {
            await checkPermission(imgPath, fileName, fileExtension);
        } catch (error) {
            appSnakBar.onShowSnakBar(error.message, 'LONG');
        }
    };

    useBackHandler(() => {
        // console.log("bbbbbb", preventBack);
        if (preventBack) {
            isShowDetailView(true);
            isPreventBack(false);
            props.isBankDetailsEditForm(false);
            setFileChooserResponse(null);
            return true;
        } else {
            return false;
        }
    });

    return (
        <>

            {showDetailView ? (
                <BankDetailsView
                    bookingId={bookingId}
                    projectId={projectId}
                    showUpdateForm={showUpdateForm}
                    setLoader={setLoader}
                    loading={props.isLoading} />
            ) : (

                    !props.isLoading ? (
                        <>
                            <ScrollView>
                                <View style={styles.container}
                                    onStartShouldSetResponder={() => {
                                        controller.close();
                                    }}>
                                    <View style={styles.inputContainer}>
                                        <AppText style={styles.title}>Select Bank</AppText>
                                        <DropDownPicker
                                            items={bankData}
                                            defaultValue={bankName}
                                            autoScrollToDefaultValue={true}
                                            style={styles.dropdownDocument}
                                            placeholderStyle={styles.dropdownPlaceholder}
                                            itemStyle={styles.dropDownItem}
                                            labelStyle={styles.label}
                                            activeLabelStyle={styles.labelSelected}
                                            selectedLabelStyle={styles.labelSelected}
                                            onChangeItem={(item) => onValueChangeBank(item.value)}
                                            placeholder="Select Property"
                                            controller={instance => controller = instance}
                                        />
                                    </View>

                                    <View style={styles.inputContainer}>
                                        <AppText style={styles.title}>Banker Email Id</AppText>
                                        <TextInput
                                            style={[styles.input, bankerEmailError ? styles.inputError : '']}
                                            value={bankerEmail}
                                            onBlur={() => {
                                                validateEmail();
                                            }}
                                            onChangeText={value => {
                                                setBankerEmail(value);
                                            }}
                                        />
                                        {bankerEmailError ? (<Text style={styles.error}>{bankerEmailError}</Text>) : (null)}
                                    </View>

                                    <View style={styles.inputContainer}>
                                        <AppText style={styles.title}>Loan Amount</AppText>
                                        <View style={styles.loanAmount}>
                                            <AppText style={styles.rupee} >&#8377;</AppText>
                                            <TextInput
                                                style={[styles.input, { flex: 0.95, marginLeft: -10, marginTop: 0 }, loanAmountError ? styles.inputError : '']}
                                                keyboardType='numeric'
                                                value={loanAmount}
                                                onBlur={() => {
                                                    validateLoanAmount();
                                                }}
                                                onChangeText={value => {
                                                    setLoanAmount(value)
                                                }}
                                            />
                                        </View>
                                        {loanAmountError ? (<AppText style={styles.error}>{loanAmountError}</AppText>) : (null)}
                                    </View>

                                    <View style={styles.inputContainer}>
                                        <AppText style={styles.title}>Loan Account Number</AppText>
                                        <TextInput
                                            style={[styles.input, loanAccountNoError ? styles.inputError : '']}
                                            value={loanAccountNumber}
                                            onBlur={() => {
                                                validateLoanAccountNo();
                                            }}
                                            onChangeText={value => {
                                                setLoanAccountNumber(value)
                                            }}
                                        />
                                        {loanAccountNoError ? (<AppText style={styles.error}>{loanAccountNoError}</AppText>) : (null)}
                                    </View>
                                    <View style={styles.inputContainer}>
                                        <View style={styles.uploadBlock}>
                                            <AppText style={styles.title}>Upload Sanction Letter</AppText>
                                            <TouchableOpacity style={styles.attachItem}
                                                onPress={() => setFileChooser(true)}>
                                                <Image style={styles.attachIcon}
                                                    source={require('../assets/images/attachment-icon.png')}
                                                    resizeMode="contain" />
                                            </TouchableOpacity>
                                        </View>
                                        {sanctionLetterUrlError ? (<AppText style={styles.error}>{sanctionLetterUrlError}</AppText>) : (null)}
                                        {fileChooserResponse ? (
                                            <View style={styles.previewBlock}>
                                                <AppText style={styles.preview}>
                                                    {fileChooserResponse.name
                                                        ? appConstant.fileNameSanitize(fileChooserResponse.name)
                                                        : appConstant.fileNameSanitize(fileChooserResponse.uri.split('/').pop())}
                                                </AppText>

                                                <TouchableOpacity style={styles.close}
                                                    onPress={() => setFileChooserResponse(null)}
                                                >
                                                    <Icon name={'close'} color={colors.charcoal} size={25} />
                                                </TouchableOpacity>
                                            </View>
                                        ) : (null)}

                                        {sanctionLetterName ? (
                                            <View>
                                                <TouchableOpacity
                                                    style={styles.downloadBlock}
                                                    onPress={() => downloadSanctionLetter()}
                                                >
                                                    <AppText style={styles.previewDownload}>
                                                        {appConstant.fileNameSanitize(sanctionLetterName)}
                                                    </AppText>
                                                    <Image style={styles.downloadIcon}
                                                        source={require('../assets/images/download-b.png')}
                                                        resizeMode="contain" />
                                                </TouchableOpacity>
                                            </View>
                                        ) : null}

                                    </View>
                                    <View style={styles.button}>
                                        <AppButton title="Submit Details"
                                            onPress={e => {
                                                handleSaveBankDetails()
                                            }} />
                                    </View>
                                </View>
                            </ScrollView>

                            <Modal
                                isVisible={fileChooser}
                                style={{
                                    margin: 0,
                                    justifyContent: 'flex-end',
                                }}
                                onBackButtonPress={closeActionSheet}
                                onBackdropPress={closeActionSheet}
                                backdropColor="rgba(0,0,0,0.5)">
                                <AppFileChooser
                                    onPress={(clickItem) => onClickFileChooserItem(clickItem)}
                                />
                            </Modal>
                        </>
                    ) : null
                )}

            {showProgress && progress && progress > 0 ? (
                <AppProgressBar progress={progress} />
            ) : null}

        </>
    );
};

const styles = StyleSheet.create({
    pageBlock: {
        padding: 10
    },
    container: {
        flex: 1,
        // paddingHorizontal: 20,
        // paddingVertical: 10,
        // paddingHorizontal: '6%',
        justifyContent: 'center',
        marginBottom: 20,
    },
    itemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        // paddingHorizontal: 24,
        paddingVertical: 5,
        marginTop: 24,
        marginVertical: 20,
        backgroundColor: '#f7f7f7',
    },
    title: {
        display: "flex",
        marginTop: -1,
        marginBottom: -10,
    },
    inputContainer: {
        marginTop: '5%',
    },
    input: {
        width: '100%',
        // marginTop: '1%',
        fontSize: appFonts.normalFontSize,
        // height: 40,
        borderBottomWidth: 1,
        marginTop: 4,
        paddingVertical: 5,
        borderBottomColor: colors.lightGray,
        fontFamily: appFonts.SourceSansProSemiBold
    },
    rupee: {
        flex: 0.05,
        fontSize: appFonts.normalFontSize,
        fontFamily: appFonts.SourceSansProSemiBold
    },
    loanAmount: {
        flex: 1,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center',
        // fontSize: appFonts.largeBold,,
        includeFontPadding: false,
        marginTop: 4,
    },
    uploadBlock: {
        flex: 1,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 10
    },
    attachItem: {
        alignItems: 'flex-end',
        // marginLeft: '20%'
    },
    attachIcon: {
        // width: 30,
        height: "100%",
        marginTop: 10,
        marginBottom: 10

    },
    previewBlock: {
        flex: 1,
        flexDirection: "row",
        alignItems: 'center',
        width: '100%',
        display: 'flex',
        backgroundColor: colors.lightGray,
        paddingHorizontal: 15,
        borderRadius: 15,
        justifyContent: 'space-between',
        paddingVertical: 15
    },
    preview: {
        flexShrink: 1,
        width: '90%',
        paddingRight: 15
    },
    close: {
        color: 'black',
        fontSize: appFonts.largeFontSize,
        width: '10%',
        alignSelf: 'baseline'
    },
    downloadBlock: {
        flex: 1,
        flexDirection: "row",
        alignItems: 'center',
        width: '100%',
        // display: 'flex',
        backgroundColor: colors.lightGray,
        paddingHorizontal: 15,
        borderRadius: 15,
        justifyContent: 'space-between'

    },
    previewDownload: {
        flexShrink: 1,
        width: '90%',
    },
    downloadIcon: {
        width: '5%',
        alignSelf: 'baseline'
    },
    error: {
        color: colors.danger,
        fontSize: appFonts.smallFontSize,
        marginBottom: 5,
    },
    inputError: {
        borderBottomColor: colors.danger,
    },
    dropdownDocument: {
        height: 50,
        marginTop: 15,
        marginBottom: 8,
        borderRightWidth: 0,
        borderLeftWidth: 0,
        borderTopWidth: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    dropdownPlaceholder: {
        fontSize: appFonts.largeFontSize,
        fontFamily: appFonts.SourceSansProRegular,
    },
    labelSelected: {
        fontSize: appFonts.largeFontSize,
        fontFamily: appFonts.SourceSansProSemiBold,
        marginBottom: 8,
    },
    label: {
        fontSize: appFonts.largeFontSize,
        fontFamily: appFonts.SourceSansProRegular,
        marginBottom: 8,
        textAlign: 'left',
    },
    dropDownItem: {
        borderBottomWidth: 1,
        borderBottomColor: colors.lightGray,
        marginBottom: 8,
        marginTop: 4,
        justifyContent: 'flex-start',
    },

})
export default BankDetails;