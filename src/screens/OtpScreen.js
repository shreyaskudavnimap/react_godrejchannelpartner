import React, {useCallback, useEffect, useState, useRef} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
  Text,
  Dimensions
} from 'react-native';

import Screen from '../components/Screen';
import AppButton from '../components/ui/AppButton';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import appFonts from '../config/appFonts';
import AppHeader from '../components/ui/AppHeader';
import {useDispatch, useSelector} from 'react-redux';
import ShowLoader from '../components/ui/ShowLoader';
import appSnakBar from '../utility/appSnakBar';
import apiLogin from './../api/apiLogin';
import DeviceInfo from 'react-native-device-info';
import * as userLoginAction from './../store/actions/userLoginAction';
import * as userSignUpAction from './../store/actions/userSignUpAction';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNavigation} from '@react-navigation/native';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import {CommonActions} from '@react-navigation/native';
import { GoogleAnalyticsTracker } from "react-native-google-analytics-bridge";

import * as sendOtpByServer from './../store/actions/sendOtpAction';
import {startClock} from 'react-native-reanimated';
import colors from '../config/colors';
import Orientation from 'react-native-orientation';
const windowHeight = Dimensions.get('window').height;
const OtpScreen = ({props, route}) => {
  const routeParams = route.params;
  const mobileNo =
    routeParams && routeParams.mobileNo && routeParams.mobileNo
      ? routeParams.mobileNo
      : '';

  const userId =
    routeParams && routeParams.userId && routeParams.userId
      ? routeParams.userId
      : '';

  const emailID =
    routeParams && routeParams.emailID && routeParams.emailID
      ? routeParams.emailID
      : '';

  const firstName =
    routeParams && routeParams.firstName && routeParams.firstName
      ? routeParams.firstName
      : '';

  const lastName =
    routeParams && routeParams.lastName && routeParams.lastName
      ? routeParams.lastName
      : '';

  const password =
    routeParams && routeParams.password && routeParams.password
      ? routeParams.password
      : '';

  const countryId =
    routeParams && routeParams.countryId && routeParams.countryId
      ? routeParams.countryId
      : '';

  const panNo =
    routeParams && routeParams.panNo && routeParams.panNo
      ? routeParams.panNo
      : '';

  const actionType =
    routeParams && routeParams.actionType && routeParams.actionType
      ? routeParams.actionType
      : '';

  const [isLoading, setIsLoading] = useState(false);
  const [isApiLoading, setApiIsLoading] = useState(false);
  const [error, setError] = useState();
  const [otp1, setOtp1] = useState('');
  const [otp2, setOtp2] = useState('');
  const [otp3, setOtp3] = useState('');
  const [otp4, setOtp4] = useState('');
  const [clockCount, setClockCount] = useState(15);
  const [isActive, setIsActive] = useState(false);
  const [isKeyboardVisible, setKeyboardVisible] = useState(false);
  //const [deviceType, setDeviceType] = useState('');
  const [otp1Editable, setOtp1Editable] = useState(true);
  const [otp2Editable, setOtp2Editable] = useState(true);
  const [otp3Editable, setOtp3Editable] = useState(true);
  const [otp4Editable, setOtp4Editable] = useState(true);
  const navigation = useNavigation();

  const first = useRef('first');
  const second = useRef('second');
  const third = useRef('third');
  const four = useRef('four');
  const otpCountStatus = useRef(0);
  var deviceType = "";

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );

  //const count = useSelector(state => state.counter.count);
  const dispatch = useDispatch();
  let interval = null;
  let version = DeviceInfo.getVersion();
  
  
  var tracker = new GoogleAnalyticsTracker("UA-171278332-1");

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  
 

  useEffect(() => {
    // if (
    //   Platform.OS === 'ios' ? setDeviceType('IOS') : setDeviceType('ANDROID')
    // );

    if(Platform.OS === 'ios'){
      deviceType = 'IOS';
    }else{
      deviceType = 'ANDROID';
    }

    if (
      actionType == 'forget_password' ||
      actionType == 'forget_pwd_by_exist_cust'
    ) {
      startClock();
      appSnakBar.onShowSnakBar(`OTP sent successfully`, 'LONG');
    } else {
      sendOtpToUser(mobileNo, deviceUniqueId);
    }

    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        setKeyboardVisible(true);
      },
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        setKeyboardVisible(false);
      },
    );

    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
      clearInterval(interval);
    };
  }, [deviceUniqueId]);

  const loginvalue = useSelector((state) => state.loginInfo.loginResponse);

  const sendOtpToUser = useCallback(
    async (mobile, deviceUniqueId) => {
      //setIsLoading(true);
      try {
        await dispatch(
          sendOtpByServer.sendOtpForVerify(mobile, deviceUniqueId),
        );
      } catch (err) {
        setError(err.message);
      }
      //setIsLoading(false);
      appSnakBar.onShowSnakBar(`OTP sent successfully`, 'LONG');
      startClock();
    },
    [dispatch, setError, deviceUniqueId],
  );

  const startClock = () => {
    if (!isActive) {
      interval = setInterval(() => {
        setClockCount((clockCount) =>
          clockCount > 0 ? clockCount - 1 : clearClock(),
        );
      }, 1000);
    }
    if (isActive) {
      clearInterval(interval);
    }
  };

  const clearClock = () => {
    setClockCount(0);
    clearInterval(interval);
  };

  const _resendOtp = () => {
    tracker.trackEvent("OTP Verification", "Click", { label: "Resend", value: 8 });
    if (otpCountStatus.current == 507) {
      otpCountStatus.current = 0;
      setOtp1Editable(true);
      setOtp2Editable(true);
      setOtp3Editable(true);
      setOtp4Editable(true);
    }
    setOtp1('');
    setOtp2('');
    setOtp3('');
    setOtp4();
    if (actionType == 'forget_pwd_by_exist_cust') {
      sendOtpByPan();
    } else {
      sendOtpToUser(mobileNo, deviceUniqueId);
      setClockCount(15);
    }
  };

  const sendOtpByPan = async () => {
    const request = {
      pan: panNo,
      device_id: deviceUniqueId,
    };
    const result = await apiLogin.apiForForgetPwdByPan(request);
    var status = result.data.status;
    if (status == 200) {
      appSnakBar.onShowSnakBar(`OTP sent successfully`, 'LONG');
    } else {
      appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
    }
    setClockCount(15);
    startClock();
  };

  const _getOtpForConfirm = () => {
    tracker.trackEvent("OTP Verification", "Submit", { label: "Verify", value: 9 });
    //console.log( otpCountStatus.current);
    if (otpCountStatus.current == 507) return null;
    if (otp1 != '' && otp2 != '' && otp3 != '' && otp4 != '') {
      _validateOtp(otp1 + otp2 + otp3 + otp4);
    } else {
      appSnakBar.onShowSnakBar(`Enter otp`, 'LONG');
    }
  };

  const _validateOtp = async (otp) => {
    const request = {
      device_id: deviceUniqueId,
      otp: otp,
    }; //user_id: userId,

    console.log(JSON.stringify(request));
    setIsLoading(true);
    const result = await apiLogin.apiForValidateOtp(request);
    setIsLoading(false);
    var status = result.data.status;
    console.log('_validateOtp = ' + JSON.stringify(result.data));
    //status = 200; /* testing purpose */
    if (status == 200) {
      if (actionType == 'proceed_with_otp') {
        callNewUserLoginApi(
          version,
          mobileNo,
          '',
          deviceUniqueId,
          deviceType,
          'otp',
        );
      }
      if (actionType == 'new_signup') {
        callNewUserSignUp(
          firstName,
          lastName,
          emailID,
          mobileNo,
          password,
          deviceType,
          deviceUniqueId,
          countryId,
          version,
        );
      }
      if (actionType == 'forget_password') {
        navigation.navigate('SetPassword', {
          userId: userId,
          actionType: 'new_user_forget_password',
        });
      }
      if (actionType == 'forget_pwd_senario_newuser') {
        _validatePassword();
      }
      if (actionType == 'exist_cust_pan_login') {
        navigation.navigate('SetPassword', {
          userSFId: firstName,
          actionType: 'exist_cust_pan_login',
        });
      }
      if (actionType == 'forget_pwd_by_exist_cust') {
        navigation.navigate('SetPassword', {
          userId: userId,
          actionType: 'forget_pwd_by_exist_cust',
        });
      }
      if (actionType == 'forget_pwd_by_exist_cust_multiMob') {
        navigation.navigate('SetPassword', {
          userSFId: firstName,
          actionType: 'forget_pwd_by_exist_cust_multiMob',
        });
      }
    } else {
      if (status == 507) {
        setOtp1('');
        setOtp2();
        setOtp3();
        setOtp4();
        setOtp1Editable(false);
        setOtp2Editable(false);
        setOtp3Editable(false);
        setOtp4Editable(false);
        otpCountStatus.current = 507;
      }
      appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
    }
  };

  const callNewUserLoginApi = useCallback(
    async (appVersion, username, password, deviceId, deviceType, loginType) => {
      setIsLoading(true);
      try {
        await dispatch(
          userLoginAction.getUserLoginInfo(
            appVersion,
            username,
            password,
            deviceId,
            deviceType,
            loginType,
          ),
        );
      } catch (err) {
        setError(err.message);
      }
      setIsLoading(false);
      checkLoginStatus();
    },
    [dispatch, setIsLoading, setError],
  );

  const callNewUserSignUp = useCallback(
    async (
      firstName,
      lastName,
      emailID,
      mobileNo,
      password,
      deviceType,
      deviceUniqueId,
      countryId,
      version,
    ) => {
      setIsLoading(true);
      try {
        await dispatch(
          userSignUpAction.getUserSignupInfo(
            firstName,
            lastName,
            emailID,
            mobileNo,
            password,
            deviceType,
            deviceUniqueId,
            countryId,
            version,
          ),
        );
      } catch (err) {
        setError(err.message);
      }
      setIsLoading(false);
      checkLoginStatus();
    },
    [dispatch, setIsLoading, setError],
  );

  const checkLoginStatus = async () => {
    const isLogin = await AsyncStorage.getItem('isLogin');
    //alert(value);
    if (isLogin == 'true') {
      let fcmToken = await AsyncStorage.getItem('fcmToken');
      let userDATA = await AsyncStorage.getItem('data');
      let tempArr = JSON.parse(userDATA);
      let userid = tempArr.userid;

      const request = {
        device_token: fcmToken,
        user_id: loginvalue.data ? loginvalue.data.userid : userid,
        device_type: deviceType,
        device_id: deviceUniqueId,
      };
      console.log('request = '+JSON.stringify(request));
      await dispatch(userLoginAction.updateDeviceToken(request));
      //navigation.navigate('AppStackRoute');
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'AppStackRoute'}],
        }),
      );
    } else {
      const msg = await AsyncStorage.getItem('error_msg');
      if (msg) {
        appSnakBar.onShowSnakBar(msg, 'LONG');
      } else {
        appSnakBar.onShowSnakBar(`Something went wrong`, 'LONG');
      }
    }
  };

  const _validatePassword = async () => {
    //alert(mobileNo);
    const request = {
      uid: userId,
      pass: password,
    };
    setIsLoading(true);
    const result = await apiLogin.apiForUpdatePassword(request);
    setIsLoading(false);
    var status = result.data.status;
    if (status == 200) {
      callNewUserLoginApi(
        version,
        countryId + mobileNo,
        password,
        deviceUniqueId,
        deviceType,
        'password',
      );
    } else {
      appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
    }
  };

  const setOtp1TextBox = (e) => {
    if (e.length == 1) {
      second.current.focus();
    }
    setOtp1(e);
  };

  const setOtp2TextBox = (e) => {
    if (e.length == 1) {
      third.current.focus();
    }
    if (e.length == 0) {
      first.current.focus();
    }
    setOtp2(e);
  };

  const setOtp3TextBox = (e) => {
    if (e.length == 1) {
      four.current.focus();
    }
    if (e.length == 0) {
      second.current.focus();
    }
    setOtp3(e);
  };

  const setOtp4TextBox = (e) => {
    if (e.length == 1) {
      Keyboard.dismiss();
    }
    if (e.length == 0) {
      third.current.focus();
    }
    setOtp4(e);
  };

  return (
    <Screen>
      <AppHeader />

      <View style={styles.container}>
        <AppTextBold style={styles.title}>VERIFY YOUR ACCOUNT</AppTextBold>

        <View style={{width: '100%', alignItems: 'center'}}>
          <Text style={{textAlign: 'center'}}>
            One Time Password(OTP) has been sent to {'\n'} your mobile number
          </Text>
        </View>
        <View style={styles.content}>
          <View style={styles.otpContainer}>
            <TextInput
              ref={first}
              style={styles.input}
              maxLength={1}
              value={otp1}
              onChangeText={(e) => {
                setOtp1TextBox(e);
              }}
              keyboardType="number-pad"
              editable={otp1Editable}
            />

            <TextInput
              ref={second}
              style={styles.input}
              maxLength={1}
              value={otp2}
              onChangeText={(e) => {
                setOtp2TextBox(e);
              }}
              keyboardType="number-pad"
              editable={otp2Editable}
            />

            <TextInput
              ref={third}
              style={styles.input}
              maxLength={1}
              value={otp3}
              onChangeText={(e) => {
                setOtp3TextBox(e);
              }}
              keyboardType="number-pad"
              editable={otp3Editable}
            />

            <TextInput
              ref={four}
              style={styles.input}
              maxLength={1}
              value={otp4}
              onChangeText={(e) => {
                setOtp4TextBox(e);
              }}
              keyboardType="number-pad"
              editable={otp4Editable}
            />
          </View>
          <View
            style={{
              flex: 1,
              // backgroundColor: '#ff0000',
              // marginTop: windowHeight*0.16,
              alignItems: 'center',
              justifyContent: 'flex-end',
              paddingBottom: 15
            }}>
            <AppText>
              Didn’t receive OTP?{' '}
              {clockCount == 0 ? '' : clockCount + ' seconds'}
            </AppText>
          </View>
        </View>
      </View>
      {!isKeyboardVisible ? (
        <View style={styles.btnContainer}>
          {clockCount == 0 ? (
            <AppButton
              color="primary"
              textColor="secondary"
              title="RESEND OTP"
              onPress={() => _resendOtp()}
            />
          ) : (
            <View />
          )}

          <AppButton title="CONFIRM" onPress={() => _getOtpForConfirm()} />
        </View>
      ) : (
        <View />
      )}

      {isLoading ? (
        //<ShowLoader/>
        <AppOverlayLoader isLoading={isLoading || isApiLoading} />
      ) : (
        <View />
      )}
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5,
  },
  otpContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    marginBottom: 15,
    fontSize:appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    alignItems: 'center',
  },
  content: {flex: 1, justifyContent: 'flex-start', marginTop: '40%'},

  input: {
    height: 50,
    borderColor: colors.darkBorderGrey,
    borderBottomWidth: 1,
    marginBottom: 60,
    fontSize: appFonts.largeBoldx,
    textAlign: 'center',
    marginLeft: 15,
    width: 55,
  },

  btnContainer: {marginHorizontal: 20, marginBottom: '15%'},
  resend: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 18,
    flexDirection: 'row',

    backgroundColor: '#FFFFFF',
    shadowColor: '#e0e0e0',
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15,
    shadowOffset: {width: 1, height: 20},
  },
  text: {
    fontSize: appFonts.largeBold,
    textTransform: 'uppercase',
    fontFamily: appFonts.SourceSansProBold,
  },
});

export default OtpScreen;
