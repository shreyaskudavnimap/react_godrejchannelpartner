import React, {useState, useEffect} from 'react';
import {View, StyleSheet, ScrollView, TouchableOpacity} from 'react-native';

import {Picker} from '@react-native-picker/picker';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppFooter from '../components/ui/AppFooter';
import appFonts from '../config/appFonts';
import AppText from '../components/ui/ AppText';
import colors from '../config/colors';
import Orientation from 'react-native-orientation';

const HomeLoan = () => {
  const [isEnquiry, setIsEnquiry] = useState(true);
  const [isLoanDetails, setIsLoanDetails] = useState(false);
  const [isDisbursal, setIsDisbursal] = useState(false);

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  return (
    <Screen>
      <AppHeader />
      <ScrollView>
        <View style={styles.container}>
          <AppText style={styles.pageTitle}>Home Loan</AppText>
          <View style={{flex: 1, fontSize: appFonts.mediumFontSize, paddingTop: '5%'}}>
            <Picker
              itemStyle={{
                color: colors.secondary,
                fontFamily: appFonts.SourceSansProRegular,
              }}
              mode="dropdown"
              style={styles.pickerStyle}>
              <Picker.Item color={colors.secondary} label="Select Property" />
            </Picker>
          </View>

          <View style={styles.tabName}>
            <View
              style={
                isEnquiry
                  ? styles.activeTab
                  : [styles.activeTab, {borderBottomWidth: 0}]
              }>
              <TouchableOpacity
                onPress={() => {
                  setIsEnquiry(true);
                  setIsLoanDetails(false);
                  setIsDisbursal(false);
                }}>
                <AppText
                  style={
                    isEnquiry
                      ? {fontWeight: 'bold', fontSize: appFonts.largeBold}
                      : {fontSize: appFonts.largeBold}
                  }>
                  Enquiry
                </AppText>
              </TouchableOpacity>
            </View>

            <View
              style={
                isLoanDetails
                  ? styles.activeTab
                  : [styles.activeTab, {borderBottomWidth: 0}]
              }>
              <TouchableOpacity
                onPress={() => {
                  setIsEnquiry(false);
                  setIsLoanDetails(true);
                  setIsDisbursal(false);
                }}>
                <AppText
                  style={
                    isLoanDetails
                      ? {fontWeight: 'bold', fontSize: appFonts.largeBold}
                      : {fontSize: appFonts.largeBold}
                  }>
                  Loan Details
                </AppText>
              </TouchableOpacity>
            </View>

            <View
              style={
                isDisbursal
                  ? styles.activeTab
                  : [styles.activeTab, {borderBottomWidth: 0}]
              }>
              <TouchableOpacity
                onPress={() => {
                  setIsEnquiry(false);
                  setIsLoanDetails(false);
                  setIsDisbursal(true);
                }}>
                <AppText
                  style={
                    isDisbursal
                      ? {fontWeight: 'bold', fontSize: appFonts.largeBold}
                      : {fontSize: appFonts.largeBold}
                  }>
                  Disbursal
                </AppText>
              </TouchableOpacity>
            </View>
          </View>

          {isEnquiry && (
            <View>
              <AppText>Enquiry View</AppText>
            </View>
          )}

          {isLoanDetails && (
            <View>
              <AppText>Loan Details View</AppText>
            </View>
          )}

          {isDisbursal && (
            <View>
              <AppText>Disbursal View</AppText>
            </View>
          )}
        </View>
      </ScrollView>

      <AppFooter activePage={0} isPostSales={false} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '6%',
    justifyContent: 'center',
  },
  pageTitle: {
    paddingTop: '5%',
    fontSize: appFonts.xxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    textTransform: 'uppercase',
  },
  tabName: {
    paddingBottom: '3%',
    marginBottom: '5%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  activeTab: {
    width: '33%',
    paddingVertical: '5%',
    borderBottomWidth: 3,
    borderBottomColor: colors.gray,
    alignItems: 'center',
  },
});

export default HomeLoan;
