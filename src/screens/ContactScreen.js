import React, { useState, useEffect } from 'react';
import {
  TextInput,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
  ScrollView,
  LayoutAnimation,
  UIManager,
  Dimensions,
  TouchableOpacity,
  Modal,
} from 'react-native';

import { validationDictionary } from '../utility/validation/dictionary';
import validatejs from 'validate.js';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import AppButton from '../components/ui/AppButton';
import apiContact from './../api/apiContact';
import appPhoneCall from '../utility/appPhoneCall';
import { useSelector, useDispatch } from 'react-redux';

import colors from '../config/colors';
import appFonts from '../config/appFonts';
import appSnakBar from '../utility/appSnakBar';
import appCountry from '../utility/appCountry';
import CountrySelectModalScreen from './modals/CountrySelectModalScreen';
import appConstant from '../utility/appConstant';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import Orientation from 'react-native-orientation';

var { height, width } = Dimensions.get('window');

const ContactScreen = (props) => {
  const [isExpandedCallUs, setIsExpandedCallUs] = useState(false);
  const [isExpandedRequestCall, setIsExpandedRequestCall] = useState(false);
  const [callUsData, setCallUsData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isAPILoading, setIsAPILoading] = useState(false);

  const [selectedCountry, setSelectedCountry] = useState(
    appCountry.COUNTRIES[0],
  );

  const [showCountryModal, setShowCountryModal] = useState(false);

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );

  const loginUserData = useSelector((state) => state.loginInfo.loginResponse);

  const userData =
    loginUserData && loginUserData.data && loginUserData.data
      ? loginUserData.data
      : '';
  const userId =
    loginUserData && loginUserData.data && loginUserData.data.userid
      ? loginUserData.data.userid
      : '';

  const [inputs, setInputs] = useState({
    mobNo: {
      type: 'indMobile',
      value: '',
      showErr: false,
    },
  });

  if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  const updateLayoutCallUs = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setIsExpandedCallUs(!isExpandedCallUs);
  };

  const updateLayoutRequestCall = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setIsExpandedRequestCall(!isExpandedRequestCall);
  };

  const dispatch = useDispatch();

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);


  useEffect(() => {
    if (deviceUniqueId) {
      getProjectsInfo();
    }
  }, [dispatch, deviceUniqueId]);


  const getProjectsInfo = async () => {
    setIsLoading(true);
    try {
      const result = await apiContact.apiProjectsList();
      const resultData = result.data;

      setIsLoading(false);
      if (
        resultData &&
        resultData.status == '200' &&
        resultData.india_number &&
        resultData.other_number
      ) {
        const { india_number, other_number } = resultData;
        setCallUsData([
          {
            id: '1',
            title: 'India Number',
            number: india_number,
          },
          {
            id: '2',
            title: 'Other Number',
            number: other_number,
          },
        ]);
      } else {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log('Get Project Info Error: ', error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const openDeviceCaller = (phoneNumber) => {
    // console.log("phoneNumber======================", phoneNumber);
    appPhoneCall.openDeviceCaller(phoneNumber);
  };

  const onSelectCountry = (selectedCountry) => {
    setSelectedCountry(selectedCountry);
    setShowCountryModal(false);

    const inputValidationType =
      selectedCountry &&
        selectedCountry.callingCodes &&
        selectedCountry.callingCodes === '+91'
        ? 'indMobile'
        : 'nriMobile';

    setInputs({
      ...inputs,
      ['mobNo']: {
        ...inputs['mobNo'],
        type: inputValidationType,
        errorLabel: inputs['mobNo'].optional
          ? null
          : validateInput({
            type: inputValidationType,
            value: inputs['mobNo'].value,
          }),
        showErr: false,
      },
    });
  };

  const requestCallBack = () => {
    if (userId && userId != '') {
      const callBackRequestLogged = {
        user_id: userId,
        mob_no: userData.mob_no ? userData.mob_no : '',
        device_id: '',
        country_code: userData.country_code ? userData.country_code : '',
      };
      sendRequestCallBack(callBackRequestLogged);
    } else {
      getFormValidation();

      if (inputs['mobNo'].value && !inputs['mobNo'].errorLabel) {
        const callBackRequest = {
          user_id: '',
          mob_no: selectedCountry.callingCodes + inputs['mobNo'].value,
          device_id: deviceUniqueId,
          country_code: selectedCountry.callingCodes,
        };
        sendRequestCallBack(callBackRequest);
      }
    }
  };

  const sendRequestCallBack = async (request) => {
    setIsAPILoading(true);
    try {
      const result = await apiContact.apiRequestCallBack(request);
      const resultData = result.data;

      setInputs({
        ...inputs,
        ['mobNo']: {
          ...inputs['mobNo'],
          value: '',
          errorLabel: null,
          showErr: false,
        },
      });

      setSelectedCountry(appCountry.COUNTRIES[0]);

      setIsAPILoading(false);

      appSnakBar.onShowSnakBar(
        resultData.msg
          ? resultData.msg
          : appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    } catch (error) {
      console.log('Get Project Info Error: ', error);
      setIsAPILoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const onInputChange = (id, value) => {
    setInputs({
      ...inputs,
      [id]: getInputValidationState(
        {
          input: inputs[id],
          value: value,
        },
        true,
      ),
    });
  };

  const getInputValidationState = ({ input, value }, isShowError) => {
    return {
      ...input,
      value,
      errorLabel: input.optional
        ? null
        : validateInput({ type: input.type, value }),
      showErr: isShowError ? isShowError : false,
    };
  };

  const validateInput = ({ type, value }) => {
    const result = validatejs(
      {
        [type]: value,
      },
      {
        [type]: validationDictionary[type],
      },
    );

    if (result) {
      return result[type][0];
    }

    return null;
  };

  const getFormValidation = () => {
    const updatedInputs = {};

    for (const [key, input] of Object.entries(inputs)) {
      updatedInputs[key] = getInputValidationState(
        {
          input,
          value: input.value,
        },
        true,
      );
    }

    setInputs(updatedInputs);
  };

  const renderError = (id) => {
    if (inputs && inputs[id].errorLabel && inputs[id].showErr) {
      return (
        <View style={styles.txtErrorContainer}>
          <AppText style={styles.txtError}>{inputs[id].errorLabel}</AppText>
        </View>
      );
    }
    return null;
  };



  const onHandleRetryBtn = () => {
    if (deviceUniqueId) {
      getProjectsInfo();
    }
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />
      {!isLoading ? (
        <>
          <ScrollView>
            <View style={styles.container}>
              <AppTextBold style={styles.pageTitle}>CONTACT</AppTextBold>
              <View
                style={[
                  styles.buttonConatainer,
                  { borderBottomWidth: isExpandedCallUs ? 0 : 1 },
                ]}>
                <TouchableWithoutFeedback onPress={updateLayoutCallUs}>
                  <View style={styles.button}>
                    <AppTextBold style={{ color: colors.expandText }}>
                      Call Us
                </AppTextBold>
                    <Image
                      source={
                        isExpandedCallUs
                          ? require('./../assets/images/up-icon-b.png')
                          : require('./../assets/images/down-icon-b.png')
                      }
                      style={styles.arrowIcon}
                    />
                  </View>
                </TouchableWithoutFeedback>

                <View
                  style={{ height: isExpandedCallUs ? null : 0, overflow: 'hidden' }}>
                  {callUsData && callUsData.length > 0 ? (
                    <>
                      {callUsData.map((item, index) => (
                        <View style={styles.numberContainer} key={index}>
                          <AppText style={{ color: colors.gray }}>
                            {item.title}
                          </AppText>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                            }}>
                            <Image
                              source={require('./../assets/menu/icon-contact-a.png')}
                              style={styles.callIcon}
                            />
                            <AppTextBold
                              style={{ fontSize:appFonts.largeBold }}
                              onPress={() => openDeviceCaller(item.number)}>
                              {item.number}
                            </AppTextBold>
                          </View>
                        </View>
                      ))}
                    </>
                  ) : null}
                </View>
              </View>

              <View style={styles.buttonConatainer}>
                <TouchableWithoutFeedback onPress={updateLayoutRequestCall}>
                  <View style={styles.button}>
                    <AppTextBold style={{ color: colors.expandText }}>
                      Request a Call Back
                </AppTextBold>
                    <Image
                      source={
                        isExpandedRequestCall
                          ? require('./../assets/images/up-icon-b.png')
                          : require('./../assets/images/down-icon-b.png')
                      }
                      style={styles.arrowIcon}
                    />
                  </View>
                </TouchableWithoutFeedback>

                {isExpandedRequestCall && (
                  <View>
                    {userId && userId != '' ? (
                      <View style={styles.userViewContainer}>
                        <AppText style={styles.userViewLabel}>
                          Mobile Number *
                    </AppText>
                        <TextInput
                          numberOfLines={1}
                          style={styles.input}
                          autoCapitalize="none"
                          autoCorrect={false}
                          keyboardType="default"
                          editable={false}
                          value={userData.mob_no ? userData.mob_no : ''}
                        />
                      </View>
                    ) : (
                        <View
                          style={[
                            styles.userViewContainer,
                            { flexDirection: 'row', alignItems: 'flex-end' },
                          ]}>
                          <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={() => setShowCountryModal(true)}>
                            <View style={styles.flagContainer}>
                              <Image
                                source={{ uri: selectedCountry.flag }}
                                style={styles.flagIcon}
                              />
                              <AppText style={styles.dropdownCountry}>
                                {selectedCountry.alpha3Code}
                              </AppText>
                              <Image
                                source={require('./../assets/images/arrow-back-Y.png')}
                                style={styles.downIcon}
                              />
                            </View>
                          </TouchableOpacity>
                          <View style={styles.editInputContainer}>
                            <AppText style={styles.userViewLabel}>
                              {`(${selectedCountry.callingCodes})`}&nbsp;Mobile Number
                        *
                      </AppText>
                            <TextInput
                              numberOfLines={1}
                              style={styles.input}
                              autoCapitalize="none"
                              autoCorrect={false}
                              keyboardType="phone-pad"
                              editable={true}
                              value={
                                inputs && inputs['mobNo'] && inputs['mobNo'].value
                                  ? inputs['mobNo'].value
                                  : ''
                              }
                              onChangeText={(value) => {
                                onInputChange('mobNo', value);
                              }}
                            />
                          </View>
                        </View>
                      )}
                    {renderError('mobNo')}

                    <View style={styles.btnContainer}>
                      <AppButton
                        title="REQUEST CALL BACK"
                        onPress={requestCallBack}
                      />
                    </View>
                  </View>
                )}
              </View>

              <Modal
                animationType="fade"
                transparent={true}
                visible={showCountryModal}
                onRequestClose={() => {
                  setShowCountryModal(false);
                }}>
                <CountrySelectModalScreen
                  onCancelPress={() => {
                    setShowCountryModal(false);
                  }}
                  searchData={''}
                  onSelectCountry={(selectedCountry) => {
                    onSelectCountry(selectedCountry);
                  }}
                  isShowCountryCode={true}
                />
              </Modal>
            </View>
          </ScrollView>
          <AppFooter activePage={3} isPostSales={false} />
        </>
      ) : null}
      <AppOverlayLoader isLoading={isLoading || isAPILoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 18,
    paddingTop: 15,
    marginLeft: '3%',
  },
  buttonConatainer: {
    width: '98%',
    marginTop: 10,
    marginBottom: 20,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  button: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 25,
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: '7%',
    textTransform: 'uppercase',
  },
  numberContainer: {
    paddingHorizontal: 25,
    paddingVertical: 18,
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGray,
  },
  arrowIcon: {
    width: 18,
    height: 18,
  },
  callIcon: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  dropdownCountry: {
    color: colors.gray2,
    marginLeft: 6,
    fontSize: appFonts.largeFontSize,
  },
  countryContainer: {
    borderColor: colors.borderGrey,
    borderBottomWidth: 2,
    height: height * 0.07,
    // width: '26%',
    marginRight: '5%',
    flexDirection: 'row',
    marginTop: 30,
    alignItems: 'center',
    paddingTop: '6%',
  },
  newUserMobile: {
    borderColor: colors.borderGrey,
    borderBottomWidth: 2,
    height: '100%',
    width: '26%',
    marginRight: 15,
    flexDirection: 'row',
  },
  flagIcon: {
    height: 25,
    width: 35,
    alignSelf: 'baseline',
  },
  countryName: {
    color: colors.gray2,
    marginLeft: 5,
  },
  downIcon: {
    height: 10,
    width: 10,
    marginLeft: 6,
  },
  numberModal: {
    position: 'absolute',
    height,
    width,
    backgroundColor: 'black',
    opacity: 0.7,
    zIndex: 27,
  },
  userViewContainer: { paddingHorizontal: 25, paddingTop: 30 },
  userViewLabel: { color: colors.gray2, fontSize: appFonts.largeFontSize },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: colors.borderGrey,
    fontSize:appFonts.largeBold,
    paddingVertical: 8,
    color: colors.jaguar,
  },
  btnContainer: {
    alignItems: 'center',
    paddingHorizontal: 25,
    paddingVertical: 25,
  },
  flagContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: colors.borderGrey,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  editInputContainer: { flex: 1, marginLeft: 18 },
  txtErrorContainer: {
    paddingHorizontal: 25,
    marginTop: 8,
    marginBottom: 10,
  },
  txtError: {
    color: colors.danger,
    fontSize: appFonts.largeFontSize,
  },
});

export default ContactScreen;
