import React, { useEffect } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
} from 'react-native';
import globlaStyles from '../styles/GlobleStyle';
import Orientation from 'react-native-orientation';
import appFonts from '../config/appFonts';
const ForgetPwd = (props) => {

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  
  return (
    <>
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={{height: '100%', width: '100%'}}>
          <KeyboardAvoidingView behavior={'position'}>
            {/* <ImageBackground source={require('../assets/images/login-bg.png')} style={{ width: '100%', height: '100%' }}> */}
            <View
              style={{
                height: '30%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image source={require('../assets/images/login-logo.png')} />
            </View>
            <View
              style={{
                height: '50%',
                marginLeft: '5%',
                marginRight: '5%',
                justifyContent: 'center',
              }}>
              <View style={{marginBottom: '20%'}}>
                <Text
                  style={{
                    color: 'blue',
                    fontWeight: 'bold',
                    fontSize: appFonts.xlargeFontSize,
                    textAlign: 'center',
                  }}>
                  Forget Password
                </Text>
              </View>

              <View style={{height: 60, justifyContent: 'center'}}>
                <TextInput
                  style={{
                    height: 40,
                    borderColor: 'gray',
                    borderBottomWidth: 2,
                    marginBottom: 20,
                  }}
                  placeholder="Email Id"
                  underlineColorAndroid="transparent"
                  // placeholderTextColor="#fff"
                />
              </View>
            </View>
            <View style={{height: '20%', marginLeft: '5%', marginRight: '5%'}}>
              <View
                style={{
                  height: 40,
                  backgroundColor: 'gray',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: 'bold', fontSize: appFonts.largeBold, color: '#fff'}}>
                  Send
                </Text>
              </View>
              <View
                style={{
                  height: 30,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text>
                  Need an account?{' '}
                  <Text onPress={() => props.navigation.navigate('Signup')}>
                    {' '}
                    Sign up
                  </Text>
                </Text>
              </View>
            </View>
            {/* </ImageBackground> */}
          </KeyboardAvoidingView>
        </View>
      </TouchableWithoutFeedback>
    </>
  );
};
export default ForgetPwd;
