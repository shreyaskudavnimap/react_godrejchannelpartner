import React, {useState, useEffect, Fragment} from 'react';
import {View, Image, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import {
  getUserJourneyCTA,
  getUserJourney,
} from '../store/actions/userAccountAction';
import {useSelector} from 'react-redux';

import AppText from '../components/ui/ AppText';

import appFonts from '../config/appFonts';
import colors from '../config/colors';
import appSnakBar from '../utility/appSnakBar';
import MyJourneyMenuItem from '../components/ui/MyJourneyMenuItem';
import MultiFileDownloadModal from './modals/MultiFileDownloadModal';
import Orientation from 'react-native-orientation';

const MyJourney = (props) => {
  const [journeyData, setJourneyData] = useState([]);
  const [modalData, setModalData] = useState({
    visible: false,
    title: '',
    content: [],
  });
  const [selectedMainSection, setSelectedMainSection] = useState({
    id: 0,
    shown: false,
  });
  // const {
  //   data: {userid: userID},
  // } = useSelector((state) => state.loginInfo.loginResponse);
  const loginvalue = useSelector((state) => state.loginInfo.loginResponse);
  // console.log("loginvalue", loginvalue);
  var userID =
    typeof loginvalue.data !== 'undefined' ? loginvalue.data.userid : '';

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
    props.setIsLoading(true);
    getUserJourney(userID, props.bookingId).then(
      (result) => {
        props.setIsLoading(false);
        console.log('>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<', result);
        setJourneyData(result);
      },
      (error) => {
        props.setIsLoading(false);
        appSnakBar.onShowSnakBar('Could not get journey details', 'LONG');
      },
    );
  }, [props.bookingId]);

  const ctaDetails = (ctaUrl, title, id) => {
    const finalUrl = ctaUrl.replace(
      'http://43.242.212.209/gpl-project/gpl-api/',
      '',
    );
    // props.setIsLoading(true);
    getUserJourneyCTA(finalUrl, userID, props.bookingId).then((result) => {
      if (result.length > 0) {
        switch (result[0].type) {
          case 'screen-redirect':
            props.setIsLoading(false);
            let pageName = '';
            let params = {};
            switch (result[0].screen_id) {
              case 'LoanManagementPage':
                pageName = 'LoanEnquiryScreen';
                params = {bookingId: props.bookingId};
                break;
              case 'ScheduleVisitPage':
                pageName = 'VisitScreen';
                const visitTypeList = [];
                if (id == '15') {
                  visitTypeList.push({
                    label: 'Joint Flat Inspection',
                    value: 502,
                  });
                } else if (id == '26') {
                  visitTypeList.push({label: 'Registration', value: 505});
                }
                params = {bookingId: props.bookingId};
                if (visitTypeList.length > 0) {
                  params.visitTypeList = visitTypeList;
                }
                break;
              case 'SavedSnagListPage':
                pageName = 'RaiseSnagScreen';
                params = {bookingId: props.bookingId};
                break;
              case 'ConstructionStatusPage':
                pageName = 'ConstructionStatus';
                params = {bookingId: props.bookingId};
                break;
              case 'StampDutyPage':
                pageName = 'StampDutyScreen';
                params = {pageData: result[0].data_capture[0].data};
                break;
              case 'RegistrationFaqPage':
                pageName = 'FAQ';
                params = {bookingId: props.bookingId};
                break;
            }

            if (pageName !== '') {
              props.navigation.navigate(pageName, params);
            }

            break;
          case 'download':
            props.handleFileDownload(result[0].url, false);
            break;
          case 'multiple-download':
            props.setIsLoading(false);
            setModalData({
              title: title,
              content: result[0].data,
              visible: true,
            });
            break;
        }
      }
    });
  };

  return (
    <>
      <View style={styles.container}>
        {journeyData.map((mainelm, index) =>
          parseInt(mainelm.status, 10) === 1 ? (
            <View style={styles.buttonContainer} key={index.toString()}>
              <TouchableWithoutFeedback
                disabled={mainelm.access === false}
                onPress={() => {
                  setSelectedMainSection({
                    id: mainelm.id,
                    shown:
                      selectedMainSection.id === mainelm.id
                        ? !selectedMainSection.shown
                        : true,
                  });
                }}>
                <View style={styles.button}>
                  <AppText
                    style={[
                      styles.buttonText,
                      mainelm.access === false
                        ? styles.buttonTextDisabled
                        : null,
                    ]}>
                    {mainelm.name}
                  </AppText>
                  <Image
                    source={
                      selectedMainSection.id === mainelm.id &&
                      selectedMainSection.shown === true
                        ? require('./../assets/images/down-icon-b.png')
                        : require('./../assets/images/arrow-back.png')
                    }
                    style={styles.icon}
                  />
                </View>
              </TouchableWithoutFeedback>
              {selectedMainSection.id === mainelm.id &&
                selectedMainSection.shown === true && (
                  <View>
                    {mainelm['sub-stages'].map((subelm, indexSub) =>
                      parseInt(subelm.status, 10) === 1 &&
                      subelm.access !== false &&
                      subelm['sub-stages'] &&
                      subelm['sub-stages'].length > 0 ? (
                        subelm['sub-stages'].map((sselm, ssIndex) =>
                          parseInt(sselm.status, 10) === 1 &&
                          sselm.access !== false &&
                          sselm['sub-stages'] &&
                          sselm['sub-stages'].length > 0 ? (
                            sselm['sub-stages'].map((tslm, tsIndex) => (
                              <MyJourneyMenuItem
                                key={tsIndex.toString() + ssIndex.toString()}
                                ctaDetails={ctaDetails}
                                item={tslm}
                              />
                            ))
                          ) : parseInt(sselm.status, 10) === 1 ? (
                            <MyJourneyMenuItem
                              key={indexSub.toString() + ssIndex.toString()}
                              ctaDetails={ctaDetails}
                              item={sselm}
                            />
                          ) : null,
                        )
                      ) : parseInt(subelm.status, 10) === 1 ? (
                        <MyJourneyMenuItem
                          key={indexSub.toString()}
                          ctaDetails={ctaDetails}
                          item={subelm}
                        />
                      ) : null,
                    )}
                  </View>
                )}
            </View>
          ) : null,
        )}
      </View>
      <MultiFileDownloadModal
        modalData={modalData}
        setModalData={setModalData}
        handleFileDownload={props.handleFileDownload}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  buttonContainer: {
    marginBottom: 20,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  button: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 25,
  },
  buttonText: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
    color: colors.secondary,
  },
  buttonTextDisabled: {color: colors.lightGray},
  icon: {
    width: 16,
    height: 16,
  },
  subIcon: {
    width: 14,
    height: 14,
  },
  text: {
    fontSize: appFonts.largeBold,
  },
});

export default MyJourney;
