import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  Platform,
  Linking,
} from 'react-native';
import colors from '../config/colors';
import AppText from '../components/ui/ AppText';
import appFonts from '../config/appFonts';
import AppButton from '../components/ui/AppButton';
import appConstant from '../utility/appConstant';
import Orientation from 'react-native-orientation';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const AppUpdateConfigScreen = () => {

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);

  const onHandleAppUpdate = () => {
    let openUrl = '';
    let openUrlDirect = '';

    if (Platform.OS === 'android') {
      openUrl = 'market://details?id=com.godrejproperties';
      openUrlDirect =
        'https://play.google.com/store/apps/details?id=com.godrejproperties';
    } else if (Platform.OS === 'ios') {
      openUrl = 'itms-apps://itunes.apple.com/app/id1471652792';
      openUrlDirect =
        'https://apps.apple.com/in/app/godrej-properties-limited/id1471652792';
    }

    Linking.canOpenURL(openUrl).then(
      (supported) => {
        if (supported) {
          Linking.openURL(openUrl);
        } else {
          Linking.openURL(openUrlDirect);
        }
      },
      (err) => console.log('Store app open error: ', err),
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.imgContainer}>
        <Image source={require('./../assets/images/login-logo.png')} />
      </View>

      <View style={styles.infoContainer}>
        <AppText style={styles.infoLabel}>
          {appConstant.appMessage.APP_UPDATE_TITLE}
        </AppText>
        <AppText style={styles.infoTxt}>
          {appConstant.appMessage.APP_UPDATE_MSG}
        </AppText>
      </View>

      <View style={styles.btnContainer}>
        <AppButton
          title="Update"
          color="primary"
          textColor="secondary"
          onPress={onHandleAppUpdate}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.secondary,
    padding: 25,
  },
  imgContainer: {
    height: windowHeight / 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoContainer: {height: windowHeight / 2, alignItems: 'center'},
  infoLabel: {
    color: colors.primary,
    fontFamily: appFonts.SourceSansProSemiBold,
    fontSize: appFonts.xxxlargeFontSize,
    marginBottom: 25,
  },
  infoTxt: {color: colors.primary, fontSize: appFonts.largeBold, lineHeight: 25},
  btnContainer: {height: windowHeight / 4, width: '100%', padding: 25},
});

export default AppUpdateConfigScreen;
