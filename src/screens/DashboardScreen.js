import React, {
  useState,
  useEffect,
  useCallback,
  useRef,
  createRef,
} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  Modal,
  TouchableOpacity,
  Image,
  Text,
  Platform,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import {EventRegister} from 'react-native-event-listeners';

import * as dashboardAction from './../store/actions/dashboardAction';
import {USER_LOGIN, clearRMData} from '../store/actions/userLoginAction';
import {clearBookingEnquiryData} from '../store/actions/bookingEnquiryAction';
import * as badgeCountAction from './../store/actions/badgeCountAction';
import * as menuTypeAction from './../store/actions/menuTypeAction';
import Carousel, {Pagination} from 'react-native-snap-carousel';

import Screen from '../components/Screen';
import appFonts from '../config/appFonts';
import AppText from '../components/ui/ AppText';
import colors from '../config/colors';

import AppProjectDetailsMenuItem from '../components/ui/AppProjectDetailsMenuItem';
import AppProjectDetailsViewItem from '../components/ui/AppProjectDetailsViewItem';
import AppProjectDetailsViewImgItemWithCache from '../components/ui/AppProjectDetailsViewImgItemWithCache';
import AsyncStotage from '@react-native-async-storage/async-storage';

import GalleryModalScreen from './modals/GalleryModalScreen';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import appSnakBar from '../utility/appSnakBar';
import AppFooter from '../components/ui/AppFooter';
import {IMAGE_PLACEHOLDER} from '../utility/appConstant';
import {CommonActions} from '@react-navigation/native';
import PostLoginTermsModalScreen from './modals/PostLoginTermsModalScreen';
import apiDashboard from '../api/apiDashboard';
import AsyncStorage from '@react-native-async-storage/async-storage';
import GetLocation from 'react-native-get-location';
import appUserAuthorization from '../utility/appUserAuthorization';
import Orientation from 'react-native-orientation';

const {width: windowWidth, height: windowHeight} = Dimensions.get('window');

import apiLogout from './../api/apiLogin';
import VideoModalScreen from './modals/VideoModalScreen';
import { VideoThumbnailPlayer } from '../components/VideoThumbnail';

const renderProject = (
  item,
  index,
  setModalVisible,
  onHandleVideoModal,
  onHandleImageModal,
  setMediaIndex,
  setIsVideoLoading,
  isVideoLoading,
  activeIndex,
  videoRefs,
) => {
  return (
    <View key={index.toString()} style={styles.container}>
      <AppText style={styles.projectTitle} numberOfLines={1}>
        {item.property_name + '/' + item.inv_flat_code}
      </AppText>
      <View style={styles.viewContainer}>
        <View
          style={styles.videoContainer}
          onPress={() => {
            setModalVisible(true);
          }}>
          {typeof item.video !== 'undefined' && item.video ? (
            <TouchableOpacity
              onPress={() => {
                setMediaIndex(0);
                onHandleVideoModal();
              }}>
              <VideoThumbnailPlayer url={item.video.thumb_url} />
              {/*<VideoPlayer*/}
              {/*  video={{*/}
              {/*    uri: item.video.thumb_url,*/}
              {/*  }}*/}
              {/*  ref={videoRefs.current[index]}*/}
              {/*  style={{height: '100%', width: '100%'}}*/}
              {/*  thumbnail={{*/}
              {/*    uri: 'https://i.picsum.photos/id/866/1600/900.jpg',*/}
              {/*  }}*/}
              {/*  muted={true}*/}
              {/*  disableSeek={true}*/}
              {/*  autoplay={true}*/}
              {/*  loop*/}
              {/*  hideControlsOnStart={true}*/}
              {/*  resizeMode="stretch"*/}
              {/*  disableControlsAutoHide={true}*/}
              {/*  customStyles={{controls: {opacity: 0}}}*/}
              {/*  onStart={() => {*/}
              {/*    console.log('ex');*/}
              {/*  }}*/}
              {/*  onLoadStart={() => {*/}
              {/*    setIsVideoLoading(true);*/}
              {/*  }}*/}
              {/*  onLoad={() => {*/}
              {/*    setIsVideoLoading(false);*/}
              {/*  }}*/}
              {/*  onEnd={(event) => {*/}
              {/*    if (activeIndex === index && videoRefs.current[index]) {*/}
              {/*      setTimeout(() => {*/}
              {/*        videoRefs.current[index].current.seek(0);*/}
              {/*        setTimeout(() => {*/}
              {/*          videoRefs.current[index].current.resume();*/}
              {/*        }, 200);*/}
              {/*      }, 200);*/}
              {/*    }*/}
              {/*  }}*/}
              {/*/>*/}

              {/*{isVideoLoading ? (*/}
              {/*  <View*/}
              {/*    style={{*/}
              {/*      height: '100%',*/}
              {/*      position: 'absolute',*/}
              {/*      left: 0,*/}
              {/*      right: 0,*/}
              {/*      top: 0,*/}
              {/*      bottom: 0,*/}
              {/*      backgroundColor: '#ffffff',*/}
              {/*      justifyContent: 'center',*/}
              {/*      alignItems: 'center',*/}
              {/*    }}>*/}
              {/*    <ActivityIndicator*/}
              {/*      color={colors.jaguar}*/}
              {/*      size="small"*/}
              {/*      style={{*/}
              {/*        flex: 1,*/}
              {/*        justifyContent: 'center',*/}
              {/*        alignItems: 'center',*/}
              {/*      }}*/}
              {/*    />*/}
              {/*  </View>*/}
              {/*) : null}*/}
            </TouchableOpacity>
          ) : (
            <AppProjectDetailsViewImgItemWithCache
              containerStyle={styles.projectMainImage}
              imgSource={item.property_image}
              onPress={() => {
                setMediaIndex(0);
                onHandleImageModal();
              }}
            />
          )}
        </View>

        <View style={styles.imgContainer}>
          <View style={styles.imgItem}>
            <AppProjectDetailsViewImgItemWithCache
              containerStyle={styles.imgItemBtn}
              imgSource={
                item.galleryImages &&
                item.galleryImages[0] &&
                item.galleryImages[0].thumbUrl
                  ? item.galleryImages[0].thumbUrl
                  : IMAGE_PLACEHOLDER
              }
              onPress={() => {
                if (
                  item.galleryImages &&
                  item.galleryImages[0] &&
                  item.galleryImages[0].thumbUrl
                ) {
                  if (item.video) {
                    setMediaIndex(0);
                  } else {
                    setMediaIndex(1);
                  }
                  onHandleImageModal();
                }
              }}
            />

            <AppProjectDetailsViewImgItemWithCache
              containerStyle={styles.imgItemBtn}
              imgSource={
                item.galleryImages &&
                item.galleryImages[1] &&
                item.galleryImages[1].thumbUrl
                  ? item.galleryImages[1].thumbUrl
                  : IMAGE_PLACEHOLDER
              }
              onPress={() => {
                if (
                  item.galleryImages &&
                  item.galleryImages[1] &&
                  item.galleryImages[1].thumbUrl
                ) {
                  if (item.video) {
                    setMediaIndex(1);
                  } else {
                    setMediaIndex(2);
                  }
                  onHandleImageModal();
                }
              }}
            />
          </View>

          <View style={styles.imgItem}>
            <AppProjectDetailsViewImgItemWithCache
              containerStyle={styles.imgItemBtn}
              imgSource={
                item.galleryImages &&
                item.galleryImages[2] &&
                item.galleryImages[2].thumbUrl
                  ? item.galleryImages[2].thumbUrl
                  : IMAGE_PLACEHOLDER
              }
              onPress={() => {
                if (
                  item.galleryImages &&
                  item.galleryImages[2] &&
                  item.galleryImages[2].thumbUrl
                ) {
                  if (item.video) {
                    setMediaIndex(2);
                  } else {
                    setMediaIndex(3);
                  }
                  onHandleImageModal();
                }
              }}
            />

            <AppProjectDetailsViewImgItemWithCache
              containerStyle={styles.imgItemBtn}
              imgSource={
                item.galleryImages &&
                item.galleryImages[3] &&
                item.galleryImages[3].thumbUrl
                  ? item.galleryImages[3].thumbUrl
                  : IMAGE_PLACEHOLDER
              }
              onPress={() => {
                if (
                  item.galleryImages &&
                  item.galleryImages[3] &&
                  item.galleryImages[3].thumbUrl
                ) {
                  if (item.video) {
                    setMediaIndex(3);
                  } else {
                    setMediaIndex(4);
                  }
                  onHandleImageModal();
                }
              }}
            />
          </View>

          <View style={styles.imgItem}>
            {/* <AppProjectDetailsViewItem
              containerStyle={styles.imgItemBtn}
              imgSource={require('./../assets/images/3d-view-icon-b.png')}
              title="3D View"
              onPress={() => {
                appSnakBar.onShowSnakBar(
                  'Feature is unavailable and will be accessible shortly',
                );
              }}
            /> */}
            <AppProjectDetailsViewImgItemWithCache
              containerStyle={styles.imgItemBtn}
              imgSource={
                item.galleryImages &&
                item.galleryImages[4] &&
                item.galleryImages[4].thumbUrl
                  ? item.galleryImages[4].thumbUrl
                  : IMAGE_PLACEHOLDER
              }
              onPress={() => {
                if (
                  item.galleryImages &&
                  item.galleryImages[4] &&
                  item.galleryImages[4].thumbUrl
                ) {
                  if (item.video) {
                    setMediaIndex(4);
                  } else {
                    setMediaIndex(5);
                  }
                  onHandleImageModal();
                }
              }}
            />
            {item.galleryImages && item.galleryImages.length > 5 ? (
              <AppProjectDetailsViewItem
                containerStyle={styles.imgItemBtn}
                imgSource={require('./../assets/images/photos-icon.png')}
                title={
                  item.galleryImages && item.galleryImages.length > 99
                    ? '+ 99 Photos'
                    : `+${item.galleryImages.length - 5} Photos`
                }
                onPress={() => {
                  if (item.galleryImages && item.galleryImages.length > 5) {
                    if (item.video) {
                      setMediaIndex(5);
                    } else {
                      setMediaIndex(6);
                    }
                    onHandleImageModal();
                  }
                }}
              />
            ) : null}
          </View>
        </View>
      </View>
    </View>
  );
};

const ProjectPagination = ({index, sliderLength}) => (
  <View style={styles.alignCenter}>
    <Pagination
      dotsLength={sliderLength}
      activeDotIndex={index}
      inactiveDotOpacity={0.4}
      inactiveDotScale={0.6}
    />
  </View>
);

const loadData = (dashboardData, loginvalue, setIsLoading, dispatch) => {
  let shouldUpdate = false;
  let date = new Date();
  if (date.getDay() !== dashboardData.saveDate) {
    shouldUpdate = true;
  } else if (date.getHours() > 5 && dashboardData.saveTime < 6) {
    shouldUpdate = true;
  }

  if (
    (!dashboardData.rmData || dashboardData.rmData.length < 1) &&
    shouldUpdate === true
  ) {
    AsyncStotage.getItem('rmData', async (error, result) => {
      let dataExist = true;
      if (error || !result) {
        dataExist = false;
      } else {
        let data = JSON.parse(result);
        if (
          date.getDay() === data.saveDate &&
          !(date.getHours() > 5 && data.saveTime < 6)
        ) {
          dispatch(dashboardAction.getRMInfoOffline(data.propertyData));
        } else if (date.getHours() > 5 && dashboardData.saveTime < 6) {
          dataExist = false;
        }
      }
      if (dataExist === false) {
        setIsLoading(true);
        // Alert.alert(
        //   'Info',
        //   'User Id: ' + loginvalue.data.userid + ' // ' + loginvalue.token,
        //   [{text: 'OK', style: 'cancel'}],
        // );
        try {
          await dispatch(dashboardAction.getRMInfo(loginvalue.data.userid));
          setIsLoading(false);
        } catch (err) {
          appSnakBar.onShowSnakBar(err.message, 'LONG');
          // navigation.goBack();
          setIsLoading(false);
        }
      }
    });
  } else if (shouldUpdate === true) {
    // OfflineImageStore.clearStore(() => {
    dispatch(dashboardAction.getRMInfo(loginvalue.data.userid)).then(() => {
      setIsLoading(false);
    });
    // });
  }
};

const snapItem = (
  index,
  setActiveIndex,
  dashboardData,
  dispatch,
  userId,
  deviceUniqueId,
  setLoadCount,
) => {
  mediaData = [];
  setActiveIndex(index);
  if (!dashboardData.rmData[index].video) {
    dispatch(
      dashboardAction.setRMVideo(
        userId,
        deviceUniqueId,
        dashboardData.rmData[index].property_id,
        index,
        dashboardData.rmData,
        setLoadCount,
      ),
    );
  }
  // else {
  //   let foundMatch = mediaData.find(
  //     (elm) =>
  //       elm.field_gallery_image_url === dashboardData.rmData[index].video.url,
  //   );
  //   if (foundMatch === undefined) {
  //     mediaData.push({
  //       field_gallery_image_url: dashboardData.rmData[index].video.url,
  //       field_gallery_thumb_image_url:
  //         dashboardData.rmData[index].video.thumb_url,
  //       field_media_type: 'video',
  //     });
  //   }
  // }

  if (
    !mediaData.find(
      (elm) =>
        elm.field_gallery_image_url ===
        dashboardData.rmData[index].property_image,
    ) &&
    !dashboardData.rmData[index].video
  ) {
    mediaData.push({
      field_gallery_image_url: dashboardData.rmData[index].property_image,
      field_gallery_thumb_image_url: dashboardData.rmData[index].property_image,
      field_media_type: 'image',
    });
  }

  if (!dashboardData.rmData[index].galleryImages) {
    dispatch(
      dashboardAction.setRMImages(
        dashboardData.rmData[index].property_id,
        index,
        dashboardData.rmData,
        setLoadCount,
      ),
    );
  } else {
    for (let item of dashboardData.rmData[index].galleryImages) {
      let foundMatch = mediaData.findIndex(
        (elm) => elm.field_gallery_image_url === item.url,
      );
      if (foundMatch < 0) {
        mediaData.push({
          field_gallery_image_url: item.url,
          field_gallery_thumb_image_url: item.thumbUrl,
          field_media_type: 'image',
        });
      }
    }
  }
};

let carousel;
let mediaData = [];
let userId = 0;

const DashboardScreen = ({navigation}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [onlyVideos, setOnlyVideos] = useState(false);
  const [activeIndex, setActiveIndex] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [isAuthLoading, setIsAuthLoading] = useState(false);
  const [currentMediaData, setCurrentMediaData] = useState([]);
  const [mediaIndex, setMediaIndex] = useState(0);
  const [isVideoLoading, setIsVideoLoading] = useState(true);
  const [loadCount, setLoadCount] = useState(0);
  const videoRefs = useRef([]);

  const {
    dashboardData,
    loginInfo: {loginResponse: loginvalue},
    deviceInfo: {deviceUniqueId},
  } = useSelector((state) => state);
  const dispatch = useDispatch();

  const badgeData = useSelector((state) => state.badgeCount);
  const notificationCount =
    badgeData && badgeData.notificationCount ? badgeData.notificationCount : '';

  useEffect(() => {
    const eventListenerRedirection = EventRegister.addEventListener(
      'notificationRedirectionEventPost',
      (data) => {
        // console.log('notificationRedirectionEventPost:: ', data);
        onPageRedirection(data);
      },
    );

    return () => {
      EventRegister.removeEventListener(eventListenerRedirection);
    };
  }, [dispatch, userId]);

  const onPageRedirection = (eventData) => {
    if (eventData && eventData.title) {
      if (eventData.title == 'birthday') {
        navigation.navigate('Profile');
      } else if (eventData.title == 'anniversary') {
        navigation.navigate('NotificationsScreen');
      } else if (eventData.title == 'support') {
        navigation.navigate('ReachUs');
      } else if (
        eventData.title == 'payment_realization_task' ||
        eventData.title == 'payment_realization'
      ) {
        if (eventData.booking_id) {
          navigation.navigate('MyAccountScreen', {
            bookingId: eventData.booking_id,
          });
        } else {
          navigation.navigate('NotificationsScreen');
        }
      } else if (eventData.title == 'documents') {
        navigation.navigate('DocumentScreen');
      } else if (
        eventData.title == 'booking_confirmation_data' ||
        eventData.title == 'eoi_booking_confirmation_data'
      ) {
        if (eventData.booking_id && eventData.user_id) {
          navigation.navigate('BookingDetailsScreen', {
            bookingId: eventData.booking_id && eventData.user_id,
            userId: eventData.user_id,
          });
        } else {
          navigation.navigate('NotificationsScreen');
        }
      } else if (eventData.title == 'my_task') {
        navigation.navigate('NotificationsScreen');
      } else if (eventData.title == 'scheduled_visits') {
        navigation.navigate('VisitScreen');
      } else if (eventData.title == 'bank_details_view') {
        if (eventData.booking_id) {
          navigation.navigate('LoanEnquiryScreen', {
            bookingId: eventData.booking_id,
            activeTab: 'LoanDetails',
          });
        } else {
          navigation.navigate('NotificationsScreen');
        }
      } else {
        navigation.navigate('NotificationsScreen');
      }
    } else {
      navigation.navigate('NotificationsScreen');
    }
  };

  useEffect(() => {
    Orientation.lockToPortrait();
    dispatch(
      menuTypeAction.setMenuData({
        menuType: 'postsale',
      }),
    );
    videoRefs.current = [0, 0, 0, 0, 0, 0, 0, 0].map(
      (ref, index) => (videoRefs.current[index] = createRef()),
    );
  }, []);
  const [termsModalVisible, setTermsModalVisible] = useState(false);
  const [termsText, setTermsText] = useState('');
  const loginData = useSelector((state) => state.loginInfo.loginResponse);
  const acceptPostLoginTerms =
    loginData && loginData.data && loginData.data.accept_post_login_terms
      ? loginData.data.accept_post_login_terms
      : '0';

  const getPostLoginTerms = async () => {
    setIsAuthLoading(true);
    try {
      const result = await apiDashboard.getPostLoginTerms();
      const resultData = result.data;
      let termsTxt =
        typeof resultData[0] !== 'undefined' ? resultData[0].body : '';

      if (termsTxt) {
        setTermsText(termsTxt);
        setTermsModalVisible(true);
      }
      setIsAuthLoading(false);
    } catch (error) {
      setIsAuthLoading(false);
      console.log(error);
    }
  };

  const handleAcceptTerms = () => {
    setTermsModalVisible(false);
    postAuthorization('Post Login Terms');
  };

  const postAuthorization = (moduleName) => {
    const storedUserData = loginData.data;
    const requestData = {
      deviceId: deviceUniqueId,
      devicePlatform: Platform.OS,
      userId: userId,
      customerName:
        storedUserData && storedUserData.first_name && storedUserData.last_name
          ? `${storedUserData.first_name} ${storedUserData.last_name}`
          : '',
      mobileNo:
        storedUserData && storedUserData.mob_no ? storedUserData.mob_no : '',
      emailId:
        storedUserData && storedUserData.email ? storedUserData.email : '',
      moduleName: moduleName,
    };
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
      .then(({latitude, longitude}) => {
        appUserAuthorization.validateUserAuthorization({
          ...requestData,
          latiTude: latitude,
          longiTude: longitude,
        });
      })
      .catch(() => {
        appUserAuthorization.validateUserAuthorization(requestData);
      });
  };

  const handleCancelTerms = async () => {
    // setTermsModalVisible(false);
    if (loginData.isLogin) {
      try {
        userLogout();

        await AsyncStorage.removeItem('sessionId');
        await AsyncStorage.removeItem('sessionName');
        await AsyncStorage.removeItem('token');
        await AsyncStorage.removeItem('data');
        await AsyncStorage.removeItem('status');
        await AsyncStorage.removeItem('isLogin');
        await AsyncStorage.removeItem('rmData');
        dispatch(clearRMData());
        dispatch(clearBookingEnquiryData());
        dispatch(
          badgeCountAction.setBadgeData({
            lastAccessed: '',
            wishlistCount: '',
            notificationCount: '',
            totalCount: '',
          }),
        );
        // OfflineImageStore.clearStore(() => {});
      } catch (exception) {
        console.log(exception);
        //return false;
      }
      dispatch({type: USER_LOGIN, data: {isLogin: false}});
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Login'}],
        }),
      );
    }
  };

  const userLogout = () => {
    if (loginvalue && loginvalue.isLogin && loginvalue.data) {
      const userId = loginvalue.data
        ? loginvalue.data.userId
          ? loginvalue.data.userId
          : loginvalue.data.userid
        : loginvalue.userid;

      const requestParms = {
        user_id: userId,
        device_id: deviceUniqueId,
      };
      apiLogout
        .apiLogout(requestParms)
        .then((data) => {
          console.log('Logout API Success: ', data);
        })
        .catch((errorL) => {
          console.log('Logout API Error: ', errorL);
        });
    }
  };

  useEffect(() => {
    if (acceptPostLoginTerms == '0') {
      getPostLoginTerms();
    }

    if (!loginvalue.isLogin || !loginvalue.data) {
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Login'}],
        }),
      );
    } else {
      userId = loginvalue.data.userid;
    }
  }, []);

  useEffect(() => {
    mediaData = [];
    getRMInfo();
    // OfflineImageStore.restore({name: 'Dashboard_image_gall'}, () => {});
  }, [dispatch]);

  useEffect(() => {
    if (dashboardData.rmData.length > 0) {
      let date = new Date();
      let propDt = {
        saveDate: date.getDay(),
        saveTime: date.getHours(),
        propertyData: dashboardData.rmData,
      };
      AsyncStotage.setItem('rmData', JSON.stringify(propDt), (error) => {
        if (error) {
          appSnakBar.onShowSnakBar('Cannot save offline', 'LONG');
        }
      });

      if (!dashboardData.rmData[0].video) {
        dispatch(
          dashboardAction.setRMVideo(
            userId,
            deviceUniqueId,
            dashboardData.rmData[0].property_id,
            0,
            dashboardData.rmData,
            setLoadCount,
          ),
        );
      }
      // else {
      //   let foundMatch = mediaData.find(
      //     (elm) =>
      //       elm.field_gallery_image_url === dashboardData.rmData[0].video.url,
      //   );
      //   if (foundMatch === undefined) {
      //     mediaData.push({
      //       field_gallery_image_url: dashboardData.rmData[0].video.url,
      //       field_gallery_thumb_image_url:
      //         dashboardData.rmData[0].video.thumb_url,
      //       field_media_type: 'video',
      //     });
      //   }
      // }

      if (
        !mediaData.find(
          (elm) =>
            elm.field_gallery_image_url ===
            dashboardData.rmData[0].property_image,
        ) &&
        !dashboardData.rmData[0].video
      ) {
        mediaData.push({
          field_gallery_image_url: dashboardData.rmData[0].property_image,
          field_gallery_thumb_image_url: dashboardData.rmData[0].property_image,
          field_media_type: 'image',
        });
      }

      if (!dashboardData.rmData[0].galleryImages) {
        dispatch(
          dashboardAction.setRMImages(
            dashboardData.rmData[0].property_id,
            0,
            dashboardData.rmData,
            setLoadCount,
          ),
        );
      } else {
        for (let item of dashboardData.rmData[0].galleryImages) {
          let foundMatch = mediaData.findIndex(
            (elm) => elm.field_gallery_image_url === item.url,
          );
          if (foundMatch < 0) {
            mediaData.push({
              field_gallery_image_url: item.url,
              field_gallery_thumb_image_url: item.thumbUrl,
              field_media_type: 'image',
            });
          }
        }
      }
    }
  }, [dashboardData.rmData, loadCount]);

  const getRMInfo = useCallback(() => {
    if (userId) {
      loadData(dashboardData, loginvalue, setIsLoading, dispatch);
    } else {
      navigation.goBack();
    }
  }, [dispatch, setIsLoading, loginvalue]);

  const onHandleRetryBtn = () => {
    // appSnakBar.onShowSnakBar('Retry', 'LONG');
  };
  const onHandleVideoModal = () => {
    setCurrentMediaData(mediaData);
    setOnlyVideos(true);
    setModalVisible(true);
  };
  const onHandleImageModal = () => {
    setCurrentMediaData(mediaData);
    setModalVisible(true);
    setOnlyVideos(false);
  };

  const goToConstruction = (bookingId) => {
    navigation.navigate('ConstructionStatus', {bookingId});
  };
  const goToMyAccount = (bookingId) => {
    navigation.navigate('MyAccountScreen', {bookingId});
  };

  const goToHomeLoan = (bookingId) => {
    navigation.navigate('LoanEnquiryScreen', {bookingId});
  };

  const goToDocuments = (bookingId) => {
    navigation.navigate('DocumentScreen', {bookingId});
  };

  return loginvalue.isLogin ? (
    <Screen onRetry={onHandleRetryBtn}>
      <View style={styles.headerContainer}>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => navigation.navigate('Profile')}
          style={styles.profileImagecontainer}>
          <Image
            source={
              loginvalue.data.user_image
                ? {uri: loginvalue.data.user_image}
                : require('./../assets/images/no-user-img.png')
            }
            style={styles.photo}
          />
        </TouchableOpacity>
        <View
          style={{
            marginTop: '7%',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => navigation.navigate('Profile')}
            style={{width: windowWidth * 0.6}}>
            <AppText>
              Welcome{' '}
              {loginvalue.data.first_name + ' ' + loginvalue.data.last_name}
            </AppText>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => {
              navigation.navigate('NotificationsScreen');
              dashboardData.rmData[carousel.currentIndex].booking_id;
            }}>
            <View
              style={{
                paddingHorizontal: '4.5%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={require('./../assets/images/bell-icon.png')}
                style={{width: 20, height: 20}}
              />
              {notificationCount && notificationCount !== '' ? (
                <View style={styles.number}>
                  <AppText style={styles.txtNumber}>
                    {notificationCount}
                  </AppText>
                </View>
              ) : null}
            </View>
          </TouchableOpacity>
        </View>
      </View>

      <ScrollView>
        <Carousel
          layout={'default'}
          ref={(ref) => (carousel = ref)}
          data={dashboardData.rmData}
          sliderWidth={windowWidth}
          itemWidth={windowWidth}
          onSnapToItem={(index) => {
            snapItem(
              index,
              setActiveIndex,
              dashboardData,
              dispatch,
              userId,
              deviceUniqueId,
              setLoadCount,
            );
          }}
          renderItem={({item, index}) =>
            renderProject(
              item,
              index,
              setModalVisible,
              onHandleVideoModal,
              onHandleImageModal,
              setMediaIndex,
              setIsVideoLoading,
              isVideoLoading,
              activeIndex,
              videoRefs,
            )
          }
        />
        <ProjectPagination
          index={activeIndex}
          sliderLength={dashboardData.rmData ? dashboardData.rmData.length : 0}
        />
        <View style={styles.propertyAction}>
          <AppProjectDetailsMenuItem
            title={'Construction Status'}
            onPress={() => {
              goToConstruction(
                dashboardData.rmData[carousel.currentIndex]
                  ? dashboardData.rmData[carousel.currentIndex].booking_id
                  : null,
              );
            }}
          />
          <AppProjectDetailsMenuItem
            title={'My Accounts'}
            onPress={() => {
              goToMyAccount(
                dashboardData.rmData[carousel.currentIndex].booking_id,
              );
            }}
          />
          <AppProjectDetailsMenuItem
            title={'Home Loan'}
            onPress={() => {
              goToHomeLoan(
                dashboardData.rmData[carousel.currentIndex]
                  ? dashboardData.rmData[carousel.currentIndex].booking_id
                  : null,
              );
            }}
          />

          <AppProjectDetailsMenuItem
            title={'My Document'}
            onPress={() => {
              goToDocuments(
                dashboardData.rmData[carousel.currentIndex].booking_id,
              );
            }}
          />
        </View>
      </ScrollView>

      <AppFooter activePage={1} isPostSales={true} />

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        supportedOrientations={['portrait', 'landscape']}
        onRequestClose={() => {
          Orientation.lockToPortrait();
          setModalVisible(!modalVisible);
        }}>
        {onlyVideos ? (
          <VideoModalScreen
            onPress={() => {
              Orientation.lockToPortrait();
              setModalVisible(!modalVisible);
            }}
            projectName={
              dashboardData.rmData[activeIndex] &&
              dashboardData.rmData[activeIndex].property_name
                ? dashboardData.rmData[activeIndex].property_name
                : ''
            }
            videoUri={
              dashboardData.rmData[activeIndex] &&
              dashboardData.rmData[activeIndex].video
                ? dashboardData.rmData[activeIndex].video.url
                : ''
            }
            setIsLoading={setIsLoading}
          />
        ) : (
          <GalleryModalScreen
            onPress={() => {
              Orientation.lockToPortrait();
              setModalVisible(!modalVisible);
            }}
            galleryData={currentMediaData}
            imgIndex={mediaIndex}
            videos={onlyVideos}
          />
        )}
      </Modal>

      <Modal
        animationType="fade"
        transparent={true}
        visible={termsModalVisible}
        onRequestClose={() => {
          // setTermsModalVisible(false);
        }}>
        <PostLoginTermsModalScreen
          onCancel={() => {
            handleCancelTerms();
          }}
          onContinue={() => {
            handleAcceptTerms();
          }}
          termsData={termsText}
        />
      </Modal>

      <AppOverlayLoader isLoading={isLoading || isAuthLoading} />
    </Screen>
  ) : (
    <Text>Not logged in</Text>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 18,
    paddingVertical: 10,
  },
  projectTitle: {
    fontSize: appFonts.xlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: 5,
    textTransform: 'uppercase',
  },
  photo: {
    width: windowWidth * 0.15,
    height: windowWidth * 0.15,
    borderRadius: 50,
    marginTop: 20,
    borderWidth: 1,
  },
  viewContainer: {
    marginTop: 25,
    height: windowHeight * 0.32,
    flexDirection: 'row',
  },
  videoContainer: {
    flex: 1,
    marginRight: 8,
  },
  imgContainer: {
    flex: 1,
    justifyContent: 'space-between',
  },
  imgItem: {
    height: '32%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  imgItemBtn: {
    width: '48%',
    backgroundColor: colors.secondary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  propertyAction: {
    paddingHorizontal: 18,
  },
  homeLoanBlock: {},
  homeLoanBlockList: {
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 25,
  },
  projectMainImage: {height: '100%', width: '100%', resizeMode: 'cover'},
  alignCenter: {alignItems: 'center'},
  headerContainer: {
    height: '12%',
    flexDirection: 'row',
    marginLeft: '5%',
    marginBottom: '5%',
    alignItems: 'center',
  },
  txtNumber: {
    color: colors.primary,
    fontSize: appFonts.smallFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  number: {
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.secondaryDark,
    borderRadius: 10,
    zIndex: 999,
    position: 'absolute',
    top: -10,
    bottom: 0,
    right: -2,
  },
  profileImagecontainer: {marginRight: 10},
});

export default DashboardScreen;
