import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  Dimensions,
  Image,
  Modal,
  FlatList,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';

import {validationDictionary} from '../utility/validation/dictionary';
import validatejs from 'validate.js';

import AppButton from '../components/ui/AppButton';
import AppFooter from '../components/ui/AppFooter';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import Screen from '../components/Screen';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import DateTimePicker from '@react-native-community/datetimepicker';
import CountryModal from '../components/CountryModal';
import apiScheduleVisit from './../api/apiScheduleVisit';
import moment from 'moment';
import {useSelector, useDispatch} from 'react-redux';
import appSnakBar from '../utility/appSnakBar';
import ShowLoader from '../components/ui/ShowLoader';
import {SvgUri} from 'react-native-svg';
import appConstant from '../utility/appConstant';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import {Avatar} from 'react-native-paper';
import {useBackHandler} from '@react-native-community/hooks';
import appCountry from '../utility/appCountry';
import CountrySelectModalScreen from './modals/CountrySelectModalScreen';
import { StackActions } from '@react-navigation/native';
import Orientation from 'react-native-orientation';

const {height, width} = Dimensions.get('window');

const VisitSchedule = ({navigation, route}) => {
  const [nameClicked, isNameClicked] = useState(false);
  const [phoneNoClicked, isPhoneNoClicked] = useState(false);
  const [emailClicked, isEmailClicked] = useState(false);
  const [countryClicked, isCountryClicked] = useState(false);
  const [selectModalVisible, setSelectModalVisible] = useState(false);
  const [selectedPropertyData, setSelectedPropertyData] = useState(null);
  const [selectedProperty, setSelectedProperty] = useState('Choose a property');
  const [selectedPropertyId, setSelectedPropertyId] = useState(null);
  const [selectedPropertyImage, setSelectedPropertyImage] = useState(null);

  const [visitorName, setVisitorName] = useState(null);
  const [visitorMobileNo, setVisitorMobileNo] = useState(null);
  const [isValidMobile, setIsValidMobile] = useState(false);
  const [mobileMaxLength, setMobileMaxLength] = useState(undefined);
  const [visitorEmail, setVisitorEmail] = useState(null);
  const [isValidEmail, setIsValidEmail] = useState(false);
  const [visitorIsCountryIndia, setVisitorIsCountryIndia] = useState(true);
  const [isCountrySelected, setCountry] = useState(false);
  const [visitorCountryName, setVisitorCountryName] = useState('IND');
  const [visitorCountryCode, setVisitorCountryCode] = useState('+91');
  const [visitorCountryFlag, setVisitorCountryFlag] = useState(
    'https://restcountries.eu/data/ind.svg',
  );
  const [visitingDate, setVisitingDate] = useState(
    moment(new Date()).format('DD MMM YYYY'),
  );
  const [visitingTime, setVisitingTime] = useState(null);
  const [timeError, setTimeError] = useState(null);
  const [dateSelected, setDateSelected] = useState(new Date(1598051730000));
  const [timeSelected, setTimeSelected] = useState(new Date(1598051730000));
  const [mode, setMode] = useState('date');
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [showTimePicker, setShowTimePicker] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [isSubmitLoading, setSubmitIsLoading] = useState(false);

  // const loginUserData = useSelector((state) => state.loginInfo.loginResponse);
  const indiaCountryCode = '+91';
  const visitMinTime = 1000;
  const visitMaxTime = 1800;

  const [projectList, setProjectList] = useState([]);
  const [selectedCountry, setSelectedCountry] = useState(
    appCountry.COUNTRIES[0],
  );
  const [showCountryModal, setShowCountryModal] = useState(false);
  const [showCountrySelectModal, setShowCountrySelectModal] = useState(false);
  const [isFormEditable, setIsFormEditable] = useState(true);
  const [dateClicked, isDateClicked] = useState(false);
  const [timeClicked, isTimeClicked] = useState(false);

  const [inputs, setInputs] = useState({
    name: {
      type: 'genericName',
      value: '',
      showErr: false,
    },
    mobNo: {
      type: 'indMobile',
      value: '',
      showErr: false,
    },
    email: {
      type: 'genericEmail',
      value: '',
      showErr: false,
    },
    country: {
      type: 'generic',
      value: '',
      showErr: false,
    },
    prefferedDate: {
      type: 'genericPrefferedDate',
      value: '',
      showErr: false,
    },
    prefferedTime: {
      type: 'genericPrefferedTime',
      value: '',
      showErr: false,
    },
  });

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );

  const loginUserData = useSelector((state) => state.loginInfo.loginResponse);

  const userData =
    loginUserData && loginUserData.data && loginUserData.data
      ? loginUserData.data
      : '';
  const userId =
    loginUserData && loginUserData.data && loginUserData.data.userid
      ? loginUserData.data.userid
      : '';

  const routeParams = route.params;
  const projectId =
    routeParams && routeParams.projectId ? routeParams.projectId : '';

  const dispatch = useDispatch();

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  useEffect(() => {
    if (deviceUniqueId) {
      getProjectsInfo();
    }
  }, [dispatch, deviceUniqueId]);

  const getProjectsInfo = async () => {
    setIsLoading(true);
    try {
      const result = await apiScheduleVisit.apiProjectsList();
      const resultData = result.data;

      setIsLoading(false);
      if (
        resultData &&
        resultData.status == '200' &&
        resultData.data &&
        resultData.data.length > 0
      ) {
        setProjectList(resultData.data);

        if (projectId && projectId != '') {
          const projects = resultData.data;
          const findIndex = projects.findIndex(
            (item) => item.proj_id === projectId,
          );
          setSelectedPropertyData(projects[findIndex]);
          setSelectedPropertyId(projects[findIndex].proj_id);
          setSelectedProperty(projects[findIndex].name);
          setSelectedPropertyImage(
            projects[findIndex].image ? projects[findIndex].image : null,
          );
        }
      } else {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
      validateApplicantInfo();
    } catch (error) {
      console.log('Get Project Info Error: ', error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
      validateApplicantInfo();
    }
  };

  const validateApplicantInfo = () => {
    if (userData && userId && userId != '') {
      setIsFormEditable(false);
    } else {
      setIsFormEditable(true);
    }

    const selectedCountry = appCountry.COUNTRIES.find(
      (item) => item.callingCodes == userData.country_code,
    );
    const defaultCountry = selectedCountry
      ? selectedCountry
      : appCountry.COUNTRIES[0];

    setInputs({
      ...inputs,
      ['name']: getInputValidationState({
        input: inputs['name'],
        value:
          userData.first_name && userData.last_name
            ? `${userData.first_name} ${userData.last_name}`
            : '',
      }),
      ['mobNo']: {
        ...inputs['mobNo'],
        value: userData.mob_no ? userData.mob_no : '',
        errorLabel: validateInput({
          type:
            defaultCountry.callingCodes === '+91' ? 'indMobile' : 'nriMobile',
          value: userData.mob_no ? userData.mob_no : '',
        }),
        optional: userData && userId && userId != '' ? true : false,
        showErr: false,
      },
      ['email']: getInputValidationState({
        input: inputs['email'],
        value: userData.email ? userData.email : '',
      }),
      ['country']: getInputValidationState({
        input: inputs['country'],
        value: defaultCountry ? defaultCountry.name : '',
      }),
      //   ['prefferedDate']: getInputValidationState({
      //     input: inputs['prefferedDate'],
      //     value: moment(new Date()).format('DD MMM YYYY'),
      //   }),
      //   ['prefferedTime']: getInputValidationState({
      //     input: inputs['prefferedTime'],
      //     value: moment(new Date()).format('hh:mm A'),
      //   }),
    });
  };

  const onSelectCountryCode = (selectedCountry) => {
    setSelectedCountry(selectedCountry);
    setShowCountryModal(false);

    const inputValidationType =
      selectedCountry &&
      selectedCountry.callingCodes &&
      selectedCountry.callingCodes === '+91'
        ? 'indMobile'
        : 'nriMobile';

    setInputs({
      ...inputs,
      ['mobNo']: {
        ...inputs['mobNo'],
        type: inputValidationType,
        errorLabel: inputs['mobNo'].optional
          ? null
          : validateInput({
              type: inputValidationType,
              value: inputs['mobNo'].value,
            }),
        showErr: false,
      },
    });
  };

  const onSelectCountry = (selectedCountry) => {
    setShowCountrySelectModal(false);

    setInputs({
      ...inputs,
      ['country']: getInputValidationState({
        input: inputs['country'],
        value: selectedCountry ? selectedCountry.name : '',
      }),
    });
  };

  const onInputChange = (id, value) => {
    setInputs({
      ...inputs,
      [id]: getInputValidationState(
        {
          input: inputs[id],
          value: value,
        },
        true,
      ),
    });
  };

  const getInputValidationState = ({input, value}, isShowError) => {
    if (input.type && input.type === 'genericPrefferedTime') {
      return {
        ...input,
        value,
        errorLabel: input.optional ? null : validateTimeInput(value),
        showErr: isShowError ? isShowError : false,
      };
    } else {
      return {
        ...input,
        value,
        errorLabel: input.optional
          ? null
          : validateInput({type: input.type, value}),
        showErr: isShowError ? isShowError : false,
      };
    }
  };

  const validateTimeInput = (timeVale) => {
    if (timeVale == '') {
      return 'Preferred Time is required';
    }

    const format = 'hh:mm A';

    const time = moment(timeVale, format);
    const beforeTime = moment('10:00 AM', format);
    const afterTime = moment('06:00 PM', format);

    if (time.isBetween(beforeTime, afterTime)) {
      return null;
    } else {
      return 'Preferred Time should be between 10:00 AM and 06:00 PM';
    }
  };

  const validateInput = ({type, value}) => {
    const result = validatejs(
      {
        [type]: value,
      },
      {
        [type]: validationDictionary[type],
      },
    );

    if (result) {
      return result[type][0];
    }

    return null;
  };

  const getFormValidation = () => {
    const updatedInputs = {};

    for (const [key, input] of Object.entries(inputs)) {
      updatedInputs[key] = getInputValidationState(
        {
          input,
          value: input.value,
        },
        true,
      );
    }

    setInputs(updatedInputs);
  };

  const renderError = (id) => {
    if (inputs && inputs[id].errorLabel && inputs[id].showErr) {
      return (
        <View style={styles.txtErrorContainer}>
          <AppText style={styles.txtError}>{inputs[id].errorLabel}</AppText>
        </View>
      );
    }
    return null;
  };

  const renderErrorMob = (id) => {
    if (inputs && inputs[id].errorLabel && inputs[id].showErr) {
      return (
        <View style={styles.txtErrorContainerMob}>
          <AppText style={styles.txtError}>{inputs[id].errorLabel}</AppText>
        </View>
      );
    }
    return null;
  };

  const showDatepicker = () => {
    setShowDatePicker(true);
    setShowTimePicker(false);

    setMode('date');
  };

  const onDateChange = (event, selectedDate) => {
    setShowDatePicker(false);

    setInputs({
      ...inputs,
      ['prefferedDate']: getInputValidationState(
        {
          input: inputs['prefferedDate'],
          value: moment(selectedDate).format('DD MMM YYYY'),
        },
        true,
      ),
    });
  };

  const showTimepicker = () => {
    setShowDatePicker(false);
    setShowTimePicker(true);

    setMode('time');
  };

  const onTimeChange = (event, selectedTime) => {
    setShowTimePicker(false);

    if (mode === 'time') {
      setInputs({
        ...inputs,
        ['prefferedTime']: getInputValidationState(
          {
            input: inputs['prefferedTime'],
            value: moment(selectedTime).format('hh:mm A'),
          },
          true,
        ),
      });
    }
  };

  const handleScheduleVisit = () => {
    getFormValidation();

    if (!selectedPropertyId) {
      appSnakBar.onShowSnakBar(`Please choose a property`, 'LONG');
    } else if (
      inputs['name'].value &&
      inputs['mobNo'].value &&
      inputs['email'].value &&
      inputs['country'].value &&
      inputs['prefferedDate'].value &&
      inputs['prefferedTime'].value &&
      !inputs['name'].errorLabel &&
      !inputs['mobNo'].errorLabel &&
      !inputs['email'].errorLabel &&
      !inputs['country'].errorLabel &&
      !inputs['prefferedDate'].errorLabel &&
      !inputs['prefferedTime'].errorLabel
    ) {
      const requestData = {
        user_id: userId ? userId : '',
        proj_id: selectedPropertyId,
        field_name: inputs['name'].value,
        field_email: inputs['email'].value,
        field_mobile_no:
          userData && userId && userId != ''
            ? inputs['mobNo'].value
            : selectedCountry.callingCodes + inputs['mobNo'].value,
        field_country: inputs['country'].value,
        field_contact_us_city: '',
        field_book_your_site_visit: '',
        field_contact_date: moment(inputs['prefferedDate'].value).format(
          'YYYY-MM-DD',
        ),
        field_contact_time: moment(
          inputs['prefferedTime'].value,
          'hh:mm A',
        ).format('HH:mm'),
        device_id: deviceUniqueId,
        country_code: selectedCountry.callingCodes,
      };

      sendRequestData(requestData);
    }
  };

  const sendRequestData = async (request) => {
    setSubmitIsLoading(true);
    try {
      const result = await apiScheduleVisit.apiScheduleVisit(request);
      const resultData = result.data;

      setSubmitIsLoading(false);

      if (resultData && resultData.status && resultData.status == '200') {
        setInputs({
          ...inputs,
          ['name']: {
            ...inputs['name'],
            value: '',
            errorLabel: null,
            showErr: false,
          },
          ['mobNo']: {
            ...inputs['mobNo'],
            value: '',
            errorLabel: null,
            showErr: false,
          },
          ['email']: {
            ...inputs['email'],
            value: '',
            errorLabel: null,
            showErr: false,
          },
          ['prefferedDate']: {
            ...inputs['prefferedDate'],
            value: '',
            errorLabel: null,
            showErr: false,
          },
          ['prefferedTime']: {
            ...inputs['prefferedTime'],
            value: '',
            errorLabel: null,
            showErr: false,
          },
        });

        const siteVisitRequestData = {
          visitor_id: request.user_id,
          visitor_name: request.field_name,
          visitor_email: request.field_email,
          visitor_mobile_no: request.field_mobile_no,
          visitor_country: request.field_country,
          visitor_country_code: request.country_code,
          visitor_contact_us_city: '',
          visitor_book_your_site_visit: '',
          visiting_date: inputs['prefferedDate'].value,
          visiting_time: inputs['prefferedTime'].value,
          visitor_device_id: deviceUniqueId,
          propertyDetails: selectedPropertyData,
          isPostSales: false,
        };

        navigation.dispatch(
          StackActions.replace('ScheduleVisitConfirmation', {
            siteVisitData: siteVisitRequestData,
          }),
        );

        // navigation.navigate('ScheduleVisitConfirmation', {
        //   siteVisitData: siteVisitRequestData,
        // });
      }

      appSnakBar.onShowSnakBar(
        resultData.msg
          ? resultData.msg
          : appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    } catch (error) {
      console.log('Get Project Info Error: ', error);
      setSubmitIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  useBackHandler(() => {
    if (selectModalVisible) {
      setSelectModalVisible(false);
      return true;
    }
    return false;
  });

  const onHandleRetryBtn = () => {
    if (deviceUniqueId) {
      getProjectsInfo();
    }
  };

  return (
    <Screen  onRetry={onHandleRetryBtn}>
      <AppHeader />

      <View style={styles.viewContainer}>
        <AppTextBold style={styles.pageTitle}>SCHEDULE A VISIT</AppTextBold>

        {isLoading ? (
          <View style={styles.container}></View>
        ) : (
          <ScrollView>
            <TouchableWithoutFeedback
              onPress={() => setSelectModalVisible(!selectModalVisible)}>
              <View style={styles.itemContainer}>
                <AppText style={styles.itemTxt} numberOfLines={1}>
                  {selectedProperty}
                </AppText>
                {!selectModalVisible && (
                  <Image
                    style={styles.arrow}
                    source={require('./../assets/images/down-icon-b.png')}
                  />
                )}
                {selectModalVisible && (
                  <Image
                    style={styles.arrow}
                    source={require('./../assets/images/up-icon-b.png')}
                  />
                )}
              </View>
            </TouchableWithoutFeedback>

            <View style={styles.container}>
              {selectedPropertyImage ? (
                <View style={styles.containerImage}>
                  <Image
                    source={{uri: selectedPropertyImage}}
                    style={styles.image}
                    resizeMode="stretch"
                  />
                </View>
              ) : (
                <>
                  {selectedProperty &&
                  selectedProperty != 'Choose a property' ? (
                    <View style={styles.containerImage}>
                      <Image
                        source={require('./../assets/images/no-image.jpg')}
                        style={styles.image}
                        resizeMode="stretch"
                      />
                    </View>
                  ) : null}
                </>
              )}
            </View>

            <View style={styles.formStyle}>
              <View style={styles.userViewContainer}>
                <AppText style={styles.userViewLabel}>Name *</AppText>
                <TextInput
                  numberOfLines={1}
                  style={styles.input}
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="default"
                  editable={isFormEditable}
                  value={
                    inputs && inputs['name'] && inputs['name'].value
                      ? inputs['name'].value
                      : ''
                  }
                  onChangeText={(value) => {
                    onInputChange('name', value);
                  }}
                />
                {renderError('name')}
              </View>

              {userId && userId != '' ? (
                <View style={styles.userViewContainer}>
                  <AppText style={styles.userViewLabel}>
                    Mobile Number *
                  </AppText>
                  <TextInput
                    numberOfLines={1}
                    style={styles.input}
                    autoCapitalize="none"
                    autoCorrect={false}
                    keyboardType="default"
                    editable={false}
                    value={
                      inputs && inputs['mobNo'] && inputs['mobNo'].value
                        ? inputs['mobNo'].value
                        : ''
                    }
                  />
                </View>
              ) : (
                <View>
                  <View
                    style={[
                      styles.userViewContainer,
                      {flexDirection: 'row', alignItems: 'flex-end'},
                    ]}>
                    <TouchableOpacity
                      activeOpacity={0.8}
                      onPress={() => setShowCountryModal(true)}>
                      <View style={styles.flagContainer}>
                        <Image
                          source={{uri: selectedCountry.flag}}
                          style={styles.flagIcon}
                        />
                        <AppText style={styles.dropdownCountry}>
                          {selectedCountry.alpha3Code}
                        </AppText>
                        <Image
                          source={require('./../assets/images/arrow-back-Y.png')}
                          style={styles.downIcon}
                        />
                      </View>
                    </TouchableOpacity>

                    <View style={styles.editInputContainer}>
                      <AppText style={styles.userViewLabel}>
                        {`(${selectedCountry.callingCodes})`}&nbsp;Mobile Number
                        *
                      </AppText>
                      <TextInput
                        numberOfLines={1}
                        style={styles.input}
                        autoCapitalize="none"
                        autoCorrect={false}
                        keyboardType="phone-pad"
                        editable={true}
                        value={
                          inputs && inputs['mobNo'] && inputs['mobNo'].value
                            ? inputs['mobNo'].value
                            : ''
                        }
                        onChangeText={(value) => {
                          onInputChange('mobNo', value);
                        }}
                      />
                    </View>
                  </View>
                  {renderErrorMob('mobNo')}
                </View>
              )}

              <View style={styles.userViewContainer}>
                <AppText style={styles.userViewLabel}>Email *</AppText>
                <TextInput
                  numberOfLines={1}
                  style={styles.input}
                  autoCapitalize="none"
                  editable={isFormEditable}
                  keyboardType="email-address"
                  value={
                    inputs && inputs['email'] && inputs['email'].value
                      ? inputs['email'].value
                      : ''
                  }
                  onChangeText={(value) => {
                    onInputChange('email', value);
                  }}
                />
                {renderError('email')}
              </View>

              <TouchableOpacity
                onPress={() => setShowCountrySelectModal(true)}
                activeOpacity={0.7}>
                <View style={styles.userViewContainer}>
                  <AppText style={styles.userViewLabel}>Country *</AppText>
                  <TextInput
                    numberOfLines={1}
                    style={styles.input}
                    autoCapitalize="none"
                    editable={false}
                    autoCorrect={false}
                    keyboardType="default"
                    value={
                      inputs && inputs['country'] && inputs['country'].value
                        ? inputs['country'].value
                        : ''
                    }
                    pointerEvents="none"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  isDateClicked(true);
                  showDatepicker();
                }}
                activeOpacity={0.7}>
                <View style={styles.userViewContainer}>
                  <AppText style={styles.userViewLabel}>
                    Preferred Date *
                  </AppText>
                  <TextInput
                    numberOfLines={1}
                    style={styles.input}
                    autoCapitalize="none"
                    autoCorrect={false}
                    keyboardType="default"
                    editable={false}
                    keyboardType="default"
                    value={
                      inputs &&
                      inputs['prefferedDate'] &&
                      inputs['prefferedDate'].value
                        ? inputs['prefferedDate'].value
                        : ''
                    }
                    pointerEvents="none"
                  />
                  {renderError('prefferedDate')}
                </View>
              </TouchableOpacity>

              {showDatePicker && (
                <DateTimePicker
                  style={{marginBottom: 12}}
                  testID="dateTimePicker"
                  value={dateSelected}
                  mode={mode}
                  display="default"
                  onChange={(e, date) => {
                    onDateChange(e, date);
                  }}
                  minimumDate={new Date()}
                  maximumDate={new Date().setFullYear(
                    new Date().getFullYear() + 1,
                  )}
                />
              )}

              <TouchableOpacity
                onPress={() => {
                  isTimeClicked(true);
                  showTimepicker();
                }}
                activeOpacity={0.7}>
                <View style={styles.userViewContainer}>
                  <AppText style={styles.userViewLabel}>
                    Preferred Time *
                  </AppText>
                  <TextInput
                    numberOfLines={1}
                    style={styles.input}
                    autoCapitalize="none"
                    autoCorrect={false}
                    keyboardType="default"
                    editable={false}
                    keyboardType="default"
                    value={
                      inputs &&
                      inputs['prefferedTime'] &&
                      inputs['prefferedTime'].value
                        ? inputs['prefferedTime'].value
                        : ''
                    }
                    pointerEvents="none"
                  />

                  {renderError('prefferedTime')}
                </View>
              </TouchableOpacity>

              {showTimePicker && (
                <DateTimePicker
                  style={{marginBottom: 12}}
                  testID="dateTimePicker"
                  value={new Date()}
                  mode={mode}
                  display="default"
                  onChange={(e, date) => {
                    onTimeChange(e, date);
                  }}
                />
              )}
            </View>

            <View style={styles.button}>
              <AppButton
                title="SCHEDULE A VISIT"
                onPress={(e) => {
                  handleScheduleVisit();
                }}
              />
            </View>
          </ScrollView>
        )}

        {isCountrySelected && (
          <CountryModal visible={true} func={setCountryInfo} />
        )}
        {modalVisible && <View style={styles.numberModal} />}

        <AppOverlayLoader isLoading={isLoading || isSubmitLoading} />

        {selectModalVisible && (
          <View style={styles.modalView}>
            <TouchableWithoutFeedback
              onPress={() => setSelectModalVisible(!selectModalVisible)}>
              <View style={styles.itemContainerModal}>
                <AppText noOf style={styles.itemTxt} numberOfLines={1}>
                  {selectedProperty}
                </AppText>

                {!selectModalVisible && (
                  <Image
                    style={styles.arrow}
                    source={require('./../assets/images/down-icon-b.png')}
                  />
                )}
                {selectModalVisible && (
                  <Image
                    style={styles.arrow}
                    source={require('./../assets/images/up-icon-b.png')}
                  />
                )}
              </View>
            </TouchableWithoutFeedback>

            <FlatList
              data={projectList}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => {
                    setSelectModalVisible(false);
                    setSelectedPropertyData(item);
                    setSelectedPropertyId(item.proj_id);
                    setSelectedProperty(item.name);
                    setSelectedPropertyImage(item.image ? item.image : null);
                  }}>
                  <View style={styles.containertext}>
                    <View style={styles.dropDownImageCon}>
                      {item.image ? (
                        <Image
                          style={styles.dropDownImage}
                          source={{uri: item.image ? item.image : null}}
                        />
                      ) : (
                        <Image
                          style={styles.dropDownImage}
                          source={require('./../assets/images/no-image.jpg')}
                        />
                      )}
                      <View style={styles.txtContainer}>
                        <AppText style={styles.nameText}>{item.name}</AppText>
                        <AppText style={styles.placeText}>
                          {item.sublocation}
                          {item.sublocation != '' ? ', ' : null}
                          {item.city}
                        </AppText>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        )}
      </View>

      <Modal
        animationType="fade"
        transparent={true}
        visible={showCountryModal}
        onRequestClose={() => {
          setShowCountryModal(false);
        }}>
        <CountrySelectModalScreen
          onCancelPress={() => {
            setShowCountryModal(false);
          }}
          searchData={''}
          onSelectCountry={(selectedCountry) => {
            onSelectCountryCode(selectedCountry);
          }}
          isShowCountryCode={true}
        />
      </Modal>

      <Modal
        animationType="fade"
        transparent={true}
        visible={showCountrySelectModal}
        onRequestClose={() => {
          setShowCountrySelectModal(false);
        }}>
        <CountrySelectModalScreen
          onCancelPress={() => {
            setShowCountrySelectModal(false);
          }}
          searchData={''}
          onSelectCountry={(selectedCountry) => {
            onSelectCountry(selectedCountry);
          }}
        />
      </Modal>

      <AppFooter activePage={4} isPostSales={false} />
    </Screen>
  );
};

export default VisitSchedule;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 18,
    paddingHorizontal: 20,
  },
  containerImage: {
    height: height / 3.5,
    width: '100%',
    marginBottom: 10,
  },
  image: {
    height: '100%',
    width: '100%',
  },
  itemTxt: {
    fontFamily: appFonts.SourceSansProSemiBold,
    fontSize: appFonts.largeBold,
  },
  viewContainer: {
    flex: 1,
    height: (height / 100) * 83,
  },
  dropDownImageCon: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  txtContainer: {justifyContent: 'center', marginLeft: 15, flex: 1},
  nameText: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
    marginBottom: 2,
  },
  placeText: {
    fontSize: appFonts.normalFontSize,
  },
  button: {
    marginBottom: 20,
    paddingHorizontal: 25,
  },
  containertext: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: colors.border,
    paddingVertical: 20,
  },
  dropDownImage: {
    width: 60,
    height: 60,
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: 5,
    textTransform: 'uppercase',
    marginHorizontal: 20,
  },
  arrow: {height: 16, width: 16},
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
    marginTop: 24,
    marginHorizontal: 20,
    backgroundColor: colors.LynxWhite,
  },
  itemContainerModal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15,
    paddingVertical: 15,
  },
  modalView: {
    margin: 25,
    backgroundColor: colors.primary,
    paddingHorizontal: 25,
    paddingVertical: 15,
    backgroundColor: colors.LynxWhite,
    height: height * 0.65,
  },
  formStyle: {
    flex: 1,
    alignSelf: 'flex-start',
    width: '100%',
    marginTop: 10,
    marginBottom: 10,
    paddingHorizontal: 20,
  },
  userViewContainer: {marginBottom: 25},
  userViewLabel: {color: colors.jaguar, fontSize: appFonts.largeFontSize},
  input: {
    borderBottomWidth: 1,
    borderBottomColor: colors.gray4,
    fontSize: appFonts.largeBoldx,
    paddingVertical: 8,
    color: colors.jaguar,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  flagContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: colors.gray4,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  editInputContainer: {flex: 1, marginLeft: 18},
  flagIcon: {
    height: 35,
    width: 40,
    alignSelf: 'baseline',
  },
  countryName: {
    color: colors.jaguar,
    marginLeft: 5,
  },
  downIcon: {
    height: 10,
    width: 10,
    marginLeft: 6,
  },
  dropdownCountry: {
    color: colors.jaguar,
    marginLeft: 6,
    fontSize: appFonts.largeFontSize,
  },
  txtErrorContainer: {
    marginTop: 8,
    marginBottom: 10,
  },
  txtErrorContainerMob: {
    marginBottom: 10,
    marginTop: -15,
  },
  txtError: {
    color: colors.danger,
    fontSize: appFonts.largeFontSize,
  },
});
