import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
  Text,
  Modal,
  LayoutAnimation,
  UIManager,
} from 'react-native';
import { useSelector } from 'react-redux';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppFooter from '../components/ui/AppFooter';
import appFonts from '../config/appFonts';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import colors from '../config/colors';
import AppButton from '../components/ui/AppButton';
import SelectPropertyModalScreen from './modals/SelectPropertyModalScreen';
import MyJourney from './MyJourney';
import ReciptDownloadModal from './modals/ReciptDownloadModal';
import apiMyAccount from '../api/apiMyAccounts';
import useFileDownload from '../hooks/useFileDownload';
import appSnakBar from '../utility/appSnakBar';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import appConstant from '../utility/appConstant';
import AppProgressBar from '../components/ui/AppProgressBar';
import Orientation from 'react-native-orientation';
let scrollRef = '';

let viewMorePosition = {};
let isPropertyOverView = false
var userId = '';

let shouldScrollToBottom = true

// const windowWidth = Dimensions.get('window').width;
// const windowHeight = Dimensions.get('window').height;

// const propertyItem = [
//   {
//     id: '1',
//     name: 'Godrej Meridien',
//     propertyName: 'Godrej Meridien/GODMERIT 1-0703',
//   },
//   {
//     id: '2',
//     name: 'Godrej Aqua',
//     propertyName: 'Godrej Aqua/GODMERIT 1-0703',
//   },
//   {
//     id: '3',
//     name: 'Godrej Home',
//     propertyName: 'Godrej Home/GODMERIT 1-0703',
//   },
// ];

const invoiceData = [
  {
    id: '1',
    title: 'Total Invoice (A)',
    value: 'â‚¹54,75,843.66',
  },
  {
    id: '2',
    title: 'Total Payments (B)',
    value: 'â‚¹54,75,291.45',
  },
  {
    id: '3',
    title: 'Total Interest',
    value: 'â‚¹556.63',
  },
  {
    id: '4',
    title: 'Total Interest Paid',
    value: 'â‚¹0.00',
  },
  {
    id: '5',
    title: 'Adjustment',
    value: 'â‚¹0.00',
  },
  {
    id: '6',
    title: 'Total Interest Payable(C)',
    value: 'â‚¹556.83',
  },
  {
    id: '7',
    title: 'Tax on Interest(D)',
    value: 'â‚¹100.39',
  },
  {
    id: '8',
    title: 'Total Payable(A-B)+(C+D)',
    value: 'â‚¹32,209.43',
  },
];

const MyAccountScreen = ({ navigation, route }) => {
  /************************************** For FAQ *****************************************/
  /**
   * 
   * @constant {boolean} isJourney It is come from FAQ screen to default selection of my journey
   * 
   */
  const isJourney = route.params ? (route.params.isJourney ? true : false) : false
  /**
   * 
   * @constant {boolean} isPropertyOverView It is come from FAQ screen to default expand of view more and expand overview
   * 
   */

  /****************************** ***************************************************/
  const [myAccount, setMyAccount] = useState(isJourney ? false : true);
  const [selectorName, setSelectorName] = useState({
    propertyName: null,
    booking_id: 0,
  });
  const {
    progress,
    showProgress,
    error,
    isSuccess,
    checkPermission,
  } = useFileDownload();
  const [isExpandedTab, setIsExpandedTab] = useState(false);
  const [isExpandedViewMore, setIsExpandedViewMore] = useState(false);
  const [isExpandedOverView, setIsExpandedOverView] = useState(false);
  const [isExpandedReceipts, setIsExpandedReceipts] = useState(false);
  const [isExpandedInvoice, setIsExpandedInvoice] = useState(false);
  const [selectModalVisible, setSelectModalVisible] = useState(false);
  const [reciptModalVisible, setReciptModalVisible] = useState(false);
  const [allSelectProperties, setAllSelectProperties] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showParticipantList, setShowParticipantList] = useState(false)

  // apidatastates
  const [account_summary, setAccount_Summary] = useState([]);
  const [singleAccSummary, setSingleAccSummary] = useState([]);
  const [propOverview, setPropOverview] = useState([]);
  const [accStatement, setAccStatement] = useState([]);
  const [interestStatement, setInterestStatement] = useState([]);
  const [receiptAndInvoice, setReceiptAndInvoice] = useState([]);
  const [bookingId, setBookingId] = useState([]);
  const [summaryDetails, setSummaryDetails] = useState([]);

  if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  const changeTabLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setIsExpandedTab(!isExpandedTab);
  };

  const changeReceiptsLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setIsExpandedReceipts(!isExpandedReceipts);
  };

  const changeInvoiceLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setIsExpandedInvoice(!isExpandedInvoice);
  };

  const changeOverviewLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setIsExpandedOverView(!isExpandedOverView);
  };

  const loginvalue = useSelector((state) => state.loginInfo.loginResponse);

  // const userId = loginvalue.data.userid;
  // console.log(userId);
  // api calls

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
    if (loginvalue.isLogin) {
      userId = loginvalue.data.userid;
    }
    console.log('account smummry is working', scrollRef);
    isPropertyOverView = route.params ? (route.params.isPropertyOverView ? true : false) : false
    console.log('>>>>>>>>>>>>>>>>>>>>>', isPropertyOverView)
    apiAccSummary();
  }, []);

  // acc_summary
  const apiAccSummary = async () => {
    try {
      setIsLoading(true);
      const requestData = { user_id: userId };
      const accsummary = await apiMyAccount.apiAccSummary(requestData);
      const booking_id = accsummary.data.data.booking_items[0].booking_id;
      setAccount_Summary(accsummary.data.data);
      setSelectorName({
        propertyName:
          accsummary.data.data.booking_items[0].project_code +
          ' / ' +
          accsummary.data.data.booking_items[0].inventory_code,
        booking_id: booking_id,
      });
      getAllDataForBookingId(booking_id);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  /**
   * 
   * @description If navigate from FAQ screen and property overview true then open view more 
   */
  const isViewMoreOpen = () => {
    setIsExpandedViewMore(isPropertyOverView);
    setIsExpandedOverView(isPropertyOverView);
  }

  /**
   * 
   * @description If navigate from FAQ screen 
   */
  const scrollToOVerViewPortion = (height) => {
    // console.log('isPropertyOverView', isPropertyOverView)


    //   if (isPropertyOverView) {
    //     if (scrollRef) {
    //       // if (viewMorePosition.y) {
    // console.log(height+"iiiiiiiiiiyury");   
    // if (shouldScrollToBottom) {
    //   if (scrollRef) {
    //     scrollRef.scrollTo({ y:height})
    //   }
    // }    
    // shouldScrollToBottom = true 

    //       // }
    //     }
    //   }
  }

  const getAllDataForBookingId = (booking_id) => {
    setIsLoading(true);
    setBookingId(booking_id);
    apiSingleAccSummary(booking_id);
    apiPropOverview(booking_id);
    apiReceiptAndInvoice(booking_id);
    apiAccStatement(booking_id);
    apiInterestStatement(booking_id);
    apiInterestSummaryDetails(booking_id);

  };

  const apiInterestSummaryDetails = async (booking_id) => {
    try {
      const requestData = { user_id: userId, booking_id: booking_id };
      const summaryDetails = await apiMyAccount.apiInterestSummaryDetails(
        requestData,
      );
      setSummaryDetails(summaryDetails.data.data);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  // acc_single_sunmmmry
  const apiSingleAccSummary = async (booking_id) => {
    try {
      const requestData = { user_id: userId, booking_id: booking_id };
      const singleaccsummary = await apiMyAccount.apiSingleAccSummary(
        requestData,
      );
      setSingleAccSummary(singleaccsummary.data.data);
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
    console.log(singleAccSummary.data);
  };

  // apiPropOverview
  const apiPropOverview = async (booking_id) => {
    try {
      const requestData = { user_id: userId, booking_id: booking_id };
      const propoverview = await apiMyAccount.apiPropOverview(requestData);
      setPropOverview(propoverview.data);
      if (isPropertyOverView) {
        isViewMoreOpen()
      }
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
    console.log({ propOverview });
  };

  // apiAccStatement
  const apiAccStatement = async (booking_id) => {
    try {
      const requestData = { user_id: userId, booking_id: booking_id };
      const accstatement = await apiMyAccount.apiAccStatement(requestData);
      setAccStatement(accstatement);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
    console.log(accStatement.data.data);
  };

  // apiInterestStatement
  const apiInterestStatement = async (booking_id) => {
    try {
      const requestData = { user_id: userId, booking_id: booking_id };
      const intereststatement = await apiMyAccount.apiInterestStatement(
        requestData,
      );
      setInterestStatement(intereststatement.data);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
    console.log(interestStatement.data);
  };

  //apiReceiptAndInvoice
  const apiReceiptAndInvoice = async (booking_id) => {
    try {
      const requestData = { user_id: userId, booking_id: booking_id };
      const receiptandinvoice = await apiMyAccount.apiReceiptAndInvoice(
        requestData,
      );
      setReceiptAndInvoice(receiptandinvoice.data.data);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const handleFileDownload = async (imgPath, isPdf) => {
    setIsLoading(true);
    let fileExt = isPdf ? 'pdf' : appConstant.getExtention(imgPath);
    let fName = appConstant.getFileName(imgPath);
    fName = fName + (isPdf ? '.pdf' : '');
    if (fileExt[0] && fName) {
      downloadFile(imgPath, fName, fileExt[0]);
    } else {
      setIsLoading(false);
    }
  };

  const downloadFile = async (imgPath, fileName, fileExtension) => {
    try {
      await checkPermission(imgPath, fileName, fileExtension);
      setIsLoading(false);
    } catch (error) {
      appSnakBar.onShowSnakBar(error.message, 'LONG');
      setIsLoading(false);
    }
  };

  const goToPayNow = () => {
    // bookingId
    navigation.navigate('PayNowScreen');

  };

  const onHandleRetryBtn = () => {
    if (loginvalue.isLogin) {
      userId = loginvalue.data.userid;
    }
    isPropertyOverView = route.params ? (route.params.isPropertyOverView ? true : false) : false
    apiAccSummary();
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />
      {/* <ScrollView> */}
      <View style={styles.container}>

        <View style={{ paddingTop: 10 }}>
          <AppText style={{ color: colors.gray3 }}>Properties</AppText>
          <TouchableWithoutFeedback
            onPress={() => {
              if (account_summary.booking_items.length > 1) {
                setSelectModalVisible(!selectModalVisible);
              }
            }}>
            <View style={styles.itemContainer}>
              <AppText style={styles.propertyText}>
                {selectorName.propertyName}
              </AppText>
              {account_summary.booking_items &&
                account_summary.booking_items.length > 1 && (
                  <Image
                    style={styles.arrow}

                    source={selectModalVisible? require('./../assets/images/up-icon-b.png'):require('./../assets/images/down-icon-b.png')}
                  />
                )}
            </View>
          </TouchableWithoutFeedback>
        </View>

        {/* my account button */}
        {!allSelectProperties ? (
          <View style={styles.tabName}>
            <View
              style={
                myAccount
                  ? styles.activeTab
                  : [styles.activeTab, { borderBottomWidth: 0 }]
              }>
              <TouchableOpacity
                onPress={() => {
                  setMyAccount(true);
                }}>
                <AppText
                  style={
                    myAccount
                      ? { fontWeight: 'bold', fontSize: appFonts.largeBold }
                      : { fontSize: appFonts.largeBold }
                  }>
                  My Account
                </AppText>
              </TouchableOpacity>
            </View>

            {/* My Journey button  */}
            <View
              style={
                !myAccount
                  ? styles.activeTab
                  : [styles.activeTab, { borderBottomWidth: 0 }]
              }>
              <TouchableOpacity
                onPress={() => {
                  setMyAccount(false);
                  isPropertyOverView = false;
                }}>
                <AppText
                  style={
                    !myAccount
                      ? { fontWeight: 'bold', fontSize: appFonts.largeBold }
                      : { fontSize: appFonts.largeBold }
                  }>
                  My Journey
                </AppText>
              </TouchableOpacity>
            </View>
          </View>
        ) : (
            <AppTextBold style={{ paddingVertical: 10 }}>My Account</AppTextBold>
          )}

        {/* my account vvalue.. */}
        <ScrollView ref={(ref) => scrollRef = ref} showsVerticalScrollIndicator={false} onContentSizeChange={(contentWidth, contentHeight) => {
          scrollToOVerViewPortion(contentHeight)
        }}>
          {myAccount ? (
            <View>
              <View style={styles.tabConatiner}>
                <View style={styles.amountBlock}>
                  <AppText style={[styles.label,{textAlign: 'center'}]}
                    textBreakStrategy="simple"
                    ellipsizeMode="tail">
                    Total Value
                  </AppText>
                  <AppTextBold
                    style={[styles.amount,{textAlign: 'center'}]}
                    textBreakStrategy="simple"
                    ellipsizeMode="tail">
                    {'\u20B9'}{' '}
                    {allSelectProperties
                      ? account_summary.revised_sales_consideration
                      : singleAccSummary.revised_sales_consideration}
                  </AppTextBold>
                </View>
                <View style={styles.row}>
                  <View style={styles.column1}>
                    <AppText style={styles.label}
                      textBreakStrategy="simple"
                      ellipsizeMode="tail">
                      Total Amount Due
                    </AppText>
                    <AppTextBold style={styles.amount}
                      textBreakStrategy="simple"
                      ellipsizeMode="tail">
                      {'\u20B9'}{' '}
                      {allSelectProperties
                        ? account_summary.total_overdue
                        : singleAccSummary.total_overdue}
                    </AppTextBold>
                  </View>
                  <View style={styles.column}>
                    <AppText style={styles.label}
                      textBreakStrategy="simple"
                      ellipsizeMode="tail">
                      Total Interest Payable
                    </AppText>
                    <AppTextBold style={styles.amount}
                      textBreakStrategy="simple"
                      ellipsizeMode="tail">
                      {'\u20B9'}{' '}
                      {allSelectProperties
                        ? account_summary.total_payable
                        : singleAccSummary.total_payable}
                    </AppTextBold>
                    <AppText style={{ fontSize: appFonts.smallFontSize, color: colors.gray3 }}>
                      As on date
                    </AppText>
                  </View>
                </View>
                <View
                  style={{
                    height: isExpandedTab ? null : 0,
                    overflow: 'hidden',
                  }}>
                  <View style={styles.row}>
                    <View style={styles.column1}>
                      <AppText style={styles.label}
                        textBreakStrategy="simple"
                        ellipsizeMode="tail">
                        Total Value Billed
                      </AppText>
                      <AppTextBold style={styles.amount}
                        textBreakStrategy="simple"
                        ellipsizeMode="tail">
                        {'\u20B9'}{' '}
                        {allSelectProperties
                          ? account_summary.total_billed
                          : singleAccSummary.total_billed}
                      </AppTextBold>
                    </View>
                    <View style={styles.column}>
                      <AppText style={styles.label}
                        textBreakStrategy="simple"
                        ellipsizeMode="tail">
                        Total Amount Paid
                      </AppText>
                      <AppTextBold style={styles.amount}
                        textBreakStrategy="simple"
                        ellipsizeMode="tail">
                        {'\u20B9'}{' '}
                        {allSelectProperties
                          ? account_summary.total_received
                          : singleAccSummary.total_received}
                      </AppTextBold>
                    </View>
                  </View>

                  {/* show when single is selected  or none is selected*/}

                  {!allSelectProperties ? (
                    <View style={styles.row}>
                      <View style={styles.column1}>
                        <AppText style={styles.label}
                          textBreakStrategy="simple"
                          ellipsizeMode="tail">
                          Last Billed Amount
                        </AppText>
                        <AppTextBold style={styles.amount}
                          textBreakStrategy="simple"
                          ellipsizeMode="tail">
                          {'\u20B9'}{' '}
                          {allSelectProperties
                            ? account_summary.last_billed
                            : singleAccSummary.last_billed}
                        </AppTextBold>
                        <AppText style={{ fontSize: appFonts.smallFontSize, color: colors.gray3 }}>
                          due on :{' '}
                          {allSelectProperties
                            ? account_summary.billed_date
                            : singleAccSummary.billed_date}
                        </AppText>
                      </View>
                      <View style={styles.column}>
                        <AppText style={styles.label}
                          textBreakStrategy="simple"
                          ellipsizeMode="tail">
                          Last Payment Received
                        </AppText>
                        <AppTextBold style={styles.amount}
                          textBreakStrategy="simple"
                          ellipsizeMode="tail">
                          {'\u20B9'}{' '}
                          {allSelectProperties
                            ? account_summary.last_payment
                            : singleAccSummary.last_payment}
                        </AppTextBold>
                        <AppText style={{ fontSize: appFonts.smallFontSize, color: colors.gray3 }}>
                          on{' '}
                          {allSelectProperties
                            ? account_summary.payment_date
                            : singleAccSummary.payment_date}
                        </AppText>
                      </View>
                    </View>
                  ) : null}
                </View>

                <View style={styles.payContainer}>
                  <View>
                    <AppText style={{ color: colors.gray3 }}>
                      Total Payable
                    </AppText>
                    <AppText style={{ fontSize: appFonts.normalFontSize, fontWeight: "bold" }}>
                      {'\u20B9'}
                      {allSelectProperties
                        ? account_summary.total_pay
                        : singleAccSummary.total_pay}
                    </AppText>
                  </View>

                  <TouchableOpacity
                    style={styles.expandButton}
                    onPress={changeTabLayout}>
                    <Image
                      source={
                        isExpandedTab
                          ? require('./../assets/images/up-icon-b.png')
                          : require('./../assets/images/down-icon-b.png')
                      }
                      style={styles.expandIcon}
                    />
                  </TouchableOpacity>

                  <View style={{ width: 130 }}>
                    <AppButton title="PAY NOW"
                      onPress={() => {
                        goToPayNow();
                      }} />
                  </View>
                </View>
              </View>

              {/* View More Component */}
              {!allSelectProperties ? (
                <View onLayout={(event) => {
                  viewMorePosition = event.nativeEvent.layout;
                }}>
                  <View style={{ padding: 15, alignItems: 'center' }}>
                    <TouchableOpacity
                      onPress={() =>
                        setIsExpandedViewMore(!isExpandedViewMore)
                      }>
                      <AppTextBold>
                        {isExpandedViewMore ? 'View Less' : 'View More'}
                      </AppTextBold>
                    </TouchableOpacity>
                  </View>
                  {isExpandedViewMore ? (
                    <View >
                      <View style={styles.buttonConatainer}>
                        <TouchableOpacity onPress={changeOverviewLayout}>
                          <View style={styles.button}>
                            <AppText style={styles.buttonText}>
                              Overview
                            </AppText>

                            <Image
                              source={
                                isExpandedOverView
                                  ? require('./../assets/images/up-icon-b.png')
                                  : require('./../assets/images/down-icon-b.png')
                              }
                              style={styles.icon}
                            />
                          </View>
                        </TouchableOpacity>

                        {isExpandedOverView && (
                          <View style={{ width: '95%', alignSelf: 'center' }}>
                            <View style={styles.overviewRow}>
                              <View style={styles.overviewColumn}>
                                <AppTextBold style={styles.overviewContentValue}>
                                  {propOverview.carpet_area == ''
                                    ? 'NA'
                                    : propOverview.carpet_area}
                                </AppTextBold>
                                <AppText
                                  style={styles.overviewContentTitle}>
                                  Carpet Area
                                </AppText>
                              </View>

                              <View style={{ ...styles.overviewColumn, borderRightWidth: 0 }}>
                                <AppTextBold style={styles.overviewContentValue}>
                                  {propOverview.exclusive_area == ''
                                    ? 'NA'
                                    : propOverview.exclusive_area}
                                </AppTextBold>
                                <AppText
                                  style={styles.overviewContentTitle}>
                                  Exclusive Area
                                </AppText>
                              </View>
                            </View>

                            <View style={styles.overviewRow}>
                              <View style={styles.overviewColumn}>
                                <AppTextBold style={styles.overviewContentValue}>
                                  {propOverview.total_area == ''
                                    ? 'NA'
                                    : propOverview.total_area}
                                </AppTextBold>
                                <AppText
                                  style={styles.overviewContentTitle}>
                                  Total Area
                                </AppText>
                              </View>

                              <View style={{ ...styles.overviewColumn, borderRightWidth: 0 }}>
                                <AppTextBold style={styles.overviewContentValue}>
                                  {propOverview.handover_date == ''
                                    ? 'NA'
                                    : propOverview.handover_date}
                                </AppTextBold>
                                <AppText
                                  style={styles.overviewContentTitle}>
                                  Planned Handover Date
                                </AppText>
                              </View>
                            </View>

                            <View style={styles.overviewRow}>
                              {propOverview.applicant && propOverview.applicant.length > 1 ?
                                <View>
                                  <TouchableOpacity onPress={() => { shouldScrollToBottom = false; setShowParticipantList(!showParticipantList) }}>
                                    <View style={{ flexDirection: 'column' }}>

                                      <View style={{ flexDirection: 'row', borderBottomWidth: 0.5, justifyContent: "center", alignItems: "center", right: 5 }}>
                                        <View style={{ ...styles.overviewColumn, width: '99%', borderRightWidth: 0, }}>
                                          <View style={{ flexDirection: 'row' }}>
                                            <AppTextBold style={{ ...styles.overviewContentValue, marginHorizontal: 10 }}>
                                              {propOverview.applicant[0]
                                                ? propOverview.applicant[0].name
                                                : 'NA'}
                                              <AppText
                                                style={{
                                                  ...styles.overviewContentTitle,
                                                  paddingLeft: 5,
                                                  marginTop: 8,
                                                  fontWeight: '100'
                                                }}>
                                                {propOverview.applicant[0]
                                                  ? ` - ${propOverview.applicant[0].type}`
                                                  : ''}
                                              </AppText>
                                            </AppTextBold>
                                          </View>
                                        </View>

                                        <Image
                                          source={
                                            showParticipantList
                                              ? require('./../assets/images/up-icon-b.png')
                                              : require('./../assets/images/down-icon-b.png')
                                          }
                                          style={{ ...styles.icon, marginTop: 3, right: 22 }}
                                        />
                                      </View>

                                      <AppText
                                        style={{ ...styles.overviewContentTitle, padding: 5 }}>
                                        Applicant(s)
                                      </AppText>
                                    </View>
                                  </TouchableOpacity>

                                  {showParticipantList ?
                                    <View style={{ borderWidth: 1, borderColor: colors.gray5, right: 4 }}>
                                      {propOverview.applicant.slice(1).map((ele, index) => (
                                        <View
                                          key={index}
                                          style={{
                                            ...styles.overviewColumn,
                                            width: '100%',
                                            height: 55,
                                            borderTopWidth: index === 0 ? 0 : 1,
                                            borderRightWidth: 0,
                                            borderTopColor: colors.gray5
                                          }}>
                                          <AppTextBold style={styles.overviewContentValue}>
                                            {ele.name}
                                          </AppTextBold>
                                          <AppText
                                            style={styles.overviewContentTitle}>
                                            {` - ${ele.type}`}
                                          </AppText>
                                        </View>
                                      ))}
                                    </View>
                                    :
                                    <View />}
                                </View>
                                :
                                <View style={{ ...styles.overviewColumn, width: '100%', borderRightWidth: 0 }}>
                                  <AppTextBold style={styles.overviewContentValue}>
                                    {propOverview.applicant[0]
                                      ? propOverview.applicant[0].name
                                      : 'NA'}
                                    <AppText
                                      style={styles.overviewContentTitle}>
                                      {propOverview.applicant[0]
                                        ? ` - ${propOverview.applicant[0].type}`
                                        : ''}
                                    </AppText>
                                  </AppTextBold>
                                </View>
                              }
                            </View>

                            <View style={{ ...styles.overviewRow, borderBottomWidth: 0 }}>
                              <View style={{ ...styles.overviewColumn, alignItems: 'center', width: '100%', borderRightWidth: 0 }}>
                                <AppTextBold style={styles.overviewContentValue}>
                                  {propOverview.rera_registration_number}
                                </AppTextBold>
                                <AppText
                                  style={styles.overviewContentTitle}>
                                  RERA Registration Number
                                </AppText>
                              </View>
                            </View>
                          </View>
                        )}
                      </View>

                      {/* Account statement */}

                      <View style={styles.buttonConatainer}>
                        <TouchableOpacity
                          onPress={() =>
                            handleFileDownload(accStatement.data.data)
                          }>
                          <View style={styles.button}>
                            <AppText style={styles.buttonText}>
                              Account Statement
                            </AppText>
                            <Image
                              source={require('./../assets/images/download-b.png')}
                              style={styles.icon}
                            />
                          </View>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.buttonConatainer}>
                        <TouchableOpacity
                          onPress={() => setReciptModalVisible(true)}>
                          <View style={styles.button}>
                            <AppText style={styles.buttonText}>
                              Interest Statement
                            </AppText>
                            <Image
                              source={require('./../assets/images/up-icon-b.png')}
                              style={[
                                styles.icon,
                                { transform: [{ rotate: '90deg' }] },
                              ]}
                            />
                          </View>
                        </TouchableOpacity>
                      </View>

                      {/* Receipts */}

                      <View style={styles.buttonConatainer}>
                        <TouchableOpacity onPress={changeReceiptsLayout}>
                          <View style={styles.button}>
                            <AppText style={styles.buttonText}>
                              Receipts
                            </AppText>
                            <Image
                              source={
                                isExpandedReceipts
                                  ? require('./../assets/images/up-icon-b.png')
                                  : require('./../assets/images/down-icon-b.png')
                              }
                              style={styles.icon}
                            />
                          </View>
                        </TouchableOpacity>

                        <View
                          style={{
                            height: isExpandedReceipts ? null : 0,
                            overflow: 'hidden',
                          }}>
                          <FlatList
                            data={receiptAndInvoice.receipt}
                            renderItem={({ item }) => (
                              <View style={styles.docContainer}>
                                <View style={styles.data}>
                                  <AppText>{item.name}</AppText>
                                  <TouchableOpacity
                                    onPress={() =>
                                      handleFileDownload(item.url, true)
                                    }>
                                    <Image
                                      source={require('./../assets/images/download-b.png')}
                                      style={styles.icon}
                                    />
                                  </TouchableOpacity>
                                </View>
                              </View>
                            )}
                          />
                        </View>
                      </View>

                      {/* invoice */}
                      <View style={styles.buttonConatainer}>
                        <TouchableOpacity onPress={changeInvoiceLayout}>
                          <View style={styles.button}>
                            <AppText style={styles.buttonText}>
                              Invoices
                            </AppText>
                            <Image
                              source={
                                isExpandedInvoice
                                  ? require('./../assets/images/up-icon-b.png')
                                  : require('./../assets/images/down-icon-b.png')
                              }
                              style={styles.icon}
                            />
                          </View>
                        </TouchableOpacity>

                        <View
                          style={{
                            height: isExpandedInvoice ? null : 0,
                            overflow: 'hidden',
                          }}>
                          <FlatList
                            data={receiptAndInvoice.invoice}
                            renderItem={({ item }) => (
                              <View style={styles.docContainer}>
                                <View style={styles.data}>
                                  <AppText>{item.name}</AppText>
                                  <TouchableOpacity
                                    onPress={() =>
                                      handleFileDownload(item.url, true)
                                    }>
                                    <Image
                                      source={require('./../assets/images/download-b.png')}
                                      style={styles.icon}
                                    />
                                  </TouchableOpacity>
                                </View>
                              </View>
                            )}
                          />
                        </View>
                      </View>
                    </View>
                  ) : null}
                </View>
              ) : null}
            </View>
          ) : (
              <View>
                <MyJourney
                  bookingId={isJourney ? route.params.bookingId : bookingId}
                  navigation={navigation}
                  handleFileDownload={handleFileDownload}
                  setIsLoading={setIsLoading}
                />
              </View>
            )}
        </ScrollView>
      </View>
      {/* </ScrollView> */}
      <Modal
        animationType="fade"
        transparent={true}
        visible={selectModalVisible}
        onRequestClose={() => {
          setSelectModalVisible(!selectModalVisible);
        }}>
        <SelectPropertyModalScreen
          properties={account_summary.booking_items}
          onCancel={() => {
            setSelectModalVisible(!selectModalVisible);
          }}
          onUpdate={() => {
            setAllSelectProperties(true);
            setSelectModalVisible(!selectModalVisible);
            setSelectorName({ propertyName: 'Select All', booking_id: 0 });
            setBookingId(0);
            isPropertyOverView = false;
          }}
          onSelect={(name, booking_id) => {
            setAllSelectProperties(false);
            setSelectModalVisible(!selectModalVisible);
            setSelectorName({ propertyName: name, booking_id: booking_id });
            getAllDataForBookingId(booking_id);

          }}
        />
      </Modal>

      {reciptModalVisible ? (
        <ReciptDownloadModal
          content={summaryDetails}
          header="SUMMARY"
          prevstate={() => {
            setReciptModalVisible(!reciptModalVisible);
          }}
          handleDownload={() => {
            handleFileDownload(interestStatement.data);
            setReciptModalVisible(!reciptModalVisible);
          }}
        />
      ) : null}

      <AppFooter activePage={0} isPostSales={true} />
      <AppOverlayLoader isLoading={isLoading} />
      {showProgress && progress && progress > 0 ? (
        <AppProgressBar progress={progress} />
      ) : null}
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '6%',
    justifyContent: 'center',
  },
  tabName: {
    paddingTop: '8%',
    paddingBottom: '3%',
    marginBottom: '5%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  activeTab: {
    width: '50%',
    paddingVertical: '5%',
    borderBottomWidth: 3,
    borderBottomColor: colors.gray,
    alignItems: 'center',
  },
  tabConatiner: {
    borderWidth: 1,
    borderRadius: 10,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  shadowBorders: {
    width: '90%',
    borderWidth: 1,
    // borderRadius: 5,
    padding: 10,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
    marginVertical: 10,
  },
  row: {
    width: '100%',
    // height: 100,
    flexDirection: 'row',
    paddingBottom: 20
  },
  column: {
    // flex: 0.5,
    width: '50%',
    paddingLeft: 15,
    paddingRight: 10,
    borderLeftWidth: 1,
    borderColor: '#e6e6e6',
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  column1: {
    // flex: 0.5,
    width: '50%',
    paddingLeft: 15,
  },
  payContainer: {
    width: '100%',
    height: 120,
    flexDirection: 'row',
    paddingHorizontal: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#f7f6f6',
  },
  buttonConatainer: {
    marginBottom: 20,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  button: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 25,
  },
  buttonText: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
    color: colors.secondary,
  },
  icon: {
    width: 20,
    height: 20,
    // marginRight: 10,
    zIndex: 9999,
  },
  expandButton: {
    bottom: 60,
    left: 21,
    width: 40,
    height: 40,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 50,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
    zIndex: 999999,
  },
  docContainer: {
    // padding: 20,
    paddingVertical: 20,
    paddingHorizontal: 25,
  },
  expandIcon: {
    width: 18,
    height: 18,
  },
  dropdownProperty: {
    borderBottomWidth: 2,
    borderBottomColor: colors.gray4,
    width: '100%',
    height: '100%',
    margin: 10,
    borderWidth: 0,
    marginBottom: 0,
    right: 20,
  },
  propertyText: {
    width: '90%',
    fontSize: appFonts.largeBold,
    fontWeight: 'bold',
    color: colors.secondary,
  },
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderBottomColor: colors.gray4,
    paddingVertical: 20,
    paddingRight: 20,
  
  },
  arrow: {
    height: 16,
    width: 16,
   
  },
  data: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  overviewRow: {
    flex: 1,
    flexDirection: 'row',
    padding: 5,
    borderBottomColor: colors.gray2,
    borderBottomWidth: 0.5
  },
  overviewColumn: {
    width: '50%',
    padding: 3,
    borderRightWidth: 0.5,
    borderColor: colors.gray2,
    alignItems: 'flex-start'
  },
  overviewContentTitle: {
    color: colors.gray3,
    fontSize: appFonts.normalFontSize
  },
  overviewContentValue: {
    fontSize: appFonts.largeBold,
    marginBottom: '1%',
    marginTop: '1%'
  },
  label: {
    width: '100%',
    color: colors.gray3,
    fontSize: appFonts.normalFontSize,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5
  },
  amount: {
    fontSize: appFonts.largeBold,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    // paddingVertical: 5
  },
  amountBlock: {
    flex: 1,
    width: '100%',
    textAlign: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: '6%' 
  }
});

export default MyAccountScreen;
