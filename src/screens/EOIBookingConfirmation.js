import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import Screen from '../components/Screen';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppButton from '../components/ui/AppButton';
import AppFooter from '../components/ui/AppFooter';

import AppOverlayLoader from '../components/ui/AppOverlayLoader';

import apiEoiBookingJourney from '../api/apiEoiBookingJourney';

import appSnakBar from '../utility/appSnakBar';
import appFonts from '../config/appFonts';
import {StackActions} from '@react-navigation/native';

import colors from '../config/colors';
import Orientation from 'react-native-orientation';

const EOIBookingConfirmation = ({navigation, route}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [bookingAmount, setBookingAmount] = useState('');
  const [projectName, setProjectName] = useState('');

  const routeParams = route.params;
  const projectId =
    routeParams && routeParams.projectId ? routeParams.projectId : '';
  const bookingId =
    routeParams && routeParams.bookingId ? routeParams.bookingId : '';
  const userId = routeParams && routeParams.userId ? routeParams.userId : '';

  const goToSiteVisitScreen = () => {
    navigation.dispatch(
      StackActions.replace('PresaleVisitScreen', {projectId: projectId}),
    );
  };

  const goToHomeScreen = () => {
    navigation.dispatch(StackActions.replace('GodrejHome'));
  };

  const goToBookingDetailsScreen = () => {
    navigation.dispatch(
      StackActions.replace('BookingDetailsScreen', {
        projectId: projectId,
        bookingId: bookingId,
        userId: userId,
      }),
    );
  };

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  useEffect(() => {
    if (projectId && bookingId && userId) {
      getEoiBookingConfirmationData(bookingId, userId);
    } else {
      navigation.goBack();
    }
  }, []);

  const getEoiBookingConfirmationData = async (bookingId, userId) => {
    try {
      setIsLoading(true);

      const requestData = {
        booking_id: bookingId,
        user_id: userId,
      };
      const bookingConfirmationResult = await apiEoiBookingJourney.apiEoiBookingConfirmationData(
        requestData,
      );

      setIsLoading(false);

      if (bookingConfirmationResult.data) {
        if (
          bookingConfirmationResult.data.status &&
          bookingConfirmationResult.data.status == '200'
        ) {
          setBookingAmount(
            bookingConfirmationResult.data.data &&
              bookingConfirmationResult.data.data.booking_amount
              ? bookingConfirmationResult.data.data.booking_amount
              : '',
          );

          setProjectName(
            bookingConfirmationResult.data.data &&
              bookingConfirmationResult.data.data.proj_name
              ? bookingConfirmationResult.data.data.proj_name
              : '',
          );
        } else {
          appSnakBar.onShowSnakBar(
            bookingConfirmationResult.data.msg
              ? bookingConfirmationResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      } else {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const onHandleRetryBtn = () => {
    if (projectId && bookingId && userId) {
      getEoiBookingConfirmationData(bookingId, userId);
    }
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      {isLoading ? (
        <View style={styles.container}></View>
      ) : (
        <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
          <View style={styles.container}>
            <View style={styles.subContainer}>
              <Image
                source={require('./../assets/images/tick-60x60.png')}
                style={styles.icon}
              />
              <View style={styles.title}>
                <AppTextBold style={styles.congText}>
                  CONGRATULATIONS
                </AppTextBold>
              </View>
            </View>

            {bookingAmount &&
            bookingAmount != '' &&
            projectName &&
            projectName != '' ? (
              <View style={styles.textContainer}>
                <AppText style={styles.details}>
                  Thank you for payment of
                  <AppText
                    style={{
                      fontSize: appFonts.largeBold,
                      fontFamily: appFonts.SourceSansProSemiBold,
                    }}>
                    &nbsp;{'\u20B9'}
                    {bookingAmount}/-&nbsp;
                  </AppText>
                  as Expression of Interest towards
                  <AppText
                    style={{
                      fontSize: appFonts.largeBold,
                      fontFamily: appFonts.SourceSansProSemiBold,
                    }}>
                    &nbsp;{projectName}
                  </AppText>
                  .
                </AppText>
                <AppText style={styles.details}>
                  Our representative will call you shortly and explain next
                  steps to book your Godrej Home
                </AppText>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={goToBookingDetailsScreen}>
                  <AppTextBold style={styles.text}>
                    View Booking Details
                  </AppTextBold>
                </TouchableOpacity>
              </View>
            ) : null}

            <View style={styles.btnContainer}>
              <AppButton
                color="primary"
                textColor="secondary"
                title="SCHEDULE SITE VISIT"
                onPress={goToSiteVisitScreen}
              />
              <AppButton title="VIEW MORE PROJECTS" onPress={goToHomeScreen} />
            </View>
          </View>
        </ScrollView>
      )}
      <AppFooter activePage={0} isPostSales={false} />
      <AppOverlayLoader isLoading={isLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: '7%',
    paddingVertical: '12%',
  },
  subContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    top: 20,
    height: 70,
    width: 70,
  },
  title: {
    paddingVertical: '12%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  congText: {
    fontSize: appFonts.xxxlargeFontSize,
    color: colors.expandText,
  },
  textContainer: {
    top: '5%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  details: {
    fontSize:appFonts.largeBold,
    lineHeight: 28,
    textAlign: 'center',
    color: colors.expandText,
    marginBottom: 12,
  },
  text: {
    fontSize: appFonts.largeBold,
    textDecorationLine: 'underline',
  },
  btnContainer: {
    top: '10%',
  },
});

export default EOIBookingConfirmation;
