import React, { useEffect, useState } from 'react';
import {
  View, StyleSheet, ScrollView, TextInput, Modal,
  Dimensions, Image, Text, TouchableOpacity, PermissionsAndroid
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import AppButton from '../components/ui/AppButton';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import Screen from '../components/Screen';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppFooter from '../components/ui/AppFooter';
import * as normStyle from '../styles/StyleSize';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import apiLogin from './../api/apiLogin';
import appSnakBar from '../utility/appSnakBar';
import { useNavigation } from '@react-navigation/native';
import CountrySelectModalScreen from './modals/CountrySelectModalScreen';
import Orientation from 'react-native-orientation';
import AsyncStorage from '@react-native-async-storage/async-storage';


const ChangesMobileNo = ({ props, route }) => {

  const routeParams = route.params;

  const userId =
    routeParams && routeParams.userId && routeParams.userId
      ? routeParams.userId
      : '';

  const mobileNo =
    routeParams && routeParams.mobileNo && routeParams.mobileNo
      ? routeParams.mobileNo
      : '';

  const [mobile, Setmobile] = useState("");
  const [isApiLoading, setApiIsLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [countryName, setCountryName] = useState("IND");
  const [countryId, setCountryId] = useState("+91");
  const [countryFlag, setFlag] = useState("https://www.countryflags.io/in/flat/64.png");
  const [disableButton, setDisableButton] = useState(true);
  const [showCountryModal, setShowCountryModal] = useState(false);
  const navigation = useNavigation();

  const dispatch = useDispatch();

  // My Logic
  var [baseUserProfile, setBaseUserProfile] = useState('');

  AsyncStorage.getItem("baseUserProfile").then((value) => {
    setBaseUserProfile(value);
  });
  
  const gpc_SendOTP = () => {
    var jsonBaseUserProfile = JSON.parse(baseUserProfile);
    console.log("jsonBaseUserProfile", jsonBaseUserProfile);
    var url = 'https://cp.godrejproperties.com/webapi/GetProfileData.ashx?userid=' + jsonBaseUserProfile.EmplID + '&module=send_otp&new_contact=' + mobile;
    console.log("url", url);
    var result = apiLogin.apiGetRequestData(url);
    
    navigation.navigate('OtpForEmailScreen',
      {
        emplId: jsonBaseUserProfile.EmplID,
        newMobile: mobile,
        actionType: 'update_mobile_no',
        country_Id: countryId,
        oldMobile: jsonBaseUserProfile.Mobile,
      }
    );
  }


  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);


  useEffect(() => {
    _getUserInfo();
  }, [loginvalue]);

  const loginvalue = useSelector(
    (state) => state.loginInfo.loginResponse,
  );

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );
  const menuType = useSelector((state) => state.menuType.menuType);
  const goBack = () => {
    navigation.goBack();
  };

  const _getUserInfo = () => {
    Setmobile(mobileNo.replace("+91", ""));
  };

  const onSelectCountry = (selectedCountry) => {
    console.log(selectedCountry.callingCodes);
    setCountryName(selectedCountry.alpha3Code);
    setCountryId(selectedCountry.callingCodes);
    setFlag(selectedCountry.flag);
    setShowCountryModal(false);
  };

  const _updateMobile = () => {
    if (mobile == '') {
      appSnakBar.onShowSnakBar(`Enter mobile number`, 'LONG');
      return null;
    }
    if (mobileNo == (countryId + mobile.trim())) {
      return null;
    } else {
      //alert(countryId+mobile.trim());
      if (mobile.length != 10) {
        appSnakBar.onShowSnakBar(`Mobile number must be 10 digits`, 'LONG');
        return null;
      } 
      else {
        navigation.navigate('OtpForEmailScreen',
          {
            userId: userId,
            mobileNo: countryId + mobile,
            actionType: 'update_mobile_no',
            country_Id: countryId,
            oldMobileValue: mobileNo,
          }
        );
      }
    }
  };

  const _onChangeMobile = (e) => {
    Setmobile(e);


    if ((mobileNo.replace(countryId, '')) == e) {
      setDisableButton(true);
    } else {
      setDisableButton(false);
    }
  };


const onHandleRetryBtn = () => {
};


  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />

    <ScrollView>
      <View style={styles.container}>

        <View style={{ width: '100%', alignItems: 'flex-start' }}>
          <AppText style={styles.pageTitle}>Edit Mobile Number</AppText>
        </View>

        <View style={styles.view1}>
          <View style={{ flex: 0.25 }}>
            <TouchableOpacity style={styles.countryFlagStyle} onPress={() => { setShowCountryModal(true) }}>
              <Image
                source={{ uri: countryFlag }}
                style={styles.flagStyle}
              />
              <AppText style={styles.countryIdstyle}>{countryName}</AppText>
              <Image
                source={require('./../assets/images/down-icon-b.png')}
                style={styles.downImgstyle}
              />
            </TouchableOpacity>
          </View>
          <View style={{ flex: 0.18, flexDirection: 'row' }}>
            <View style={{ flex: 0.2 }}></View>
            <View style={styles.countryIdStyle}>
              <AppText style={{ color: colors.secondaryDark }}>{countryId}</AppText>
            </View>
          </View>
          <View style={styles.mobileInputStyle}>
            <TextInput
              style={styles.userMobile}
              placeholder={"Mobile Number"}
              underlineColorAndroid="transparent"
              placeholderTextColor={colors.gray}
              keyboardType='number-pad'
              color={colors.secondaryDark}
              value={mobile}
              onChangeText={e => {
                _onChangeMobile(e);
              }}
            />
          </View>
        </View>


        {/* <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}> */}
          <View style={styles.button}>
            <AppButton
             color={disableButton ? 'charcoal' : 'secondary'}
             disabled={disableButton}
             textColor = {disableButton ? 'primary' : 'primary'}
              title="Send OTP"
              onPress={() => { gpc_SendOTP() }}
            />
          {/* </View> */}
        </View>

      </View>
    </ScrollView>
      <Modal
        animationType="fade"
        transparent={true}
        visible={showCountryModal}
        onRequestClose={() => {
          setShowCountryModal(false);
        }}>
        <CountrySelectModalScreen
          onCancelPress={() => {
            setShowCountryModal(false);
          }}
          searchData={''}
          onSelectCountry={(selectedCountry) => {
            onSelectCountry(selectedCountry);
          }}
          isShowCountryCode={true}
        />
      </Modal>
      <AppFooter activePage={0} isPostSales={menuType == 'postsale' ? true : false} />
      {/* {isCountrySelected && <CountryModal visible={true} func={setCountryInfo}/>} */}
      <AppOverlayLoader isLoading={isLoading || isApiLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  pageTitle: {
    fontSize: normStyle.normalizeWidth(20),
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginLeft: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(40),
  },
  imageView1: {
    width: normStyle.normalizeWidth(130),
    height: normStyle.normalizeWidth(105),
    flexDirection: 'row',
  },
  button: {
    // width: normStyle.normalizeWidth(100),
    // height: normStyle.normalizeWidth(0),
    alignItems: 'center', justifyContent: 'center',
    marginHorizontal:15,
    paddingBottom: normStyle.normalizeWidth(40),
    paddingTop: normStyle.normalizeWidth(40),
  },
  view1: {
    width: '100%',
    flexDirection: 'row',
    height: normStyle.normalizeWidth(60),
    paddingLeft: normStyle.normalizeWidth(10),
    paddingRight: normStyle.normalizeWidth(10),
  },
  mobileInputStyle: {
    flex: 0.58,
    borderBottomWidth: normStyle.normalizeWidth(2),
    borderColor: colors.borderGrey,
    alignItems: 'center',
    justifyContent: 'center'
  },
  countryFlagStyle: {
    flex: 1,
    borderColor: colors.borderGrey,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: normStyle.normalizeWidth(2),
    paddingLeft: normStyle.normalizeWidth(5),
  },
  countryIdstyle: {
    fontSize: normStyle.normalizeWidth(14),
    marginLeft: normStyle.normalizeWidth(5),
    color: colors.secondaryDark
  },
  downImgstyle: {
    width: normStyle.normalizeWidth(10),
    height: normStyle.normalizeWidth(20),
    marginLeft: normStyle.normalizeWidth(5)
  },
  countryIdStyle: {
    flex: 0.8,
    alignItems: 'flex-start',
    justifyContent: 'center',
    borderBottomWidth: 2,
    borderColor: colors.borderGrey,
  },
  userMobile: {
    width: '100%', paddingLeft: normStyle.normalizeWidth(20)
  },
  flagStyle: {
    width: normStyle.normalizeWidth(30),
    height: normStyle.normalizeWidth(30)
  }
});

export default ChangesMobileNo;
