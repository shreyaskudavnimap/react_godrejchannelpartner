import React, {useState, useEffect, useCallback, useRef} from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  Alert,
  Modal,
  ActivityIndicator,
  Platform,
} from 'react-native';
import moment from 'moment';
import GetLocation from 'react-native-get-location';
import {WebView} from 'react-native-webview';
import {useBackHandler} from '@react-native-community/hooks';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import AppButton from '../components/ui/AppButton';
import AppExpandButton from '../components/ui/AppExpandButton';
import BookingPropertyComponent from '../components/BookingPropertyComponent';
import PaymentPlanComponent from '../components/PaymentPlanComponent';
import CostSheetComponent from '../components/CostSheetComponent';
import BookingSourceComponent from '../components/BookingSourceComponent';
import ApplicantDetailsComponent from '../components/ApplicantDetailsComponent';
import PrimaryApplicant from '../components/PrimaryApplicant';
import BookingAmountComponent from '../components/BookingAmountComponent';

import {useDispatch, useSelector} from 'react-redux';
import * as invBookingJourneyAction from './../store/actions/invBookingJourneyAction';
import * as bookingEnquiryAction from './../store/actions/bookingEnquiryAction';
import * as inventoryMPLAction from './../store/actions/inventoryMPLAction';

import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';

import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import PaymentDetailModalScreen from './modals/PaymentDetailModalScreen';
import appCurrencyFormatter from '../utility/appCurrencyFormatter';
import useFileDownload from '../hooks/useFileDownload';
import AppProgressBar from '../components/ui/AppProgressBar';

import apiInvBookingJourney from './../api/apiInvBookingJourney';
import apiInventoryMLP from './../api/apiInventoryMLP';

import CountrySelectModalScreen from './modals/CountrySelectModalScreen';
import BookingTermConditionModalScreen from './modals/BookingTermConditionModalScreen';

import {validationDictionary} from '../utility/validation/dictionary';
import validatejs from 'validate.js';
import appCountry from '../utility/appCountry';
import {StackActions} from '@react-navigation/native';
import AppClickableText from '../components/ui/AppClickableText';

import appUserAuthorization from '../utility/appUserAuthorization';
import Orientation from 'react-native-orientation';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const promoCodeDataInitial = {
  payment_plan_id: '',
  total_new_price: '',
  total_old_price: '',
  disc_amt: '',
  disc_upto_limit: '',
  disc_type: '',
  disc_percentage: '',
  promo_id: '',
  promo_code: '',
};

const bookingAmountDataInitial = {
  booking_amount_radio: true,
  booking_amount: '',
  min_amount: '',
  max_amount: '',
  amount_paid: '',
  remaining_amount: '',
  custom_amount: '',
  payment_method: 'full', //custom/full
  is_booking_amount_error: false,
  booking_id: null,
  booking_no: null,
  res_booking_amount: null,
};

const SOCIAL_PREFERENCE = {
  is_show_referred_by_channel_partner: '1',
  is_referred_by_channel_partner: false,
  channel_partner_name: '',
  is_show_referred_by_customer: '1',
  is_referred_by_customer: false,
  customer_name: '',
  is_show_existing_customer: '1',
  is_existing_customer: false,
  booked_project_name: '',
  is_show_how_did_you_know: '1',
  is_how_did_you_know: false,
  how_did_you_know: '',
  source_protection_answer_id: '',
  is_source_custom_value: 0,
};

const bookingSource = [
  {
    key: '1',
    title: 'Referred by a Channel Partner',
    placeholderTxt: 'Name of the Channel Partner',
  },
  {
    key: '2',
    title: 'Referred by an existing customer',
    placeholderTxt: 'Name of the Customer',
  },
  {
    key: '3',
    title: "Existing Godrej Properties' customer",
    placeholderTxt: 'Project Name',
  },
  {
    key: '4',
    title: "Godrej Properties' advertisement",
    placeholderTxt: 'Select',
  },
];

const expandMenu = {
  propertyDetails: false,
  paymentDetails: false,
  costSheetDetails: false,
  bookingAmount: false,
  bookingSource: false,
  applicantDetails: false,
};

const BookingScreen = ({navigation, route}) => {
  const scrollRef = useRef();
  const dropPurpose = useRef();
  const dropDocumentId = useRef();
  const dropDocumentAddress = useRef();
  const dropSource = useRef();

  const [isTermsSelected, setIsTermsSelected] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [isSourceLoading, setIsSourceLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [isCouponApplied, setIsCouponApplied] = useState(false);
  const [selectedExpandMenu, setSelectedExpandMenu] = useState(expandMenu);
  const [selectedPaymentPlanDetails, setSelectedPaymentPlanDetails] = useState(
    null,
  );
  const [termsModalVisible, setTermsModalVisible] = useState(false);
  const [bookingTermsData, setBookingTermsData] = useState('');

  /* ------------------------ Source Protection Data State Start ------------------------ */
  const [sourceProtection, setSourceProtection] = useState(null);
  const [socialPreference, setSocialPreference] = useState(SOCIAL_PREFERENCE);
  const [sourceSelectedValue, setSourceSelectedValue] = useState(null);
  const [sourcePartner, setSourcePartner] = useState(null);
  const [sourceReferral, setSourceReferral] = useState(null);
  const [sourceLoyalty, setSourceLoyalty] = useState(null);
  const [sourceDirect, setSourceDirect] = useState(null);

  const [sourcePartnerDisabled, setSourcePartnerDisabled] = useState(false);
  const [sourceReferralDisabled, setSourceReferralDisabled] = useState(false);
  const [sourceLoyaltyDisabled, setSourceLoyaltyDisabled] = useState(false);
  const [sourceDirectDisabled, setSourceDirectDisabled] = useState(false);

  const [selectedSourcePartner, setSelectedSourcePartner] = useState('');
  const [selectedSourceReferral, setSelectedSourceReferral] = useState('');
  const [selectedSourceLoyalty, setSelectedSourceLoyalty] = useState('');
  const [selectedSourceDirect, setSelectedSourceDirect] = useState('');

  const [defaultChannelPartnerId, setDefaultChannelPartnerId] = useState('');
  const [defaultName, setDefaultName] = useState('');

  const [sourceProtectionError, setSourceProtectionError] = useState(false);
  /* ------------------------ Source Protection Data State End ------------------------ */

  const [propertyDetails, setPropertyDetails] = useState({});

  const routeParams = route.params;
  const projectId =
    routeParams && routeParams.project_id ? routeParams.project_id : '';
  const typologyData = routeParams.unitData.typologyData;
  const towerData = routeParams.unitData.towerData;
  const floorData = routeParams.unitData.floorData;
  const inventoryData = routeParams.unitData.inventoryData;
  const foyrTxnID = routeParams.unitData.foyrTxnID;
  const inventoryPreference = routeParams.unitData.inventory_preference;

  console.log("towerData", towerData)
  const {
    progress,
    showProgress,
    error,
    isSuccess,
    checkPermission,
  } = useFileDownload();

  const dispatch = useDispatch();

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );

  const loginUserData = useSelector((state) => state.loginInfo.loginResponse);
  const userId =
    loginUserData && loginUserData.data && loginUserData.data.userid
      ? loginUserData.data.userid
      : '';

  const invBookingJourneyData = useSelector((state) => state.invBookingJourney);
  const paymentPlanData = invBookingJourneyData.paymentPlan;
  // const paymentPlanDetails = invBookingJourneyData.paymentPlanDetails;
  const costSheetData = invBookingJourneyData.costSheet;
  const bookingPropertyInfo = invBookingJourneyData.bookingPropertyInfo;
  // const sourceProtectionData = invBookingJourneyData.sourceProtection;
  const promoCodeData = invBookingJourneyData.promoCode;
  const bookingAmount = invBookingJourneyData.bookingAmount;

  const bookingEnquiryDataArr = useSelector(
    (state) => state.bookingEnquiryDataArr,
  );
  const bookingEnquiryData = bookingEnquiryDataArr.bookingEnquiryData;

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
    if (deviceUniqueId && userId) {
      if (projectId) {
        getBookingPropertyInfo(
          userId,
          projectId,
          deviceUniqueId,
          inventoryData.id,
          towerData.id,
        );
      } else {
        navigation.goBack();
      }
    }
  }, [dispatch, deviceUniqueId, userId]);

  const displayErrorMsg = (message, isGoBack) => {
    appSnakBar.onShowSnakBar(message, 'LONG');
    if (isGoBack) {
      dispatch(inventoryMPLAction.resetInventoryList());
      navigation.goBack();
    }
  };

  const getBookingPropertyInfo = useCallback(
    async (userId, projId, deviceId, inventoryId, towerId) => {
      let enquiryId = '';
      let enquiryName = '';

      if (bookingEnquiryData) {
        const findIndex = bookingEnquiryData.findIndex(
          (element) => element.project_id == projectId,
        );
        if (findIndex > -1) {
          enquiryId = bookingEnquiryData[findIndex].booking_enquiry_id;
          enquiryName = bookingEnquiryData[findIndex].booking_enquiry_name;
        } else {
          enquiryId = '';
          enquiryName = '';
        }
      } else {
        enquiryId = '';
        enquiryName = '';
      }

      try {
        const bookingPropertyInfoResult = await dispatch(
          invBookingJourneyAction.getBookingPropertyInfo(
            userId,
            projId,
            deviceId,
            inventoryId,
            enquiryId,
            enquiryName,
          ),
        );

        if (
          bookingPropertyInfoResult.status &&
          bookingPropertyInfoResult.status == '200' &&
          bookingPropertyInfoResult.data
        ) {
          const projectInfoDetails = bookingPropertyInfoResult.data;

          updatePropertyDetails(projectInfoDetails);
          updateBookingEnquiryData(projectInfoDetails);
          validateApplicantInfo(bookingPropertyInfoResult);

          if (bookingPropertyInfoResult.source_protection) {
            // const testSocialProtection = {
            //   src_protection_flag: 'Source',
            //   enquiry_sfid: '',
            //   enquiry_name: '',
            //   date_of_enquiry: '',
            //   date_of_sitevisit: '',
            //   eoi_flag: true,
            //   closing_manager_name: '',
            //   enquiry_type: '',
            //   enquiry_source: '',
            //   contact_sfid: '',
            //   project_sfid: '',
            //   project_name: '',
            //   walkin_source_mobile: 'Channel Partner',
            //   walkin_source: '',
            //   source_name: 'CP-GPL / Square yards',
            //   other_cp_name: 'Soutrik',
            //   broker_account_sfid: '0016F00002yk66oQAA',
            //   loyalty_sfid: '',
            //   loyalty_name: '',
            //   referral_sfid: '',
            //   referral_name: '',
            //   employee_referral_name: '',
            //   referred_partner_flag: '',
            //   comments: '',
            // };
            // updateSourceProtection(testSocialProtection);
            updateSourceProtection(bookingPropertyInfoResult.source_protection);
          } else {
            setSourceProtection(null);
          }

          getPaymentPlan(projectId, towerId, inventoryId, userId);
        } else {
          setIsLoading(false);
          displayErrorMsg(
            paymentPlanResult.msg
              ? paymentPlanResult.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            true,
          );
        }
      } catch (err) {
        setIsLoading(false);
        displayErrorMsg(err.message, true);
      }
    },
    [dispatch, setIsLoading, isLoading, deviceUniqueId, projectId, userId],
  );

  const updatePropertyDetails = (resultData) => {
    if (resultData) {
      setPropertyDetails({
        proj_name: resultData.proj_name ? resultData.proj_name : '',
        proj_code: resultData.proj_code ? resultData.proj_code : '',
        proj_unit: resultData.proj_unit ? resultData.proj_unit : '',
        flat_type: resultData.flat_type ? resultData.flat_type : '',
        floor_no: resultData.floor_no ? resultData.floor_no : '',
        floor_code: resultData.floor_code ? resultData.floor_code : '',
        tower_code: resultData.tower_code ? resultData.tower_code : '',
        tower_name: resultData.tower_name ? resultData.tower_name : '',
        wing: resultData.wing ? resultData.wing : '',
        carpet_area: resultData.carpet_area ? resultData.carpet_area : '',
        carpet_area_unit: resultData.carpet_area_unit
          ? resultData.carpet_area_unit
          : '',
        open_balcony_area_sq_mt: resultData.open_balcony_area_sq_mt
          ? resultData.open_balcony_area_sq_mt
          : '',
        open_balcony_area_sq_ft: resultData.open_balcony_area_sq_ft
          ? resultData.open_balcony_area_sq_ft
          : '',
        plan_image: resultData.plan_image ? resultData.plan_image : '',
        inv_price: resultData.inv_price ? resultData.inv_price : '',
        total_cost: resultData.total_cost ? resultData.total_cost : '',
        booking_amount: resultData.booking_amount
          ? resultData.booking_amount
          : '',
        unit_no: resultData.unit_no ? resultData.unit_no : '',
      });
    }
  };

  const updateBookingEnquiryData = (resultData) => {
    if (resultData) {
      if (
        resultData.enquiry_update_status &&
        resultData.enquiry_update_status === true
      ) {
        const enquiryId = resultData.enquiry_id ? resultData.enquiry_id : '';
        const enquiryName = resultData.enquiry_name
          ? resultData.enquiry_name
          : '';

        const enquiryDataObj = {
          project_id: projectId,
          booking_type: 'Inventory',
          booking_enquiry_id: enquiryId,
          booking_enquiry_name: enquiryName,
        };
        const enquiryDataArr = [enquiryDataObj];
        const bookingEnqData = [...bookingEnquiryData];
        if (bookingEnqData) {
          const findIndex = bookingEnqData.findIndex(
            (element) => element.project_id == projectId,
          );
          if (findIndex > -1) {
            bookingEnqData[findIndex].booking_enquiry_id = enquiryId;
            bookingEnqData[findIndex].booking_enquiry_name = enquiryName;

            dispatch(
              bookingEnquiryAction.setBookingEnquiryData(bookingEnqData),
            );
          } else {
            bookingEnqData.push(enquiryDataObj);
            dispatch(
              bookingEnquiryAction.setBookingEnquiryData(bookingEnqData),
            );
          }
        } else {
          dispatch(bookingEnquiryAction.setBookingEnquiryData(enquiryDataArr));
        }
      }
    }
  };

  /* ------------------------ Source Protection Functionality Start ------------------------ */

  const updateSourceProtection = (resultData) => {
    if (resultData) {
      if (resultData.src_protection_flag.toLowerCase() == 'source') {
        if (
          resultData.walkin_source_mobile.toLowerCase() == 'channel partner'
        ) {
          socialPreference.is_referred_by_channel_partner = true;
          socialPreference.channel_partner_name = resultData.source_name;
          socialPreference.source_protection_answer_id =
            resultData.broker_account_sfid;

          setSelectedSourcePartner(resultData.source_name);
          onClickRadioItem('1', resultData);
        } else if (
          resultData.walkin_source_mobile.toLowerCase() == 'referral'
        ) {
          socialPreference.is_referred_by_customer = true;
          socialPreference.customer_name = resultData.referral_name;
          socialPreference.source_protection_answer_id =
            resultData.referral_sfid;

          setSelectedSourceReferral(resultData.referral_name);

          if (resultData.referral_name) {
            setSourceReferralDisabled(true);
          }

          onClickRadioItem('2', resultData);
        } else if (resultData.walkin_source_mobile.toLowerCase() == 'loyalty') {
          socialPreference.is_existing_customer = true;
          socialPreference.booked_project_name = resultData.loyalty_name;
          socialPreference.source_protection_answer_id =
            resultData.loyalty_sfid;

          setSelectedSourceLoyalty(resultData.loyalty_name);

          if (resultData.loyalty_name) {
            setSourceLoyaltyDisabled(true);
          }

          onClickRadioItem('3', resultData);
        } else if (resultData.walkin_source_mobile.toLowerCase() == 'direct') {
          socialPreference.is_how_did_you_know = true;
          socialPreference.how_did_you_know = resultData.walkin_source;

          setSelectedSourceDirect(resultData.walkin_source);

          if (resultData.walkin_source) {
            setSourceDirectDisabled(true);
          }

          onClickRadioItem('4', resultData);
        }
      } else if (resultData.src_protection_flag.toLowerCase() == 'edit') {
        if (
          resultData.walkin_source_mobile.toLowerCase() == 'channel partner'
        ) {
          socialPreference.is_referred_by_channel_partner = true;
          socialPreference.channel_partner_name = resultData.source_name;
          socialPreference.source_protection_answer_id =
            resultData.broker_account_sfid;

          setSelectedSourcePartner(resultData.source_name);
          onClickRadioItem('1', resultData);
        } else if (
          resultData.walkin_source_mobile.toLowerCase() == 'referral'
        ) {
          socialPreference.is_referred_by_customer = true;
          socialPreference.customer_name = resultData.referral_name;
          socialPreference.source_protection_answer_id =
            resultData.referral_sfid;

          setSelectedSourceReferral(resultData.referral_name);
          onClickRadioItem('2', resultData);
        } else if (resultData.walkin_source_mobile.toLowerCase() == 'loyalty') {
          socialPreference.is_existing_customer = true;
          socialPreference.booked_project_name = resultData.loyalty_name;
          socialPreference.source_protection_answer_id =
            resultData.loyalty_sfid;

          setSelectedSourceLoyalty(resultData.loyalty_name);
          onClickRadioItem('3', resultData);
        } else if (resultData.walkin_source_mobile.toLowerCase() == 'direct') {
          socialPreference.is_how_did_you_know = true;
          socialPreference.how_did_you_know = resultData.walkin_source;

          setSelectedSourceDirect(resultData.walkin_source);
          onClickRadioItem('4', resultData);
        }
      }

      setSourceProtection(resultData);
    } else {
      setSourceProtection(null);
    }
  };

  const onClickRadioItem = (...args) => {
    let sourceProtectionData = {};
    let key = '1';
    let isClicked = false;

    onCloseAllDropdown();

    if (args && args.length > 0) {
      key = args[0];
      if (args.length > 1) {
        isClicked = false;
        sourceProtectionData = {...args[1]};
      } else {
        sourceProtectionData = {...sourceProtection};
        isClicked = true;
      }
    } else {
      return;
    }

    let isOperationDone = false;
    if (
      sourceProtectionData.src_protection_flag.toLowerCase() !== 'source' &&
      isClicked
    ) {
      isOperationDone = true;
    } else {
      if (!isClicked) {
        isOperationDone = true;
      } else {
        isOperationDone = false;
      }
    }

    if (isOperationDone) {
      setSourceProtectionError(false);

      if (isClicked) {
        setSelectedSourcePartner('');
        setSelectedSourceReferral('');
        setSelectedSourceLoyalty('');

        setSelectedSourceDirect('');
      }

      if (key === '1') {
        socialPreference.is_referred_by_channel_partner = true;
        if (!sourcePartner) {
          getSourceControlData(key, 'partner', sourceProtectionData);
        } else {
          setSourceSelectedValue(key);

          socialPreference.channel_partner_name = '';
        }

        socialPreference.is_referred_by_customer = false;
        socialPreference.customer_name = '';

        socialPreference.is_existing_customer = false;
        socialPreference.booked_project_name = '';

        socialPreference.is_how_did_you_know = false;
        socialPreference.how_did_you_know = '';
      } else if (key === '2') {
        socialPreference.is_referred_by_customer = true;

        if (!sourceReferral) {
          getSourceControlData(key, 'referral', sourceProtectionData);
        } else {
          setSourceSelectedValue(key);
          socialPreference.customer_name = '';
        }

        socialPreference.is_referred_by_channel_partner = false;
        socialPreference.channel_partner_name = '';

        socialPreference.is_existing_customer = false;
        socialPreference.booked_project_name = '';

        socialPreference.is_how_did_you_know = false;
        socialPreference.how_did_you_know = '';
      } else if (key === '3') {
        socialPreference.is_existing_customer = true;

        if (!sourceLoyalty) {
          getSourceControlData(key, 'loyalty', sourceProtectionData);
        } else {
          setSourceSelectedValue(key);
          socialPreference.booked_project_name = '';
        }

        socialPreference.is_referred_by_channel_partner = false;
        socialPreference.channel_partner_name = '';

        socialPreference.is_referred_by_customer = false;
        socialPreference.customer_name = '';

        socialPreference.is_how_did_you_know = false;
        socialPreference.how_did_you_know = '';
      } else if (key === '4') {
        socialPreference.is_how_did_you_know = true;

        if (!sourceDirect) {
          getSourceControlData(key, 'direct', sourceProtectionData);
        } else {
          setSourceSelectedValue(key);
          socialPreference.how_did_you_know = '';
        }

        socialPreference.is_referred_by_channel_partner = false;
        socialPreference.channel_partner_name = '';

        socialPreference.is_referred_by_customer = false;
        socialPreference.customer_name = '';

        socialPreference.is_existing_customer = false;
        socialPreference.booked_project_name = '';
      }
    }
  };

  const getSourceControlData = async (
    key,
    sourceType,
    sourceProtectionParams,
  ) => {
    try {
      setIsSourceLoading(true);

      const requestData = {
        user_id: userId,
        device_id: deviceUniqueId,
        source_type: sourceType,
      };

      const sourceControlResult = await apiInvBookingJourney.apiSourceControl(
        requestData,
      );

      if (
        sourceControlResult.data.status &&
        sourceControlResult.data.status == '200' &&
        sourceControlResult.data.data
      ) {
        const resultData = sourceControlResult.data;

        const defaultParamId =
          sourceControlResult.data.default_channelPartnerId &&
          sourceControlResult.data.default_channelPartnerId != ''
            ? sourceControlResult.data.default_channelPartnerId
            : '';

        const defaultParamName =
          sourceControlResult.data.default_name &&
          sourceControlResult.data.default_name != ''
            ? sourceControlResult.data.default_name
            : '';

        setDefaultChannelPartnerId(defaultParamId);
        setDefaultName(defaultParamName);

        if (sourceType === 'partner') {
          if (resultData.data && resultData.data.length > 0) {
            const sourceProtectionData = {...sourceProtectionParams};

            if (sourceProtectionData.broker_account_sfid == defaultParamId) {
              socialPreference.channel_partner_name = '';
              if (sourceProtectionData.other_cp_name) {
                socialPreference.channel_partner_name =
                  sourceProtectionData.other_cp_name;

                setSelectedSourcePartner(socialPreference.channel_partner_name);
              }
            }
            if (
              socialPreference.channel_partner_name &&
              sourceProtectionData.src_protection_flag.toLowerCase() == 'source'
            ) {
              setSourcePartnerDisabled(true);
            }

            setSourcePartner(resultData.data);
          }
        } else if (sourceType === 'referral') {
          if (resultData.data && resultData.data.length > 0) {
            socialPreference.source_protection_answer_id = resultData.data[0];

            setSourceReferral(resultData.data);
          }
        } else if (sourceType === 'loyalty') {
          if (resultData.data && resultData.data.length > 0) {
            socialPreference.source_protection_answer_id = resultData.data[0];

            setSourceLoyalty(resultData.data);
          }
        } else if (sourceType === 'direct') {
          if (resultData.data && resultData.data.length > 0) {
            const directData = [];
            directData.push({label: 'Select', value: 'Select'});
            resultData.data.map((item) => {
              directData.push({label: item, value: item});
            });
            setSourceDirect(directData);
          }
        }

        setSourceSelectedValue(key);
      } else {
        displayErrorMsg(
          sourceControlResult.data.msg
            ? sourceControlResult.data.msg
            : appConstant.appMessage.APP_GENERIC_ERROR,
          false,
        );
      }

      setIsSourceLoading(false);
    } catch (error) {
      console.log(error);
      setIsSourceLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const onChangeSourceSelection = (changedValue) => {
    setSourceProtectionError(false);

    switch (sourceSelectedValue) {
      case '1':
        const findResult = sourcePartner.find(
          (item) => item.name === changedValue,
        );
        if (findResult && findResult.channelPartnerId_18) {
          socialPreference.channel_partner_name = changedValue;
          socialPreference.is_source_custom_value = 0;
          socialPreference.source_protection_answer_id =
            findResult.channelPartnerId_18;
        } else {
          socialPreference.channel_partner_name = changedValue;
          socialPreference.is_source_custom_value = 1;
          socialPreference.source_protection_answer_id = '';
        }

        setSelectedSourcePartner(changedValue);
        break;
      case '2':
        socialPreference.customer_name = changedValue;
        socialPreference.is_source_custom_value = 1;

        setSelectedSourceReferral(changedValue);
        break;
      case '3':
        socialPreference.booked_project_name = changedValue;
        socialPreference.is_source_custom_value = 1;

        setSelectedSourceLoyalty(changedValue);
        break;
      case '4':
        socialPreference.how_did_you_know = changedValue;
        socialPreference.is_source_custom_value = 0;

        setSelectedSourceDirect(changedValue);
        break;
    }
  };

  const getSourceSelectedValue = () => {
    let selectedVal = '';
    switch (sourceSelectedValue) {
      case '1':
        selectedVal = selectedSourcePartner;
        break;
      case '2':
        selectedVal = selectedSourceReferral;
        break;
      case '3':
        selectedVal = selectedSourceLoyalty;
        break;
      case '4':
        selectedVal = selectedSourceDirect;
        break;

      default:
        selectedVal = '';
        break;
    }
    return selectedVal;
  };

  const getSourceDisabledValue = () => {
    let disabledVal = false;
    switch (sourceSelectedValue) {
      case '1':
        disabledVal = sourcePartnerDisabled;
        break;
      case '2':
        disabledVal = sourceReferralDisabled;
        break;
      case '3':
        disabledVal = sourceLoyaltyDisabled;
        break;
      case '4':
        disabledVal = sourceDirectDisabled;
        break;

      default:
        disabledVal = false;
        break;
    }
    return disabledVal;
  };
  /* ------------------------ Source Protection Functionality End ------------------------ */

  const getPaymentPlan = useCallback(
    async (projectId, towerId, inventoryId, userId) => {
      try {
        const paymentPlanResult = await dispatch(
          invBookingJourneyAction.getPaymentPlan(
            projectId,
            towerId,
            inventoryId,
            userId,
          ),
        );

        if (paymentPlanResult.status && paymentPlanResult.status == '200') {
          const plans = paymentPlanResult.data.curl_response.payment_plans;
          if (plans && plans.length > 0) {
            onSelectPaymentPlan(plans[0], false);
          } else {
            setIsLoading(false);
            displayErrorMsg(appConstant.appMessage.APP_GENERIC_ERROR, true);
          }
        } else {
          setIsLoading(false);
          displayErrorMsg(
            paymentPlanResult.msg
              ? paymentPlanResult.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            true,
          );
        }
      } catch (err) {
        displayErrorMsg(err.message, true);
        setIsLoading(false);
      }
    },
    [dispatch, setIsLoading, isLoading, deviceUniqueId, projectId, userId],
  );

  const onSelectPaymentPlan = (selectedPlan, isCouponRemoved) => {
    setIsLoading(true);

    dispatch(invBookingJourneyAction.updatePaymentPlan(selectedPlan));

    getCostSheetData(
      projectId,
      userId,
      deviceUniqueId,
      inventoryData.id,
      selectedPlan.payment_plan_id,
      '',
      '',
      '',
      'WithoutPromoCode',
      isCouponRemoved,
    );
  };

  const onHandleAppliedCoupon = (couponCode) => {
    const selectedPlan = paymentPlanData.data.curl_response.payment_plans.find(
      (item) => item.plan_selected == '1',
    );

    if (selectedPlan) {
      if (isCouponApplied) {
        onSelectPaymentPlan(selectedPlan, true);
      } else {
        dispatch(
          invBookingJourneyAction.setPromoCodeData(promoCodeDataInitial),
        );

        dispatch(
          invBookingJourneyAction.setBookingAmountData(
            bookingAmountDataInitial,
          ),
        );

        const totSalesConsA =
          costSheetData.data.curl_response.project_price[0].value;
        const totalOldPrice =
          costSheetData.data.curl_response.project_stock.total_price;

        setIsLoading(true);
        getCostSheetData(
          projectId,
          userId,
          deviceUniqueId,
          inventoryData.id,
          selectedPlan.payment_plan_id,
          couponCode,
          totSalesConsA ? totSalesConsA : '',
          totalOldPrice ? totalOldPrice : '',
          'WithPromoCode',
          false,
        );
      }
    }
  };

  const getCostSheetData = useCallback(
    async (
      projId,
      userId,
      deviceId,
      inventoryId,
      paymentPlanId,
      promoCode,
      totSalesConsA,
      totalOldPrice,
      fetchType,
      isCouponRemoved,
    ) => {
      try {
        const costSheetResult = await dispatch(
          invBookingJourneyAction.getCostSheet(
            projId,
            userId,
            deviceId,
            inventoryId,
            paymentPlanId,
            promoCode,
            totSalesConsA,
            totalOldPrice,
          ),
        );

        if (costSheetResult.status && costSheetResult.status == '200') {
          if (costSheetResult.data.curl_response.hasOwnProperty('errors')) {
            if (costSheetResult.data.curl_response.errors.status == '1') {
              setIsLoading(false);
              displayErrorMsg(
                appConstant.appMessage.COST_SHEET_BREAKUP_CURL_REPONSE_ERROR,
                fetchType === 'WithPromoCode' ? false : true,
              );
            } else {
              setIsLoading(false);

              if (fetchType === 'WithoutPromoCode') {
                if (isCouponRemoved) {
                  displayErrorMsg(
                    appConstant.appMessage.PROMO_CODE_REMOVE,
                    false,
                  );
                }

                dispatch(
                  invBookingJourneyAction.setPromoCodeData(
                    promoCodeDataInitial,
                  ),
                );
                setIsCouponApplied(false);
              } else if (fetchType === 'WithPromoCode') {
                displayErrorMsg(appConstant.appMessage.PROMO_CODE_ADD, false);

                const promoCodeData = {
                  payment_plan_id: paymentPlanId,
                  total_new_price: costSheetResult.data.total_new_price
                    ? costSheetResult.data.total_new_price
                    : '',
                  total_old_price: costSheetResult.data.total_old_price
                    ? costSheetResult.data.total_old_price
                    : '',
                  disc_amt: costSheetResult.data.disc_amt
                    ? costSheetResult.data.disc_amt
                    : '',
                  disc_upto_limit: costSheetResult.data.disc_upto_limit
                    ? costSheetResult.data.disc_upto_limit
                    : '',
                  disc_type: costSheetResult.data.disc_type
                    ? costSheetResult.data.disc_type
                    : '',
                  disc_percentage: costSheetResult.data.disc_percentage
                    ? costSheetResult.data.disc_percentage
                    : '',
                  promo_id: costSheetResult.data.promo_id
                    ? costSheetResult.data.promo_id
                    : '',
                  promo_code: promoCode,
                };
                dispatch(
                  invBookingJourneyAction.setPromoCodeData(promoCodeData),
                );

                setIsCouponApplied(true);
              }

              const projectStock =
                costSheetResult.data.curl_response.project_stock;

              if (projectStock) {
                let bookingAmountRadio = false;
                if (
                  projectStock.custom_amount_to_be_shown &&
                  projectStock.custom_amount_to_be_shown.toLowerCase() === 'y'
                ) {
                  bookingAmountRadio = true;
                }

                let bookingAmount = '0';
                if (projectStock.booking_amount) {
                  bookingAmount = projectStock.booking_amount;
                }

                let minAmount = '0';
                if (projectStock.min_amount) {
                  minAmount = projectStock.min_amount;
                }

                let maxAmount = '0';
                if (projectStock.max_amount) {
                  maxAmount = projectStock.max_amount;
                }

                const bookingAmountData = {
                  booking_amount_radio: bookingAmountRadio,
                  booking_amount: bookingAmount,
                  min_amount: minAmount,
                  max_amount: maxAmount,
                  amount_paid: '',
                  remaining_amount: '',
                  custom_amount: '',
                  payment_method: 'full',
                  is_booking_amount_error: true,
                  booking_id: null,
                  booking_no: null,
                  res_booking_amount: null,
                };

                dispatch(
                  invBookingJourneyAction.setBookingAmountData(
                    bookingAmountData,
                  ),
                );
              } else {
                const bookingAmountDataBlank = {
                  booking_amount_radio: false,
                  booking_amount: '0',
                  min_amount: '0',
                  max_amount: '0',
                  amount_paid: '',
                  remaining_amount: '',
                  custom_amount: '',
                  payment_method: 'full',
                  is_booking_amount_error: true,
                  booking_id: null,
                  booking_no: null,
                  res_booking_amount: null,
                };

                dispatch(
                  invBookingJourneyAction.setBookingAmountData(
                    bookingAmountDataBlank,
                  ),
                );
              }
            }
          } else {
            setIsLoading(false);
            displayErrorMsg(
              appConstant.appMessage.COST_SHEET_BREAKUP_CURL_REPONSE_ERROR,
              fetchType === 'WithPromoCode' ? false : true,
            );
          }
        } else {
          setIsLoading(false);
          displayErrorMsg(
            costSheetResult.msg
              ? costSheetResult.msg
              : appConstant.appMessage.COST_SHEET_BREAKUP_STATUS_ERROR,
            fetchType === 'WithPromoCode' ? false : true,
          );
        }
      } catch (err) {
        displayErrorMsg(
          err.message,
          fetchType === 'WithPromoCode' ? false : true,
        );
        setIsLoading(false);
      }
    },
    [dispatch, setIsLoading, isLoading, deviceUniqueId, projectId, userId],
  );

  const [invalidCustomBookingAmount, setInvalidCustomBookingAmount] = useState(
    '',
  );

  const bookingCustomAmountValiation = (inpValue) => {
    let input = inpValue
      .toString()
      .split('.')
      .map((el, i) => (i ? el.split('').slice(0, 2).join('') : el))
      .join('.')
      .replace(/^([^.]*\.)(.*)$/, function (a, b, c) {
        return b + c.replace(/\./g, '');
      });

    let isValid = true;

    const bookingAmountDetails = {...bookingAmount};

    if (inpValue && inpValue != '') {
      if (isValid) {
        bookingAmountDetails.custom_amount = input;
        setInvalidCustomBookingAmount('');
        if (
          Number(bookingAmountDetails.custom_amount) >=
            Number(bookingAmountDetails.min_amount) &&
          Number(bookingAmountDetails.custom_amount) <=
            Number(bookingAmountDetails.max_amount) - 1
        ) {
          bookingAmountDetails.is_booking_amount_error = false;
        } else {
          bookingAmountDetails.is_booking_amount_error = true;
        }
      } else {
        // setInvalidCustomBookingAmount(
        //   'Invalid booking amount. Only 2 decimals allowed',
        // );
        setInvalidCustomBookingAmount('Invalid booking amount');
        bookingAmountDetails.is_booking_amount_error = true;
      }
    } else {
      setInvalidCustomBookingAmount('');
      bookingAmountDetails.custom_amount = input;
      bookingAmountDetails.is_booking_amount_error = true;
    }

    dispatch(
      invBookingJourneyAction.setBookingAmountData(bookingAmountDetails),
    );
  };

  const getRemainingBookingAmount = () => {
    return (
      Number(bookingAmount.max_amount) - Number(bookingAmount.custom_amount)
    );
  };

  const onClickBookingAmountType = (type) => {
    setInvalidCustomBookingAmount('');
    const bookingAmountDetails = {...bookingAmount};
    if (type == '1') {
      if (bookingAmountDetails.payment_method != 'full') {
        bookingAmountDetails.custom_amount = '';
        bookingAmountDetails.is_booking_amount_error = true;
        bookingAmountDetails.payment_method = 'full';
      }
    } else if (type == '2') {
      if (bookingAmountDetails.payment_method != 'custom') {
        bookingAmountDetails.payment_method = 'custom';
        bookingAmountDetails.custom_amount = '';
      }
    }

    dispatch(
      invBookingJourneyAction.setBookingAmountData(bookingAmountDetails),
    );
  };

  const getCustomAmountInputPlacehlder = () => {
    return `Pay any amount between ₹ ${appCurrencyFormatter.getIndianCurrencyFormat(
      bookingAmount.min_amount,
    )} and ₹ ${appCurrencyFormatter.getIndianCurrencyFormat(
      Number(bookingAmount.max_amount) - 1,
    )}`;
  };

  const getCustomAmountInputError = () => {
    return `Please enter an amount between ₹ ${appCurrencyFormatter.getIndianCurrencyFormat(
      bookingAmount.min_amount,
    )} and ₹ ${appCurrencyFormatter.getIndianCurrencyFormat(
      Number(bookingAmount.max_amount) - 1,
    )}`;
  };

  const onDownloadCostSheet = () => {
    // http://43.242.212.209/gpl-project/gpl-api/get_costsheet_download/319767-0.00-a1i6F0000077DOtQAM-a1u2s000000wlGSAAY-868704

    const bookingEnquiryId =
      bookingPropertyInfo &&
      bookingPropertyInfo.data &&
      bookingPropertyInfo.data.enquiry_id
        ? bookingPropertyInfo.data.enquiry_id
        : '';

    let fileUrl = '';

    const selectedPlan = paymentPlanData.data.curl_response.payment_plans.find(
      (item) => item.plan_selected == '1',
    );

    let promoID = '';
    if (
      promoCodeData &&
      promoCodeData.promo_id &&
      promoCodeData.promo_id != ''
    ) {
      promoID = promoCodeData.promo_id;
    } else {
      promoID = '0.00';
    }

    fileUrl =
      appConstant.buildInstance.baseUrl +
      'get_costsheet_download/' +
      inventoryData.id +
      '-' +
      promoID +
      '-' +
      (selectedPlan.payment_plan_id ? selectedPlan.payment_plan_id : '') +
      '-' +
      bookingEnquiryId +
      '-' +
      userId;

    const fileName =
      (inventoryData.unit_no ? inventoryData.unit_no : '') +
      moment(new Date()).format('yyyyMMDDhhmmss').toString() +
      '_CostSheet.pdf';

    const ext = fileName.split('.')[0];
    if (ext) {
      downloadFile(fileUrl, fileName, ext);

      // downloadFile('https://customercare.godrejproperties.com/sites/default/files/cj-booking-data/332464/AF/booking_form.pdf', fileName, ext);
      // downloadFile('http://43.242.212.209/gpl-project/gpl-api/get_costsheet_download/319767-0.00-a1i6F0000077DOtQAM-a1u2s000000wlGSAAY-868704', fileName, ext);
    }
  };

  const onDownloadApplicantDocument = (fileName, fileUrl) => {
    if (fileName && fileName != '' && fileUrl && fileUrl != '') {
      const ext = fileName.split('.')[0];
      if (ext) {
        downloadFile(fileUrl, fileName, ext);
      }
    }
  };

  const downloadFile = async (imgPath, fileName, fileExtension) => {
    try {
      await checkPermission(imgPath, fileName, fileExtension);
    } catch (error) {
      appSnakBar.onShowSnakBar(error.message, 'LONG');
    }
  };

  const onPressPaymentPlanInfo = () => {
    Alert.alert(
      'Price Info',
      'Click on View Plan Details for GST Exclusive of Stamp Duty & Registration Charge',
      [{text: 'GOT IT', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  };

  const onPressPaymentPlanDetails = (paymentPlan) => {
    console.log(paymentPlan);

    let discountAmount = '';
    if (
      promoCodeData &&
      promoCodeData.payment_plan_id == paymentPlan.payment_plan_id &&
      promoCodeData.disc_amt &&
      promoCodeData.disc_amt != ''
    ) {
      discountAmount = promoCodeData.disc_amt;
    } else {
      discountAmount = '';
    }

    getPaymenPlanDetailsData(
      paymentPlan.payment_plan_id,
      towerData.id,
      projectId,
      userId,
      inventoryData.id,
      discountAmount,
    );
  };

  const getPaymenPlanDetailsData = useCallback(
    async (
      paymentPlanId,
      towerId,
      projectId,
      userId,
      inventoryId,
      discountAmt,
    ) => {
      setIsLoading(true);
      try {
        const paymentPlanDetailsResult = await dispatch(
          invBookingJourneyAction.getPaymentPlanDetails(
            paymentPlanId,
            towerId,
            projectId,
            userId,
            inventoryId,
            discountAmt,
          ),
        );

        if (
          paymentPlanDetailsResult.status &&
          paymentPlanDetailsResult.status == '200'
        ) {
          if (
            paymentPlanDetailsResult.data.curl_response.hasOwnProperty('errors')
          ) {
            if (
              paymentPlanDetailsResult.data.curl_response.errors.status == '1'
            ) {
              displayErrorMsg('No plan details available', false);
            } else {
              const planDetails =
                paymentPlanDetailsResult.data.curl_response.milestones;
              if (planDetails && planDetails.length > 0) {
                const planDetailsObj = {};

                let noteMsg = '';
                let noteMsgAmount = '';
                if (
                  paymentPlanDetailsResult.data.curl_response.booking_amount &&
                  paymentPlanDetailsResult.data.curl_response.booking_amount !=
                    '' &&
                  paymentPlanDetailsResult.data.curl_response.booking_amount !=
                    0
                ) {
                  noteMsg = `${
                    paymentPlanDetailsResult.data.curl_response
                      .booking_days_label
                      ? paymentPlanDetailsResult.data.curl_response
                          .booking_days_label
                      : ''
                  }`;
                  noteMsgAmount = `${
                    paymentPlanDetailsResult.data.curl_response.currency
                      ? paymentPlanDetailsResult.data.curl_response.currency
                      : ''
                  } ${
                    paymentPlanDetailsResult.data.curl_response.booking_amount
                      ? appCurrencyFormatter.getIndianCurrencyFormat(
                          paymentPlanDetailsResult.data.curl_response
                            .booking_amount,
                        )
                      : ''
                  }`;
                } else {
                  noteMsg = '';
                  noteMsgAmount = '';
                }

                const plans = [];
                plans.push({
                  title: 'Payment Milestone',
                  interest: '%',
                  amount: 'Amount (INR)',
                  gst: 'GST (INR)',
                  total: 'Total',
                });

                planDetails.map((planDetail, index) => {
                  plans.push({
                    title: planDetail.milestones,
                    interest: planDetail.milestone_pct,
                    amount: appCurrencyFormatter.getIndianCurrencyFormat(
                      planDetail.amount,
                    ),
                    gst: appCurrencyFormatter.getIndianCurrencyFormat(
                      planDetail.gst,
                    ),
                    total: appCurrencyFormatter.getIndianCurrencyFormat(
                      planDetail.total,
                    ),
                    completed: planDetail.completed,
                  });
                });

                if (
                  paymentPlanDetailsResult.data.curl_response
                    .registration_amount &&
                  paymentPlanDetailsResult.data.curl_response
                    .registration_amount != ''
                ) {
                  plans.push({
                    title: paymentPlanDetailsResult.data.curl_response
                      .registration_label
                      ? paymentPlanDetailsResult.data.curl_response
                          .registration_label
                      : '',
                    interest: '',
                    amount: '',
                    gst: '',
                    total: appCurrencyFormatter.getIndianCurrencyFormat(
                      paymentPlanDetailsResult.data.curl_response
                        .registration_amount,
                    ),
                  });
                }

                plans.push({
                  title: 'Total',
                  interest: '',
                  amount: paymentPlanDetailsResult.data.curl_response
                    .grand_amount
                    ? appCurrencyFormatter.getIndianCurrencyFormat(
                        paymentPlanDetailsResult.data.curl_response
                          .grand_amount,
                      )
                    : 0,
                  gst: paymentPlanDetailsResult.data.curl_response.grand_gst
                    ? appCurrencyFormatter.getIndianCurrencyFormat(
                        paymentPlanDetailsResult.data.curl_response.grand_gst,
                      )
                    : 0,
                  total: paymentPlanDetailsResult.data.curl_response.grand_total
                    ? appCurrencyFormatter.getIndianCurrencyFormat(
                        paymentPlanDetailsResult.data.curl_response.grand_total,
                      )
                    : 0,
                });

                planDetailsObj['message'] = noteMsg.trim();
                planDetailsObj['messageAmount'] = noteMsgAmount.trim();
                planDetailsObj['plans'] = plans;
                planDetailsObj['payment_plan_name'] =
                  paymentPlanDetailsResult.data.curl_response.payment_plan_name;
                planDetailsObj['cip_pp_name'] =
                  paymentPlanDetailsResult.data.curl_response.cip_pp_name;
                planDetailsObj['payment_plan_id'] =
                  paymentPlanDetailsResult.data.curl_response.payment_plan_id;
                planDetailsObj['project_id'] =
                  paymentPlanDetailsResult.data.curl_response.project_id;

                planDetailsObj[
                  'sdr_status'
                ] = paymentPlanDetailsResult.sdr_status
                  ? paymentPlanDetailsResult.sdr_status
                  : false;

                planDetailsObj['disclaimer'] = paymentPlanData.disclaimer
                  ? paymentPlanData.disclaimer
                  : '';

                setSelectedPaymentPlanDetails(planDetailsObj);
                setModalVisible(true);
              } else {
                displayErrorMsg('No plan details available', false);
              }
            }
          } else {
            displayErrorMsg('No plan details available', false);
          }
        } else {
          displayErrorMsg(
            paymentPlanDetailsResult.msg
              ? paymentPlanDetailsResult.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            false,
          );
        }

        setIsLoading(false);
      } catch (err) {
        displayErrorMsg(err.message, false);
        setIsLoading(false);
      }
    },
    [dispatch, setIsLoading, isLoading, deviceUniqueId, projectId, userId],
  );

  /* ------------------------ Booking Form Functionality Start ------------------------ */

  const isNri = () => {
    // console.log(loginUserData)
    const isNriCustomer =
      loginUserData && loginUserData.data && loginUserData.data.country_code
        ? loginUserData.data.country_code
        : '+91';
    return isNriCustomer && isNriCustomer == '+91' ? false : true;
    // return true
  };

  const countryIndia = appCountry.COUNTRIES[0];
  const countryAfghanistan = appCountry.COUNTRIES[1];
  const idProofDocTypeInd = [
    {label: 'PAN Card', value: 'PAN Card'},
    {label: 'Passport', value: 'Passport'},
  ];
  const addressProofDocTypeInd = [
    {label: 'Aadhaar Card', value: 'Aadhaar Card'},
    {label: 'Passport', value: 'Passport'},
  ];
  const idProofDocTypeNri = [
    {label: 'Passport Front Page', value: 'Passport Front Page'},
  ];
  const addressProofDocTypeNri = [
    {label: 'Passport Back Page', value: 'Passport Back Page'},
  ];
  const [defaultCountry, setDefaultCountry] = useState(countryIndia);
  const [permanentCountry, setPermanentCountry] = useState(defaultCountry);
  const [communicationCountry, setCommunicationCountry] = useState(
    defaultCountry,
  );
  const [isSameAddress, setIsSameAddress] = useState(false);
  const [showCountryModal, setShowCountryModal] = useState(false);
  const [countryModalForPermanent, setCountryModalForPermanent] = useState(
    true,
  );
  const [purposeOfPurchase, setPurposeOfPurchase] = useState([]);
  const [isDobEditable, setIsDobEditable] = useState(true);
  const [attachedFileIdProof, setAttachedFileIdProof] = useState({});
  const [attachedFileAddressProof, setAttachedFileAddressProof] = useState({});

  const [inputs, setInputs] = useState({
    name: {
      type: 'generic',
      value: '',
      showErr: false,
    },
    mobNo: {
      type: 'generic',
      value: '',
      showErr: false,
    },
    email: {
      type: 'generic',
      value: '',
      showErr: false,
    },
    dob: {
      type: 'genericDob',
      value: '',
      showErr: false,
    },
    house_no: {
      type: 'houseNo',
      value: '',
      showErr: false,
    },
    street: {
      type: 'street',
      value: '',
      showErr: false,
    },
    locality: {
      type: 'locality',
      value: '',
      showErr: false,
    },
    pincode: {
      type: 'indPincode',
      value: '',
      showErr: false,
    },
    city: {
      type: 'city',
      value: '',
      showErr: false,
    },
    state: {
      type: 'state',
      value: '',
      showErr: false,
    },
    country: {
      type: 'generic',
      value: permanentCountry.name ? permanentCountry.name : '',
      showErr: false,
    },
    comm_house_no: {
      type: 'commHouseNo',
      value: '',
      showErr: false,
    },
    comm_street: {
      type: 'commStreet',
      value: '',
      showErr: false,
    },
    comm_locality: {
      type: 'commLocality',
      value: '',
      showErr: false,
    },
    comm_pincode: {
      type: 'commIndPincode',
      value: '',
      showErr: false,
    },
    comm_city: {
      type: 'commCity',
      value: '',
      showErr: false,
    },
    comm_state: {
      type: 'commState',
      value: '',
      showErr: false,
    },
    comm_country: {
      type: 'generic',
      value: communicationCountry.name ? communicationCountry.name : '',
      showErr: false,
    },
    pan_number: {
      type: 'panNo',
      value: '',
      showErr: false,
    },
    passport_number: {
      type: 'passportNo',
      value: '',
      showErr: false,
    },
    id_proof_doc: {
      type: 'genericIDProof',
      value: '',
      showErr: false,
    },
    address_proof_doc: {
      type: 'genericAddressProof',
      value: '',
      showErr: false,
    },
    purpose_of_purchase: {
      type: 'genericPurposeOfPurchase',
      value: '',
      showErr: false,
    },
    id_proof: {
      type: 'generic',
      value: '',
      showErr: false,
    },
    address_proof: {
      type: 'generic',
      value: '',
      showErr: false,
    },
  });

  const validateApplicantInfo = (responseData) => {
    // console.log(responseData);

    const purposeArr = [];
    if (responseData['purpose'] && responseData['purpose'].length > 0)
      responseData['purpose'].map((item) => {
        purposeArr.push({label: item, value: item});
      });
    setPurposeOfPurchase(purposeArr);

    const applicantData =
      responseData && responseData.data && responseData.data.applicants
        ? responseData.data.applicants
        : null;
    const storedUserData = loginUserData.data;

    let userDOB = '';
    if (
      storedUserData.hasOwnProperty('birth_dt') &&
      storedUserData.birth_dt &&
      storedUserData.birth_dt != ''
    ) {
      userDOB = storedUserData.birth_dt;
    } else {
      if (
        applicantData &&
        applicantData.hasOwnProperty('field_dob') &&
        applicantData.field_dob &&
        applicantData.field_dob != ''
      ) {
        userDOB = applicantData.field_dob;
      } else {
        userDOB = '';
      }
    }

    const isCustomer = storedUserData.is_customer;
    if (userDOB != '') {
      setIsDobEditable(isCustomer && isCustomer == '1' ? false : true);
    } else {
      setIsDobEditable(true);
    }

    let userPAN = '';
    if (
      storedUserData.hasOwnProperty('pan_no') &&
      storedUserData.pan_no &&
      storedUserData.pan_no != '' &&
      storedUserData.pan_no.toLowerCase() != 'null'
    ) {
      userPAN = storedUserData.pan_no;
    } else {
      if (
        applicantData &&
        applicantData.hasOwnProperty('pan_no') &&
        applicantData.pan_no &&
        applicantData.pan_no != '' &&
        applicantData.pan_no.toLowerCase() != 'null'
      ) {
        userPAN = applicantData.pan_no;
      } else {
        userPAN = '';
      }
    }

    let userPassport = '';
    if (
      applicantData &&
      applicantData.hasOwnProperty('field_passport_no') &&
      applicantData.field_passport_no &&
      applicantData.field_passport_no != '' &&
      applicantData.field_passport_no.toLowerCase() != 'null'
    ) {
      userPassport = applicantData.field_passport_no;
    } else {
      userPassport = '';
    }

    let purposeOfPurchaseData = '';
    if (
      applicantData &&
      applicantData.hasOwnProperty('field_applicant_purpose') &&
      applicantData.field_applicant_purpose &&
      applicantData.field_applicant_purpose != ''
    ) {
      if (applicantData.field_applicant_purpose == 'Self Use') {
        purposeOfPurchaseData = 'Self-Use';
      } else {
        purposeOfPurchaseData = applicantData.field_applicant_purpose;
      }
    } else {
      purposeOfPurchaseData = '';
    }

    const idProofFileType =
      applicantData &&
      applicantData.hasOwnProperty('id_proof_file_type') &&
      applicantData.id_proof_file_type &&
      applicantData.id_proof_file_type != ''
        ? applicantData.id_proof_file_type
        : '';

    const addressProofFileType =
      applicantData &&
      applicantData.hasOwnProperty('address_proof_file_type') &&
      applicantData.address_proof_file_type &&
      applicantData.address_proof_file_type != ''
        ? applicantData.address_proof_file_type
        : '';

    const perHouseNo =
      applicantData &&
      applicantData.hasOwnProperty('field_permanent_house_no_') &&
      applicantData.field_permanent_house_no_ &&
      applicantData.field_permanent_house_no_ != ''
        ? applicantData.field_permanent_house_no_
        : '';
    const perStreet =
      applicantData &&
      applicantData.hasOwnProperty('field_permanent_street') &&
      applicantData.field_permanent_street &&
      applicantData.field_permanent_street != ''
        ? applicantData.field_permanent_street
        : '';
    const perLocality =
      applicantData &&
      applicantData.hasOwnProperty('field_permanent_locality') &&
      applicantData.field_permanent_locality &&
      applicantData.field_permanent_locality != ''
        ? applicantData.field_permanent_locality
        : '';
    const perPincode =
      applicantData &&
      applicantData.hasOwnProperty('field_permanent_pincode') &&
      applicantData.field_permanent_pincode &&
      applicantData.field_permanent_pincode != ''
        ? applicantData.field_permanent_pincode
        : '';
    const perCity =
      applicantData &&
      applicantData.hasOwnProperty('field_permanent_city') &&
      applicantData.field_permanent_city &&
      applicantData.field_permanent_city != ''
        ? applicantData.field_permanent_city
        : '';
    const perState =
      applicantData &&
      applicantData.hasOwnProperty('field_permanent_state') &&
      applicantData.field_permanent_state &&
      applicantData.field_permanent_state != ''
        ? applicantData.field_permanent_state
        : '';
    const perCountry =
      applicantData &&
      applicantData.hasOwnProperty('field_permanent_country') &&
      applicantData.field_permanent_country &&
      applicantData.field_permanent_country != ''
        ? applicantData.field_permanent_country
        : '';

    let defaultCountry = countryIndia;
    if (perCountry != '') {
      const selectedPreCountry = appCountry.COUNTRIES.find(
        (item) => item.name.toLowerCase() == perCountry.toLowerCase(),
      );
      defaultCountry = selectedPreCountry ? selectedPreCountry : countryIndia;
    } else {
      if (isNri()) {
        const selectedCountry = appCountry.COUNTRIES.find(
          (item) => item.callingCodes == storedUserData.country_code,
        );
        defaultCountry = selectedCountry ? selectedCountry : countryAfghanistan;
      } else {
        defaultCountry = countryIndia;
      }
    }

    const commHouseNo =
      applicantData &&
      applicantData.hasOwnProperty('field_communication_house_no_') &&
      applicantData.field_communication_house_no_ &&
      applicantData.field_communication_house_no_ != ''
        ? applicantData.field_communication_house_no_
        : '';
    const commStreet =
      applicantData &&
      applicantData.hasOwnProperty('field_communication_street') &&
      applicantData.field_communication_street &&
      applicantData.field_communication_street != ''
        ? applicantData.field_communication_street
        : '';
    const commLocality =
      applicantData &&
      applicantData.hasOwnProperty('field_communication_locality') &&
      applicantData.field_communication_locality &&
      applicantData.field_communication_locality != ''
        ? applicantData.field_communication_locality
        : '';
    const commPincode =
      applicantData &&
      applicantData.hasOwnProperty('field_communication_pincode') &&
      applicantData.field_communication_pincode &&
      applicantData.field_communication_pincode != ''
        ? applicantData.field_communication_pincode
        : '';
    const commCity =
      applicantData &&
      applicantData.hasOwnProperty('field_communication_city') &&
      applicantData.field_communication_city &&
      applicantData.field_communication_city != ''
        ? applicantData.field_communication_city
        : '';
    const commState =
      applicantData &&
      applicantData.hasOwnProperty('field_communication_state') &&
      applicantData.field_communication_state &&
      applicantData.field_communication_state != ''
        ? applicantData.field_communication_state
        : '';
    const commCountry =
      applicantData &&
      applicantData.hasOwnProperty('field_communication_country') &&
      applicantData.field_communication_country &&
      applicantData.field_communication_country != ''
        ? applicantData.field_communication_country
        : '';

    let defaultCommCountry = countryIndia;
    if (commCountry != '') {
      const selectedCommCountry = appCountry.COUNTRIES.find(
        (item) => item.name.toLowerCase() == commCountry.toLowerCase(),
      );
      defaultCommCountry = selectedCommCountry
        ? selectedCommCountry
        : countryIndia;
    } else {
      if (isNri()) {
        const selectedCountryC = appCountry.COUNTRIES.find(
          (item) => item.callingCodes == storedUserData.country_code,
        );
        defaultCommCountry = selectedCountryC
          ? selectedCountryC
          : countryAfghanistan;
      } else {
        defaultCommCountry = countryIndia;
      }
    }

    setPermanentCountry(defaultCountry);
    setCommunicationCountry(defaultCommCountry);

    if (
      perHouseNo != '' &&
      perStreet != '' &&
      perLocality != '' &&
      perPincode != '' &&
      perCity != '' &&
      perState != '' &&
      perCountry != '' &&
      commHouseNo != '' &&
      commStreet != '' &&
      commLocality != '' &&
      commPincode != '' &&
      commCity != '' &&
      commState != '' &&
      commCountry != '' &&
      perHouseNo == commHouseNo &&
      perStreet == commStreet &&
      perLocality == commLocality &&
      perPincode == commPincode &&
      perCity == commCity &&
      perState == commState &&
      perCountry == commCountry
    ) {
      setIsSameAddress(true);
    } else {
      setIsSameAddress(false);
    }

    let idProofName = '';
    let addressProofName = '';

    if (
      applicantData &&
      applicantData.hasOwnProperty('field_residance_type') &&
      applicantData.field_residance_type
    ) {
      if (applicantData.field_residance_type == 'Indian') {
        idProofName =
          applicantData.hasOwnProperty('fl_id_name') && applicantData.fl_id_name
            ? applicantData.fl_id_name
            : '';
        addressProofName =
          applicantData.hasOwnProperty('fl_addr_name') &&
          applicantData.fl_addr_name
            ? applicantData.fl_addr_name
            : '';

        const nameSplitIdIndian =
          applicantData.hasOwnProperty('fl_id_name') && applicantData.fl_id_name
            ? applicantData.fl_id_name.split('.')
            : [];

        const idType =
          nameSplitIdIndian && nameSplitIdIndian.length > 0
            ? nameSplitIdIndian[nameSplitIdIndian.length - 1].toString()
            : '';

        const nameSplitAddressIndian =
          applicantData.hasOwnProperty('fl_addr_name') &&
          applicantData.fl_addr_name
            ? applicantData.fl_addr_name.split('.')
            : [];

        const addressType =
          nameSplitAddressIndian && nameSplitAddressIndian.length > 0
            ? nameSplitAddressIndian[
                nameSplitAddressIndian.length - 1
              ].toString()
            : '';

        setAttachedFileIdProof({
          file_name:
            applicantData.hasOwnProperty('fl_id_name') &&
            applicantData.fl_id_name
              ? applicantData.fl_id_name
              : '',
          file_format:
            idType &&
            (idType.toLowerCase() == 'jpg' ||
              idType.toLowerCase() == 'jpeg' ||
              idType.toLowerCase() == 'png')
              ? 'image'
              : idType
              ? idType
              : '',
          file_size: '',
          file:
            applicantData.hasOwnProperty('fl_id_url') && applicantData.fl_id_url
              ? applicantData.fl_id_url
              : '',
          fileUploaded: 'no',
          fid:
            applicantData.hasOwnProperty('fl_id_fid') && applicantData.fl_id_fid
              ? applicantData.fl_id_fid
              : '',
          type: 'field_id_proof',
        });

        setAttachedFileAddressProof({
          file_name:
            applicantData.hasOwnProperty('fl_addr_name') &&
            applicantData.fl_addr_name
              ? applicantData.fl_addr_name
              : '',
          file_format:
            addressType &&
            (addressType.toLowerCase() == 'jpg' ||
              addressType.toLowerCase() == 'jpeg' ||
              addressType.toLowerCase() == 'png')
              ? 'image'
              : addressType
              ? addressType
              : '',
          file_size: '',
          file:
            applicantData.hasOwnProperty('fl_addr_url') &&
            applicantData.fl_addr_url
              ? applicantData.fl_addr_url
              : '',
          fileUploaded: 'no',
          fid:
            applicantData.hasOwnProperty('fl_addr_fid') &&
            applicantData.fl_addr_fid
              ? applicantData.fl_addr_fid
              : '',
          type: 'field_address_proof',
        });
      } else {
        idProofName =
          applicantData.hasOwnProperty('fl_pass_front_name') &&
          applicantData.fl_pass_front_name
            ? applicantData.fl_pass_front_name
            : '';
        addressProofName =
          applicantData.hasOwnProperty('fl_pass_back_name') &&
          applicantData.fl_pass_back_name
            ? applicantData.fl_pass_back_name
            : '';

        const nameSplitIdNri =
          applicantData.hasOwnProperty('fl_pass_front_name') &&
          applicantData.fl_pass_front_name
            ? applicantData.fl_pass_front_name.split('.')
            : [];

        const idTypeNri =
          nameSplitIdNri && nameSplitIdNri.length > 0
            ? nameSplitIdNri[nameSplitIdNri.length - 1].toString()
            : '';

        const nameSplitAddressNri =
          applicantData.hasOwnProperty('fl_pass_back_name') &&
          applicantData.fl_pass_back_name
            ? applicantData.fl_pass_back_name.split('.')
            : [];

        const addressTypeNri =
          nameSplitAddressNri && nameSplitAddressNri.length > 0
            ? nameSplitAddressNri[nameSplitAddressNri.length - 1].toString()
            : '';

        setAttachedFileIdProof({
          file_name:
            applicantData.hasOwnProperty('fl_pass_front_name') &&
            applicantData.fl_pass_front_name
              ? applicantData.fl_pass_front_name
              : '',
          file_format:
            idTypeNri &&
            (idTypeNri.toLowerCase() == 'jpg' ||
              idTypeNri.toLowerCase() == 'jpeg' ||
              idTypeNri.toLowerCase() == 'png')
              ? 'image'
              : idTypeNri
              ? idTypeNri
              : '',
          file_size: '',
          file:
            applicantData.hasOwnProperty('fl_pass_front_url') &&
            applicantData.fl_pass_front_url
              ? applicantData.fl_pass_front_url
              : '',
          fileUploaded: 'no',
          fid:
            applicantData.hasOwnProperty('fl_pass_front_fid') &&
            applicantData.fl_pass_front_fid
              ? applicantData.fl_pass_front_fid
              : '',
          type: 'field_passport_front_page',
        });

        setAttachedFileAddressProof({
          file_name:
            applicantData.hasOwnProperty('fl_pass_back_name') &&
            applicantData.fl_pass_back_name
              ? applicantData.fl_pass_back_name
              : '',
          file_format:
            addressTypeNri &&
            (addressTypeNri.toLowerCase() == 'jpg' ||
              addressTypeNri.toLowerCase() == 'jpeg' ||
              addressTypeNri.toLowerCase() == 'png')
              ? 'image'
              : addressTypeNri
              ? addressTypeNri
              : '',
          file_size: '',
          file:
            applicantData.hasOwnProperty('fl_pass_back_url') &&
            applicantData.fl_pass_back_url
              ? applicantData.fl_pass_back_url
              : '',
          fileUploaded: 'no',
          fid:
            applicantData.hasOwnProperty('fl_pass_back_fid') &&
            applicantData.fl_pass_back_fid
              ? applicantData.fl_pass_back_fid
              : '',
          type: 'field_passport_back_page',
        });
      }
    }

    setInputs({
      ...inputs,
      ['name']: getInputValidationState({
        input: inputs['name'],
        value:
          storedUserData.first_name && storedUserData.last_name
            ? `${storedUserData.first_name} ${storedUserData.last_name}`
            : '',
      }),
      ['mobNo']: getInputValidationState({
        input: inputs['mobNo'],
        value: storedUserData.mob_no ? storedUserData.mob_no : '',
      }),
      ['email']: getInputValidationState({
        input: inputs['email'],
        value: storedUserData.email ? storedUserData.email : '',
      }),
      ['dob']: getInputValidationState({
        input: inputs['dob'],
        value: userDOB,
      }),
      ['house_no']: getInputValidationState({
        input: inputs['house_no'],
        value: perHouseNo,
      }),
      ['street']: getInputValidationState({
        input: inputs['street'],
        value: perStreet,
      }),
      ['locality']: getInputValidationState({
        input: inputs['locality'],
        value: perLocality,
      }),
      ['pincode']: {
        ...inputs['pincode'],
        type:
          defaultCountry.callingCodes === '+91' ? 'indPincode' : 'nriPincode',
        value: perPincode,
        errorLabel: validateInput({
          type:
            defaultCountry.callingCodes === '+91' ? 'indPincode' : 'nriPincode',
          value: perPincode,
        }),
        showErr: false,
      },
      ['city']: getInputValidationState({
        input: inputs['city'],
        value: perCity,
      }),
      ['state']: getInputValidationState({
        input: inputs['state'],
        value: perState,
      }),
      ['country']: getInputValidationState({
        input: inputs['country'],
        value: defaultCountry ? defaultCountry.name : '',
      }),
      ['comm_house_no']: getInputValidationState({
        input: inputs['house_no'],
        value: commHouseNo,
      }),
      ['comm_street']: getInputValidationState({
        input: inputs['street'],
        value: commStreet,
      }),
      ['comm_locality']: getInputValidationState({
        input: inputs['locality'],
        value: commLocality,
      }),
      ['comm_pincode']: {
        ...inputs['comm_pincode'],
        type:
          defaultCommCountry.callingCodes === '+91'
            ? 'indPincode'
            : 'nriPincode',
        value: commPincode,
        errorLabel: validateInput({
          type:
            defaultCommCountry.callingCodes === '+91'
              ? 'indPincode'
              : 'nriPincode',
          value: commPincode,
        }),
        showErr: false,
      },
      ['comm_city']: getInputValidationState({
        input: inputs['city'],
        value: commCity,
      }),
      ['comm_state']: getInputValidationState({
        input: inputs['state'],
        value: commState,
      }),
      ['comm_country']: getInputValidationState({
        input: inputs['comm_country'],
        value: defaultCommCountry ? defaultCommCountry.name : '',
      }),
      ['pan_number']: getInputValidationState({
        input: inputs['pan_number'],
        value: userPAN,
      }),
      ['passport_number']: getInputValidationState({
        input: inputs['passport_number'],
        value: userPassport,
      }),
      ['id_proof_doc']: getInputValidationState({
        input: inputs['id_proof_doc'],
        value: idProofFileType,
      }),
      ['address_proof_doc']: getInputValidationState({
        input: inputs['address_proof_doc'],
        value: addressProofFileType,
      }),
      ['purpose_of_purchase']: getInputValidationState({
        input: inputs['purpose_of_purchase'],
        value: purposeOfPurchaseData,
      }),
      ['id_proof']: getInputValidationState({
        input: inputs['id_proof'],
        value: idProofName,
      }),
      ['address_proof']: getInputValidationState({
        input: inputs['address_proof'],
        value: addressProofName,
      }),
    });
  };

  const onInputChange = (id, value) => {
    if (isSameAddress) {
      if (
        id === 'house_no' ||
        id === 'street' ||
        id === 'locality' ||
        id === 'pincode' ||
        id === 'city' ||
        id === 'state' ||
        id === 'country'
      ) {
        setInputs({
          ...inputs,
          [id]: getInputValidationState(
            {
              input: inputs[id],
              value,
            },
            true,
          ),
          [`comm_${id}`]: getInputValidationState(
            {
              input: inputs[id],
              value,
            },
            true,
          ),
        });
      } else {
        setInputs({
          ...inputs,
          [id]: getInputValidationState(
            {
              input: inputs[id],
              value: id === 'pan_number' ? value.toUpperCase() : value,
            },
            true,
          ),
        });
      }
    } else {
      setInputs({
        ...inputs,
        [id]: getInputValidationState(
          {
            input: inputs[id],
            value: id === 'pan_number' ? value.toUpperCase() : value,
          },
          true,
        ),
      });
    }
  };

  const getInputValidationState = ({input, value}, isShowError) => {
    return {
      ...input,
      value,
      errorLabel: input.optional
        ? null
        : validateInput({type: input.type, value}),
      showErr: isShowError ? isShowError : false,
    };
  };

  const validateInput = ({type, value}) => {
    const result = validatejs(
      {
        [type]: value,
      },
      {
        [type]: validationDictionary[type],
      },
    );

    if (result) {
      return result[type][0];
    }

    return null;
  };

  const getFormValidation = () => {
    const updatedInputs = {};

    for (const [key, input] of Object.entries(inputs)) {
      updatedInputs[key] = getInputValidationState(
        {
          input,
          value: input.value,
        },
        true,
      );
    }

    setInputs(updatedInputs);
  };

  const onSelectPermanentCountry = () => {
    setCountryModalForPermanent(true);
    setShowCountryModal(true);
  };

  const onSelectCommunicationCountry = () => {
    setCountryModalForPermanent(false);
    setShowCountryModal(true);
  };

  const onSelectCountry = (selectedCountry) => {
    // console.log('selectedCountry: ', selectedCountry);

    const inputValidationType =
      selectedCountry &&
      selectedCountry.callingCodes &&
      selectedCountry.callingCodes === '+91'
        ? 'indPincode'
        : 'nriPincode';

    if (countryModalForPermanent) {
      if (isSameAddress) {
        setPermanentCountry(selectedCountry);
        setCommunicationCountry(selectedCountry);

        setInputs({
          ...inputs,
          ['pincode']: {
            ...inputs['pincode'],
            type: inputValidationType,
            errorLabel: inputs['pincode'].optional
              ? null
              : validateInput({
                  type: inputValidationType,
                  value: inputs['pincode'].value,
                }),
            showErr: true,
          },
          ['comm_pincode']: {
            ...inputs['comm_pincode'],
            type: inputValidationType,
            errorLabel: inputs['comm_pincode'].optional
              ? null
              : validateInput({
                  type: inputValidationType,
                  value: inputs['comm_pincode'].value,
                }),
            showErr: true,
          },
          ['country']: getInputValidationState(
            {
              input: inputs['country'],
              value: selectedCountry.name,
            },
            true,
          ),
          ['comm_country']: getInputValidationState(
            {
              input: inputs['comm_country'],
              value: selectedCountry.name,
            },
            true,
          ),
        });
      } else {
        setPermanentCountry(selectedCountry);
        setInputs({
          ...inputs,
          ['pincode']: {
            ...inputs['pincode'],
            type: inputValidationType,

            showErr: true,
            errorLabel: inputs['pincode'].optional
              ? null
              : validateInput({
                  type: inputValidationType,
                  value: inputs['pincode'].value,
                }),
          },
          ['country']: getInputValidationState(
            {
              input: inputs['country'],
              value: selectedCountry.name,
            },
            true,
          ),
        });
      }
    } else {
      setCommunicationCountry(selectedCountry);
      setInputs({
        ...inputs,
        ['comm_pincode']: {
          ...inputs['comm_pincode'],
          type: inputValidationType,
          errorLabel: inputs['comm_pincode'].optional
            ? null
            : validateInput({
                type: inputValidationType,
                value: inputs['comm_pincode'].value,
              }),
          showErr: true,
        },
        ['comm_country']: getInputValidationState(
          {
            input: inputs['comm_country'],
            value: selectedCountry.name,
          },
          true,
        ),
      });
    }

    setShowCountryModal(false);
  };

  const onSelectSameAddress = (isSameAddress) => {
    if (isSameAddress) {
      setInputs({
        ...inputs,
        ['house_no']: getInputValidationState(
          {
            input: inputs['house_no'],
            value: inputs['house_no'].value,
          },
          true,
        ),
        ['street']: getInputValidationState(
          {
            input: inputs['street'],
            value: inputs['street'].value,
          },
          true,
        ),
        ['locality']: getInputValidationState(
          {
            input: inputs['locality'],
            value: inputs['locality'].value,
          },
          true,
        ),
        ['pincode']: getInputValidationState(
          {
            input: inputs['pincode'],
            value: inputs['pincode'].value,
          },
          true,
        ),
        ['city']: getInputValidationState(
          {
            input: inputs['city'],
            value: inputs['city'].value,
          },
          true,
        ),
        ['state']: getInputValidationState(
          {
            input: inputs['state'],
            value: inputs['state'].value,
          },
          true,
        ),
        ['country']: getInputValidationState(
          {
            input: inputs['country'],
            value: inputs['country'].value,
          },
          true,
        ),
      });

      if (
        inputs['house_no'].value &&
        inputs['street'].value &&
        inputs['locality'].value &&
        inputs['pincode'].value &&
        inputs['city'].value &&
        inputs['state'].value &&
        inputs['country'].value &&
        !inputs['house_no'].errorLabel &&
        !inputs['street'].errorLabel &&
        !inputs['locality'].errorLabel &&
        !inputs['pincode'].errorLabel &&
        !inputs['city'].errorLabel &&
        !inputs['state'].errorLabel &&
        !inputs['country'].errorLabel
      ) {
        setIsSameAddress(isSameAddress);

        setCommunicationCountry(inputs['country'].value);
        setInputs({
          ...inputs,
          ['comm_house_no']: getInputValidationState(
            {
              input: inputs['comm_house_no'],
              value: inputs['house_no'].value,
            },
            true,
          ),
          ['comm_street']: getInputValidationState(
            {
              input: inputs['comm_street'],
              value: inputs['street'].value,
            },
            true,
          ),
          ['comm_locality']: getInputValidationState(
            {
              input: inputs['comm_locality'],
              value: inputs['locality'].value,
            },
            true,
          ),
          ['comm_pincode']: getInputValidationState(
            {
              input: inputs['comm_pincode'],
              value: inputs['pincode'].value,
            },
            true,
          ),
          ['comm_city']: getInputValidationState(
            {
              input: inputs['comm_city'],
              value: inputs['city'].value,
            },
            true,
          ),
          ['comm_state']: getInputValidationState(
            {
              input: inputs['comm_state'],
              value: inputs['state'].value,
            },
            true,
          ),
          ['comm_country']: getInputValidationState(
            {
              input: inputs['comm_country'],
              value: inputs['country'].value,
            },
            true,
          ),
        });
      } else {
        appSnakBar.onShowSnakBar(
          'Please fill up the Permanent Address properly',
        );
      }
    } else {
      setIsSameAddress(isSameAddress);

      setCommunicationCountry(defaultCountry);
      setInputs({
        ...inputs,
        ['comm_house_no']: getInputValidationState({
          input: inputs['comm_house_no'],
          value: '',
          showErr: false,
        }),
        ['comm_street']: getInputValidationState({
          input: inputs['comm_street'],
          value: '',
          showErr: false,
        }),
        ['comm_locality']: getInputValidationState({
          input: inputs['comm_locality'],
          value: '',
          showErr: false,
        }),
        ['comm_pincode']: getInputValidationState({
          input: inputs['comm_pincode'],
          value: '',
          showErr: false,
        }),
        ['comm_city']: getInputValidationState({
          input: inputs['comm_city'],
          value: '',
          showErr: false,
        }),
        ['comm_state']: getInputValidationState({
          input: inputs['comm_state'],
          value: '',
          showErr: false,
        }),
        ['comm_country']: getInputValidationState({
          input: inputs['comm_country'],
          value: isNri() ? countryAfghanistan.name : defaultCountry.name,
          showErr: false,
        }),
      });
    }
  };

  const getIdProofDocType = () => {
    if (isNri()) {
      return idProofDocTypeNri;
    } else {
      return idProofDocTypeInd;
    }
  };

  const getAddressProofDocType = () => {
    if (isNri()) {
      return addressProofDocTypeNri;
    } else {
      return addressProofDocTypeInd;
    }
  };

  const onUploadFileSelect = (type, fileResponse) => {
    // console.log('onUploadFile: type: ', type);
    // console.log('onUploadFile: fileResponse: ', fileResponse);

    let fileName = '';

    let fileFormat = '';
    let fileSize = '';
    let file = '';
    let fileBase64 = '';

    if (fileResponse) {
      fileName = fileResponse.name
        ? fileResponse.name
        : fileResponse.uri
        ? fileResponse.uri.split('/').pop()
        : '';

      fileFormat =
        fileResponse.type && fileResponse.type == 'application/pdf'
          ? 'pdf'
          : 'image';

      fileSize = fileResponse.fileSize
        ? fileResponse.fileSize
        : fileResponse.size
        ? fileResponse.size
        : '';

      file = fileResponse.uri ? fileResponse.uri : '';
      fileBase64 = fileResponse.data ? fileResponse.data : '';

      if (fileName && fileFormat && fileSize && file && fileBase64) {
        if (type === 'idProof') {
          setAttachedFileIdProof({
            file_name: fileName,
            file_format: fileFormat,
            file_size: fileSize,
            file: file,
            fileBase64: fileBase64,
            fileUploaded: 'yes',
            fid: '',
            type: isNri() ? 'field_passport_front_page' : 'field_id_proof',
          });

          setInputs({
            ...inputs,
            ['id_proof']: getInputValidationState(
              {
                input: inputs['id_proof'],
                value: fileName,
              },
              true,
            ),
          });
        } else if (type === 'addressProof') {
          setAttachedFileAddressProof({
            file_name: fileName,
            file_format: fileFormat,
            file_size: fileSize,
            file: file,
            fileBase64: fileBase64,
            fileUploaded: 'yes',
            fid: '',
            type: isNri() ? 'field_passport_back_page' : 'field_address_proof',
          });

          setInputs({
            ...inputs,
            ['address_proof']: getInputValidationState(
              {
                input: inputs['address_proof'],
                value: fileName,
              },
              true,
            ),
          });
        }
      }
    }
  };

  /* ------------------------ Booking Form Functionality End ------------------------ */

  const onHandleBooking = () => {
    // console.log('onHandleBooking :: sourceProtection: ', sourceProtection);
    // console.log('onHandleBooking :: socialPreference: ', socialPreference);
    // console.log(
    //   'onHandleBooking :: sourceSelectedValue: ',
    //   sourceSelectedValue,
    // );

    if (
      bookingAmount.is_booking_amount_error &&
      bookingAmount.payment_method == 'custom'
    ) {
      onExpandSelect('bookingAmount', true);
      appSnakBar.onShowSnakBar(getCustomAmountInputError(), 'LONG');
      return;
    }

    let isSocialPreferenceError = false;

    if (sourceSelectedValue) {
      if (
        socialPreference.is_referred_by_channel_partner &&
        socialPreference.channel_partner_name.trim() == ''
      ) {
        isSocialPreferenceError = true;
      } else {
        if (
          socialPreference.is_referred_by_customer &&
          socialPreference.customer_name.trim() == ''
        ) {
          isSocialPreferenceError = true;
        } else {
          if (
            socialPreference.is_existing_customer &&
            socialPreference.booked_project_name.trim() == ''
          ) {
            isSocialPreferenceError = true;
          } else {
            if (
              socialPreference.is_how_did_you_know == true &&
              (socialPreference.how_did_you_know.trim() == '' ||
                socialPreference.how_did_you_know.trim() == 'Select')
            ) {
              isSocialPreferenceError = true;
            } else {
              isSocialPreferenceError = false;
            }
          }
        }
      }
    } else {
      isSocialPreferenceError = true;
    }

    if (isSocialPreferenceError) {
      setSourceProtectionError(true);
      appSnakBar.onShowSnakBar(
        `Please fill the 'How did you hear about us?' section`,
        'LONG',
      );
      onExpandSelect('bookingSource', true);
      return;
    } else {
      setSourceProtectionError(false);
    }

    getFormValidation();
    // console.log('onHandleBookingData: ', inputs);

    // console.log('attachedFileIdProof: ', attachedFileIdProof);
    // console.log('attachedFileAddressProof: ', attachedFileAddressProof);

    if (
      inputs['name'].value &&
      inputs['mobNo'].value &&
      inputs['email'].value &&
      inputs['dob'].value &&
      inputs['house_no'].value &&
      inputs['street'].value &&
      inputs['locality'].value &&
      inputs['pincode'].value &&
      inputs['city'].value &&
      inputs['state'].value &&
      inputs['country'].value &&
      inputs['comm_house_no'].value &&
      inputs['comm_street'].value &&
      inputs['comm_locality'].value &&
      inputs['comm_pincode'].value &&
      inputs['comm_city'].value &&
      inputs['comm_state'].value &&
      inputs['comm_country'].value &&
      inputs['id_proof_doc'].value &&
      inputs['address_proof_doc'].value &&
      inputs['purpose_of_purchase'].value &&
      inputs['id_proof'].value &&
      inputs['address_proof'].value &&
      !inputs['name'].errorLabel &&
      !inputs['mobNo'].errorLabel &&
      !inputs['email'].errorLabel &&
      !inputs['dob'].errorLabel &&
      !inputs['house_no'].errorLabel &&
      !inputs['street'].errorLabel &&
      !inputs['locality'].errorLabel &&
      !inputs['pincode'].errorLabel &&
      !inputs['city'].errorLabel &&
      !inputs['state'].errorLabel &&
      !inputs['country'].errorLabel &&
      !inputs['comm_house_no'].errorLabel &&
      !inputs['comm_street'].errorLabel &&
      !inputs['comm_locality'].errorLabel &&
      !inputs['comm_pincode'].errorLabel &&
      !inputs['comm_city'].errorLabel &&
      !inputs['comm_state'].errorLabel &&
      !inputs['comm_country'].errorLabel &&
      !inputs['id_proof_doc'].errorLabel &&
      !inputs['address_proof_doc'].errorLabel &&
      !inputs['purpose_of_purchase'].errorLabel &&
      !inputs['id_proof'].errorLabel &&
      !inputs['address_proof'].errorLabel &&
      ((!isNri() &&
        inputs['pan_number'].value &&
        !inputs['pan_number'].errorLabel) ||
        (isNri() &&
          inputs['passport_number'].value &&
          !inputs['passport_number'].errorLabel)) &&
      Object.keys(attachedFileIdProof).length > 0 &&
      Object.keys(attachedFileAddressProof).length > 0
    ) {
      if (!isTermsSelected) {
        appSnakBar.onShowSnakBar(
          `Please accept the Terms & Conditions`,
          'LONG',
        );
        return;
      }
      checkInventoryStatus();
    } else {
      appSnakBar.onShowSnakBar(`Please fill the booking form`, 'LONG');
      onExpandSelect('applicantDetails', true);
    }
  };

  const checkInventoryStatus = async () => {
    try {
      setIsLoading(true);

      const requestData = {
        user_id: userId,
        inventory_id: inventoryData.id,
      };
      const inventoryStatusResult = await apiInventoryMLP.apiCheckInventoryStatus(
        requestData,
      );

      if (inventoryStatusResult.data) {
        if (
          inventoryStatusResult.data.status &&
          inventoryStatusResult.data.status == '200'
        ) {
          setBookingData();
        } else if (
          inventoryStatusResult.data.status &&
          inventoryStatusResult.data.status == '500'
        ) {
          setIsLoading(false);
          displayErrorMsg(
            inventoryStatusResult.data.msg
              ? inventoryStatusResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            true,
          );
        } else {
          setIsLoading(false);
          displayErrorMsg(
            inventoryStatusResult.data.msg
              ? inventoryStatusResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            true,
          );
        }
      } else {
        setIsLoading(false);
        displayErrorMsg(appConstant.appMessage.APP_GENERIC_ERROR, false);
      }
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      displayErrorMsg(appConstant.appMessage.APP_GENERIC_ERROR, false);
    }
  };

  const setBookingData = () => {
    let source_protection_source = '';
    let source_protection_answer = '';
    let source_protection_answer_id =
      socialPreference.source_protection_answer_id;
    let is_source_custom_value = socialPreference.is_source_custom_value;

    if (socialPreference.is_referred_by_channel_partner) {
      source_protection_source = 'Partner';
      source_protection_answer = socialPreference.channel_partner_name;
    } else if (socialPreference.is_referred_by_customer) {
      source_protection_source = 'Referral';
      source_protection_answer = socialPreference.customer_name;
    } else if (socialPreference.is_existing_customer) {
      source_protection_source = 'Loyalty';
      source_protection_answer = socialPreference.booked_project_name;
    } else if (socialPreference.is_how_did_you_know) {
      source_protection_source = 'Direct';
      source_protection_answer = socialPreference.how_did_you_know;
      source_protection_answer_id = '';
    }

    const bookingEnquiryId =
      bookingPropertyInfo &&
      bookingPropertyInfo.data &&
      bookingPropertyInfo.data.enquiry_id
        ? bookingPropertyInfo.data.enquiry_id
        : '';

    const bookingEnquiryTitle =
      bookingPropertyInfo &&
      bookingPropertyInfo.data &&
      bookingPropertyInfo.data.enquiry_name
        ? bookingPropertyInfo.data.enquiry_name
        : '';

    const kycDocumentArr = [];
    if (attachedFileIdProof.fileUploaded == 'yes') {
      kycDocumentArr.push({
        ...attachedFileIdProof,
        file: attachedFileIdProof.fileBase64,
        fileBase64: '',
      });
    } else {
      kycDocumentArr.push(attachedFileIdProof);
    }
    if (attachedFileAddressProof.fileUploaded == 'yes') {
      kycDocumentArr.push({
        ...attachedFileAddressProof,
        file: attachedFileAddressProof.fileBase64,
        fileBase64: '',
      });
    } else {
      kycDocumentArr.push(attachedFileAddressProof);
    }

    const selectedPlan = paymentPlanData.data.curl_response.payment_plans.find(
      (item) => item.plan_selected == '1',
    );
    // console.log('selectedPlan:All ', paymentPlanData.data.curl_response.payment_plans);
    // console.log('selectedPlan: ', selectedPlan);

    const requestData = {
      nationality: isNri() ? 'NRI' : 'Indian',
      name: inputs['name'].value,
      mobile: inputs['mobNo'].value,
      email: inputs['email'].value,
      dob: inputs['dob'].value,
      permanentAddress: inputs['house_no'].value,
      street: inputs['street'].value,
      locality: inputs['locality'].value,
      country: inputs['country'].value,
      state: inputs['state'].value,
      city: inputs['city'].value,
      pincode: inputs['pincode'].value,
      communicationHouseNo: inputs['comm_house_no'].value,
      communicationStreet: inputs['comm_street'].value,
      communicationLocality: inputs['comm_locality'].value,
      communicationCountry: inputs['comm_country'].value,
      communicationState: inputs['comm_state'].value,
      communicationCity: inputs['comm_city'].value,
      communicationPincode: inputs['comm_pincode'].value,
      pan: inputs['pan_number'].value,
      passport: inputs['passport_number'].value,
      purposeOfPurchase: inputs['purpose_of_purchase'].value,
      id_proof_file_type: inputs['id_proof_doc'].value,
      address_proof_file_type: inputs['address_proof_doc'].value,
      device_id: deviceUniqueId,
      inventory_id: inventoryData.id,
      project_id: projectId,
      user_id: userId,
      disc_amt:
        promoCodeData && promoCodeData.disc_amt ? promoCodeData.disc_amt : '',
      disc_upto_limit:
        promoCodeData && promoCodeData.disc_upto_limit
          ? promoCodeData.disc_upto_limit
          : '',
      disc_type:
        promoCodeData && promoCodeData.disc_type ? promoCodeData.disc_type : '',
      disc_percentage:
        promoCodeData && promoCodeData.disc_percentage
          ? promoCodeData.disc_percentage
          : '',
      promo_id:
        promoCodeData && promoCodeData.promo_id ? promoCodeData.promo_id : '',
      promo_code:
        promoCodeData && promoCodeData.promo_code
          ? promoCodeData.promo_code
          : '',
      payment_plan_id:
        selectedPlan && selectedPlan.payment_plan_id
          ? selectedPlan.payment_plan_id
          : '',
      payment_plan_name:
        selectedPlan && selectedPlan.payment_plan_name
          ? selectedPlan.payment_plan_name
          : '',
      booking_enquiry_id: bookingEnquiryId,
      booking_enquiry_title: bookingEnquiryTitle,
      source_protection_source: source_protection_source,
      source_protection_answer: source_protection_answer,
      source_protection_answer_id: source_protection_answer_id,
      is_source_custom_value: is_source_custom_value,
      kyc_document: kycDocumentArr,
      foyrTxnID: foyrTxnID ? foyrTxnID : '',
      field_inventory_booking_source: inventoryPreference
        ? inventoryPreference
        : '',
      min_amount: bookingAmount.min_amount,
      max_amount: bookingAmount.max_amount,
      amount_paid:
        bookingAmount.booking_amount_radio &&
        bookingAmount.payment_method === 'custom'
          ? bookingAmount.custom_amount
          : bookingAmount.max_amount,
      remaining_amount:
        bookingAmount.booking_amount_radio &&
        bookingAmount.payment_method === 'custom'
          ? getRemainingBookingAmount()
          : '0',
      payment_method: bookingAmount.payment_method,
    };
    // console.log('onHandleBooking :: promoCodeData: ', promoCodeData);
    // console.log('onHandleBooking :: Request Data: ', requestData);

    postBookingDetails(requestData);
  };

  const [addBookingDataDetails, setAddBookingDataDetails] = useState({});
  const [bookingId, setBookingId] = useState('');
  const [showWebview, setShowWebview] = useState(false);
  const [paymentUrl, setPaymentUrl] = useState('');

  const postBookingDetails = async (request) => {
    try {
      setIsLoading(true);

      const jsonRequest = request;

      if (addBookingDataDetails.booking_id) {
        jsonRequest['booking_id'] = addBookingDataDetails.booking_id;
      }

      const addBookingSuccessResult = await apiInvBookingJourney.apiBookingAddData(
        jsonRequest,
      );

      setIsLoading(false);

      if (addBookingSuccessResult.data) {
        if (
          addBookingSuccessResult.data.status &&
          addBookingSuccessResult.data.status == '200'
        ) {
          if (addBookingSuccessResult.data.booking_id) {
            setBookingId(addBookingSuccessResult.data.booking_id);
            let bookingId = addBookingSuccessResult.data.booking_id;
            let bookingNo = '';
            let bookingAmount = '';

            if (addBookingSuccessResult.data.booking_no) {
              bookingNo = addBookingSuccessResult.data.booking_no;
            } else {
              if (this.bookingAmountDetails.booking_no) {
                bookingNo = this.bookingAmountDetails.booking_no;
              }
            }

            if (addBookingSuccessResult.data.amount) {
              bookingAmount = addBookingSuccessResult.data.amount;
            } else {
              if (this.bookingAmountDetails.res_booking_amount) {
                bookingAmount = this.bookingAmountDetails.res_booking_amount;
              }
            }

            if (addBookingSuccessResult.data.presale_payment_id) {
              setAddBookingDataDetails({
                bookingId: '',
                bookingNo: '',
                bookingAmount: '',
              });

              if (
                bookingId &&
                bookingId != '' &&
                bookingNo &&
                bookingNo != '' &&
                bookingAmount &&
                bookingAmount != ''
              ) {
                openPaymentPage(bookingId, bookingNo, bookingAmount);
                onExpandDeSelectAll();
              } else {
                displayErrorMsg(
                  appConstant.appMessage.APP_GENERIC_ERROR,
                  false,
                );
              }
            } else {
              bookingId = addBookingSuccessResult.data.booking_id
                ? addBookingSuccessResult.data.booking_id
                : '';
              bookingNo = addBookingSuccessResult.data.booking_no
                ? addBookingSuccessResult.data.booking_no
                : '';
              bookingAmount = addBookingSuccessResult.data.amount
                ? addBookingSuccessResult.data.amount
                : '';

              setAddBookingDataDetails({bookingId, bookingNo, bookingAmount});

              displayErrorMsg(appConstant.appMessage.APP_GENERIC_ERROR, false);
            }
          } else {
            displayErrorMsg(appConstant.appMessage.APP_GENERIC_ERROR, false);
          }

          // console.log('addBookingSuccessResult: ', addBookingSuccessResult)
        } else if (
          addBookingSuccessResult.data.status &&
          (addBookingSuccessResult.data.status == '505' ||
            addBookingSuccessResult.data.status == '506')
        ) {
          displayErrorMsg(
            addBookingSuccessResult.data.msg
              ? addBookingSuccessResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            true,
          );
        } else {
          displayErrorMsg(
            addBookingSuccessResult.data.msg
              ? addBookingSuccessResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            false,
          );
        }
      } else {
        displayErrorMsg(appConstant.appMessage.APP_GENERIC_ERROR, false);
      }
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      displayErrorMsg(appConstant.appMessage.APP_GENERIC_ERROR, false);
    }
  };

  const openPaymentPage = (bookingId, bookingNo, bookingAmount) => {
    const selectedPlan = paymentPlanData.data.curl_response.payment_plans.find(
      (item) => item.plan_selected == '1',
    );
    const paymentPlanId =
      selectedPlan && selectedPlan.payment_plan_id
        ? selectedPlan.payment_plan_id
        : '';

    const sessionId =
      loginUserData && loginUserData.sessionId ? loginUserData.sessionId : '';

    const urlParams = `?auth=${sessionId}&user_id=${userId}&booking_id=${bookingId}&booking_no=${bookingNo}&amount=${bookingAmount}&payment_plan_id=${paymentPlanId}`;

    const paymentURL = `${appConstant.buildInstance.baseUrl}V2/api_payment_redirect${urlParams}`;

    console.log('Payment URL: ' + paymentURL);

    setPaymentUrl(paymentURL);
    setShowWebview(true);
  };

  const showPaymentFaiedAlert = (alertMsg) => {
    Alert.alert(
      appConstant.appMessage.NO_BOOKING_AVAILABLE_ALERT_TITLE,
      alertMsg,
      [
        {
          text:
            appConstant.appMessage.NO_BOOKING_AVAILABLE_ALERT_POSITIVE_BUTTON,
          onPress: () => console.log('OK Pressed'),
        },
      ],
      {cancelable: false},
    );
  };

  const getBookingStatus = async (_bookingId, isSuccess) => {
    try {
      setIsLoading(true);

      const requestData = {
        booking_id: _bookingId,
      };
      const bookingSuccessResult = await apiInvBookingJourney.apiCheckBookingStatus(
        requestData,
      );

      setIsLoading(false);

      if (bookingSuccessResult.data) {
        if (
          bookingSuccessResult.data.status &&
          bookingSuccessResult.data.status == '200' &&
          bookingSuccessResult.data.booking_status &&
          bookingSuccessResult.data.booking_status == 'Completed'
        ) {
          if (isSuccess) {
            // displayErrorMsg(
            //   bookingSuccessResult.data.msg
            //     ? bookingSuccessResult.data.msg
            //     : 'Payment Successful',
            //   false,
            // );

            setBookingId('');
            dispatch(inventoryMPLAction.resetInventoryList());

            navigation.dispatch(
              StackActions.replace('BookingConfirmation', {
                projectId: projectId,
                bookingId: _bookingId,
                userId: userId,
              }),
            );
          } else {
            showPaymentFaiedAlert(
              bookingSuccessResult.data.msg
                ? bookingSuccessResult.data.msg
                : 'Payment Failed',
            );
          }
        } else {
          showPaymentFaiedAlert(
            bookingSuccessResult.data.msg
              ? bookingSuccessResult.data.msg
              : 'Payment Failed',
          );
        }
      } else {
        displayErrorMsg(appConstant.appMessage.APP_GENERIC_ERROR, false);
      }
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      displayErrorMsg(appConstant.appMessage.APP_GENERIC_ERROR, false);
    }
  };

  const onCancelBooking = () => {
    navigation.goBack();

    // navigation.navigate('BookingConfirmation', {
    //   projectId: projectId,
    //   bookingId: '1114711',
    //   userId: userId,
    // })
  };

  const onExpandSelect = (type, isSelected) => {
    const selectedMenuTimeout = {...selectedExpandMenu};

    if (type === 'propertyDetails') {
      selectedMenuTimeout.propertyDetails = isSelected;
    } else if (type === 'paymentDetails') {
      selectedMenuTimeout.paymentDetails = isSelected;
    } else if (type === 'costSheetDetails') {
      selectedMenuTimeout.costSheetDetails = isSelected;
    } else if (type === 'bookingAmount') {
      selectedMenuTimeout.bookingAmount = isSelected;
    } else if (type === 'bookingSource') {
      selectedMenuTimeout.bookingSource = isSelected;
    } else if (type === 'applicantDetails') {
      selectedMenuTimeout.applicantDetails = isSelected;
    } else {
      selectedMenuTimeout.propertyDetails = false;
      selectedMenuTimeout.paymentDetails = false;
      selectedMenuTimeout.costSheetDetails = false;
      selectedMenuTimeout.bookingAmount = false;
      selectedMenuTimeout.bookingSource = false;
      selectedMenuTimeout.applicantDetails = false;
    }

    setSelectedExpandMenu(selectedMenuTimeout);
  };

  const onExpandDeSelectAll = () => {
    const selectedMenuTimeout = {...selectedExpandMenu};

    selectedMenuTimeout.propertyDetails = false;
    selectedMenuTimeout.paymentDetails = false;
    selectedMenuTimeout.costSheetDetails = false;
    selectedMenuTimeout.bookingAmount = false;
    selectedMenuTimeout.bookingSource = false;
    selectedMenuTimeout.applicantDetails = false;

    setSelectedExpandMenu(selectedMenuTimeout);
  };

  const onCloseWebView = () => {
    setPaymentUrl('');
    setShowWebview(false);
  };

  const handleWebViewNavigationStateChange = ({url}) => {
    console.log('handleWebViewNavigationStateChange newNavState : ', url);
    if (
      url ==
      appConstant.buildInstance.baseUrl + 'api_eoi_payment_success_final.json'
    ) {
      onCloseWebView();
      getBookingStatus(bookingId, true);
    } else if (url == appConstant.buildInstance.baseUrl + 'ccavCancelHandler') {
      onCloseWebView();
      getBookingStatus(bookingId, false);
    }
  };

  const ActivityIndicatorElement = () => {
    return (
      <ActivityIndicator
        color={colors.jaguar}
        size="small"
        style={styles.activityIndicatorStyle}
      />
    );
  };

  useBackHandler(() => {
    if (showWebview) {
      setPaymentUrl('');
      setShowWebview(false);
      return true;
    }
    return false;
  });

  const onShowTermsModal = async () => {
    try {
      setIsLoading(true);

      const termsDataResult = await apiInvBookingJourney.apiBookingTerms(
        projectId,
      );

      setIsLoading(false);

      if (
        termsDataResult.data &&
        termsDataResult.data[0] &&
        termsDataResult.data[0].page_content &&
        termsDataResult.data[0].page_content != ''
      ) {
        setBookingTermsData(termsDataResult.data[0].page_content);
        setTermsModalVisible(true);
      } else {
        displayErrorMsg(appConstant.appMessage.APP_GENERIC_ERROR, false);
      }
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      displayErrorMsg(appConstant.appMessage.APP_GENERIC_ERROR, false);
    }
  };

  const onHandleRetryBtn = () => {
    if (
      userId &&
      projectId &&
      deviceUniqueId &&
      inventoryData.id &&
      towerData.id
    ) {
      getBookingPropertyInfo(
        userId,
        projectId,
        deviceUniqueId,
        inventoryData.id,
        towerData.id,
      );
    }
  };

  const onHandleTermsSelection = (isSelected) => {
    setIsTermsSelected(isSelected);

    if (isSelected) {
      loadLocation();
    }
  };

  const loadLocation = () => {
    const storedUserData = loginUserData.data;
    const requestData = {
      deviceId: deviceUniqueId,
      devicePlatform: Platform.OS,
      userId: userId,
      customerName:
        storedUserData && storedUserData.first_name && storedUserData.last_name
          ? `${storedUserData.first_name} ${storedUserData.last_name}`
          : '',
      mobileNo:
        storedUserData && storedUserData.mob_no ? storedUserData.mob_no : '',
      emailId:
        storedUserData && storedUserData.email ? storedUserData.email : '',
      moduleName: 'Booking Details',
    };

    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
      .then(({latitude, longitude}) => {
        appUserAuthorization.validateUserAuthorization({
          ...requestData,
          latiTude: latitude,
          longiTude: longitude,
        });
      })
      .catch((error) => {
        appUserAuthorization.validateUserAuthorization(requestData);
      });
  };

  const onCloseAllDropdown = () => {
    if (dropSource && dropSource.current) {
      dropSource.current.close();
    }
    if (dropPurpose && dropPurpose.current) {
      dropPurpose.current.close();
    }
    if (dropDocumentId && dropDocumentId.current) {
      dropDocumentId.current.close();
    }
    if (dropDocumentAddress && dropDocumentAddress.current) {
      dropDocumentAddress.current.close();
    }
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      {showWebview && paymentUrl ? (
        <WebView
          originWhitelist={['*']}
          source={{
            uri: paymentUrl,
          }}
          style={{flex: 1}}
          scalesPageToFit
          javaScriptEnabledAndroid={true}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          mediaPlaybackRequiresUserAction={true}
          startInLoadingState={true}
          renderLoading={ActivityIndicatorElement}
          onNavigationStateChange={handleWebViewNavigationStateChange}
        />
      ) : null}

      {!showWebview && <AppHeader />}

      {!showWebview && (
        <ScrollView ref={scrollRef}>
          <TouchableWithoutFeedback onPress={onCloseAllDropdown}>
            <View style={styles.container}>
              <AppTextBold style={styles.pageTitle}>
                BOOKING DETAILS
              </AppTextBold>

              {selectedExpandMenu.propertyDetails ? (
                <BookingPropertyComponent
                  propertyDetails={propertyDetails}
                  onPress={() => onExpandSelect('propertyDetails', false)}
                />
              ) : (
                <AppExpandButton
                  title="Property Details"
                  onPress={() => {
                    onExpandSelect('propertyDetails', true);
                  }}
                />
              )}

              {selectedExpandMenu.paymentDetails ? (
                <PaymentPlanComponent
                  data={paymentPlanData}
                  onSelectPaymentPlan={(plan) =>
                    onSelectPaymentPlan(plan, false)
                  }
                  onPress={() => onExpandSelect('paymentDetails', false)}
                  onPressInfo={onPressPaymentPlanInfo}
                  onPressPaymentPlanDetails={(paymentPlan) =>
                    onPressPaymentPlanDetails(paymentPlan)
                  }
                />
              ) : (
                <AppExpandButton
                  title="Payment Plan"
                  onPress={() => {
                    onExpandSelect('paymentDetails', true);
                  }}
                />
              )}

              {selectedExpandMenu.costSheetDetails ? (
                <CostSheetComponent
                  data={costSheetData.data}
                  onPress={() => onExpandSelect('costSheetDetails', false)}
                  showApplyCoupon={true}
                  isCouponApplied={isCouponApplied}
                  onCouponApplied={(couponCode) =>
                    onHandleAppliedCoupon(couponCode)
                  }
                  promoCodeData={promoCodeData}
                  onDownloadCostSheet={() => onDownloadCostSheet()}
                  isShowDisclaimer={
                    costSheetData && costSheetData.sdr_status ? false : true
                  }
                />
              ) : (
                <AppExpandButton
                  title="Cost Sheet Details"
                  onPress={() => {
                    onExpandSelect('costSheetDetails', true);
                  }}
                />
              )}

              {selectedExpandMenu.bookingAmount ? (
                <BookingAmountComponent
                  onPress={() => onExpandSelect('bookingAmount', false)}
                  bookingAmountData={bookingAmount}
                  customAmountInputError={getCustomAmountInputError()}
                  invalidCustomBookingAmount={invalidCustomBookingAmount}
                  remainingBookingAmount={getRemainingBookingAmount()}
                  customAmountInputPlacehlder={getCustomAmountInputPlacehlder()}
                  onClickRadioButton={(type) => onClickBookingAmountType(type)}
                  onChangePriceInput={(value) =>
                    bookingCustomAmountValiation(value)
                  }
                />
              ) : (
                <AppExpandButton
                  title="Booking Amount"
                  onPress={() => {
                    onExpandSelect('bookingAmount', true);
                  }}
                />
              )}

              {selectedExpandMenu.bookingSource ? (
                <BookingSourceComponent
                  onPress={() => onExpandSelect('bookingSource', false)}
                  sourceProtectionData={sourceProtection}
                  bookingSource={bookingSource}
                  onClickRadioItem={onClickRadioItem}
                  sourceSelectedValue={sourceSelectedValue}
                  sourcePartner={sourcePartner}
                  sourceReferral={sourceReferral}
                  sourceLoyalty={sourceLoyalty}
                  sourceDirect={sourceDirect}
                  changeSourceSelection={onChangeSourceSelection}
                  selectedValue={getSourceSelectedValue()}
                  disabledFiled={getSourceDisabledValue()}
                  isShowError={sourceProtectionError}
                  setDropSource={(instance) => {
                    dropSource.current = instance;
                  }}
                />
              ) : (
                <AppExpandButton
                  title="How did you hear about us?"
                  onPress={() => {
                    onExpandSelect('bookingSource', true);
                  }}
                />
              )}

              {selectedExpandMenu.applicantDetails ? (
                <PrimaryApplicant
                  fieldInputs={inputs}
                  onInputChange={onInputChange}
                  onPress={() => onExpandSelect('applicantDetails', false)}
                  onSelectPermanentCountry={onSelectPermanentCountry}
                  onSelectCommunicationCountry={onSelectCommunicationCountry}
                  onSelectSameAddress={onSelectSameAddress}
                  isSameAddress={isSameAddress}
                  purposeOfPurchase={purposeOfPurchase}
                  idProofDocType={getIdProofDocType()}
                  addressProofDocType={getAddressProofDocType()}
                  isNriCustomer={isNri()}
                  isDobEditable={isDobEditable}
                  attachedFileIdProof={attachedFileIdProof}
                  attachedFileAddressProof={attachedFileAddressProof}
                  downloadApplicantDocument={onDownloadApplicantDocument}
                  onUploadFileSelect={(type, fileResponse) =>
                    onUploadFileSelect(type, fileResponse)
                  }
                  setDropPurpose={(instance) => {
                    dropPurpose.current = instance;
                  }}
                  setDropDocumentId={(instance) => {
                    dropDocumentId.current = instance;
                  }}
                  setDropDocumentAddress={(instance) => {
                    dropDocumentAddress.current = instance;
                  }}
                />
              ) : (
                <AppExpandButton
                  title="Primary Applicant Details"
                  onPress={() => {
                    onExpandSelect('applicantDetails', true);
                  }}
                />
              )}

              <View style={styles.bottomContainer}>
                <AppText style={styles.txtMsg}>
                  All the information provided above will be used to process
                  your booking and will not be changed later. In case, any
                  details like Primary Applicant’s info and Source details are
                  looking incorrect, kindly get in touch with us before going
                  ahead with the transaction.
                </AppText>
                <AppText style={styles.txtMsg}>
                  Additional applicants can be added later at the time of
                  booking process completion along with payment of balance
                  Booking Amount, KYC documentation and Application Form
                  signing. Final booking and allotment is subject to completion
                  of these offline steps.
                </AppText>
                <AppText style={styles.txtMsg}>
                  Once you move to the payment gateway, the selected unit will
                  be kept on hold for 15 minutes to complete the transaction. In
                  case, the transaction is not successfully completed within 15
                  minutes, the selected unit will be released for other
                  customers to book.
                </AppText>

                <View style={styles.checkboxContainer}>
                  <TouchableWithoutFeedback
                    onPress={() => {
                      onHandleTermsSelection(!isTermsSelected);
                    }}>
                    <Image
                      style={styles.checkbox}
                      source={
                        isTermsSelected == true
                          ? require('./../assets/images/checked-icon.png')
                          : require('./../assets/images/unchecked-icon.png')
                      }
                    />
                  </TouchableWithoutFeedback>

                  <AppText style={styles.checkboxLabel}>
                    I accept the &nbsp;
                    <AppClickableText
                      onPress={onShowTermsModal}
                      style={{
                        fontSize: appFonts.largeFontSize,
                        textDecorationLine: 'underline',
                      }}>
                      Terms &amp; Conditions
                    </AppClickableText>
                  </AppText>
                </View>

                <AppButton
                  color="primary"
                  textColor="secondary"
                  title="CANCEL"
                  onPress={onCancelBooking}
                />

                <AppButton
                  title="PROCEED TO PAYMENT"
                  onPress={onHandleBooking}
                />
              </View>
            </View>
          </TouchableWithoutFeedback>
        </ScrollView>
      )}

      {!showWebview && <AppFooter activePage={0} isPostSales={false} />}

      <AppOverlayLoader isLoading={isLoading || isSourceLoading} />

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <PaymentDetailModalScreen
          onPress={() => {
            setModalVisible(!modalVisible);
          }}
          paymentPlanDetails={selectedPaymentPlanDetails}
          cancel={true}
        />
      </Modal>

      <Modal
        animationType="fade"
        transparent={true}
        visible={showCountryModal}
        onRequestClose={() => {
          setShowCountryModal(false);
        }}>
        <CountrySelectModalScreen
          onCancelPress={() => {
            setShowCountryModal(false);
          }}
          searchData={''}
          onSelectCountry={(selectedCountry) => {
            onSelectCountry(selectedCountry);
          }}
        />
      </Modal>

      <Modal
        animationType="fade"
        transparent={true}
        visible={termsModalVisible}
        onRequestClose={() => {
          setTermsModalVisible(!termsModalVisible);
        }}>
        <BookingTermConditionModalScreen
          onCancel={() => {
            setTermsModalVisible(!termsModalVisible);
          }}
          onContinue={() => {
            setTermsModalVisible(!termsModalVisible);
            if (!isTermsSelected) {
              onHandleTermsSelection(true);
            }
          }}
          bookingTermsData={bookingTermsData}
        />
      </Modal>

      {/* && progress && progress > 0 */}
      {showProgress ? <AppProgressBar progress={progress} /> : null}
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 10,
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    paddingHorizontal: 25,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: '7%',
    textTransform: 'uppercase',
  },
  viewContainer: {
    width: windowWidth,
    paddingBottom: 20,
    marginBottom: 25,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  titleLine: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  value: {
    fontSize: appFonts.largeBold,
    color: colors.expandText,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  icon: {
    width: 16,
    height: 16,
  },
  layoutImg: {
    width: '90%',
    marginVertical: 20,
  },
  detailsLine: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
  },
  checkboxContainer: {
    flexDirection: 'row',
    paddingBottom: '5%',
    alignItems: 'center',
  },
  checkboxLabel: {
    fontSize: appFonts.largeFontSize,
    color: colors.jaguar,
    margin: 8,
    fontFamily: appFonts.SourceSansProRegular,
  },
  checkbox: {height: 24, width: 24},
  bottomContainer: {padding: 24},
  txtMsg: {
    fontSize: appFonts.normalFontSize,
    marginBottom: 20,
    lineHeight: 20,
    textAlign: 'justify',
  },
  activityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default BookingScreen;
