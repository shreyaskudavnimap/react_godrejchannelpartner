import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import Screen from '../components/Screen';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppButton from '../components/ui/AppButton';
import AppFooter from '../components/ui/AppFooter';

import colors from '../config/colors';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';

import apiInvBookingJourney from '../api/apiInvBookingJourney';

import appSnakBar from '../utility/appSnakBar';
import appFonts from '../config/appFonts';
import {StackActions} from '@react-navigation/native';
import appConstant from '../utility/appConstant';
import Orientation from 'react-native-orientation';

const BookingConfirmation = ({navigation, route}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [bookingAmount, setBookingAmount] = useState('');

  const routeParams = route.params;
  const projectId =
    routeParams && routeParams.projectId ? routeParams.projectId : '';
  const bookingId =
    routeParams && routeParams.bookingId ? routeParams.bookingId : '';
  const userId = routeParams && routeParams.userId ? routeParams.userId : '';

  const goToSiteVisitScreen = () => {
    // navigation.navigate('PresaleVisitScreen', {projectId: projectId});
    navigation.dispatch(
      StackActions.replace('PresaleVisitScreen', {projectId: projectId}),
    );
  };

  const goToHomeScreen = () => {
    // navigation.navigate('GodrejHome');
    navigation.dispatch(StackActions.replace('GodrejHome'));
  };

  const goToBookingDetailsScreen = () => {
    // navigation.navigate('BookingDetailsScreen', {
    //   projectId: projectId,
    //   bookingId: bookingId,
    //   userId: userId,
    // });
    navigation.dispatch(
      StackActions.replace('BookingDetailsScreen', {
        projectId: projectId,
        bookingId: bookingId,
        userId: userId,
      }),
    );
  };

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);


  useEffect(() => {
    if (projectId && bookingId && userId) {
      getBookingConfirmationData(bookingId, userId);
    } else {
      navigation.goBack();
    }
  }, []);

  const getBookingConfirmationData = async (bookingId, userId) => {
    try {
      setIsLoading(true);

      const requestData = {
        booking_id: bookingId,
        user_id: userId,
      };
      const bookingConfirmationResult = await apiInvBookingJourney.apiBookingConfirmationData(
        requestData,
      );

      setIsLoading(false);

      if (bookingConfirmationResult.data) {
        if (
          bookingConfirmationResult.data.status &&
          bookingConfirmationResult.data.status == '200'
        ) {
          setBookingAmount(
            bookingConfirmationResult.data.data &&
              bookingConfirmationResult.data.data.booking_amount
              ? bookingConfirmationResult.data.data.booking_amount
              : '',
          );
        } else {
          appSnakBar.onShowSnakBar(
            bookingConfirmationResult.data.msg
              ? bookingConfirmationResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      } else {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const onHandleRetryBtn = () => {
    if (projectId && bookingId && userId) {
      getBookingConfirmationData(bookingId, userId);
    }
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      {isLoading ? (
        <View style={styles.container}></View>
      ) : (
        <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
          <View style={styles.container}>
            <View style={styles.subContainer}>
              <Image
                source={require('./../assets/images/tick-60x60.png')}
                style={styles.icon}
              />
              <View style={styles.title}>
                <AppTextBold style={styles.congText}>
                  CONGRATULATIONS
                </AppTextBold>
                <AppText style={styles.text}>
                  You successfully booked your dream home
                </AppText>
              </View>
            </View>

            {bookingAmount && bookingAmount != '' ? (
              <View style={styles.textContainer}>
                <AppText style={styles.details}>
                  We have received your pre-booking amount of
                  <AppText
                    style={{
                      fontSize:appFonts.largeBoldx,
                      fontFamily: appFonts.SourceSansProSemiBold,
                    }}>
                    &nbsp;{'\u20B9'}
                    {bookingAmount}/-&nbsp;
                  </AppText>
                  Our sales executive will get in touch with you shortly.
                </AppText>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={goToBookingDetailsScreen}>
                  <AppTextBold style={[styles.text, {textDecorationLine: 'underline'}]}>
                    View Booking Details
                  </AppTextBold>
                </TouchableOpacity>
              </View>
            ) : null}

            <View style={styles.btnContainer}>
              <AppButton
                color="primary"
                textColor="secondary"
                title="SCHEDULE SITE VISIT"
                onPress={goToSiteVisitScreen}
              />
              <AppButton title="VIEW MORE PROJECTS" onPress={goToHomeScreen} />
            </View>
          </View>
        </ScrollView>
      )}
      <AppFooter activePage={0} isPostSales={false} />
      <AppOverlayLoader isLoading={isLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: '7%',
    paddingVertical: '12%',
  },
  subContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    top: 20,
    height: 70,
    width: 70,
  },
  title: {
    paddingVertical: '12%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  congText: {
    fontSize: appFonts.xxxlargeFontSize,
    color: colors.expandText,
  },
  textContainer: {
    top: '5%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  details: {
    fontSize: appFonts.largeBold,
    lineHeight: 28,
    textAlign: 'center',
    color: colors.expandText,
    marginBottom: 12
  },
  text: {
    fontSize:appFonts.largeBold,
    
  },
  btnContainer: {
    top: '10%',
  },
});

export default BookingConfirmation;
