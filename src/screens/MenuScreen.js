import React, {useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import FooterMenu from '../components/FotterMenu';
import Orientation from 'react-native-orientation';

const Menu = (props) => {
  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  return (
    <>
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate('Login');
          }}>
          <Text>Login</Text>
        </TouchableOpacity>
      </View>
      <FooterMenu {...props} />
    </>
  );
};

export default Menu;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
