import React, {useState, useRef} from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  BackHandler,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
  Alert,
} from 'react-native';
import Modal from 'react-native-modal';

import AppFileChooser from './../components/actionSheet/AppFileChooser';
import appFileChooser from './../utility/appFileChooser';
import Screen from '../components/Screen';
import AppFooter from '../components/ui/AppFooter';

import appPhoneCall from '../utility/appPhoneCall';
import appPhoneEmail from '../utility/appPhoneEmail';
import appPhoneMap from '../utility/appPhoneMap';
import appFonts from '../config/appFonts';
import appSnakBar from '../utility/appSnakBar';
import AppProgressBar from '../components/ui/AppProgressBar';
import useFileDownload from '../hooks/useFileDownload';
import appConstant from '../utility/appConstant';

import {WebView} from 'react-native-webview';
import {useBackHandler} from '@react-native-community/hooks';
import Orientation from 'react-native-orientation';

const RnDScreen = () => {
  const [showWebview, setShowWebview] = useState(false);
  const ipWebView = useRef(null);

  const [fileChooserResponse, setFileChooserResponse] = useState(null);

  const [fileChooser, setFileChooser] = useState(false);
  const closeActionSheet = () => setFileChooser(false);

  const onClickFileChooserItem = async (clickItem) => {
    switch (clickItem) {
      case 'Remove':
        setFileChooser(false);
        setFileChooserResponse(null);
        break;

      case 'Gallery':
        appFileChooser.onLaunchImageLibrary((fileChooserResponse) => {
          setFileChooser(false);
          setFileChooserResponse(fileChooserResponse);
        });
        break;

      case 'Camera':
        appFileChooser.onLaunchCamera((fileChooserResponse) => {
          setFileChooser(false);
          setFileChooserResponse(fileChooserResponse);
        });
        break;

      case 'Phone':
        try {
          const chooserResponse = await appFileChooser.onSelectFile();
          setFileChooser(false);
          setFileChooserResponse(chooserResponse);

          // const dataUriResponse = await appFileChooser.toBase64(fileChooserResponse.uri);
          // console.log(dataUriResponse)
        } catch (error) {
          console.log('Error onSelectFile : ', error);
          setFileChooserResponse(null);
        }
        break;
    }
  };

  const validateFileSize = (fileSize) => {
    let msg = '';
    appFileChooser.validateFileSize(fileSize, (fileChooserResponse) => {
      msg = fileChooserResponse
        ? 'Valid file..'
        : 'File size should  be less than 5MB..';
    });
    return msg;
  };

  const openDeviceEmail = () => {
    appPhoneEmail.openDeviceEmail(
      'soutrik.me@indusnet.co.in',
      'Test Subject',
      'Test Body',
    );
  };

  const openDeviceCaller = (phoneNumber) => {
    appPhoneCall.openDeviceCaller(phoneNumber);
  };

  const openDeviceMap = () => {
    appPhoneMap.openDeviceMap();
  };

  const {
    progress,
    showProgress,
    error,
    isSuccess,
    checkPermission,
  } = useFileDownload();

  if (isSuccess) {
    appSnakBar.onShowSnakBar(
      'The file has been downloaded successfully!',
      'LONG',
    );
  }

  // const REMOTE_IMAGE_PATH = 'https://raw.githubusercontent.com/AboutReact/sampleresource/master/gift.png';
  const REMOTE_IMAGE_PATH =
    'https://customercare.godrejproperties.com/sites/default/files/cj-booking-data/332464/AF/booking_form.pdf';
  // const REMOTE_IMAGE_PATH =
  //   'https://customercare.godrejproperties.com/gpl-api/generate_invoice_receipt/a1t6F000009qLXxQAM';

  const onHandleDownloadFile = () => {
    // const fileName = 'invoice_2018-07-10.pdf';
    let fileName;

    if (fileName) {
      const ext = fileName.split('.')[0];
      if (ext) {
        downloadFile(REMOTE_IMAGE_PATH, fileName, ext);
      }
    } else {
      let fileExt = appConstant.getExtention(REMOTE_IMAGE_PATH);

      const fName = appConstant.getFileName(REMOTE_IMAGE_PATH);
      if (fileExt[0] && fName) {
        downloadFile(REMOTE_IMAGE_PATH, fName, fileExt[0]);
      }
    }
  };

  const downloadFile = async (imgPath, fileName, fileExtension) => {
    try {
      await checkPermission(imgPath, fileName, fileExtension);
    } catch (error) {
      appSnakBar.onShowSnakBar(error.message, 'LONG');
    }
  };

  const onHandleOpenWebview = () => {
    Orientation.lockToLandscape();
    setShowWebview(true);
  };

  useBackHandler(() => {
    if (showWebview) {
      Orientation.lockToPortrait();
      setShowWebview(false);
      return true;
    }
    return false;
  });

  const onHandleLoadStart = (URL) => {
    console.log("onHandleLoadStart : ", URL)
    const pathClose = URL.split("interactive-asset-close=");
    if (pathClose.length > 1 && pathClose[1] == "true") {
      Orientation.lockToPortrait();
      setShowWebview(false);
    }
  }

  return (
    <Screen>
      {showWebview && (
        <WebView
          originWhitelist={['*']}
          source={{uri: 'https://customercare.godrejproperties.com/gpl-api/interactive_asset_mobile/a1l2s00000003VlAAI/interactive_2bhk'}}
          style={{flex: 1}}
          ref={ipWebView}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          mediaPlaybackRequiresUserAction={true}
          onLoadStart={(data) => onHandleLoadStart(data.nativeEvent.url)}
          // onLoad={(data) => console.log("onLoad : " ,data.nativeEvent.url)}
        />
      )}

      {!showWebview && (
        <ScrollView>
          <View style={styles.container}>
            <TouchableOpacity activeOpacity={0.7} onPress={onHandleOpenWebview}>
              <View style={styles.buttonStyle}>
                <Text style={styles.buttonText}>Open Webview</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.7}
              onPress={onHandleDownloadFile}>
              <View style={styles.buttonStyle}>
                <Text style={styles.buttonText}>Download File</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => openDeviceMap()}>
              <View style={styles.buttonStyle}>
                <Text style={styles.buttonText}>Open Device Map</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => openDeviceCaller('8902402109')}>
              <View style={styles.buttonStyle}>
                <Text style={styles.buttonText}>Open Device Call</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.7} onPress={openDeviceEmail}>
              <View style={styles.buttonStyle}>
                <Text style={styles.buttonText}>Open Device Email</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => setFileChooser(true)}>
              <View style={styles.buttonStyle}>
                <Text style={styles.buttonText}>Action Sheet</Text>
              </View>
            </TouchableOpacity>

            {fileChooserResponse && fileChooserResponse.uri && (
              <View style={styles.fileChooserResponse}>
                <Text>
                  Res fileSize:{' '}
                  {fileChooserResponse.fileSize
                    ? fileChooserResponse.fileSize
                    : fileChooserResponse.size}
                </Text>
                <Text>Res uri: {fileChooserResponse.uri}</Text>
                <Text>
                  Res name:{' '}
                  {fileChooserResponse.name
                    ? fileChooserResponse.name
                    : fileChooserResponse.uri.split('/').pop()}
                </Text>
                <Text>
                  Res Validate:{' '}
                  {validateFileSize(
                    fileChooserResponse.fileSize
                      ? fileChooserResponse.fileSize
                      : fileChooserResponse.size,
                  )}
                </Text>
              </View>
            )}

            {fileChooserResponse && (
              <View style={styles.image}>
                <Image
                  style={{width: 200, height: 200}}
                  source={{uri: fileChooserResponse.uri}}
                />
              </View>
            )}
          </View>
        </ScrollView>
      )}

      {!showWebview && <AppFooter />}

      {showProgress && progress && progress > 0 ? (
        <AppProgressBar progress={progress} />
      ) : null}

      <Modal
        isVisible={fileChooser}
        style={{
          margin: 0,
          justifyContent: 'flex-end',
        }}
        onBackButtonPress={closeActionSheet}
        onBackdropPress={closeActionSheet}
        backdropColor="rgba(0,0,0,0.5)">
        <AppFileChooser
          onPress={(clickItem) => onClickFileChooserItem(clickItem)}
        />
      </Modal>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  buttonStyle: {
    height: 50,
    backgroundColor: 'rgb(0,98,255)',
    width: Dimensions.get('window').width - 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    margin: 10,
  },
  buttonText: {
    fontSize: appFonts.largeFontSize,
    color: 'rgb(255,255,255)',
  },
  image: {
    marginVertical: 24,
    alignItems: 'center',
  },
  response: {
    marginVertical: 16,
    marginHorizontal: 8,
  },
});

export default RnDScreen;
