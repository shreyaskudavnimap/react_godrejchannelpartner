import React, { useEffect, useState } from 'react';
import {
  View, StyleSheet, ScrollView, TextInput,
  Dimensions, Image, Text, TouchableOpacity, PermissionsAndroid
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import AppButton from '../components/ui/AppButton';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import Screen from '../components/Screen';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppFooter from '../components/ui/AppFooter';
import * as normStyle from '../styles/StyleSize';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import apiLogin from './../api/apiLogin';
import appSnakBar from '../utility/appSnakBar';
import { useNavigation } from '@react-navigation/native';
import ShowPassword from '../components/showPassword';
import { CommonActions } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import { ActionTypes } from 'redux-devtools';
import appAlert from '../utility/appAlert';
import Orientation from 'react-native-orientation';



const ChangeAddress = ({ props, route }) => {

  const routeParams = route.params;

  const userId =
    routeParams && routeParams.userId && routeParams.userId
      ? routeParams.userId
      : '';

  const street1 =
    routeParams && routeParams.street1 && routeParams.street1
      ? routeParams.street1
      : '';

  const street2 =
    routeParams && routeParams.street2 && routeParams.street2
      ? routeParams.street2
      : '';

  const street3 =
    routeParams && routeParams.street3 && routeParams.street3
      ? routeParams.street3
      : '';

  const city =
    routeParams && routeParams.city && routeParams.city
      ? routeParams.city
      : '';

  const pincode =
    routeParams && routeParams.pincode && routeParams.pincode
      ? routeParams.pincode
      : '';

  const state =
    routeParams && routeParams.state && routeParams.state
      ? routeParams.state
      : '';

  const country =
    routeParams && routeParams.country && routeParams.country
      ? routeParams.country
      : '';

  const addType =
    routeParams && routeParams.addType && routeParams.addType
      ? routeParams.addType
      : '';

  const [mobile, Setmobile] = useState("");
  const [isApiLoading, setApiIsLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [street_1, setStreet1] = useState("");
  const [street_2, setStreet2] = useState("");
  const [street_3, setStreet3] = useState("");
  const [cityName, setCity] = useState("");
  const [pincodeName, setPincode] = useState("");
  const [stateName, setState] = useState("");
  const [countryName, setCountry] = useState("");
  const [docName, setDocName] = useState("");
  const [docBase64, setDocBase64] = useState("");
  const [docId, setDocId] = useState("");
  const [docSize, setDocSize] = useState("");
  const [docMime, setDocMime] = useState("");
  const [disableButton, setDisableButton] = useState(true);

  const navigation = useNavigation();

  const dispatch = useDispatch();

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);


  useEffect(() => {
    _getUserInfo();
  }, []);

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );
  const menuType = useSelector((state) => state.menuType.menuType);
  const _getUserInfo = () => {
    setStreet1(street1);
    setStreet2(street2);
    setStreet3(street3);
    setCity(city);
    setPincode(pincode);
    setState(state);
    setCountry(country);
  };

  const chooseImage = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [
          DocumentPicker.types.images,
          DocumentPicker.types.pdf,
        ],
        readContent: true,
      });
      var fileType = (res.type).substring((res.type).lastIndexOf('/') + 1);
      console.log(fileType);
      if (fileType == 'png' || fileType == 'PNG' || fileType == 'jpeg' || fileType == 'JPEG' || fileType == 'pdf'
        || fileType == 'PDF' || fileType == 'JPG' || fileType == 'jpg') {
        setDocName(res.name);
        setDocSize(res.size);
        setDocMime((res.type).substring((res.type).lastIndexOf('/') + 1));
        _getBase64(res.uri);
      } else {
        appSnakBar.onShowSnakBar('Only image and pdf file is accepted', 'LONG');
      }
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  const _getBase64 = (uri) => {
    RNFetchBlob.fs.readFile(uri, 'base64')
      .then((files) => {
        setDocBase64(files);
      })
  };

  const goBack = () => {

    navigation.goBack();
  };

  const _updateAddress = () => {
    if (addType == 'residential')
      _updateResidentialAddress();
    if (addType == 'mailing')
      _updateMailingAddress();
  };

  const _updateMailingAddress = async () => {
    if (street_1 == '' || street_2 == '' || street_3 == '' || cityName == '' || pincodeName == '' || stateName == '' || countryName == '') {
      appSnakBar.onShowSnakBar('All field are mandatory', 'LONG');
      return null;
    } else {
      const request = {
        device_id: deviceUniqueId,
        new_value: street_1 + ", " + street_2 + ", " + street_3 + ", " + cityName + ", " + pincodeName + ", " + stateName + ", " + countryName,
        old_value: street1 + ", " + street2 + ", " + street3 + ", " + city + ", " + pincode + ", " + state + ", " + country,
        type: 'communication_address',
        user_id: userId,
      };
      console.log(request);
      setIsLoading(true);
      const result = await apiLogin.apiForUpdateProfileData(request);
      setIsLoading(false);
      var status = result.data.status;
      console.log(result.data);
      if (status == 200) {
        appAlert.showAppAlert("Service Request", "Request submitted successfully. Your service request number is " + result.data.request_id);
        goBack();
      } else {
        appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
      }
    }
  };


  const _updateResidentialAddress = async () => {
    if (street1 == street_1 && street2 == street_2 && street3 == street_3 && cityName == city &&
      pincodeName == pincode && stateName == state && country == countryName) {
      return null;
    }
    if (street_1 == '' || street_2 == '' || street_3 == '' || cityName == '' || pincodeName == '' || stateName == '' || countryName == '') {
      appSnakBar.onShowSnakBar('All field are mandatory', 'LONG');
      return null;
    } else {
      if (docBase64 != '' && docName != '' && docSize != '' && docMime != '') {
        const request = {
          device_id: deviceUniqueId,
          new_value: street_1 + ", " + street_2 + ", " + street_3 + ", " + cityName + ", " + pincodeName + ", " + stateName + ", " + countryName,
          old_value: street1 + ", " + street2 + ", " + street3 + ", " + city + ", " + pincode + ", " + state + ", " + country,
          type: 'res_address',
          user_id: userId,
          attachedFiles: {
            file: docBase64,
            file_id: 123,
            file_name: docName,
            file_size: docSize,
            mimeType: docMime,
          }
        };
        console.log(request);
        setIsLoading(true);
        const result = await apiLogin.apiForUpdateProfileData(request);
        setIsLoading(false);
        var status = result.data.status;
        console.log(result.data);
        if (status == 200) {
          appAlert.showAppAlert("Service Request", "Request submitted successfully. Your service request number is " + result.data.request_id);
          goBack();
        } else {
          appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
        }
      } else {
        appSnakBar.onShowSnakBar('Attach address proof document', 'LONG');
      }
    }
  };

  const _removeDoc = () => {
    setDocName(""); setDocBase64(""); setDocSize(""); setDocMime("");
  };

  const _setStreet1 = (e) => {
    setStreet1(e);
    console.log(street1 + " ll " + e);
    if (street1 === e.trim() && street2 === street_2 && street3 === street_3 && city === cityName && pincode === pincodeName
      && state === stateName && country === countryName) {
      setDisableButton(true);
    } else {
      setDisableButton(false);
    }
  };

  const _setStreet2 = (e) => {
    setStreet2(e);
    if (street1 === street_1 && street2 === e.trim() && street3 === street_3 && city === cityName && pincode === pincodeName
      && state === stateName && country === countryName) {
      setDisableButton(true);
    } else {
      setDisableButton(false);
    }
  };

  const _setStreet3 = (e) => {
    setStreet3(e);
    if (street1 === street_1 && street2 === street_2 && street3 === e.trim() && city === cityName && pincode === pincodeName
      && state === stateName && country === countryName) {
      setDisableButton(true);
    } else {
      setDisableButton(false);
    }
  };

  const _setCity = (e) => {
    setCity(e);
    if (street1 === street_1 && street2 === street_2 && street3 === street_3 && city === e.trim() && pincode === pincodeName
      && state === stateName && country === countryName) {
      setDisableButton(true);
    } else {
      setDisableButton(false);
    }
  };

  const _setPincode = (e) => {
    setPincode(e);
    if (street1 === street_1 && street2 === street_2 && street3 === street_3 && city === cityName && pincode === e.trim()
      && state === stateName && country === countryName) {
      setDisableButton(true);
    } else {
      setDisableButton(false);
    }
  };

  const _setState = (e) => {
    setState(e);
    if (street1 === street_1 && street2 === street_2 && street3 === street_3 && city === cityName && pincode === pincodeName
      && state === e.trim() && country === countryName) {
      setDisableButton(true);
    } else {
      setDisableButton(false);
    }
  };

  const _setCountry = (e) => {
    setCountry(e);
    if (street1 === street_1.trim() && street2 === street_2.trim() && street3 === street_3.trim() && city === cityName.trim() && pincode === pincodeName.trim()
      && state === stateName.trim() && country === e.trim()) {
      setDisableButton(true);
    } else {
      setDisableButton(false);
    }
  };

  const onHandleRetryBtn = () => {
    
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />

    <ScrollView >
      <View style={styles.container}>

          <View style={{ width: '100%', alignItems: 'flex-start' }}>
            <AppText style={styles.pageTitle}>
              {(addType == 'residential') ? 'Edit Residential Address' : 'Edit Mailing Address'}
            </AppText>
          </View>

          <View style={{ width: '100%', alignItems: 'flex-start' }}>
            <AppText style={{ marginLeft: '5%', color: colors.gray }}>
              {(addType == 'residential') ? 'Residential Street1 *' : 'Street1 *'}
            </AppText>
          </View>
          <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
            <TextInput
              style={styles.nameStyle}
              placeholder={"Address line 1"}
              placeholderTextColor={colors.gray}
              value={street_1}
              color="#000000"
              onChangeText={e => {
                _setStreet1(e);
              }}

            />
          </View>

          <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(10) }}>
            <AppText style={{ marginLeft: '5%', color: colors.gray }}>
              {(addType == 'residential') ? 'Residential Street2 *' : 'Street2 *'}
            </AppText>
          </View>
          <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
            <TextInput
              style={styles.nameStyle}
              placeholder={"Address line 2"}
              placeholderTextColor={colors.gray}
              value={street_2}
              color="#000000"
              onChangeText={e => {
                _setStreet2(e);
              }}

            />
          </View>

          <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(10) }}>
            <AppText style={{ marginLeft: '5%', color: colors.gray }}>
              {(addType == 'residential') ? 'Residential Street3 *' : 'Street3 *'}
            </AppText>
          </View>
          <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
            <TextInput
              style={styles.nameStyle}
              placeholder={"Address line 3"}
              placeholderTextColor={colors.gray}
              value={street_3}
              color="#000000"
              onChangeText={e => {
                _setStreet3(e);
              }}

            />
          </View>

          <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(10) }}>
            <AppText style={{ marginLeft: '5%', color: colors.gray }}>City *</AppText>
          </View>
          <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
            <TextInput
              style={styles.nameStyle}
              placeholder={"Enter city name"}
              placeholderTextColor={colors.gray}
              value={cityName}
              color="#000000"
              onChangeText={e => {
                _setCity(e);
              }}

            />
          </View>

          <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(10) }}>
            <AppText style={{ marginLeft: '5%', color: colors.gray }}>Pincode *</AppText>
          </View>
          <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
            <TextInput
              style={styles.nameStyle}
              placeholder={"Enter pincode"}
              placeholderTextColor={colors.gray}
              value={pincodeName}
              color="#000000"
              onChangeText={e => {
                _setPincode(e);
              }}

            />
          </View>

          <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(10) }}>
            <AppText style={{ marginLeft: '5%', color: colors.gray }}>State *</AppText>
          </View>
          <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
            <TextInput
              style={styles.nameStyle}
              placeholder={"Enter state"}
              placeholderTextColor={colors.gray}
              value={stateName}
              color="#000000"
              onChangeText={e => {
                _setState(e);
              }}

            />
          </View>

          <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(10) }}>
            <AppText style={{ marginLeft: '5%', color: colors.gray }}>Country *</AppText>
          </View>
          <View style={{ width: '100%', alignItems: 'center', }}>
            <TextInput
              style={styles.nameStyle}
              placeholder={"Enter country"}
              placeholderTextColor={colors.gray}
              value={countryName}
              color="#000000"
              onChangeText={e => {
                _setCountry(e);
              }}

            />
          </View>

          {
            (addType == 'residential')
              ?
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <View style={{ width: '100%', alignItems: 'flex-start', flexDirection: 'row', marginTop: normStyle.normalizeWidth(10) }}>
                  <View style={{ flex: 0.8, flexDirection: 'column' }}>
                    <AppText style={{ marginLeft: '5%', color: colors.gray }}>Attach the proof of address.</AppText>
                    <AppText style={{ marginLeft: '5%', color: colors.gray }}>Accepted address proofs are Adhar Card, Passport and recent electricty bills(3months).</AppText>
                  </View>
                  <View style={{ flex: 0.2, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { chooseImage(); }}>
                      <Image
                        source={require('./../assets/images/attachment-icon.png')}
                        style={styles.cameraStyle}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                {
                  (docName)
                    ?
                    <View style={styles.docStyle}>
                      <View style={{ flex: 0.8 }}>
                        <AppText style={{ color: colors.greenColor }}>{docName}</AppText>
                      </View>
                      <View style={{ flex: 0.2, alignItems: 'flex-end', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => { _removeDoc(); }}>
                          <Image
                            source={require('./../assets/images/cancel.png')}
                            style={styles.cancelStyle}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                    :
                    <View />
                }

              </View>
              :
              <View />
          }


          {/* <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}> */}
            <View style={styles.button}>
              {/* <AppButton
                  title="UPDATE"
                  color="primary"
                  textColor="secondary"
                  onPress={() => {_updateAddress();}}
                /> */}
              <AppButton
             color={disableButton ? 'charcoal' : 'secondary'}
                disabled={disableButton}
                textColor = {disableButton ? 'primary' : 'primary'}
                title="UPDATE"
                onPress={() => { _updateAddress() }}
              />
            </View>
          {/* </View> */}

        

      </View>
    </ScrollView>
      <AppFooter activePage={0} isPostSales={menuType == 'postsale' ? true : false} />
      <AppOverlayLoader isLoading={isLoading || isApiLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  pageTitle: {
    fontSize: normStyle.normalizeWidth(20),
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginLeft: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(40),
  },
  nameStyle: {
    width: '90%', height: normStyle.normalizeWidth(50),
    borderBottomWidth: 0.5, fontSize: normStyle.normalizeWidth(18),
    fontWeight: 'bold'
  },
  view2: {
    width: '100%', flexDirection: 'row',
    marginTop: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(20),
  },
  button: {
   
    // width: normStyle.normalizeWidth(100),
    // height: normStyle.normalizeWidth(0),
    alignItems: 'center', justifyContent: 'center',
    marginHorizontal:15,
    paddingBottom: normStyle.normalizeWidth(40),
    paddingTop: normStyle.normalizeWidth(40),
  },
  cameraStyle: {
    width: normStyle.normalizeWidth(25),
    height: normStyle.normalizeWidth(25),
  },
  cancelStyle: {
    width: normStyle.normalizeWidth(15),
    height: normStyle.normalizeWidth(15),
  },
  docStyle: {
    width: '90%', marginTop: normStyle.normalizeWidth(10), flexDirection: 'row',
    backgroundColor: colors.gray, padding: normStyle.normalizeWidth(10)
  }
});

export default ChangeAddress;
