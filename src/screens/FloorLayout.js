import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList,
  ScrollView,
  TouchableWithoutFeedback,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {EventRegister} from 'react-native-event-listeners';

import ImageModal from 'react-native-image-modal';
import {WebView} from 'react-native-webview';
import {useBackHandler} from '@react-native-community/hooks';
import Orientation from 'react-native-orientation';

import Screen from '../components/Screen';
import AppButton from '../components/ui/AppButton';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import appFonts from '../config/appFonts';
import AppHeader from '../components/ui/AppHeader';
import colors from '../config/colors';

import AppTypologyListItem from '../components/ui/AppTypologyListItem';
import AppTowerListItem from '../components/ui/AppTowerListItem';
import AppFloorListItem from '../components/ui/AppFloorListItem';
import AppInventoryListItem from '../components/ui/AppInventoryListItem';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';

import {useDispatch, useSelector} from 'react-redux';
import * as inventoryMPLAction from './../store/actions/inventoryMPLAction';

import apiInventoryMLP from './../api/apiInventoryMLP';
import apiWishlist from './../api/apiWishlist';

import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';
import {StackActions} from '@react-navigation/native';
import DropDownPicker from 'react-native-dropdown-picker';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const FloorScreen = ({navigation, route}) => {
  const [isSqFt, setIsSqFt] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [isInnerLoading, setIsInnerLoading] = useState(false);
  const [isFloorLoading, setIsFloorLoading] = useState(false);
  const [isInventoryLoading, setIsInventoryLoading] = useState(false);
  const [isJourneyLoading, setIsJourneyLoading] = useState(false);

  const [showWebview, setShowWebview] = useState(false);
  const [isFOYR, setIsFOYR] = useState(false);
  const [foyrUrl, setFOYRUrl] = useState('');

  const routeParams = route.params;
  const projectId =
    routeParams && routeParams.item && routeParams.item.proj_id
      ? routeParams.item.proj_id
      : '';

  const dispatch = useDispatch();

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );

  const loginUserData = useSelector((state) => state.loginInfo.loginResponse);
  const userId =
    loginUserData && loginUserData.data && loginUserData.data.userid
      ? loginUserData.data.userid
      : '';

  const inventoryMLPData = useSelector((state) => state.inventoryMLP);

  var typologyList = inventoryMLPData.typologyList;
  var towerList = inventoryMLPData.towerList;
  var floorList = inventoryMLPData.floorList;
  var inventoryList = inventoryMLPData.inventoryList;
  
  
  var typologyList = JSON.parse('{"msg":"Successful","status":"200","project_name":"Godrej Royale Woods","field_foyr_url":"https://customercare.godrejproperties.com/gpl-api/virtual_grid_mobile/a1l2s00000003VlAAI","data":[{"floor_plan_id":"1098782","title":"Banglore","field_floor_price_range":"5.09  Net BV(Cr)","field_floor_size_square_foot":"476.52 sqft","field_floor_size_square_meter":"6 bookings","field_property_layout_plan_url":"https://customercare.godrejproperties.com/sites/default/files/2020-06/Godrej_Royale_Woods_MLP_1_BHK.jpg","field_typology_tag":"329","selectedTypology":true,"scssTypology":"bhk_1"},{"floor_plan_id":"880771","title":"Mumbai","field_floor_price_range":"15.56 Net BV(Cr)","field_floor_size_square_foot":"574.04 to 700.41 sqft","field_floor_size_square_meter":"13 bookings","field_property_layout_plan_url":"https://customercare.godrejproperties.com/sites/default/files/2020-05/Godrej%20Royale%20Woods%20MLP_New_2%20BHK.JPG","field_typology_tag":"330","selectedTypology":false,"scssTypology":"bhk_2"},{"floor_plan_id":"880772","title":"3 BHK","field_floor_price_range":"50.03 - 59.04 Lakh","field_floor_size_square_foot":"697.51 to 701.49 sqft","field_floor_size_square_meter":"64.8 to 65.17 sqmt","field_property_layout_plan_url":"https://customercare.godrejproperties.com/sites/default/files/2020-05/Godrej%20Royale%20Woods%20MLP_New_3%20BHK.JPG","field_typology_tag":"331","selectedTypology":false,"scssTypology":"bhk_3"},{"floor_plan_id":"1098783","title":"STUDIO","field_floor_price_range":"23.00 - 28.02 Lakh","field_floor_size_square_foot":"261.03 to 262 sqft","field_floor_size_square_meter":"24.25 to 24.34 sqmt","field_property_layout_plan_url":"https://customercare.godrejproperties.com/sites/default/files/2020-06/Godrej_Royale_Woods_MLP_Studio.jpg","field_typology_tag":"495","selectedTypology":false,"scssTypology":"studio"}]}');
  
  // console.log("typologyList", JSON.stringify(typologyList))
  
 
  var towerList = JSON.parse('[{"id":"878844","title":"Godrej 24","code":"4","filling_fast":"","inv_count":2.78,"selectedTower":true}]');
  // console.log("towerList", JSON.stringify(towerList))
  
  
  var floorList = JSON.parse('[{"id":"878849","title":"Mrs. Krutika Don Bosco Fernando","code":"RW06_1","selectedFloor":true},{"id":"878845","title":"Mr. Ritish Ramesh Haldipur","code":"RW06_2","selectedFloor":false},{"id":"878852","title":"Mr. Jitesh Agarwal","code":"RW06_3","selectedFloor":false},{"id":"878853","title":"Mr. Tirumani Sarath Chandra","code":"RW06_4","selectedFloor":false},{"id":"878854","title":"5th floor","code":"RW06_5","selectedFloor":false},{"id":"878846","title":"6th floor","code":"RW06_6","selectedFloor":false},{"id":"878850","title":"7th floor","code":"RW06_7","selectedFloor":false},{"id":"878855","title":"8th floor","code":"RW06_8","selectedFloor":false},{"id":"878856","title":"9th floor","code":"RW06_9","selectedFloor":false},{"id":"878857","title":"10th floor","code":"RW06_10","selectedFloor":false},{"id":"878847","title":"11th floor","code":"RW06_11","selectedFloor":false},{"id":"878858","title":"12th floor","code":"RW06_12","selectedFloor":false},{"id":"878859","title":"13th floor","code":"RW06_13","selectedFloor":false}]');
  
  // console.log("floorList", JSON.stringify(floorList));
  
  var inventoryList = JSON.parse('[{"id":"880258","unit_no":"0.54 Value (IN CR)","field_wing":"B","carpet_area":"30 Sep 2020","carpet_area_sq_ft":"476.52 sqft","inv_img":"https://dxdwcef76c2aw.cloudfront.net/sites/default/files/2020-07/SENIOR_LIVING_01_BHK_UNIT_FLOOR-1_TO_16_UNIT_NO-6B-101_TO_6B-1601.jpg","inventory_price_amount":"3999002.75","inventory_price_formatted":"39.99 Lakh","floor_img":"https://dxdwcef76c2aw.cloudfront.net/sites/default/files/2020-06/SENIOR_LIVING_01_BHK_UNIT_FLOOR-1_TO_16_UNIT_NO-6B-101_TO_6B-1601.jpg","floor_title":"24.27%","typology_title":"1 BHK","wishlist_status":0,"show_floor_plan":"0","show_unit_plan":"1","selectedInventory":false,"isSelectedFloorPlan":true}]');
  // console.log("inventoryList", JSON.stringify(inventoryList));

  
  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  useEffect(() => {
    setShowWebview(false);
    // Orientation.lockToPortrait();
    if (deviceUniqueId) {
      getTypologyList(userId, deviceUniqueId, projectId);
    }
  }, [dispatch, deviceUniqueId]);



  useEffect(() => {
    const eventListener = EventRegister.addEventListener(
      'FloorLayoutUpdateEvent',
      (data) => {
        if (data) {
          if (userId) {
            wishlistItems(data);
          }
        }
      },
    );
    
    const eventListenerBooking = EventRegister.addEventListener(
      'FloorLayoutUpdateEventBooking',
      (data) => {
        if (data) {
          if (userId) {
            checkInventoryStatus(data);
          }
        }
      },
    );

    return () => {
      EventRegister.removeEventListener(eventListener);
      EventRegister.removeEventListener(eventListenerBooking);
    };
  }, [dispatch, userId]);

  const getTypologyList = useCallback(
    async (userId, deviceId, projectId) => {
      setIsLoading(true);
      try {
        const typologyResult = await dispatch(
          inventoryMPLAction.getTypologyList(userId, deviceId, projectId),
        );
        setIsLoading(false);

        if (
          typologyResult &&
          typologyResult.data &&
          typologyResult.data.length > 0
        ) {
          getTowerList(
            userId,
            deviceId,
            projectId,
            typologyResult.data[0].field_typology_tag,
          );
        } else {
          appSnakBar.onShowSnakBar(
            typologyResult.msg
              ? typologyResult.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
          navigation.goBack();
        }
      } catch (err) {
        appSnakBar.onShowSnakBar(err.message, 'LONG');
        setIsLoading(false);
        navigation.goBack();
      }
    },
    [
      dispatch,
      setIsLoading,
      isLoading,
      deviceUniqueId,
      typologyList,
      towerList,
    ],
  );

  const onSelectTypology = (typology) => {
    dispatch(inventoryMPLAction.updateTypologyList(typology));
    getTowerList(
      userId,
      deviceUniqueId,
      projectId,
      typology.field_typology_tag,
    );
  };

  const getFieldPropertyLayoutPlanImg = () => {
    if (typologyList && typologyList.data && typologyList.data.length > 0) {
      const selectedTypologyIndex = typologyList.data.findIndex(
        (x) => x.selectedTypology == true,
      );
      return typologyList.data[selectedTypologyIndex]
        .field_property_layout_plan_url;
    } else {
      return '';
    }
  };

  const getTowerList = useCallback(
    async (userId, deviceId, projectId, typologyId) => {
      setIsInnerLoading(true);
      try {
        await dispatch(
          inventoryMPLAction.getTowerList(
            userId,
            deviceId,
            projectId,
            typologyId,
          ),
        );
        setIsInnerLoading(false);
      } catch (err) {
        appSnakBar.onShowSnakBar(err.message, 'LONG');
        // navigation.goBack();
        setIsInnerLoading(false);
      }
    },
    [dispatch, setIsInnerLoading, isInnerLoading, deviceUniqueId, floorList],
  );

  const onSelectTower = (tower) => {
    dispatch(inventoryMPLAction.updateTowerList(tower));

    if (tower.selectedTower) {
      if (typologyList && typologyList.data && typologyList.data.length > 0) {
        const selectedTypologyIndex = typologyList.data.findIndex(
          (x) => x.selectedTypology == true,
        );

        getFloorList(
          userId,
          deviceUniqueId,
          projectId,
          typologyList.data[selectedTypologyIndex].field_typology_tag,
          tower.id,
        );
      }
    }
  };

  const getFloorList = useCallback(
    async (userId, deviceId, projectId, typologyId, towerId) => {
      setIsFloorLoading(true);
      try {
        await dispatch(
          inventoryMPLAction.getFloorList(
            userId,
            deviceId,
            projectId,
            typologyId,
            towerId,
          ),
        );
        setIsFloorLoading(false);
      } catch (err) {
        appSnakBar.onShowSnakBar(err.message, 'LONG');
        setIsFloorLoading(false);
      }
    },
    [
      dispatch,
      setIsFloorLoading,
      isFloorLoading,
      deviceUniqueId,
      typologyList,
      towerList,
    ],
  );

  const onSelectFloor = (floor) => {
    dispatch(inventoryMPLAction.updateFloorList(floor));

    if (floor.selectedFloor) {
      const selectedTypologyIndex = typologyList.data.findIndex(
        (x) => x.selectedTypology == true,
      );
      const selectedToweIndex = towerList.findIndex(
        (x) => x.selectedTower == true,
      );

      if (selectedTypologyIndex > -1 && selectedToweIndex > -1) {
        getInventoryList(
          userId,
          deviceUniqueId,
          projectId,
          typologyList.data[selectedTypologyIndex].field_typology_tag,
          towerList[selectedToweIndex].id,
          floor.id,
          true,
        );
      }
    }
  };

  const getInventoryList = useCallback(
    async (
      userId,
      deviceId,
      projectId,
      typologyId,
      towerId,
      floorId,
      isShowErrorMsg,
    ) => {
      setIsInventoryLoading(true);
      try {
        await dispatch(
          inventoryMPLAction.getInventoryList(
            userId,
            deviceId,
            projectId,
            typologyId,
            towerId,
            floorId,
          ),
        );
        setIsInventoryLoading(false);
      } catch (err) {
        if (isShowErrorMsg) {
          appSnakBar.onShowSnakBar(err.message, 'LONG');
        } else {
          dispatch(inventoryMPLAction.resetInventoryList());
        }
        setIsInventoryLoading(false);
      }
    },
    [
      dispatch,
      setIsFloorLoading,
      isFloorLoading,
      deviceUniqueId,
      typologyList,
      towerList,
    ],
  );

  const onSelectInventory = (inventory) => {
    dispatch(inventoryMPLAction.updateInventoryList(inventory));
  };

  const onClickPriceInfo = () => {
    Alert.alert(
      'Price Info',
      'Exclusive of Other Charges and All Govt.Taxes',
      [{text: 'GOT IT', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  };

  const onChangeUnit = () => {
    setIsSqFt(!isSqFt);
  };

  const isShowPlanImg = (inventory) => {
    if (inventory.show_floor_plan && inventory.show_unit_plan) {
      if (
        (inventory.show_floor_plan == '1' &&
          inventory.floor_img &&
          inventory.show_unit_plan == '1' &&
          inventory.inv_img) ||
        (inventory.show_floor_plan == '1' &&
          inventory.floor_img &&
          inventory.show_unit_plan != '1') ||
        (inventory.show_floor_plan !== '1' &&
          inventory.show_unit_plan == '1' &&
          inventory.inv_img)
      ) {
        return true;
      }
    }

    return false;
  };

  const getPlanImg = (inventoryItem) => {
    if (inventoryItem) {
      if (
        inventoryItem.show_floor_plan == '1' &&
        inventoryItem.show_unit_plan == '1' &&
        inventoryItem.floor_img &&
        inventoryItem.inv_img
      ) {
        return inventoryItem.isSelectedFloorPlan
          ? inventoryItem.floor_img
          : inventoryItem.inv_img;
      } else if (
        inventoryItem.show_floor_plan == '1' &&
        inventoryItem.show_unit_plan == '0' &&
        inventoryItem.floor_img
      ) {
        return inventoryItem.floor_img;
      } else if (
        inventoryItem.show_floor_plan == '0' &&
        inventoryItem.show_unit_plan == '1' &&
        inventoryItem.inv_img
      ) {
        return inventoryItem.inv_img;
      }
    }

    return null;
  };

  const onSelectionPlanImg = (inventory) => {
    dispatch(inventoryMPLAction.updateInventoryPlan(inventory));
  };

  const ActivityIndicatorElement = () => {
    return (
      <AppOverlayLoader isLoading={true} isWhiteBackground={true} />
      // <ActivityIndicator
      //   color={colors.jaguar}
      //   size="small"
      //   style={styles.activityIndicatorStyle}
      // />
    );
  };

  const goToFOYR = () => {
    const enquiryFormData = {};
    checkProjectAvailability(enquiryFormData, '3DView');
  };

  const onHandleOpenWebviewFOYR = () => {
    Orientation.lockToLandscape();
    setFOYRUrl(typologyList.field_foyr_url);
    setIsFOYR(true);
    setShowWebview(true);
  };

  const onCloseFOYRWebView = () => {
    setFOYRUrl('');
    setIsFOYR(false);
    Orientation.lockToPortrait();
    setShowWebview(false);
  };

  const handleWebViewNavigationStateChange = ({url}) => {
    console.log('handleWebViewNavigationStateChange newNavState : ', url);
    if (isFOYR) {
      const pathClose = url.split('foyr-close=');
      if (pathClose && pathClose.length > 1 && pathClose[1] == 'true') {
        onCloseFOYRWebView();
      } else {
        const pathCloseFOYR = url.split('foyrTxnID=');
        if (pathCloseFOYR && pathCloseFOYR.length > 0) {
          const foyrData = appConstant.getJsonFromUrl(url);
          console.log('foyrData: ', foyrData);

          if (foyrData && foyrData['inv_id'] && foyrData['foyrTxnID']) {
            onCloseFOYRWebView();
            getMLPDataFromFOYR(foyrData['inv_id'], foyrData['foyrTxnID']);
          } else {
            if (!showWebview)
              appSnakBar.onShowSnakBar(
                'Inventory selection facility is not available through FOYR',
                'LONG',
              );
          }
        }
      }
    }
  };

  const getMLPDataFromFOYR = async (inventoryId, foyrTxnID) => {
    try {
      setIsJourneyLoading(true);

      const requestData = {
        inventory_id: inventoryId,
      };

      const inventoryStatusResult = await apiInventoryMLP.apiInventoryInfo(
        requestData,
      );

      setIsJourneyLoading(false);

      if (inventoryStatusResult.data) {
        if (
          inventoryStatusResult.data.status &&
          inventoryStatusResult.data.status == '200'
        ) {
          const response = inventoryStatusResult.data;

          const typologyData = {
            title: response.data[0].typology_title,
          };
          const towerData = {
            id: response.data[0].towerid,
            title: response.data[0].tower,
          };
          const floorData = {
            title: response.data[0].floor_title,
          };
          const inventoryData = {
            carpet_area: response.data[0].carpet_area_sq_mt,
            carpet_area_sq_ft: response.data[0].carpet_area_sq_ft,
            floor_img: response.data[0].floor_img,
            floor_title: response.data[0].floor,
            id: response.data[0].id,
            inv_img: response.data[0].inv_img,
            inventory_price_amount: response.data[0].inventory_price_amount,
            typology_title: response.data[0].typology_title,
            unit_no: response.data[0].unit_no,
          };

          const enquiryFormData = {
            typologyData: typologyData,
            towerData: towerData,
            floorData: floorData,
            inventoryData: inventoryData,
            foyrTxnID: foyrTxnID,
            inventory_preference: '2',
          };
          checkGuestLogin(enquiryFormData, '3DView');
        } else {
          appSnakBar.onShowSnakBar(
            inventoryStatusResult.data.msg
              ? inventoryStatusResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      } else {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      setIsJourneyLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  useBackHandler(() => {
    if (showWebview) {
      setFOYRUrl('');
      Orientation.lockToPortrait();
      setShowWebview(false);
      return true;
    }
    return false;
  });

  const goToSiteVisitScreen = () => {
    navigation.navigate('PresaleVisitScreen', {projectId: projectId});
  };

  const goToBookingDetails = () => {
    const selectedTypologyIndex = typologyList.data.findIndex(
      (x) => x.selectedTypology == true,
    );

    const selectedTowerIndex = towerList.findIndex(
      (x) => x.selectedTower == true,
    );

    const selectedFloorIndex = floorList.findIndex(
      (x) => x.selectedFloor == true,
    );

    const selectedinventoryIndex = inventoryList.findIndex(
      (x) => x.selectedInventory == true,
    );

    if (
      selectedTypologyIndex > -1 &&
      selectedTowerIndex > -1 &&
      selectedFloorIndex > -1 &&
      selectedinventoryIndex > -1
    ) {
      const enquiryFormData = {
        typologyData: typologyList.data[selectedTypologyIndex],
        towerData: towerList[selectedTowerIndex],
        floorData: floorList[selectedFloorIndex],
        inventoryData: inventoryList[selectedinventoryIndex],
        foyrTxnID: '',
        inventory_preference: '1',
      };
      checkProjectAvailability(enquiryFormData, 'Booking');
    }
  };

  const checkProjectAvailability = async (enquiryFormData, action) => {
    setIsJourneyLoading(true);

    try {
      const projectAvailabilityResult = await apiInventoryMLP.apiCheckProjectAvailability(
        projectId,
      );
      setIsJourneyLoading(false);

      if (
        projectAvailabilityResult.data[0].hasOwnProperty('status') &&
        projectAvailabilityResult.data[0].status != null &&
        projectAvailabilityResult.data[0].status == '1'
      ) {
        if (action == 'Booking') {
          checkGuestLogin(enquiryFormData, 'Booking');
        } else if (action == '3DView') {
          onHandleOpenWebviewFOYR();
        }
      } else {
        setIsJourneyLoading(false);
        projectAvailabilityError();
      }
    } catch (error) {
      setIsJourneyLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const projectAvailabilityError = () => {
    Alert.alert(
      appConstant.appMessage.NO_BOOKING_AVAILABLE_ALERT_TITLE,
      appConstant.appMessage.NO_BOOKING_AVAILABLE_ALERT_MESSAGE,
      [
        {
          text:
            appConstant.appMessage.NO_BOOKING_AVAILABLE_ALERT_POSITIVE_BUTTON,
          onPress: () => console.log('OK Pressed'),
        },
      ],
      {cancelable: false},
    );
  };

  const checkGuestLogin = (generatedData, action) => {
    if (userId && userId != '') {
      if (action === 'Booking' || action === '3DView') {
        checkInventoryStatus(generatedData);
      } else if (action === 'Wishlist') {
        addOrRemoveWishlist(generatedData);
      }
    } else {
      if (action === 'Booking' || action === '3DView') {
        if(action === '3DView') {
          setIsJourneyLoading(true);
          setTimeout(() => {
            setIsJourneyLoading(false);
            validateLoginBooking(generatedData);
          }, 600);
        } else {
          validateLoginBooking(generatedData);
        }
      } else if (action === 'Wishlist') {
        validateLogin(generatedData);
      }
    }
  };

  const validateLogin = async (generatedData) => {
    navigation.dispatch(
      StackActions.push('Login', {
        backRoute: 'AppStackRoute',
        action: 'wishlist_set',
        screen: 'FloorLayout',
        item: generatedData,
      }),
    );
  };

  const validateLoginBooking = async (generatedData) => {
    navigation.dispatch(
      StackActions.push('Login', {
        backRoute: 'AppStackRoute',
        action: 'booking',
        screen: 'FloorLayout',
        item: generatedData,
      }),
    );
  };

  const checkInventoryStatus = async (enquiryFormData) => {
    try {
      setIsJourneyLoading(true);

      const requestData = {
        user_id: userId,
        inventory_id: enquiryFormData.inventoryData.id,
      };
      const inventoryStatusResult = await apiInventoryMLP.apiCheckInventoryStatus(
        requestData,
      );

      setIsJourneyLoading(false);

      if (inventoryStatusResult.data) {
        if (
          inventoryStatusResult.data.status &&
          inventoryStatusResult.data.status == '200'
        ) {
          // dispatch(inventoryMPLAction.resetInventoryList());

          const unitDetails = {
            project_id: projectId,
            unitData: enquiryFormData,
          };
          navigation.navigate('BookingScreen', unitDetails);
        } else if (
          inventoryStatusResult.data.status &&
          inventoryStatusResult.data.status == '500'
        ) {
          console.log('enquiryFormData: ', enquiryFormData);

          appSnakBar.onShowSnakBar(
            inventoryStatusResult.data.msg
              ? inventoryStatusResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );

          if (
            enquiryFormData &&
            enquiryFormData.inventory_preference &&
            enquiryFormData.inventory_preference == '1'
          ) {
            getInventoryList(
              userId,
              deviceUniqueId,
              projectId,
              enquiryFormData.typologyData.field_typology_tag,
              enquiryFormData.towerData.id,
              enquiryFormData.floorData.id,
              false,
            );
          }
        } else {
          appSnakBar.onShowSnakBar(
            inventoryStatusResult.data.msg
              ? inventoryStatusResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      } else {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      setIsJourneyLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const onHandleWishlist = (inventoryItem) => {
    checkGuestLogin(inventoryItem, 'Wishlist');
  };

  const addOrRemoveWishlist = async (inventoryItem) => {
    try {
      setIsJourneyLoading(true);

      const requestData = {
        user_id: userId,
        proj_id: projectId,
        inventory_id: inventoryItem.id,
        type: inventoryItem.wishlist_status == '0' ? 'add' : 'delete',
        item_type: 'inventory',
      };
      const wishlistItem = await apiWishlist.apiAddRemoveWishlist(requestData);

      setIsJourneyLoading(false);

      if (wishlistItem.data) {
        if (wishlistItem.data.status && wishlistItem.data.status == '200') {
          EventRegister.emit('wishlistUpdateEvent', 'update');

          appSnakBar.onShowSnakBar(
            wishlistItem.data.msg ? wishlistItem.data.msg : 'Wishlist...',
            'LONG',
          );

          dispatch(inventoryMPLAction.updateInventoryWishlist(inventoryItem));
        } else {
          appSnakBar.onShowSnakBar(
            wishlistItem.data.msg
              ? wishlistItem.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      } else {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      setIsJourneyLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const wishlistItems = async (inventoryItem) => {
    try {
      setIsJourneyLoading(true);

      const requestData = {
        user_id: userId,
        item_type: 'inventory',
      };
      const wishlistItems = await apiWishlist.apiWishlistItems(requestData);

      setIsJourneyLoading(false);

      if (wishlistItems.data) {
        if (wishlistItems.data.status && wishlistItems.data.status == '200') {
          const responseItems = wishlistItems.data.data;
          if (responseItems && responseItems.length > 0) {
            dispatch(
              inventoryMPLAction.updateInventoryWishlistArr(responseItems),
            );
          }
        } else {
          appSnakBar.onShowSnakBar(
            wishlistItems.data.msg
              ? wishlistItems.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      } else {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      setIsJourneyLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const onHandleRetryBtn = () => {
    if (userId && deviceUniqueId && projectId) {
      getTypologyList(userId, deviceUniqueId, projectId);
    }
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      {showWebview && foyrUrl ? (
        <WebView
          originWhitelist={['*']}
          source={{
            uri: foyrUrl,
          }}
          style={{flex: 1}}
          scalesPageToFit
          javaScriptEnabledAndroid={true}
          javaScriptEnabled={true}
          // domStorageEnabled={true}
          cacheEnabled={false}
          incognito={true}
          mediaPlaybackRequiresUserAction={true}
          startInLoadingState={true}
          renderLoading={ActivityIndicatorElement}
          onNavigationStateChange={handleWebViewNavigationStateChange}
        />
      ) : null}

      {!showWebview && (
        <AppHeader
          isShowUnitSwitch={false}
          isSqFt={isSqFt}
          changeUnit={onChangeUnit}
        />
      )}

      {isLoading && !showWebview ? (
        <View style={styles.container}></View>
      ) : (
        <>
          {!showWebview && (
            <ScrollView>
              <View style={styles.container}>
                <AppTextBold style={styles.pageTitle}>
                  {/* FLOOR PLANS &amp; LAYOUTS */}
                  BOOKING DATA
                </AppTextBold>
                

                <View style={styles.formStyle}>
                  <View style={styles.userViewContainer}>
                      <DropDownPicker
                        items={[{ "label": "India", "value": "India"}]}
                        style={styles.input}
                        onChangeItem={(value) => {
                          onChangeCountryDropDown(value);
                        }}
                        placeholder={"Select Financial Year"}
                      />
                  </View>
                </View>

                <View style={styles.formStyle}>
                  <View style={styles.userViewContainer}>
                      <DropDownPicker
                        items={[{ "label": "India", "value": "India"}]}
                        style={styles.input}
                        onChangeItem={(value) => {
                          onChangeCountryDropDown(value);
                        }}
                        placeholder={"Select Quarter"}
                      />
                  </View>
                </View>

                {/* Typology List */}
                {typologyList &&
                  typologyList.data &&
                  typologyList.data.length > 0 && (
                    <View style={styles.typologyContainer}>
                      <FlatList
                        showsHorizontalScrollIndicator={false}
                        horizontal
                        data={typologyList.data}
                        renderItem={({item, index}) => (
                          <AppTypologyListItem
                            menuItem={item}
                            index={index}
                            color={
                              item.selectedTypology
                                ? item.scssTypology && colors[item.scssTypology]
                                  ? item.scssTypology
                                  : 'secondary'
                                : 'primary'
                            }
                            textColor={
                              item.selectedTypology
                                ? 'primary'
                                : item.scssTypology && colors[item.scssTypology]
                                ? item.scssTypology
                                : 'secondary'
                            }
                            borderColor={
                              item.selectedTypology
                                ? 'primary'
                                : item.scssTypology && colors[item.scssTypology]
                                ? item.scssTypology
                                : 'secondary'
                            }
                            onPress={() => {
                              if (!item.selectedTypology)
                                onSelectTypology(item);
                            }}
                            isSqFt={isSqFt}
                          />
                        )}
                        keyExtractor={(item) => item.field_typology_tag}
                      />
{/* 
                      {!isInnerLoading && getFieldPropertyLayoutPlanImg() ? (
                        <View style={styles.img3d}>
                          <ImageModal
                            resizeMode="contain"
                            // imageBackgroundColor="#ffffff"
                            style={styles.img3dSource}
                            source={{
                              uri: getFieldPropertyLayoutPlanImg(),
                            }}
                          />
                        </View>
                      ) : null}

                      {!isInnerLoading &&
                      getFieldPropertyLayoutPlanImg() &&
                      typologyList.field_foyr_url ? (
                        <View style={styles.btn3DContainer}>
                          <TouchableOpacity
                            activeOpacity={0.8}
                            style={styles.btn3D}
                            onPress={goToFOYR}>
                            <AppText style={styles.text3d}>3D View</AppText>
                          </TouchableOpacity>
                        </View>
                      ) : null} */}
                    </View>
                  )}

                {/* Tower List */}

                {!isInnerLoading && (
                  <View>
                    {towerList && towerList.length > 0 ? (
                      <View style={styles.towerListContainer}>
                        {towerList.map((towerItem, indexTowerItem) => (
                          <View key={indexTowerItem}>
                            {!towerItem.selectedTower ? (
                              <AppTowerListItem
                                menuItem={towerItem}
                                onPress={() => {
                                  if (!towerItem.selectedTower)
                                    onSelectTower(towerItem);
                                }}
                              />
                            ) : (
                              <View style={styles.tdtContainer}>
                                <TouchableWithoutFeedback
                                  onPress={() => {
                                    if (towerItem.selectedTower)
                                      onSelectTower(towerItem);
                                  }}>
                                  <View style={styles.tdtHeaderItemContainer}>
                                    <AppText
                                      style={styles.tdtText}
                                      numberOfLines={1}>
                                      {towerItem.title} | {towerItem.code} | {towerItem.inv_count}
                                    </AppText>

                                    <Image
                                      style={styles.tdtIcon}
                                      source={require('./../assets/images/cancel.png')}
                                    />
                                  </View>
                                </TouchableWithoutFeedback>

                                {/* Floor List */}
                                {!isFloorLoading && (
                                  <View>
                                    {floorList && floorList.length > 0 ? (
                                      <View>
                                        {floorList.map(
                                          (floorItem, indexFloorItem) => (
                                            <View key={indexFloorItem}>
                                              {!floorItem.selectedFloor ? (
                                                <AppFloorListItem
                                                  menuItem={floorItem}
                                                  onPress={() => {
                                                    if (
                                                      !floorItem.selectedFloor
                                                    )
                                                      onSelectFloor(floorItem);
                                                  }}
                                                />
                                              ) : (
                                                <View
                                                  style={styles.fldContainer}>
                                                  <AppFloorListItem
                                                    menuItem={floorItem}
                                                    onPress={() => {
                                                      if (
                                                        floorItem.selectedFloor
                                                      )
                                                        onSelectFloor(
                                                          floorItem,
                                                        );
                                                    }}
                                                  />
                                                  {/* Inventory List */}
                                                  {!isInventoryLoading && (
                                                    <View>
                                                      {inventoryList &&
                                                      inventoryList.length >
                                                        0 ? (
                                                        <View>
                                                          {inventoryList.map(
                                                            (
                                                              inventoryItem,
                                                              indexInventoryItem,
                                                            ) => (
                                                              <View
                                                                key={
                                                                  indexInventoryItem
                                                                }>
                                                                {/* {!inventoryItem.selectedInventory ? (
                                                                  <AppInventoryListItem
                                                                    menuItem={
                                                                      inventoryItem
                                                                    }
                                                                    onClickPriceInfo={
                                                                      onClickPriceInfo
                                                                    }
                                                                    onPress={() => {
                                                                      onSelectInventory(
                                                                        inventoryItem,
                                                                      );
                                                                    }}
                                                                    isSqFt={
                                                                      isSqFt
                                                                    }
                                                                    onWishlistPress={(
                                                                      menuItem,
                                                                    ) =>
                                                                      onHandleWishlist(
                                                                        menuItem,
                                                                      )
                                                                    }
                                                                  />
                                                                ) : ( */}
                                                                  <View>
                                                                    <AppInventoryListItem
                                                                      menuItem={
                                                                        inventoryItem
                                                                      }
                                                                      onClickPriceInfo={
                                                                        onClickPriceInfo
                                                                      }
                                                                      onPress={() => {
                                                                        onSelectInventory(
                                                                          inventoryItem,
                                                                        );
                                                                      }}
                                                                      isSqFt={
                                                                        isSqFt
                                                                      }
                                                                      onWishlistPress={(
                                                                        menuItem,
                                                                      ) =>
                                                                        onHandleWishlist(
                                                                          menuItem,
                                                                        )
                                                                      }
                                                                    />

                                                                    {/* <View
                                                                      style={
                                                                        styles.planImageContainer
                                                                      }>
                                                                      {isShowPlanImg(
                                                                        inventoryItem,
                                                                      ) && (
                                                                        <>
                                                                          <View
                                                                            style={
                                                                              styles.planContainer
                                                                            }>
                                                                            {inventoryItem.show_floor_plan ==
                                                                              '1' &&
                                                                            inventoryItem.show_unit_plan ==
                                                                              '1' &&
                                                                            inventoryItem.floor_img &&
                                                                            inventoryItem.inv_img ? (
                                                                              <>
                                                                                <TouchableOpacity
                                                                                  activeOpacity={
                                                                                    0.8
                                                                                  }
                                                                                  style={
                                                                                    inventoryItem.isSelectedFloorPlan
                                                                                      ? styles.buttonPlanSelected
                                                                                      : styles.buttonPlan
                                                                                  }
                                                                                  onPress={() => {
                                                                                    if (
                                                                                      !inventoryItem.isSelectedFloorPlan
                                                                                    )
                                                                                      onSelectionPlanImg(
                                                                                        inventoryItem,
                                                                                      );
                                                                                  }}>
                                                                                  <AppText
                                                                                    style={
                                                                                      inventoryItem.isSelectedFloorPlan
                                                                                        ? styles.textPlanSelected
                                                                                        : styles.textPlan
                                                                                    }>
                                                                                    Floor
                                                                                    Plan
                                                                                  </AppText>
                                                                                </TouchableOpacity>
                                                                                <TouchableOpacity
                                                                                  activeOpacity={
                                                                                    0.8
                                                                                  }
                                                                                  style={
                                                                                    !inventoryItem.isSelectedFloorPlan
                                                                                      ? styles.buttonPlanSelected
                                                                                      : styles.buttonPlan
                                                                                  }
                                                                                  onPress={() => {
                                                                                    if (
                                                                                      inventoryItem.isSelectedFloorPlan
                                                                                    )
                                                                                      onSelectionPlanImg(
                                                                                        inventoryItem,
                                                                                      );
                                                                                  }}>
                                                                                  <AppText
                                                                                    style={
                                                                                      !inventoryItem.isSelectedFloorPlan
                                                                                        ? styles.textPlanSelected
                                                                                        : styles.textPlan
                                                                                    }>
                                                                                    Unit
                                                                                    Plan
                                                                                  </AppText>
                                                                                </TouchableOpacity>
                                                                              </>
                                                                            ) : (
                                                                              <>
                                                                                {inventoryItem.show_floor_plan ==
                                                                                  '1' &&
                                                                                inventoryItem.show_unit_plan ==
                                                                                  '0' &&
                                                                                inventoryItem.floor_img ? (
                                                                                  <View
                                                                                    style={
                                                                                      styles.buttonPlanSelectedSingle
                                                                                    }>
                                                                                    <AppText
                                                                                      style={
                                                                                        inventoryItem.isSelectedFloorPlan
                                                                                          ? styles.textPlanSelected
                                                                                          : styles.textPlan
                                                                                      }>
                                                                                      Floor
                                                                                      Plan
                                                                                    </AppText>
                                                                                  </View>
                                                                                ) : null}
                                                                                {inventoryItem.show_floor_plan ==
                                                                                  '0' &&
                                                                                inventoryItem.show_unit_plan ==
                                                                                  '1' &&
                                                                                inventoryItem.inv_img ? (
                                                                                  <View
                                                                                    style={
                                                                                      styles.buttonPlanSelectedSingle
                                                                                    }>
                                                                                    <AppText
                                                                                      style={
                                                                                        inventoryItem.isSelectedFloorPlan
                                                                                          ? styles.textPlanSelected
                                                                                          : styles.textPlan
                                                                                      }>
                                                                                      Unit
                                                                                      Plan
                                                                                    </AppText>
                                                                                  </View>
                                                                                ) : null}
                                                                              </>
                                                                            )}
                                                                          </View>

                                                                          {getPlanImg(
                                                                            inventoryItem,
                                                                          ) && (
                                                                            <View
                                                                              style={
                                                                                styles.pImgContainer
                                                                              }>
                                                                              <ImageModal
                                                                                resizeMode="contain"
                                                                                imageBackgroundColor="#ffffff"
                                                                                style={
                                                                                  styles.planImage
                                                                                }
                                                                                source={{
                                                                                  uri: getPlanImg(
                                                                                    inventoryItem,
                                                                                  ),
                                                                                }}
                                                                              />
                                                                            </View>
                                                                          )}
                                                                        </>
                                                                      )}

                                                                      <View>
                                                                        <AppButton
                                                                          title={
                                                                            'SCHEDULE A VISIT '
                                                                          }
                                                                          textColor="secondary"
                                                                          color="primary"
                                                                          onPress={
                                                                            goToSiteVisitScreen
                                                                          }
                                                                        />
                                                                        <AppButton
                                                                          title="BOOK ONLINE"
                                                                          onPress={() =>
                                                                            goToBookingDetails(
                                                                              'Booking',
                                                                            )
                                                                          }
                                                                        />
                                                                      </View>
                                                                    </View> */}
                                                                  </View>
                                                                {/* )} */}
                                                              </View>
                                                            ),
                                                          )}
                                                        </View>
                                                      ) : (
                                                        <AppText
                                                          style={
                                                            styles.noDataMsg
                                                          }>
                                                          {isInventoryLoading
                                                            ? ''
                                                            : 'Inventory not found'}
                                                        </AppText>
                                                      )}
                                                    </View>
                                                  )}
                                                </View>
                                              )}
                                            </View>
                                          ),
                                        )}
                                      </View>
                                    ) : (
                                      <AppText style={styles.noDataMsg}>
                                        {isFloorLoading
                                          ? ''
                                          : 'Floor Not Found'}
                                      </AppText>
                                    )}
                                  </View>
                                )}
                              </View>
                            )}
                          </View>
                        ))}
                      </View>
                    ) : (
                      <AppText style={styles.noDataMsg}>
                        {isInnerLoading ? '' : 'Tower Not Found'}
                      </AppText>
                    )}
                  </View>
                )}
              </View>
            </ScrollView>
          )}
        </>
      )}

      {!showWebview && <AppFooter activePage={0} isPostSales={false} />}

      <AppOverlayLoader
        isLoading={
          isLoading ||
          isInnerLoading ||
          isFloorLoading ||
          isInventoryLoading ||
          isJourneyLoading
        }
      />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  formStyle: {
    flex: 1,
    alignSelf: 'flex-start',
    width: '100%',
    marginTop: 10,
    marginBottom: 10,
    paddingHorizontal: 20,
  },
  pageTitle: {
    fontSize:appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.jaguar,
    textTransform: 'uppercase',
  },
  typologyContainer: {
    marginTop: 15,
    marginBottom: 15,
    // alignItems: 'center',
    justifyContent: 'center',
  },
  img3d: {
    width: windowWidth * 0.9,
    height: windowHeight / 3.5,
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: '#e1e4e8',
  },
  img3dSource: {
    width: windowWidth * 0.9,
    height: windowHeight / 3.2,
    alignSelf: 'center',
  },
  btn3DContainer: {
    marginTop: 12,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn3D: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 14,
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
    marginVertical: 10,
  },
  text3d: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  towerListContainer: {
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tdtContainer: {
    alignItems: 'center',
    padding: 20,
    flexDirection: 'column',
    width: '100%',
    marginVertical: 9,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  tdtHeaderItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 12,
  },
  tdtText: {
    flex: 1,
    fontSize:appFonts.largeBold,
    textTransform: 'capitalize',
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  tdtIcon: {
    width: 12,
    height: 12,
  },
  planImageContainer: {
    marginBottom: 15,
    justifyContent: 'center',
  },
  planContainer: {
    flexDirection: 'row',
  },
  buttonPlan: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 22,
    paddingVertical: 8,
    flexDirection: 'row',
    borderBottomColor: colors.lightGray,
    borderBottomWidth: 1,
    backgroundColor: colors.primary,
    marginVertical: 6,
  },
  textPlan: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  buttonPlanSelected: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 22,
    paddingVertical: 8,
    flexDirection: 'row',
    borderBottomColor: colors.jaguar,
    borderBottomWidth: 3,
    backgroundColor: colors.primary,
    marginVertical: 6,
  },
  buttonPlanSelectedSingle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 22,
    paddingVertical: 8,
    borderBottomColor: colors.jaguar,
    borderBottomWidth: 3,
    backgroundColor: colors.primary,
    marginVertical: 6,
  },
  textPlanSelected: {
    fontSize:appFonts.largeBold,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.jaguar,
  },
  pImgContainer: {
    marginVertical: 22,
    width: '100%',
    height: windowHeight / 4,
    // backgroundColor: '#e1e4e8',
    alignItems: 'center',
    justifyContent: 'center',
  },
  planImage: {
    width: windowWidth * 0.8,
    height: windowHeight / 4,
  },
  noDataMsg: {
    marginLeft: 20,
  },
});

export default FloorScreen;
