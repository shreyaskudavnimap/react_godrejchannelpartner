import React, {Component, useEffect, useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Platform,
  Dimensions,
  Image,
  ScrollView,
  StatusBar,
  ActivityIndicator,
} from 'react-native';
import FooterMenu from '../components/FotterMenu';
const {width, height} = Dimensions.get('window');
import CardComponent from '../components/CardComponent';
import LivingExpComponent from '../components/LivingingExpComponrnt';
import VideoDiscription from '../components/VideoDiscription';
import StatusBarComponent from '../components/StatusBarComponent';
import AppTextBold from '../components/ui/AppTextBold';
import ProjectNameComponent from '../components/contentConponent/ProjectNameComponent';
import VideoComponent from '../components/VideoComponent';
import apiLandingPage from './../api/apiLanding';
import useDevice from '../hooks/useDevice';
import useLocation from './../hooks/useLocation';
import JustCardComponent from '../components/JustLaunchedCard';
import ActivityIndicatorCmp from '../components/ActivityIndecatorCompoent';
import Orientation from 'react-native-orientation';

const HomeScreen = (props) => {
  const device = useDevice();
  const location = useLocation();
  const [videoData, setVideoData] = useState('');
  const [exploringData, setExploringData] = useState('');
  const [exploringDataStatus, setExploringDataStatus] = useState('');
  const [cityWiseData, setcityWiseData] = useState('');
  const [cityWiseDataStatus, setcityWiseDataStatus] = useState('');
  const [justLaunchedData, setjustLaunchedData] = useState('');
  const [justLaunchedStatus, setjustLaunchedStatus] = useState('');
  const [awardWinnigData, setawardWinnigData] = useState('');
  const [awardWinnigStatus, setawardWinnigStatus] = useState('');
  const [greenLivingData, setGreenLivingData] = useState('');
  const [greenLivingStatus, setGreenLivingStatus] = useState('');

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  
  useEffect(() => {
    // console.log('location home: ', location);
    // console.log('Devices home: ', device);
    getLandingPageVideo();
  }, [location]);

  const getLandingPageVideo = async () => {
    try {
      const request = {
        user_id: '',
        device_id: device.deviceUniqueId,
        city: 'Anantapur',
        lat: 22.9867569,
        lng: 87.8549755,
      };
      const result = await apiLandingPage.apiLandingPageVideoInfo(request);
      setVideoData(result.data);
      continueExploring();
    } catch (error) {}
  };
  const continueExploring = async () => {
    try {
      const continueExploringRequest = {
        user_id: '',
        device_id: 'AQWs43fdsg12KSKFG',
      };
      // livingExperience()
      cityWiseProperty();
      const result = await apiLandingPage.apiLandingPageContinueExpoloringInfo(
        continueExploringRequest,
      );
      setExploringDataStatus(result.data.status);
      if (result.data.status === '200') {
        setExploringData(result.data);
      } else if (result.data.status === '500') {
        setExploringData(result.data.msg);
      }
    } catch (error) {
      console.log('Event Listing Error: ', error);
    }
  };
  const livingExperience = async () => {
    try {
      const livingExperienceRequest = {
        experience_id: '548',
      };
      const result = await apiLandingPage.apiLandingPageLivingExperienceInfo(
        livingExperienceRequest,
      );
    } catch (error) {
      console.log('Event Listing Error: ', error);
    }
  };
  const cityWiseProperty = async () => {
    try {
      const cityWisePropertyRequest = {
        user_id: '',
        device_id: 'AQWs43fdsg12KSKFG',
        city: 'Mumbai',
        lat: 22.9867569,
        lng: 87.8549755,
      };
      justLaunched();
      const result = await apiLandingPage.apiLandingPageCityWiseProperty(
        cityWisePropertyRequest,
      );
      setcityWiseDataStatus(result.data.status);
      if (result.data.status === '200') {
        setcityWiseData(result.data);
      }
    } catch (error) {
      console.log('Event Listing Error: ', error);
    }
  };
  const justLaunched = async () => {
    try {
      const justLaunchedRequest = {
        user_id: '',
        device_id: 'AQWs43fdsg12KSKFG',
        type: 'new_launch',
      };
      greenLiving();
      // awardWinnig()
      const result = await apiLandingPage.apiProjectByTypeProperty(
        justLaunchedRequest,
      );
      setjustLaunchedStatus(result.data.status);
      if (result.data.status === '200') {
        setjustLaunchedData(result.data);
      } else {
        setjustLaunchedData(result.data.msg);
      }
    } catch (error) {
      console.log('Event Listing Error: ', error);
    }
  };
  const greenLiving = async () => {
    try {
      const greenLivingRequest = {
        user_id: '',
        device_id: 'AQWs43fdsg12KSKFG',
        type: 'green_living',
      };
      awardWinnig();
      const result = await apiLandingPage.apiProjectByTypeProperty(
        greenLivingRequest,
      );
      setGreenLivingStatus(result.data.status);
      if (result.data.status === '200') {
        setGreenLivingData(result.data);
      } else {
        setGreenLivingData(result.data.msg);
      }
    } catch (error) {
      console.log('Event Listing Error: ', error);
    }
  };
  const awardWinnig = async () => {
    try {
      const awardWinnigRequest = {
        user_id: '',
        device_id: 'AQWs43fdsg12KSKFG',
        type: 'award_winning',
      };

      const result = await apiLandingPage.apiProjectByTypeProperty(
        awardWinnigRequest,
      );
      setawardWinnigStatus(result.data.status);
      if (result.data.status === '200') {
        setawardWinnigData(result.data);
      } else {
        setawardWinnigData(result.data.msg);
      }
    } catch (error) {
      console.log('Event Listing Error: ', error);
    }
  };
  return (
    <>
      <StatusBarComponent />
      <ScrollView
        style={{marginBottom: 50}}
        howsHorizontalScrollIndicator={false}>
        <View style={{height: '100%', backgroundColor: '#fff'}}>
          <View style={{height: height, width: width}}>
            {videoData !== '' ? <VideoComponent videoData={videoData} /> : null}
          </View>
          <VideoDiscription />
          <View style={{marginLeft: '3%'}}>
            <AppTextBold>CONTINUE EXPLORING...</AppTextBold>
          </View>
          {exploringData !== '' ? (
            <View style={{marginLeft: '3%'}}>
              {exploringDataStatus == '200' ? (
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}>
                  {exploringData.data.map((data, i) => {
                    return (
                      <TouchableOpacity
                        onPress={() =>
                          props.navigation.navigate('ProjectDetails')
                        }
                        key={i}>
                        <CardComponent
                          image={data.proj_img}
                          name={data.proj_name}
                          city={
                            data.cities.split(',').length == 1
                              ? data.cities.split(',')[0]
                              : data.cities.split(',')[1]
                          }
                          price={data.min_price}
                        />
                      </TouchableOpacity>
                    );
                  })}
                </ScrollView>
              ) : (
                <View>
                  <Text>{exploringData}</Text>
                </View>
              )}
            </View>
          ) : (
            <ActivityIndicatorCmp />
          )}

          {/* <LivingExpComponent >WHAT KIND OF LIVING EXPERIENCE ARE YOU LOOKING FOR </LivingExpComponent> */}
          {cityWiseData !== '' ? (
            <View>
              {cityWiseDataStatus == '200' ? (
                <View>
                  {cityWiseData.data.map((data, i) => {
                    return (
                      <View style={{marginLeft: '3%'}} key={i}>
                        <AppTextBold>{data.city_name}</AppTextBold>
                        <ScrollView
                          horizontal={true}
                          showsHorizontalScrollIndicator={false}>
                          {data.proj_list.map((data, i) => {
                            return (
                              <TouchableOpacity
                                onPress={() =>
                                  props.navigation.navigate('ProjectDetails')
                                }
                                key={i}>
                                <CardComponent
                                  image={data.field_project_image}
                                  name={data.title}
                                  city={data.sub_location}
                                  price={data.starting_price}
                                />
                              </TouchableOpacity>
                            );
                          })}
                        </ScrollView>
                      </View>
                    );
                  })}
                </View>
              ) : null}
            </View>
          ) : (
            <View>
              <ActivityIndicatorCmp />
            </View>
          )}
          {/* <LivingExpComponent >WHAT TYPE OF HOME  PERSONALITY ARE YOU ? </LivingExpComponent> */}
          <View style={{marginLeft: '3%'}}>
            <AppTextBold>JUST LAUNCHED</AppTextBold>
          </View>
          {justLaunchedData !== '' ? (
            <View style={{marginLeft: '3%', marginRight: '3%'}}>
              {justLaunchedStatus == '200' ? (
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}>
                  {justLaunchedData.data.map((data, i) => {
                    return (
                      <JustCardComponent
                        key={i}
                        image={data.field_new_launched_image_url}
                        name={data.title}
                        city={data.field_city}
                        //  price = {data.min_price}
                      />
                    );
                  })}
                </ScrollView>
              ) : (
                <View>
                  <Text>{justLaunchedData}</Text>
                </View>
              )}
            </View>
          ) : (
            <ActivityIndicatorCmp />
          )}
          <View style={{marginLeft: '3%'}}>
            <AppTextBold>GREEN LIVING</AppTextBold>
          </View>
          {greenLivingData !== '' ? (
            <View style={{marginLeft: '3%'}}>
              {greenLivingStatus == '200' ? (
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}>
                  {greenLivingData.data.map((data, i) => {
                    return (
                      <TouchableOpacity
                        onPress={() =>
                          props.navigation.navigate('ProjectDetails')
                        }
                        key={i}>
                        <CardComponent
                          image={data.field_new_launched_image_url}
                          name={data.title}
                          city={data.field_city}
                          //  price = {data.min_price}
                        />
                      </TouchableOpacity>
                    );
                  })}
                </ScrollView>
              ) : (
                <View>
                  <Text>{awardWinnigData}</Text>
                </View>
              )}
            </View>
          ) : null}
          <View style={{marginLeft: '3%'}}>
            <AppTextBold>AWARD WINNING</AppTextBold>
          </View>
          {awardWinnigData !== '' ? (
            <View style={{marginLeft: '3%'}}>
              {awardWinnigStatus == '200' ? (
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}>
                  {awardWinnigData.data.map((data, i) => {
                    return (
                      <CardComponent
                        key={i}
                        image={data.field_new_launched_image_url}
                        name={data.title}
                        city={data.field_city}
                        //  price = {data.min_price}
                      />
                    );
                  })}
                </ScrollView>
              ) : (
                <View>
                  <Text>{awardWinnigData}</Text>
                </View>
              )}
            </View>
          ) : null}
          <LivingExpComponent>ASK AN EXPERT ? </LivingExpComponent>
        </View>
      </ScrollView>
      <FooterMenu {...props} />
    </>
  );
};

export default HomeScreen;
