import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Image, TouchableOpacity, Alert} from 'react-native';

import Screen from '../components/Screen';
import AppButton from '../components/ui/AppButton';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppTextBoldSmall from '../components/ui/AppTextBoldSmall';
import AppFooter from '../components/ui/AppFooter';
import appFonts from '../config/appFonts';
import appPhoneCall from '../utility/appPhoneCall';
import appPhoneMap from '../utility/appPhoneMap';
import addCalenderEvent from '../utility/appAddEventCalender';
import moment from 'moment';
import {StackActions} from '@react-navigation/native';
import appSnakBar from '../utility/appSnakBar';
import colors from '../config/colors';

import * as AddCalendarEvent from 'react-native-add-calendar-event';
import Orientation from 'react-native-orientation';

const ThankYouScreen = (props) => {
  // console.log(
  //   'props.siteVisitData===========',
  //   props.route.params.siteVisitData,
  // );
  // // const [siteVisitSuccessData, setSiteVisitSuccessData] = useState(null)

  const routeParams = props.route.params;

  const {
    visiting_date,
    visiting_time,
    propertyDetails,
    isPostSales,
    visitor_country_code,
  } = props.route.params.siteVisitData;

  const {
    proj_id,
    name,
    lng,
    lat,
    address,
    callus_india,
    callus_other_country,
  } = propertyDetails;

  const projectId = proj_id;
  const indiaCountryCode = '+91';

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  const openDeviceCaller = (phoneNumber) => {
    appPhoneCall.openDeviceCaller(phoneNumber);
  };

  const openDeviceMap = (latitude, longitude, title) => {
    if (
      latitude &&
      longitude &&
      latitude != '' &&
      longitude != '' &&
      latitude != '0' &&
      longitude != '0'
    ) {
      appPhoneMap.openDeviceMap(latitude, longitude, title);
    } else {
      appSnakBar.onShowSnakBar(
        'Direction is not available for this project',
        'LONG',
      );
    }
  };

  const EVENT_TITLE = 'Lunch';
  const TIME_NOW_IN_UTC = moment.utc(new Date(`21 Jan 2021 01:06 PM`));

  const utcDateToString = (momentInUTC) => {
    let s = moment.utc(momentInUTC).format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    return s;
  };

  const addToCalenderEvent = () => {
    let title = `Schedule Visit at ${name}`;

    const startDateUTC = utcDateToString(
      moment.utc(new Date(visiting_date + ' ' + visiting_time)),
    );
    const endDateUTC = utcDateToString(
      moment.utc(new Date(visiting_date + ' ' + visiting_time)).add(3, 'hours'),
    );

    // console.log('visiting_date======================', visiting_date);
    // console.log('visiting_time======================', visiting_time);

    // console.log('startDateUTC======================', startDateUTC);
    // console.log('endDateUTC======================', endDateUTC);

    if (startDateUTC && endDateUTC) {
      const eventConfig = {
        title,
        startDate: startDateUTC,
        endDate: endDateUTC,
        notes: 'Site Visit!',
        navigationBarIOS: {
          tintColor: colors.secondary,
          backgroundColor: colors.primary,
          titleColor: colors.jaguar,
        },
      };

      AddCalendarEvent.presentEventCreatingDialog(eventConfig)
        .then((eventInfo) => {
          console.log('eventInfo -> ' + JSON.stringify(eventInfo));

          if (eventInfo.action == 'SAVED') {
            Alert.alert(
              'Event',
              'A new event has been saved successfully!',
              [{text: 'Ok', onPress: () => console.log('OK Pressed')}],
              {cancelable: false},
            );
          }
        })
        .catch((error) => {
          // handle error such as when user rejected permissions
          console.log('Error -> ' + error);
        });
    }
  };

  return (
    <Screen>
      <View style={styles.container}>
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <View style={styles.imageCon}>
            <Image
              source={require('./../assets/images/tick-60x60.png')}
              style={styles.icon}
            />
          </View>
          <AppText style={styles.text1}>Thank you for your interest in</AppText>
          <AppTextBold style={styles.propertyName}>{name}</AppTextBold>
          <AppText
            style={styles.text1}>{`Peace & tranquility awaits you.`}</AppText>
        </View>

        <View style={styles.detail}>
          <AppText style={styles.text}>Your site visit is scheduled on</AppText>
          <AppTextBoldSmall
            style={{fontSize: appFonts.largeFontSize, textAlign: 'center'}}>{`${moment(
            visiting_date,
          ).format('DD MMM YYYY')} at ${moment(visiting_time, 'hh:mm A').format(
            'hh:mm A',
          )}.`}</AppTextBoldSmall>
          <AppText style={styles.text}>
            Our sales executive will greet you at the site.
          </AppText>
          <AppTextBoldSmall style={styles.text2}>
            <AppText
              onPress={() => {
                openDeviceCaller(
                  visitor_country_code === indiaCountryCode
                    ? callus_india
                    : callus_other_country,
                );
              }}>{` Call for Assistance `}</AppText>
            &nbsp; | &nbsp;
            <AppText
              onPress={() => {
                addToCalenderEvent();
              }}>{` Add to calendar `}</AppText>
          </AppTextBoldSmall>
        </View>

        <View style={styles.btn}>
          <AppButton
            color="primary"
            textColor="secondary"
            title="GET DIRECTIONS"
            onPress={() => {
              openDeviceMap(lat, lng, name ? name : 'Open in Maps');
            }}
          />
          <AppButton
            title="CONTINUE EXPLORING"
            onPress={() => {
              props.navigation.dispatch(
                StackActions.replace('ProjectDetails', {
                  item: {proj_id: projectId},
                }),
              );
              // props.navigation.navigate('ProjectDetails', {
              //   item: {proj_id: projectId},
              // });
            }}
          />
        </View>
      </View>
      <AppFooter activePage={0} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    margin: 15,
  },
  imageCon: {
    height: '20%',
    width: '20%',
    marginTop: '10%',
    marginBottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text1: {fontSize: appFonts.largeBold, textAlign: 'center'},
  text2: {fontSize: appFonts.largeFontSize, marginTop: 10, alignItems: 'center'},
  icon: {
    height: 60,
    width: 60,
  },
  propertyName: {
    fontSize: appFonts.xxxlargeFontSize,
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  detail: {
    marginTop: '5%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn: {marginTop: '20%', marginHorizontal: 10, marginBottom: 25},
  text: {
    fontSize: appFonts.largeBold,
    lineHeight: 28,
    textAlign: 'center',
  },
});

export default ThankYouScreen;
