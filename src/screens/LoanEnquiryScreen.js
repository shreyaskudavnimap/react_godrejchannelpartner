import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  FlatList,
  TouchableWithoutFeedback,
  TextInput,
  ScrollView,
  Picker,
  Modal,
  Alert,
  Dimensions,
  Platform,
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import colors from '../config/colors';
import appFonts from '../config/appFonts';
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppButton from '../components/ui/AppButton';
import AppFooter from '../components/ui/AppFooter';
import appSnakBar from '../utility/appSnakBar';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import {useSelector} from 'react-redux';
import apiLoanDisbursement from '../api/apiLoanDisbursement';
import appConstant from '../utility/appConstant';
import BankDetails from './BankDetails';
import LoanDisbursal from './LoanDisbursal';
import GetLocation from 'react-native-get-location';
import appUserAuthorization from '../utility/appUserAuthorization';
import Orientation from 'react-native-orientation';
import GestureRecognizer from 'react-native-swipe-gestures';
// import SelectDropdownModalScreen from './modals/SelectDropdownModalScreen';
// import userTermsAuthorization from '../hooks/userTermsAuthorization';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const LoanEnquiryScreen = ({navigation, route}) => {
  let controller;

  const [isLoading, setIsLoading] = useState(false);
  const [showPropertyDropdown, isShowPropertyDropdown] = useState(false);
  const [bankDetailMasterData, setBankDetailMasterData] = useState([]);
  const [propertyDisclaimer, setPropertyDisclaimer] = useState('');
  const [search, setSearch] = useState('');
  const [filteredDataSource, setFilteredDataSource] = useState([]);
  const [masterDataSource, setMasterDataSource] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [termsText, setTermsText] = useState('');
  const [bankDetailsEditForm, isBankDetailsEditForm] = useState(false);
  const [showBankDetailView, isShowBankDetailView] = useState(false);

  const loginvalue = useSelector((state) => state.loginInfo.loginResponse);
  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );
  var userId =
    typeof loginvalue.data !== 'undefined' ? loginvalue.data.userid : '';
  const dashboardData = useSelector((state) => state.dashboardData);
  const propertyData =
    typeof dashboardData.rmData !== 'undefined' ? dashboardData.rmData : null;
  // console.log("propertyData", propertyData);
  const propertyListData = []; // need to declare here
  for (let item of propertyData) {
    propertyListData.push({
      label: item.property_name + '/' + item.inv_flat_code,
      value: item.booking_id,
    });
  }

  const routeParams = route.params;
  let booking_id =
    routeParams && routeParams.bookingId ? routeParams.bookingId : '';
  // console.log("booking_id", booking_id);
  let propertyTitle = '';
  if (!booking_id) {
    if (propertyListData.length > 0) {
      booking_id = propertyListData[0].value;
      propertyTitle = propertyListData[0].label;
    }
  } else {
    const propertyIndex = propertyListData.findIndex(
      (x) => x.value == booking_id,
    );
    if (propertyIndex > -1) {
      propertyTitle = propertyListData[propertyIndex].label;
    }
  }

  const [selectedProperty, setSelectedProperty] = useState(propertyTitle);
  const [bookingId, setBookingId] = useState(booking_id);

  // console.log("bookingId", bookingId);
  const active_tab =
    routeParams && routeParams.activeTab
      ? routeParams.activeTab
      : 'LoanEnquiry';
  // console.log("active_tab", active_tab);
  const [activeTab, setActiveTab] = useState(active_tab);
  // console.log("activeTab", activeTab);

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
    getLoanDetails();
  }, []);
  useEffect(() => {
    prepareBankData();
  }, [bankDetailMasterData, bookingId]);
  useEffect(() => {
    searchFilterFunction();
  }, [masterDataSource]);

  const getLoanDetails = async () => {
    setIsLoading(true);
    try {
      const requestData = {user_id: userId};
      const loanDetails = await apiLoanDisbursement.getLoanDetails(requestData);
      // setIsLoading(false);
      setBankDetailMasterData(loanDetails.data.data);
    } catch (error) {
      console.log(error);
      // setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
    getTerms();
  };

  const getTerms = async () => {
    // setIsLoading(true);
    try {
      const terms = await apiLoanDisbursement.getTermsText();
      // console.log("terms.data", terms.data);
      let termData = terms.data[0].body.replace(/<(.|\n)*?>/g, '');
      setTermsText(termData);
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
    // prepareBankData();
  };

  const prepareBankData = () => {
    // console.log("bankDetailMasterData2", bankDetailMasterData);
    // if (bankDetailMasterData.length > 0) {
    if (bookingId && bookingId != '') {
      // console.log("bankDetailMasterData[bookingId]", bankDetailMasterData[bookingId]);
      if (bankDetailMasterData && bankDetailMasterData[bookingId]) {
        setMasterDataSource(bankDetailMasterData[bookingId].bank);
        setPropertyDisclaimer(bankDetailMasterData[bookingId].disclaimer);
        // console.log("bankDetailMasterData[bookingId].bank", bankDetailMasterData[bookingId].bank);
      } else {
        setMasterDataSource([]);
      }
    } else {
      setMasterDataSource([]);
    }
    // }
    // console.log("MasterDataSource", bookingId);
  };

  const searchFilterFunction = (text) => {
    if (text) {
      const newData = masterDataSource.filter(function (item) {
        const itemData = item.Bank_name
          ? item.Bank_name.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
      setSearch(text);
    } else {
      setFilteredDataSource(masterDataSource);
      setSearch(text);
    }
  };

  const onValueChangeProperty = (value) => {
    // setProperty(value);
    setBookingId(value);
  };

  // const { postAuthorization } = userTermsAuthorization();
  const postAuthorization = (moduleName) => {
    const storedUserData = loginvalue.data;
    const requestData = {
      deviceId: deviceUniqueId,
      devicePlatform: Platform.OS,
      userId: userId,
      customerName:
        storedUserData && storedUserData.first_name && storedUserData.last_name
          ? `${storedUserData.first_name} ${storedUserData.last_name}`
          : '',
      mobileNo:
        storedUserData && storedUserData.mob_no ? storedUserData.mob_no : '',
      emailId:
        storedUserData && storedUserData.email ? storedUserData.email : '',
      moduleName: moduleName,
    };
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
      .then(({latitude, longitude}) => {
        appUserAuthorization.validateUserAuthorization({
          ...requestData,
          latiTude: latitude,
          longiTude: longitude,
        });
      })
      .catch((error) => {
        appUserAuthorization.validateUserAuthorization(requestData);
      });
  };

  const loanEnquiry = async (bankId) => {
    Alert.alert(
      appConstant.appMessage.LOAN_ENQUIRY_ALERT_TITLE,
      termsText,
      [
        {
          text: appConstant.appMessage.LOAN_ENQUIRY_ALERT_NEGATIVE_BUTTON,
          onPress: () => console.log('Cancel Pressed'),
        },
        {
          text: appConstant.appMessage.LOAN_ENQUIRY_ALERT_POSITIVE_BUTTON,
          onPress: () => {
            postAuthorization('Loan Enquiry');
            postLoanEnquiry(bankId);
          },
        },
      ],
      {cancelable: false},
    );
  };

  const postLoanEnquiry = async (bankId) => {
    setIsLoading(true);
    try {
      const requestData = {
        user_id: userId,
        bank_id: bankId,
        booking_id: bookingId,
      };
      const loanEnquiry = await apiLoanDisbursement.loanEnquiry(requestData);
      setIsLoading(false);
      // console.log(loanEnquiry.data.msg);
      Alert.alert(
        '',
        loanEnquiry.data.msg,
        [
          {
            text: appConstant.appMessage.LOAN_ENQUIRY_ALERT_CLOSE_BUTTON,
            onPress: () => console.log('Cancel Pressed'),
          },
        ],
        {cancelable: true},
      );
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const checkBankDetailsExist = async () => {
    // setIsLoading(true);
    try {
      const requestData = {user_id: userId, booking_id: bookingId};
      const checkBankDetail = await apiLoanDisbursement.checkBankDetails(
        requestData,
      );
      const bankDetailStatus = checkBankDetail.data.status;

      // setIsLoading(false);
      if (bankDetailStatus == 200) {
        Alert.alert(
          'Loan Disbursement',
          'Please enter your loan details to enable us generate your Disbursement Request Letter',
          [
            {
              text: 'Enter Loan Details',
              onPress: () => {
                setActiveTab('LoanDetails');
              },
            },
          ],
          {cancelable: false},
        );
      } else if (bankDetailStatus == 501) {
        navigation.navigate('PayNowScreen', {
          bookingId: bookingId,
        });
      } else {
        Alert.alert(
          'Loan Disbursement',
          checkBankDetail.data.msg,
          [
            {
              text: 'OK',
              onPress: () => {
                setActiveTab('LoanDetails');
              },
            },
          ],
          {cancelable: false},
        );
      }
    } catch (error) {
      console.log(error);
      // setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  // console.log("showBankDetailView", showBankDetailView);
  console.log('masterDataSource', masterDataSource);

  const onHandleRetryBtn = () => {
    getLoanDetails();
  };
  const handlerSwipeLeft = () => {
    console.log('Swipe Left');
    if (activeTab == 'LoanEnquiry') {
      setActiveTab('LoanDetails');
    } else if (activeTab == 'LoanDetails') {
      checkBankDetailsExist();
    }
  };
  const handlerSwipeRight = () => {
    console.log('Swipe Right');
    if (activeTab == 'LoanEnquiry') {
      checkBankDetailsExist();
    } else if (activeTab == 'LoanDetails') {
      setActiveTab('LoanEnquiry');
    }
  };

  const configSwipe = {
    velocityThreshold: 0.3,
    directionalOffsetThreshold: 80,
  };
  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader
        bankDetailsEditForm={bankDetailsEditForm}
        isBankDetailsEditForm={isBankDetailsEditForm}
        isShowBankDetailView={isShowBankDetailView}
      />
      <ScrollView>
        <View
          style={styles.container}
          onStartShouldSetResponder={() => {
            if (propertyListData.length > 1) {
              controller.close();
            }
          }}>
          <AppTextBold style={styles.pageTitle}>Home Loan</AppTextBold>

          {propertyListData.length > 1 ? (
            <DropDownPicker
              items={propertyListData}
              defaultValue={parseInt(bookingId, 10)}
              autoScrollToDefaultValue={true}
              style={styles.dropdownDocument}
              placeholderStyle={styles.dropdownPlaceholder}
              itemStyle={styles.dropDownItem}
              labelStyle={styles.label}
              activeLabelStyle={styles.labelSelected}
              selectedLabelStyle={styles.labelSelected}
              onChangeItem={(item) => onValueChangeProperty(item.value)}
              placeholder="Select Property"
              controller={(instance) => (controller = instance)}
            />
          ) : propertyListData.length > 0 ? (
            <AppTextBold style={styles.dropdownDocument}>
              {propertyListData[0].label}
            </AppTextBold>
          ) : null}
          <View style={styles.tabName}>
            <GestureRecognizer
              onSwipeLeft={() => handlerSwipeLeft()}
              onSwipeRight={() => handlerSwipeRight()}
              style={{
                flex: 1,
                flexDirection: 'row',
              }}
              config={configSwipe}>
              <TouchableOpacity
                style={
                  activeTab == 'LoanEnquiry'
                    ? styles.activeTab
                    : [styles.activeTab, {borderBottomWidth: 0}]
                }
                onPress={() => {
                  setActiveTab('LoanEnquiry');
                }}>
                <View>
                  <AppText
                    style={
                      activeTab == 'LoanEnquiry'
                        ? { fontWeight: 'bold', fontSize:appFonts.largeBold }
                        : { fontSize: appFonts.largeBold}
                    }>
                    Enquiry
                  </AppText>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  activeTab == 'LoanDetails'
                    ? styles.activeTab
                    : [styles.activeTab, {borderBottomWidth: 0}]
                }
                onPress={() => {
                  setActiveTab('LoanDetails');
                }}>
                <View>
                  <AppText
                    style={
                      activeTab == 'LoanDetails'
                        ? { fontWeight: 'bold', fontSize: appFonts.largeBold }
                        : { fontSize: appFonts.largeBold }
                    }>
                    Loan Details
                  </AppText>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  activeTab == 'Disbursal'
                    ? styles.activeTab
                    : [styles.activeTab, {borderBottomWidth: 0}]
                }
                onPress={() => {
                  // setActiveTab('Disbursal');
                  checkBankDetailsExist();
                }}>
                <View>
                  <AppText
                    style={
                      activeTab == 'Disbursal'
                        ? { fontWeight: 'bold', fontSize: appFonts.largeBold }
                        : { fontSize: appFonts.largeBold }
                    }>
                    Disbursal
                  </AppText>
                </View>
              </TouchableOpacity>
            </GestureRecognizer>
          </View>
          {activeTab == 'LoanEnquiry' ? (
            !isLoading ? (
              <>
                <View style={styles.searchSection}>
                  <Image
                    style={styles.searchIcon}
                    source={require('./../assets/images/icon-search.png')}
                  />
                  <TextInput
                    style={styles.searchBox}
                    onChangeText={(text) => searchFilterFunction(text)}
                    value={search}
                    placeholder="Search"
                    underlineColorAndroid="transparent"
                  />
                </View>
                <View>
                  <View style={styles.bankCount}>
                    <AppText style={{color: colors.gray4}}>
                      {filteredDataSource ? filteredDataSource.length : 0} Banks
                    </AppText>
                  </View>
                  <FlatList
                    nestedScrollEnabled
                    data={filteredDataSource ? filteredDataSource : ''}
                    renderItem={({item, index}) => (
                      <View style={styles.bankListItem} key={index}>
                        <View style={{flex: 0.2}}>
                          <Text style={styles.bullets}>{'\u2022' + ' '}</Text>
                        </View>
                        <View style={{flex: 1}}>
                          <AppText style={styles.bankListTitle}>
                            {item.Bank_name}
                          </AppText>
                        </View>
                        <View style={{flex: 1}}>
                          <AppText
                            style={styles.enquiryBtn}
                            onPress={() => loanEnquiry(item.Bank_id)}>
                            ENQUIRE NOW
                          </AppText>
                        </View>
                      </View>
                    )}
                    keyExtractor={(item) => item.Bank_id}
                  />
                </View>
                <View>
                  <View>
                    <AppText style={styles.disclaimerTitle}>DISCLAIMER</AppText>
                  </View>
                  <View>
                    <AppText style={styles.disclaimerText}>
                      {propertyDisclaimer}
                    </AppText>
                  </View>
                </View>
              </>
            ) : null
          ) : activeTab == 'LoanDetails' ? (
            <BankDetails
              bookingId={bookingId}
              activeTab={activeTab}
              setIsLoading={setIsLoading}
              isLoading={isLoading}
              isBankDetailsEditForm={isBankDetailsEditForm}
              showBankDetailView={showBankDetailView}
            />
          ) : (
            <LoanDisbursal
              bookingId={bookingId}
              setActiveTab={setActiveTab}
              activeTab={activeTab}
              setIsLoading={setIsLoading}
              isLoading={isLoading}
            />
          )}
        </View>
      </ScrollView>
      <AppFooter isPostSales={true} />
      <AppOverlayLoader isLoading={isLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  itemContainer: {
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    // alignItems: 'center',
    // paddingHorizontal: 24,
    // paddingVertical: 5,
    // marginTop: 24,
    // marginVertical: 20,
    // backgroundColor: '#f7f7f7',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: colors.gray4,
    paddingVertical: 20,
    paddingRight: 20,
  },
  itemTxt: {
    color: colors.jaguar,
    marginRight: 5,
  },
  arrow: {height: 16, width: 16},
  searchBox: {flex: 1},
  searchIcon: {
    padding: 10,
    margin: 5,
    height: 16,
    width: 16,
    resizeMode: 'stretch',
    alignItems: 'center',
  },

  searchSection: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderColor: 'gray',
    borderBottomWidth: 2,
    // marginBottom: 20,
  },
  bankListItem: {
    // paddingBottom: 5,
    // paddingTop: 10,
    // marginVertical: 10,
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  bankListTitle: {},
  enquiryBtn: {
    textAlign: 'right',
    color: '#000',
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  bankCount: {
    marginTop: '5%',
    borderBottomColor: colors.veryLightGray,
    borderBottomWidth: 1,
    paddingVertical: 10,
  },
  disclaimerTitle: {
    color: colors.gray3,
    marginTop: '5%',
    borderBottomColor: colors.veryLightGray,
    borderBottomWidth: 1,
    marginVertical: 10,
  },
  disclaimerText: {
    color: colors.gray3,
  },
  bulletsFlex: {
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bullets: {
    color: '#000',
    fontSize: 32,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalContainer: {
    alignSelf: 'center',
    height: 20,
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 29,
  },
  modalView: {
    backgroundColor: 'white',
    width: '90%',
    borderRadius: 10,
    padding: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalText: {
    marginBottom: 20,
    textAlign: 'center',
    fontSize: appFonts.xxlargeFontSize,
    fontWeight: 'bold',
  },
  close: {
    left: '95%',
    color: 'grey',
  },
  tabName: {
    paddingBottom: '3%',
    marginBottom: '5%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  activeTab: {
    display: 'flex',
    width: '33%',
    paddingVertical: '5%',
    borderBottomWidth: 3,
    borderBottomColor: colors.gray,
    alignItems: 'center',
  },
  dropdownDocument: {
    height: 50,
    marginTop: 15,
    marginBottom: 8,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderTopWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropdownPlaceholder: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
  },
  labelSelected: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
    marginBottom: 8,
  },
  label: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    marginBottom: 8,
    textAlign: 'left',
  },
  dropDownItem: {
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGray,
    marginBottom: 8,
    marginTop: 4,
    justifyContent: 'flex-start',
  },
  propertyText: {
    width: '90%',
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
  },
});
export default LoanEnquiryScreen;
