import React, { useEffect, useState } from 'react';
import {
  View, StyleSheet, ScrollView, TextInput,
  Dimensions, Image, Text, TouchableOpacity, PermissionsAndroid
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import AppButton from '../components/ui/AppButton';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import Screen from '../components/Screen';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppFooter from '../components/ui/AppFooter';
import * as normStyle from '../styles/StyleSize';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import apiLogin from './../api/apiLogin';
import appSnakBar from '../utility/appSnakBar';
import { useNavigation } from '@react-navigation/native';
import ShowPassword from '../components/showPassword';
import { CommonActions } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { USER_LOGIN } from './../store/actions/userLoginAction';
import { GoogleAnalyticsTracker } from "react-native-google-analytics-bridge";
import Orientation from 'react-native-orientation';
import DropDownPicker from 'react-native-dropdown-picker';
import Modal from 'react-native-modal';
import AppFileChooser from './../components/actionSheet/AppFileChooser';
import appFileChooser from './../utility/appFileChooser';
import Selecteddocumentlistmodal from './modals/Selecteddocumentlistmodal'
import DocumentUploaded from '../screens/modals/DocumentUploaded'
import apiDocument from '../api/apiDocument';


const ChangeRegisteredAddress = ({ props, route }) => {

  const routeParams = route.params;

  const userId =
    routeParams && routeParams.userId && routeParams.userId
      ? routeParams.userId
      : '';

  const emplId =
    routeParams && routeParams.emplId && routeParams.emplId
      ? routeParams.emplId
      : '';

  const reg_country =
    routeParams && routeParams.reg_country && routeParams.reg_country
      ? routeParams.reg_country
      : '';

  const reg_state =
    routeParams && routeParams.reg_state && routeParams.reg_state
      ? routeParams.reg_state
      : '';

  const reg_city =
    routeParams && routeParams.reg_city && routeParams.reg_city
      ? routeParams.reg_city
      : '';

  const reg_zipcode =
    routeParams && routeParams.reg_zipcode && routeParams.reg_zipcode
      ? routeParams.reg_zipcode
      : '';
  const reg_street =
    routeParams && routeParams.reg_street && routeParams.reg_street
      ? routeParams.reg_street
      : '';


  const [oldpassword, Setoldpassword] = useState("");
  const [newpassword, Setnewpassword] = useState("");
  const [confirmpassword, Setconfirmpassword] = useState("");
  const [isApiLoading, setApiIsLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [disableButton, setDisableButton] = useState(true);
  const navigation = useNavigation();

  const [fileChooserResponse, setFileChooserResponse] = useState(null);
	const [fileChooser, setFileChooser] = useState(false);
	const [documentuploadedModal, setDocumentuploadedModal] = useState(false)
	const [documentselectedlistModal, setDocumentselectedlistModal] = useState(false)
	const [documentuploadedModalMSG, setDocumentuploadedModalMSG] = useState(false)
	const closeActionSheet = () => setFileChooser(false);
  const [filesToUpload, setFilesToUpload] = useState([]);

  const dispatch = useDispatch();

  const [noFilesAttach, SetNoOfAttach] = useState("");
  const [userName, SetUserName] = useState("");
  const [password, SetPassword] = useState("");

  const [newCountry, SetCountry] = useState("");
  const [newState, SetState] = useState("");
  const [newCity, SetCity] = useState("");
  const [newStreet, SetStreet] = useState("");
  const [newZipCode, SetZipCode] = useState("");

  var dropDownCountries = []
  var dropDownStates = [];
  var [dropDownCities, SetDropDownCities] = useState([]);

  var countryList = [];
  var stateList = [];
  var cityList = [];


  const FillCountryDropDown = async () => {
    const countryUrl = 'http://cp.godrejproperties.com/gplservice/CPService.svc/GetCountry';
    countryList = await apiLogin.apiGetRequest(countryUrl);
    //console.log("countryList", countryList.GetCountryjsonResult);
    var countries = countryList.GetCountryjsonResult;
    for (var i = 0; i < countries.length; i++) {
      dropDownCountries.push({ label: countries[i].CountryName, value: countries[i].CountryName })
    }
    // console.log("dropDownCountries", dropDownCountries);
  }

  const FillStateDropDown = async () => {
    const stateUrl = 'http://cp.godrejproperties.com/gplservice/CPService.svc/GetStateData/1';
    stateList = await apiLogin.apiGetRequest(stateUrl);
    //console.log("stateList", stateList.GetStateDatajsonResult);
    var states = stateList.GetStateDatajsonResult;
    for (var i = 0; i < states.length; i++) {
      dropDownStates.push({ label: states[i].StateName.replace("\n", ""), value: states[i].StateName.replace("\n", "") })
    }
    //console.log("dropDownStates", dropDownStates);
  }

  const FillCityDropDown = async (selectedState) => {
    dropDownCities = [];
    var selectedStateId = Object.values(stateList)[0].filter(x => x.StateName == selectedState.value)[0].StateId;
    //console.log("selectedStateId", selectedStateId);
    const cityUrl = 'http://cp.godrejproperties.com/gplservice/CPService.svc/GetCity/' + selectedStateId;
    cityList = await apiLogin.apiGetRequest(cityUrl);
    var cities = cityList.GetCityjsonResult;
    for (var i = 0; i < cities.length; i++) {
      dropDownCities.push({ label: cities[i].CityName, value: cities[i].CityName })
    }
    SetDropDownCities(dropDownCities);
    //console.log("dropDownCities", dropDownCities);
  }

  FillCountryDropDown();
  FillStateDropDown();

  const onChangeCountryDropDown = (selectedItem) => {
    SetCountry(selectedItem.value);
  }

  const onChangeStateDropDown = (selectedItem) => {
    SetState(selectedItem.value);
  }

  const onChangeCityDropDown = (selectedItem) => {
    SetCity(selectedItem.value);
  }

  const Submit = async () => {
    console.log("Street", newStreet);
    console.log("Country", newCountry);
    console.log("State", newState);
    console.log("City", newCity);
    console.log("ZipCode", newZipCode);
    console.log("File", filesToUpload[0].file_name);

    var url = 'https://0d5af0c8e517.ngrok.io/webapi/GetProfileData.ashx?userid=' + emplId
      + '&module=reg_addr&oldRegAddress=' + reg_street + '&newRegAddress=' + newStreet
      + '&oldRegCountry=' + reg_country + '&newRegCountry=' + newCountry
      + '&oldRegState=' + reg_state + '&newRegState=' + newState
      + '&oldRegCity=' + reg_city + '&newRegCity=' + newCity
      + '&oldRegZipCode=' + reg_zipcode + '&newRegZipCode=' + newZipCode
      + '&addrProof='+ filesToUpload[0].file_name;

    // console.log("url", url);
    var result = await apiLogin.apiGetRequest(url);
    console.log("result", result);
    if (result.data == "Updated") {
      appSnakBar.onShowSnakBar('Request sent for approval', 'LONG');
      navigation.goBack();
    }
  };

  const onClickFileChooserItem = async (clickItem) => {
    switch (clickItem) {
      case 'Remove':
        setFileChooser(false);
        setFileChooserResponse(null);
        break;

      case 'Gallery':
        appFileChooser.onLaunchImageLibrary((fileChooserResponse) => {
          setFileChooser(false);
          if (fileChooserResponse && fileChooserResponse.uri) {
            appFileChooser.toBase64(fileChooserResponse.uri).then((resp) => {
              base64(resp, fileChooserResponse.fileName ? fileChooserResponse.fileName : appConstant.getFileName(fileChooserResponse.uri));

            }).catch((err) =>
              console.log(err)
            );
          }
        });
        break;

      case 'Camera':
        appFileChooser.onLaunchCamera((fileChooserResponse) => {
          setFileChooser(false);
          if (fileChooserResponse && fileChooserResponse.uri) {
            appFileChooser.toBase64(fileChooserResponse.uri).then((resp) => {
              base64(resp, fileChooserResponse.fileName ? fileChooserResponse.fileName : appConstant.getFileName(fileChooserResponse.uri));

            }).catch((err) =>
              console.log(err)
            );
          }
        });
        break;

      case 'Phone':
        try {
          const chooserResponse = await appFileChooser.onSelectFile();
          const data = await appFileChooser.toBase64(chooserResponse.uri);
          setFileChooser(false);
          base64(data, chooserResponse.name)
        } catch (error) {
          setFileChooser(false);
          console.log('Error onSelectFile : ', error);
        }
        break;
    }

  };

  const base64 = (resp, name) => {
		// alert('File fetched successfully : ' + JSON.stringify(resp))
	  setIsLoading(true)
		resp = resp;
		name = name;
		const  files = filesToUpload;
		if(name == null || name == undefined || name == ''){
			name = appConstant.getFileName(imgPath);
		}
		files.push({ "file_base64_encoded": resp, "file_name": name })
		setFilesToUpload(files);
		// for(let x in files){console.log("file", files[x].file_name)}
		setTimeout(() => {
			setDocumentselectedlistModal(true);
		}, 200);
		setIsLoading(false)	
	}

  const api_Add_DOCUMENTS = () => {
		const requestData = JSON.stringify(filesToUpload);

		setIsLoading(true);
		if(filesToUpload.length == 1) {
      apiDocument.api_Add_DOCUMENTS(requestData).then((response) => {
        setIsLoading(false)
        setDocumentselectedlistModal(false);
        console.log("response", response);
        
        if (response.statusCode == "200") {
          const msg = 'Document has been attached. Please proceed with rest of the form.';
          //setDocumentuploadedModalMSG(msg);
          //setDocumentuploadedModal(true)
        }
      })
    }
    else{
      setIsLoading(false)
    }
    //setFilesToUpload([]);
	}

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);


  useEffect(() => {
    _getUserInfo();
  }, [loginvalue]);

  var tracker = new GoogleAnalyticsTracker("UA-171278332-1");

  const loginvalue = useSelector(
    (state) => state.loginInfo.loginResponse,
  );

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );
  const menuType = useSelector((state) => state.menuType.menuType);
  const goBack = () => {
    navigation.goBack();
  };

  const _getUserInfo = () => {

  };

  // const validatePassword = (pwd) => {
  //   var patt = new RegExp("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d!@#$%^*?%/'._\\-,\\\\`~]{6,14}$");
  //   var res = patt.test(pwd);
  //   if (pwd.length < 6) {
  //     appSnakBar.onShowSnakBar(`Password should be minimum 6 characters long`, 'LONG');
  //     return false;
  //   } else {
  //     if (pwd.length > 14) {
  //       appSnakBar.onShowSnakBar(`Password should be maximum 6 characters long`, 'LONG');
  //       return false;
  //     } else {
  //       if (res) {
  //         return true;
  //       } else {
  //         appSnakBar.onShowSnakBar(`Password should be alpha-numeric`, 'LONG');
  //         return false;
  //       }
  //     }
  //   }
  // };



  const onHandleRetryBtn = () => {
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />

      <ScrollView>
        <View style={styles.container}>

          <View style={{ width: '100%', alignItems: 'flex-start' }}>
            <AppText style={styles.pageTitle}>Update Registered Address</AppText>
          </View>

          <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(10) }}>
            <Text style={{ marginLeft: '5%', color: colors.gray }}>Existing Registered Address</Text>
          </View>
          <View style={{ width: '100%', marginLeft: '5%' }}>
            <AppText style={styles.text1}>{reg_street}</AppText>
            <AppText style={styles.text1}>{reg_country}, {reg_state}, {reg_city}, {reg_zipcode}</AppText>
          </View>


          <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(25) }}>
            <Text style={{ marginLeft: '5%', color: colors.gray }}>Registered Street *</Text>
          </View>
          <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', marginBottom: 10 }}>
            <TextInput
              style={styles.nameStyle}
              placeholder={"Enter Street"}
              placeholderTextColor={colors.gray}
              value={newStreet}
              onChangeText={e => {
                SetStreet(e);
              }}
            />
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <DropDownPicker
                items={dropDownCountries}
                style={styles.input}
                onChangeItem={(value) => {
                  onChangeCountryDropDown(value);
                }}
                placeholder={"Select Country"}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <DropDownPicker
                items={dropDownStates}
                style={styles.input}
                onChangeItem={(value) => {
                  onChangeStateDropDown(value);
                  FillCityDropDown(value);
                }}
                placeholder={"Select State"}
              />
            </View>
          </View>


          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <DropDownPicker
                items={dropDownCities}
                style={styles.input}
                onChangeItem={(value) => {
                  onChangeCityDropDown(value);
                }}
                placeholder={"Select City"}
              />
            </View>
          </View>

          <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(10) }}>
            <Text style={{ marginLeft: '5%', color: colors.gray }}>Communication ZipCode *</Text>
          </View>
          <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', marginBottom: 10 }}>
            <TextInput
              style={styles.nameStyle}
              placeholder={"Enter Zipcode"}
              placeholderTextColor={colors.gray}
              value={newZipCode}
              onChangeText={e => {
                SetZipCode(e);
              }}
            />
          </View>

          <View style={styles.headerContainer}>
            <AppText style={styles.pageTitle}> Upload Address Proof</AppText> 
            <Text style={{fontSize:11,  marginRight:10, alignItems: 'flex-start', marginTop:5}}>({filesToUpload.length} files selected)</Text>
            <TouchableOpacity onPress={() => setFileChooser(true)} >
              <Image
                style={styles.iconAdd}
                source={require('../assets/images/plus-icon.png')}
              />
            </TouchableOpacity>
            
          </View>

          {/* <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}> */}
          <View style={styles.button}>
            <AppButton
              color={'secondary'}
              // disabled={disableButton}
              textColor={'primary'}
              title="Submit"
              onPress={() => {
                Submit()
              }}
            />
            {/* </View> */}
          </View>


          {documentuploadedModal ? <DocumentUploaded msg={documentuploadedModalMSG} func={setDocumentuploadedModal} /> : null}
          {documentselectedlistModal && filesToUpload.length >= 1 ? <Selecteddocumentlistmodal func={setDocumentselectedlistModal} filesToUpload={filesToUpload} fileslength={filesToUpload.length} choosefileagain={() => {
            setDocumentselectedlistModal(false); setTimeout(() => {
              setFileChooser(true)
            }, 0);
          }} /*removeFile={removeFile}*/ onsubmitdocument={() => api_Add_DOCUMENTS()} aftercancelresetdata={setFilesToUpload} /> : null}
          <Modal
            isVisible={fileChooser}
            style={{
              margin: 0,
              justifyContent: 'flex-end',
            }}
            onBackButtonPress={closeActionSheet}
            onBackdropPress={closeActionSheet}
            backdropColor="rgba(0,0,0,0.5)">
            <AppFileChooser
              onPress={(clickItem) => onClickFileChooserItem(clickItem)}

            />
          </Modal>

        </View>
      </ScrollView>

      <AppFooter activePage={0} isPostSales={menuType == 'postsale' ? true : false} />
      <AppOverlayLoader isLoading={isLoading || isApiLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: normStyle.normalizeWidth(10),
  },
  formStyle: {
    flex: 1,
    alignSelf: 'flex-start',
    width: '100%',
    marginTop: 10,
    marginBottom: 10,
    paddingHorizontal: 20,
  },
  iconAdd: {
    height: 26,
    width: 26,
    marginTop: 1,
    marginRight: 16,
  },
  pageTitle: {
    fontSize: normStyle.normalizeWidth(20),
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginLeft: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(40),
  },
  nameStyle: {
    width: '90%', height: normStyle.normalizeWidth(40),
    borderBottomWidth: 0.5, fontSize: normStyle.normalizeWidth(18),
    fontSize: 13,
  },
  seprator: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  line: {
    width: '95%',
    height: normStyle.normalizeWidth(1),
    backgroundColor: colors.gray,
    marginTop: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(20),
  },
  viewStyle: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: normStyle.normalizeWidth(20),
  },
  imageView1: {
    width: normStyle.normalizeWidth(130),
    height: normStyle.normalizeWidth(105),
    flexDirection: 'row',
  },
  imageText: {
    width: normStyle.normalizeWidth(110),
    height: normStyle.normalizeWidth(110),
    borderRadius: normStyle.normalizeWidth(55),
    textAlign: 'center',
    textAlignVertical: 'center',
    fontWeight: 'bold',
    fontSize: normStyle.normalizeWidth(30),
  },
  editImg: {
    width: normStyle.normalizeWidth(20),
    height: normStyle.normalizeWidth(20),
    marginLeft: normStyle.normalizeWidth(10),
  },
  editImg1: {
    width: normStyle.normalizeWidth(20),
    height: normStyle.normalizeWidth(20),
    marginRight: normStyle.normalizeWidth(10),
  },
  text1: {
    marginTop: normStyle.normalizeWidth(7),
  },
  view1: {
    width: '100%', flexDirection: 'row'
  },
  view2: {
    width: '100%', flexDirection: 'row',
    marginTop: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(20),
  },
  verifiedImg: {
    width: normStyle.normalizeWidth(10),
    height: normStyle.normalizeWidth(10),
  },
  editProfileStyle: {
    flex: 0.2, bottom: 0, right: 0, position: 'absolute',
    paddingRight: normStyle.normalizeWidth(20)
  },
  cameraStyle: {
    width: normStyle.normalizeWidth(25),
    height: normStyle.normalizeWidth(25),
  },
  edit1: {
    flex: 0.2, alignItems: 'flex-end', paddingRight: '2.5%',
  },
  button: {
    // width: normStyle.normalizeWidth(100),
    // height: normStyle.normalizeWidth(0),
    alignItems: 'center', justifyContent: 'center',
    marginHorizontal: 15,
    paddingBottom: normStyle.normalizeWidth(40),
    paddingTop: normStyle.normalizeWidth(40),
  }
});

export default ChangeRegisteredAddress;
