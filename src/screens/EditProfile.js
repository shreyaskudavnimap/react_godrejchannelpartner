import React, { useEffect } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Dimensions,
  ImageBackground,
  Image,
} from 'react-native';
import {Formik} from 'formik';

import AppButton from '../components/ui/AppButton';
import FooterMenu from '../components/FotterMenu';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import Screen from '../components/Screen';
import DropDownPicker from 'react-native-dropdown-picker';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import Orientation from 'react-native-orientation';

var {height} = Dimensions.get('window');

const EditProfile = (props) => {

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  
  return (
    <>
      <Screen>
        <AppHeader />

        <View style={{height: (height / 100) * 83}}>
          <AppText style={styles.pageTitle}>MY PROFILE</AppText>

          <ScrollView>
            <View style={styles.container}>
              <TouchableOpacity>
                <ImageBackground
                  source={require('./../assets/images/no-image.jpg')}
                  imageStyle={{borderRadius: 50}}
                  style={styles.photo}>
                  <Image
                    source={require('./../assets/images/camera-icon-b.png')}
                    style={styles.cameraIcon}
                  />
                </ImageBackground>
              </TouchableOpacity>

              <View style={styles.formStyle}>
                <Formik
                  initialValues={{
                    salutations: 'Mrs',
                    FirstName: 'Arshi',
                    LastName: 'Arbab',
                    MobileNumber: '999999999',
                    EmailID: 'arbabarshi@gmail.com',
                    Password: '************',
                    country: 'IND',
                  }}>
                  {({handleChange, handleBlur, values}) => (
                    <View>
                      <AppText>Salutations</AppText>
                      <DropDownPicker
                        items={[
                          {label: 'MR', value: 'MR'},
                          {label: 'MRS', value: 'MRS'},
                        ]}
                        style={styles.dropDown}
                        placeholderStyle={{
                          fontSize: appFonts.xlargeFontSize,
                          fontWeight: 'bold',
                        }}
                        placeholder={values.salutations}
                      />

                      <AppText>First Name</AppText>
                      <TextInput
                        style={styles.input}
                        onChangeText={handleChange('FirstName')}
                        onBlur={handleBlur('FirstName')}
                        value={values.FirstName}
                      />

                      <AppText>Last Name</AppText>
                      <TextInput
                        style={styles.input}
                        onChangeText={handleChange('LastName')}
                        onBlur={handleBlur('LastName')}
                        value={values.LastName}
                      />

                      <View style={styles.imageContainer}>
                        <View style={styles.iconConatiner}>
                          <View
                            style={{
                              height: '40%',
                              width: '60%',
                              marginTop: 20,
                              right: 10,
                            }}>
                            <Image
                              source={require('./../assets/images/flag-icon.png')}
                              style={[
                                styles.icon,
                                {height: '100%', width: '100%'},
                              ]}
                            />
                          </View>
                        </View>

                        <View style={styles.dropDownContainer}>
                          <DropDownPicker
                            items={[
                              {label: 'IND', value: 'IND'},
                              {label: 'USA', value: 'USA'},
                            ]}
                            style={styles.dropdownCountry}
                            placeholderStyle={{
                              fontSize: appFonts.xlargeFontSize,
                              fontWeight: 'bold',
                            }}
                            placeholder={values.country}
                          />
                        </View>

                        <View
                          style={{
                            height: '100%',
                            width: '50%',
                            justifyContent: 'center',
                          }}>
                          <AppText>Mobile Number</AppText>
                          <TextInput
                            style={styles.phno}
                            onChangeText={handleChange('MobileNumber')}
                            onBlur={handleBlur('MobileNumber')}
                            value={values.MobileNumber}
                          />
                        </View>
                      </View>

                      <AppText style={{marginTop: 40}}>Email ID</AppText>
                      <TextInput
                        style={styles.input}
                        onChangeText={handleChange('EmailID')}
                        onBlur={handleBlur('EmailID')}
                        value={values.EmailID}
                      />

                      <View>
                        <AppText style={styles.update}>UPDATE PASSWORD</AppText>
                        <AppText>Old Password</AppText>
                        <TextInput
                          style={styles.input}
                          onChangeText={handleChange('Password')}
                          onBlur={handleBlur('Password')}
                          value={values.Password}
                        />

                        <AppText>New Password</AppText>
                        <TextInput
                          style={styles.input}
                          onChangeText={handleChange('Password')}
                          onBlur={handleBlur('Password')}
                          value={values.Password}
                        />

                        <AppText>Confirm Password</AppText>
                        <TextInput
                          style={styles.input}
                          onChangeText={handleChange('Password')}
                          onBlur={handleBlur('Password')}
                          value={values.Password}
                        />
                      </View>
                    </View>
                  )}
                </Formik>
              </View>
            </View>
          </ScrollView>

          <View style={styles.button}>
            <TouchableOpacity
              style={styles.cancle}
              onPress={() => props.navigation.goBack()}>
              <AppText style={styles.text}>CANCEL</AppText>
            </TouchableOpacity>
            <AppButton
              title="UPDATE"
              onPress={() => props.navigation.navigate('EditProfile')}
            />
          </View>
        </View>

        <FooterMenu {...props} />
      </Screen>
    </>
  );
};

export default EditProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  update: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    textTransform: 'uppercase',
    marginVertical: '3%',
    marginBottom: '10%',
    alignSelf: 'flex-start',
  },

  cancle: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 18,
    flexDirection: 'row',

    backgroundColor: '#FFFFFF',
    shadowColor: '#e0e0e0',
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15,
    shadowOffset: {width: 1, height: 20},
  },
  text: {
    fontSize: appFonts.largeBold,
    textTransform: 'uppercase',
    fontFamily: appFonts.SourceSansProBold,
  },
  button: {
    marginVertical: '5%',
    marginHorizontal: '5%',
  },
  photo: {
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    margin: '10%',
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: 5,
    textTransform: 'uppercase',
    marginHorizontal: 20,
  },
  icon: {
    width: '20%',
    height: '50%',
    marginTop: '3%',
    marginBottom: '3%',
  },

  formStyle: {
    flex: 1,
    alignSelf: 'flex-start',
  },
  iconText: {
    marginTop: '5.5%',
    marginHorizontal: '2%',
    paddingRight: 10,
  },
  input: {
    fontSize: appFonts.xlargeFontSize,
    fontWeight: 'bold',
    color: '#26262a',
    marginBottom: 40,
    borderBottomColor: '#d8d8d8',
    borderBottomWidth: 1,
  },
  cameraIcon: {
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    backgroundColor: '#ffffff',
    left: '70%',
    top: '70%',
  },
  dropDown: {
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    marginBottom: 40,
  },
  dropDownContainer: {
    height: '100%',
    width: '30%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    flexDirection: 'row',
    height: (height / 100) * 10,
    borderBottomColor: '#d8d8d8',
    marginLeft: '5%',
    marginRight: '5%',
  },
  iconConatiner: {
    height: '100%',
    width: '20%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropdownCountry: {
    width: '100%',
    height: '100%',
    margin: 20,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    marginBottom: 4,
    right: 20,
  },
  phno: {
    width: '100%',
    borderBottomColor: '#d8d8d8',
    borderBottomWidth: 1,
    fontSize: appFonts.xlargeFontSize,
    fontWeight: 'bold',
  },
});
