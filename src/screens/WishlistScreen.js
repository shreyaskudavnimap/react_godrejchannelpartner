import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  FlatList,
  Modal,
  Dimensions,
} from 'react-native';
import { EventRegister } from 'react-native-event-listeners';

//api
import WISH_LIST_API from '../api/apiWishlist';
import { useDispatch, useSelector } from 'react-redux';

import * as projectDetailsAction from './../store/actions/projectDetailsAction';
import * as continueExploringAction from './../store/actions/homeContinueExploringAction';
import * as cityWiseProjectsAction from './../store/actions/homeCityWiseProjectsAction';
import * as newLaunchAction from './../store/actions/homeNewLaunchAction';
import * as greenLivingAction from './../store/actions/homeGreenLivingAction';
import * as awardWiningAction from './../store/actions/homeAwardWiningAction';

//components
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import AppButton from '../components/ui/AppButton';
import WishlistComponent from '../components/WishlistComponent';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
//config
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import appSnakBar from '../utility/appSnakBar';
import Orientation from 'react-native-orientation';

const windowWidth = Dimensions.get('window').width;

const WishlistScreen = ({ navigation, route }) => {
  /**
   *
   * @constant {boolean} isInventory It is come from FAQ screen to default selection of inventory
   *
   */
  const isInventory = route.params
    ? route.params.isInventory
      ? true
      : false
    : false;
  const [isProject, isProjectSelected] = useState(isInventory ? false : true);
  const [projects, setProjects] = useState([]);
  const [inventories, setInventories] = useState([]);
  const [selectedBtn, setSelectedBtn] = useState(
    isInventory ? 'inventory' : 'projects',
  );
  const [isProjectsArrayLength, setIsProjectArrayLength] = useState(true);
  const [isInventoryArrayLength, setIsInventoryArrayLength] = useState(true);
  const [isShowLoader, setIsShowLoader] = useState(true);
  const [isInventoryVisit, setIsInventoryVisit] = useState(false);
  const [isDeletedModalVisible, setIsdeletedModalVisible] = useState(false);
  // const { loginInfo } = useSelector((state) => state);
  // const userId = loginInfo.loginResponse
  //   ? loginInfo.loginResponse.data.userid
  //   : '';
  const loginUserData = useSelector((state) => state.loginInfo.loginResponse);
  const userId = typeof loginUserData.data !== 'undefined' ? loginUserData.data.userid : '';

  const menuType = useSelector((state) => state.menuType.menuType);
  const dispatch = useDispatch();

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);


  useEffect(() => {
    if (isInventory) {
      setInventoryList();
    } else {
      setProjectList();
    }
  }, []);
  /**
   * @description Set project list
   */
  const setProjectList = () => {
    setIsShowLoader(true);
    WISH_LIST_API.apiWishlistItems({ user_id: userId })
      .then((response) => {
        setIsShowLoader(false);
        if (response.data && response.data.data) {
          const projectList = response.data.data;
          if (!projectList.length) {
            setIsProjectArrayLength(false);
          }
          setProjects(projectList);
        }
      })
      .catch((err) => {
        console.log('errrrrr', err);
      });
  };

  /**
   *
   * @description Set inventory list
   */
  const setInventoryList = () => {
    setIsShowLoader(true);
    WISH_LIST_API.apiWishlistItems({ user_id: userId, item_type: 'inventory' })
      .then((response) => {
        setIsShowLoader(false);
        setIsInventoryVisit(true);
        console.log('inventorylist', response);
        if (response.data && response.data.data) {
          const inventoryList = response.data.data;
          if (!inventoryList.length) {
            setIsInventoryArrayLength(false);
          }
          setInventories(inventoryList);
        }
      })
      .catch((err) => {
        console.log('erroe');
      });
  };

  const onPressMyPlans = () => {
    isProjectSelected(true);
    setSelectedBtn('projects');
    setProjectList();
  };

  const onPressMyUpdates = () => {
    isProjectSelected(false);
    setSelectedBtn('inventory');
    setInventoryList();
  };

  const groupProjectAndInventoryByCity = (data) => {
    const newData = [...data];
    const compare = (a, b) => {
      if (a.cities < b.cities) {
        return -1;
      }
      if (a.cities > b.cities) {
        return 1;
      }
      return 0;
    };
    const newArr = newData.sort(compare);
    const projectInSameCity = [];
    const modifiedProj_inventory = newArr.map((elem) => {
      const index = projectInSameCity.indexOf(elem.cities);
      if (index > -1) {
        elem.isSameCity = true;
      } else {
        projectInSameCity.push(elem.cities);
        elem.isSameCity = false;
      }
      return elem;
    });
    return modifiedProj_inventory;
  };

  const popUp = () => {
    const message = isProject
      ? 'The project has been removed from wishlist'
      : 'The inventory has been removed from wishlist';

    return (
      <View style={styles.modalContainer}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={isDeletedModalVisible}>
          <View style={styles.centeredView}>
            <View style={{ ...styles.modalView }}>
              <AppText style={{ color: '#ffffff', textAlign: 'center' }}>
                {message}
              </AppText>
            </View>
          </View>
        </Modal>
      </View>
    );
  };

  /**
   *
   * @param {array} project
   * @description delete project from wishlist
   */
  const deleteWishList = (project) => {
    setIsShowLoader(true);
    WISH_LIST_API.apiAddRemoveWishlist({
      user_id: userId,
      proj_id: project.proj_id,
      type: 'delete',
    })
      .then((result) => {
        if (result.data && result.data.status && result.data.status == '200') {
          EventRegister.emit('wishlistUpdateEvent', 'update');

          setIsShowLoader(false);
          // setIsdeletedModalVisible(true);
          // autometicallyHideDeletedModal(3000);

          const currentProjectIndex = projects.findIndex((elem) => {
            return elem.proj_id == project.proj_id;
          });
          projects.splice(currentProjectIndex, 1);
          setProjects([...projects]);

          dispatch(projectDetailsAction.updateProjetWishlist());
          dispatch(
            continueExploringAction.updateProjetWishlist(project.proj_id),
          );
          dispatch(
            cityWiseProjectsAction.updateProjetWishlist(project.proj_id),
          );
          dispatch(newLaunchAction.updateProjetWishlist(project.proj_id));
          dispatch(greenLivingAction.updateProjetWishlist(project.proj_id));
          dispatch(awardWiningAction.updateProjetWishlist(project.proj_id));

          appSnakBar.onShowSnakBar(
            result.data.msg
              ? result.data.msg
              : 'The project has been removed from wishlist',
            'LONG',
          );
        }
      })
      .catch(() => {
        setIsShowLoader(false);
      });
  };
  /**
   *
   * @param {number} time
   * @description Hide successdeletemodal after some time
   */
  const autometicallyHideDeletedModal = (time) => {
    setTimeout(() => {
      setIsdeletedModalVisible(false);
    }, time);
  };
  /**
   *
   * @param {object} project
   * @description Delete project from inventory list
   */
  const deleteInventoryList = (project) => {
    setIsShowLoader(true);
    WISH_LIST_API.apiAddRemoveWishlist({
      user_id: userId,
      proj_id: project.proj_id,
      type: 'delete',
      item_type: 'inventory',
      inventory_id: project.nid,
    })
      .then((result) => {
        if (result.data && result.data.status && result.data.status == '200') {
          EventRegister.emit('wishlistUpdateEvent', 'update');

          setIsShowLoader(false);
          // setIsdeletedModalVisible(true);
          // autometicallyHideDeletedModal(3000);
          const currentProjectIndex = inventories.findIndex((elem) => {
            return elem.proj_id == project.proj_id;
          });
          inventories.splice(currentProjectIndex, 1);
          setInventories([...inventories]);

          appSnakBar.onShowSnakBar(
            result.data.msg
              ? result.data.msg
              : 'The inventory has been removed from wishlist',
            'LONG',
          );
        }
      })
      .catch(() => {
        setIsShowLoader(false);
      });
  };
  /**
   *
   * @description If no data found return text
   * @returns {Jsx}
   */
  const noDataFoundText = () => {
    if (isProject) {
      if (!projects.length && !isProjectsArrayLength) {
        return <Text>No project found</Text>;
      }
    } else {
      if (!inventories.length && !isInventoryArrayLength) {
        return <Text>No inventory found</Text>;
      }
    }
  };

  const onHandleRetryBtn = () => {
    if (isInventory) {
      setInventoryList();
    } else {
      setProjectList();
    }
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />

      <View style={styles.container}>
        <AppTextBold style={styles.pageTitle}>MY WISHLIST</AppTextBold>

        <View
          style={{
            // width: '60%',
            width: windowWidth * 0.35,
            paddingVertical: 12,
            flexDirection: 'row',
            justifyContent: 'flex-start',
          }}>
          <TouchableOpacity
            style={
              selectedBtn == 'projects' ? styles.btnPress : styles.btnNormal
            }
            onPress={onPressMyPlans}>
            <AppTextBold
              style={
                selectedBtn == 'projects'
                  ? styles.activeTab
                  : styles.inactiveTab
              }
              textBreakStrategy="simple"
              ellipsizeMode="tail">
              Projects
            </AppTextBold>
          </TouchableOpacity>
          <TouchableOpacity
            style={
              selectedBtn === 'inventory' ? styles.btnPress : styles.btnNormal
            }
            onPress={onPressMyUpdates}>
            <AppTextBold
              style={
                selectedBtn == 'inventory'
                  ? styles.activeTab
                  : styles.inactiveTab
              }
              textBreakStrategy="simple"
              ellipsizeMode="tail">
              Inventory
            </AppTextBold>
          </TouchableOpacity>
        </View>
        {noDataFoundText()}
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          data={
            isProject
              ? groupProjectAndInventoryByCity(projects)
              : groupProjectAndInventoryByCity(inventories)
          }
          renderItem={({ item }) => (
            <WishlistComponent
              deleteWishList={deleteWishList}
              isProject={isProject}
              deleteInventoryList={deleteInventoryList}
              isProject={isProject}
              item={item}
            />
          )}
        />
        <AppOverlayLoader isLoading={isShowLoader} />
      </View>

      {popUp(5000)}
      <AppFooter activePage={0} isPostSales={menuType == 'postsale' ? true : false} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 18,
    paddingTop: 10,
    marginLeft: '3%',
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: '3%',
    textTransform: 'uppercase',
  },
  btnNormal: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 12,
    paddingVertical: 18,
    width: '100%',
    marginVertical: 9,
    marginRight: 15,
    borderRadius: 1,
    borderColor: colors.lightGray,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 5,
    elevation: 10,
    backgroundColor: colors.primary,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 350,
  },
  modalContainer: {
    alignSelf: 'center',
    height: 50,
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 29,
  },
  modalView: {
    backgroundColor: '#333333',
    width: '90%',
    height: 70,
    borderRadius: 40,
    padding: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  close: {
    left: '70%',
    color: 'grey',
  },
  btnPress: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 12,
    paddingVertical: 18,
    width: '100%',
    marginVertical: 9,
    marginRight: 15,
    borderRadius: 1,
    backgroundColor: colors.jaguar,
  },
  activeTab: {
    color: colors.primary,
    fontSize: appFonts.largeBold,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inactiveTab: {
    color: colors.secondary,
    fontSize: appFonts.largeBold,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  }
});

export default WishlistScreen;
