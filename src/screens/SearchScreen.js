import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  ScrollView,
  Dimensions,
  Modal,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import SearchListComponent from '../components/SearchListComponent';
import AppSearchListFilterListItem from '../components/ui/AppSearchListFilterListItem';
import TermConditionScreen from '../screens/modals/TermConditionScreen';
import SelectLocationModalScreen from './modals/SelectLocationModalScreen';
import appSnakBar from '../utility/appSnakBar';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import appCurrencyFormatter from './../utility/appCurrencyFormatter';

import * as propertySearchAction from './../store/actions/propertySearchAction';
import apiWishlist from '../api/apiWishlist';
import * as continueExploringAction from '../store/actions/homeContinueExploringAction';
import * as cityWiseProjectsAction from '../store/actions/homeCityWiseProjectsAction';
import * as newLaunchAction from '../store/actions/homeNewLaunchAction';
import * as greenLivingAction from '../store/actions/homeGreenLivingAction';
import * as awardWiningAction from '../store/actions/homeAwardWiningAction';
import appConstant from '../utility/appConstant';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {clearRMData} from '../store/actions/userLoginAction';
import {StackActions} from '@react-navigation/native';
import GetLocation from 'react-native-get-location';
import AppText from '../components/ui/ AppText';
import {
  setBudgetData,
  setCityTermMenu,
  setLastSearchPagination,
  setProjectStatus,
  setTypologyTermMenu,
} from './../store/actions/propertySearchAction';
import {EventRegister} from 'react-native-event-listeners';
import Orientation from 'react-native-orientation';

var projectsData = [];
var budgetPriceList = [];
var priceRaceList = [];
var selected_price = [];

const addToWishList = (index, userId, setUpdateView, updateView) => {
  return (dispatch) => {
    const item = projectsData[index];
    try {
      const requestData = {
        user_id: userId,
        proj_id: item.id,
        type: item.wishlist_status === '0' ? 'add' : 'delete',
      };
      projectsData[index].wishlist_status =
        item.wishlist_status === '0' ? '1' : '0';
      setUpdateView(!updateView);
      apiWishlist
        .apiAddRemoveWishlist(requestData)
        .then((wishlistItem) => {
          if (wishlistItem.data) {
            if (
              wishlistItem.data.status &&
              wishlistItem.data.status === '200'
            ) {
              appSnakBar.onShowSnakBar(
                wishlistItem.data.msg ? wishlistItem.data.msg : 'Wishlist...',
                'LONG',
              );

              dispatch(
                continueExploringAction.updateProjetWishlist(item.proj_id),
              );
              dispatch(
                cityWiseProjectsAction.updateProjetWishlist(item.proj_id),
              );
              dispatch(newLaunchAction.updateProjetWishlist(item.proj_id));
              dispatch(greenLivingAction.updateProjetWishlist(item.proj_id));
              dispatch(awardWiningAction.updateProjetWishlist(item.proj_id));
            } else {
              appSnakBar.onShowSnakBar(
                wishlistItem.data.msg
                  ? wishlistItem.data.msg
                  : appConstant.appMessage.APP_GENERIC_ERROR,
                'LONG',
              );
              projectsData[index].wishlist_status =
                projectsData[index].wishlist_status === '0' ? '1' : '0';
              setUpdateView(!updateView);
            }
          } else {
            appSnakBar.onShowSnakBar(
              appConstant.appMessage.APP_GENERIC_ERROR,
              'LONG',
            );
            projectsData[index].wishlist_status =
              projectsData[index].wishlist_status === '0' ? '1' : '0';
            setUpdateView(!updateView);
          }
        })
        .catch((error) => {
          console.log(error);
          projectsData[index].wishlist_status =
            projectsData[index].wishlist_status === '0' ? '1' : '0';
          setUpdateView(!updateView);
          appSnakBar.onShowSnakBar(
            appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        });
    } catch (error) {
      console.log(error);
      projectsData[index].wishlist_status =
        projectsData[index].wishlist_status === '0' ? '1' : '0';
      setUpdateView(!updateView);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };
};

const logOut = async (navigation, dispatch) => {
  await AsyncStorage.removeItem('sessionId');
  await AsyncStorage.removeItem('sessionName');
  await AsyncStorage.removeItem('token');
  await AsyncStorage.removeItem('data');
  await AsyncStorage.removeItem('status');
  await AsyncStorage.removeItem('isLogin');
  await AsyncStorage.removeItem('rmData');
  dispatch(clearRMData());

  // navigation.dispatch(
  //   StackActions.replace('Login', {
  //     backRoute: 'AppStackRoute',
  //     action: 'wishlist_set',
  //     screen: 'SearchFilter',
  //   }),
  // );

  navigation.dispatch(
    StackActions.push('Login', {
      backRoute: 'AppStackRoute',
      action: 'wishlist_set',
      screen: 'SearchFilter',
    }),
  );
};

const getLocationData = async (setLocationData) => {
  let cityName = 'Mumbai';

  try {
    const {latitude, longitude} = GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    });
    const apiKey = appConstant.googleMapKey.key;
    const URL = `https://maps.googleapis.com/maps/api/geocode/json?address=${latitude},${longitude}&key=${apiKey}`;
    try {
      const response = await fetch(URL);
      const responseJson = await response.json();
      if (
        responseJson &&
        responseJson.status &&
        responseJson.status === 'OK' &&
        responseJson.results &&
        responseJson.results.length > 0 &&
        responseJson.results[0].address_components &&
        responseJson.results[0].address_components.length > 0
      ) {
        const addressComponents = responseJson.results[0].address_components;
        const cityResult = addressComponents.findIndex((elements) =>
          elements.types.includes('locality'),
        );

        if (cityResult > -1) {
          cityName = addressComponents[cityResult].long_name;
        }
      }
      setLocationData({city: cityName, lat: latitude, lng: longitude});
    } catch (e) {}
  } catch (e) {}
};

const getLastDesect = (data, type) => {
  switch (type) {
    case 'location':
    case 'possession':
    case 'typology':
      return data.filter((elm) => {
        return !elm.selected;
      });
    case 'price':
      return [{min: data.min}, {max: data.max}];
  }
};

const SearchScreen = (props) => {
  const [isLoading, setIsLoading] = useState(true);
  const [isInnerLoading, setIsInnerLoading] = useState(false);
  const [error, setError] = useState();
  const [searchResult, setsearchResult] = useState([]);
  const [City, setCity] = useState('');
  const [selectModalVisible, setSelectModalVisible] = useState(false);
  const [selectModalPossession, setSelectModalPossession] = useState(false);
  const [selectModalTypology, setSelectModalTypology] = useState(false);
  const [selectModalPrice, setSelectModalPrice] = useState(false);
  const [selectedCity, setSelectedCity] = useState([]);
  const [selectedPossession, setSelectedPossession] = useState([]);
  const [selectedprice, setSelectedPrice] = useState([]);
  const [selectedTypoloy, setSelectedTypoloy] = useState([]);
  const [displayCity, setDisplayCity] = useState('Location');
  const [displayPossession, SetdisplayPossession] = useState('Possession');
  const [displayTypology, SetdisplayTypology] = useState('Typology');
  const [displayPrice, SetdisplayPrice] = useState('Price Range');
  const [updateView, setUpdateView] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [locationData, setLocationData] = useState({
    city: 'Mumbai',
    lat: 19.076,
    lng: 72.8777,
  });

  const {deviceUniqueId, userId} = useSelector((state) => ({
    propertySearch: state.propertySearch,
    deviceUniqueId: state.deviceInfo.deviceUniqueId,
    userId: state.loginInfo.loginResponse.isLogin
      ? state.loginInfo.loginResponse.data.userid
      : '',
  }));

  const propertySearchData = useSelector((state) => state.propertySearch);

  const searchResultData = propertySearchData.searchResult.list_data;
  const cityTermMenu = propertySearchData.cityTermMenu;
  const projectStatus = propertySearchData.projectStatus;
  const typologyTermMenu = propertySearchData.typologyTermMenu;
  let projectStatus_selected = propertySearchData.projectStatus_selected;
  let typologyData_selected = propertySearchData.typologyData_selected;
  let resultPrice = propertySearchData.resultPrice;
  let selected_location = propertySearchData.selected_city;

  const dispatch = useDispatch();

  // useEffect(() => {
  //
  //   selectedCityText = '';
  //   let selectedCityLength = 0;
  //   for (let city of cityTermMenu) {
  //     if (city.selected) {
  //       if (selectedCityText === '') {
  //         selectedCityText = city.name;
  //       } else {
  //         selectedCityLength++;
  //       }
  //     }
  //   }
  //
  //   if (selectedCityLength > 0) {
  //     selectedCityText += ` (+${selectedCityLength})`
  //   }
  //
  //
  //
  // },[cityTermMenu, projectStatus, typologyTermMenu]);

  // useEffect(() => {
  //   return props.navigation.addListener('focus', () => {
  //
  //     // getLocationData(setLocationData);
  //   });
  // }, []);

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
    setIsLoading(false);
    if (!projectsData || projectsData.length === 0) {
      setCurrentPage(0);
    }
    let project_data = {};
    projectsData = [];
    if (searchResultData) {
      for (let data of searchResultData) {
        let p_d = '';
        let t_d = '';
        let am_d = '';
        let format = appCurrencyFormatter.getSearchCurrencyFormat;
        data.field_possession_status
          ? (p_d = data.field_possession_status + ' | ')
          : (p_d = '');
        data.field_typology_tags
          ? (t_d = data.field_typology_tags + ' | ')
          : (t_d = '');
        data.field_inventory_price
          ? (am_d = ' ₹ ' + format(data.field_inventory_price))
          : (am_d = '');
        project_data = {
          id: data.nid ? data.nid : data.id,
          city: data.field_city,
          title: data.title,
          imageUri: data.project_image,
          address: data.field_city,
          details: p_d + t_d + am_d,
          wishlist_status: data.wishlist_status,
        };
        projectsData.push(project_data);
        setsearchResult(searchResult.concat(projectsData));
      }
    }
    let cityDisplayText = '';
    let cityMenuLength = 0;
    for (let city of cityTermMenu) {
      if (city.selected) {
        if (cityDisplayText === '') {
          cityDisplayText = city.name;
        } else {
          cityMenuLength++;
        }
      }
    }
    if (cityMenuLength > 0) {
      cityDisplayText += ` (+${cityMenuLength})`;
    }
    if (cityDisplayText === '') {
      cityDisplayText = 'Location';
    }
    setDisplayCity(cityDisplayText);

    let possessionDisplayText = '';
    let possessionMenuLength = 0;
    for (let pos of projectStatus) {
      if (pos.selected) {
        if (possessionDisplayText === '') {
          possessionDisplayText = pos.name;
        } else {
          possessionMenuLength++;
        }
      }
    }
    if (possessionMenuLength > 0) {
      possessionDisplayText += ` (+${possessionMenuLength})`;
    }

    if (possessionDisplayText === '') {
      possessionDisplayText = 'Possession';
    }
    SetdisplayPossession(possessionDisplayText);

    let typoDisplayText = '';
    let typoMenuLength = 0;
    for (let typ of typologyTermMenu) {
      if (typ.selected) {
        if (typoDisplayText === '') {
          typoDisplayText = typ.name;
        } else {
          typoMenuLength++;
        }
      }
    }
    if (typoMenuLength > 0) {
      typoDisplayText += ` (+${typoMenuLength})`;
    }
    if (typoDisplayText === '') {
      typoDisplayText = 'Typology';
    }
    SetdisplayTypology(typoDisplayText);

    let displayTextPrice =
      appCurrencyFormatter.getSearchCurrencyFormat(resultPrice[0]) +
      '-' +
      appCurrencyFormatter.getSearchCurrencyFormat(resultPrice[1]);
    let maxCurr = appCurrencyFormatter.getSearchCurrencyFormat(resultPrice[1]);
    if (displayTextPrice === '0-0' || maxCurr !== maxCurr) {
      displayTextPrice = 'Price';
    }
    SetdisplayPrice(displayTextPrice);
  }, [searchResultData]);
  useEffect(() => {
    if (projectStatus_selected.length > 0) {
    }
  }, [projectStatus_selected]);

  useEffect(() => {
    setPriceRange();
  }, [resultPrice]);

  useEffect(() => {
    const eventListener = EventRegister.addEventListener(
      'SearchFilterUpdateEvent',
      (data) => {
        if (data && data === 'updateList') {
          if (userId) {
            wishlistItems(userId);
          }
        }
      },
    );

    return () => {
      EventRegister.removeEventListener(eventListener);
    };
  }, [dispatch, userId]);

  const wishlistItems = async (user_id) => {
    try {
      setIsInnerLoading(true);

      const requestData = {
        user_id: user_id,
      };
      const wishlistItems = await apiWishlist.apiWishlistItems(requestData);

      if (wishlistItems.data) {
        if (wishlistItems.data.status && wishlistItems.data.status == '200') {
          const responseItems = wishlistItems.data.data;
          if (responseItems && responseItems.length > 0) {
            console.log('projectsData: ', projectsData);
            projectsData.map((mapItem) => {
              responseItems.map((dataItem) => {
                if (mapItem.id == dataItem.proj_id) {
                  console.log(
                    'findIndex: ',
                    mapItem.id + ' -- ' + dataItem.proj_id,
                  );
                  mapItem.wishlist_status =
                    mapItem.wishlist_status == '0' ? '1' : '0';
                }
              });
            });

            dispatch(propertySearchAction.updateLastDesect(responseItems));

            dispatch(
              continueExploringAction.updateProjetWishlistArr(responseItems),
            );
            dispatch(
              cityWiseProjectsAction.updateProjetWishlistArr(responseItems),
            );
            dispatch(newLaunchAction.updateProjetWishlistArr(responseItems));
            dispatch(greenLivingAction.updateProjetWishlistArr(responseItems));
            dispatch(awardWiningAction.updateProjetWishlistArr(responseItems));
          }
        } else {
          appSnakBar.onShowSnakBar(
            wishlistItems.data.msg
              ? wishlistItems.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }

        setIsInnerLoading(false);
      } else {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      setIsInnerLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const onHandleOpenLocationModal = () => {
    if (cityTermMenu && cityTermMenu.length > 0) {
      setSelectedCity([]);
      setSelectModalVisible(true);
    } else {
      appSnakBar.onShowSnakBar('No location found', 'LONG');
    }
  };

  const onHandleOpenPossessionModal = () => {
    if (projectStatus && projectStatus.length > 0) {
      setSelectedPossession([]);
      setSelectModalPossession(true);
    } else {
      appSnakBar.onShowSnakBar('No possession found', 'LONG');
    }
  };

  const onHandleOpenTypologyModal = () => {
    if (typologyTermMenu && typologyTermMenu.length > 0) {
      setSelectedTypoloy([]);
      setSelectModalTypology(true);
    } else {
      appSnakBar.onShowSnakBar('No typology found', 'LONG');
    }
  };

  const onHandleOpenPriceModal = () => {
    if (priceRaceList && priceRaceList.length > 0) {
      setSelectModalPrice(true);
    } else {
      appSnakBar.onShowSnakBar('No priceRange found', 'LONG');
    }
  };

  const setPriceRange = () => {
    // setSelectModalPrice(true)
    const minPriceValue = resultPrice[0];
    const maxPriceValue = resultPrice[1];
    let format = appCurrencyFormatter.getSearchCurrencyFormat;
    let divisibleVal = 1500000;
    if (parseInt(maxPriceValue) < 10000000) {
      divisibleVal = 1500000;
    } else {
      divisibleVal = 5000000;
    }
    let num = parseInt(maxPriceValue) / divisibleVal;
    if (parseInt(minPriceValue) < divisibleVal) {
      budgetPriceList.length = 0;

      budgetPriceList.push({
        label:
          '< ' + appCurrencyFormatter.getSearchCurrencyFormat(divisibleVal),
        checked: true,
        minPrice: parseInt(minPriceValue),
        maxPrice: divisibleVal,
      });
      budgetPriceList.length = 0;
      for (let i = 1; i <= num; i++) {
        if (parseInt(maxPriceValue) >= (i + 1) * divisibleVal) {
          budgetPriceList.push({
            label:
              appCurrencyFormatter.getSearchCurrencyFormat(i * divisibleVal) +
              ' - ' +
              appCurrencyFormatter.getSearchCurrencyFormat(
                (i + 1) * divisibleVal,
              ),
            checked: true,
            minPrice: i * divisibleVal,
            maxPrice: (i + 1) * divisibleVal,
          });
        } else {
          budgetPriceList.push({
            label:
              '> ' +
              appCurrencyFormatter.getSearchCurrencyFormat(i * divisibleVal),
            checked: false,
            minPrice: i * divisibleVal,
            maxPrice: (i + 1) * divisibleVal,
          });
        }
      }
    } else {
      budgetPriceList.length = 0;
      for (let i = 1; i <= num; i++) {
        if (parseInt(maxPriceValue) >= (i + 1) * divisibleVal) {
          budgetPriceList.push({
            label:
              appCurrencyFormatter.getSearchCurrencyFormat(i * divisibleVal) +
              ' - ' +
              appCurrencyFormatter.getSearchCurrencyFormat(
                (i + 1) * divisibleVal,
              ),
            checked: true,
            minPrice: i * divisibleVal,
            maxPrice: (i + 1) * divisibleVal,
          });
        } else {
          budgetPriceList.push({
            label:
              '> ' +
              appCurrencyFormatter.getSearchCurrencyFormat(i * divisibleVal),
            checked: false,
            minPrice: i * divisibleVal,
            maxPrice: (i + 1) * divisibleVal,
          });
        }
      }
    }

    let id = 0;
    priceRaceList.length = 0;
    for (let data of budgetPriceList) {
      let price_data = {
        id: id++,
        name: data.label,
        selected: data.checked,
        maxPrice: data.maxPrice,
        minPrice: data.minPrice,
      };
      priceRaceList.push(price_data);
    }
  };

  const onSelectCity = (city) => {
    cityTermMenu.map((cityItem) => {
      if (cityItem.id === city.id) {
        cityItem.selected = !cityItem.selected;
      }
    });
    if (city.selected) {
      selected_location.push(city);
    }
    setSelectedCity([...selectedCity, city]);
  };

  const onSelectPossession = (status) => {
    projectStatus.map((statusItem) => {
      if (statusItem.id === status.id) {
        statusItem.selected = !statusItem.selected;
      }
    });
    if (status.selected) {
      projectStatus_selected.push(status);
    }
    setSelectedPossession([...selectedPossession, status]);
  };

  const onSelectTypology = (type) => {
    typologyTermMenu.map((typeItem) => {
      if (typeItem.id === type.id) {
        typeItem.selected = !typeItem.selected;
      }
    });
    if (type.selected) {
      typologyData_selected.push(type);
    }

    setSelectedTypoloy([...selectedTypoloy, type]);
  };

  const onSelectPrice = (price) => {
    priceRaceList.map((pricevalue) => {
      if (pricevalue.id === price.id) {
        pricevalue.selected = !pricevalue.selected;
      }
    });
    console.log(priceRaceList);
    if (price.selected) {
      selected_price.push(price);
    }

    setSelectedPrice([...selected_price, price]);
  };

  const onCancelCityUpdate = () => {
    setSelectModalVisible(false);
    cityTermMenu.map((cityMainItem) => {
      selectedCity.map((citySelectedItem) => {
        if (cityMainItem.id === citySelectedItem.id) {
          cityMainItem.selected = false;
        }
      });
    });
    // dispatch(propertySearchAction.setCityTermMenu(cityTermMenu));
  };

  const onCancelPossessionUpdate = () => {
    setSelectModalPossession(false);
    projectStatus.map((StatusMainItem) => {
      projectStatus_selected.map((StatusSelectedItem) => {
        if (StatusMainItem.id === StatusSelectedItem.id) {
          StatusMainItem.selected = false;
        }
      });
    });
    // dispatch(propertySearchAction.setCityTermMenu(cityTermMenu));
  };

  const onCancelTypologyUpdate = () => {
    setSelectModalTypology(false);
    typologyTermMenu.map((TypeMainItem) => {
      typologyData_selected.map((TypeSelectedItem) => {
        if (TypeMainItem.id === TypeSelectedItem.id) {
          TypeMainItem.selected = false;
        }
      });
    });
    // dispatch(propertySearchAction.setCityTermMenu(cityTermMenu));
  };

  const onCancelPriceUpdate = () => {
    setSelectModalPrice(false);
    priceRaceList.map((PriceMainItem) => {
      selected_price.map((PriceSelectedItem) => {
        if (PriceMainItem.id === PriceSelectedItem.id) {
          PriceMainItem.selected = false;
        }
      });
    });
    // dispatch(propertySearchAction.setCityTermMenu(cityTermMenu));
  };

  const onSelectCityUpdate = () => {
    let displayText = '';
    setCurrentPage(0);
    selected_location = [];
    for (let item of cityTermMenu) {
      if (item.selected) {
        selected_location.push(item);
        if (displayText === '') {
          displayText = item.name;
        }
      }
    }
    if (selected_location.length > 1) {
      displayText += ` (+${selected_location.length - 1})`;
    }

    let selectedCityIDS = selected_location.filter(
      (elm) => elm.selected === true,
    );
    if (selectedCityIDS.length > 0) {
      selectedCityIDS = selectedCityIDS.map((elm) => elm.id);
    }

    let selectedPossessionNames = projectStatus_selected.filter(
      (elm) => elm.selected === true,
    );
    if (selectedPossessionNames.length > 0) {
      selectedPossessionNames = selectedPossessionNames.map((elm) => elm.name);
    }

    let selectedTypologyIDS = typologyData_selected.filter(
      (elm) => elm.selected === true,
    );
    if (selectedTypologyIDS.length > 0) {
      selectedTypologyIDS = selectedTypologyIDS.map((elm) => elm.id);
    }

    const lastDesect = getLastDesect(selected_location, 'location');

    if (userId === '') {
      dispatch(
        setCityTermMenu(
          '',
          deviceUniqueId,
          '1',
          selectedCityIDS.join(','),
          '',
          selectedPossessionNames.join(','),
          selectedTypologyIDS.join(','),
          resultPrice[0],
          resultPrice[1],
          'location',
          lastDesect,
          selected_location,
        ),
      );
      getLastSelectData(
        '',
        deviceUniqueId,
        '1',
        selected_location,
        '',
        '',
        resultPrice[0],
        resultPrice[1],
        projectStatus_selected,
        typologyData_selected,
        '',
        0,
      );
    } else {
      dispatch(
        setCityTermMenu(
          userId,
          '',
          '1',
          selectedCityIDS.join(','),
          '',
          selectedPossessionNames.join(','),
          selectedTypologyIDS.join(','),
          resultPrice[0],
          resultPrice[1],
          'location',
          lastDesect,
          selected_location,
        ),
      );
      getLastSelectData(
        userId,
        '',
        '1',
        selected_location,
        '',
        '',
        resultPrice[0],
        resultPrice[1],
        projectStatus_selected,
        typologyData_selected,
        '',
        0,
      );
    }
    if (displayText === '') {
      displayText = 'Location';
    }
    setDisplayCity(displayText);
    setSelectModalVisible(false);
  };

  const onSelectPossessionUpdate = () => {
    let displayText = '';
    setCurrentPage(0);
    projectStatus_selected = [];
    for (let item of projectStatus) {
      if (item.selected) {
        projectStatus_selected.push(item);
        if (displayText === '') {
          displayText = item.name;
        }
      }
    }

    if (projectStatus_selected.length > 1) {
      displayText += ` (+${projectStatus_selected.length - 1})`;
    }

    let selectedCityIDS = selected_location.filter(
      (elm) => elm.selected === true,
    );
    if (selectedCityIDS.length > 0) {
      selectedCityIDS = selectedCityIDS.map((elm) => elm.id);
    }

    let selectedPossessionNames = projectStatus_selected.filter(
      (elm) => elm.selected === true,
    );
    if (selectedPossessionNames.length > 0) {
      selectedPossessionNames = selectedPossessionNames.map((elm) => elm.name);
    }

    let selectedTypologyIDS = typologyData_selected.filter(
      (elm) => elm.selected === true,
    );
    if (selectedTypologyIDS.length > 0) {
      selectedTypologyIDS = selectedTypologyIDS.map((elm) => elm.id);
    }

    const lastDesect = getLastDesect(projectStatus_selected, 'possession');

    if (userId === '') {
      dispatch(
        setProjectStatus(
          '',
          deviceUniqueId,
          '1',
          selectedCityIDS.join(','),
          '',
          selectedPossessionNames.join(','),
          selectedTypologyIDS.join(','),
          resultPrice[0],
          resultPrice[1],
          'possession',
          lastDesect,
          projectStatus_selected,
        ),
      );
      getLastSelectData(
        '',
        deviceUniqueId,
        '1',
        selected_location,
        '',
        '',
        resultPrice[0],
        resultPrice[1],
        projectStatus_selected,
        typologyData_selected,
        '',
        0,
      );
    } else {
      dispatch(
        setProjectStatus(
          userId,
          '',
          '1',
          selectedCityIDS.join(','),
          '',
          selectedPossessionNames.join(','),
          selectedTypologyIDS.join(','),
          resultPrice[0],
          resultPrice[1],
          'possession',
          lastDesect,
          projectStatus_selected,
        ),
      );
      getLastSelectData(
        userId,
        '',
        '1',
        selected_location,
        '',
        '',
        resultPrice[0],
        resultPrice[1],
        projectStatus_selected,
        typologyData_selected,
        '',
        0,
      );
    }

    if (displayText === '') {
      displayText = 'Possession';
    }
    SetdisplayPossession(displayText);
    setSelectModalPossession(false);
  };

  const onSelectTypologyUpdate = () => {
    let displayText = '';
    setCurrentPage(0);
    typologyData_selected = [];
    for (let item of typologyTermMenu) {
      if (item.selected) {
        typologyData_selected.push(item);
        if (displayText === '') {
          displayText = item.name;
        }
      }
    }

    if (typologyData_selected.length > 1) {
      displayText += ` (+${typologyData_selected.length - 1})`;
    }

    let selectedCityIDS = selected_location.filter(
      (elm) => elm.selected === true,
    );
    if (selectedCityIDS.length > 0) {
      selectedCityIDS = selectedCityIDS.map((elm) => elm.id);
    }

    let selectedPossessionNames = projectStatus_selected.filter(
      (elm) => elm.selected === true,
    );
    if (selectedPossessionNames.length > 0) {
      selectedPossessionNames = selectedPossessionNames.map((elm) => elm.name);
    }

    let selectedTypologyIDS = typologyData_selected.filter(
      (elm) => elm.selected === true,
    );
    if (selectedTypologyIDS.length > 0) {
      selectedTypologyIDS = selectedTypologyIDS.map((elm) => elm.id);
    }

    const lastDesect = getLastDesect(typologyData_selected, 'typology');

    if (userId === '') {
      dispatch(
        setTypologyTermMenu(
          '',
          deviceUniqueId,
          '1',
          selectedCityIDS.join(','),
          '',
          selectedPossessionNames.join(','),
          selectedTypologyIDS.join(','),
          resultPrice[0],
          resultPrice[1],
          'typology',
          lastDesect,
          typologyData_selected,
        ),
      );
      getLastSelectData(
        '',
        deviceUniqueId,
        '1',
        selected_location,
        '',
        '',
        resultPrice[0],
        resultPrice[1],
        projectStatus_selected,
        typologyData_selected,
        '',
        0,
      );
    } else {
      dispatch(
        setTypologyTermMenu(
          userId,
          '',
          '1',
          selectedCityIDS.join(','),
          '',
          selectedPossessionNames.join(','),
          selectedTypologyIDS.join(','),
          resultPrice[0],
          resultPrice[1],
          'typology',
          lastDesect,
          typologyData_selected,
        ),
      );
      getLastSelectData(
        userId,
        '',
        '1',
        selected_location,
        '',
        '',
        resultPrice[0],
        resultPrice[1],
        projectStatus_selected,
        typologyData_selected,
        '',
        0,
      );
    }

    if (displayText === '') {
      displayText = 'Typology';
    }

    SetdisplayTypology(displayText);
    setSelectModalTypology(false);
  };

  const onSelectPriceUpdate = () => {
    let displayText = '';
    setCurrentPage(0);
    selected_price = [];
    for (let item of priceRaceList) {
      if (item.selected) {
        selected_price.push(item);
      }
    }

    let max = '';
    let min = '';
    if (selected_price.length > 0) {
      min = selected_price[0].minPrice;
      max = selected_price.pop().maxPrice;
      displayText =
        appCurrencyFormatter.getSearchCurrencyFormat(min) +
        '-' +
        appCurrencyFormatter.getSearchCurrencyFormat(max);
    }

    let selectedCityIDS = selected_location.filter(
      (elm) => elm.selected === true,
    );
    if (selectedCityIDS.length > 0) {
      selectedCityIDS = selectedCityIDS.map((elm) => elm.id);
    }

    let selectedPossessionNames = projectStatus_selected.filter(
      (elm) => elm.selected === true,
    );
    if (selectedPossessionNames.length > 0) {
      selectedPossessionNames = selectedPossessionNames.map((elm) => elm.name);
    }

    let selectedTypologyIDS = typologyData_selected.filter(
      (elm) => elm.selected === true,
    );
    if (selectedTypologyIDS.length > 0) {
      selectedTypologyIDS = selectedTypologyIDS.map((elm) => elm.id);
    }

    const lastDesect = getLastDesect({min, max}, 'price');

    if (userId === '') {
      dispatch(
        setBudgetData(
          '',
          deviceUniqueId,
          '1',
          selectedCityIDS.join(','),
          '',
          selectedPossessionNames.join(','),
          selectedTypologyIDS.join(','),
          min,
          max,
          'price',
          lastDesect,
          {min: min, max: max},
        ),
      );
      getLastSelectData(
        '',
        deviceUniqueId,
        '1',
        selected_location,
        '',
        '',
        min,
        max,
        projectStatus_selected,
        typologyData_selected,
        '',
        0,
      );
    } else {
      dispatch(
        setBudgetData(
          userId,
          '',
          '1',
          selectedCityIDS.join(','),
          '',
          selectedPossessionNames.join(','),
          selectedTypologyIDS.join(','),
          min,
          max,
          'price',
          lastDesect,
          {min: min, max: max},
        ),
      );
      getLastSelectData(
        userId,
        '',
        '1',
        selected_location,
        '',
        '',
        min,
        max,
        projectStatus_selected,
        typologyData_selected,
        '',
        0,
      );
    }
    if (displayText === '' || displayText === '0-0') {
      displayText = 'Price';
    }
    SetdisplayPrice(displayText);
    setSelectModalPrice(false);
  };

  const getLastSelectData = useCallback(
    async (
      userId,
      deviceId,
      project_status,
      location,
      lat,
      lng,
      minPrice,
      maxPrice,
      possession,
      typology,
      sublocation,
      page,
      lastStatus,
      lastType,
    ) => {
      setIsLoading(true);
      setError(null);
      try {
        if (userId === '') {
          await dispatch(
            propertySearchAction.setLastSearch(
              '',
              deviceId,
              project_status,
              location,
              '',
              '',
              minPrice.toString(),
              maxPrice.toString(),
              possession,
              typology,
              sublocation,
              page,
              () => {},
              () => {},
            ),
          );
          setIsLoading(false);
        } else {
          await dispatch(
            propertySearchAction.setLastSearch(
              userId,
              '',
              project_status,
              location,
              '',
              '',
              minPrice.toString(),
              maxPrice.toString(),
              possession,
              typology,
              sublocation,
              page,
              () => {},
              () => {},
            ),
          );
          setIsLoading(false);
        }

        // navigation.replace('SearchResult')
      } catch (err) {
        console.log('my err', err);
        appSnakBar.onShowSnakBar(err.message, 'LONG');
        props.navigation.goBack();
        setError(err.message);
        setIsLoading(false);
      }
    },
    [dispatch, setIsLoading, setError, isLoading, deviceUniqueId],
  );

  const onHandleRetryBtn = () => {
    props.navigation.goBack();
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />
      {isLoading ? (
        <View style={styles.container} />
      ) : (
        <View style={styles.container}>
          {/* <TermConditionScreen header="Terms & Conditions" content= {TermCondition} /> */}
          <AppTextBold style={styles.pageTitle}>
            {projectsData.length}{' '}
            {projectsData.length > 1 ? 'PROPERTIES' : 'PROPERTY'}
          </AppTextBold>

          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row', height: 70, marginBottom: 25}}>
              <AppSearchListFilterListItem
                title={displayCity}
                onPress={() => onHandleOpenLocationModal()}
              />

              <AppSearchListFilterListItem
                title={displayPossession}
                onPress={() => onHandleOpenPossessionModal()}
              />

              <AppSearchListFilterListItem
                title={displayTypology}
                onPress={() => onHandleOpenTypologyModal()}
              />

              <AppSearchListFilterListItem
                title={displayPrice}
                onPress={() => onHandleOpenPriceModal()}
              />
            </View>
          </ScrollView>

          <FlatList
            data={projectsData}
            ListEmptyComponent={
              <AppText style={{alignSelf: 'center'}}>Not project found</AppText>
            }
            showsVerticalScrollIndicator={false}
            renderItem={({item, index}) => (
              <SearchListComponent
                item={item}
                onPress={(itemRender) => {
                  let proj_id = item.id;
                  // props.navigation.navigate('ProjectDetails', {
                  //   item: {...item, proj_id: proj_id},
                  // });
                  props.navigation.dispatch(
                    StackActions.push('ProjectDetails', {
                      item: {...item, proj_id: proj_id},
                    }),
                  );
                }}
                onPressWishlist={() => {
                  if (userId === '') {
                    logOut(props.navigation, dispatch).then();
                  } else {
                    dispatch(
                      addToWishList(index, userId, setUpdateView, updateView),
                    );
                  }
                }}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            removeClippedSubviews={true}
            maxToRenderPerBatch={10}
            onEndReachedThreshold={0.7}
            updateCellsBatchingPeriod={100}
            initialNumToRender={10}
            windowSize={5}
            onEndReached={() => {
              if (currentPage < propertySearchData.searchResult.total_page) {
                let pageNo = currentPage + 1;

                if (userId === '') {
                  dispatch(
                    setLastSearchPagination(
                      '',
                      deviceUniqueId,
                      '1',
                      selected_location,
                      '',
                      '',
                      resultPrice[0].toString(),
                      resultPrice[1].toString(),
                      projectStatus_selected,
                      typologyData_selected,
                      '',
                      pageNo,
                      () => {
                        setCurrentPage(pageNo);
                      },
                      () => {
                        setCurrentPage(pageNo - 1);
                      },
                    ),
                  );
                } else {
                  dispatch(
                    setLastSearchPagination(
                      userId,
                      '',
                      '1',
                      selected_location,
                      '',
                      '',
                      resultPrice[0].toString(),
                      resultPrice[1].toString(),
                      projectStatus_selected,
                      typologyData_selected,
                      '',
                      pageNo,
                      () => {
                        setCurrentPage(pageNo);
                      },
                      () => {
                        setCurrentPage(pageNo - 1);
                      },
                    ),
                  );
                }
              }
            }}
          />
        </View>
      )}

      <Modal
        animationType="fade"
        transparent={true}
        visible={selectModalVisible}
        onRequestClose={() => {
          setSelectModalVisible(false);
          onCancelCityUpdate();
        }}>
        <SelectLocationModalScreen
          cities={cityTermMenu}
          onSelectCity={(city) => onSelectCity(city)}
          onCancel={() => {
            setSelectModalVisible(false);
            onCancelCityUpdate();
          }}
          onUpdate={onSelectCityUpdate}
          titleSelect="Select Location"
        />
      </Modal>

      <Modal
        animationType="fade"
        transparent={true}
        visible={selectModalPossession}
        onRequestClose={() => {
          onCancelPossessionUpdate();
          setSelectModalPossession(!selectModalPossession);
        }}>
        <SelectLocationModalScreen
          cities={projectStatus}
          onSelectCity={(item) => onSelectPossession(item)}
          onCancel={() => {
            onCancelPossessionUpdate();
            setSelectModalPossession(!selectModalPossession);
          }}
          onUpdate={onSelectPossessionUpdate}
          titleSelect="Select Possession"
        />
      </Modal>

      <Modal
        animationType="fade"
        transparent={true}
        visible={selectModalTypology}
        onRequestClose={() => {
          onCancelTypologyUpdate();
          setSelectModalTypology(!selectModalTypology);
        }}>
        <SelectLocationModalScreen
          cities={typologyTermMenu}
          onSelectCity={(item) => onSelectTypology(item)}
          onCancel={() => {
            onCancelTypologyUpdate();
            setSelectModalTypology(!selectModalTypology);
          }}
          onUpdate={onSelectTypologyUpdate}
          titleSelect="Select Typology"
        />
      </Modal>
      <Modal
        animationType="fade"
        transparent={true}
        visible={selectModalPrice}
        onRequestClose={() => {
          onCancelPriceUpdate();
          setSelectModalVisible(!selectModalPrice);
        }}>
        <SelectLocationModalScreen
          cities={priceRaceList}
          onSelectCity={(item) => onSelectPrice(item)}
          onCancel={() => {
            onCancelPriceUpdate();
            setSelectModalVisible(!selectModalPrice);
          }}
          onUpdate={onSelectPriceUpdate}
          titleSelect="Select Price"
        />
      </Modal>
      <AppOverlayLoader isLoading={isLoading || isInnerLoading} />
      <AppFooter activePage={2} isPostSales={false} isGoBackLink={true} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 18,
    paddingVertical: 10,
    // marginLeft: '3%',
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: '7%',
    textTransform: 'uppercase',
  },
  btnNormal: {
    backgroundColor: 'white',
    shadowColor: '#e0e0e0',
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15,
    shadowOffset: {width: 1, height: 20},
  },
  placeHolder: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  dropdownContainer: {
    height: '85%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default SearchScreen;
