import React, {useEffect} from 'react';
import {View, StyleSheet, Image, Dimensions, Platform} from 'react-native';
import Screen from '../components/Screen';
import colors from '../config/colors';
import AppText from '../components/ui/ AppText';
import appFonts from '../config/appFonts';
import AppButton from '../components/ui/AppButton';
import Orientation from 'react-native-orientation';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const DeviceConfigScreen = ({deviceConfig, showRetryBtn = false, onPress}) => {
  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.imgContainer}>
        <Image source={require('./../assets/images/login-logo.png')} />
      </View>

      <View style={styles.infoContainer}>
        <AppText style={styles.infoLabel}>
          {deviceConfig === 'internet' ? 'No Internet!' : 'Access Denied!'}
        </AppText>
        <AppText style={styles.infoTxt}>
          {deviceConfig === 'internet'
            ? 'No network connection found. Please check your network connection.'
            : Platform.OS === 'android'
            ? `This is a rooted device. This application can't be running on this rooted device.`
            : `This is a jailbreaked device. This application can't be running on this jailbreaked device.`}
        </AppText>
      </View>

      {showRetryBtn ? (
        <View style={styles.btnContainer}>
          <AppButton
            title="Retry"
            color="primary"
            textColor="secondary"
            onPress={onPress}
          />
        </View>
      ) : (
        <View style={styles.btnContainer}></View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.secondary,
    padding: 25,
  },
  imgContainer: {
    height: windowHeight / 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoContainer: {height: windowHeight / 2, alignItems: 'center'},
  infoLabel: {
    color: colors.primary,
    fontFamily: appFonts.SourceSansProSemiBold,
    fontSize: appFonts.xxxlargeFontSize,
    marginBottom: 25,
  },
  infoTxt: {color: colors.primary, fontSize: appFonts.largeBold, lineHeight: 25},
  btnContainer: {height: windowHeight / 4, width: '100%', padding: 25},
});

export default DeviceConfigScreen;
