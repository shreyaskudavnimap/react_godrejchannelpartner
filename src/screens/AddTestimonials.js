import React, {useState, useEffect} from 'react';
import Modal from 'react-native-modal';
import {
  View,
  StyleSheet,
  ScrollView,
  Image,
  Dimensions,
  TextInput,
  Text,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Alert,
  Platform,
} from 'react-native';
import {useSelector} from 'react-redux';
import Entypo from 'react-native-vector-icons/Entypo';
import DropDownPicker from 'react-native-dropdown-picker';
//components
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppButton from '../components/ui/AppButton';
import SubmitPopUp from './modals/TestimonialPopup';
import SelectPropertyModalScreen from './modals/SelectPropertyModalScreen';
import AppFileChooser from './../components/actionSheet/AppFileChooser';
import appFileChooser from './../utility/appFileChooser';
import ImageorVideo from './../components/actionSheet/ImageorVideo';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
//api
import MYACCOUNT_API from '../api/apiMyAccounts';
import TESTIMONIAL_API from '../api/apiTestimonial';
import Orientation from 'react-native-orientation';
import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';

import appUserAuthorization from '../utility/appUserAuthorization';
import GetLocation from 'react-native-get-location';
import { useIsFocused } from '@react-navigation/native';

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;
const AddTestimonialsScreen = (props) => {
  const [attachFile, setAttachFile] = useState('');
  const [isSubmitModalVisible, setSubmitModalVisible] = useState(false);
  const [projects, setProjects] = useState([]);

  const {loginInfo} = useSelector((state) => state);
  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );

  const [description, setDescription] = useState('');
  const [isVisibleModal, setSelectModalVisible] = useState(false);
  const [mediaTypeVisibleModal, setMediaTypeVisibleModal] = useState(false);

  const [mediaType, setMediaType] = useState('');
  const [isAttachFileVisible, setAttachFileVisible] = useState(false);
  const [attachedFileList, setAttachedFileList] = useState([]);
  const [propertyIndex, setPropertyIndex] = useState(0);
  const [isSuccessSubmitted, setSuccessSubmitted] = useState(false);
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [disclaimer, setDisclaimer] = useState('');
  const [scrollPadding, setScrollPadding] = useState(0);
  const user =
    loginInfo && loginInfo.loginResponse && loginInfo.loginResponse.data
      ? loginInfo.loginResponse.data
      : {};
  const dashboardData = useSelector((state) => state.dashboardData);
  const propertyData = dashboardData.rmData;
  const dropDownPropertyData = propertyData.map((item) => {
    return {
      label: `${item.property_name}/${item.inv_flat_code}`,
      value: item,
    };
  });

  const isFocused = useIsFocused();

  useEffect(() => {    
    if(!isFocused) {
      setAttachedFileList([]);
    }
  }, [isFocused]);

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
    getTestimonialDisclaimer();
  }, []);



  /**
   *
   * @description Get testimonial disclaimer details
   */
  const getTestimonialDisclaimer = () => {
    setIsShowLoader(true);
    TESTIMONIAL_API.getTestimonialDisclaimer()
      .then((response) => {
        setIsShowLoader(false);
        if (response.data && response.data.length) {
          const rejex = /<.*?>/g;
          const createDisclaimer = response.data[0].body.replace(rejex, '');
          setDisclaimer(createDisclaimer);
        }
      })
      .catch((error) => {});
  };
  const onHandlePropertyChange = (itemValue, itemIndex) => {
    setPropertyIndex(itemIndex);
  };
  /**
   *
   * @description Render property
   */
  const renderPropertyList = () => {
    return (
      <View>
        {propertyData.length > 0 && (
          <View>
            {propertyData.length > 1 ? (
              <View>
                <View style={({zIndex: 90}, styles.dropDownContainer)}>
                  {/* <Picker
                    itemStyle={styles.propertyStyle}
                    selectedValue={propertyData[propertyIndex]}
                    onValueChange={onHandlePropertyChange}
                    mode="dropdown">
                    {propertyData.map((data, index) => (
                        <Picker.Item
                        color={colors.secondary}
                        label={`${data.property_name}/${data.inv_flat_code}`}
                        value={data}
                        />
                    ))}
                    </Picker> */}
                  <DropDownPicker
                    defaultValue={propertyData[propertyIndex]}
                    items={dropDownPropertyData}
                    style={styles.dropdownDocument}
                    placeholderStyle={styles.dropdownPlaceholder}
                    itemStyle={styles.dropDownItem}
                    labelStyle={styles.label}
                    onChangeItem={onHandlePropertyChange}
                    //placeholder="Godrej Meridian"
                  />
                </View>
              </View>
            ) : (
              <View>
                {
                  <AppText style={styles.propertyStyle}>
                    {`${propertyData[0].property_name}/${propertyData[0].inv_flat_code}`}
                  </AppText>
                }
              </View>
            )}
          </View>
        )}
      </View>
    );
  };
  /**
   *
   * @param {number} index
   * @description Set index of selected property
   */
  const setCurrentPropertyIndex = (index) => {
    setPropertyIndex(index);
  };

  useEffect(() => {
    const requestData = {user_id: user && user.userid ? user.userid : ''};
    MYACCOUNT_API.apiAccSummary(requestData).then((response) => {
      if (response && response.data && response.data.data) {
        setProjects();
      }
    });
  }, []);

  /**
   *
   * @description Submit testimonial
   */
  const submit = () => {
    loadLocation();
    setSubmitModalVisible(false);
    setIsShowLoader(true);

    const submittedData = {
      user_id: user && user.userid ? user.userid : '',
      description: description,
      attached_files: [],
    };
    if (propertyData && propertyData[propertyIndex]) {
      submittedData['project_id'] = propertyData[propertyIndex].property_id;
    }
    attachedFileList.forEach((file) => {
      submittedData['attached_files'].push({
        file_name: file.fileName
          ? file.fileName
          : file.uri
          ? file.uri.split('/').pop()
          : '',
        file_type: file.type ? file.type : '',
        file_base64_encoded: file.data,
      });
    });
    console.log('submittedData:: ', submittedData);
    TESTIMONIAL_API.apiAddTestimonial(submittedData)
      .then((response) => {
        setIsShowLoader(false);

        if (
          response &&
          response.data &&
          response.data.status &&
          response.data.status == '200'
        ) {
          setSuccessSubmitted(true);
          setSubmitModalVisible(true);
        } else {
          appSnakBar.onShowSnakBar(
            response && response.data && response.data.msg && response.data.msg
              ? response.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      })
      .catch((err) => {
        console.log('error', err);
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,   
          'LONG',
        );
      });
  };

  const loadLocation = () => {
    const storedUserData = user;
    const requestData = {
      deviceId: deviceUniqueId,
      devicePlatform: Platform.OS,
      userId: user && user.userid ? user.userid : '',
      customerName:
        storedUserData && storedUserData.first_name && storedUserData.last_name
          ? `${storedUserData.first_name} ${storedUserData.last_name}`
          : '',
      mobileNo:
        storedUserData && storedUserData.mob_no ? storedUserData.mob_no : '',
      emailId:
        storedUserData && storedUserData.email ? storedUserData.email : '',
      moduleName: 'Add Testimonials',
    };

    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
      .then(({latitude, longitude}) => {
        appUserAuthorization.validateUserAuthorization({
          ...requestData,
          latiTude: latitude,
          longiTude: longitude,
        });
      })
      .catch((error) => {
        appUserAuthorization.validateUserAuthorization(requestData);
      });
  };

  /**
   *
   * @param {array} attachedFiles
   * @param {object} fileChooserResponse
   */
  const pushAttachFile = (attachedFiles, fileChooserResponse) => {
    if (!fileChooserResponse.didCancel) {
      attachedFiles.push(fileChooserResponse);
      setAttachedFileList(attachedFiles);
    }
    setAttachFileVisible(false);
  };
  /**
   *
   * @param {string} clickItem
   * @description Choose file
   */
  //   const onClickFileChooserItem = async (clickItem) => {
  //     const attachedFiles = [...attachedFileList];
  //     switch (clickItem) {
  //       case 'Remove':
  //         setAttachFileVisible(false);
  //         break;
  //       case 'Gallery':
  //         setMediaTypeVisibleModal(true);
  //         setMediaType('Gallery');
  //         break;
  //       case 'Camera':
  //         setMediaTypeVisibleModal(true);
  //         setMediaType('Camera');
  //         break;
  //       case 'Phone':
  //         try {
  //           const chooserResponse = await appFileChooser.onSelectFile();
  //           const data = await appFileChooser.toBase64(chooserResponse.uri);
  //           const updateResponse = {
  //             ...chooserResponse,
  //             fileName: chooserResponse.name,
  //             data: data,
  //           };
  //           pushAttachFile(attachedFiles, updateResponse);
  //         } catch (error) {
  //           console.log('Error onSelectFile : ', error);
  //         }
  //         break;
  //     }
  //   };

  /**
   *
   * @param {number} index
   */
  const selectmediatype = (clickItem) => {
    const attachedFiles = [...attachedFileList];
    switch (clickItem) {
      case 'Remove':
        setMediaTypeVisibleModal(false);
        break;
      case 'Photos':
        appFileChooser.onLaunchImageLibrary((fileChooserResponse) => {
          if (fileChooserResponse) {
            if (!fileChooserResponse.error) {
              if (fileChooserResponse.fileSize) {
                if (validateFileSize(fileChooserResponse.fileSize)) {
                  pushAttachFile(attachedFiles, fileChooserResponse);
                } else {
                  invalidFile();
                }
              }
            }
          }
          setMediaTypeVisibleModal(false);
        });
        break;

      case 'Video':
        appFileChooser.onLaunchImageLibraryVideo(
          async (fileChooserResponse) => {
            setMediaTypeVisibleModal(false);
            if (fileChooserResponse && fileChooserResponse.uri) {
              try {
                const videoDetails = await appFileChooser.getVideoDetails(
                  fileChooserResponse.uri,
                );
                console.log('videoDetails:: ', videoDetails);

                if (videoDetails && videoDetails.size) {
                  if (validateFileSize(videoDetails.size)) {
                    const data = await appFileChooser.toBase64(
                      fileChooserResponse.uri,
                    );

                    // const fileName = fileChooserResponse.name
                    //   ? fileChooserResponse.name
                    //   : fileChooserResponse.uri
                    //   ? fileChooserResponse.uri.split('/').pop()
                    //   : '';

                    const fileName = videoDetails.originalFilepath
                      ? videoDetails.originalFilepath.split('/').pop()
                      : videoDetails.path
                      ? videoDetails.path.split('/').pop()
                      : fileChooserResponse.uri
                      ? fileChooserResponse.uri.split('/').pop()
                      : '';

                    const re = /(?:\.([^.]+))?$/;
                    const ext = re.exec(fileName)[1];
                    const updateResponse = {
                      ...fileChooserResponse,
                      type: `video/${ext ? ext : 'mp4'}`,
                      fileName: fileName,
                      data: data,
                    };
                    console.log(
                      'fileChooserResponse: updateResponse: ',
                      updateResponse,
                    );
                    pushAttachFile(attachedFiles, updateResponse);
                  } else {
                    invalidFile();
                  }
                }
              } catch (error) {
                console.log('videoDetails: error: ', error);
              }
            } else {
              // invalidFileErr();
            }
          },
        );
        break;
    }
  };

  const setVideoFile = async (fileChooserResponse) => {
    const data = await appFileChooser.toBase64(fileChooserResponse.uri);
    const fileName = fileChooserResponse.path.split('/');
    const updateResponse = {
      ...fileChooserResponse,
      fileName: fileName[fileName.length - 1],
      data: data,
    };
    return updateResponse;
  };

  const validateFileSize = (fileSize) => {
    let isValidFile = true;
    appFileChooser.validateFileSize(fileSize, (fileChooserResponse) => {
      isValidFile = fileChooserResponse ? true : false;
    });
    return isValidFile;
  };

  const invalidFile = () => {
    Alert.alert(
      'Oops!',
      'Invalid file size. File size should be less than 5MB',
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  };

  const invalidFileErr = () => {
    Alert.alert(
      'Oops!',
      'Invalid file.',
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  };

  const invalidFileFormat = () => {
    Alert.alert(
      'Oops!',
      'Invalid file format. Allowed document type - jpg, jpeg, png, pdf',
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  };

  const removeFile = (index) => {
    const attachedFiles = [...attachedFileList];
    attachedFiles.splice(index, 1);
    setAttachedFileList(attachedFiles);
  };

  /**
   * @description render attach file list
   */
  const attachedFiles = () => {
    const getFileName = (file) => {
      return file.fileName
        ? file.fileName
        : file.uri
        ? file.uri.split('/').pop()
        : '';
    };

    return (
      <View
        onLayout={(event) => {
          var {height} = event.nativeEvent.layout;
          setScrollPadding(height);
        }}>
        {attachedFileList.map((file, index) => {
          return (
            <View
              key={index}
              style={{
                ...styles.previewBlock,
                flexDirection: 'row',
                marginTop: 8,
                paddingHorizontal: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <AppText numberOfLines={2} style={{alignSelf: 'flex-start'}}>
                  {getFileName(file)}
                </AppText>
              </View>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginLeft: 10,
                }}>
                <Entypo
                  name={'cross'}
                  size={24}
                  onPress={() => removeFile(index)}></Entypo>
              </View>
            </View>
          );
        })}
      </View>
    );
  };

  const onHandleRetryBtn = () => {};

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />
      <ScrollView style={{flexGrow: 1}}>
        <View
          style={{
            ...styles.container,
            paddingBottom: attachedFileList.length
              ? attachedFileList.length * attachedFileList.length + 100
              : 100,
          }}>
          <AppTextBold style={styles.pageTitle}>Add Testimonials </AppTextBold>
          <View style={styles.infoContainer}>
            <View style={styles.imgContainer}>
              {user && user.user_image ? (
                <Image source={{uri: user.user_image}} style={styles.profile} />
              ) : (
                <Image
                  source={require('./../assets/images/no-user-img.png')}
                  style={styles.profile}
                />
              )}
              <AppTextBold>
                {user && user.first_name ? user.first_name : ''}&nbsp;
                {user && user.last_name ? user.last_name : ''}
              </AppTextBold>
            </View>
          </View>
          {/* <AppText>Property</AppText> */}
          {renderPropertyList()}
          <View style={styles.commentTitle}>
            <AppText>Write Here</AppText>
            <View>
              <View style={{...styles.card}}>
                <ScrollView nestedScrollEnabled={true}>
                  <TextInput
                    style={styles.comment}
                    multiline={true}
                    numberOfLines={5}
                    placeholder="type your comment here"
                    onChangeText={(desc) => setDescription(desc)}
                  />
                </ScrollView>
              </View>
            </View>
            <View style={styles.uploadBlock}>
              <View style={{...styles.media}}>
                <AppText>Media</AppText>
              </View>
              {attachedFileList.length < 5 && (
                <TouchableOpacity
                  style={styles.attachItem}
                  onPress={() => {
                    if (attachedFileList.length < 5) {
                      setMediaTypeVisibleModal(true);
                    }
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                      alignItems: 'center',
                    }}>
                    <Image
                      style={styles.attachIcon}
                      source={require('../assets/images/attachment-icon.png')}
                      resizeMode="contain"
                    />
                    <View>
                      <Text>Attach files</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            </View>
            {attachedFiles()}
            <View style={{marginBottom: '15%', marginTop: '8%'}}>
              <AppButton
                color={description.length ? 'secondary' : 'charcoal'}
                disabled={description.length ? false : true}
                textColor = {description.length ? 'primary' : 'primary'}
                title="SUBMIT"
                onPress={() => {
                  setSubmitModalVisible(true);
                }}
              />
            </View>
          </View>
        </View>
      </ScrollView>

      {/* <Modal
        visible={isAttachFileVisible}
        style={{
          //marginTop: 100,
          margin: 0,
          justifyContent: 'flex-end',
        }}
        onBackButtonPress={() => setAttachFileVisible(false)}
        onBackdropPress={() => setAttachFileVisible(false)}
        backdropColor="rgba(0,0,0,0.5)">
        <AppFileChooser
          onPress={(clickItem) => onClickFileChooserItem(clickItem)}
        />
      </Modal> */}

      <Modal
        visible={mediaTypeVisibleModal}
        style={{
          //marginTop: 100,
          margin: 0,
          justifyContent: 'flex-end',
        }}
        onBackButtonPress={() => setMediaTypeVisibleModal(false)}
        onBackdropPress={() => setMediaTypeVisibleModal(false)}
        backdropColor="rgba(0,0,0,0.5)">
        <ImageorVideo
          onPress={(clickItem) => selectmediatype(clickItem)}></ImageorVideo>
      </Modal>
      {
        <SubmitPopUp
          setSubmitModalVisible={setSubmitModalVisible}
          disclaimer={disclaimer}
          isSubmitModalVisible={isSubmitModalVisible}
          submit={submit}
          isSuccessSubmitted={isSuccessSubmitted}
        />
      }
      <AppFooter activePage={0} isPostSales={true} />
      <AppOverlayLoader isLoading={isShowLoader} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.jaguar,
    textTransform: 'uppercase',
  },
  profile: {
    height: 80,
    width: 80,
    borderRadius: 40,
  },
  commentTitle: {marginTop: '5%', zIndex: -1},
  mediaTitle: {marginBottom: '5%'},
  fileName: {
    backgroundColor: colors.LynxWhite,
    height: '10%',
    marginBottom: '5%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '10%',
    marginBottom: '5%',
  },
  imgContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  comment: {
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    padding: 10,
    //marginBottom: "-6%",
    marginTop: '5%',
    width: '100%',
    height: '100%',
    backgroundColor: colors.LynxWhite,
    textAlignVertical: 'top',
  },
  rectangle: {
    height: 40,
    //width: '90%',
    backgroundColor: colors.LynxWhite,
    //elevation: 1
    //zIndex: 2
  },
  arrow: {
    height: 16,
    width: 16,
  },
  propertyText: {
    width: '90%',
    fontSize: appFonts.largeBold,
    fontWeight: 'bold',
    color: colors.secondary,
  },
  mainContainer: {
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
  },
  modalContainer: {
    marginTop: windowHeight * 0.36,
    backgroundColor: colors.LynxWhite,
    marginHorizontal: 20,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 3, height: 4},
    shadowOpacity: 0.9,
    shadowRadius: 3,
    elevation: 3,
  },
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
  },
  itemContainerCity: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
  },
  itemTxt: {
    fontSize: appFonts.largeBold,
    color: colors.jaguar,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  itemTxt1: {
    fontSize: appFonts.largeBold,
    padding: 20,
    paddingLeft: 25,
    color: colors.jaguar,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  uploadBlock: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    //marginTop: -60
  },
  attachItem: {
    alignItems: 'flex-end',
    marginLeft: '50%',
  },
  attachIcon: {
    width: 19,
    //marginTop: 10
  },

  previewBlock: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    display: 'flex',
    backgroundColor: colors.lightGray,
    padding: 15,
    borderRadius: 15,
    justifyContent: 'space-between',
  },
  card: {
    height: windowHeight * 0.2,
    width: windowWidth * 0.9,
  },
  media: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginVertical: '5%',
  },
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    //borderBottomWidth: 2,
    //borderBottomColor: colors.gray4,
  },
  dropdownDocument: {
    height: '100%',
    width: '100%',
    marginBottom: '5%',
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderTopWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 999,
  },
  dropdownPlaceholder: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  dropDownItem: {
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGray,
    // marginTop: '5%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '5%',
  },
  dropDownContainer: {
    ...Platform.select({ios: {zIndex: 1000}}),
  },
});

export default AddTestimonialsScreen;
