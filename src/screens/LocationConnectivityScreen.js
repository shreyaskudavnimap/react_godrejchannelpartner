import React, {useEffect, useState, useCallback} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  LayoutAnimation,
  UIManager,
  Platform,
  TouchableWithoutFeedback,
  Modal,
  Text,
  Image,
} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps';

import {useDispatch, useSelector} from 'react-redux';

import * as projectLocationConnectivityAction from './../store/actions/projectLocationConnectivityAction';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppExpandableComponent from '../components/ui/AppExpandableComponent';
import AppVideoThumbView from '../components/ui/AppVideoThumbView';
import AppFooter from '../components/ui/AppFooter';
import VideoModalScreen from './modals/VideoModalScreen';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import appSnakBar from '../utility/appSnakBar';
import appPhoneMap from '../utility/appPhoneMap';
import Orientation from 'react-native-orientation';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const LocationConnectivityScreen = ({navigation, route}) => {
  const [listDataSource, setListDataSource] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState();
  const [videoUrl, setVideoUrl] = useState('');
  const [modalVisible, setModalVisible] = useState(false);

  const routeParams = route.params;
  const projectId =
    routeParams && routeParams.item && routeParams.item.proj_id
      ? routeParams.item.proj_id
      : '';

  const dispatch = useDispatch();

  const propertyLocationConnectivity = useSelector(
    (state) => state.projectLocationConnectivity.locationConnectivity,
  );
  const propertyNearbyPoints = useSelector(
    (state) => state.projectLocationConnectivity.nearbyPoints,
  );
  const nearbyPointsDetails = useSelector(
    (state) => state.projectLocationConnectivity.nearbyPointsDetails,
  );

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
    getPropertyLocationConnectivity();
  }, [dispatch]);

  const getPropertyLocationConnectivity = useCallback(async () => {
    if (!projectId) {
      navigation.goBack();
      return;
    }

    setIsLoading(true);
    setError(null);
    try {
      await dispatch(
        projectLocationConnectivityAction.getPropertyLocationConnectivity(
          projectId,
        ),
      );
      getPropertyNearbyPoints();
    } catch (err) {
      appSnakBar.onShowSnakBar(err.message, 'LONG');
      navigation.goBack();
      setError(err.message);
      setIsLoading(false);
    }
  }, [dispatch, setIsLoading, setError, isLoading]);

  const getPropertyNearbyPoints = useCallback(async () => {
    try {
      await dispatch(
        projectLocationConnectivityAction.getPropertyNearbyPoints(projectId),
      );
      getNearbyPointsDetails();
    } catch (err) {
      setError(err.message);
      setIsLoading(false);
    }
  }, [dispatch, setIsLoading, setError, isLoading]);

  const getNearbyPointsDetails = useCallback(async () => {
    try {
      await dispatch(
        projectLocationConnectivityAction.getNearbyPointsDetails(projectId),
      );
      setIsLoading(false);
      setListDataSource(nearbyPointsDetails);
    } catch (err) {
      setError(err.message);
      setIsLoading(false);
    }
  }, [dispatch, setIsLoading, setError, isLoading]);

  if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  const updateLayout = (index) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    const array = [...listDataSource];
    array[index]['isExpanded'] = !array[index]['isExpanded'];
    setListDataSource(array);
  };

  const onHandleVideoModal = (videoUrl) => {
    setVideoUrl(videoUrl);
    setModalVisible(true);
  };

  const openDeviceMap = () => {
    appPhoneMap.openDeviceMap(
      propertyLocationConnectivity.lat
        ? Number(propertyLocationConnectivity.lat)
        : 0,
      propertyLocationConnectivity.lng
        ? Number(propertyLocationConnectivity.lng)
        : 0,
      propertyLocationConnectivity.title,
    );
  };

  const onHandleRetryBtn = () => {
    getPropertyLocationConnectivity();
  };

  const handlePin = (title) => {
    return (
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <AppText
          style={{
            padding: 5,
            fontSize: appFonts.largeBold,
            color: colors.danger,
            fontFamily: appFonts.SourceSansProSemiBold,
            backgroundColor: colors.primary,
            marginBottom: 5
          }}>
          {title}
        </AppText>
        <Image
          source={require('./../assets/images/map-marker.png')}
          style={{width: 38, height: 38}}
        />
      </View>
    );
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />

      {isLoading ? (
        <View style={styles.container}></View>
      ) : (
        <ScrollView>
          <View style={styles.container}>
            <AppText style={styles.pageTitle}>
              LOCATION &amp; CONNECTIVITY
            </AppText>

            <View style={styles.mapContainer}>
              <View style={styles.mapView}>
                <MapView
                  provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                  style={styles.map}
                  region={{
                    latitude: propertyLocationConnectivity.lat
                      ? Number(propertyLocationConnectivity.lat)
                      : 0,
                    longitude: propertyLocationConnectivity.lng
                      ? Number(propertyLocationConnectivity.lng)
                      : 0,
                    latitudeDelta: 0.9004,
                    longitudeDelta: 0.90121,
                  }}>
                  <Marker
                    coordinate={{
                      latitude: propertyLocationConnectivity.lat
                        ? Number(propertyLocationConnectivity.lat)
                        : 0,
                      longitude: propertyLocationConnectivity.lng
                        ? Number(propertyLocationConnectivity.lng)
                        : 0,
                    }}
                    title={propertyLocationConnectivity.title}>
                    {/* {Platform.OS === 'ios' ? (
                      <Callout tooltip={true}>
                        <AppText>{propertyLocationConnectivity.title}</AppText>
                      </Callout>
                    ) : null} */}
                    {handlePin(propertyLocationConnectivity.title)}
                  </Marker>
                </MapView>
              </View>
              <TouchableWithoutFeedback onPress={openDeviceMap}>
                <View style={styles.mapOverlayContainer}></View>
              </TouchableWithoutFeedback>
            </View>

            <View style={styles.addressContainer}>
              {propertyLocationConnectivity.title && (
                <AppText style={styles.addressTitle}>
                  {propertyLocationConnectivity.title}
                </AppText>
              )}

              {propertyLocationConnectivity.address ? (
                <AppText style={styles.addressValue}>
                  {propertyLocationConnectivity.address}
                </AppText>
              ) : null}
            </View>

            {propertyLocationConnectivity.neighborhood_text ? (
              <View style={styles.neighbourhoodContainer}>
                <AppText style={styles.neighbourhoodTitle}>
                  THE NEIGHBOURHOOD
                </AppText>
                <AppText style={styles.neighbourhoodValue}>
                  {propertyLocationConnectivity.neighborhood_text}
                </AppText>
              </View>
            ) : null}

            {propertyNearbyPoints && propertyNearbyPoints.length > 0 ? (
              <View style={styles.gettingThereContainer}>
                <AppText style={styles.gettingThereTitle}>
                  GETTING THERE
                </AppText>

                {propertyNearbyPoints.map((nearbyPoint, index) => (
                  <View key={index}>
                    <AppText style={styles.gettingThereLabel}>
                      {nearbyPoint.location_title}
                    </AppText>
                    <AppText style={styles.gettingThereDistance}>
                      {nearbyPoint.location_description}
                    </AppText>
                  </View>
                ))}
              </View>
            ) : null}

            {propertyLocationConnectivity.location_video_thumbnail &&
            propertyLocationConnectivity.location_video_url ? (
              <View style={styles.videoContainer}>
                <AppVideoThumbView
                  imageUri={
                    propertyLocationConnectivity.location_video_thumbnail
                  }
                  onPress={() => {
                    onHandleVideoModal(
                      propertyLocationConnectivity.location_video_url,
                    );
                  }}
                />
              </View>
            ) : null}

            {listDataSource && listDataSource.length > 0 ? (
              <View style={styles.nearByList}>
                {listDataSource.map((item, index) => (
                  <AppExpandableComponent
                    key={index}
                    onPress={() => {
                      updateLayout(index);
                    }}
                    onChildPress={(title) => {
                      //   alert(' val: ' + title);
                    }}
                    item={item}
                  />
                ))}
              </View>
            ) : null}
          </View>
        </ScrollView>
      )}

      <AppFooter activePage={5} isPostSales={false} />

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        supportedOrientations={['portrait', 'landscape']}
        onRequestClose={() => {
          Orientation.lockToPortrait();
          setModalVisible(!modalVisible);
        }}>
        <VideoModalScreen
          onPress={() => {
            Orientation.lockToPortrait();
            setModalVisible(!modalVisible);
          }}
          projectName={
            propertyLocationConnectivity && propertyLocationConnectivity.title
              ? propertyLocationConnectivity.title
              : ''
          }
          videoUri={videoUrl}
          setIsLoading={setIsLoading}
        />
      </Modal>

      <AppOverlayLoader isLoading={isLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 10,
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: 15,
    textTransform: 'uppercase',
    paddingHorizontal: 18,
  },
  mapContainer: {
    height: windowHeight / 3,
    width: '100%',
    marginVertical: 20,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  mapView: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  addressContainer: {
    marginVertical: 20,
    paddingHorizontal: 18,
  },
  addressTitle: {
    fontSize: appFonts.xxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: 8,
    textTransform: 'uppercase',
  },
  addressValue: {
    fontSize: appFonts.largeFontSize,
    lineHeight: 25,
    textAlign: 'justify',
  },
  neighbourhoodContainer: {
    marginVertical: 20,
    paddingHorizontal: 18,
  },
  neighbourhoodTitle: {
    fontSize: appFonts.xxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: 8,
    textTransform: 'uppercase',
  },
  neighbourhoodValue: {
    fontSize: appFonts.largeFontSize,
    lineHeight: 25,
    textAlign: 'justify',
  },
  gettingThereContainer: {
    marginVertical: 20,
    paddingHorizontal: appFonts.largeBold,
  },
  gettingThereTitle: {
    fontSize: appFonts.xxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: 8,
    textTransform: 'uppercase',
  },
  gettingThereLabel: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginTop: 15,
    marginBottom: 8,
    textTransform: 'capitalize',
  },
  gettingThereDistance: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.secondary,
  },
  videoContainer: {
    marginVertical: 5,
    paddingHorizontal: 18,
  },
  nearByList: {
    marginVertical: 20,
    paddingHorizontal: 18,
  },
  mapOverlayContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
});

export default LocationConnectivityScreen;
