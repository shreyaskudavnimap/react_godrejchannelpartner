import React, {useEffect, useState, useRef} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  Modal,
  FlatList,
  Dimensions,
  BackHandler,
} from 'react-native';
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppButton from '../components/ui/AppButton';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppText from '../components/ui/ AppText';
import {
  getPropertySearchData,
  setBudgetData,
  setCityTermMenu,
  setLastDesect,
  setLastSearch,
  setProjectStatus,
  setTypologyTermMenu,
} from '../store/actions/propertySearchAction';
import {useDispatch, useSelector} from 'react-redux';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import SelectLocationModalScreen from './modals/SelectLocationModalScreen';
import AppSearchFilterListItem from '../components/ui/AppSearchFilterListItem';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import appCurrencyFormatter from '../utility/appCurrencyFormatter';
import GetLocation from 'react-native-get-location';
import appConstant from '../utility/appConstant';
import Orientation from 'react-native-orientation';

const {width: windowWidth} = Dimensions.get('window');

let searchAPIRequest = {
  userId: '',
  deviceUniqueId: '',
  propertyTypeTermMenu: '',
  cityTermMenu: '',
  subLocation: '',
  projectStatus: '',
  typologyTermMenu: '',
  minPrice: 0,
  maxPrice: 0,
  lastSearch: '',
  lastDesect: '',
};

const returnSelectedCity = (str, nextElm) => {
  let joinString = '';
  if (typeof str === 'string') {
    joinString = str;
  } else if (str.selected) {
    joinString = str.name;
  }
  if (nextElm.selected) {
    if (joinString === '') {
      return nextElm.name;
    } else {
      return joinString + ', ' + nextElm.name;
    }
  } else {
    return joinString;
  }
};

const getLastDesect = (data, type) => {
  switch (type) {
    case 'location':
    case 'possession':
    case 'typology':
      return data.filter((elm) => {
        return !elm.selected;
      });
    case 'price':
      return [{min: data.min}, {max: data.max}];
  }
};

let lastDesect = null;
let selectedCityIDS = [];
let selectedPossessionNames = [];
let selectedTypologyIDS = [];

const getLocationData = async (setLocationData) => {
  let cityName = 'Mumbai';

  try {
    const {latitude, longitude} = await GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 10000,
    });
    const apiKey = appConstant.googleMapKey.key;
    const URL = `https://maps.googleapis.com/maps/api/geocode/json?address=${latitude},${longitude}&key=${apiKey}`;
    try {
      const response = await fetch(URL);
      const responseJson = await response.json();
      if (
        responseJson &&
        responseJson.status &&
        responseJson.status === 'OK' &&
        responseJson.results &&
        responseJson.results.length > 0
      ) {
        const address_component = responseJson.results.find(
          (elm) => elm.geometry.location_type === 'GEOMETRIC_CENTER',
        );
        if (address_component) {
          const city = address_component.address_components.find((elm) =>
            elm.types.includes('locality'),
          );
          cityName = city.long_name;
        }
      }
      setLocationData({
        city: cityName,
        lat: latitude,
        lng: longitude,
        locationSet: false,
      });
    } catch (e) {
      console.log(e);
    }
  } catch (e) {
    console.log(e);
  }
};

const setRemoteCityList = (
  cityTermMenu,
  setCityListSelectionText,
  setSelectedCityList,
  extraData,
) => {
  let selectedCities = '';
  if (cityTermMenu && cityTermMenu.length > 0) {
    if (cityTermMenu.length > 1) {
      selectedCities = cityTermMenu.reduce(returnSelectedCity);
    } else {
      selectedCities = cityTermMenu[0].selected ? cityTermMenu[0].name : '';
    }
    if (
      selectedCities === '' &&
      extraData &&
      !extraData.locationData.locationSet &&
      extraData.lastDesectProp.length === 0
    ) {
      let index = cityTermMenu.findIndex(
        (elm) => elm.name === extraData.locationData.city,
      );
      if (index >= 0) {
        extraData.setLocationData({
          ...extraData.locationData,
          locationSet: true,
        });
        const cityL = JSON.parse(JSON.stringify(cityTermMenu));
        cityL[index].selected = true;
        lastDesect = getLastDesect(cityL, 'location');
        setSelectedCityList(cityL);
        setCityListSelectionText(selectedCities);
        extraData.setIsLoading(true);
        selectedCityIDS = [cityL[index].id];
        if (extraData.isLogin) {
          extraData.dispatch(
            setCityTermMenu(
              extraData.userId,
              '',
              '1',
              selectedCityIDS.join(','),
              '',
              selectedPossessionNames.join(','),
              selectedTypologyIDS.join(','),
              parseInt(extraData.priceRange.min),
              parseInt(extraData.priceRange.max),
              'location',
              lastDesect,
              cityL,
              true,
            ),
          );
        } else {
          extraData.dispatch(
            setCityTermMenu(
              '',
              extraData.deviceUniqueId,
              '1',
              selectedCityIDS.join(','),
              '',
              selectedPossessionNames.join(','),
              selectedTypologyIDS.join(','),
              parseInt(extraData.priceRange.min),
              parseInt(extraData.priceRange.max),
              'location',
              lastDesect,
              cityL,
              true,
            ),
          );
        }
        lastDesect = null;
      } else {
        setCityListSelectionText(selectedCities);
        setSelectedCityList(cityTermMenu);
      }
    } else {
      setCityListSelectionText(selectedCities);
      setSelectedCityList(cityTermMenu);
    }
  }
};

const SearchFilterScreen = ({navigation}) => {
  const possessionScrollRef = useRef(null);
  const backHandlerRef = useRef(null);
  const [isLoading, setIsLoading] = useState(false);
  const [locationModalVisible, setLocationModalVisible] = useState(false);
  const [selectedCityList, setSelectedCityList] = useState([]);
  const [selectedTypologyList, setSelectedTypologyList] = useState([]);
  const [selectedPosessionList, setSelectedPosessionList] = useState([]);
  const [cityListSelectionText, setCityListSelectionText] = useState('');
  const [locationData, setLocationData] = useState({
    city: 'Mumbai',
    lat: 19.076,
    lng: 72.8777,
    locationSet: false,
  });
  const dispatch = useDispatch();
  const {
    cityTermMenu,
    projectStatus,
    typologyTermMenu,
    priceLimit,
    priceRange,
    lastDesect: lastDesectProp,
  } = useSelector((state) => state.propertySearch);
  const {
    loginResponse: {isLogin, data: userData},
    deviceInfo: {deviceUniqueId},
  } = useSelector((state) => ({
    loginResponse: state.loginInfo.loginResponse,
    deviceInfo: state.deviceInfo,
  }));

  useEffect(() => {
    Orientation.lockToPortrait();
    backHandlerRef.current = BackHandler.addEventListener(
      'hardwareBackPress',
      () => {
        dispatch({type: 'RESET_FILTERS'});
        navigation.goBack();
        return true;
      },
    );

    return () => {
      if (
        backHandlerRef.current &&
        backHandlerRef.current instanceof Function
      ) {
        backHandlerRef.current();
        backHandlerRef.current = null;
      }
    };
  }, []);

  // On Focus
  useEffect(() => {
    return navigation.addListener('focus', () => {
      lastDesect = null;
      selectedCityIDS = [];
      selectedPossessionNames = [];
      selectedTypologyIDS = [];
      if (isLogin) {
        searchAPIRequest.userId = userData.userid;
        searchAPIRequest.deviceUniqueId = '';
      } else {
        searchAPIRequest.userId = '';
        searchAPIRequest.deviceUniqueId = deviceUniqueId;
      }
      setIsLoading(true);
      dispatch(
        getPropertySearchData(
          searchAPIRequest.userId,
          searchAPIRequest.deviceUniqueId,
          '',
          '',
          '',
          '',
          '',
          '',
          '',
          '',
          lastDesect,
          true,
        ),
      );
      getLocationData(setLocationData).then();
    });
  }, []);

  // On city menu change
  useEffect(() => {
    setIsLoading(false);
    // City
    setRemoteCityList(
      cityTermMenu,
      setCityListSelectionText,
      setSelectedCityList,
      undefined,
    );
    // City

    // Possession
    setSelectedPosessionList(projectStatus);
    setSelectedTypologyList(typologyTermMenu);
    if (possessionScrollRef.current && projectStatus.length < 3) {
      possessionScrollRef.current.scrollTo({x: 0, y: 0, animated: true});
    }
    // Possession
  }, [cityTermMenu, projectStatus, typologyTermMenu, priceRange, priceLimit]);

  useEffect(() => {
    if (!locationData.locationSet) {
      let extraData = {
        locationData: locationData,
        setLocationData: setLocationData,
        setIsLoading: setIsLoading,
        isLogin: isLogin,
        dispatch: dispatch,
        userId: isLogin ? userData.userid : '',
        deviceUniqueId: deviceUniqueId,
        priceRange: priceRange,
        lastDesectProp: lastDesectProp,
      };
      setRemoteCityList(
        cityTermMenu,
        setCityListSelectionText,
        setSelectedCityList,
        extraData,
      );
      // City

      // Possession
      setSelectedPosessionList(projectStatus);
      setSelectedTypologyList(typologyTermMenu);
    }
  }, [locationData]);

  const onHandleRetryBtn = () => {
    navigation.goBack();
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />
      <ScrollView>
        <View style={styles.container}>
          <AppTextBold style={styles.pageTitle}>SEARCH</AppTextBold>

          <TouchableWithoutFeedback
            onPress={() => {
              setLocationModalVisible(true);
            }}>
            <View style={styles.itemContainer}>
              <AppText
                numberOfLines={1}
                style={[
                  styles.itemTxt,
                  {
                    fontFamily: appFonts.SourceSansProSemiBold,
                    fontSize: appFonts.largeFontSize,
                  },
                ]}>
                {cityListSelectionText !== ''
                  ? cityListSelectionText
                  : 'Select location'}
              </AppText>
              <Image
                style={styles.arrow}
                source={require('./../assets/images/down-icon-b.png')}
              />
            </View>
          </TouchableWithoutFeedback>

          {/*Project Status*/}

          <View style={styles.possesionsView}>
            <AppText style={styles.possesionsTitle}>Possession</AppText>
            <ScrollView
              ref={possessionScrollRef}
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              {selectedPosessionList.length > 0 ? (
                selectedPosessionList.map((item, index) => (
                  <AppSearchFilterListItem
                    title={item.name}
                    index={index}
                    key={index.toString()}
                    color={item.selected ? 'jaguar' : 'primary'}
                    textColor={item.selected ? 'primary' : 'jaguar'}
                    onPress={() => {
                      setIsLoading(true);
                      const possessionList = JSON.parse(
                        JSON.stringify(selectedPosessionList),
                      );
                      let foundMatch = false;
                      selectedPossessionNames = [];
                      for (let possessionIndex in possessionList) {
                        if (
                          !foundMatch &&
                          possessionList[possessionIndex].id === item.id
                        ) {
                          possessionList[
                            possessionIndex
                          ].selected = !possessionList[possessionIndex]
                            .selected;
                          foundMatch = true;
                        }
                        if (possessionList[possessionIndex].selected) {
                          selectedPossessionNames.push(
                            possessionList[possessionIndex].name,
                          );
                        }
                      }
                      lastDesect = getLastDesect(possessionList, 'possession');
                      if (isLogin) {
                        dispatch(
                          setProjectStatus(
                            userData.userid,
                            '',
                            '1',
                            selectedCityIDS.join(','),
                            '',
                            selectedPossessionNames.join(','),
                            selectedTypologyIDS.join(','),
                            parseInt(priceRange.min),
                            parseInt(priceRange.max),
                            'possession',
                            lastDesect,
                            possessionList,
                          ),
                        );
                      } else {
                        dispatch(
                          setProjectStatus(
                            '',
                            deviceUniqueId,
                            '1',
                            selectedCityIDS.join(','),
                            '',
                            selectedPossessionNames.join(','),
                            selectedTypologyIDS.join(','),
                            parseInt(priceRange.min),
                            parseInt(priceRange.max),
                            'possession',
                            lastDesect,
                            possessionList,
                          ),
                        );
                      }
                      lastDesect = null;
                    }}
                  />
                ))
              ) : (
                <AppText>Not applicable</AppText>
              )}
            </ScrollView>
            {/*<FlatList*/}
            {/*  showsHorizontalScrollIndicator={false}*/}
            {/*  horizontal*/}
            {/*  data={selectedPosessionList}*/}
            {/*  ListEmptyComponent={<AppText>Not applicable</AppText>}*/}
            {/*  renderItem={({item, index}) => (*/}
            {/*    */}
            {/*  )}*/}
            {/*  keyExtractor={(item, index) => index.toString()}*/}
            {/*/>*/}
          </View>

          {/*Project Status*/}

          {/*Typology*/}

          <View style={styles.typologyView}>
            <AppText style={styles.typologyTitle}>Typology</AppText>
            <FlatList
              showsHorizontalScrollIndicator={false}
              horizontal
              data={selectedTypologyList}
              ListEmptyComponent={<AppText>Not applicable</AppText>}
              renderItem={({item, index}) => (
                <AppSearchFilterListItem
                  title={item.name}
                  index={index}
                  color={item.selected ? 'jaguar' : 'primary'}
                  textColor={item.selected ? 'primary' : 'jaguar'}
                  onPress={() => {
                    setIsLoading(true);
                    const typologyList = JSON.parse(
                      JSON.stringify(selectedTypologyList),
                    );
                    let foundMatch = false;
                    selectedTypologyIDS = [];
                    for (let typologyIndex in typologyList) {
                      if (
                        !foundMatch &&
                        typologyList[typologyIndex].id === item.id
                      ) {
                        typologyList[typologyIndex].selected = !typologyList[
                          typologyIndex
                        ].selected;
                        foundMatch = true;
                      }
                      if (typologyList[typologyIndex].selected) {
                        selectedTypologyIDS.push(
                          typologyList[typologyIndex].id,
                        );
                      }
                    }
                    lastDesect = getLastDesect(typologyList, 'typology');
                    if (isLogin) {
                      dispatch(
                        setTypologyTermMenu(
                          userData.userid,
                          '',
                          '1',
                          selectedCityIDS.join(','),
                          '',
                          selectedPossessionNames.join(','),
                          selectedTypologyIDS.join(','),
                          parseInt(priceRange.min),
                          parseInt(priceRange.max),
                          'typology',
                          lastDesect,
                          typologyList,
                        ),
                      );
                    } else {
                      dispatch(
                        setTypologyTermMenu(
                          '',
                          deviceUniqueId,
                          '1',
                          selectedCityIDS.join(','),
                          '',
                          selectedPossessionNames.join(','),
                          selectedTypologyIDS.join(','),
                          parseInt(priceRange.min),
                          parseInt(priceRange.max),
                          'typology',
                          lastDesect,
                          typologyList,
                        ),
                      );
                    }
                    lastDesect = null;
                  }}
                />
              )}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          {/*Topology*/}

          <View style={styles.priceRangeView}>
            <AppText style={styles.priceRangeTitle}>Price Range</AppText>
            {priceLimit.min &&
            priceLimit.max &&
            parseInt(priceRange.max) > 0 ? (
              <View style={styles.placeHolderView}>
                {/* {budgetData.min ? (
                  <AppText style={styles.placeHolderAmount}>
                    ₹
                    {appCurrencyFormatter.getSearchCurrencyFormat(
                      multiSliderValue[0],
                    )}
                  </AppText>
                ) : null} */}
                <AppText style={styles.placeHolderAmount}>
                  ₹
                  {appCurrencyFormatter.getSearchCurrencyFormat(priceRange.min)}
                </AppText>
                <AppText style={styles.placeHolderAmount}>
                  ₹
                  {appCurrencyFormatter.getSearchCurrencyFormat(priceRange.max)}
                </AppText>
              </View>
            ) : (
              <AppText>Not applicable</AppText>
            )}

            {priceLimit.min &&
            priceLimit.max &&
            parseInt(priceRange.max) > 0 ? (
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <MultiSlider
                  trackStyle={{backgroundColor: colors.lightGray, height: 5}}
                  selectedStyle={{backgroundColor: colors.jaguar, height: 8}}
                  twoMarkerValue={2}
                  oneMarkerLeftPosition={2}
                  sliderLength={windowWidth * 0.88}
                  values={[parseInt(priceRange.min), parseInt(priceRange.max)]}
                  onValuesChangeFinish={(evt) => {
                    setIsLoading(true);
                    lastDesect = getLastDesect(
                      {min: priceRange.min, max: priceRange.max},
                      'price',
                    );
                    if (isLogin) {
                      dispatch(
                        setBudgetData(
                          userData.userid,
                          '',
                          '1',
                          selectedCityIDS.join(','),
                          '',
                          selectedPossessionNames.join(','),
                          selectedTypologyIDS.join(','),
                          parseInt(evt[0]),
                          parseInt(evt[1]),
                          'price',
                          lastDesect,
                          {min: parseInt(evt[0]), max: parseInt(evt[1])},
                        ),
                      );
                    } else {
                      dispatch(
                        setBudgetData(
                          '',
                          deviceUniqueId,
                          '1',
                          selectedCityIDS.join(','),
                          '',
                          selectedPossessionNames.join(','),
                          selectedTypologyIDS.join(','),
                          parseInt(evt[0]),
                          parseInt(evt[1]),
                          'price',
                          lastDesect,
                          {min: parseInt(evt[0]), max: parseInt(evt[1])},
                        ),
                      );
                    }
                    lastDesect = null;
                    console.log(evt);
                  }}
                  markerStyle={styles.markerStyle}
                  min={parseInt(priceLimit.min)}
                  max={parseInt(priceLimit.max)}
                  step={1000}
                />
              </View>
            ) : null}
          </View>

          <View style={styles.searchBtn}>
            <AppButton
              onPress={() => {
                setIsLoading(true);

                if (isLogin) {
                  dispatch(
                    setLastSearch(
                      userData.userid,
                      '',
                      '1',
                      selectedCityList,
                      locationData.lat,
                      locationData.lng,
                      priceRange.min,
                      priceRange.max,
                      selectedPosessionList,
                      selectedTypologyList,
                      '',
                      0,
                      () => {
                        navigation.navigate('SearchResult');
                      },
                      (error) => {
                        console.log(error);
                        setIsLoading(false);
                      },
                    ),
                  );
                } else {
                  dispatch(
                    setLastSearch(
                      '',
                      deviceUniqueId,
                      '1',
                      selectedCityList,
                      locationData.lat,
                      locationData.lng,
                      priceRange.min,
                      priceRange.max,
                      selectedPosessionList,
                      selectedTypologyList,
                      '',
                      0,
                      () => {
                        navigation.navigate('SearchResult');
                      },
                      (error) => {
                        console.log(error);
                        setIsLoading(false);
                      },
                    ),
                  );
                }
              }}
              title="SEARCH"
            />
          </View>
        </View>
      </ScrollView>
      <Modal
        animationType="fade"
        transparent={true}
        onRequestClose={() => {
          setRemoteCityList(
            cityTermMenu,
            setCityListSelectionText,
            setSelectedCityList,
          );
          setTimeout(() => {
            setLocationModalVisible(false);
          });
        }}
        visible={locationModalVisible}>
        <SelectLocationModalScreen
          cities={selectedCityList}
          onSelectCity={(city) => {
            const cityL = JSON.parse(JSON.stringify(selectedCityList));
            let index = cityL.findIndex((elm) => elm.id === city.id);
            cityL[index].selected = !cityL[index].selected;
            lastDesect = getLastDesect(cityL, 'location');
            setSelectedCityList(cityL);
          }}
          onCancel={() => {
            setRemoteCityList(
              cityTermMenu,
              setCityListSelectionText,
              setSelectedCityList,
            );
            setTimeout(() => {
              setLocationModalVisible(false);
            });
          }}
          onUpdate={() => {
            setIsLoading(true);
            selectedCityIDS = [];
            for (let item of selectedCityList) {
              if (item.selected) {
                selectedCityIDS.push(item.id);
              }
            }
            if (isLogin) {
              dispatch(
                setCityTermMenu(
                  userData.userid,
                  '',
                  '1',
                  selectedCityIDS.join(','),
                  '',
                  selectedPossessionNames.join(','),
                  selectedTypologyIDS.join(','),
                  parseInt(priceRange.min),
                  parseInt(priceRange.max),
                  'location',
                  lastDesect,
                  selectedCityList,
                ),
              );
            } else {
              dispatch(
                setCityTermMenu(
                  '',
                  deviceUniqueId,
                  '1',
                  selectedCityIDS.join(','),
                  '',
                  selectedPossessionNames.join(','),
                  selectedTypologyIDS.join(','),
                  parseInt(priceRange.min),
                  parseInt(priceRange.max),
                  'location',
                  lastDesect,
                  selectedCityList,
                ),
              );
            }
            lastDesect = null;
            setLocationModalVisible(false);
          }}
        />
      </Modal>
      <AppOverlayLoader isLoading={isLoading} />
      <AppFooter activePage={2} isPostSales={false} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  pageTitle: {
    fontSize: appFonts.xlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.jaguar,
    textTransform: 'uppercase',
  },

  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 16,
    marginVertical: 5,
    backgroundColor: '#f7f7f7',
  },
  itemTxt: {
    color: colors.jaguar,
    marginRight: 5,
  },
  arrow: {height: 16, width: 16},
  placeHolderView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    fontFamily: 'SourceSansPro-SemiBold',
  },
  placeHolderAmount: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
    marginTop: 15,
  },
  possesionsView: {
    marginVertical: 20,
  },
  possesionsTitle: {
    color: colors.jaguar,
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProBold,
    marginBottom: 8,
  },
  typologyView: {
    marginVertical: 20,
  },
  typologyTitle: {
    color: colors.jaguar,
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProBold,
    marginBottom: 8,
  },
  priceRangeView: {
    marginVertical: 20,
  },
  priceRangeTitle: {
    color: colors.jaguar,
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProBold,
    marginBottom: 8,
  },
  markerStyle: {
    backgroundColor: colors.primary,
    height: 18,
    width: 18,
    borderWidth: 1,
    borderColor: colors.gray,
    top: 2,
  },
  searchBtn: {
    marginVertical: 20,
  },
});

export default SearchFilterScreen;
