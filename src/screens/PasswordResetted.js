import React, { useEffect } from 'react';
import { View, StyleSheet, Image, Text} from 'react-native';

import Screen from '../components/Screen';
import AppButton from '../components/ui/AppButton';
import appFonts from '../config/appFonts';
import AppHeader from '../components/ui/AppHeader';
import Orientation from 'react-native-orientation';

const PasswordResetted = (props) => {

    useEffect(() => {
        Orientation.lockToPortrait();
    }, []);
    

    const _goToLoginScreen = () => {
        props.navigation.navigate('Login');
    };

    return (
        <Screen>
            {/* <AppHeader /> */}
            <View style={styles.container}>

                <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <View style={{ height: "20%", width: '20%', marginBottom:10 }}>
                        <Image
                            source={require('./../assets/images/tick-60x60.png')}
                            style={[styles.icon, { height: 50, width: 50, }]}
                        />
                    </View>


                    <Text style={{
                        marginBottom: 15, fontSize:appFonts.xxxlargeFontSize,
                        fontFamily: appFonts.SourceSansProBold,
                         alignItems: "center"
                    }}>YOUR PASSWORD HAS  </Text>
                     <Text style={{
                        marginBottom: 15, fontSize: appFonts.xxxlargeFontSize,
                        fontFamily: appFonts.SourceSansProBold,
                         alignItems: "center"
                    }}>BEEN RESET</Text>
                     <Text style={{
                        marginBottom: 15, fontSize: appFonts.xxxlargeFontSize,
                        fontFamily: appFonts.SourceSansProBold,
                         alignItems: "center"
                    }}>SUCCESSFULLY</Text>

                </View>



            </View>
            <View style={{ marginHorizontal: 20, marginBottom: "15%" }}>

                <AppButton title="LOGIN AGAIN" onPress={() => {_goToLoginScreen();}}/>
            </View>


        </Screen>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        margin: 5
    },

})

export default PasswordResetted;

