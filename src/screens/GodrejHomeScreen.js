import React, {useCallback, useEffect, useState, useRef} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  BackHandler,
  Alert,
  Dimensions,
  TouchableWithoutFeedback,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useIsFocused, useFocusEffect, StackActions} from '@react-navigation/native';
import GetLocation from 'react-native-get-location';
import {useSelector, useDispatch} from 'react-redux';
import VideoPlayer from 'react-native-video-player';
import Orientation from 'react-native-orientation';
import {EventRegister} from 'react-native-event-listeners';

import Screen from '../components/Screen';
import AppFooter from '../components/ui/AppFooter';
import colors from '../config/colors';
import AppUserChoice from '../components/ui/AppUserChoice';
import AppLandingViewItem from '../components/AppLandingViewItem';
import AppLandingViewItemJustLaunch from '../components/AppLandingViewItemJustLaunch';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';

import * as landingInfoAction from './../store/actions/homeLandingInfoAction';
import * as continueExploringAction from './../store/actions/homeContinueExploringAction';
import * as cityWiseProjectsAction from './../store/actions/homeCityWiseProjectsAction';
import * as newLaunchAction from './../store/actions/homeNewLaunchAction';
import * as greenLivingAction from './../store/actions/homeGreenLivingAction';
import * as awardWiningAction from './../store/actions/homeAwardWiningAction';
import * as menuTypeAction from './../store/actions/menuTypeAction';

import apiLandingPage from './../api/apiLanding';
import appPhoneCall from '../utility/appPhoneCall';
import AppText from '../components/ui/ AppText';
import appFonts from '../config/appFonts';
import appConstant from '../utility/appConstant';

import apiWishlist from './../api/apiWishlist';
import appSnakBar from '../utility/appSnakBar';
import {resetPropertySearch} from '../store/actions/propertySearchAction';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const GodrejHomeScreen = ({navigation}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState();
  const [latitude, setLatitude] = useState('');
  const [longitude, setLongitude] = useState('');
  const [city, setCity] = useState('Mumbai');
  const [isApiLoading, setApiIsLoading] = useState(false);
  const [isInnerLoading, setIsInnerLoading] = useState(false);

  const [isVideoMuted, setIsVideoMuted] = useState(true);
  const [isVideoLoading, setIsVideoLoading] = useState(false);
  const [isVideoEnd, setIsVideoEnd] = useState(false);
  const videoRef = useRef();

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );

  const loginParam = useSelector((state) => state.loginInfo.loginParam);
  const loginUserData = useSelector((state) => state.loginInfo.loginResponse);
  const userId =
    loginUserData && loginUserData.data && loginUserData.data.userid
      ? loginUserData.data.userid
      : '';

  const landingInfoPropertyData = useSelector(
    (state) => state.landingInfo.landingInfo.data,
  );
  const continueExploringPropertyData = useSelector(
    (state) => state.continueExploring.continueExploring.data,
  );
  const cityWisePropertyData = useSelector(
    (state) => state.cityWiseProjects.cityWiseProjects.data,
  );
  const newLaunchPropertyData = useSelector((state) =>
    state.newLaunch.newLaunch.data && state.newLaunch.newLaunch.data.length > 0
      ? state.newLaunch.newLaunch.data[0]
      : null,
  );
  const greenLivingPropertyData = useSelector(
    (state) => state.greenLiving.greenLiving.data,
  );
  const awardWiningPropertyData = useSelector(
    (state) => state.awardWining.awardWining.data,
  );

  const dispatch = useDispatch();
  const isFocused = useIsFocused();

  useEffect(() => {
    if (typeof videoRef != 'undefined' && videoRef && videoRef.current) {
      if (!isFocused) {
        videoRef.current.pause();
      }
    }
  }, [isFocused]);

  useEffect(() => {
    try {
      if (isVideoEnd) {
        if (typeof videoRef != 'undefined' && videoRef && videoRef.current) {
          setIsVideoEnd(false);

          videoRef.current.stop();
          setTimeout(() => {
            videoRef.current.resume();
          }, 250);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }, [isVideoEnd]);

  useEffect(() => {
    Orientation.lockToPortrait();
    dispatch(
      menuTypeAction.setMenuData({
        menuType: 'presale',
      }),
    );
  }, []);

  useEffect(() => {
    // Orientation.lockToPortrait();
    if (deviceUniqueId) {
      loadLocation();
    }
  }, [dispatch, deviceUniqueId]);

  useEffect(() => {
    const eventListener = EventRegister.addEventListener(
      'GodrejHomeUpdateEvent',
      (data) => {
        if (data && data === 'updateList') {
          if (userId) {
            wishlistItems(userId, false);
          }
        }
      },
    );
    const eventListenerOher = EventRegister.addEventListener(
      'GodrejHomeUpdateEventOther',
      (data) => {
        if (data && data === 'updateList') {
          if (userId) {
            wishlistItems(userId, true);
          }
        }
      },
    );

    const eventListenerRedirection = EventRegister.addEventListener(
      'notificationRedirectionEventPre',
      (data) => {
        // console.log('notificationRedirectionEventPre:: ', data);
        onPageRedirection(data);
      },
    );

    const eventListenerRedirectionPost = EventRegister.addEventListener(
      'notificationRedirectionEventPost',
      (data) => {
        // console.log('notificationRedirectionEventPost:: ', data);
        onPageRedirectionPost(data);
      },
    );

    return () => {
      EventRegister.removeEventListener(eventListener);
      EventRegister.removeEventListener(eventListenerOher);
      EventRegister.removeEventListener(eventListenerRedirection);
      EventRegister.removeEventListener(eventListenerRedirectionPost);
    };
  }, [dispatch, userId]);

  const onPageRedirection = (eventData) => {
    if (eventData && eventData.title) {
      if (
        eventData.title == 'birthday' ||
        eventData.title == 'anniversary' ||
        eventData.title == 'my_task'
      ) {
        navigation.navigate('NotificationsScreen');
      } else if (
        eventData.title == 'booking_confirmation_data' ||
        eventData.title == 'eoi_booking_confirmation_data'
      ) {
        if (eventData.booking_id && eventData.user_id) {
          navigation.navigate('BookingDetailsScreen', {
            bookingId: eventData.booking_id && eventData.user_id,
            userId: eventData.user_id,
          });
        } else {
          navigation.navigate('NotificationsScreen');
        }
      } else {
        navigation.navigate('NotificationsScreen');
      }
    } else {
      navigation.navigate('NotificationsScreen');
    }
  };

  const onPageRedirectionPost = (eventData) => {
    if (eventData && eventData.title) {
      if (eventData.title == 'birthday') {
        navigation.navigate('Profile');
      } else if (eventData.title == 'anniversary') {
        navigation.navigate('NotificationsScreen');
      } else if (eventData.title == 'support') {
        navigation.navigate('ReachUs');
      } else if (
        eventData.title == 'payment_realization_task' ||
        eventData.title == 'payment_realization'
      ) {
        if (eventData.booking_id) {
          navigation.navigate('MyAccountScreen', {
            bookingId: eventData.booking_id,
          });
        } else {
          navigation.navigate('NotificationsScreen');
        }
      } else if (eventData.title == 'documents') {
        navigation.navigate('DocumentScreen');
      } else if (
        eventData.title == 'booking_confirmation_data' ||
        eventData.title == 'eoi_booking_confirmation_data'
      ) {
        if (eventData.booking_id && eventData.user_id) {
          navigation.navigate('BookingDetailsScreen', {
            bookingId: eventData.booking_id && eventData.user_id,
            userId: eventData.user_id,
          });
        } else {
          navigation.navigate('NotificationsScreen');
        }
      } else if (eventData.title == 'my_task') {
        navigation.navigate('NotificationsScreen');
      } else if (eventData.title == 'scheduled_visits') {
        navigation.navigate('VisitScreen');
      } else if (eventData.title == 'bank_details_view') {
        if (eventData.booking_id) {
          navigation.navigate('LoanEnquiryScreen', {
            bookingId: eventData.booking_id,
            activeTab: 'LoanDetails',
          });
        } else {
          navigation.navigate('NotificationsScreen');
        }
      } else {
        navigation.navigate('NotificationsScreen');
      }
    } else {
      navigation.navigate('NotificationsScreen');
    }
  };

  const loadLocation = useCallback(async () => {
    setIsLoading(true);
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
      .then(({latitude, longitude}) => {
        setLatitude(latitude);
        setLongitude(longitude);

        getCityName(latitude, longitude);
      })
      .catch((error) => {
        const {code, message} = error;

        loadLandingInfo(city, '', '');
      });
  }, [setLatitude, setLongitude, setIsLoading, deviceUniqueId]);

  const getCityName = useCallback(
    async (latitude, longitude) => {
      const apiKey = appConstant.googleMapKey.key;
      const URL = `https://maps.googleapis.com/maps/api/geocode/json?address=${latitude},${longitude}&key=${apiKey}`;

      let cityName = 'Mumbai';
      try {
        const response = await fetch(URL);
        const responseJson = await response.json();
        if (
          responseJson &&
          responseJson.status &&
          responseJson.status == 'OK' &&
          responseJson.results &&
          responseJson.results.length > 0 &&
          responseJson.results[0].address_components &&
          responseJson.results[0].address_components.length > 0
        ) {
          const addressComponents = responseJson.results[0].address_components;
          const cityResult = addressComponents.findIndex((elements) =>
            elements.types.includes('locality'),
          );

          if (cityResult > -1) {
            cityName = addressComponents[cityResult].long_name;
          }
        }
        console.log('getCityName cityName : ', cityName);
        setCity(cityName);
        loadLandingInfo(cityName, latitude, longitude);
      } catch (error) {
        loadLandingInfo(city, latitude, longitude);
      }
    },
    [setCity, setIsLoading, deviceUniqueId],
  );

  const loadLandingInfo = useCallback(
    async (city, latitude, longitude) => {
      setError(null);
      try {
        await dispatch(
          landingInfoAction.getLandingInfo(
            userId,
            deviceUniqueId,
            city,
            latitude,
            longitude,
          ),
        );
        loadContinueExploringProperty(city, latitude, longitude);
      } catch (err) {
        setError(err.message);
        setIsLoading(false);
      }
    },
    [dispatch, setIsLoading, setError, deviceUniqueId, userId],
  );

  const loadContinueExploringProperty = useCallback(
    async (city, latitude, longitude) => {
      try {
        await dispatch(
          continueExploringAction.getContinueExpoloringProperty(
            userId,
            deviceUniqueId,
          ),
        );
        loadCityWiseProperty(city, latitude, longitude);
      } catch (err) {
        setError(err.message);
        setIsLoading(false);
      }
    },
    [dispatch, setIsLoading, setError, deviceUniqueId, userId],
  );

  const loadCityWiseProperty = useCallback(
    async (city, latitude, longitude) => {
      try {
        await dispatch(
          cityWiseProjectsAction.getCityWiseProperty(
            userId,
            deviceUniqueId,
            city,
            latitude,
            longitude,
          ),
        );
        loadNewLaunchProperty();
      } catch (err) {
        setError(err.message);
        setIsLoading(false);
      }
    },
    [dispatch, setIsLoading, setError, deviceUniqueId, userId],
  );

  const loadNewLaunchProperty = useCallback(async () => {
    try {
      await dispatch(
        newLaunchAction.getNewLaunchProperty(userId, deviceUniqueId),
      );
      loadGreenLivingProperty();
    } catch (err) {
      setError(err.message);
      setIsLoading(false);
    }
  }, [dispatch, setIsLoading, setError, deviceUniqueId, userId]);

  const loadGreenLivingProperty = useCallback(async () => {
    try {
      await dispatch(
        greenLivingAction.getGreenLivingProperty(userId, deviceUniqueId),
      );
      loadAwardWiningProperty();
    } catch (err) {
      setError(err.message);
      setIsLoading(false);
    }
  }, [dispatch, setIsLoading, setError, deviceUniqueId, userId]);

  const loadAwardWiningProperty = useCallback(async () => {
    try {
      await dispatch(
        awardWiningAction.getAwardWiningProperty(userId, deviceUniqueId),
      );
    } catch (err) {
      setError(err.message);
    }
    setIsLoading(false);
  }, [dispatch, setIsLoading, setError, deviceUniqueId, userId]);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        Alert.alert(
          'Exit App',
          'Do you want to exit?',
          [
            {
              text: 'Cancel',
              style: 'cancel',
            },
            {
              text: 'YES',
              onPress: () => {
                BackHandler.exitApp();
              },
            },
          ],
          {cancelable: false},
        );
        return true;
      };

      if (typeof videoRef != 'undefined' && videoRef && videoRef.current) {
        if (isFocused) {
          videoRef.current.resume();
        }
      }

      dispatch(resetPropertySearch());

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () => {
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
      };
    }, []),
  );

  const onAskAnExpert = async () => {
    setApiIsLoading(true);
    try {
      const result = await apiLandingPage.apiLandingPageAskExpert();

      setApiIsLoading(false);
      if (
        result.data &&
        result.data.status == '200' &&
        result.data.expert_ph_no &&
        result.data.expert_ph_no != ''
      ) {
        appPhoneCall.openDeviceCaller(result.data.expert_ph_no);
      }
    } catch (error) {
      setApiIsLoading(false);
    }
  };

  const goToProjectDetails = () => {
    // navigation.navigate('ProjectDetails', {item: landingInfoPropertyData});

    navigation.dispatch(
      StackActions.push('ProjectDetails', {item: landingInfoPropertyData}),
    );
  };

  const wishlistItems = async (user_id, isNotShow) => {
    try {
      if (!isNotShow) {
        setIsInnerLoading(true);
      }

      const requestData = {
        user_id: user_id,
      };
      const wishlistItems = await apiWishlist.apiWishlistItems(requestData);

      if (wishlistItems.data) {
        if (wishlistItems.data.status && wishlistItems.data.status == '200') {
          const responseItems = wishlistItems.data.data;
          if (responseItems && responseItems.length > 0) {
            dispatch(
              continueExploringAction.updateProjetWishlistArr(responseItems),
            );
            dispatch(
              cityWiseProjectsAction.updateProjetWishlistArr(responseItems),
            );
            dispatch(newLaunchAction.updateProjetWishlistArr(responseItems));
            dispatch(greenLivingAction.updateProjetWishlistArr(responseItems));
            dispatch(awardWiningAction.updateProjetWishlistArr(responseItems));
          }
        } else {
          appSnakBar.onShowSnakBar(
            wishlistItems.data.msg
              ? wishlistItems.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
        if (!isNotShow) {
          setIsInnerLoading(false);
        }
      } else {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      if (!isNotShow) {
        setIsInnerLoading(false);
      }
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const addToWishList = (item) => {
    try {
      setIsInnerLoading(true);

      const requestData = {
        user_id: userId,
        proj_id: item.proj_id,
        type: item.wishlist_status == '0' ? 'add' : 'delete',
      };

      apiWishlist
        .apiAddRemoveWishlist(requestData)
        .then((wishlistItem) => {
          if (wishlistItem.data) {
            if (wishlistItem.data.status && wishlistItem.data.status == '200') {
              appSnakBar.onShowSnakBar(
                wishlistItem.data.msg ? wishlistItem.data.msg : 'Wishlist...',
                'LONG',
              );
              EventRegister.emit('wishlistUpdateEvent', 'update');

              dispatch(
                continueExploringAction.updateProjetWishlist(item.proj_id),
              );
              dispatch(
                cityWiseProjectsAction.updateProjetWishlist(item.proj_id),
              );
              dispatch(newLaunchAction.updateProjetWishlist(item.proj_id));
              dispatch(greenLivingAction.updateProjetWishlist(item.proj_id));
              dispatch(awardWiningAction.updateProjetWishlist(item.proj_id));
            } else {
              appSnakBar.onShowSnakBar(
                wishlistItem.data.msg
                  ? wishlistItem.data.msg
                  : appConstant.appMessage.APP_GENERIC_ERROR,
                'LONG',
              );
            }

            setIsInnerLoading(false);
          } else {
            appSnakBar.onShowSnakBar(
              appConstant.appMessage.APP_GENERIC_ERROR,
              'LONG',
            );
          }
        })
        .catch((error) => {
          console.log(error);
          setIsInnerLoading(false);
          appSnakBar.onShowSnakBar(
            appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        });
    } catch (error) {
      console.log(error);
      setIsInnerLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const onHandleRetryBtn = () => {
    if (deviceUniqueId) {
      loadLocation();
    }
  };

  const onVideoMuteHandler = () => {
    setIsVideoMuted(isVideoMuted ? false : true);
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <ScrollView style={styles.scrollContainer}>
        {!isLoading && (
          <View style={styles.container}>
            {landingInfoPropertyData && (
              <TouchableWithoutFeedback onPress={goToProjectDetails}>
                <View>
                  <View style={styles.videoContainer}>
                    <VideoPlayer
                      ref={videoRef}
                      pointerEvents="none"
                      video={{
                        uri: landingInfoPropertyData.field_featured_media_url,
                      }}
                      style={{height: '100%', width: '100%'}}
                      thumbnail={{
                        uri:
                          landingInfoPropertyData.field_featured_media_thumb_url,
                      }}
                      muted={isVideoMuted}
                      defaultMuted={false}
                      disableSeek={false}
                      autoplay={true}
                      loop={true}
                      hideControlsOnStart={true}
                      resizeMode="cover"
                      disableControlsAutoHide={true}
                      customStyles={{
                        controls: {opacity: 0},
                        seekBar: {opacity: 0},
                      }}
                      onLoad={() => {
                        setIsVideoLoading(true);
                      }}
                      onEnd={() => {
                        if (
                          typeof videoRef != 'undefined' &&
                          videoRef &&
                          videoRef.current
                        ) {
                          setIsVideoEnd(true);
                        }
                      }}
                    />
                    <View style={styles.viewOverlay}>
                      {isVideoLoading ? (
                        <TouchableOpacity
                          onPress={onVideoMuteHandler}
                          activeOpacity={0.8}>
                          <View
                            style={{
                              padding: 10,
                              margin: 5,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <Image
                              style={{
                                width: windowWidth > 360 ? 29 : 20,
                                height: windowWidth > 360 ? 29 : 20,
                              }}
                              source={
                                isVideoMuted
                                  ? require('./../assets/images/mute.png')
                                  : require('./../assets/images/sound.png')
                              }
                            />
                          </View>
                        </TouchableOpacity>
                      ) : null}
                    </View>
                  </View>

                  <View style={styles.videoTxtContainer}>
                    <View style={styles.info}>
                      <View style={styles.separator} />
                      <AppText style={styles.separatorTxt}>
                        Featured Residences
                      </AppText>
                      <View style={styles.separator} />
                    </View>

                    <AppText style={styles.videoProjectTitle} numberOfLines={2}>
                      {landingInfoPropertyData.title}
                    </AppText>

                    <AppText style={styles.videoProjectAddress}>
                      {landingInfoPropertyData.location}
                    </AppText>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            )}

            {continueExploringPropertyData && (
              <AppLandingViewItem
                title="CONTINUE EXPLORING…"
                loginParam={loginParam}
                projectData={continueExploringPropertyData}
                onPressWishlist={addToWishList}
              />
            )}

            {/* <AppUserChoice title="WHAT KIND OF LIVING EXPERIENCE ARE YOU LOOKING FOR?" /> */}

            {cityWisePropertyData &&
              cityWisePropertyData.length > 0 &&
              cityWisePropertyData.map((cityItem) => (
                <AppLandingViewItem
                  key={cityItem.city_id}
                  title={cityItem.city_name}
                  projectData={cityItem.proj_list}
                  loginParam={loginParam}
                  isCityWise={true}
                  onPressWishlist={addToWishList}
                />
              ))}

            {/* <AppUserChoice title="WHAT TYPE OF HOME PERSONALITY ARE YOU?" /> */}

            {newLaunchPropertyData && (
              <AppLandingViewItemJustLaunch
                title="Just Launched"
                projectData={newLaunchPropertyData}
                onPressWishlist={addToWishList}
                loginParam={loginParam}
              />
            )}

            {greenLivingPropertyData && (
              <AppLandingViewItem
                title="Green Living"
                loginParam={loginParam}
                projectData={greenLivingPropertyData}
                onPressWishlist={addToWishList}
              />
            )}

            {awardWiningPropertyData && (
              <AppLandingViewItem
                title="Award Winning"
                loginParam={loginParam}
                projectData={awardWiningPropertyData}
                onPressWishlist={addToWishList}
              />
            )}

            <AppUserChoice title="ASK AN EXPERT" onPress={onAskAnExpert} />
            <View style={{height: 40}} />
          </View>
        )}
      </ScrollView>

      <AppFooter isPostSales={false} activePage={1} />

      <AppOverlayLoader
        isLoading={isLoading || isApiLoading || isInnerLoading}
      />
    </Screen>
  );
};

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  videoContainer: {
    width: '100%',
    height: windowHeight * 0.75,
  },
  viewOverlay: {
    height: '100%',
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: '#00000000',
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    zIndex: 18,
  },
  videoTxtContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255,255,255,0.5)',
    paddingVertical: 10,
    marginBottom: 25,
  },
  info: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  separator: {
    width: 38,
    height: 1,
    backgroundColor: colors.jaguar,
  },
  separatorTxt: {
    marginHorizontal: 12,
    color: colors.jaguar,
    fontSize: appFonts.largeFontSize,
  },
  videoProjectTitle: {
    fontFamily: appFonts.RobotoBold,
    fontSize: appFonts.xxlargeFontSize,
    marginVertical: 10,
    paddingHorizontal: 28,
    color: colors.jaguar,
    textTransform: 'uppercase',
    textAlign: 'center',
    lineHeight: 30,
  },
  videoProjectAddress: {
    color: colors.jaguar,
    fontSize: appFonts.largeBold,
  },
});

export default GodrejHomeScreen;
