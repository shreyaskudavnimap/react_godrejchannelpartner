import React, { useEffect, useState } from 'react';
import {
  View, StyleSheet, ScrollView, TextInput,
  Dimensions, Image, Text, TouchableOpacity, PermissionsAndroid
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import AppButton from '../components/ui/AppButton';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import Screen from '../components/Screen';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppFooter from '../components/ui/AppFooter';
import * as normStyle from '../styles/StyleSize';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import apiLogin from './../api/apiLogin';
import appSnakBar from '../utility/appSnakBar';
import { useNavigation } from '@react-navigation/native';
import ShowPassword from '../components/showPassword';
import { CommonActions } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { USER_LOGIN } from './../store/actions/userLoginAction';
import { GoogleAnalyticsTracker } from "react-native-google-analytics-bridge";
import Orientation from 'react-native-orientation';


const ChangesPassword = ({ props, route }) => {

  const routeParams = route.params;

  const userId =
    routeParams && routeParams.userId && routeParams.userId
      ? routeParams.userId
      : '';

  const emplId =
  routeParams && routeParams.emplId && routeParams.emplId
    ? routeParams.emplId
    : '';

  const [oldpassword, Setoldpassword] = useState("");
  const [newpassword, Setnewpassword] = useState("");
  const [confirmpassword, Setconfirmpassword] = useState("");
  const [isApiLoading, setApiIsLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [disableButton, setDisableButton] = useState(true);
  const navigation = useNavigation();

  const dispatch = useDispatch();

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);


  useEffect(() => {
    _getUserInfo();
  }, [loginvalue]);
  var tracker = new GoogleAnalyticsTracker("UA-171278332-1");

  const loginvalue = useSelector(
    (state) => state.loginInfo.loginResponse,
  );

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );
  const menuType = useSelector((state) => state.menuType.menuType);
  const goBack = () => {
    navigation.goBack();
  };

  const _getUserInfo = () => {

  };

  const validatePassword = (pwd) => {
    var patt = new RegExp("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d!@#$%^*?%/'._\\-,\\\\`~]{6,14}$");
    var res = patt.test(pwd);
    if (pwd.length < 6) {
      appSnakBar.onShowSnakBar(`Password should be minimum 6 characters long`, 'LONG');
      return false;
    } else {
      if (pwd.length > 14) {
        appSnakBar.onShowSnakBar(`Password should be maximum 6 characters long`, 'LONG');
        return false;
      } else {
        if (res) {
          return true;
        } else {
          appSnakBar.onShowSnakBar(`Password should be alpha-numeric`, 'LONG');
          return false;
        }
      }
    }
  };

  const _updatePassword = async () => {
    if (newpassword.trim() == '' || confirmpassword.trim() == '' || oldpassword.trim() == '') {
      return null;
    }
    if (validatePassword(newpassword)) {
      if (newpassword == confirmpassword) {
        _callApiChangepassword();
        tracker.trackEvent("Reset Password", "Submit", { label: "Reset Password Success", value: 10 });
      } else {
        appSnakBar.onShowSnakBar(`Password and Confirm Password mismatch`, 'LONG');
      }
    }
  };

  const _callApiChangepassword = async () => {
    var url = 'https://cp.godrejproperties.com/webapi/ChangePassword.ashx?userid='+emplId+'&old_password='+oldpassword.trim()+'&new_password='+newpassword.trim();
    console.log(url);
    var result = await apiLogin.apiGetRequest(url)
    console.log("result", result);
    appSnakBar.onShowSnakBar(result.msg, 'LONG');

    if(result.statusCode == 200){
      navigation.goBack();
    }

    // const request = {
    //   user_id: userId,
    //   old_pass: oldpassword.trim(),
    //   new_pass: newpassword.trim(),
    //   cnf_pass: confirmpassword.trim(),
    // };
    // console.log(request);
    // setIsLoading(true);
    // const result = await apiLogin.apiForUpdateUserPassword(request);
    // setIsLoading(false);
    // var status = result.data.status;
    // console.log(result.data);
    // if (status == 200) {
    //   _goToLoginscreen();
    //   tracker.trackEvent("Go To Page", "Click", { label: "Login", value: 11 });
    // } else {
    //   appSnakBar.onShowSnakBar(result.data.msg, 'LONG');
    // }
  };

  const _goToLoginscreen = async () => {
    try {
      await AsyncStorage.removeItem('sessionId');
      await AsyncStorage.removeItem('sessionName');
      await AsyncStorage.removeItem('token');
      await AsyncStorage.removeItem('data');
      await AsyncStorage.removeItem('status');
      await AsyncStorage.removeItem('isLogin');
      await AsyncStorage.removeItem('rmData');
      dispatch(clearRMData());
      // OfflineImageStore.clearStore(() => {});
    } catch (exception) {
      console.log(exception);
      //return false;
    }
    dispatch({ type: USER_LOGIN, data: { isLogin: false } });
    appSnakBar.onShowSnakBar(`Login with new password`, 'LONG');
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{ name: 'Login' }],
      }),
    );
  };

  const _onChangePassword = (e) => {
    Setoldpassword(e);
    if (e != '' && newpassword != '' && confirmpassword != '') {
      setDisableButton(false);
    } else {
      setDisableButton(true);
    }
  }

  const _onchangeNewPassword = (e) => {
    Setnewpassword(e);
    if (oldpassword != '' && e != '' && confirmpassword != '') {
      setDisableButton(false);
    } else {
      setDisableButton(true);
    }
  };

  const _onChangeConfirmPwd = (e) => {
    Setconfirmpassword(e);
    if (oldpassword != '' && newpassword != '' && e != '') {
      setDisableButton(false);
    } else {
      setDisableButton(true);
    }
  }


const onHandleRetryBtn = () => {
};

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />
      
    <ScrollView>
      <View style={styles.container}>

        <View style={{ width: '100%', alignItems: 'flex-start' }}>
          <AppText style={styles.pageTitle}>Edit Password</AppText>
        </View>


        <View style={{ width: '100%', alignItems: 'flex-start' }}>
          <AppText style={{ marginLeft: '5%', color: colors.gray }}>Old Password *</AppText>
        </View>
        <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ width: '90%', height: normStyle.normalizeWidth(40), }}>
            <ShowPassword
              title="Enter old password"
              colorText="black"
              onChangeText={(e) => { _onChangePassword(e); }}
              value={oldpassword}
            />
          </View>
        </View>

        <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(20) }}>
          <AppText style={{ marginLeft: '5%', color: colors.gray }}>New Password *</AppText>
        </View>
        <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ width: '90%', height: normStyle.normalizeWidth(40), }}>
            <ShowPassword
              title="Enter New Password"
              colorText="black"
              onChangeText={(e) => { _onchangeNewPassword(e); }}
              value={newpassword}
            />
          </View>
        </View>

        <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(20) }}>
          <AppText style={{ marginLeft: '5%', color: colors.gray }}>Confirm Password *</AppText>
        </View>
        <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ width: '90%', height: normStyle.normalizeWidth(40), }}>
            <ShowPassword
              title="Re-Enter New Password"
              colorText="black"
              onChangeText={(e) => { _onChangeConfirmPwd(e); }}
              value={confirmpassword}
            />
          </View>
        </View>


        {/* <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}> */}
          <View style={styles.button}>
            <AppButton
            color={disableButton ? 'charcoal' : 'secondary'}
            disabled={disableButton}
            textColor = {disableButton ? 'primary' : 'primary'}
              title="Update"
              onPress={() => {
                _updatePassword()
              }}
            />
          {/* </View> */}
        </View>

      </View>
    </ScrollView>
      
      <AppFooter activePage={0} isPostSales={menuType == 'postsale' ? true : false} />
      <AppOverlayLoader isLoading={isLoading || isApiLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  pageTitle: {
    fontSize: normStyle.normalizeWidth(20),
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginLeft: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(40),
  },
  nameStyle: {
    width: '90%', height: normStyle.normalizeWidth(40),
    borderBottomWidth: 0.5, fontSize: normStyle.normalizeWidth(18),
    fontWeight: 'bold',
  },
  seprator: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  line: {
    width: '95%',
    height: normStyle.normalizeWidth(1),
    backgroundColor: colors.gray,
    marginTop: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(20),
  },
  viewStyle: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: normStyle.normalizeWidth(20),
  },
  imageView1: {
    width: normStyle.normalizeWidth(130),
    height: normStyle.normalizeWidth(105),
    flexDirection: 'row',
  },
  imageText: {
    width: normStyle.normalizeWidth(110),
    height: normStyle.normalizeWidth(110),
    borderRadius: normStyle.normalizeWidth(55),
    textAlign: 'center',
    textAlignVertical: 'center',
    fontWeight: 'bold',
    fontSize: normStyle.normalizeWidth(30),
  },
  editImg: {
    width: normStyle.normalizeWidth(20),
    height: normStyle.normalizeWidth(20),
    marginLeft: normStyle.normalizeWidth(10),
  },
  editImg1: {
    width: normStyle.normalizeWidth(20),
    height: normStyle.normalizeWidth(20),
    marginRight: normStyle.normalizeWidth(10),
  },
  text1: {
    marginTop: normStyle.normalizeWidth(7),
  },
  view1: {
    width: '100%', flexDirection: 'row'
  },
  view2: {
    width: '100%', flexDirection: 'row',
    marginTop: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(20),
  },
  verifiedImg: {
    width: normStyle.normalizeWidth(10),
    height: normStyle.normalizeWidth(10),
  },
  editProfileStyle: {
    flex: 0.2, bottom: 0, right: 0, position: 'absolute',
    paddingRight: normStyle.normalizeWidth(20)
  },
  cameraStyle: {
    width: normStyle.normalizeWidth(25),
    height: normStyle.normalizeWidth(25),
  },
  edit1: {
    flex: 0.2, alignItems: 'flex-end', paddingRight: '2.5%',
  },
  button: {
    // width: normStyle.normalizeWidth(100),
    // height: normStyle.normalizeWidth(0),
    alignItems: 'center', justifyContent: 'center',
    marginHorizontal:15,
    paddingBottom: normStyle.normalizeWidth(40),
    paddingTop: normStyle.normalizeWidth(40),
  }
});

export default ChangesPassword;
