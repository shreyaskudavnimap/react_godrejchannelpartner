import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  TextInput,
  ScrollView,
  CheckBox,
} from 'react-native';
import globlaStyles from '../styles/GlobleStyle';
import Orientation from 'react-native-orientation';
import appFonts from '../config/appFonts';
const Signup = (props) => {
  const [isSelected, setisSelected] = useState(false);

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  return (
    <>
      <View style={{height: '100%', width: '100%'}}>
        {/* <ImageBackground source={require('../assets/images/login-bg.png')} style={{ width: '100%', height: '100%', backgroundColor:'#fff'}}> */}
        <ScrollView style={{paddingBottom: '5%'}}>
          <View>
            <View
              style={{
                flexDirection: 'row',
                marginLeft: '5%',
                marginRight: '5%',
                justifyContent: 'space-between',
              }}>
              <View style={{width: '10%'}}>
                <Image source={require('../assets/images/back.png')} />
                {/* <Text style={{color:"#fff"}}>Back</Text> */}
              </View>
              <View
                style={{
                  height: 200,
                  width: '70%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: '10%',
                }}>
                <Image
                  source={require('../assets/images/gpl-siginup-img.png')}
                  style={{height: 150, width: '100%'}}
                />
                <Text style={{color: 'blue', fontWeight: 'bold', fontSize: appFonts.largeBoldx}}>
                  {' '}
                  Sign Up
                </Text>
              </View>
              <View style={{width: '10%'}}>
                <Text style={{color: 'gray', fontWeight: 'bold'}}>Skip</Text>
              </View>
            </View>
            <View style={{marginLeft: '5%', marginRight: '5%'}}>
              <View style={{height: 80}}>
                <TextInput
                  style={{
                    height: 70,
                    borderColor: 'gray',
                    borderBottomWidth: 2,
                    marginBottom: 20,
                  }}
                  placeholder="First Name"
                  underlineColorAndroid="transparent"
                  // placeholderTextColor="#fff"
                />
              </View>
              <View style={{height: 80}}>
                <TextInput
                  style={{
                    height: 70,
                    borderColor: 'gray',
                    borderBottomWidth: 2,
                    marginBottom: 20,
                  }}
                  placeholder="Last Name"
                  underlineColorAndroid="transparent"
                  // placeholderTextColor="#fff"
                />
              </View>
              <View style={{height: 80}}>
                <TextInput
                  style={{
                    height: 70,
                    borderColor: 'gray',
                    borderBottomWidth: 2,
                    marginBottom: 20,
                  }}
                  placeholder="Mobile Number"
                  underlineColorAndroid="transparent"
                  // placeholderTextColor="#fff"
                />
              </View>
              <View style={{height: 80}}>
                <TextInput
                  style={{
                    height: 70,
                    borderColor: 'gray',
                    borderBottomWidth: 2,
                    marginBottom: 20,
                  }}
                  placeholder="Email ID"
                  underlineColorAndroid="transparent"
                  // placeholderTextColor="#fff"
                />
              </View>
              <View style={{height: 80}}>
                <TextInput
                  style={{
                    height: 70,
                    borderColor: 'gray',
                    borderBottomWidth: 2,
                    marginBottom: 20,
                  }}
                  placeholder="Password"
                  underlineColorAndroid="transparent"
                  // placeholderTextColor="#fff"
                />
              </View>
              <View style={{height: 80}}>
                <TextInput
                  style={{
                    height: 70,
                    borderColor: 'gray',
                    borderBottomWidth: 2,
                    marginBottom: 20,
                  }}
                  placeholder="Confirm Password"
                  underlineColorAndroid="transparent"
                  // placeholderTextColor="#fff"
                />
              </View>
              <View style={{flexDirection: 'row'}}>
                <CheckBox
                  value={isSelected}
                  onValueChange={() => {
                    setisSelected(true);
                  }}
                  style={{alignSelf: 'center'}}
                />
                <Text style={{margin: 8}}>
                  I/We have full read, understood and agree to{' '}
                  <Text
                    style={{color: 'blue', textDecorationLine: 'underline'}}>
                    Terms of Use and Privacy Policy
                  </Text>
                </Text>
              </View>
              <View
                style={{
                  height: 40,
                  backgroundColor: 'gray',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: 'bold', fontSize: appFonts.largeBold, color: '#fff'}}>
                  Create Account
                </Text>
              </View>
              <View
                style={{
                  height: 30,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text>
                  Alerady have an account?{' '}
                  <Text
                    style={{color: '#fff'}}
                    onPress={() => props.navigation.navigate('Login')}>
                    Login
                  </Text>
                </Text>
              </View>
            </View>
          </View>
        </ScrollView>
        {/* </ImageBackground> */}
      </View>
    </>
  );
};
export default Signup;
