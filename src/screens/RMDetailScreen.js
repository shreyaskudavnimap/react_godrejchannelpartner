import React, { useEffect } from 'react';
import {View, ScrollView, StyleSheet, Image} from 'react-native';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import HTML from 'react-native-render-html';

import appFonts from '../config/appFonts';
import colors from '../config/colors';
import Orientation from 'react-native-orientation';

const RMDetailScreen = (props) => {

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  
  return (
    <Screen>
      <AppHeader />
      <ScrollView>
        <View style={styles.container}>
          <AppTextBold style={styles.pageTitle}>Reach Us</AppTextBold>
          <View style={styles.imgContainer}>
          
             <Image
                            source={props.route.params.rmImg != "" ? { uri: props.route.params.rmImg } : require('./../assets/images/no-user-img.png')}
                            style={[styles.icon, { height: 104, width: 104, borderRadius: 52 }]}
                        />
            <View style={{margin: 20,width: "65%", }}>
              <AppTextBold>{props.route.params.rmName}</AppTextBold>
              {props.route.params.npsScore != '' && (
                <AppText>{props.route.params.npsMonth!= "" && props.route.params.npsYear != ''? props.route.params.npsMonth + ", " : null}
                  {props.route.params.npsYear != ''
                    ? props.route.params.npsYear + ' '
                    : null}
                  Net Promoter Score: {props.route.params.npsScore}%
                </AppText>
              )}
            </View>
          </View>

          <View style={styles.detailContainer}>
            <HTML
            allowFontScaling={false} 
              tagsStyles={{
                p: {
                  fontSize: appFonts.largeFontSize,
                  lineHeight: 24,
                  textAlign: 'justify',
                  fontFamily: appFonts.SourceSansProRegular,
                },
              }}
              ignoredStyles={['font-family']}
              html={props.route.params.rmInfo}
            />
          </View>
        </View>
      </ScrollView>
      <AppFooter activePage={0} isPostSales={true} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginBottom: 30,
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.jaguar,
    textTransform: 'uppercase',
  },
  imgContainer: {
    flexDirection: 'row',
    // justifyContent: "space-around",
    padding: 10,
    alignItems: 'center',
    marginTop: 10,
  },
  image: {
    height: 104,
    width: 104,
    borderRadius: 52,
  },
  detailContainer: {
    paddingVertical: '10%',
    paddingHorizontal: '3%',
  },
  text: {
    lineHeight: 28,
    fontSize: appFonts.largeFontSize,
    marginBottom: '5%',
  },
});

export default RMDetailScreen;
