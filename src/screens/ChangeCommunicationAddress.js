import React, { useEffect, useState } from 'react';
import {
  View, StyleSheet, ScrollView, TextInput,
  Dimensions, Image, Text, TouchableOpacity, PermissionsAndroid
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import AppButton from '../components/ui/AppButton';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import Screen from '../components/Screen';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppFooter from '../components/ui/AppFooter';
import * as normStyle from '../styles/StyleSize';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import apiLogin from './../api/apiLogin';
import appSnakBar from '../utility/appSnakBar';
import { useNavigation } from '@react-navigation/native';
import ShowPassword from '../components/showPassword';
import { CommonActions } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { USER_LOGIN } from './../store/actions/userLoginAction';
import { GoogleAnalyticsTracker } from "react-native-google-analytics-bridge";
import Orientation from 'react-native-orientation';
import DropDownPicker from 'react-native-dropdown-picker';


const ChangeCommunicationAddress = ({ props, route }) => {

  const routeParams = route.params;

  const userId =
    routeParams && routeParams.userId && routeParams.userId
      ? routeParams.userId
      : '';

  const emplId =
    routeParams && routeParams.emplId && routeParams.emplId
      ? routeParams.emplId
      : '';

  const comm_country =
    routeParams && routeParams.comm_country && routeParams.comm_country
      ? routeParams.comm_country
      : '';

  const comm_state =
    routeParams && routeParams.comm_state && routeParams.comm_state
      ? routeParams.comm_state
      : '';

  const comm_city =
    routeParams && routeParams.comm_city && routeParams.comm_city
      ? routeParams.comm_city
      : '';

  const comm_zipcode =
    routeParams && routeParams.comm_zipcode && routeParams.comm_zipcode
      ? routeParams.comm_zipcode
      : '';
  const comm_street =
    routeParams && routeParams.comm_street && routeParams.comm_street
      ? routeParams.comm_street
      : '';


  const [oldpassword, Setoldpassword] = useState("");
  const [newpassword, Setnewpassword] = useState("");
  const [confirmpassword, Setconfirmpassword] = useState("");
  const [isApiLoading, setApiIsLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [disableButton, setDisableButton] = useState(true);
  const navigation = useNavigation();

  const dispatch = useDispatch();

  const [userName, SetUserName] = useState("");
  const [password, SetPassword] = useState("");

  const [newCountry, SetCountry] = useState("");
  const [newState, SetState] = useState("");
  const [newCity, SetCity] = useState("");
  const [newStreet, SetStreet] = useState("");
  const [newZipCode, SetZipCode] = useState("");
  
  var dropDownCountries = []
  var dropDownStates = [];
  var [dropDownCities, SetDropDownCities] = useState([]);

  var countryList = [];
  var stateList = [];
  var cityList = [];
  

  const FillCountryDropDown = async() => {
    const countryUrl = 'http://cp.godrejproperties.com/gplservice/CPService.svc/GetCountry';
    countryList = await apiLogin.apiGetRequest(countryUrl);
    //console.log("countryList", countryList.GetCountryjsonResult);
    var countries = countryList.GetCountryjsonResult;
    for(var i=0; i < countries.length; i++) {
      dropDownCountries.push({ label: countries[i].CountryName, value: countries[i].CountryName })
    }
    // console.log("dropDownCountries", dropDownCountries);
  }

  const FillStateDropDown = async() => {
    const stateUrl = 'http://cp.godrejproperties.com/gplservice/CPService.svc/GetStateData/1';
    stateList = await apiLogin.apiGetRequest(stateUrl);
    //console.log("stateList", stateList.GetStateDatajsonResult);
    var states = stateList.GetStateDatajsonResult;
    for(var i=0; i < states.length; i++) {
      dropDownStates.push({ label: states[i].StateName.replace("\n", ""), value: states[i].StateName.replace("\n", "") })
    }
    //console.log("dropDownStates", dropDownStates);
  }
  
  const FillCityDropDown = async(selectedState) => {
    dropDownCities = [];
    var selectedStateId = Object.values(stateList)[0].filter(x => x.StateName == selectedState.value)[0].StateId;
    //console.log("selectedStateId", selectedStateId);
    const cityUrl = 'http://cp.godrejproperties.com/gplservice/CPService.svc/GetCity/' + selectedStateId;
    cityList = await apiLogin.apiGetRequest(cityUrl);
    var cities = cityList.GetCityjsonResult;
    for(var i=0; i < cities.length; i++) {
      dropDownCities.push({ label: cities[i].CityName, value: cities[i].CityName })
    }
    SetDropDownCities(dropDownCities);
    //console.log("dropDownCities", dropDownCities);
  }

  FillCountryDropDown();
  FillStateDropDown();
  
  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  const onChangeCountryDropDown = (selectedItem) => {
    SetCountry(selectedItem.value);
  }

  const onChangeStateDropDown = (selectedItem) => {
    SetState(selectedItem.value);
  }

  const onChangeCityDropDown = (selectedItem) => {
    SetCity(selectedItem.value);
  }

  const Submit = async () => {
    console.log("Street", newStreet);
    console.log("Country", newCountry);
    console.log("State", newState);
    console.log("City", newCity);
    console.log("ZipCode", newZipCode);

    var url = 'https://b907974dca41.ngrok.io/webapi/GetProfileData.ashx?userid='+ emplId + 
    '&module=comm_addr' + '&newCommAddress=' + newStreet + '&oldCommAddress='+ comm_street
    + '&newCommCity='+ newCity + '&oldCommCity='+ comm_city
    + '&newCommState='+ newState + '&oldCommState='+ comm_state
    + '&newCommZipPostal='+ newZipCode + '&oldCommZipPostal='+ comm_zipcode
    + '&input_user=' + userName +'&input_pass=' + password;
    
    var result = await apiLogin.apiGetRequest(url);
    if (result == "Updated") {
      appSnakBar.onShowSnakBar('Communication Address Updated Successfully', 'LONG');
      navigation.goBack();
    }
    else if (result == "INVALID_LOGIN") {
      appSnakBar.onShowSnakBar('Invalid Login Details', 'LONG');
    }
  };

  useEffect(() => {
    _getUserInfo();
  }, [loginvalue]);

  var tracker = new GoogleAnalyticsTracker("UA-171278332-1");

  const loginvalue = useSelector(
    (state) => state.loginInfo.loginResponse,
  );

  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );
  const menuType = useSelector((state) => state.menuType.menuType);
  const goBack = () => {
    navigation.goBack();
  };

  const _getUserInfo = () => {

  };

  const onHandleRetryBtn = () => {
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />

      <ScrollView>
        <View style={styles.container}>

          <View style={{ width: '100%', alignItems: 'flex-start' }}>
            <AppText style={styles.pageTitle}>Update Communication Address</AppText>
          </View>

          <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(10) }}>
            <Text style={{ marginLeft: '5%', color: colors.gray }}>Existing Communication Address</Text>
          </View>
          <View style={{ width: '100%', marginLeft: '5%' }}>
              <AppText style={styles.text1}>{comm_street }</AppText>
              <AppText style={styles.text1}>{ comm_country }, { comm_state }, { comm_city }, { comm_zipcode }</AppText>
          </View>

          <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(25) }}>
            <Text style={{ marginLeft: '5%', color: colors.gray }}>Communication Street *</Text>
          </View>
          <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', marginBottom:10 }}>
            <TextInput
              style={styles.nameStyle}
              placeholder={"Enter Street"}
              placeholderTextColor={colors.gray}
              value={newStreet}
              onChangeText={e => {
                SetStreet(e);
              }}
            />
          </View>


          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
                <DropDownPicker
                  items={dropDownCountries}
                  style={styles.input}
                  onChangeItem={(value) => {
                    onChangeCountryDropDown(value);
                  }}
                  placeholder={"Select Country"}
                />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
                <DropDownPicker
                  items={dropDownStates}
                  style={styles.input}
                  onChangeItem={(value) => {
                    onChangeStateDropDown(value);
                    FillCityDropDown(value);
                  }}
                  placeholder={"Select State"}
                />
            </View>
          </View>
          

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
                <DropDownPicker
                  items={dropDownCities}
                  style={styles.input}
                  onChangeItem={(value) => {
                    onChangeCityDropDown(value);
                  }}
                  placeholder={"Select City"}
                />
            </View>
          </View>

          <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(10) }}>
            <Text style={{ marginLeft: '5%', color: colors.gray }}>Communication ZipCode *</Text>
          </View>
          <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', marginBottom:10 }}>
            <TextInput
              style={styles.nameStyle}
              placeholder={"Enter Zipcode"}
              placeholderTextColor={colors.gray}
              value={newZipCode}
              onChangeText={e => {
                SetZipCode(e);
              }}
            />
          </View>

          <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(10) }}>
            <Text style={{ marginLeft: '5%', color: colors.gray }}>Username * </Text>
          </View>
          <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', marginBottom:10 }}>
            <TextInput
              style={styles.nameStyle}
              placeholder={"Enter username"}
              placeholderTextColor={colors.gray}
              value={userName}
              color={colors.secondaryDark}
              onChangeText={e => {
                SetUserName(e);
              }}
            />
          </View>

          <View style={{ width: '100%', alignItems: 'flex-start', marginTop: normStyle.normalizeWidth(10) }}>
            <Text style={{ marginLeft: '5%', color: colors.gray }}>Password *</Text>
          </View>
          <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', marginBottom:10 }}>
            <TextInput
              style={styles.nameStyle}
              placeholder={"Enter Password"}
              placeholderTextColor={colors.gray}
              value={password}
              color={colors.secondaryDark}
              //onBlur={() => {handleMobileChange();}}
              onChangeText={e => {
                SetPassword(e);
              }}
            />
          </View>

          {/* <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}> */}
          <View style={styles.button}>
            <AppButton
              color={'secondary'}
              // disabled={disableButton}
              textColor={'primary'}
              title="Submit"
              onPress={() => {
                Submit()
              }}
            />
            {/* </View> */}
          </View>

        </View>
      </ScrollView>

      <AppFooter activePage={0} isPostSales={menuType == 'postsale' ? true : false} />
      <AppOverlayLoader isLoading={isLoading || isApiLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  formStyle: {
    flex: 1,
    alignSelf: 'flex-start',
    width: '100%',
    marginTop: 10,
    marginBottom: 10,
    paddingHorizontal: 20,
  },
  pageTitle: {
    fontSize: normStyle.normalizeWidth(20),
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginLeft: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(40),
  },
  nameStyle: {
    width: '90%', height: normStyle.normalizeWidth(40),
    borderBottomWidth: 0.5, fontSize: normStyle.normalizeWidth(18),
    fontSize: 13,
  },
  seprator: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  line: {
    width: '95%',
    height: normStyle.normalizeWidth(1),
    backgroundColor: colors.gray,
    marginTop: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(20),
  },
  viewStyle: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: normStyle.normalizeWidth(20),
  },
  imageView1: {
    width: normStyle.normalizeWidth(130),
    height: normStyle.normalizeWidth(105),
    flexDirection: 'row',
  },
  imageText: {
    width: normStyle.normalizeWidth(110),
    height: normStyle.normalizeWidth(110),
    borderRadius: normStyle.normalizeWidth(55),
    textAlign: 'center',
    textAlignVertical: 'center',
    fontWeight: 'bold',
    fontSize: normStyle.normalizeWidth(30),
  },
  editImg: {
    width: normStyle.normalizeWidth(20),
    height: normStyle.normalizeWidth(20),
    marginLeft: normStyle.normalizeWidth(10),
  },
  editImg1: {
    width: normStyle.normalizeWidth(20),
    height: normStyle.normalizeWidth(20),
    marginRight: normStyle.normalizeWidth(10),
  },
  text1: {
    marginTop: normStyle.normalizeWidth(7),
  },
  view1: {
    width: '100%', flexDirection: 'row'
  },
  view2: {
    width: '100%', flexDirection: 'row',
    marginTop: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(20),
  },
  verifiedImg: {
    width: normStyle.normalizeWidth(10),
    height: normStyle.normalizeWidth(10),
  },
  editProfileStyle: {
    flex: 0.2, bottom: 0, right: 0, position: 'absolute',
    paddingRight: normStyle.normalizeWidth(20)
  },
  cameraStyle: {
    width: normStyle.normalizeWidth(25),
    height: normStyle.normalizeWidth(25),
  },
  edit1: {
    flex: 0.2, alignItems: 'flex-end', paddingRight: '2.5%',
  },
  button: {
    // width: normStyle.normalizeWidth(100),
    // height: normStyle.normalizeWidth(0),
    alignItems: 'center', justifyContent: 'center',
    marginHorizontal: 15,
    paddingBottom: normStyle.normalizeWidth(40),
    paddingTop: normStyle.normalizeWidth(40),
  }
});

export default ChangeCommunicationAddress;
