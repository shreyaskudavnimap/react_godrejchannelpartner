import React, { useEffect, useState } from 'react';
import {
    View,
    Image,
    Modal,
    StyleSheet,
    ScrollView,
    TouchableWithoutFeedback,
    Alert,
    ActivityIndicator,
    Dimensions
} from 'react-native';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppFooter from '../components/ui/AppFooter';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import colors from '../config/colors';
import AppButton from '../components/ui/AppButton';

import SplitPaymentComponent from '../components/SplitPaymentComponent';
import { useSelector } from 'react-redux';
import appFonts from '../config/appFonts';
import DropDownPicker from 'react-native-dropdown-picker';
import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';
import apiLoanDisbursement from '../api/apiLoanDisbursement';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { WebView } from 'react-native-webview';
import { useBackHandler } from '@react-native-community/hooks';
import GetLocation from 'react-native-get-location';
import appUserAuthorization from '../utility/appUserAuthorization';
import BookingTermConditionModalScreen from './modals/BookingTermConditionModalScreen';
import Orientation from 'react-native-orientation';
import AutoSearchInputModalScreen from './modals/AutoSearchInputModalScreen';
// import SelectDropdownModalScreen from './modals/SelectDropdownModalScreen';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const PayNowScreen = ({ navigation, route }) => {
    let controller;

    const deviceUniqueId = useSelector(
        (state) => state.deviceInfo.deviceUniqueId,
    );
    const loginvalue = useSelector(
        (state) => state.loginInfo.loginResponse,
    );
    // console.log("loginvalue", loginvalue);
    var userId = typeof loginvalue.data !== 'undefined' ? loginvalue.data.userid : '';
    const sessionId = typeof loginvalue.sessionId !== 'undefined' ? loginvalue.sessionId : '';
    const dashboardData = useSelector((state) => state.dashboardData);
    const propertyData = typeof dashboardData.rmData !== 'undefined' ? dashboardData.rmData : null;
    // console.log("propertyData", propertyData);
    const propertyListData = []; // need to declare here
    for (let item of propertyData) {
        propertyListData.push({ label: item.property_name + '/' + item.inv_flat_code, value: item.booking_id });
    }
    if (propertyListData.length > 1) {
        propertyListData.push({ label: "All Properties(" + propertyListData.length + ")", value: "NA" });
    }

    //   const propertyPickerData = propertyListData;

    const routeParams = route.params;
    let booking_id = routeParams && routeParams.bookingId ? routeParams.bookingId : "";
    // console.log("booking_id",booking_id);
    let propertyTitle = "";
    const propertyIndex = propertyListData.findIndex(
        (x) => x.value == booking_id
    );
    if (propertyIndex == -1) {
        booking_id = "";
    }
    if (!booking_id) {
        if (propertyListData.length > 0) {
            booking_id = propertyListData[0].value;
            propertyTitle = propertyListData[0].label;
        }
    } else {
        const propertyIndex = propertyListData.findIndex(
            (x) => x.value == booking_id
        );
        if (propertyIndex > -1) {
            propertyTitle = propertyListData[propertyIndex].label;
        }
    }
    // console.log("booking_id2",booking_id);
    // console.log("propertyListData", propertyListData);
    const [selectedProperty, setSelectedProperty] = useState(propertyTitle);
    const [bookingId, setBookingId] = useState(booking_id);
    const [paymentId, setPaymentId] = useState("");
    const [totalPayableAmount, setTotalPayableAmount] = useState("0.00");
    const [totalPayment, setTotalPayment] = useState("0.00");
    const [bookingItemsAccList, setBookingItemsAccList] = useState({});

    const [isSelected, setIsSelected] = useState(false);
    const [selectAllLabel, setSelectAllLabel] = useState('Select All');
    const [isLoading, setIsLoading] = useState(false);
    const [termsText, setTermsText] = useState('');

    const [showWebview, setShowWebview] = useState(false);
    const [paymentUrl, setPaymentUrl] = useState('');
    const [showPropertyDropdown, isShowPropertyDropdown] = useState(false);
    const [termsModalVisible, setTermsModalVisible] = useState(false);
    const [dynamicBottomPadding, setDynamicBottomPadding] = useState(0);
    const [childRender, isChildRender] = useState(false);

    // console.log("bookingId11111",bookingId);

    useEffect(() => {
        Orientation.lockToPortrait();
    }, []);

    useEffect(() => {
        if (bookingId != "NA") {
            getSingleAccountSummary();
        } else {
            setIsSelected(false)
            setSelectAllLabel("Select All");
            getAccountSummary();
        }
        // console.log("bookingId",bookingId);
    }, [bookingId]);

    const getAccountSummary = async () => {
        setIsLoading(true);
        try {
            const requestData = { user_id: userId, split_payment: "1" };
            const response = await apiLoanDisbursement.accountSummary(requestData);
            // console.log(response.data);
            if (response.data.status == 200) {
                let returnedData = response.data.data || {};
                setTotalPayableAmount(returnedData.total_pay);
                if (
                    returnedData.booking_items &&
                    returnedData.booking_items.length > 0
                ) {
                    const allBooking = returnedData.booking_items;
                    // console.log("allBooking1", allBooking);                    
                    let bookingItemAccList = [];
                    allBooking.map((bookingItem, index1) => {
                        let bookingItems = [];
                        let bookingItemArr = [];
                        var bKey = bookingItem.project_sfid;
                        if (!bookingItemAccList.hasOwnProperty(bKey)) {
                            bookingItemAccList[bKey] = [];
                        }
                        bookingItemArr['project_name'] = bookingItem.project_code;
                        bookingItemArr['inventory_name'] = bookingItem.inventory_code;
                        bookingItemArr['booking_id'] = bookingItem.booking_id;
                        let accItemArr = [];
                        if (bookingItem.bank_details.length > 0) {
                            for (var i = 0; i < bookingItem.bank_details.length; i++) {
                                var key = bookingItem.bank_details[i].field_ac_number;

                                if (!accItemArr.hasOwnProperty(key)) {
                                    accItemArr[key] = [];
                                }
                                bookingItem.bank_details[i]["is_checked"] = false;
                                bookingItem.bank_details[i]["field_amount"] = "0.00";
                                accItemArr[key].push(bookingItem.bank_details[i]);
                            }
                        }
                        var accItemObj = [];
                        Object.keys(accItemArr).forEach(function (key) {
                            var duplicateIndex = {};
                            var accItemArrFinal = [];
                            let accObj = accItemArr[key];
                            for (var i = 0; i < accObj.length; i++) {
                                var item = accObj[i];
                                var collisionIndex = duplicateIndex[item['otherChargeGroup']];
                                if (collisionIndex > -1) {
                                    accItemArrFinal[collisionIndex]["field_amount"] = parseFloat(accItemArrFinal[collisionIndex]["field_amount"]) + parseFloat(item["field_amount"]);
                                } else {
                                    accItemArrFinal.push(item);
                                    duplicateIndex[item['otherChargeGroup']] = accItemArrFinal.length - 1;
                                }
                            }
                            accItemObj[key] = accItemArrFinal;
                        });

                        bookingItemArr['bank_list'] = accItemObj;
                        // bookingItemAccList[index1] = bookingItemArr;
                        bookingItemAccList[bKey].push(bookingItemArr)
                    });
                    // console.log("allBooking1", bookingItemAccList);
                    setBookingItemsAccList(bookingItemAccList);
                    setTotalPayment("0.00");
                }
                setTermsText(response.data.disclaimer_text);
            }
            setIsLoading(false);
        } catch (error) {
            console.log(error);
            setIsLoading(false);
            appSnakBar.onShowSnakBar(
                appConstant.appMessage.APP_GENERIC_ERROR,
                'LONG',
            );
        }
    }

    const getSingleAccountSummary = async () => {
        setIsLoading(true);
        try {
            const requestData = { user_id: userId, booking_id: bookingId, split_payment: "1" };
            const response = await apiLoanDisbursement.singleAccountSummary(requestData);
            // console.log(response.data);
            if (response.data.status == 200) {
                let returnedData = response.data.data || {};
                setTotalPayableAmount(returnedData.total_pay);
                if (
                    returnedData.booking_items &&
                    returnedData.booking_items.length > 0
                ) {
                    const allBooking = returnedData.booking_items;
                    const filterData = allBooking.filter(
                        (x) => x.booking_id == bookingId
                    );
                    // console.log("filterData", filterData);
                    let bookingItemAccList = [];
                    filterData.map((bookingItem, index1) => {
                        let bookingItems = [];
                        let bookingItemArr = [];
                        var bKey = bookingItem.project_sfid;
                        if (!bookingItemAccList.hasOwnProperty(bKey)) {
                            bookingItemAccList[bKey] = [];
                        }
                        bookingItemArr['project_name'] = bookingItem.project_code;
                        bookingItemArr['inventory_name'] = bookingItem.inventory_code;
                        bookingItemArr['booking_id'] = bookingItem.booking_id;
                        let accItemArr = [];
                        if (bookingItem.bank_details.length > 0) {
                            for (var i = 0; i < bookingItem.bank_details.length; i++) {
                                var key = bookingItem.bank_details[i].field_ac_number;

                                if (!accItemArr.hasOwnProperty(key)) {
                                    accItemArr[key] = [];
                                }
                                bookingItem.bank_details[i]["is_checked"] = true;
                                let fieldAmount = bookingItem.bank_details[i].due_amount > 0 ? bookingItem.bank_details[i].due_amount.toFixed(2) : "0.00";
                                bookingItem.bank_details[i]["field_amount"] = fieldAmount;
                                accItemArr[key].push(bookingItem.bank_details[i]);
                            }
                        }
                        var accItemObj = [];
                        Object.keys(accItemArr).forEach(function (key) {
                            var duplicateIndex = {};
                            var accItemArrFinal = [];
                            let accObj = accItemArr[key];
                            for (var i = 0; i < accObj.length; i++) {
                                var item = accObj[i];
                                var collisionIndex = duplicateIndex[item['otherChargeGroup']];
                                if (collisionIndex > -1) {
                                    accItemArrFinal[collisionIndex]["field_amount"] = parseFloat(accItemArrFinal[collisionIndex]["field_amount"]) + parseFloat(item["field_amount"]);
                                } else {
                                    accItemArrFinal.push(item);
                                    duplicateIndex[item['otherChargeGroup']] = accItemArrFinal.length - 1;
                                }
                            }
                            accItemObj[key] = accItemArrFinal;
                        });

                        bookingItemArr['bank_list'] = accItemObj;
                        // bookingItemAccList[index1] = bookingItemArr;
                        bookingItemAccList[bKey].push(bookingItemArr)
                    });
                    // console.log("SingleBooking", bookingItemAccList);
                    let total_payment = 0;
                    Object.keys(bookingItemAccList).forEach(function (bKey) {
                        bookingItemAccList[bKey].map((bookingItem, index1) => {
                            Object.keys(bookingItem.bank_list).forEach(function (key) {
                                let accObj = bookingItem.bank_list[key];
                                // console.log("accObj", accObj);
                                for (var i = 0; i < accObj.length; i++) {
                                    var item = accObj[i];
                                    let fieldAmount = (item.due_amount > 0) ? item.due_amount.toFixed(2) : "";
                                    if (fieldAmount != "" && fieldAmount > 0) {
                                        total_payment = total_payment + parseFloat(fieldAmount);
                                    }
                                }
                            });
                        });
                    });
                    setTotalPayment(total_payment.toFixed(2));
                    setBookingItemsAccList(bookingItemAccList);
                    setIsSelected(true);
                    setSelectAllLabel("Deselect All");
                }
                setTermsText(response.data.disclaimer_text);
            }
            setIsLoading(false);
        } catch (error) {
            console.log(error);
            setIsLoading(false);
            appSnakBar.onShowSnakBar(
                appConstant.appMessage.APP_GENERIC_ERROR,
                'LONG',
            );
        }
    }

    const selectAll = () => {

        let bookingItemAccList = [];
        let total_payment = 0;
        Object.keys(bookingItemsAccList).forEach(function (bKey) {
            let bookingItemArr = [];
            bookingItemsAccList[bKey].map((bookingItem, index1) => {

                bookingItemArr[index1] = bookingItem;
                console.log("bookingItem", bookingItem);
                // let accItemArr = [];
                var accItemObj = [];
                Object.keys(bookingItem.bank_list).forEach(function (key) {
                    var accItemArrFinal = [];
                    let accObj = bookingItem.bank_list[key];
                    // console.log("accObj", accObj);
                    for (var i = 0; i < accObj.length; i++) {
                        var item = accObj[i];
                        // console.log("item", item);
                        item["is_checked"] = !isSelected;
                        let fieldAmount = (item.due_amount > 0 && !isSelected) ? item.due_amount.toFixed(2) : "0.00";
                        item["field_amount"] = fieldAmount;
                        if (fieldAmount != "" && fieldAmount > 0) {
                            total_payment = total_payment + parseFloat(fieldAmount);
                        }
                        accItemArrFinal.push(item);
                    }
                    accItemObj[key] = accItemArrFinal;
                });
                bookingItemArr[index1]['bank_list'] = accItemObj;
            });
            bookingItemAccList[bKey] = bookingItemArr
        });
        // console.log("bookingItemAccListpppp", bookingItemAccList);
        setIsSelected(!isSelected)
        setSelectAllLabel(!isSelected ? "Deselect All" : "Select All");
        setBookingItemsAccList(bookingItemAccList);
        setTotalPayment(total_payment.toFixed(2));
    }

    const updateAmount = (project_id, index1, account_number, index2, amountVal) => {
        let amount = validateAmount(amountVal);
        try {
            let bookingAccList = bookingItemsAccList;//.slice(0);
            bookingAccList[project_id][index1]["bank_list"][account_number][index2].field_amount = amount;
            let total_payment = 0;
            Object.keys(bookingAccList).forEach(function (bKey) {
                bookingAccList[bKey].map((bookingItem, index1) => {
                    Object.keys(bookingItem.bank_list).forEach(function (key) {
                        let accObj = bookingItem.bank_list[key];
                        for (var i = 0; i < accObj.length; i++) {
                            var item = accObj[i];
                            let fieldAmount = (item.field_amount > 0) ? item.field_amount : "0.00";
                            if (fieldAmount != "" && fieldAmount > 0) {
                                total_payment = total_payment + parseFloat(fieldAmount);
                            }
                        }
                    });
                });
            });
            setBookingItemsAccList(bookingAccList);
            setTotalPayment(total_payment.toFixed(2));
        } catch (error) {
            console.log(error);
            appSnakBar.onShowSnakBar(
                appConstant.appMessage.APP_GENERIC_ERROR,
                'LONG',
            );
        }
        isChildRender(!childRender);
    }

    const validateAmount = (amountVal) => {
        let cleanAmount = amountVal
            .toString()
            .split('.')
            .map((el, i) => (i ? el.split('').slice(0, 2).join('') : el))
            .join('.')
            .replace(/^([^.]*\.)(.*)$/, function (a, b, c) {
                return b + c.replace(/\./g, '');
            });
        return cleanAmount;
    };



    const markInput = (project_id, index1, account_number, index2, is_checked) => {
        console.log(project_id, index1, account_number, index2, is_checked);
        try {
            let bookingAccList = bookingItemsAccList;//.slice(0);
            console.log("bookingAccList", bookingAccList)
            if (is_checked) {
                bookingAccList[project_id][index1]["bank_list"][account_number][index2].is_checked = false;
                bookingAccList[project_id][index1]["bank_list"][account_number][index2].field_amount = "0.00";
            } else {
                bookingAccList[project_id][index1]["bank_list"][account_number][index2].is_checked = true;
                let fieldAmount = bookingAccList[project_id][index1]["bank_list"][account_number][index2].due_amount > 0 ? bookingAccList[project_id][index1]["bank_list"][account_number][index2].due_amount.toFixed(2) : "0.00";
                bookingAccList[project_id][index1]["bank_list"][account_number][index2].field_amount = fieldAmount;
            }

            let paymentAmount = 0
            let selectAll = true;
            Object.keys(bookingAccList).forEach(function (bKey) {
                bookingAccList[bKey].map((bookingItem) => {
                    Object.keys(bookingItem.bank_list).forEach(function (key) {
                        let accObj = bookingItem.bank_list[key];
                        for (var i = 0; i < accObj.length; i++) {
                            var item = accObj[i];
                            if (item.field_amount > 0) {
                                paymentAmount = paymentAmount + parseFloat(item.field_amount);
                            }
                            selectAll = selectAll * item.is_checked;
                        }
                    });
                });
            });
            setBookingItemsAccList(bookingAccList);
            setIsSelected(selectAll)
            setSelectAllLabel(selectAll ? "Deselect All" : "Select All");
            setTotalPayment(paymentAmount.toFixed(2));
            isChildRender(!childRender);
        } catch (error) {
            console.log(error);
            appSnakBar.onShowSnakBar(
                appConstant.appMessage.APP_GENERIC_ERROR,
                'LONG',
            );
        }

    }

    const requestDisbursal = async () => {
        if (totalPayment > 0) {
            setIsLoading(true);
            try {
                const requestData = { user_id: userId, booking_id: bookingId };
                const checkBankDetail = await apiLoanDisbursement.checkBankDetails(requestData);
                const bankDetailStatus = checkBankDetail.data.status;
                setIsLoading(false);
                if (bankDetailStatus == 200) {
                    Alert.alert(
                        "Loan Disbursement",
                        "Please enter your loan details to enable us generate your Disbursement Request Letter",
                        [
                            {
                                text: "Enter Loan Details",
                                onPress: () => {
                                    navigation.navigate("LoanEnquiryScreen", {
                                        bookingId: bookingId,
                                        activeTab: "LoanDetails"
                                    });
                                },
                            }
                        ],
                        { cancelable: false },
                    );
                } else if (bankDetailStatus == 501) {
                    proceedToDisbursal();
                } else {
                    Alert.alert(
                        "Loan Disbursement",
                        checkBankDetail.data.msg,
                        [
                            {
                                text: "OK",
                                onPress: () => {
                                    navigation.navigate("LoanEnquiryScreen", {
                                        bookingId: bookingId,
                                        activeTab: "LoanDetails"
                                    });
                                },
                            }
                        ],
                        { cancelable: false },
                    );

                }

            } catch (error) {
                console.log(error);
                setIsLoading(false);
                appSnakBar.onShowSnakBar(
                    appConstant.appMessage.APP_GENERIC_ERROR,
                    'LONG',
                );
            }

        } else {
            Alert.alert(
                "Loan Disbursement",
                "Please enter any amount to process with Loan Disbursal",
                [
                    {
                        text: "OK",
                        onPress: () => {

                        },
                    }
                ],
                { cancelable: false },
            );
        }
    }

    const proceedToDisbursal = async () => {
        setIsLoading(true);
        try {
            const requestData = { user_id: userId, booking_id: bookingId };
            const checkBankDetail = await apiLoanDisbursement.getBankDetails(requestData);
            const bankDetails = checkBankDetail.data;
            setIsLoading(false);
            if (bankDetails.status == "200") {
                if (bankDetails.bank_details[0].bank_name.toLowerCase() != 'hdfc bank') {
                    appSnakBar.onShowSnakBar(
                        'This payment disbursement feature is currently not available for your bank',
                        'LONG',
                    );
                } else if (bankDetails.bank_details[0].sanction_letter_url == '' || bankDetails.bank_details[0].sanction_letter_url == null) {
                    appSnakBar.onShowSnakBar(
                        'Please upload the sanction letter before you proceed with the Loan Disbursal process',
                        'LONG',
                    );
                } else {
                    let splitPayment = [];

                    Object.keys(bookingItemsAccList).forEach(function (bKey) {

                        bookingItemsAccList[bKey].map((bookingItem) => {
                            Object.keys(bookingItem.bank_list).forEach(function (key) {
                                let accObj = bookingItem.bank_list[key];
                                for (var i = 0; i < accObj.length; i++) {
                                    var bankItem = accObj[i];
                                    if (bankItem.is_checked) {
                                        if (
                                            bankItem.field_amount &&
                                            Number(bankItem.field_amount) > 0
                                        ) {
                                            let bankData = {
                                                accountType: bankItem.accountType,
                                                bankName: bankItem.bankName,
                                                bank_name: bankItem.bank_name,
                                                banks: bankItem.banks,
                                                bussinessAssociate: bankItem.bussinessAssociate,
                                                field_ac_number: bankItem.field_ac_number,
                                                field_amount: Number(bankItem.field_amount),
                                                field_favoring: bankItem.field_favoring,
                                                ifsc_code: bankItem.ifsc_code,
                                                merchant_id: bankItem.merchant_id,
                                                projectPhase: bankItem.projectPhase,
                                                sub_merchant_id: bankItem.sub_merchant_id,
                                            };
                                            splitPayment.push(bankData);
                                        }
                                    }
                                }
                            });
                        });
                    });

                    navigation.navigate("LoanDisbursementForm", {
                        booking_id: bookingId,
                        payment_amount: totalPayment,
                        split_payment: splitPayment,
                    });
                }
            }

        } catch (error) {
            console.log(error);
            setIsLoading(false);
            appSnakBar.onShowSnakBar(
                appConstant.appMessage.APP_GENERIC_ERROR,
                'LONG',
            );
        }

    }

    const gotoPayment = async () => {
        if (totalPayment > 0) {
            let isProceed = true;
            let booking_id = '';
            Object.keys(bookingItemsAccList).forEach(function (bKey) {
                bookingItemsAccList[bKey].map((bookingItem) => {
                    console.log(bookingItem);
                    if (bookingId != "NA") {
                        booking_id = bookingId;
                    } else {
                        booking_id = bookingItem.booking_id;
                    }
                    bookingItem.bank_list.map((bankDetailsItem) => {
                        if (bankDetailsItem.is_checked &&
                            Number(bankDetailsItem.field_amount) == 0
                        ) {
                            isProceed = false;
                        }
                    });
                });
            });
            if (isProceed) {
                setIsLoading(true);
                try {
                    const requestData = { user_id: userId, booking_id: booking_id };
                    const response = await apiLoanDisbursement.checkApplicantInfo(requestData);
                    const resultData = response.data;
                    setIsLoading(false);
                    if (resultData.status == "200") {
                        setTermsModalVisible(true);
                    } else {
                        appSnakBar.onShowSnakBar(
                            resultData.msg ? resultData.msg : appConstant.appMessage.APP_GENERIC_ERROR,
                            'LONG',
                        );
                    }
                } catch (error) {
                    console.log(error);
                    setIsLoading(false);
                    appSnakBar.onShowSnakBar(
                        appConstant.appMessage.APP_GENERIC_ERROR,
                        'LONG',
                    );
                }
            } else {
                appSnakBar.onShowSnakBar(
                    "Please enter amount for all of the selected account",
                    'LONG',
                );
            }
        } else {
            appSnakBar.onShowSnakBar(
                "Please select & enter amount for atleast one of the listed account",
                'LONG',
            );
        }
    }

    const makePayment = async () => {
        let split_details = {};
        let milestone_id = 1;
        let payment_breakup = [];
        Object.keys(bookingItemsAccList).forEach(function (bKey) {
            bookingItemsAccList[bKey].map((bookingItem) => {
                Object.keys(bookingItem.bank_list).forEach(function (key) {
                    let accObj = bookingItem.bank_list[key];
                    for (var i = 0; i < accObj.length; i++) {
                        var bankDetailsItem = accObj[i];
                        if (bankDetailsItem.is_checked) {
                            if (
                                bankDetailsItem.field_amount &&
                                Number(bankDetailsItem.field_amount) > 0
                            ) {
                                split_details = {
                                    'milestone_id': milestone_id,
                                    'booking_id': bookingItem.booking_id,
                                    'payment_id': bankDetailsItem.payment_id,
                                    'submerchant_id': bankDetailsItem.sub_merchant_id,
                                    'amount': bankDetailsItem.field_amount,
                                    'bank_id': bankDetailsItem.bank_id,
                                    'business_associate': bankDetailsItem.bussinessAssociate,
                                    'split_branch_name': bankDetailsItem.branchName,
                                    'other_charge_group': bankDetailsItem.otherChargeGroup,
                                }
                                milestone_id = milestone_id + 1;
                                payment_breakup.push(split_details);
                            }
                        }
                    }
                });
            });
        });
        console.log("payment_breakup", payment_breakup);
        if (payment_breakup.length > 0) {

            setIsLoading(true);
            try {
                const requestData = {
                    user_id: userId,
                    total_payable_amount: totalPayment,
                    payment_breakup: payment_breakup
                };
                const response = await apiLoanDisbursement.addSplitPaymentDetails(requestData);
                const resultData = response.data;
                setIsLoading(false);
                if (resultData.status == "200") {
                    let paymentNid = resultData.data.payment_nid;

                    const param =
                        "?auth=" +
                        sessionId +
                        "&user_id=" + userId +
                        "&booking_id=" + paymentNid;
                    const url = appConstant.buildInstance.baseUrl + "api_postsale_payment_redirect_v2" + param;
                    console.log("CC Payment url : " + url);
                    setPaymentUrl(url);
                    setShowWebview(true);
                    setPaymentId(paymentNid);

                }

            } catch (error) {
                console.log(error);
                setIsLoading(false);
                appSnakBar.onShowSnakBar(
                    appConstant.appMessage.APP_GENERIC_ERROR,
                    'LONG',
                );
            }

        }
    }

    const onCloseWebView = () => {
        setPaymentUrl('');
        setShowWebview(false);
    };

    const ActivityIndicatorElement = () => {
        return (
            <ActivityIndicator
                color={colors.jaguar}
                size="small"
                style={styles.activityIndicatorStyle}
            />
        );
    };

    const handleWebViewNavigationStateChange = ({ url }) => {
        console.log('handleWebViewNavigationStateChange newNavState : ', url);
        if (
            url ==
            appConstant.buildInstance.baseUrl + 'api_eoi_payment_success_final.json'
        ) {
            onCloseWebView();
            getBookingStatus(paymentId, true);
        } else if (
            url ==
            appConstant.buildInstance.baseUrl + 'api_eoi_payment_failed.json'
        ) {
            onCloseWebView();
            getBookingStatus(paymentId, false);
        }
    };

    useBackHandler(() => {
        if (showWebview) {
            setPaymentUrl('');
            setShowWebview(false);
            return true;
        }
        return false;
    });

    const onValueChangeProperty = (value) => {
        setBookingId(value);
    }

    const getBookingStatus = async (_bookingId, isSuccess) => {
        try {
            setIsLoading(true);
            const requestData = { booking_id: _bookingId };
            const response = await apiLoanDisbursement.getPaymentStatus(requestData);
            const resultData = response.data;
            setIsLoading(false);
            if (
                resultData.status &&
                resultData.status == '200' &&
                resultData.payment_status &&
                resultData.payment_status == 'Completed'
            ) {
                if (isSuccess) {
                    showAlert(resultData.msg ? resultData.msg : 'Payment Successful');
                } else {
                    showAlert(resultData.msg ? resultData.msg : 'Payment Failed');
                }
            } else {
                showAlert('Payment Failed');
            }
        } catch (error) {
            console.log(error);
            setIsLoading(false);
            showAlert(appConstant.appMessage.APP_GENERIC_ERROR);
        }
    };

    const showAlert = (msg) => {
        Alert.alert(
            "",
            msg,
            [
                {
                    text: "OK",
                    onPress: () => {

                    },
                }
            ],
            { cancelable: false },
        );
    }

    // const onSelectProperty = (property) => {
    //     setBookingId(property.value);
    //     setSelectedProperty(property.title);
    //     isShowPropertyDropdown(false);
    // };

    const postAuthorization = (moduleName) => {
        const storedUserData = loginvalue.data;
        const requestData = {
            deviceId: deviceUniqueId,
            devicePlatform: Platform.OS,
            userId: userId,
            customerName:
                storedUserData && storedUserData.first_name && storedUserData.last_name
                    ? `${storedUserData.first_name} ${storedUserData.last_name}`
                    : '',
            mobileNo:
                storedUserData && storedUserData.mob_no ? storedUserData.mob_no : '',
            emailId:
                storedUserData && storedUserData.email ? storedUserData.email : '',
            moduleName: moduleName,
        };
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        })
            .then(({ latitude, longitude }) => {
                appUserAuthorization.validateUserAuthorization({
                    ...requestData,
                    latiTude: latitude,
                    longiTude: longitude,
                });
            })
            .catch((error) => {
                appUserAuthorization.validateUserAuthorization(requestData);
            });
    };

    const onHandleTermsSelection = (isSelected) => {
        setTermsModalVisible(false);
        if (isSelected) {
            postAuthorization("Pay Now");
            makePayment();
        }
    };

    const onHandleRetryBtn = () => {
        if (bookingId != "NA") {
            getSingleAccountSummary();
        } else {
            setIsSelected(false)
            setSelectAllLabel("Select All");
            getAccountSummary();
        }
    };

    return (
        <Screen onRetry={onHandleRetryBtn}>
            {showWebview && paymentUrl ? (
                <WebView
                    originWhitelist={['*']}
                    source={{
                        uri: paymentUrl,
                    }}
                    style={{ flex: 1 }}
                    scalesPageToFit
                    javaScriptEnabledAndroid={true}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    mediaPlaybackRequiresUserAction={true}
                    startInLoadingState={true}
                    renderLoading={ActivityIndicatorElement}
                    onNavigationStateChange={handleWebViewNavigationStateChange}
                />
            ) : null}
            {!showWebview && <AppHeader />}
            {!showWebview && (
                <>
                    {!isLoading ? (
                        <>
                            <ScrollView>
                                <View style={styles.container}
                                    onStartShouldSetResponder={() => {
                                        if (propertyListData.length > 1) {
                                            controller.close();
                                        }

                                    }}>
                                    <View style={{ paddingTop: 10 }, styles.dropDownContainer}>
                                        <AppText style={{ color: colors.gray3 }}>Payment</AppText>
                                        {/* <TouchableWithoutFeedback
                                            onPress={() => {
                                                if (propertyListData.length > 1) {
                                                    isShowPropertyDropdown(true);
                                                }
                                            }}>
                                            <View style={styles.itemContainer}>
                                                <AppText style={styles.propertyText} numberOfLines={1}>
                                                    {selectedProperty}
                                                </AppText>
                                                {propertyListData.length > 1 && (
                                                    <Image
                                                        style={styles.arrow}
                                                        source={require('./../assets/images/down-icon-b.png')}
                                                    />
                                                )}
                                            </View>
                                        </TouchableWithoutFeedback> */}
                                        {propertyListData.length > 1 ? (
                                            <DropDownPicker
                                                items={propertyListData}
                                                defaultValue={bookingId}
                                                autoScrollToDefaultValue={true}
                                                style={styles.dropdownDocument}
                                                placeholderStyle={styles.dropdownPlaceholder}
                                                itemStyle={styles.dropDownItem}
                                                labelStyle={styles.label}
                                                activeLabelStyle={styles.labelSelected}
                                                selectedLabelStyle={styles.labelSelected}
                                                onChangeItem={(item) => onValueChangeProperty(item.value)}
                                                placeholder="Select Property"
                                                controller={instance => controller = instance}
                                                onOpen={() => {
                                                    setDynamicBottomPadding(130);
                                                }}
                                                onClose={() => {
                                                    setDynamicBottomPadding(0);
                                                }}
                                            />
                                        ) : (
                                                propertyListData.length > 0 ? (
                                                    <AppTextBold style={styles.dropdownText}>{typeof propertyListData[0] !== 'undefined' ? propertyListData[0].label : ''}</AppTextBold>
                                                ) : null
                                            )}
                                        <View style={{ paddingBottom: dynamicBottomPadding }}></View>
                                    </View>
                                    <View style={styles.payableContainer}>
                                        <AppText style={styles.totalPayLabelTop}
                                            textBreakStrategy="simple"
                                            ellipsizeMode="tail">Total Payable Amount</AppText>
                                        <AppText style={styles.amount}
                                            textBreakStrategy="simple"
                                            ellipsizeMode="tail">{'\u20B9'}{totalPayment}</AppText>
                                    </View>
                                    <View style={styles.checkboxContainer}>
                                        <TouchableWithoutFeedback onPress={() => { selectAll(); }}>
                                            <Image
                                                style={styles.checkbox}
                                                source={
                                                    isSelected == true
                                                        ? require('./../assets/images/checked-icon.png')
                                                        : require('./../assets/images/unchecked-gray-icon.png')
                                                }
                                            />
                                        </TouchableWithoutFeedback>
                                        <AppText style={styles.checkboxLabel}>{selectAllLabel}</AppText>
                                    </View>

                                    <SplitPaymentComponent bookingItemsAccList={bookingItemsAccList}
                                        totalPayment={totalPayment}
                                        updateAmount={updateAmount}
                                        markInput={markInput} />

                                    <AppButton title='PAY NOW' onPress={() => gotoPayment()} />
                                    <AppButton color='primary' textColor='secondary' disabled={bookingId == "NA" ? true : false}
                                        title='REQUEST FOR DISBURSAL' onPress={() => requestDisbursal()} />

                                </View>
                            </ScrollView>


                            <AppFooter activePage={0} isPostSales={true} />
                        </>
                    ) : null}
                </>
            )}
            {/* <Modal
                animationType="fade"
                transparent={true}
                visible={showPropertyDropdown}
                onRequestClose={() => {
                    isShowPropertyDropdown(false);
                }}>
                <SelectDropdownModalScreen
                    itemHeight={windowHeight / 3}
                    properties={propertyListData}
                    onClose={() => {
                        isShowPropertyDropdown(false);
                    }}
                    onSelect={(property) => {
                        onSelectProperty(property);
                    }}
                />
            </Modal> */}
            <Modal
                animationType="fade"
                transparent={true}
                visible={termsModalVisible}
                onRequestClose={() => {
                    setTermsModalVisible(false);
                }}>
                <BookingTermConditionModalScreen
                    onCancel={() => {
                        setTermsModalVisible(false);
                    }}
                    onContinue={() => {
                        onHandleTermsSelection(true);
                    }}
                    bookingTermsData={termsText}
                />
            </Modal>
            <AppOverlayLoader isLoading={isLoading} />
        </Screen>
    );
};

const styles = StyleSheet.create({
    itemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 2,
        borderBottomColor: colors.gray4,
        paddingVertical: 20,
        paddingRight: 20
    },
    arrow: {
        height: 16,
        width: 16,
    },
    container: {
        flex: 1,
        paddingHorizontal: '6%',
        justifyContent: 'center'
    },
    dropdownProperty: {
        borderBottomWidth: 2,
        borderBottomColor: colors.gray4,
        width: "100%",
        height: '100%',
        borderWidth: 0,
        marginBottom: 0,
    },
    propertyText: {
        width: '100%',
        fontSize: appFonts.largeBold,
        fontWeight: 'bold',
        color: colors.secondary
    },
    payableContainer: {
        flex: 1,
        width: '100%',//windowWidth * 0.8,
        paddingVertical: 20,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    amount: {
        width: '40%',
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: appFonts.xlargeFontSize,
        fontWeight: 'bold',
    },
    checkboxContainer: {
        flexDirection: "row",
        paddingBottom: '5%',
        alignItems: 'center',
    },
    checkboxLabel: {
        color: colors.gray3,
        margin: 6,
    },
    checkbox: { height: 22, width: 22 },
    dropdownDocument: {
        height: 50,
        marginTop: 15,
        marginBottom: 8,
        borderRightWidth: 0,
        borderLeftWidth: 0,
        borderTopWidth: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    dropdownPlaceholder: {
        fontSize: appFonts.largeFontSize,
        fontFamily: appFonts.SourceSansProRegular,
    },
    labelSelected: {
        fontSize: appFonts.largeFontSize,
        fontFamily: appFonts.SourceSansProSemiBold,
        marginBottom: 8,
    },
    label: {
        fontSize: appFonts.largeFontSize,
        fontFamily: appFonts.SourceSansProRegular,
        marginBottom: 8,
        textAlign: 'left',
    },
    dropDownItem: {
        borderBottomWidth: 1,
        borderBottomColor: colors.lightGray,
        marginBottom: 8,
        marginTop: 4,
        justifyContent: 'flex-start',
    },
    activityIndicatorStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    dropDownContainer: {
        ...Platform.select({ ios: { zIndex: 1000 } })
    },
    totalPayLabelTop: {
        color: colors.gray3,
        width: '55%',
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center',
    },

});

export default PayNowScreen;
