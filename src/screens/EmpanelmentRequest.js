import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
  TextInput
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import AppButton from '../components/ui/AppButton';
import AppHeader from '../components/ui/AppHeader';
import AppTextBold from '../components/ui/AppTextBold';
import AppText from '../components/ui/ AppText';
import Screen from '../components/Screen';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppFooter from '../components/ui/AppFooter';
import * as normStyle from '../styles/StyleSize';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import apiLogin from './../api/apiLogin';
import ImagePicker from 'react-native-image-picker';
import { useIsFocused } from '@react-navigation/native';
import RNFetchBlob from 'rn-fetch-blob';
import AsyncStorage from '@react-native-async-storage/async-storage';
import appConstant from './../utility/appConstant.js';
import Orientation from 'react-native-orientation';
import DropDownPicker from 'react-native-dropdown-picker';
import * as userLoginAction from './../store/actions/userLoginAction';
import appSnakBar from '../utility/appSnakBar';
import CheckBox from '@react-native-community/checkbox';

const { height } = Dimensions.get('window');

const form = {
  salutations: 'Mrs',
  FirstName: 'Arshi',
  LastName: 'Arbab',
  MobileNumber: '999999999',
  EmailID: 'arbabarshi@gmail.com',
  Password: '************',
};

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const EmpanelmentRequest = (props) => {
  
  const [dob, SetDob] = useState('');
  
  const [verifyMobile, SetVerifyMobile] = useState(0);
  
  const [verifyEmail, SetVerifyEmail] = useState(0);
  const [permanentAddStr1, SetpermanentAddStr1] = useState('');
  const [permanentAddStr2, SetpermanentAddStr2] = useState('');
  const [permanentAddStr3, SetpermanentAddStr3] = useState('');
  const [permanentCity, SetpermanentCity] = useState('');
  const [permanentState, SetpermanentState] = useState('');
  const [permanentCountry, SetpermanentCountry] = useState('');
  const [permanentPincode, SetpermanentPincode] = useState('');
  const [presentAddStr1, SetpresentAddStr1] = useState('');
  const [presentAddStr2, SetpresentAddStr2] = useState('');
  const [presentAddStr3, SetpresentAddStr3] = useState('');
  const [presentCity, SetpresentCity] = useState('');
  const [presentState, SetpresentState] = useState('');
  const [presentCountry, SetpresentCountry] = useState('');
  const [presentPincode, SetpresentPincode] = useState('');
  const [userImage, SetUserImage] = useState('');
  const [userImageName, SetUserImageName] = useState('');
  const [userId, SetUserId] = useState('');
  const [isApiLoading, setApiIsLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [profileBase64, SetprofileBase64] = useState('');
  const [isCustomer, SetIsCustomer] = useState('');
  const isVisible = useIsFocused();
  const [isSameAddress, SetIsSameAddress] = useState(false)

  const dispatch = useDispatch();

  const [firstName, SetFirstName] = useState('');
  const [lastName, SetLastName] = useState('');
  const [mobile, SetMobile] = useState('');
  const [email, SetEmail] = useState('');

  const [companyName, SetCompanyName] = useState('');
  const [entity, SetEntityType] = useState('');
  const [pan, SetPan] = useState('');

  const [billingCountry, SetBillingCountry] = useState("");
  const [registeredCountry, SetRegisteredCountry] = useState("");
  const [communicationCountry, SetCommunicationCountry] = useState("");

  const [billingState, SetBillingState] = useState("");
  const [registeredState, SetRegisteredState] = useState("");
  const [communicationState, SetCommunicationState] = useState("");

  const [billingCity, SetBillingCity] = useState("");
  const [registeredCity, SetRegisteredCity] = useState("");
  const [communicationCity, SetCommunicationCity] = useState("");

  const [billingStreet, SetBillingStreet] = useState("");
  const [registeredStreet, SetRegisteredStreet] = useState("");
  const [communicationStreet, SetCommunicationStreet] = useState("");

  const [billingZipCode, SetBillingZipCode] = useState("");
  const [registeredZipCode, SetRegisteredZipCode] = useState("");
  const [communicationZipCode, SetCommunicationZipCode] = useState("");

  var dropDownBillingCountries = []
  var dropDownBillingStates = [];
  var [dropDownBillingCities, SetDropDownBillingCities] = useState([]);

  var dropDownRegisteredCountries = []
  var dropDownRegisteredStates = [];
  var [dropDownRegisteredCities, SetDropDownRegisteredCities] = useState([]);

  var dropDownCommunicationCountries = []
  var dropDownCommunicationStates = [];
  var [dropDownCommunicationCities, SetDropDownCommunicationCities] = useState([]);

  var countryList = [];
  var stateList = [];
  var cityList = [];

  const FillCountryDropDown = async () => {
    const countryUrl = 'http://cp.godrejproperties.com/gplservice/CPService.svc/GetCountry';
    countryList = await apiLogin.apiGetRequest(countryUrl);
    //console.log("countryList", countryList.GetCountryjsonResult);
    var countries = countryList.GetCountryjsonResult;
    for (var i = 0; i < countries.length; i++) {
      dropDownBillingCountries.push({ label: countries[i].CountryName, value: countries[i].CountryName });
      dropDownRegisteredCountries.push({ label: countries[i].CountryName, value: countries[i].CountryName });
      dropDownCommunicationCountries.push({ label: countries[i].CountryName, value: countries[i].CountryName });
    }
    // console.log("dropDownCountries", dropDownCountries);
  }

  const FillStateDropDown = async () => {
    const stateUrl = 'http://cp.godrejproperties.com/gplservice/CPService.svc/GetStateData/1';
    stateList = await apiLogin.apiGetRequest(stateUrl);
    //console.log("stateList", stateList.GetStateDatajsonResult);
    var states = stateList.GetStateDatajsonResult;
    for (var i = 0; i < states.length; i++) {
      dropDownBillingStates.push({ label: states[i].StateName.replace("\n", ""), value: states[i].StateName.replace("\n", "") });
      dropDownRegisteredStates.push({ label: states[i].StateName.replace("\n", ""), value: states[i].StateName.replace("\n", "") });
      dropDownCommunicationStates.push({ label: states[i].StateName.replace("\n", ""), value: states[i].StateName.replace("\n", "") });
    }
    //console.log("dropDownStates", dropDownStates);
  }

  const FillCityDropDown = async (selectedState) => {
    setIsLoading(true);
    dropDownBillingCities = [];
    dropDownRegisteredCities = [];
    dropDownCommunicationCities = [];
    var selectedStateId = Object.values(stateList)[0].filter(x => x.StateName == selectedState.value)[0].StateId;
    //console.log("selectedStateId", selectedStateId);
    const cityUrl = 'http://cp.godrejproperties.com/gplservice/CPService.svc/GetCity/' + selectedStateId;
    cityList = await apiLogin.apiGetRequest(cityUrl);
    var cities = cityList.GetCityjsonResult;
    for (var i = 0; i < cities.length; i++) {
      dropDownBillingCities.push({ label: cities[i].CityName, value: cities[i].CityName });
      dropDownRegisteredCities.push({ label: cities[i].CityName, value: cities[i].CityName });
      dropDownCommunicationCities.push({ label: cities[i].CityName, value: cities[i].CityName });
    }
    SetDropDownBillingCities(dropDownBillingCities);
    SetDropDownCommunicationCities(dropDownCommunicationCities);
    SetDropDownRegisteredCities(dropDownRegisteredCities);
    //console.log("dropDownCities", dropDownCities);
    setIsLoading(false);
  }

  FillCountryDropDown();
  FillStateDropDown();

  const onChangeIsSameAddress = (isChecked) => {
    SetIsSameAddress(isChecked);
    console.log("Selected same address", isChecked);
  }

  const Submit = () => {
    console.log(companyName);
    console.log(entity);
    console.log(firstName);
    console.log(lastName);
    console.log(mobile);
    console.log(email);
    console.log(pan);
    console.log(billingStreet);
    console.log(billingCountry);
    console.log(billingState);
    console.log(billingCity);
    console.log(billingZipCode);
    console.log(registeredStreet);
    console.log(registeredCountry);
    console.log(registeredState);
    console.log(registeredCity);
    console.log(registeredZipCode);
    console.log(communicationStreet);
    console.log(communicationCountry);
    console.log(communicationState);
    console.log(communicationCity);
    console.log(communicationZipCode);

  }

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
    if (isVisible) {
      _getUserInfo(false);
    }
  }, [loginvalue, isVisible]);

  const loginvalue = useSelector((state) => state.loginInfo.loginResponse);
  // console.log('loginvalue: ', loginvalue)
  const menuType = useSelector((state) => state.menuType.menuType);

  const _getUserInfo = async (isDispatch) => {
    //alert(appConstant.buildInstance.baseUrl+'update_user_api.json');
    if (loginvalue.isLogin) {
      var temp = loginvalue.data.is_customer;
      SetIsCustomer(temp);
    }
    var userid = '';
    if (loginvalue.isLogin) {
      userid = loginvalue.data.userid;
    }

    const request = {
      user_id: userid,
    };
    setIsLoading(true);
    const result = await apiLogin.apiForGetUserData(request);
    setIsLoading(false);
    console.log(result.data);
    var status = result.data.status;
    if (status == 200) {
      if (
        result.data.data[0].first_name != '' &&
        result.data.data[0].first_name != null &&
        result.data.data[0].last_name != '' &&
        result.data.data[0].last_name != null
      ) {
        _getImageName(
          result.data.data[0].first_name,
          result.data.data[0].last_name,
        );
      }
      if (
        result.data.data[0].first_name != '' &&
        result.data.data[0].first_name != null
      ) {
        SetFirstName(result.data.data[0].first_name);
      }
      if (
        result.data.data[0].last_name != '' &&
        result.data.data[0].last_name != null
      ) {
        SetlastName(result.data.data[0].last_name);
      }
      if (
        result.data.data[0].birth_dt != '' &&
        result.data.data[0].birth_dt != null
      ) {
        _convertBirth(result.data.data[0].birth_dt); //_convertBirth('1981-02-12');
      }
      if (
        result.data.data[0].mobile != '' &&
        result.data.data[0].mobile != null
      ) {
        SetMobile(result.data.data[0].mobile);
      }
      if (
        result.data.data[0].is_verified_mobile != '' &&
        result.data.data[0].is_verified_mobile != null
      ) {
        SetVerifyMobile(result.data.data[0].is_verified_mobile);
      }
      if (
        result.data.data[0].email != '' &&
        result.data.data[0].email != null
      ) {
        SetEmail(result.data.data[0].email);
      }
      if (
        result.data.data[0].is_verified_email != '' &&
        result.data.data[0].is_verified_email != null
      ) {
        SetVerifyEmail(result.data.data[0].is_verified_email);
      }
      if (
        result.data.data[0].field_permanent_address_street_1 != '' &&
        result.data.data[0].field_permanent_address_street_1 != null
      ) {
        SetpermanentAddStr1(
          result.data.data[0].field_permanent_address_street_1,
        );
      }
      if (
        result.data.data[0].field_permanent_address_street_2 != '' &&
        result.data.data[0].field_permanent_address_street_2 != null
      ) {
        SetpermanentAddStr2(
          result.data.data[0].field_permanent_address_street_2,
        );
      }
      if (
        result.data.data[0].field_permanent_address_street_3 != '' &&
        result.data.data[0].field_permanent_address_street_3 != null
      ) {
        SetpermanentAddStr3(
          result.data.data[0].field_permanent_address_street_3,
        );
      }
      if (
        result.data.data[0].field_permanent_city != '' &&
        result.data.data[0].field_permanent_city != null
      ) {
        SetpermanentCity(result.data.data[0].field_permanent_city);
      }
      if (
        result.data.data[0].field_permanent_state != '' &&
        result.data.data[0].field_permanent_state != null
      ) {
        SetpermanentState(result.data.data[0].field_permanent_state);
      }
      if (
        result.data.data[0].field_permanent_country != '' &&
        result.data.data[0].field_permanent_country != null
      ) {
        SetpermanentCountry(result.data.data[0].field_permanent_country);
      }
      if (
        result.data.data[0].field_permanent_pin_code != '' &&
        result.data.data[0].field_permanent_pin_code != null
      ) {
        SetpermanentPincode(result.data.data[0].field_permanent_pin_code);
      }
      if (
        result.data.data[0].field_present_address_street_1 != '' &&
        result.data.data[0].field_present_address_street_1 != null
      ) {
        SetpresentAddStr1(result.data.data[0].field_present_address_street_1);
      }
      if (
        result.data.data[0].field_present_address_street_2 != '' &&
        result.data.data[0].field_present_address_street_2 != null
      ) {
        SetpresentAddStr2(result.data.data[0].field_present_address_street_2);
      }
      if (
        result.data.data[0].field_present_address_street_3 != '' &&
        result.data.data[0].field_present_address_street_3 != null
      ) {
        SetpresentAddStr3(result.data.data[0].field_present_address_street_3);
      }
      if (
        result.data.data[0].field_present_city != '' &&
        result.data.data[0].field_present_city != null
      ) {
        SetpresentCity(result.data.data[0].field_present_city);
      }
      if (
        result.data.data[0].field_present_state != '' &&
        result.data.data[0].field_present_state != null
      ) {
        SetpresentState(result.data.data[0].field_present_state);
      }
      if (
        result.data.data[0].field_present_country != '' &&
        result.data.data[0].field_present_country != null
      ) {
        SetpresentCountry(result.data.data[0].field_present_country);
      }
      if (
        result.data.data[0].field_present_pin_code != '' &&
        result.data.data[0].field_present_pin_code != null
      ) {
        SetpresentPincode(result.data.data[0].field_present_pin_code);
      }
      if (
        result.data.data[0].user_image != '' &&
        result.data.data[0].user_image != null
      ) {
        SetUserImage(result.data.data[0].user_image);

        if (isDispatch) {
          dispatch(
            userLoginAction.updateUserImage(result.data.data[0].user_image),
          );
        }
      }
      if (
        result.data.data[0].user_id != '' &&
        result.data.data[0].user_id != null
      ) {
        SetUserId(result.data.data[0].user_id);
      }
    }
  };

  const _convertBirth = (dob) => {
    var date = new Date(dob);
    var dtArr = dob.split('-');
    SetDob(dtArr[2] + ' ' + monthNames[date.getMonth()] + ', ' + dtArr[0]);
  };

  const _getImageName = (fn, ln) => {
    SetUserImageName(
      fn.charAt(0).toUpperCase() + ' ' + ln.charAt(0).toUpperCase(),
    );
  };

  const _seprator = () => {
    return (
      <View style={styles.seprator}>
        <View style={styles.line}></View>
      </View>
    );
  };

  const chooseImage = () => {
    let options = {
      title: 'Select Image',
      // customButtons: [
      //   { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
      // ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        //console.log('User cancelled image picker');
      } else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
        //alert(response.customButton);
      } else {
        const source = { uri: response.uri };
        SetprofileBase64(response.data);
        //console.log(JSON.stringify(response));
        var ImgName = response.fileName;
        var imgPath = response.path;
        //console.log(response.fileSize);
        _setUserProfile(response.data, ImgName);
      }
    });
  };

  const _setUserProfile = async (imgData, ImgName) => {
    setIsLoading(true);
    var url = appConstant.buildInstance.baseUrl + 'update_user_api.json';
    const request = {
      fileName:
        loginvalue.data.userid + '_' + new Date().getTime() + '_' + ImgName,
    };
    //apiForUpdateUserProfile  apiLogin.apiForUpdateProfileData(request);

    const tokenValue = await AsyncStorage.getItem('token');
    const sessionName = await AsyncStorage.getItem('sessionName');
    const sessionValue = await AsyncStorage.getItem('sessionId');

    RNFetchBlob.fetch(
      'POST',
      url,
      {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        'X-CSRF-TOKEN': tokenValue,
        'x-requested-with': sessionName + '=' + sessionValue,
      },
      [
        { name: 'myDoc', filename: request.fileName, data: imgData },
        { name: 'user_id', data: userId },
      ],
    )
      .then((response) => {
        const obj = JSON.parse(response.data);
        const status = obj.status;

        if (status == 200) {
          setIsLoading(false);

          if (obj && obj.user_image) {
            dispatch(userLoginAction.updateUserImage(obj.user_image));

            SetUserImage(obj.user_image);

            appSnakBar.onShowSnakBar(
              obj && obj.msg
                ? obj.msg
                : 'Profile photo has been uploaded successfully',
              'LONG',
            );
          } else {
            _getUserInfo(true);
          }
        } else {
          setIsLoading(false);
          // appSnakBar.onShowSnakBar(response.data.msg, 'LONG');
          appSnakBar.onShowSnakBar(
            obj && obj.msg ? obj.msg : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      })
      .catch((error) => {
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
        console.log(JSON.stringify(error));
      });
  };

  const _edit = (type) => {
    if (type == 'name') {
      props.navigation.navigate('ChangeNameScreen', {
        fname: firstName,
        lname: lastName,
        userId: userId,
      });
    }
    if (type == 'dob') {
      props.navigation.navigate('ChangeDOBScreen', {
        dobValue: dob,
        userId: userId,
      });
    }
    if (type == 'password') {
      props.navigation.navigate('ChangesPasswordScreen', { userId: userId });
    }
    if (type == 'mobile_no') {
      props.navigation.navigate('ChangesMobileNoScreen', {
        userId: userId,
        mobileNo: mobile,
      });
    }
    if (type == 'email_id') {
      props.navigation.navigate('ChangeEmailIdScreen', {
        userId: userId,
        emailid: email,
        userName: firstName + ' ' + lastName,
      });
    }
    if (type == 'residential') {
      props.navigation.navigate('ChangeAddressScreen', {
        userId: userId,
        street1: permanentAddStr1,
        street2: permanentAddStr2,
        street3: permanentAddStr3,
        city: permanentCity,
        pincode: permanentPincode,
        state: permanentState,
        country: permanentCountry,
        addType: 'residential',
      });
    }
    if (type == 'mailing') {
      props.navigation.navigate('ChangeAddressScreen', {
        userId: userId,
        street1: presentAddStr1,
        street2: presentAddStr2,
        street3: presentAddStr3,
        city: presentCity,
        pincode: presentPincode,
        state: presentState,
        country: presentCountry,
        addType: 'mailing',
      });
    }
    if (type == 'image') {
      chooseImage();
    }
  };

  const _verifyMobile = (type) => {
    if (type == 'mobile') {
      if (mobile != '' && mobile != undefined) {
        props.navigation.navigate('OtpForEmailScreen', {
          userId: userId,
          mobileNo: mobile,
          actionType: 'verify_mobile',
        });
      }
    }
    if (type == 'email')
      if (email != '' && email != undefined) {
        props.navigation.navigate('OtpForEmailScreen', {
          userId: userId,
          mobileNo: email,
          actionType: 'verify_email',
        });
      }
  };

  const onHandleRetryBtn = () => {
    if (isVisible) {
      _getUserInfo(false);
    }
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />

      <ScrollView>
        <View style={styles.container}>
          <AppText style={styles.pageTitle}>REGISTER EMPANELMENT</AppText>
          {/* 
<View style={styles.viewStyle}>
  <View style={styles.imageView1}>
    <View
      style={{
        flex: 0.8,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      {userImage ? (
        <Image
          source={{ uri: userImage }}
          style={styles.profileImg}
        //resizeMode='center'
        />
      ) : (
          <View
            style={[
              styles.profileImg,
              { alignItems: 'center', justifyContent: 'center' },
            ]}>
            <AppText style={styles.imageText}>{userImageName}</AppText>
          </View>
        )}
    </View>
    <View style={styles.editProfileStyle}>
      <TouchableOpacity
        onPress={() => {
          _edit('image');
        }}>
        <Image
          source={require('./../assets/images/camera-l-icon.png')}
          style={styles.cameraStyle}
        />
      </TouchableOpacity>
    </View>
  </View>
</View> */}

          {_seprator()}

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <AppText style={styles.userViewLabel}>Company Name *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={companyName}
                onChangeText={e => {
                  SetCompanyName(e);
                }}
              />
            </View>
          </View>


          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <DropDownPicker
                items={[
                  { label: 'PARTNERSHIP', value: 'CN1' },
                  { label: 'INDIVIDUAL', value: 'EL1' },
                  { label: 'PROPRIETOR', value: 'RA1' },
                  { label: 'COMPANY', value: 'CA1' },
                  { label: 'LLP', value: 'RD1' },
                ]}
                style={styles.input}
                onChangeItem={(value) => {
                  //onRequestChange(value);
                }}
                placeholder={"Type of Entity"}
                value={entity}
                onChangeText={e => {
                  SetEntityType(e.value);
                }}
              />
            </View>
          </View>


          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <AppText style={styles.userViewLabel}>First Name *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={firstName}
                onChangeText={e => {
                  SetFirstName(e);
                }}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <AppText style={styles.userViewLabel}>Last Name *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={lastName}
                onChangeText={e => {
                  SetLastName(e);
                }}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <AppText style={styles.userViewLabel}>Mobile *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={mobile}
                onChangeText={e => {
                  SetMobile(e);
                }}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <AppText style={styles.userViewLabel}>Email *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={email}
                onChangeText={e => {
                  SetEmail(e);
                }}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <AppText style={styles.userViewLabel}>PAN *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={false}
                value={pan}
                onChangeText={e => {
                  SetPan(e);
                }}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <AppText style={styles.sectionTitle}>BILLING DETAILS</AppText>
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <AppText style={styles.userViewLabel}>Billing Street *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={billingStreet}
                onChangeText={e => {
                  SetBillingStreet(e);
                }}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <DropDownPicker
                items={dropDownBillingCountries}
                style={styles.input}
                value={billingCountry}
                onChangeItem={(value) => {
                  SetBillingCountry(value.value);
                }}
                placeholder={"Select Billing Country"}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <DropDownPicker
                items={dropDownBillingStates}
                style={styles.input}
                value={billingState}
                onChangeItem={(value) => {
                  SetBillingState(value.value);
                  FillCityDropDown(value);
                }}
                placeholder={"Select Billing State"}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <DropDownPicker
                items={dropDownBillingCities}
                style={styles.input}
                value={billingCity}
                onChangeItem={(value) => {
                  SetBillingCity(value.value);
                }}
                placeholder={"Select Billing City"}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <AppText style={styles.userViewLabel}>Billing ZipCode *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={billingZipCode}
                onChangeText={e => {
                  SetBillingZipCode(e);
                }}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <AppText style={styles.sectionTitle}>REGISTERED DETAILS</AppText>
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <AppText style={styles.userViewLabel}>Registered Street *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={registeredStreet}
                onChangeText={e => {
                  SetRegisteredStreet(e);
                }}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <DropDownPicker
                items={dropDownRegisteredCountries}
                style={styles.input}
                value={registeredCountry}
                onChangeItem={(value) => {
                  SetRegisteredCountry(value.value);
                }}
                placeholder={"Select Registered Country"}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <DropDownPicker
                items={dropDownRegisteredStates}
                style={styles.input}
                value={registeredState}
                onChangeItem={(value) => {
                  SetRegisteredState(value.value);
                  FillCityDropDown(value);
                }}
                placeholder={"Select Registered State"}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <DropDownPicker
                items={dropDownRegisteredCities}
                style={styles.input}
                value={registeredCity}
                onChangeItem={(value) => {
                  SetRegisteredCity(value.value);
                }}
                placeholder={"Select Registered City"}
              />
            </View>
          </View>

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <AppText style={styles.userViewLabel}>Registered ZipCode *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={registeredZipCode}
                onChangeText={e => {
                  SetRegisteredZipCode(e);
                }}
              />
            </View>
          </View>


          <View style={styles.view1}>
            <View
              style={{ flex: 0.15, alignItems: 'flex-end', marginBottom: 10 }}>
              <CheckBox
                value={isSameAddress}
                onValueChange={(newValue) => onChangeIsSameAddress(newValue)} 
                />
            </View>
            <View
              style={{ flex: 0.8, flexDirection: 'column' }}>
              <AppText style={styles.text1}>Communication address same as above</AppText>
            </View>
          </View>

          {!isSameAddress ? (
          <View>
            
            <View style={styles.formStyle}>
              <View style={styles.userViewContainer}>
                <AppText style={styles.sectionTitle}>COMMUNICATION DETAILS</AppText>
              </View>
            </View>

            <View style={styles.formStyle}>
              <View style={styles.userViewContainer}>
                <AppText style={styles.userViewLabel}>Communication Street *</AppText>
                <TextInput
                  numberOfLines={1}
                  style={styles.input}
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="default"
                  editable={true}
                  value={communicationStreet}
                  onChangeText={e => {
                    SetCommunicationStreet(e);
                  }}
                />
              </View>
            </View>

            <View style={styles.formStyle}>
              <View style={styles.userViewContainer}>
                <DropDownPicker
                  items={dropDownCommunicationCountries}
                  style={styles.input}
                  value={communicationCountry}
                  onChangeItem={(value) => {
                    SetCommunicationCountry(value.value);
                  }}
                  placeholder={"Select Communication Country"}
                />
              </View>
            </View>

            <View style={styles.formStyle}>
              <View style={styles.userViewContainer}>
                <DropDownPicker
                  items={dropDownCommunicationStates}
                  style={styles.input}
                  value={communicationState}
                  onChangeItem={(value) => {
                    SetCommunicationState(value.value);
                    FillCityDropDown(value);
                  }}
                  placeholder={"Select Communication State"}
                />
              </View>
            </View>

            <View style={styles.formStyle}>
              <View style={styles.userViewContainer}>
                <DropDownPicker
                  items={dropDownCommunicationCities}
                  style={styles.input}
                  value={communicationCity}
                  onChangeItem={(value) => {
                    SetCommunicationCity(value.value);
                  }}
                  placeholder={"Select Communication City"}
                />
              </View>
            </View>

            <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <AppText style={styles.userViewLabel}>Communication ZipCode *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={communicationZipCode}
                onChangeText={e => {
                  SetCommunicationZipCode(e);
                }}
              />
            </View>
          </View>

          </View>
          ) : (
            <View></View>
          )}
          <View style={styles.view1}>
            <View
              style={{ flex: 0.15, alignItems: 'flex-end' }}>
              <CheckBox
                disabled={false}
                // value={isAgreeTerms}
                // onValueChange={(newValue) => setIsAgreeTerms(newValue)} 
                />
            </View>
            <View
              style={{ flex: 0.8, flexDirection: 'column' }}>
              <AppText style={styles.text1}>I agree to the Terms of Use/Disclaimer</AppText>
            </View>
          </View>

          {_seprator()}

          <AppButton
            title="SUBMIT"
            color="secondary"
            textColor="primary"
            onPress={() => {
              Submit()
            }}
          />

        </View>
      </ScrollView>
      <AppFooter
        activePage={0}
        isPostSales={menuType == 'postsale' ? true : false}
      />
      <AppOverlayLoader isLoading={isLoading || isApiLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userViewContainer: { marginBottom: 25 },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: colors.gray4,
    fontSize: appFonts.largeBoldx,
    paddingVertical: 8,
    color: colors.jaguar,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  formStyle: {
    flex: 1,
    alignSelf: 'flex-start',
    width: '100%',
    marginTop: 10,
    marginBottom: 10,
    paddingHorizontal: 20,
  },
  pageTitle: {
    fontSize: normStyle.normalizeWidth(24),
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    textTransform: 'uppercase',
    marginLeft: normStyle.normalizeWidth(20),
  },
  nameStyle: {
    fontWeight: 'bold',
    fontSize: normStyle.normalizeWidth(16),
  },
  seprator: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  line: {
    width: '95%',
    height: normStyle.normalizeWidth(1),
    backgroundColor: colors.veryLightGray,
    marginTop: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(20),
  },
  viewStyle: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: normStyle.normalizeWidth(20),
  },
  imageView1: {
    width: normStyle.normalizeWidth(130),
    height: normStyle.normalizeWidth(105),
    flexDirection: 'row',
    paddingLeft: normStyle.normalizeWidth(20),
  },
  profileImg: {
    width: normStyle.normalizeWidth(100),
    height: normStyle.normalizeWidth(100),
    borderRadius: normStyle.normalizeWidth(50),
    backgroundColor: colors.veryLightGray,
  },
  imageText: {
    width: normStyle.normalizeWidth(110),
    height: normStyle.normalizeWidth(110),
    borderRadius: normStyle.normalizeWidth(55),
    textAlign: 'center',
    textAlignVertical: 'center',
    fontWeight: 'bold',
    fontSize: normStyle.normalizeWidth(30),
  },
  editImg: {
    width: normStyle.normalizeWidth(17),
    height: normStyle.normalizeWidth(17),
    marginLeft: normStyle.normalizeWidth(10),
  },
  editImg1: {
    width: normStyle.normalizeWidth(17),
    height: normStyle.normalizeWidth(17),
    marginRight: normStyle.normalizeWidth(10),
  },
  text1: {
    marginTop: normStyle.normalizeWidth(7),
  },
  view1: {
    width: '100%',
    flexDirection: 'row',
  },
  view2: {
    width: '100%',
    flexDirection: 'row',
    marginTop: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(20),
  },
  unverify: {
    marginLeft: normStyle.normalizeWidth(10),
    color: colors.danger,
  },
  verified: {
    color: colors.greenColor,
  },
  verifiedImg: {
    width: normStyle.normalizeWidth(15),
    height: normStyle.normalizeWidth(15),
    marginLeft: normStyle.normalizeWidth(5),
  },
  editProfileStyle: {
    flex: 0.2,
    bottom: 0,
    right: 0,
    position: 'absolute',
    paddingRight: normStyle.normalizeWidth(10),
  },
  cameraStyle: {
    width: normStyle.normalizeWidth(35),
    height: normStyle.normalizeWidth(35),
  },
  edit1: {
    flex: 0.2,
    alignItems: 'flex-end',
    paddingRight: '2.5%',
  },
  sectionTitle: {
    fontWeight: 'bold',
  }
});

export default EmpanelmentRequest;
