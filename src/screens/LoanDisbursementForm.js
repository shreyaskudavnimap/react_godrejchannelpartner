import React, { useEffect, useState } from 'react';
import { View, Image, TextInput, StyleSheet, ScrollView, Alert, Modal, Dimensions } from 'react-native';

import DropDownPicker from 'react-native-dropdown-picker';
import { ProgressBar } from 'react-native-paper';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppFooter from '../components/ui/AppFooter';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppButton from '../components/ui/AppButton';
import appSnakBar from '../utility/appSnakBar';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import { useSelector } from 'react-redux';
import apiLoanDisbursement from '../api/apiLoanDisbursement';
import { validationDictionary } from '../utility/validation/dictionary';
import validatejs from 'validate.js';
// import { set } from 'react-native-reanimated';
import appConstant from '../utility/appConstant';
import GetLocation from 'react-native-get-location';
import appUserAuthorization from '../utility/appUserAuthorization';
import Orientation from 'react-native-orientation';
// import SelectDropdownModalScreen from './modals/SelectDropdownModalScreen';
// import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const paymentModeData = [
  { label: 'Cheque', value: 'Cheque' },
  { label: 'Demand Draft', value: 'Demand Draft' },
  { label: 'Electronic Fund Transfer', value: 'Electronic Fund Transfer' },
];

const LoanDisbursementForm = ({ navigation, route }) => {
  let controller;
  const [isLoading, setIsLoading] = useState(false);
  const [showPaymentModeDropdown, isShowPaymentModeDropdown] = useState(false);
  const routeParams = route.params;
  const booking_id = routeParams && routeParams.booking_id ? routeParams.booking_id : '';
  const [bookingId, setBookingId] = useState(booking_id);
  const payment_amount = routeParams && routeParams.payment_amount ? routeParams.payment_amount : '';
  const [paymentAmount, setPaymentAmount] = useState(payment_amount);
  const split_payment = routeParams && routeParams.split_payment ? routeParams.split_payment : [];
  const [splitPayment, setSplitPayment] = useState(split_payment);
  console.log("splitPayment", splitPayment);
  console.log("paymentAmount", paymentAmount);
  // const active_tab = routeParams && routeParams.activeTab ? routeParams.activeTab : 'LoanEnquiry';
  // const [activeTab, setActiveTab] = useState(active_tab);
  const [propertyAddress, setPropertyAddress] = useState('');
  const [presentStageConstruct, setPresentStageConstruct] = useState('');
  const [dynamicBottomPadding, setDynamicBottomPadding] = useState(0);

  const [existDisburseData, setExistDisburseData] = useState([]);

  const [bankData, setBankData] = useState({
    in_favouring: "",
    account_number: "",
    bank_name: "",
    ifsc_code: "",
    branch_name: "",
    bank_loan_amount: ""
  })
  const [inputs, setInputs] = useState({
    borrower_name: {
      type: 'generic',
      value: '',
    },
    loan_amount: {
      type: 'generic',
      value: '',
    },
    selected_payment_mode: {
      type: 'generic',
      value: '',
    },
    hdfc_file_number: {
      type: 'generic',
      value: '',
    }
  });

  const loginvalue = useSelector(
    (state) => state.loginInfo.loginResponse,
  );
  const userId = typeof loginvalue.data !== 'undefined' ? loginvalue.data.userid : '';
  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );
  // console.log("splitPayment", splitPayment);

useEffect(() => {
  Orientation.lockToPortrait();
}, []);


  useEffect(() => {
    showAuthPopup();
    getBankDetails();
    getPropertyDetails();
    getLoanDisburseExistData();
  }, []);

  const showAuthPopup = async () => {
    setIsLoading(true);
    try {
      const requestData = { user_id: userId, type: "loan_disbursment_authorization" };
      const response = await apiLoanDisbursement.loanDisclaimerContent(requestData);
      const resultData = response.data
      if (resultData.status == "200" && resultData.disclaimer_text) {
        setIsLoading(false);
        // console.log(loanEnquiry.data.msg);
        Alert.alert(
          'Authorization',
          resultData.disclaimer_text.replace(/<(.|\n)*?>/g, ''),
          [
            {
              text: 'Cancel',
              onPress: () => {
                navigation.goBack();
              },
            },
            {
              text: 'I Accept',
              onPress: () => {
                console.log('I Accept Pressed');
                postAuthorization("Request for Disbursal");
              },
            }
          ],
          { cancelable: false },
        );
      }
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const getPropertyDetails = async () => {
    try {
      const requestData = { user_id: userId, booking_id: bookingId };
      const response = await apiLoanDisbursement.getPropertyAddress(requestData);
      const resultData = response.data
      if (resultData.status == "200") {
        let propertyAddress = resultData.property_address && resultData.property_address != ""
          ? resultData.property_address
          : ""
        setPropertyAddress(propertyAddress);
        let presentStageConstruct =
          resultData.present_stage_construct &&
            resultData.present_stage_construct != ""
            ? resultData.present_stage_construct
            : "";
        setPresentStageConstruct(presentStageConstruct);
      }
    } catch (error) {
      console.log(error);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  }

  const getBankDetails = async () => {
    try {
      const requestParam = { user_id: userId, booking_id: bookingId }
      const bankDetails = await apiLoanDisbursement.getBankDetails(requestParam);
      if (bankDetails.data.status == 200) {
        const resultData = bankDetails.data.bank_details[0];
        const bankData = {
          in_favouring: resultData.in_favouring,
          account_number: resultData.account_no,
          bank_name: resultData.bank_name,
          ifsc_code: resultData.ifsc_code,
          branch_name: resultData.branch_name,
          bank_loan_amount: resultData.bank_loan_amount
        }
        setBankData(bankData);
        getLoanDisburseExistData(resultData.bank_loan_amount);
      }
    } catch (error) {
      console.log(error);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  }

  const getLoanDisburseExistData = async (loanAmount) => {
    try {
      const requestParam = { user_id: userId, booking_id: bookingId }
      const response = await apiLoanDisbursement.existDisbursementDetails(requestParam);
      if (response.data.status == 200) {
        const resultData = response.data.data;
        setExistDisburseData(resultData);
        setInputs(
          {
            loan_amount: {
              type: 'generic',
              value: loanAmount,
            },
            borrower_name: {
              type: 'generic',
              value: resultData.borrower_name,
            },
            selected_payment_mode: {
              type: 'generic',
              value: resultData.payment_type,
            },
            hdfc_file_number: {
              type: 'generic',
              value: resultData.hdfc_file_number
            }
          });
      } else {
        onInputChange('loan_amount', loanAmount);
      }
    } catch (error) {
      console.log(error);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }

  }
  // console.log("INPUTS", inputs);

  const getInputValidationState = ({ input, value }) => {
    return {
      ...input,
      value,
      errorLabel: input.optional
        ? null
        : validateInput({ type: input.type, value }),
    };
  };

  const validateInput = ({ type, value }) => {
    const result = validatejs(
      {
        [type]: value,
      },
      {
        [type]: validationDictionary[type],
      },
    );

    if (result) {
      return result[type][0];
    }

    return null;
  };

  const getFormValidation = () => {
    const updatedInputs = {};

    for (const [key, input] of Object.entries(inputs)) {
      updatedInputs[key] = getInputValidationState({
        input,
        value: input.value,
      });
    }
    setInputs(updatedInputs);
  };

  const renderError = (id) => {
    if (inputs[id].errorLabel) {
      return <AppText style={styles.txtError}>{inputs[id].errorLabel}</AppText>;
    }
    return null;
  };

  const onSelectPaymentMode = (item) => {
    onInputChange('selected_payment_mode', item.value)
    isShowPaymentModeDropdown(false);
  };

  const onInputChange = (id, value) => {
    // console.log("Input=====", id, value);
    setInputs({
      ...inputs,
      [id]: getInputValidationState({
        input: inputs[id],
        value,
      }),
    });
  };

  const handleSubmit = () => {
    getFormValidation();
    // console.log("inputs", inputs);
    let payableLocation = typeof existDisburseData.payable_location !== 'undefined' ? existDisburseData.payable_location : "";
    let payableDate = typeof existDisburseData.payable_date !== 'undefined' ? existDisburseData.payable_date : "";
    let disbursementDate = typeof existDisburseData.disbursement_date !== 'undefined' ? existDisburseData.disbursement_date : "";
    let presentStageConstruct = typeof existDisburseData.present_stage_construct !== 'undefined' ? existDisburseData.present_stage_construct : "";

    if (inputs.borrower_name.value && inputs.hdfc_file_number.value) {
      const disburseStep1 = {
        hdfc_file_number: inputs.hdfc_file_number.value,
        loan_amount: inputs.loan_amount.value,
        borrower_name: inputs.borrower_name.value,
        property_details: propertyAddress,
        selected_payment_mode: inputs.selected_payment_mode.value,
        in_favouring: bankData.in_favouring,
        account_number: bankData.account_number,
        bank_name: bankData.bank_name,
        ifsc_code: bankData.ifsc_code,
        branch_name: bankData.branch_name,
        present_stage_construct: presentStageConstruct,
        payable_location: payableLocation,
        payable_date: payableDate,
        disbursement_date: disbursementDate,
        present_stage_construct: presentStageConstruct,
        splitPayment: splitPayment,
        loan_amount_disbursed: paymentAmount
      };
      // console.log('disburseStep1');
      // console.log(disburseStep1);
      navigation.navigate("LoanDisbursementForm2", {
        disburseStep1: disburseStep1,
        booking_id: bookingId,
        // exDisburseData: this.exDisburseData
      });

    }
  };

  const postAuthorization = (moduleName) => {
    const storedUserData = loginvalue.data;
    const requestData = {
      deviceId: deviceUniqueId,
      devicePlatform: Platform.OS,
      userId: userId,
      customerName:
        storedUserData && storedUserData.first_name && storedUserData.last_name
          ? `${storedUserData.first_name} ${storedUserData.last_name}`
          : '',
      mobileNo:
        storedUserData && storedUserData.mob_no ? storedUserData.mob_no : '',
      emailId:
        storedUserData && storedUserData.email ? storedUserData.email : '',
      moduleName: moduleName,
    };
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
      .then(({ latitude, longitude }) => {
        appUserAuthorization.validateUserAuthorization({
          ...requestData,
          latiTude: latitude,
          longiTude: longitude,
        });
      })
      .catch((error) => {
        appUserAuthorization.validateUserAuthorization(requestData);
      });
  };
  const onHandleRetryBtn = () => {
    showAuthPopup();
    getBankDetails();
    getPropertyDetails();
    getLoanDisburseExistData();
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />
      <ScrollView>
        <View style={styles.container}
          onStartShouldSetResponder={() => {
            controller.close();
          }}>
          <AppText style={styles.pageTitle}>Loan Disbursement</AppText>

          <View style={styles.coverLetterCon}>
            <AppText style={styles.coverText}>Covering Letter</AppText>
            <Image
              source={require('./../assets/images/hdfc-card.png')}
              style={styles.img}
            />
          </View>

          <AppText style={styles.updateText}>Update your details(1/2)</AppText>
          <ProgressBar
            style={{ marginBottom: 25 }}
            progress={0.5}
            color={colors.gray}
          />

          <AppTextBold style={{ fontSize: appFonts.xlargeFontSize }}>Customer Details</AppTextBold>

          <View style={styles.inputContainer}>
            <AppText>Name of the Borrower*</AppText>
            <TextInput style={styles.input}
              value={
                inputs['borrower_name'] && inputs['borrower_name'].value ? inputs['borrower_name'].value : ''
              }
              onChangeText={(value) => {
                onInputChange('borrower_name', value);
              }}
            />
            {renderError('borrower_name')}
          </View>
          <View style={styles.inputContainer}>
            <AppText>Loan Amount</AppText>
            <View style={styles.loanAmount}>
              <AppText style={{ marginBottom: "5%", fontSize:appFonts.largeBold }} >&#8377;</AppText>
              <TextInput style={styles.input}
                maxLength={10}
                contextMenuHidden={true}
                keyboardType={'numeric'}
                value={
                  inputs['loan_amount'] && inputs['loan_amount'].value ? inputs['loan_amount'].value : ''
                }
                onChangeText={(value) => {
                  onInputChange('loan_amount', value);
                }}
              />
            </View>
            {/* {renderError('loan_amount')} */}
          </View>
          <View style={styles.inputContainer}>
            <AppText>Payment Mode</AppText>
            {/* <TouchableWithoutFeedback
              onPress={() => {
                if (paymentModeData.length > 1) {
                  isShowPaymentModeDropdown(true);
                }
              }}>
              <View style={styles.itemContainer}>
                <AppText style={styles.propertyText} numberOfLines={1}>
                  {inputs['selected_payment_mode'] && inputs['selected_payment_mode'].value
                    ? inputs['selected_payment_mode'].value
                    : ''}
                </AppText>
                {paymentModeData.length > 1 && (
                  <Image
                    style={styles.arrow}
                    source={require('./../assets/images/down-icon-b.png')}
                  />
                )}
              </View>
            </TouchableWithoutFeedback> */}
            <DropDownPicker
              items={paymentModeData}
              itemStyle={styles.dropDownItem}
              style={styles.dropDown}
              placeholderStyle={styles.dropdownPlaceholder}
              placeholder="Select Payment Mode"
              onChangeItem={(item) => onInputChange('selected_payment_mode', item.value)}
              defaultValue={
                inputs['selected_payment_mode'] && inputs['selected_payment_mode'].value
                  ? inputs['selected_payment_mode'].value
                  : ''
              }
              controller={instance => controller = instance}
              onOpen={() => {
                setDynamicBottomPadding(130);
              }}
              onClose={() => {
                setDynamicBottomPadding(0);
              }}
            />
            <View style={{ paddingBottom: dynamicBottomPadding }}></View>
          </View>
          <View style={styles.inputContainer}>
            <AppText>HDFC File No*</AppText>
            <TextInput style={styles.input}
              value={
                inputs['hdfc_file_number'] && inputs['hdfc_file_number'].value ? inputs['hdfc_file_number'].value : ''
              }
              onChangeText={(value) => {
                onInputChange('hdfc_file_number', value);
              }}
            />
            {renderError('hdfc_file_number')}
          </View>
          <View style={styles.inputContainer}>
            <AppText>Property Details</AppText>
            <AppText style={[styles.input, styles.propertyAddress]}>{unescape(propertyAddress)}</AppText>
          </View>

          <AppButton title="NEXT" onPress={() => { handleSubmit() }} />
        </View>
      </ScrollView>

      {/* <Modal
        animationType="fade"
        transparent={true}
        visible={showPaymentModeDropdown}
        onRequestClose={() => {
          isShowPaymentModeDropdown(false);
        }}>
        <SelectDropdownModalScreen
          itemHeight={windowHeight / 3.5}
          properties={paymentModeData}
          onClose={() => {
            isShowPaymentModeDropdown(false);
          }}
          onSelect={(item) => {
            onSelectPaymentMode(item);
          }}
        />
      </Modal> */}

      <AppFooter isPostSales={true} />
      <AppOverlayLoader isLoading={isLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '6%',
    justifyContent: 'center',
    marginBottom: 20,
  },
  pageTitle: {
    fontSize: appFonts.xxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    textTransform: 'uppercase',
    paddingTop: '5%',
    paddingBottom: '8%',
  },
  img: {
    width: 150,
    height: 30,
  },
  coverLetterCon: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  coverText: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProBold,
  },
  updateText: {
    fontSize: appFonts.largeBold,
    marginTop: 25,
    paddingBottom: 5,
    alignSelf: 'center',
  },
  inputContainer: {
    marginTop: '5%',
  },
  input: {
    width: '100%',
    marginBottom: '3%',
    fontSize:appFonts.largeBold,
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGray,
  },
  loanAmount: {
    flex: 1,
    flexDirection: "row",
    alignItems: 'center',
    fontSize:appFonts.largeBold,
  },
  propertyAddress: {
    height: 'auto'
  },
  dropDownItem: {
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGray,
  },
  dropDown: {
    marginBottom: '3%',
    height: 50,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGray,
  },
  dropdownPlaceholder: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
  },
  txtError: {
    position: 'absolute',
    bottom: -32,
    color: colors.danger,
    fontSize: appFonts.largeFontSize,
    marginBottom: 20,
  },
  txtErrorDoc: {
    color: colors.danger,
    fontSize: appFonts.largeFontSize,
    marginBottom: 20,
  },
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: colors.gray4,
    paddingVertical: 20,
    paddingRight: 20,
  },
  arrow: { height: 16, width: 16 },
});

export default LoanDisbursementForm;
