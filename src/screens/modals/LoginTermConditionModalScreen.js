import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
  useWindowDimensions,
} from 'react-native';
import HTML from 'react-native-render-html';

import AppButton from '../../components/ui/AppButton';
import appFonts from '../../config/appFonts';
import colors from '../../config/colors';
import AppText from '../../components/ui/ AppText';
import {SafeAreaView} from 'react-native-safe-area-context';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const LoginTermConditionModalScreen = ({onCancel, onContinue, termsData}) => {

  return (
    <View style={styles.mainContainer}>
      <View style={styles.container}>
        <SafeAreaView style={{flex: 1, paddingVertical: 25}}>
          <View style={styles.headerContainer}>
            <AppText style={styles.headerText}>Terms of Use</AppText>
          </View>

          <ScrollView bounces={false}>
            {termsData ? (
              <View style={styles.contentViewContainer}>
                <HTML
                  allowFontScaling={false}
                  containerStyle={{color: colors.gray3}}
                  listsPrefixesRenderers={{
                    ol: (
                      htmlAttribs,
                      children,
                      convertedCSSStyles,
                      passProps,
                    ) => {
                      return (
                        <AppText
                          style={{
                            color: colors.gray3,
                            fontSize: appFonts.normalFontSize,
                            fontFamily: appFonts.SourceSansProRegular,
                            marginTop: 6,
                            marginRight: 6,
                          }}>
                          {passProps && passProps.index
                            ? `${passProps.index + 1}. `
                            : '1. '}
                        </AppText>
                      );
                    },
                  }}
                  tagsStyles={{
                    p: {
                      fontSize: appFonts.normalFontSize,
                      lineHeight: 19,
                      textAlign: 'justify',
                      fontFamily: appFonts.SourceSansProRegular,
                      marginBottom: 13,
                      color: colors.gray3,
                    },
                    li: {
                      fontSize: appFonts.normalFontSize,
                      lineHeight: 19,
                      textAlign: 'justify',
                      fontFamily: appFonts.SourceSansProRegular,
                      color: colors.gray3,
                      marginTop: 5,
                    },
                  }}
                  ignoredStyles={['font-family']}
                  html={termsData}
                  contentWidth={useWindowDimensions().width}
                />
              </View>
            ) : null}
          </ScrollView>

          <View style={styles.btnContainer}>
            <AppButton title="I Agree" onPress={onContinue} />
          </View>
        </SafeAreaView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    height: windowHeight,
    width: windowWidth,
    backgroundColor: colors.primary,
    padding: 20,
    marginHorizontal: 22,
    borderWidth: 1,
    borderRadius: 8,
    borderColor: colors.lightGray,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 25,
  },
  headerContainerImg: {
    alignItems: 'flex-end',
  },
  headerText: {
    flex: 1,
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProBold,
    textAlign: 'center',
  },
  iconCancel: {
    width: 14,
    height: 14,
    alignSelf: 'flex-end',
  },
  contentViewContainer: {width: '95%', marginVertical: 5, justifyContent: 'center', alignItems: 'center'},
  btnContainer: {
    marginTop: 25,
  },
});
export default LoginTermConditionModalScreen;
