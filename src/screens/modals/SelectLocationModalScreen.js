import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import AppText from '../../components/ui/ AppText';
import Screen from '../../components/Screen';
import AppButton from '../../components/ui/AppButton';
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import Icon from 'react-native-vector-icons/MaterialIcons';

const {width: windowWidth, height: windowHeight} = Dimensions.get('window');

const SelectLocationModalScreen = ({
  cities,
  onSelectCity,
  onCancel,
  onUpdate,
  titleSelect = 'Select Location',
}) => {
  return (
    <Screen style={styles.mainContainr}>
      {cities && (
        <View style={styles.container}>
          <View>
            <TouchableWithoutFeedback onPress={onCancel}>
              <View style={styles.itemContainer}>
                <AppText
                  style={[
                    styles.itemTxt,
                    {
                      fontFamily: appFonts.SourceSansProSemiBold,
                      fontSize: appFonts.largeBold,
                    },
                  ]}>
                  {titleSelect}
                </AppText>
                <Image
                  style={styles.arrow}
                  source={require('./../../assets/images/up-icon-b.png')}
                />
              </View>
            </TouchableWithoutFeedback>
            <View style={styles.devider} />
            <View style={{height: windowHeight * 0.45}}>
              <ScrollView bounces={false}>
                {cities.map((item, index) => (
                  <View key={index}>
                    <TouchableWithoutFeedback
                      onPress={() => onSelectCity(item)}>
                      <View style={styles.itemContainerCity}>
                        <Image
                          style={styles.checkbox}
                          source={
                            item.selected
                              ? require('./../../assets/images/checked-icon.png')
                              : require('./../../assets/images/unchecked-icon.png')
                          }
                        />
                        <AppText
                          style={[
                            styles.itemTxt,
                            {
                              fontFamily: item.selected
                                ? appFonts.SourceSansProBold
                                : appFonts.SourceSansProSemiBold,
                              fontSize: item.selected ? appFonts.largeBoldx : appFonts.largeBold,
                            },
                          ]}>
                          {item.name}
                        </AppText>
                      </View>
                    </TouchableWithoutFeedback>

                    <View style={styles.devider} />
                  </View>
                ))}
              </ScrollView>
              {cities.length > 4 ? (
                <Icon
                  name="keyboard-arrow-down"
                  color="black"
                  size={30}
                  style={{alignSelf: 'center', height: 20}}
                />
              ) : null}
            </View>
          </View>

          <View style={styles.btnContainer}>
            <AppButton
              title="CANCEL"
              color="primary"
              textColor="secondary"
              onPress={onCancel}
            />

            <AppButton title="UPDATE" onPress={onUpdate} />
          </View>
        </View>
      )}
    </Screen>
  );
};

const styles = StyleSheet.create({
  mainContainr: {
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
  },
  container: {
    marginTop: windowHeight * 0.05,
    backgroundColor: '#f7f7f7',
    marginHorizontal: 20,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 3, height: 4},
    shadowOpacity: 0.9,
    shadowRadius: 3,
    elevation: 3,
  },
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
  },
  itemContainerCity: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 15,
  },
  itemTxt: {
    color: colors.jaguar,
  },
  arrow: {height: 16, width: 16},
  checkbox: {height: 24, width: 24, marginRight: 15},
  devider: {backgroundColor: '#e5e5e5', height: 1},
  btnContainer: {
    marginBottom: 25,
    paddingHorizontal: 26,
    paddingTop: 5
  },
});

export default SelectLocationModalScreen;
