import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
  ScrollView,
  Image,
} from 'react-native';
import Screen from '../../components/Screen';
import AppButton from '../../components/ui/AppButton';
import AppTextBold from '../../components/ui/AppTextBold';
import AppText from '../../components/ui/ AppText';
import appFonts from '../../config/appFonts';
import colors from '../../config/colors';
import {Calendar} from 'react-native-calendars'
import appSnakBar from '../../utility/appSnakBar';
import appConstant from '../../utility/appConstant';
import moment from 'moment'
import apiScheduledVisit from '../../api/apiScheduledVisit'
import DropDownPicker from 'react-native-dropdown-picker';

var {height, width} = Dimensions.get('window');

const ReschedulePopUp = ({isOpen, userId, toggleModal, selectedScheduleVisit, setCallGetScheduleVisit, setLoading}) => {
	const [showCalendar, setShowCalendar] = useState(false)
  const [preferredDate, setPreferredDate] = useState(selectedScheduleVisit['date_of_visit'])

  const [isDateAvailable, setIsDateAvailable] = useState(true)
  const [fromDate, setFromDate] = useState('')
  const [toDate, setToDate] = useState('')
  const [daysConfig, setDaysConfig] = useState([])
  const [availableSlots, setAvailableSlots] = useState({})
  const [isSlotsAvailable, setIsSlotsAvailable] = useState(true)
  const [slots, setSlots] = useState([])
  const [slot, setSlot] = useState({})
  
  useEffect(() => {
    getScheduleDetails()
  }, [])

  const getScheduleDetails = async () => {
    const payload = {
      user_id: userId
    }

    try {
      const result = await apiScheduledVisit.apiGetScheduleVisitDetails(payload)
      const res = result.data
      if (res.status == 200 && res.data) {
        const scheduleDetails = res.data
        let prerequisite = {};
        let selectedPrerequisite = {}
        let minDate, maxDate;
        const projectId = selectedScheduleVisit['project_id'] || null
        if (projectId && scheduleDetails && scheduleDetails['prerequisite']) {
          prerequisite = scheduleDetails["prerequisite"]

          if (prerequisite && selectedScheduleVisit['schedule_type_id']) {
            let key = `${projectId}_${selectedScheduleVisit['schedule_type_id']}`
            selectedPrerequisite = prerequisite[key] || {}
          }

          minDate = selectedPrerequisite && selectedPrerequisite["from_date"] ? selectedPrerequisite["from_date"] : null
          maxDate = selectedPrerequisite && selectedPrerequisite["to_date"] ? selectedPrerequisite["to_date"] : null
          doDateValidation(minDate, maxDate)
        }

        getRMVisitDetails(projectId, selectedScheduleVisit["inventory_id"], selectedScheduleVisit["schedule_type_id"])
      }
    } catch (err) {
      setLoading(false)
      console.log(err)
      appSnakBar.onShowSnakBar(appConstant.appMessage.APP_GENERIC_ERROR, 'LONG');
    }
  }

  const getRMVisitDetails = async (projectId, inventoryId, visitId) => {
    const payload = {
      project_id: projectId,
      visit_id: visitId,
      inventory_id: inventoryId
    }

    try {
      const result = await apiScheduledVisit.apiGetRMVistDates(payload)
      const res = result.data
      if (res && res.status == 200 && res.dates && res.dates.length) {
        const fromDate = res.dates[0];
        const toDate = res.dates[res.dates.length - 1];
        createConfigs(
          res.dates,
          getDateRange(res.dates[0], res.dates[res.dates.length - 1])
        )

        setFromDate(fromDate)
        setToDate(toDate)
        doDateValidation(fromDate, toDate)
        getAvailableSlots(selectedScheduleVisit['date_of_visit'])
      }
    } catch (err) {
      setLoading(false)
      console.log(err)
      appSnakBar.onShowSnakBar(appConstant.appMessage.APP_GENERIC_ERROR, 'LONG');
    }
  }

  const createConfigs = (enableDates, allDates) => {
    let isDateAvailable = false
    const dateConfigs = []
    const currentDate = new Date();
    const currentDateTime = currentDate.getTime()
    if (allDates.length && enableDates.length) {
      allDates.forEach(date => {
        const fromDate = new Date(`${date}T23:59:59`);
        const fromDateTime = fromDate.getTime();

        if (fromDateTime > currentDateTime) {
          dateConfigs.push({
            date,
            disable: !enableDates.includes(date)
          })

          isDateAvailable = !enableDates.includes(date)
        } else {
          dateConfigs.push({
            date,
            disable: true
          })
        }
      })

      setDaysConfig(dateConfigs)
    }
  }

  const getDateRange = (startDateParam, endDateParam) => {
    const startDate = new Date(startDateParam)
    const endDate = new Date(endDateParam)

    const dates = []
    let currentDate = startDate

    const addDays = function (days) {
      const date = new Date(this.valueOf())
      date.setDate(date.getDate() + days) 
      return date
    }

    while (currentDate <= endDate) {
      const dateString = currentDate.getFullYear() + 
                          "-" +
                          (currentDate.getMonth() + 1 < 10
                          ? "0" + (currentDate.getMonth() + 1)
                          : currentDate.getMonth() + 1) +
                          "-" +
                          (currentDate.getDate() < 10 
                          ? "0" + currentDate.getDate()
                          : currentDate.getDate());
      dates.push(dateString)
      currentDate = addDays.call(currentDate, 1)
    }

    return dates
  }

  const getAvailableSlots = async (date) => {
    const payload = {
      user_id: userId,
      inventory_nid: selectedScheduleVisit["inventory_id"],
      visit_type: selectedScheduleVisit["schedule_type_id"],
      date
    }

    setLoading(true)
    try {
      const result = await apiScheduledVisit.apiGetAvailableSlots(payload)
      const res = result.data
      if (res && res.status == 200 && res.data) {
        const isSlotsAvailable =  false
        const bookingDetilsByDate = res.data
        if (bookingDetilsByDate && bookingDetilsByDate['time_slots']) {
          const slots = bookingDetilsByDate['time_slots'] || {}
          prepareSlots(slots)
        }
      }
      setLoading(false)
    } catch (err) {
      setLoading(false)
      console.log(err)
      appSnakBar.onShowSnakBar(appConstant.appMessage.APP_GENERIC_ERROR, 'LONG');
    }
  }

  const prepareSlots = (slotsParam) => {
    const availableSlots = {}
    const slots = []
    let isSlotsAvailable = false
    let selectedSlot = {}
    for (const key in slotsParam) {
      if (slotsParam.hasOwnProperty(key)) {
        const slot = slotsParam[key]
        if (slot['max_value'] && !isNaN(slot['max_value']) && parseInt(slot['max_value']) > 0) {
          availableSlots[key] = slot
          slots.push({
            label: slot.text,
            value: key
          })
          if (selectedScheduleVisit['time_slot'] === slot.text) {
            selectedSlot = {
              label: slot.text,
              value: key
             }
          }
          isSlotsAvailable = true
        }
      }
    }

    setAvailableSlots(availableSlots)
    setSlots(slots)
    setSlot(selectedSlot)
    setIsSlotsAvailable(isSlotsAvailable)
  }

  const doDateValidation = (minDate, maxDate) => {
    let isDateAvailable = true
    if (minDate && maxDate) {
      const fromDate = new Date(minDate + 'T23:59:59')
      const toDate = new Date(maxDate + 'T23:59:59')
      const currentDate = new Date()

      const fromDateTime = fromDate.getTime()
      const toDateTime = toDate.getTime()

      const currentDateTime = currentDate.getTime()
      isDateAvailable = true
      if (fromDateTime < currentDateTime) {
        minDate = currentDate
      }
      if (toDateTime < currentDateTime) {
        isDateAvailable = false
      }
      minDate = moment(minDate).format('YYYY-MM-DD')
      setFromDate(minDate)
      setToDate(maxDate)
    } else {
      setFromDate(null)
      setToDate(null)
      isDateAvailable = false
    }
    setIsDateAvailable(isDateAvailable)
  }

  const getMarkedDates = (daysConfig) => {
    const markedDates = {}

    daysConfig.forEach(dc => {
      markedDates[dc.date] = { disabled: dc.disable, disableTouchEvent: dc.disable }
    })

    return markedDates
  }

  const onDateChange = (date) => {
    setPreferredDate(date)
    getAvailableSlots(date)
  }

  const onSlotChange = (slot) => {
    setSlot(slot)
  }

  const onSubmit = () => {
    let slotFlag = true
    const payload = {
      user_id: userId,
      visit_id: selectedScheduleVisit['visit_id'],
      number_of_persons: selectedScheduleVisit['number_of_persons'],
      date_of_visit: preferredDate,
      time_slot: slot.value,
      save_time_slot: slot.value
    }

    if (preferredDate == selectedScheduleVisit['date_of_visit'] && slot.label == selectedScheduleVisit['time_slot']) {
      slotFlag = false
      if (selectedScheduleVisit['status'] == 'Rescheduled') {
        appSnakBar.onShowSnakBar('You have already requested for Reschdule on the selected date', 'LONG');
      } else {
        appSnakBar.onShowSnakBar('Please select another date to reschedule', 'LONG');
      } 
    } else {
      proceedSubmit(payload)
    }
  } 

  const proceedSubmit = async (payload) => {
    setLoading(true)
    try {
      const result = await apiScheduledVisit.apiUpdateScheduleVisit(payload)
      const res = result.data
      if (res && res.status == 200 && res.data && res.data['time_slot']) {
        appSnakBar.onShowSnakBar(res.msg? res.msg : "Your request for Reschedule Visit is submitted successfully.", 'LONG');
        setTimeout(() => {
          toggleModal(false)
          setLoading(false)
          setCallGetScheduleVisit(new Date().getTime())  
        }, 1000);
      } else {
        if (res.status && res.status == '506') {
          appSnakBar.onShowSnakBar(res.msg, 'LONG');
          toggleModal(false)
        }
      }
    } catch (err) {
      setLoading(false)
      console.log(err)
      appSnakBar.onShowSnakBar(appConstant.appMessage.APP_GENERIC_ERROR, 'LONG');
    }
  }

  return (
    <Screen>
      <Modal visible={isOpen} animationType={'fade'} transparent={true}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={{paddingHorizontal: 20}}>
              <View style={[styles.titleLine, {paddingVertical: 0}]}>
                <AppTextBold style={styles.title}>RESCHEDULE</AppTextBold>

                <TouchableWithoutFeedback
                  onPress={() => {
                    toggleModal(false);
                  }}>
                  <Image
                    source={require('./../../assets/images/cancel.png')}
                    style={styles.icon}
                  />
                </TouchableWithoutFeedback>
              </View>
            </View>

            <ScrollView>
              <View>
								<View style={styles.listStyle}>
									<View>
										<AppText>Property</AppText>
										<AppTextBold>{selectedScheduleVisit['project_name']}</AppTextBold>
									</View>
								</View>

								<View style={styles.listStyle}>
									<View>
										<AppText>Purpose of Visit</AppText>
										<AppTextBold>{selectedScheduleVisit['schedule_type_name']}</AppTextBold>
									</View>
								</View>

								<View style={styles.listStyle}>
									<View>
										<AppText>Prefered Date</AppText>
										<TouchableOpacity style={styles.dropdown} onPress={() => setShowCalendar(!showCalendar)}>
											{!showCalendar ? <AppTextBold>{moment(preferredDate).format("DD-MMM-YYYY")}</AppTextBold> : <View/>}
											<Image
												style={styles.down}
												source={require('./../../assets/images/down-icon-b.png')}
											/>
										</TouchableOpacity>
									</View>
									{showCalendar ?
                  <Calendar
                    minDate={fromDate}
                    maxDate={toDate}
                    onDayPress={({dateString}) => {
                      onDateChange(dateString)
                      setShowCalendar(false)
                    }}
                    markedDates={getMarkedDates(daysConfig)}
                  /> :
										<View />
									}
								</View>

                <AppText>Preferred Time</AppText>
                    <DropDownPicker
                      defaultValue={slot.value}
                      items={slots}
                      itemStyle={styles.dropDownItem}
                      labelStyle={styles.label}
                      style={styles.dropDown}
                      onChangeItem={onSlotChange}
                      dropDownMaxHeight={500}
                    />
                  {isDateAvailable && !isSlotsAvailable ?
                    <View>
                      <AppText style={{ color: colors.danger }}>Slot Is Not Available</AppText>  
                    </View> : <View />
                  }

								<View style={styles.listStyle}>
									<View>
										<AppText>Person(s)</AppText>
										<AppTextBold>{selectedScheduleVisit['number_of_persons']}</AppTextBold>
									</View>
								</View>

								<View style={styles.listStyle}>
									<View>
										<AppText>Visitors's Details</AppText>
										<AppTextBold>{selectedScheduleVisit['visitors_details']}</AppTextBold>
									</View>
								</View>
              </View>
            </ScrollView>
            <View style={{marginTop: '5%'}}>
              <AppButton
                title="SUBMIT"
                onPress={() => {
                  onSubmit()
                }}
              />
            </View>
          </View>
        </View>
      </Modal>
    </Screen>
  );
};
const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
    width: width,
    backgroundColor: colors.blurColor,
  },
  down: {
    height: 15,
    width: 15,
    top: '5%',
  },
  dropdown: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  pageTitle: {
    fontFamily: appFonts.SourceSansProBold,
    color: colors.jaguar,
    textTransform: 'uppercase',
    marginBottom: '5%',
  },
  titleLine: {
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15,
  },
  title: {
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
    left: '120%',
    marginTop: '10%',
  },
  icon: {
    width: 10,
    height: 10,
    bottom: '5%',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    height: '90%',
    width: '90%',
    padding: 20,
    backgroundColor: colors.milkWhite,
    borderRadius: 6,
  },
  listStyle: {
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: colors.borderGrey,
    marginBottom: 10,
    marginTop: '2%',
  },
  label: {
    fontSize: appFonts.xlargeFontSize,
    fontFamily: 'SourceSansPro-SemiBold',
    padding: 0,
    marginBottom: '5%',
  },
  dropDownItem: {
    borderBottomWidth: 1,
    borderBottomColor: '#ededed',
    marginTop: '5%',
    justifyContent: 'flex-start'
  },
  dropDown: {
    // marginTop:10,
    height: '100%',
    width: width * 0.9,
    right: '5%',
    marginBottom: '5%',
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderTopWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: colors.lightGray,
  },
});
export default ReschedulePopUp;
