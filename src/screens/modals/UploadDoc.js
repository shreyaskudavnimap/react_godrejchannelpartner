import React, { useState } from 'react';
import {
    View,
    StyleSheet,
    Modal,
    TouchableWithoutFeedback,
    Dimensions,
    ScrollView,
    Image
} from 'react-native';
import Screen from '../../components/Screen';
import AppButton from '../../components/ui/AppButton';
import AppTextBold from '../../components/ui/AppTextBold'
import AppText from '../../components/ui/ AppText'
import appFonts from '../../config/appFonts';
import colors from '../../config/colors';

var { height, width } = Dimensions.get('window');


const UploadDoc = (props) => {

    const [modalVisible, setModalVisible] = useState(props.visible)
    const [isSubmitClicked, setSubmit] = useState(false)
    return (
        <Screen>
            <Modal visible={modalVisible}
                animationType={'fade'}
                transparent={true}
            >
                <ScrollView>

                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>

                            {isSubmitClicked == false &&
                                <View>
                                    <View style={styles.content}>

                                        <View style={styles.cancelContainer}>
                                            <TouchableWithoutFeedback onPress={() => { setModalVisible(false); props.func(false) }}>

                                                <Image
                                                    source={require('./../../assets/images/cancel.png')}
                                                    style={styles.icon}
                                                />

                                            </TouchableWithoutFeedback>
                                        </View>
                                        <View style={styles.title}>
                                            <AppTextBold style={styles.pageTitle}>
                                                Upload Documents
                                     </AppTextBold>
                                        </View>
                                    </View>

                                    <View style={styles.docContainer}>

                                        <Image
                                            source={require('./../../assets/images/pdf-docment-b.png')}
                                            style={styles.iconDoc}
                                        />
                                        <AppText>cvv__photo.jpg</AppText>
                                    </View>
                                    <View style={{ marginTop: "5%" }}>
                                        <AppButton title="ATTACH MORE" color="primary" textColor="secondary"
                                            onPress={() => {
                                                setModalVisible(!modalVisible);
                                                props.func(false)
                                            }}
                                        />
                                        <AppButton title="SUBMIT"
                                            onPress={() => { setSubmit(true) }}
                                        />

                                    </View>
                                </View>
                            }
                            {isSubmitClicked == true &&

                                <View style={styles.content}>
                                    <View style={styles.cancelContainer}>
                                        <TouchableWithoutFeedback onPress={() => { setModalVisible(false); props.func(false) }}>

                                            <Image
                                                source={require('./../../assets/images/cancel.png')}
                                                style={styles.icon}
                                            />

                                        </TouchableWithoutFeedback>
                                    </View>
                                    <View style={styles.tickContainer}>
                                        <Image
                                            source={require('./../../assets/images/tick-60x60.png')}
                                            style={styles.iconTick}
                                        />
                                        <AppText>Documents has been submitted to your Relationship
                                        Manager for review and would
                                        appear under My Uploads on acceptance</AppText>
                                    </View>
                                </View>

                            }
                        </View>
                    </View>


                </ScrollView>
            </Modal>
        </Screen>
    )
}
const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        height: height,
        width: width,
        backgroundColor: colors.blurColor,
    },
    title: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: "12%",
        marginBottom: "8%"
    },
    content: {
        paddingHorizontal: 20,
        marginBottom: 20
    },
    docContainer: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: "90%",
        height: "10%",
        margin: 10
    },
    pageTitle: {
        fontFamily: appFonts.SourceSansProBold,
        color: colors.jaguar,
        textTransform: 'uppercase',
        marginBottom: "5%"
    },
    cancelContainer: {
        justifyContent: "flex-end",
        alignItems: "flex-end",
        marginTop: "5%",
    },
    icon: {
        width: 10,
        height: 10,
    },
    iconDoc: {
        width: 30,
        height: 30
    },
    tickContainer: {
        justifyContent: "center",
        alignItems: "center",
        marginTop: "20%"
    },
    iconTick: {
        height: 50,
        width: 50,
        marginBottom: "20%"
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        height: "50%",
        width: "90%",
        padding: 20,
        backgroundColor: colors.milkWhite,
        borderRadius: 6,
    },
})
export default UploadDoc;