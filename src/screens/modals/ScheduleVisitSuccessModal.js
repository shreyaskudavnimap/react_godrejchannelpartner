import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Text
} from 'react-native';
import appFonts from '../../config/appFonts';
import colors from '../../config/colors';
import AppText from '../../components/ui/ AppText';

const ScheduleVisitSuccessModal = ({onCancel, message}) => {
  return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
        <AppText style={styles.headerText}></AppText>
          <TouchableOpacity onPress={onCancel}>
            <Image
              source={require('./../../assets/images/cancel.png')}
              style={styles.iconCancel}
            />
          </TouchableOpacity>
        </View>

        <ScrollView>
          <View style={styles.contentViewContainer}>
              <AppText style={{textAlign: 'center'}}>{message}</AppText>
          </View>
        </ScrollView>
      </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderRadius: 10,
    height: 150,
    borderColor: colors.lightGray,
    backgroundColor: colors.whiteSmoke,
    width: '80%',   
    marginTop: '50%',
    marginLeft: '10%',  

  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    marginRight: 10
  },
  headerText: {flex: 1, fontSize: appFonts.xlargeFontSize, fontFamily: appFonts.SourceSansProBold, textAlign:'center'},
  iconCancel: {
    width: 18,
    height: 18,
  },
  contentViewContainer: { marginVertical: 25, paddingRight: 15, paddingLeft: 15},
});
export default ScheduleVisitSuccessModal;
