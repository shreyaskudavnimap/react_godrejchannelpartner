import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Modal,
  TouchableWithoutFeedback,
  Dimensions,
  ScrollView,
  Image,
  Text,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
//Components
import Screen from '../../components/Screen';
import AppButton from '../../components/ui/AppButton';
import AppTextBold from '../../components/ui/AppTextBold';
import AppText from '../../components/ui/ AppText';
import appFonts from '../../config/appFonts';
import colors from '../../config/colors';
import AppOverlayLoader from '../../components/ui/AppOverlayLoader';
//Api
import TESTIMONIAL_API from '../../api/apiTestimonial';

var {height, width} = Dimensions.get('window');

const Testimonial = (props) => {
  const [modalVisible, setModalVisible] = useState(props.visible);
  const [isSubmitClicked, setSubmit] = useState(false);
  const {isSuccessSubmitted, isSubmitModalVisible, disclaimer} = props;
  const navigation = useNavigation();

  return (
    <Screen>
      <Modal
        visible={isSubmitModalVisible}
        animationType={'fade'}
        transparent={true}>
        <ScrollView >
          <View style={styles.centeredView}>
            <View
              style={
                isSubmitClicked == false
                  ? styles.modalView
                  : styles.modalViewSubmit
              }>
              {isSubmitClicked == false && (
                <View>
                  <View style={styles.content}>
                    <View style={styles.cancelContainer}>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          props.setSubmitModalVisible(false);
                        }}>
                        <Image
                          source={require('./../../assets/images/cancel.png')}
                          style={styles.icon}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                  </View>
                 
                  <View style={styles.title}>
                      <AppText style={styles.pageTitle}>Disclaimer</AppText>
                    </View>
                    
                  <ScrollView bounces={false}>
                    <View style={styles.docContainer}>
                      <Text style={{...styles.textStyle, flex: 1}}>
                        {disclaimer}
                      </Text>
                    </View>
                  </ScrollView>
                  <View style={{marginTop: '5%'}}>
                    <AppButton
                      title="ACCEPT & CONTINUE"
                      onPress={() => {
                        props.submit();
                        setSubmit(true);
                      }}
                    />
                  </View>
                  <AppButton
                    title="CANCEL"
                    color="primary"
                    textColor="secondary"
                    onPress={() => {
                      props.setSubmitModalVisible(false);
                      setSubmit(false);
                    }}
                  />
                </View>
              )}
              {isSubmitClicked == true && (
                <ScrollView bounces={false}>
                <View style={styles.contentSuccess}>
                  <View style={styles.cancelContainer}>
                    <TouchableWithoutFeedback
                      onPress={() => {
                        props.setSubmitModalVisible(false);
                        setSubmit(false);
                        navigation.navigate('Testimonial');
                      }}>
                      <Image
                        source={require('./../../assets/images/cancel.png')}
                        style={styles.icon}
                      />
                    </TouchableWithoutFeedback>
                  </View>
                  <View style={styles.tickContainer}>
                    <Image
                      source={require('./../../assets/images/tick-60x60.png')}
                      style={styles.iconTick}
                    />
                    <AppText style={{textAlign: 'center', fontSize: 18}}>Thank You!</AppText>
                    <AppText style={{textAlign: 'center', fontSize: 17, marginTop: 8}}>
                      Your testimonial will appear after moderation
                    </AppText>
                  </View>
                </View>
                </ScrollView>  
              )}
            </View>
          </View>
        </ScrollView>
      </Modal>
    </Screen>
  );
};
const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
    width: width,
    backgroundColor: colors.blurColor,
  },
  title: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '5%',
  },
  content: {
    paddingHorizontal: 20,
    marginBottom: 12,
  },
  contentSuccess: {
    marginBottom: 12,
    width: '100%',
    height: '40%',
  },
  docContainer: {
    width: '100%',
    height: '40%',
    //margin: 10
  },
  pageTitle: {
    fontFamily: appFonts.SourceSansProBold,
    color: colors.jaguar,
    fontSize: appFonts.xxlargeFontSize
    // textTransform: 'uppercase',
    // marginBottom: "5%"
  },
  cancelContainer: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginTop: '5%',
  },
  icon: {
    width: 14,
    height: 14,
    alignSelf: 'flex-end',
  },
  iconDoc: {
    width: 30,
    height: 30,
  },
  tickContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '20%',
  },
  iconTick: {
    height: 50,
    width: 50,
    marginBottom: '10%',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    //height: "100%",
    width: '90%',
    padding: 25,
    backgroundColor: colors.milkWhite,
    borderRadius: 6,
  },
  modalViewSubmit: {
    margin: 20,
    backgroundColor: 'white',
    //height: '100%',
    width: '90%',
    padding: 20,
    backgroundColor: colors.milkWhite,
    borderRadius: 6,
  },
  textStyle: {
    fontSize: appFonts.normalFontSize,
    lineHeight: 19,
    textAlign: 'justify',
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.gray3,
    marginTop: 5,
  },
});
export default Testimonial;
