import React, { useState } from 'react';
import {
    View,
    StyleSheet,
    Modal,
    Dimensions,
    Image,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Text
} from 'react-native';
import Screen from '../../components/Screen';
import AppText from '../../components/ui/ AppText';
import AppTextBold from '../../components/ui/AppTextBold';
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import AppButton from '../../components/ui/AppButton';
import Entypo from 'react-native-vector-icons/Entypo';


var { height, width } = Dimensions.get('window');

const Selecteddocumentlistmodal = (props) => {
    const [filelist, setFilelist] = useState(props?.filesToUpload)
   
    return (
        <Screen>
            <Modal
                transparent={true}
                visible={true}
                animationType={'fade'}>
                <View style={styles.centeredView}>
                    <View style={styles.modal}>
                        <View style={{display:"flex",flexDirection:"row",justifyContent:"space-between"}}>
                    <AppTextBold style={{marginTop:0}}>Upload Documents</AppTextBold>
                        <View style={styles.cancelContainer}>
                            <TouchableWithoutFeedback onPress={() => {
                                props.func(false);
                                props.aftercancelresetdata([])
                            }} hitSlop={{ top: 20, bottom: 20, left: 100, right: 50 }}>

                                <Image
                                    source={require('./../../assets/images/cancel.png')}
                                    style={styles.icon}
                                />

                            </TouchableWithoutFeedback>
                        </View>
                        </View>
                        <View style={{marginTop:40,marginBottom:20}}>
                            {filelist.map((item, index) => {
                                return <View>
                                    <View style={{padding: 15, borderRadius: 15, backgroundColor: colors.lightGray, flexDirection: 'row', marginTop: 3}}>
                                            <Text>{item.file_name}</Text>
                                    </View>
                                </View>
                            })}
                        </View>
                        {props.fileslength>0 && props.fileslength<5 ?
                        <View style={styles.attachfilebutton}>
                            <AppText style={{ margin: 10 }} >
                                Attach more 
												</AppText>
                            <TouchableOpacity style={styles.attachfilebutton} onPress={() => {
                                props.choosefileagain()
                            }}>

                                <Image
                                    style={styles.iconAdd}
                                    source={require('../../assets/images/attachment-icon.png')} />
                            </TouchableOpacity>

                        </View>:null}
                        <View style={styles.appButton}>
                            <AppButton title="SUBMIT" onPress={() => { props.onsubmitdocument(); props.func(false) }} />
                        </View>
                    </View>
                </View>
            </Modal>
        </Screen>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.blurColor,
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        height: height,
        width: width,
        backgroundColor: colors.blurColor,
    },
    modal: {
        backgroundColor: "white",
        width: "90%",
        padding: 20,
        paddingTop: 10,
        backgroundColor: colors.milkWhite,
        borderRadius: 6,
        color: "black",
        
    },
    cancelContainer: {
        justifyContent: "flex-end",
        alignItems: "flex-end",
        borderColor:"black",
        
    

    },
    icon: {
        
        width: 15,
        height: 15,
    },
    image: { height: 51, width: 51, marginBottom: 10, padding: 10, },

    appButton: {
        width: "100%",
        alignSelf: 'center',
        marginTop: "8%",
        marginBottom: "5%"
    },
    attachfilebutton: {
        marginTop: 3,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    attachfilebutton: {
        marginTop: 3,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    iconAdd: {
        height: 25,
        width: 25,
        marginHorizontal: 2,
        marginBottom: 1
    }
})

export default Selecteddocumentlistmodal