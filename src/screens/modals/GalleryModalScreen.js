import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Image,
  Dimensions,
} from 'react-native';

import Carousel from 'react-native-snap-carousel';
import Orientation from 'react-native-orientation';

import Screen from '../../components/Screen';
import colors from '../../config/colors';
import AppText from '../../components/ui/ AppText';
import appFonts from '../../config/appFonts';
import AppGalleryItem from '../../components/ui/AppGalleryItem';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const window = Dimensions.get('window');
const screen = Dimensions.get('screen');

let previewCarousel;

const GalleryModalScreen = ({
  onPress,
  projectName = '',
  galleryData,
  imgIndex = 0,
  mediaIndex = 0,
}) => {
  // const [activeIndex, setActiveIndex] = useState(
  //   mediaIndex > 0 ? mediaIndex : 0,
  // );

  const [activeIndex, setActiveIndex] = useState(imgIndex > 0 ? imgIndex : 0);

  const [dimensions, setDimensions] = useState({window, screen});
  console.log(
    'dimensions:: width: ',
    dimensions.window.width + ' :: height: ' + dimensions.window.height,
  );

  useEffect(() => {
    Orientation.unlockAllOrientations();
  });

  const onChange = ({window, screen}) => {
    setDimensions({window, screen});
  };

  useEffect(() => {
    Dimensions.addEventListener('change', onChange);
    return () => {
      Dimensions.removeEventListener('change', onChange);
    };
  });

  return (
    <Screen style={styles.container}>
      <View style={styles.centeredView}>
        <View style={styles.titleContainer}>
          <AppText style={styles.title} numberOfLines={1}>
            {projectName}
          </AppText>
          <TouchableHighlight onPress={onPress}>
            <Image
              style={styles.closeImg}
              source={require('./../../assets/images/cross-icon.png')}
            />
          </TouchableHighlight>
        </View>

        {galleryData && galleryData.length > 0 ? (
          <AppText style={styles.txtCount}>
            {activeIndex + 1}/{galleryData.length}
          </AppText>
        ) : null}

        {/* <AppText style={styles.txtCount}>
          {dimensions.window.width}/{dimensions.window.height}
        </AppText> */}

        {galleryData && galleryData.length > 0 ? (
          <View style={styles.viewContainer}>
            <Carousel
              layout={'default'}
              ref={(ref) => (previewCarousel = ref)}
              data={galleryData}
              sliderWidth={windowWidth}
              sliderHeight={windowHeight / 4}
              itemWidth={windowWidth}
              itemHeight={windowHeight / 4}
              // sliderWidth={dimensions.window.width}
              // sliderHeight={dimensions.window.height}
              // itemWidth={dimensions.window.width}
              // itemHeight={dimensions.window.height}
              firstItem={activeIndex}
              renderItem={AppGalleryItem}
              onSnapToItem={(index) => setActiveIndex(index)}
            />
            {galleryData && galleryData.length > 1 && (
              <View style={[styles.btnArrow, styles.leftArrow]}>
                <TouchableWithoutFeedback
                  onPress={() => {
                    previewCarousel.snapToPrev(true);
                  }}>
                  <Image
                    style={styles.moveArrow}
                    source={require('./../../assets/images/prev-icon.png')}
                  />
                </TouchableWithoutFeedback>
              </View>
            )}
            {galleryData && galleryData.length > 1 && (
              <View style={[styles.btnArrow, styles.rightArraow]}>
                <TouchableWithoutFeedback
                  onPress={() => {
                    previewCarousel.snapToNext(true);
                  }}>
                  <Image
                    style={styles.moveArrow}
                    source={require('./../../assets/images/next-icon.png')}
                  />
                </TouchableWithoutFeedback>
              </View>
            )}
          </View>
        ) : (
          <View style={styles.viewContainer}>
            <AppText style={{color: colors.primary}}>No Data Available</AppText>
          </View>
        )}

        {galleryData &&
        galleryData[activeIndex] &&
        galleryData[activeIndex].field_caption ? (
          <AppText style={styles.txtCaption}>
            {galleryData[activeIndex].field_caption}
          </AppText>
        ) : null}
      </View>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  centeredView: {
    flex: 1,
    backgroundColor: colors.secondaryDark,
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 20,
    justifyContent: 'space-evenly',
  },
  title: {
    flex: 1,
    color: colors.primary,
    fontFamily: appFonts.SourceSansProSemiBold,
    fontSize: appFonts.xlargeFontSize,
    marginRight: 10,
  },
  closeImg: {width: 18, height: 18, margin: 8},
  txtCount: {
    paddingHorizontal: 20,
    marginBottom: 10,
    color: colors.primary,
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.xlargeFontSize,
  },
  txtCaption: {
    padding: 20,
    marginBottom: 10,
    color: colors.primary,
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.normalFontSize,
  },
  viewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnArrow: {
    width: 60,
    height: 60,
    position: 'absolute',
  },
  leftArrow: {
    left: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  rightArraow: {
    right: 5,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  moveArrow: {width: 24, height: 24},
});

export default GalleryModalScreen;
