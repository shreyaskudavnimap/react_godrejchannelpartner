import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  TextInput,
  FlatList,
} from 'react-native';

import AppText from '../../components/ui/ AppText';
import AppNote from '../../components/ui/AppNote';

import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import Screen from '../../components/Screen';
import AppNotePink from '../../components/ui/AppNotePink';

const windowWidth = Dimensions.get('window').width;

const AutoSearchInputModalScreen = ({
  onCancelPress,
  fieldData,
  searchData,
  onSelectPartnerData,
}) => {
  const [search, setSearch] = useState(searchData ? searchData : '');
  const [filteredDataSource, setFilteredDataSource] = useState(fieldData);
  const [masterDataSource, setMasterDataSource] = useState(fieldData);

  useEffect(() => {
    searchFilterFunction(search);
  }, []);

  const searchFilterFunction = (text) => {
    if (text) {
      const newData = masterDataSource.filter(function (item) {
        const itemData = item.name ? item.name.toUpperCase() : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
      setSearch(text);
    } else {
      setFilteredDataSource(masterDataSource);
      setSearch(text);
    }
  };

  const ItemView = ({item}) => {
    return (
      <TouchableWithoutFeedback onPress={() => getItem(item)}>
        <AppText style={styles.itemStyle}>{item.name}</AppText>
      </TouchableWithoutFeedback>
    );
  };

  const ItemSeparatorView = () => {
    return (
      <View
        style={{
          height: 0.5,
          width: windowWidth,
          backgroundColor: colors.gray5,
        }}
      />
    );
  };

  const getItem = (item) => {
    onSelectPartnerData(item.name);
  };

  const getItemTest = (item) => {
    onSelectPartnerData(item);
  };

  return (
    <Screen>
      <View style={styles.container}>
        <TouchableWithoutFeedback
        //  onPress={onCancelPress}
        onPress={() => getItemTest(search)}
         >
          <Image
            source={require('./../../assets/images/cancel.png')}
            style={styles.cancelIcon}
          />
        </TouchableWithoutFeedback>

        <TextInput
          style={styles.textInputStyle}
          onChangeText={(text) => searchFilterFunction(text)}
          value={search}
          underlineColorAndroid="transparent"
          placeholder="Name of the Channel Partner"
        />

        {filteredDataSource && filteredDataSource.length > 0 ? (
          <FlatList
            data={filteredDataSource}
            keyExtractor={(item, index) => index.toString()}
            ItemSeparatorComponent={ItemSeparatorView}
            renderItem={ItemView}
          />
        ) : // <AppText>No Data Available</AppText>
        null}
      </View>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: windowWidth,
    padding: 20,
    backgroundColor: colors.primary,
  },
  cancelIcon: {
    width: 16,
    height: 16,
    alignSelf: 'flex-end',
  },
  itemStyle: {
    paddingHorizontal: 10,
    paddingVertical: 15,
    fontSize: appFonts.largeFontSize,
  },
  textInputStyle: {
    height: 50,
    borderBottomWidth: 0.5,
    paddingLeft: 10,
    margin: 10,
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.largeFontSize,
    borderBottomColor: colors.jaguar,
    backgroundColor: colors.primary,
  },
});

export default AutoSearchInputModalScreen;
