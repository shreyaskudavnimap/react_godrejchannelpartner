import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  Modal,
  TouchableOpacity,
  FlatList,
  Image,
} from 'react-native';
import {Directions} from 'react-native-gesture-handler';
import {FilterType} from 'react-native-video';
import Screen from '../../components/Screen';
import AppText from '../../components/ui/ AppText';
import AppTextBold from '../../components/ui/AppTextBold';
import appFonts from '../../config/appFonts';
import colors from '../../config/colors';

const ReciptDownloadModal = ({header, content, prevstate, handleDownload}) => {
  const [reciptModalVisible, setReciptModalVisible] = useState(true);
  return (
    <Screen>
      <Modal
        visible={reciptModalVisible}
        animationType={'fade'}
        transparent={true}>
        <View style={styles.container}>
          <View style={styles.modalView}>
            <View style={styles.header}>
              <AppTextBold style={styles.textDesign}>{header}</AppTextBold>
              <TouchableOpacity style={{left: '200%'}} onPress={prevstate}>
                <Image
                  source={require('../../assets/images/cancel.png')}
                  style={styles.cancel}
                />
              </TouchableOpacity>
            </View>

            <View style={styles.textDesign1}>
              <AppText style={styles.textDesign2}>Total Invoice (A) </AppText>
              <AppText style={styles.textDesign2}>
                {'\u20B9'} {content.total_invoice_a}
              </AppText>
            </View>
            <View style={styles.textDesign1}>
              <AppText style={styles.textDesign2}>Total Payments (B) </AppText>
              <AppText style={styles.textDesign2}>
                {'\u20B9'} {content.total_payments_b}
              </AppText>
            </View>
            <View style={styles.textDesign1}>
              <AppText style={styles.textDesign2}>Total Interest </AppText>
              <AppText style={styles.textDesign2}>
                {'\u20B9'} {content.total_interest}
              </AppText>
            </View>
            <View style={styles.textDesign1}>
              <AppText style={styles.textDesign2}>Total Interest Paid </AppText>
              <AppText style={styles.textDesign2}>
                {'\u20B9'} {content.total_interest_paid}
              </AppText>
            </View>
            <View style={styles.textDesign1}>
              <AppText style={styles.textDesign2}>Adjustment </AppText>
              <AppText style={styles.textDesign2}>
                {'\u20B9'} {content.adjustment}
              </AppText>
            </View>
            <View style={styles.textDesign1}>
              <AppText style={styles.textDesign2}>
                Total Interest Payable(C){' '}
              </AppText>
              <AppText style={styles.textDesign2}>
                {'\u20B9'} {content.total_interest_payable_c}
              </AppText>
            </View>

            <View style={styles.textDesign1}>
              <AppText style={styles.textDesign2}>Tax on Interest(D) </AppText>
              <AppText style={styles.textDesign2}>
                {'\u20B9'} {content.tax_on_interest_d}
              </AppText>
            </View>
            <View style={styles.textDesign1}>
              <AppText style={styles.textDesign2}>
                Total Payable(A-B)+(C+D){' '}
              </AppText>
              <AppText style={styles.textDesign2}>
                {'\u20B9'} {content.total_payable}
              </AppText>
            </View>

            <View style={styles.continueBtn}>
              <AppText style={styles.textDesign4}>Interest Statement</AppText>
              <TouchableOpacity onPress={handleDownload}>
                <Image
                  source={require('../../assets/images/download-b.png')}
                  style={styles.icon}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.blurColor,
  },
  modalView: {
    margin: '5%',
    // marginTop: '40%',
    height: 'auto',
    width: '89%',
    backgroundColor: colors.milkWhite,
    borderRadius: 6,
    padding: '5%',
    paddingTop: '1%',
    paddingBottom: '5%',
    borderWidth: 3,
    borderColor: colors.borderGrey,
  },
  cancel: {
    width: 14,
    height: 14,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textDesign: {
    marginLeft: '4%',
  },
  textDesign1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textDesign2: {
    color: colors.gray3,
    fontFamily: appFonts.SourceSansProRegular,
    paddingTop: 8,
    paddingBottom: 8,
    marginTop: 2,
    display: 'flex',
    flexDirection: 'row',
    fontSize: appFonts.mediumFontSize,
  },
  continueBtn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 15,
  },
  icon: {
    width: 20,
    height: 20,
  },
  textDesign4: {
    fontWeight: 'bold',
  },
});
export default ReciptDownloadModal;
