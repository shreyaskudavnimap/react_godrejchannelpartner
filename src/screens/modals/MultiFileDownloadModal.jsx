import React  from 'react';
import {
  View,
  StyleSheet,
  Text,
  Modal,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
  Dimensions
} from "react-native";
import Screen from '../../components/Screen';
import AppText from '../../components/ui/ AppText';
import AppTextBold from '../../components/ui/AppTextBold'
import colors from '../../config/colors';
import Icon from 'react-native-vector-icons/MaterialIcons';
import appFonts from '../../config/appFonts';
const {height, width} = Dimensions.get('window');



const MultiFileDownloadModal = ({ modalData, setModalData, handleFileDownload, setIsPowerOfAttornyDraftModalOpen}) => (
    <Screen>
      <Modal visible={modalData.visible}
             animationType={'fade'}
             transparent={true} >

        <View style={styles.container}>
          <View style={styles.modalView}>
            <View style={styles.header}>
              <View style={styles.headerTextWrapper}>
                <AppTextBold>{modalData.title}</AppTextBold>
              </View>
              <View style={styles.closeIconWrapper}>
                <TouchableOpacity
                                  onPress={() => {
                                    setModalData({
                                    visible: false,
                                    title: '',
                                    content: [],
                                    })
                                    if(setIsPowerOfAttornyDraftModalOpen){
                                      setIsPowerOfAttornyDraftModalOpen(false);
                                    }
                                }}
                >
                <Icon name={'close'} size={25} color={colors.gray} />
                </TouchableOpacity>
            </View>
            </View>
            {modalData.content.map((elm, index) => (
              <TouchableWithoutFeedback
                key={index}
                onPress={() => {
                 if (elm.url) {
                 handleFileDownload(elm.url, false);
                   setModalData({
                     visible: false,
                     title: '',
                     content: [],
                   })
                 }
                }}>
                <View style={[styles.button, {marginHorizontal: 5}]}>
                  <AppText
                    style={styles.text}>
                    {elm.name}
                  </AppText>
                    <Image
                      source={require('../../assets/images/download-b.png')}
                      style={styles.subIcon}
                    />
                </View>
              </TouchableWithoutFeedback>
            ))}
          </View>
        </View>
      </Modal>
    </Screen>
  )
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.blurColor,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    paddingHorizontal: width * 0.03,
    paddingVertical: height * 0.03,

  },
  modalView: {
    minHeight: height * 0.25,
    width: width * 0.9,
    backgroundColor: colors.milkWhite,
    borderRadius: 6,
    paddingHorizontal: width * 0.03,
    paddingVertical: height * 0.03,
    borderWidth: 3,
    borderColor: colors.borderGrey
  },
  cancel: {
    fontSize: appFonts.xlargeFontSize,
    fontWeight: 'bold',
    marginTop: 10,
    color: colors.gray,
  },
  header: {
    flexDirection: "row",
    alignItems: 'center',
    alignSelf: 'stretch',
    marginBottom: 10
  },
  button: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'stretch',
    paddingHorizontal: 10,
  },
  text: {
    fontSize: appFonts.largeBold,
  },
  subIcon: {
    width: 14,
    height: 14,
  },
  closeIconWrapper: {flex: 1, alignItems: 'flex-end'},
  headerTextWrapper: {flex: 9, alignItems: 'center'}
})
export default MultiFileDownloadModal;
