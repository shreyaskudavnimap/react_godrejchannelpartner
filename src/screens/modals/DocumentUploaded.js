import React from 'react';
import {
    View,
    StyleSheet,
    Modal,
    Dimensions,
    Image,
    TouchableWithoutFeedback
} from 'react-native';
import Screen from '../../components/Screen';
import AppText from '../../components/ui/ AppText';
import AppTextBold from '../../components/ui/AppTextBold'
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
var { height, width } = Dimensions.get('window');

const DocumentUploaded = (props) => {
    return (
        <Screen>
            <Modal
                transparent={true}
                visible={true}
                animationType={'fade'}>
                <View style={styles.centeredView}>
                    <View style={styles.modal}>
                        <View style={styles.cancelContainer}>
                            <TouchableWithoutFeedback onPress={() => { props.func(false) }}>

                                <Image
                                    source={require('./../../assets/images/cancel.png')}
                                    style={styles.icon}
                                />

                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.content}>
                            <Image
                                source={require('./../../assets/images/tick-60x60.png')}
                                style={styles.image}
                            />
                            <AppText style={{fontSize:appFonts.normalFontSize,marginTop:5, textAlign: 'center'}}>{props.msg}</AppText>
                        </View>
                       
                    </View>
                </View>
            </Modal>
        </Screen>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.blurColor,
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        height: height,
        width: width,
        backgroundColor: colors.blurColor,
    },
    modal: {
        backgroundColor: "white",
        width: "90%",
        padding: 20,
        backgroundColor: colors.milkWhite,
        borderRadius: 6,
    },
    cancelContainer: {
        justifyContent: "flex-end",
        alignItems: "flex-end",
        marginTop: "5%",
    },
    icon: {
        width: 15,
        height: 15,
    },
    image: { height: 50, width: 50, marginBottom: 10, padding: 10 },
    content: { justifyContent: "center", alignItems: "center", padding: 5 }
})

export default DocumentUploaded