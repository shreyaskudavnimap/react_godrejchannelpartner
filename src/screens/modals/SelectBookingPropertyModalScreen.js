import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import AppText from '../../components/ui/ AppText';
import Screen from '../../components/Screen';
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const SelectBookingPropertyModalScreen = ({properties, onSelect, onClose}) => {
  return (
    <Screen style={styles.mainContainer}>
      <TouchableWithoutFeedback onPress={onClose}>
        <View style={{flex: 1}}>
          <View style={styles.container}>
            <ScrollView bounces={false}>
              <View>
                {properties &&
                  properties.length > 0 &&
                  properties.map((item, index) => (
                    <View key={index}>
                      <TouchableWithoutFeedback onPress={() => onSelect(item)}>
                        <View style={styles.itemContainerCity}>
                          <AppText style={styles.itemTxt}>{item.title}</AppText>
                        </View>
                      </TouchableWithoutFeedback>
                      <View style={styles.divider} />
                    </View>
                  ))}
              </View>
            </ScrollView>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Screen>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
  },
  container: {
    height: windowHeight / 2,
    marginTop: windowHeight * 0.18,
    backgroundColor: colors.LynxWhite,
    marginHorizontal: 20,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 3, height: 4},
    shadowOpacity: 0.9,
    shadowRadius: 3,
    elevation: 3,
  },
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
  },
  itemContainerCity: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 20,
  },
  itemTxt: {
    fontSize: appFonts.largeBold,
    color: colors.jaguar,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  itemTxt1: {
    fontSize: appFonts.largeBold,
    padding: 20,
    paddingLeft: 25,
    color: colors.jaguar,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  arrow: {height: 16, width: 16},
  checkbox: {height: 24, width: 24, marginRight: 15},
  divider: {backgroundColor: colors.gray5, height: 1},
});

export default SelectBookingPropertyModalScreen;
