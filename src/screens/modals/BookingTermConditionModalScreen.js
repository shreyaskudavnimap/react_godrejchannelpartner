import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
  useWindowDimensions,
} from 'react-native';
import HTML from 'react-native-render-html';

import AppButton from '../../components/ui/AppButton';
import appFonts from '../../config/appFonts';
import colors from '../../config/colors';
import AppText from '../../components/ui/ AppText';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const BookingTermConditionModalScreen = ({
  onCancel,
  onContinue,
  bookingTermsData,
}) => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.container}>
        <View style={styles.headerContainerImg}>
          <TouchableOpacity onPress={onCancel} style={{paddingTop:10, paddingBottom: 10, paddingLeft: 10}}>
            <View  >
            <Image
              source={require('./../../assets/images/cancel.png')}
              style={styles.iconCancel}
            />
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.headerContainer}>
          <AppText style={styles.headerText}>Terms &amp; Conditions</AppText>
        </View>

        <ScrollView bounces={false}>
          <View style={styles.contentViewContainer}>
            <HTML
              allowFontScaling={false}
              containerStyle={{color: colors.gray3}}
              listsPrefixesRenderers={{
                ol: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                  console.log('passProps: ', passProps);
                  return (
                    <AppText
                      style={{
                        color: colors.gray3,
                        fontSize: appFonts.normalFontSize,
                        fontFamily: appFonts.SourceSansProRegular,
                        marginTop: 6,
                        marginRight: 6,
                      }}>
                      {passProps && passProps.index
                        ? `${passProps.index + 1}. `
                        : '1. '}
                    </AppText>
                  );
                },
              }}
              tagsStyles={{
                p: {
                  fontSize: appFonts.normalFontSize,
                  lineHeight: 19,
                  textAlign: 'justify',
                  fontFamily: appFonts.SourceSansProRegular,
                  marginBottom: 13,
                  color: colors.gray3,
                },
                li: {
                  fontSize: appFonts.normalFontSize,
                  lineHeight: 19,
                  textAlign: 'justify',
                  fontFamily: appFonts.SourceSansProRegular,
                  color: colors.gray3,
                  marginTop: 5,
                },
              }}
              ignoredStyles={['font-family']}
              html={bookingTermsData}
              contentWidth={useWindowDimensions().width}
            />
          </View>
        </ScrollView>

        <View style={styles.btnContainer}>
          <AppButton title="CONTINUE" onPress={onContinue} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    height: windowHeight / 1.45,
    backgroundColor: colors.primary,
    padding: 25,
    marginHorizontal: 22,
    borderWidth: 1,
    borderRadius: 8,
    borderColor: colors.lightGray,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 25,
  },
  headerContainerImg: {
    alignItems: 'flex-end',
  },
  headerText: {
    flex: 1,
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProBold,
    textAlign: 'center',
  },
  iconCancel: {
    width: 14,
    height: 14,
    alignSelf: 'flex-end',
  },
  contentViewContainer: {marginVertical: 5},
  btnContainer: {
    marginTop: 25,
  },
});
export default BookingTermConditionModalScreen;
