import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  StyleSheet,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Image,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import VideoPlayer from 'react-native-video-player';
import Orientation from 'react-native-orientation';

import Screen from '../../components/Screen';
import colors from '../../config/colors';
import AppText from '../../components/ui/ AppText';
import appFonts from '../../config/appFonts';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const VideoModalScreen = ({
  onPress,
  projectName = 'Project Name',
  videoUri,
}) => {
  const [isVideoLoading, setIsVideoLoading] = useState(true);

  const videoRef = useRef();

  useEffect(()=> {
    Orientation.unlockAllOrientations();
  });

  return (
    <Screen style={styles.container}>
      <View style={styles.centeredView}>
        <View style={styles.titleContainer}>
          <AppText style={styles.title} numberOfLines={1}>
            {projectName}
          </AppText>
          <TouchableHighlight onPress={onPress}>
            <Image
              style={styles.closeImg}
              source={require('./../../assets/images/cross-icon.png')}
            />
          </TouchableHighlight>
        </View>

        <View style={styles.viewContainer}>
          <View
            style={{
              height: windowHeight / 4,
              width: windowWidth * 0.8,
              // backgroundColor: 'floralwhite',
            }}>
            {videoUri ? (
              <View>
                <VideoPlayer
                  ref={videoRef}
                  video={{
                    uri: videoUri,
                  }}
                  autoplay={true}
                  videoHeight={windowHeight / 4}
                  videoWidth={windowWidth * 0.8}
                  resizeMode="stretch"
                  loop={true}
                  defaultMuted={false}
                  onLoadStart={() => {
                    setIsVideoLoading(true);
                  }}
                  onLoad={() => {
                    setTimeout(() => {
                      setIsVideoLoading(false);

                      // if (
                      //   typeof videoRef != 'undefined' &&
                      //   videoRef &&
                      //   videoRef.current
                      // ) {
                      //   videoRef.current.onToggleFullScreen();
                      // }
                    }, 500);
                  }}
                />

                {isVideoLoading ? (
                  <View
                    style={{
                      height: '100%',
                      position: 'absolute',
                      left: 0,
                      right: 0,
                      top: 0,
                      bottom: 0,
                      backgroundColor: '#000000',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <ActivityIndicator
                      color={colors.primary}
                      size="large"
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    />
                  </View>
                ) : null}
              </View>
            ) : null}
          </View>
        </View>
      </View>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  centeredView: {
    flex: 1,
    backgroundColor: colors.secondaryDark,
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 20,
    justifyContent: 'space-evenly',
  },
  title: {
    flex: 1,
    color: colors.primary,
    fontFamily: appFonts.SourceSansProSemiBold,
    fontSize: appFonts.xlargeFontSize,
    marginRight: 10,
  },
  closeImg: {width: 18, height: 18, margin: 8},
  txtCount: {
    paddingHorizontal: 20,
    marginBottom: 10,
    color: colors.primary,
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.xlargeFontSize,
  },
  txtCaption: {
    padding: 20,
    marginBottom: 10,
    color: colors.primary,
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.normalFontSize,
  },
  viewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnArrow: {
    width: 60,
    height: 60,
    position: 'absolute',
  },
  leftArrow: {
    left: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  rightArraow: {
    right: 5,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  moveArrow: {width: 24, height: 24},
});

export default VideoModalScreen;
