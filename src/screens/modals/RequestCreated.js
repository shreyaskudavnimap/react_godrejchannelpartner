import React from 'react';
import {
    View,
    StyleSheet,
    Modal,
    Dimensions,
    Image,
    TouchableWithoutFeedback
} from 'react-native';
import Screen from '../../components/Screen';
import AppText from '../../components/ui/ AppText';
import AppTextBold from '../../components/ui/AppTextBold'
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';

var { height, width } = Dimensions.get('window');

const RequestCreated = (props) => {
    return (
        <Screen>
            <Modal
                transparent={true}
                visible={true}
                animationType={'fade'}>
                <View style={styles.centeredView}>
                    <View style={styles.modal}>
                        <View style={styles.cancelContainer}>
                            <TouchableWithoutFeedback onPress={() => { props.func(false) }}>

                                <Image
                                    source={require('./../../assets/images/cancel.png')}
                                    style={styles.icon}
                                />

                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.content}>
                            <View style={{ height: 80 }}>
                                <Image
                                    source={require('./../../assets/images/tick_big.png')}
                                    style={styles.image}
                                />
                            </View>
                            <AppText style={{fontSize: appFonts.normalFontSize, textAlign: 'center', marginBottom: 8}}> Your Service Request is created successfully!</AppText>
                            <AppText style={{fontSize: appFonts.largeFontSize, textAlign: 'center', fontFamily: appFonts.SourceSansProSemiBold}}>{props.msg}</AppText>
                        </View>

                    </View>
                </View>
            </Modal>
        </Screen>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.blurColor,
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        height: height,
        width: width,
        backgroundColor: colors.blurColor,
    },
    modal: {
        backgroundColor: "white",
        width: "90%",
        padding: 20,
        paddingTop: 10,
        backgroundColor: colors.milkWhite,
        borderRadius: 6,
    },
    cancelContainer: {
        justifyContent: "flex-end",
        alignItems: "flex-end",
        marginTop: "5%",
    },
    icon: {
        width: 15,
        height: 15,
    },
    image: { height: 51, width: 51, marginBottom: 10, padding: 10, },
    content: { justifyContent: "center", alignItems: "center", marginTop: "5%", padding: 5, marginBottom: "3%" }
})

export default RequestCreated