import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  TextInput,
  FlatList,
} from 'react-native';
import {SvgUri} from 'react-native-svg';

import AppText from '../../components/ui/ AppText';
import AppNote from '../../components/ui/AppNote';
// import appFonts from '../../config/appFonts';
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import Screen from '../../components/Screen';
import AppNotePink from '../../components/ui/AppNotePink';
import appCountry from '../../utility/appCountry';

const windowWidth = Dimensions.get('window').width;

const CountrySelectModalScreen = ({
  onCancelPress,
  searchData,
  onSelectCountry,
  isShowCountryCode= false
}) => {
  const [search, setSearch] = useState(searchData ? searchData : '');
  const [filteredDataSource, setFilteredDataSource] = useState(
    appCountry.COUNTRIES,
  );
  const [masterDataSource, setMasterDataSource] = useState(
    appCountry.COUNTRIES,
  );

  useEffect(() => {
    searchFilterFunction(search);
  }, []);

  const searchFilterFunction = (text) => {
    if (text) {
      const newData = masterDataSource.filter(function (item) {
        const itemData = item.name ? item.name.toUpperCase() : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
      setSearch(text);
    } else {
      setFilteredDataSource(masterDataSource);
      setSearch(text);
    }
  };

  const ItemView = ({item}) => {
    return (
      <TouchableWithoutFeedback onPress={() => getItem(item)}>
        <View style={{flexDirection: 'row', marginBottom: 4, alignItems: 'center'}}>
          <View
            style={{
              height: 54,
              width: 54,
            }}>
            <Image
              style={{height: '100%', width: '100%'}}
              source={{uri: item.flag}}
            />
          </View>
          <AppText style={styles.itemStyle}>{item.name}</AppText>
          {isShowCountryCode ? <AppText>{item.callingCodes}</AppText> : null}
        </View>
      </TouchableWithoutFeedback>
    );
  };

  const ItemSeparatorView = () => {
    return (
      <View
        style={{
          height: 0.5,
          width: windowWidth,
          backgroundColor: colors.gray5,
        }}
      />
    );
  };

  const getItem = (item) => {
    onSelectCountry(item);
  };

  return (
    <Screen>
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={onCancelPress}>
          <Image
            source={require('./../../assets/images/cancel.png')}
            style={styles.cancelIcon}
          />
        </TouchableWithoutFeedback>

        <TextInput
          style={styles.textInputStyle}
          onChangeText={(text) => searchFilterFunction(text)}
          value={search}
          underlineColorAndroid="transparent"
          placeholder="Search"
        />

        {filteredDataSource && filteredDataSource.length > 0 ? (
          <FlatList
            data={filteredDataSource}
            keyExtractor={(item, index) => index.toString()}
            ItemSeparatorComponent={ItemSeparatorView}
            renderItem={ItemView}
          />
        ) : (
          <AppText>No Data Available</AppText>
        )}
      </View>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: windowWidth,
    padding: 20,
    backgroundColor: colors.primary,
  },
  cancelIcon: {
    width: 16,
    height: 16,
    alignSelf: 'flex-end',
  },
  itemStyle: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 15,
    fontSize: appFonts.largeFontSize,
  },
  textInputStyle: {
    height: 50,
    borderBottomWidth: 0.5,
    paddingLeft: 10,
    margin: 10,
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.largeFontSize,
    borderBottomColor: colors.jaguar,
    backgroundColor: colors.primary,
  },
});

export default CountrySelectModalScreen;
