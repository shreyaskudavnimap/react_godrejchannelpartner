import React, { useState } from 'react';
import { View, StyleSheet, Text, Modal, TouchableOpacity, FlatList } from 'react-native';
import Screen from '../../components/Screen';
import AppButton from '../../components/ui/AppButton';
import AppTextBold from '../../components/ui/AppTextBold'
import appFonts from '../../config/appFonts';
import colors from '../../config/colors';


const TermConditionScreen = (props) => {

    const [modalVisible, setModalVisible] = useState(true)
    return (
        <Screen>
            <Modal visible={modalVisible}
                animationType={'fade'}
                transparent={true}
            >

                <View style={styles.container}>
                    <View style={styles.modalView}>


                        <TouchableOpacity style={styles.closeIcon} 
                             onPress={() =>{setModalVisible(!modalVisible)}}
                        >
                            <Text style={styles.cancel}>X</Text>

                        </TouchableOpacity>

                        <AppTextBold style={styles.textDesign}>{props.header}</AppTextBold>

                        <FlatList
                            data={props.content}
                            renderItem={({ item }) => (
                                <View style={styles.textDesign1}>
                                    <Text style={styles.textDesign2}>{item.id}  </Text>
                                    <Text style={styles.textDesign2}>{item.title}</Text>
                                </View>
                            )}
                        />

                     <View style={styles.continueBtn}>
                     <AppButton
                            title="CONTINUE"
                            onPress={() => {
                               setModalVisible(!modalVisible)
                            }}>
                        </AppButton>
                     </View>
                       

                    </View>
                </View>
            </Modal>
        </Screen>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.blurColor,
    },
    modalView: {
        margin: "5%",
        marginTop: "20%",
        height: "75%",
        width: "89%",
        backgroundColor: colors.milkWhite,
        borderRadius: 6,
        padding: "3%",
        alignItems: "center",
       
    },

    cancel: {
        fontSize: appFonts.xxlargeFontSize,
        color: colors.gray,
        marginLeft: "80%"
    },
    textDesign: {
        marginTop: "10%"
    },
    textDesign1: {
        marginLeft: "3%",
        marginRight: "10%",
        flexDirection: "row",
        textAlign: "justify",
        marginTop: 5,
    },
    textDesign2: {
        color: colors.gray3,
        fontFamily: appFonts.SourceSansProRegular
    },
   continueBtn: {
       marginBottom: "5%",
       width: "90%",
   }

})
export default TermConditionScreen;