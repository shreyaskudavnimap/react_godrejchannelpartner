import React from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';

import AppText from '../../components/ui/ AppText';
import AppNote from '../../components/ui/AppNote';

import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import Screen from '../../components/Screen';
import AppNotePink from '../../components/ui/AppNotePink';

const windowWidth = Dimensions.get('window').width;

const PaymentDetailModalScreen = ({paymentPlanDetails, cancel, onPress}) => {
  // console.log(paymentPlanDetails);
  return (
    <Screen>
      <ScrollView bounces={false} nestedScrollEnabled={true}>
        <View style={styles.container}>
          <View style={{paddingHorizontal: 20}}>
            <View
              style={
                cancel
                  ? styles.titleLine
                  : [styles.titleLine, {paddingVertical: 0}]
              }>
              {paymentPlanDetails && paymentPlanDetails.cip_pp_name ? (
                <AppText
                  style={[
                    styles.title,
                    {fontFamily: appFonts.SourceSansProSemiBold},
                  ]}>
                  {paymentPlanDetails.cip_pp_name
                    ? paymentPlanDetails.cip_pp_name
                    : ''}
                </AppText>
              ) : null}

              <TouchableWithoutFeedback onPress={onPress}>
                <Image
                  source={
                    cancel ? require('./../../assets/images/cancel.png') : null
                  }
                  style={styles.icon}
                />
              </TouchableWithoutFeedback>
            </View>

            {paymentPlanDetails &&
            paymentPlanDetails.messageAmount &&
            paymentPlanDetails.messageAmount != '' ? (
              <AppNotePink
                showNote={true}
                label={paymentPlanDetails.message}
                labelAmount={paymentPlanDetails.messageAmount}
              />
            ) : null}

            {paymentPlanDetails && paymentPlanDetails.sdr_status ? null : (
              <AppNote
                label={`Amounts shown above are exclusive of all Government Levies. Please select Payment Plan for GST details. Stamp Duty & Registration Charges extra.`}
              />
            )}

            <View style={styles.tableContainer}>
              <View style={styles.paymentContainer}>
                {paymentPlanDetails &&
                  paymentPlanDetails.plans &&
                  paymentPlanDetails.plans.length > 0 &&
                  paymentPlanDetails.plans.map((data, index) => (
                    <View
                      key={index}
                      style={[
                        styles.paymentTitle,
                        {
                          alignItems:
                            index === 0 ||
                            index === paymentPlanDetails.plans.length - 1
                              ? 'center'
                              : 'flex-start',
                          backgroundColor:
                            index === 0 ||
                            index === paymentPlanDetails.plans.length - 1
                              ? colors.LynxWhite
                              : data.completed === 'Y'
                              ? colors.completePayment
                              : colors.primary,
                        },
                      ]}>
                      <>
                        {index === 0 ||
                        index === paymentPlanDetails.plans.length - 1 ? (
                          <AppText
                            style={[
                              styles.titleTxt,
                              {
                                fontFamily:
                                  index === 0 ||
                                  index === paymentPlanDetails.plans.length - 1
                                    ? appFonts.SourceSansProSemiBold
                                    : appFonts.SourceSansProRegular,
                                fontSize:
                                  index === 0 ||
                                  index === paymentPlanDetails.plans.length - 1
                                    ? appFonts.largeFontSize
                                    : appFonts.normalFontSize,
                              },
                            ]}>
                            {data.title}
                          </AppText>
                        ) : (
                          <ScrollView
                            bounces={false}
                            nestedScrollEnabled={true}
                            contentContainerStyle={{
                              flexGrow: 1,
                              justifyContent: 'center',
                            }}
                            style={{
                              height: 90,
                            }}>
                            <View
                              style={{
                                paddingHorizontal: 8,
                                paddingVertical: 20,
                              }}>
                              <AppText
                                style={[
                                  styles.titleTxt,
                                  {
                                    fontFamily:
                                      index === 0 ||
                                      index ===
                                        paymentPlanDetails.plans.length - 1
                                        ? appFonts.SourceSansProSemiBold
                                        : appFonts.SourceSansProRegular,
                                    fontSize:
                                      index === 0 ||
                                      index ===
                                        paymentPlanDetails.plans.length - 1
                                        ? appFonts.largeFontSize
                                        : appFonts.normalFontSize,
                                  },
                                ]}>
                                {data.title}
                              </AppText>
                            </View>
                          </ScrollView>
                        )}
                      </>
                    </View>
                  ))}
              </View>
              <View style={{flex: 0.5}}>
                <ScrollView
                  horizontal={true}
                  bounces={false}
                  nestedScrollEnabled={true}
                  showsHorizontalScrollIndicator={false}>
                  <View>
                    {paymentPlanDetails &&
                      paymentPlanDetails.plans &&
                      paymentPlanDetails.plans.length > 0 &&
                      paymentPlanDetails.plans.map((data, index) => (
                        <View
                          key={index}
                          style={[
                            styles.paymentValue,
                            {
                              backgroundColor:
                                index === 0 ||
                                index === paymentPlanDetails.plans.length - 1
                                  ? colors.LynxWhite
                                  : data.completed === 'Y'
                                  ? colors.completePayment
                                  : colors.milkWhite,
                            },
                          ]}>
                          <View style={styles.valueBox}>
                            {index === 0 ? (
                              <AppText
                                style={[
                                  styles.titleTxt,
                                  {
                                    fontFamily:
                                      index === 0 ||
                                      index ===
                                        paymentPlanDetails.plans.length - 1
                                        ? appFonts.SourceSansProSemiBold
                                        : appFonts.SourceSansProRegular,
                                    fontSize:
                                      index === 0 ||
                                      index ===
                                        paymentPlanDetails.plans.length - 1
                                        ? appFonts.largeFontSize
                                        : appFonts.normalFontSize,
                                  },
                                ]}>
                                {'%'}
                              </AppText>
                            ) : (
                              <AppText
                                style={[
                                  styles.titleTxt,
                                  {
                                    fontFamily:
                                      index === 0 ||
                                      index ===
                                        paymentPlanDetails.plans.length - 1
                                        ? appFonts.SourceSansProSemiBold
                                        : appFonts.SourceSansProRegular,
                                    fontSize:
                                      index === 0 ||
                                      index ===
                                        paymentPlanDetails.plans.length - 1
                                        ? appFonts.largeFontSize
                                        : appFonts.normalFontSize,
                                  },
                                ]}>
                                {data.interest ? data.interest : ''}
                              </AppText>
                            )}
                          </View>
                          <View style={styles.valueBox}>
                            <AppText
                              style={[
                                styles.titleTxt,
                                {
                                  fontFamily:
                                    index === 0 ||
                                    index ===
                                      paymentPlanDetails.plans.length - 1
                                      ? appFonts.SourceSansProSemiBold
                                      : appFonts.SourceSansProRegular,
                                  fontSize:
                                    index === 0 ||
                                    index ===
                                      paymentPlanDetails.plans.length - 1
                                      ? appFonts.largeFontSize
                                      : appFonts.normalFontSize,
                                },
                              ]}>
                              {data.amount}
                            </AppText>
                          </View>
                          <View style={styles.valueBox}>
                            <AppText
                              style={[
                                styles.titleTxt,
                                {
                                  fontFamily:
                                    index === 0 ||
                                    index ===
                                      paymentPlanDetails.plans.length - 1
                                      ? appFonts.SourceSansProSemiBold
                                      : appFonts.SourceSansProRegular,
                                  fontSize:
                                    index === 0 ||
                                    index ===
                                      paymentPlanDetails.plans.length - 1
                                      ? appFonts.largeFontSize
                                      : appFonts.normalFontSize,
                                },
                              ]}>
                              {data.gst}
                            </AppText>
                          </View>
                          <View style={styles.valueBox}>
                            <AppText
                              style={[
                                styles.titleTxt,
                                {
                                  fontFamily:
                                    index === 0 ||
                                    index ===
                                      paymentPlanDetails.plans.length - 1
                                      ? appFonts.SourceSansProSemiBold
                                      : appFonts.SourceSansProRegular,
                                  fontSize:
                                    index === 0 ||
                                    index ===
                                      paymentPlanDetails.plans.length - 1
                                      ? appFonts.largeFontSize
                                      : appFonts.normalFontSize,
                                },
                              ]}>
                              {data.total}
                            </AppText>
                          </View>
                        </View>
                      ))}
                  </View>
                </ScrollView>
              </View>
            </View>

            {paymentPlanDetails && paymentPlanDetails.sdr_status ? null : (
              <AppNote
                label={
                  paymentPlanDetails.disclaimer
                    ? paymentPlanDetails.disclaimer
                    : `Exclusive of Stamp Duty & Registration Charges`
                }
              />
            )}
          </View>
        </View>
      </ScrollView>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    width: windowWidth,
    paddingVertical: 20,
    backgroundColor: colors.primary,
  },
  titleLine: {
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15,
  },
  title: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  tableContainer: {flexDirection: 'row', marginBottom: 12},
  paymentContainer: {
    flex: 0.5,
    borderRightWidth: 2,
    borderRightColor: colors.gray5,
  },
  value: {
    fontSize: appFonts.normalFontSize,
    color: colors.expandText,
    fontFamily: appFonts.SourceSansProSemiBold,
    lineHeight: 18,
  },
  titleTxt: {
    fontSize: appFonts.largeBold,
    color: colors.jaguar,
  },
  icon: {
    width: 16,
    height: 16,
  },
  paymentTitle: {
    height: 100,
    justifyContent: 'center',
    borderBottomWidth: 1,
    paddingHorizontal: 12,
    borderBottomColor: colors.gray5,
  },
  paymentValue: {
    height: 100,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: colors.gray5,
  },
  valueBox: {
    width: 135,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default PaymentDetailModalScreen;
