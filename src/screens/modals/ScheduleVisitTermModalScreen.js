import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import HTML from 'react-native-render-html';

import Screen from '../../components/Screen';
import AppButton from '../../components/ui/AppButton';
import appFonts from '../../config/appFonts';
import colors from '../../config/colors';
import AppText from '../../components/ui/ AppText';

const ScheduleVisitTermModalScreen = ({onCancel, onContinue, termsData}) => {
  return (
    <Screen>
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <AppText style={styles.headerText}>Terms &amp; Conditions</AppText>
          <TouchableOpacity onPress={onCancel}>
            <Image
              source={require('./../../assets/images/cancel.png')}
              style={styles.iconCancel}
            />
          </TouchableOpacity>
        </View>

        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.contentViewContainer}>
            <HTML
             allowFontScaling={false}
              tagsStyles={{
                p: {
                  fontSize: appFonts.normalFontSize,
                  lineHeight: 20,
                  textAlign: 'justify',
                  fontFamily: appFonts.SourceSansProRegular,
                  marginBottom: 12,
                },
                li: {
                  fontSize: appFonts.normalFontSize,
                  lineHeight: 20,
                  textAlign: 'justify',
                  fontFamily: appFonts.SourceSansProRegular,
                },
              }}
              ignoredStyles={['font-family']}
              html={termsData}
            />
          </View>
        </ScrollView>

        <AppButton title="CONTINUE" onPress={onContinue} />
      </View>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
    padding: 25,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 18,
  },
  headerText: {flex: 1, fontSize: appFonts.xlargeFontSize, fontFamily: appFonts.SourceSansProBold, textAlign:'center'},
  iconCancel: {
    width: 18,
    height: 18,
  },
  contentViewContainer: {marginVertical: 25},
});
export default ScheduleVisitTermModalScreen;
