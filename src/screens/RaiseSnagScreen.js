import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList
} from 'react-native';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import AppButton from '../components/ui/AppButton';
import colors from '../config/colors';
import appFonts from '../config/appFonts';
import AddSnagComponent from '../components/AddSnagComponent';
import apiRaiseasnag from '../api/apiRaiseasnag'
import { useSelector } from 'react-redux';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import SavedSnagList from '../components/SavedSnagList'
import Orientation from 'react-native-orientation';

const SnagList = [
  {
    id: '1',
    date: 'June 2020',
    subcategory: [
      {
        id: 'A',
        image: require('./../assets/images/no-image.jpg'),
        title: 'Master Bedroom/GODMERIT1-0703',
        Description: 'Water seepage on the seeling of the master bedroom',
      },
      {
        id: 'B',
        image: require('./../assets/images/no-image.jpg'),
        title: 'Kitchen/GODMERIT1-0703',
        Description: 'A tray in kitchen cabinet is broken and needs replacing',
      },
    ],
  },
  {
    id: '2',
    date: 'Jan 2020',
    subcategory: [
      {
        id: 'A',
        image: require('./../assets/images/no-image.jpg'),
        title: '111Kitchen/GODMERIT1-0703',
        Description: 'A tray in kitchen cabinet is broken and needs replacing',
      },
      {
        id: 'B',
        image: require('./../assets/images/no-image.jpg'),
        title: '111Master Bedroom/GODMERIT1-0703',
        Description: 'Water seepage on the seeling of the master bedroom',
      },
      {
        id: 'C',
        image: require('./../assets/images/no-image.jpg'),
        title: '111Kitchen/GODMERIT1-0703',
        Description: 'A tray in kitchen cabinet is broken and needs replacing',
      },
    ],
  },
];

const RaiseSnagScreen = ({ route, navigation }) => {
  const [isNewRequest, setIsNewRequest] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const loginvalue = useSelector((state) => state.loginInfo.loginResponse);

  const userId = loginvalue.data.userid;
  const bookingId = route.params ? route.params.bookingId : ''
  console.log(userId);

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  
  
  return (
    <Screen>
      <AppHeader />
      <ScrollView>
        <View style={styles.container}>
          <AppTextBold style={styles.pageTitle}>RAISE A SNAG</AppTextBold>

          <View style={styles.tabName}>
            <View
              style={
                isNewRequest
                  ? styles.activeTab
                  : [styles.activeTab, { borderBottomWidth: 0 }]
              }>
              <TouchableOpacity
                onPress={() => {
                  setIsNewRequest(true);
                }}>
                <AppText
                  style={
                    isNewRequest
                      ? { fontWeight: 'bold', fontSize: appFonts.largeBold }
                      : { fontSize: appFonts.largeBold }
                  }>
                  New Request
                </AppText>
              </TouchableOpacity>
            </View>

            <View
              style={
                !isNewRequest
                  ? styles.activeTab
                  : [styles.activeTab, { borderBottomWidth: 0 }]
              }>
              <TouchableOpacity
                onPress={() => {
                  setIsNewRequest(false);
                }}>
                <AppText
                  style={
                    !isNewRequest
                      ? { fontWeight: 'bold', fontSize: appFonts.largeBold }
                      : { fontSize:appFonts.largeBold }
                  }>
                  Snag List
                </AppText>
              </TouchableOpacity>
            </View>
          </View>
          {isNewRequest ? (
            <View>
              <AddSnagComponent bookingId={bookingId} userId={userId} setIsLoading={setIsLoading} />
            </View>
          ) : (
              <SavedSnagList bookingId={bookingId} userId={userId} setIsLoading={setIsLoading} navigation={navigation} />
            )}
        </View>
      </ScrollView>
      <AppFooter activePage={0} isPostSales={true} />
      <AppOverlayLoader isLoading={isLoading} />
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '6%',
    paddingBottom: '6%',
  },
  pageTitle: {
    fontSize: appFonts.xxlargeFontSize,
    color: colors.secondary,
    textTransform: 'uppercase',
  },
  tabName: {
    paddingBottom: '3%',
    marginBottom: '5%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  activeTab: {
    width: '50%',
    paddingVertical: '5%',
    borderBottomWidth: 3,
    borderBottomColor: colors.gray,
    alignItems: 'center',
  },
  snagInfo: {
    paddingHorizontal: '5%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    height: 40,
    alignItems: 'flex-end',
    borderBottomColor: colors.borderGrey,
    borderBottomWidth: 1,
    paddingBottom: 5,
    
  },
  snagInfoText: {
    fontSize: appFonts.largeFontSize,
    color: colors.gray4,
  },
  images: {
    height: 100,
    width: 100,
    marginLeft:20
  
  },
  snagItems: {
    paddingHorizontal: '7%',
    marginTop: '5%',
    flexDirection: 'row',
    justifyContent: 'center',
    width: '75%',
  },
  card: {
    alignItems: 'center',
    paddingBottom: '5%',
    marginBottom: 30,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  titleDesign: {
    fontFamily: appFonts.SourceSansProBold,
    fontSize: appFonts.largeFontSize,
  },
  descriptionDesign: {
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.largeFontSize,
  },
  editDeleteButton: {
    flexDirection: 'row',
  },
  btnText: {
    fontSize: appFonts.mediumFontSize,
    fontWeight: 'bold',
  },
  addItems: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '38%',
    marginBottom: '5%',
  },
  addIcon: {
    height: 40,
    width: 40,
  },
  addMoreItems: {
    marginLeft: '5%',
    fontFamily: appFonts.SourceSansProBold,
    fontSize: appFonts.largeBold,
  },
});
export default RaiseSnagScreen;