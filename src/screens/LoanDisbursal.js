import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppButton from '../components/ui/AppButton';
import BankPaymentSplitComponent from '../components/BankPaymentSplitComponent';
import { useNavigation } from '@react-navigation/native';
import apiLoanDisbursement from '../api/apiLoanDisbursement';
import appConstant from '../utility/appConstant';
import appSnakBar from '../utility/appSnakBar';
import { useSelector } from 'react-redux';
import Orientation from 'react-native-orientation';

const LoanDisbursal = (props) => {
  const bookingId = props.bookingId;
  const activeTab = props.activeTab;
  const navigation = useNavigation();
  const [totalPayableAmount, setTotalPayableAmount] = useState("0.00");
  const [bookingAccList, setBookingAccList] = useState([]);
  const [bankAccList, setBankAccList] = useState([]);
  const [totalPayment, setTotalPayment] = useState("0.00");
  const [pageLoad, isPageLoad] = useState(false);

  const loginvalue = useSelector(
    (state) => state.loginInfo.loginResponse,
  );
  var userId = typeof loginvalue.data !== 'undefined' ? loginvalue.data.userid : '';

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  useEffect(() => {
    checkBankDetailsExist();
    isPageLoad(true);
  }, [bookingId, activeTab]);

  const checkBankDetailsExist = async () => {
    props.setIsLoading(true);
    try {
      const requestData = { user_id: userId, booking_id: bookingId };
      const checkBankDetail = await apiLoanDisbursement.checkBankDetails(requestData);
      const bankDetailStatus = checkBankDetail.data.status;
      props.setIsLoading(false);
      if (bankDetailStatus == 200) {
        Alert.alert(
          "Loan Disbursement",
          "Please enter your loan details to enable us generate your Disbursement Request Letter",
          [
            {
              text: "Enter Loan Details",
              onPress: () => {
                props.setActiveTab("LoanDetails");
              },
            }
          ],
          { cancelable: false },
        );
      } else if (bankDetailStatus == 501) {
        Alert.alert(
          "Loan Disbursement",
          "Please enter any amount to process with Loan Disbursal",
          [
            {
              text: "OK",
              onPress: () => {
                getSingleAccountSummary();
              },
            }
          ],
          { cancelable: false },
        );
      } else {
        props.setActiveTab("LoanDetails");
      }

    } catch (error) {
      console.log(error);
      props.setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  }

  const getSingleAccountSummary = async () => {
    props.setIsLoading(true);
    try {
      const requestData = { user_id: userId, booking_id: bookingId, split_payment: "1" };
      const response = await apiLoanDisbursement.singleAccountSummary(requestData);
      // console.log(response.data);
      if (response.data.status == 200) {
        let returnedData = response.data.data || {};
        setTotalPayableAmount(returnedData.total_pay);
        if (
          returnedData.booking_items &&
          returnedData.booking_items.length > 0
        ) {
          const allBooking = returnedData.booking_items;
          const filterData = allBooking.filter(
            (x) => x.booking_id == bookingId
          );
          // console.log(filterData);

          let bankData = filterData[0].bank_details;
          let paymentAmount = 0;
          bankData.map((accItem, index) => {
            bankData[index].isChecked = true;
            let fieldAmount = accItem.due_amount > 0 ? accItem.due_amount.toFixed(2) : "";
            bankData[index].field_amount = fieldAmount;
            if (accItem.due_amount > 0) {
              paymentAmount = paymentAmount + parseFloat(accItem.due_amount);
            }
          });
          setTotalPayment(paymentAmount.toFixed(2));
          setBankAccList(bankData);
        }

      }
      props.setIsLoading(false);
    } catch (error) {
      console.log(error);
      props.setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  }
  // console.log("BankAccList", bankAccList);
  const requestDisbursal = async () => {
    // console.log("BankAccList", bankAccList);
    if (totalPayment > 0) {
      props.setIsLoading(true);
      try {
        const requestData = { user_id: userId, booking_id: bookingId };
        const checkBankDetail = await apiLoanDisbursement.checkBankDetails(requestData);
        const bankDetailStatus = checkBankDetail.data.status;
        props.setIsLoading(false);
        if (bankDetailStatus == 200) {
          Alert.alert(
            "Loan Disbursement",
            "Please enter your loan details to enable us generate your Disbursement Request Letter",
            [
              {
                text: "Enter Loan Details",
                onPress: () => {
                  props.setActiveTab("LoanDetails");
                },
              }
            ],
            { cancelable: false },
          );
        } else if (bankDetailStatus == 501) {
          proceedToDisbursal();
        } else {
          props.setActiveTab("LoanDetails");
        }

      } catch (error) {
        console.log(error);
        props.setIsLoading(false);
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }

    } else {
      Alert.alert(
        "Loan Disbursement",
        "Please enter any amount to process with Loan Disbursal",
        [
          {
            text: "OK",
            onPress: () => {

            },
          }
        ],
        { cancelable: false },
      );
    }
  }

  const proceedToDisbursal = async () => {
    props.setIsLoading(true);
    try {
      const requestData = { user_id: userId, booking_id: bookingId };
      const checkBankDetail = await apiLoanDisbursement.getBankDetails(requestData);
      const bankDetails = checkBankDetail.data;
      props.setIsLoading(false);
      if (bankDetails.status == "200") {
        if (bankDetails.bank_details[0].bank_name.toLowerCase() != 'hdfc bank') {
          appSnakBar.onShowSnakBar(
            'This payment disbursement feature is currently not available for your bank',
            'LONG',
          );
        } else if (bankDetails.bank_details[0].sanction_letter_url == '' || bankDetails.bank_details[0].sanction_letter_url == null) {
          appSnakBar.onShowSnakBar(
            'Please upload the sanction letter before you proceed with the Loan Disbursal process',
            'LONG',
          );
        } else {
          let splitPayment = [];
          bankAccList.map((bankItem, index) => {
            //  console.log("bankItem",bankItem);
            if (bankItem.isChecked) {
              if (
                bankItem.field_amount &&
                Number(bankItem.field_amount) > 0
              ) {
                let bankData = {
                  accountType: bankItem.accountType,
                  bankName: bankItem.bankName,
                  bank_name: bankItem.bank_name,
                  banks: bankItem.banks,
                  bussinessAssociate: bankItem.bussinessAssociate,
                  field_ac_number: bankItem.field_ac_number,
                  field_amount: Number(bankItem.field_amount),
                  field_favoring: bankItem.field_favoring,
                  ifsc_code: bankItem.ifsc_code,
                  merchant_id: bankItem.merchant_id,
                  projectPhase: bankItem.projectPhase,
                  sub_merchant_id: bankItem.sub_merchant_id,
                };
                splitPayment.push(bankData);
              }
            }
          });
          // console.log("splitPayment=====",splitPayment);
          navigation.navigate("LoanDisbursementForm", {
            booking_id: bookingId,
            payment_amount: totalPayment,
            split_payment: splitPayment,
          });
        }
      }

    } catch (error) {
      console.log(error);
      props.setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }

  }

  return (
    <>
      {!props.isLoading ? (
        <ScrollView>
          <View style={styles.container}>
            <View style={styles.payableContainer}>
              <AppText style={{ color: colors.gray3 }}>
                Total Payable Amount
        </AppText>
              <AppTextBold style={styles.amount}>
                {'\u20B9'}
                {totalPayableAmount}
              </AppTextBold>
            </View>

            <BankPaymentSplitComponent
              bankAccList={bankAccList}
              totalPayment={totalPayment}
              setBankAccList={setBankAccList}
              setTotalPayment={setTotalPayment}
              pageLoad={pageLoad} />

            <AppButton title="REQUEST FOR DISBURSAL" onPress={() => { requestDisbursal(); }} />
          </View>
        </ScrollView>
      ) : null}
    </>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '6%',
    justifyContent: 'center',
    marginBottom: 20,
  },
  pageTitle: {
    fontSize: appFonts.xxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    textTransform: 'uppercase',
  },
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderBottomColor: colors.gray4,
  },
  payableContainer: {
    // paddingVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  amount: {
    fontSize: appFonts.largeBold,
  },
  checkboxContainer: {
    flexDirection: 'row',
    paddingBottom: '5%',
    alignItems: 'center',
  },
  checkboxLabel: {
    color: colors.gray3,
    margin: 6,
  },
  checkbox: { height: 22, width: 22 },
});

export default LoanDisbursal;
