import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
  TextInput
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import AppButton from '../components/ui/AppButton';
import AppHeader from '../components/ui/AppHeader';
import AppTextBold from '../components/ui/AppTextBold';
import AppText from '../components/ui/ AppText';
import Screen from '../components/Screen';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppFooter from '../components/ui/AppFooter';
import * as normStyle from '../styles/StyleSize';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import apiLogin from './../api/apiLogin';
import ImagePicker from 'react-native-image-picker';
import { useIsFocused } from '@react-navigation/native';
import RNFetchBlob from 'rn-fetch-blob';
import AsyncStorage from '@react-native-async-storage/async-storage';
import appConstant from './../utility/appConstant.js';
import Orientation from 'react-native-orientation';
import DropDownPicker from 'react-native-dropdown-picker';
import * as userLoginAction from './../store/actions/userLoginAction';
import appSnakBar from '../utility/appSnakBar';
import CheckBox from '@react-native-community/checkbox';
import {CommonActions, StackActions} from '@react-navigation/native';

const { height } = Dimensions.get('window');

const form = {
  salutations: 'Mrs',
  FirstName: 'Arshi',
  LastName: 'Arbab',
  MobileNumber: '999999999',
  EmailID: 'arbabarshi@gmail.com',
  Password: '************',
};

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const EmpanelmentPan = (props) => {
  
  const [dob, SetDob] = useState('');
  
  const [verifyMobile, SetVerifyMobile] = useState(0);
  
  const [verifyEmail, SetVerifyEmail] = useState(0);
  const [permanentAddStr1, SetpermanentAddStr1] = useState('');
  const [permanentAddStr2, SetpermanentAddStr2] = useState('');
  const [permanentAddStr3, SetpermanentAddStr3] = useState('');
  const [permanentCity, SetpermanentCity] = useState('');
  const [permanentState, SetpermanentState] = useState('');
  const [permanentCountry, SetpermanentCountry] = useState('');
  const [permanentPincode, SetpermanentPincode] = useState('');
  const [presentAddStr1, SetpresentAddStr1] = useState('');
  const [presentAddStr2, SetpresentAddStr2] = useState('');
  const [presentAddStr3, SetpresentAddStr3] = useState('');
  const [presentCity, SetpresentCity] = useState('');
  const [presentState, SetpresentState] = useState('');
  const [presentCountry, SetpresentCountry] = useState('');
  const [presentPincode, SetpresentPincode] = useState('');
  const [userImage, SetUserImage] = useState('');
  const [userImageName, SetUserImageName] = useState('');
  const [userId, SetUserId] = useState('');
  const [isApiLoading, setApiIsLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [profileBase64, SetprofileBase64] = useState('');
  const [isCustomer, SetIsCustomer] = useState('');
  const isVisible = useIsFocused();
  const [isSameAddress, SetIsSameAddress] = useState(false)

  const dispatch = useDispatch();

  const [pan, SetPan] = useState('');

  const Submit = () => {
    StackActions.replace('EmpanelmentRequest');
  }

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
    if (isVisible) {
      _getUserInfo(false);
    }
  }, [loginvalue, isVisible]);

  const loginvalue = useSelector((state) => state.loginInfo.loginResponse);
  // console.log('loginvalue: ', loginvalue)
  const menuType = useSelector((state) => state.menuType.menuType);

  const _getUserInfo = async (isDispatch) => {
    //alert(appConstant.buildInstance.baseUrl+'update_user_api.json');
    if (loginvalue.isLogin) {
      var temp = loginvalue.data.is_customer;
      SetIsCustomer(temp);
    }
    var userid = '';
    if (loginvalue.isLogin) {
      userid = loginvalue.data.userid;
    }

    const request = {
      user_id: userid,
    };
    setIsLoading(true);
    const result = await apiLogin.apiForGetUserData(request);
    setIsLoading(false);
    console.log(result.data);
    var status = result.data.status;
    if (status == 200) {
      if (
        result.data.data[0].first_name != '' &&
        result.data.data[0].first_name != null &&
        result.data.data[0].last_name != '' &&
        result.data.data[0].last_name != null
      ) {
        _getImageName(
          result.data.data[0].first_name,
          result.data.data[0].last_name,
        );
      }
      if (
        result.data.data[0].first_name != '' &&
        result.data.data[0].first_name != null
      ) {
        SetFirstName(result.data.data[0].first_name);
      }
      if (
        result.data.data[0].last_name != '' &&
        result.data.data[0].last_name != null
      ) {
        SetlastName(result.data.data[0].last_name);
      }
      if (
        result.data.data[0].birth_dt != '' &&
        result.data.data[0].birth_dt != null
      ) {
        _convertBirth(result.data.data[0].birth_dt); //_convertBirth('1981-02-12');
      }
      if (
        result.data.data[0].mobile != '' &&
        result.data.data[0].mobile != null
      ) {
        SetMobile(result.data.data[0].mobile);
      }
      if (
        result.data.data[0].is_verified_mobile != '' &&
        result.data.data[0].is_verified_mobile != null
      ) {
        SetVerifyMobile(result.data.data[0].is_verified_mobile);
      }
      if (
        result.data.data[0].email != '' &&
        result.data.data[0].email != null
      ) {
        SetEmail(result.data.data[0].email);
      }
      if (
        result.data.data[0].is_verified_email != '' &&
        result.data.data[0].is_verified_email != null
      ) {
        SetVerifyEmail(result.data.data[0].is_verified_email);
      }
      if (
        result.data.data[0].field_permanent_address_street_1 != '' &&
        result.data.data[0].field_permanent_address_street_1 != null
      ) {
        SetpermanentAddStr1(
          result.data.data[0].field_permanent_address_street_1,
        );
      }
      if (
        result.data.data[0].field_permanent_address_street_2 != '' &&
        result.data.data[0].field_permanent_address_street_2 != null
      ) {
        SetpermanentAddStr2(
          result.data.data[0].field_permanent_address_street_2,
        );
      }
      if (
        result.data.data[0].field_permanent_address_street_3 != '' &&
        result.data.data[0].field_permanent_address_street_3 != null
      ) {
        SetpermanentAddStr3(
          result.data.data[0].field_permanent_address_street_3,
        );
      }
      if (
        result.data.data[0].field_permanent_city != '' &&
        result.data.data[0].field_permanent_city != null
      ) {
        SetpermanentCity(result.data.data[0].field_permanent_city);
      }
      if (
        result.data.data[0].field_permanent_state != '' &&
        result.data.data[0].field_permanent_state != null
      ) {
        SetpermanentState(result.data.data[0].field_permanent_state);
      }
      if (
        result.data.data[0].field_permanent_country != '' &&
        result.data.data[0].field_permanent_country != null
      ) {
        SetpermanentCountry(result.data.data[0].field_permanent_country);
      }
      if (
        result.data.data[0].field_permanent_pin_code != '' &&
        result.data.data[0].field_permanent_pin_code != null
      ) {
        SetpermanentPincode(result.data.data[0].field_permanent_pin_code);
      }
      if (
        result.data.data[0].field_present_address_street_1 != '' &&
        result.data.data[0].field_present_address_street_1 != null
      ) {
        SetpresentAddStr1(result.data.data[0].field_present_address_street_1);
      }
      if (
        result.data.data[0].field_present_address_street_2 != '' &&
        result.data.data[0].field_present_address_street_2 != null
      ) {
        SetpresentAddStr2(result.data.data[0].field_present_address_street_2);
      }
      if (
        result.data.data[0].field_present_address_street_3 != '' &&
        result.data.data[0].field_present_address_street_3 != null
      ) {
        SetpresentAddStr3(result.data.data[0].field_present_address_street_3);
      }
      if (
        result.data.data[0].field_present_city != '' &&
        result.data.data[0].field_present_city != null
      ) {
        SetpresentCity(result.data.data[0].field_present_city);
      }
      if (
        result.data.data[0].field_present_state != '' &&
        result.data.data[0].field_present_state != null
      ) {
        SetpresentState(result.data.data[0].field_present_state);
      }
      if (
        result.data.data[0].field_present_country != '' &&
        result.data.data[0].field_present_country != null
      ) {
        SetpresentCountry(result.data.data[0].field_present_country);
      }
      if (
        result.data.data[0].field_present_pin_code != '' &&
        result.data.data[0].field_present_pin_code != null
      ) {
        SetpresentPincode(result.data.data[0].field_present_pin_code);
      }
      if (
        result.data.data[0].user_image != '' &&
        result.data.data[0].user_image != null
      ) {
        SetUserImage(result.data.data[0].user_image);

        if (isDispatch) {
          dispatch(
            userLoginAction.updateUserImage(result.data.data[0].user_image),
          );
        }
      }
      if (
        result.data.data[0].user_id != '' &&
        result.data.data[0].user_id != null
      ) {
        SetUserId(result.data.data[0].user_id);
      }
    }
  };

  const _convertBirth = (dob) => {
    var date = new Date(dob);
    var dtArr = dob.split('-');
    SetDob(dtArr[2] + ' ' + monthNames[date.getMonth()] + ', ' + dtArr[0]);
  };

  const _getImageName = (fn, ln) => {
    SetUserImageName(
      fn.charAt(0).toUpperCase() + ' ' + ln.charAt(0).toUpperCase(),
    );
  };

  const _seprator = () => {
    return (
      <View style={styles.seprator}>
        <View style={styles.line}></View>
      </View>
    );
  };


  const onHandleRetryBtn = () => {
    if (isVisible) {
      _getUserInfo(false);
    }
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />

      <ScrollView>
        <View style={styles.container}>
          <AppText style={styles.pageTitle}>VALIDATE PAN</AppText>
          {/* 
<View style={styles.viewStyle}>
  <View style={styles.imageView1}>
    <View
      style={{
        flex: 0.8,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      {userImage ? (
        <Image
          source={{ uri: userImage }}
          style={styles.profileImg}
        //resizeMode='center'
        />
      ) : (
          <View
            style={[
              styles.profileImg,
              { alignItems: 'center', justifyContent: 'center' },
            ]}>
            <AppText style={styles.imageText}>{userImageName}</AppText>
          </View>
        )}
    </View>
    <View style={styles.editProfileStyle}>
      <TouchableOpacity
        onPress={() => {
          _edit('image');
        }}>
        <Image
          source={require('./../assets/images/camera-l-icon.png')}
          style={styles.cameraStyle}
        />
      </TouchableOpacity>
    </View>
  </View>
</View> */}

          {_seprator()}

          <View style={styles.formStyle}>
            <View style={styles.userViewContainer}>
              <AppText style={styles.userViewLabel}>Enter PAN Number *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={pan}
                onChangeText={e => {
                  SetPan(e);
                }}
              />
            </View>
          </View>

          {_seprator()}

          <AppButton
            title="VALIDATE"
            color="secondary"
            textColor="primary"
            onPress={() => {
              Submit()
            }}
          />

        </View>
      </ScrollView>
      <AppFooter
        activePage={0}
        isPostSales={menuType == 'postsale' ? true : false}
      />
      <AppOverlayLoader isLoading={isLoading || isApiLoading} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userViewContainer: { marginBottom: 25 },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: colors.gray4,
    fontSize: appFonts.largeBoldx,
    paddingVertical: 8,
    color: colors.jaguar,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  formStyle: {
    flex: 1,
    alignSelf: 'flex-start',
    width: '100%',
    marginTop: 10,
    marginBottom: 10,
    paddingHorizontal: 20,
  },
  pageTitle: {
    fontSize: normStyle.normalizeWidth(24),
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    textTransform: 'uppercase',
    marginLeft: normStyle.normalizeWidth(20),
  },
  nameStyle: {
    fontWeight: 'bold',
    fontSize: normStyle.normalizeWidth(16),
  },
  seprator: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  line: {
    width: '95%',
    height: normStyle.normalizeWidth(1),
    backgroundColor: colors.veryLightGray,
    marginTop: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(20),
  },
  viewStyle: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: normStyle.normalizeWidth(20),
  },
  imageView1: {
    width: normStyle.normalizeWidth(130),
    height: normStyle.normalizeWidth(105),
    flexDirection: 'row',
    paddingLeft: normStyle.normalizeWidth(20),
  },
  profileImg: {
    width: normStyle.normalizeWidth(100),
    height: normStyle.normalizeWidth(100),
    borderRadius: normStyle.normalizeWidth(50),
    backgroundColor: colors.veryLightGray,
  },
  imageText: {
    width: normStyle.normalizeWidth(110),
    height: normStyle.normalizeWidth(110),
    borderRadius: normStyle.normalizeWidth(55),
    textAlign: 'center',
    textAlignVertical: 'center',
    fontWeight: 'bold',
    fontSize: normStyle.normalizeWidth(30),
  },
  editImg: {
    width: normStyle.normalizeWidth(17),
    height: normStyle.normalizeWidth(17),
    marginLeft: normStyle.normalizeWidth(10),
  },
  editImg1: {
    width: normStyle.normalizeWidth(17),
    height: normStyle.normalizeWidth(17),
    marginRight: normStyle.normalizeWidth(10),
  },
  text1: {
    marginTop: normStyle.normalizeWidth(7),
  },
  view1: {
    width: '100%',
    flexDirection: 'row',
  },
  view2: {
    width: '100%',
    flexDirection: 'row',
    marginTop: normStyle.normalizeWidth(20),
    marginBottom: normStyle.normalizeWidth(20),
  },
  unverify: {
    marginLeft: normStyle.normalizeWidth(10),
    color: colors.danger,
  },
  verified: {
    color: colors.greenColor,
  },
  verifiedImg: {
    width: normStyle.normalizeWidth(15),
    height: normStyle.normalizeWidth(15),
    marginLeft: normStyle.normalizeWidth(5),
  },
  editProfileStyle: {
    flex: 0.2,
    bottom: 0,
    right: 0,
    position: 'absolute',
    paddingRight: normStyle.normalizeWidth(10),
  },
  cameraStyle: {
    width: normStyle.normalizeWidth(35),
    height: normStyle.normalizeWidth(35),
  },
  edit1: {
    flex: 0.2,
    alignItems: 'flex-end',
    paddingRight: '2.5%',
  },
  sectionTitle: {
    fontWeight: 'bold',
  }
});

export default EmpanelmentPan;
