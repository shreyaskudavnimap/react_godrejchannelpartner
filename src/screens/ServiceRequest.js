import React, { useEffect, useState, useRef } from 'react';
import {
	View,
	StyleSheet,
	TouchableOpacity,
	FlatList,
	Dimensions,
	TextInput,
	ScrollView,
	Image

}
	from 'react-native';
import Modal from 'react-native-modal';
import DropDownPicker from 'react-native-dropdown-picker';
import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppButton from '../components/ui/AppButton';
import RequestCreated from '../screens/modals/RequestCreated';
import apiservicerequest from '../api/apiservicerequest'
import appSnakBar from '../utility/appSnakBar';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import AppFileChooser from './../components/actionSheet/AppFileChooser';
import appFileChooser from './../utility/appFileChooser';
import appConstant from '../utility/appConstant';
import ServiceRequestDetails from './ServiceRequestDetails'
import { useSelector } from 'react-redux';
import AppTextBoldSmall from '../components/ui/AppTextBoldSmall';
import { fn } from 'moment';
import Orientation from 'react-native-orientation';


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;



const ServiceRequest = (props) => {
	const [isLoading, setIsLoading] = useState(false);
	const routeParams = props.route.params;
	const tabIndex = routeParams && routeParams.tabIndex ? routeParams.tabIndex : 1;
	const [isViewIndex, setViewIndex] = useState(tabIndex);
	const [modalView, setModalview] = useState(false);
	const [statusOpen, setStatusOpen] = useState([]);
	const [statusClosed, setStatusClosed] = useState([]);
	const [showOpenrequest, setShowOpenrequest] = useState(false);
	const [showCloserequest, setShowCloserequest] = useState(false);
	const [requetdetail, setRequetdetail] = useState([]);
	const [createRequestResponse, setCreateRequestResponse] = useState([]);
	const [flatCode, setFlatCode] = useState({});
	const [isflatCodeSelected, setisFlatCodeSelected] = useState(false);
	const [searchdata, setSearchdata] = useState([]);
	const [showSearchdata, setshowSearchdata] = useState([]);
	const [isSearching, setisSearching] = useState(false);
	const [fileChooserResponse, setFileChooserResponse] = useState(null);
	const [documentforcreaterequest, setDocumentforcreaterequest] = useState([]);


	const [description, setDescription] = useState('');
	const [filesToUpload, setFilesToUpload] = useState([]);
	const [fileChooser, setFileChooser] = useState(false);
	const inputreset = useRef();


	const closeActionSheet = () => setFileChooser(false);

	const loginvalue = useSelector((state) => state.loginInfo.loginResponse);
	const dashboardData = useSelector((state) => state.dashboardData);
	const propertyData = dashboardData.rmData;
	//const userId = loginvalue.data.userid;
	//console.log(userId);
	const userId = loginvalue && loginvalue.data && loginvalue.data.userid
		? loginvalue.data.userid
		: '';

	useEffect(() => {
		Orientation.lockToPortrait();
	}, []);


	useEffect(() => {
		initialcalls();
	
	}, [])

	// apicalls
	const initialcalls = () => {
		// api
		api_FLAT_CODE();
		api_SERVICE_REQUEST_CLOSED();
		api_SERVICE_REQUEST_OPEN();

		setShowOpenrequest(true);
		setShowCloserequest(true);
	}
	const Property_code = [];
	const Booking_id = [];
	const api_FLAT_CODE = async () => {
		try {
			setIsLoading(true);
			const requestData = { user_id: userId };
			const reqdata = await apiservicerequest.api_FLAT_CODE(requestData);
			setFlatCode(reqdata.data);



			setIsLoading(false);
		} catch (error) {
			console.log(error)
			setIsLoading(false);
			appSnakBar.onShowSnakBar(
				appConstant.appMessage.APP_GENERIC_ERROR,
				'LONG',
			);
		}

	};

	const splitt = (flatdata) => {

		var res = flatdata.split("_");

		return res;
	}
	var flatcodeat0 = "";
	for (let invent_id in flatCode.data_new) {
		let Code = flatCode.data_new[invent_id];
		let res = splitt(Code);
		Booking_id.push({ id: res[1] })

		Property_code.push({ label: res[0], value: invent_id });
		flatcodeat0 = Property_code[0].label;
	}


	var inventory_id = "";
	var booking_id = ""
	const onselect_property_code = (text) => {
		inventory_id = text;
		for (let x in Property_code) {
			if (text == Property_code[x].value) {
				booking_id = Booking_id[x].id;

			}

		}

		console.log(JSON.stringify(booking_id) + "hey this selected booking id")

	}

	var invent_idatzeroindex = {};
	for (const key in Property_code) {
		if (key == 0) {
			invent_idatzeroindex = Property_code[key];


		}
	}

	var booking_idatzeroindex = {};
	for (const key in Booking_id) {
		if (key == 0) {
			booking_idatzeroindex = Booking_id[key].id;
			console.log(JSON.stringify(booking_idatzeroindex) + "booking id at zero index")
		}
	}

	const onClickFileChooserItem = async (clickItem) => {
		switch (clickItem) {
			case 'Remove':
				setFileChooser(false);
				setFileChooserResponse(null);
				break;

			case 'Gallery':
				appFileChooser.onLaunchImageLibrary((fileChooserResponse) => {
					setFileChooser(false);
					if (fileChooserResponse && fileChooserResponse.uri) {
						appFileChooser.toBase64(fileChooserResponse.uri).then((resp) => {
							base64(resp, fileChooserResponse.fileName ? fileChooserResponse.fileName : appConstant.getFileName(fileChooserResponse.uri));

						}).catch((err) =>
							console.log(err)
						);
					}
				});
				break;

			case 'Camera':
				appFileChooser.onLaunchCamera((fileChooserResponse) => {
					setFileChooser(false);
					if (fileChooserResponse && fileChooserResponse.uri) {
						appFileChooser.toBase64(fileChooserResponse.uri).then((resp) => {
							base64(resp, fileChooserResponse.fileName ? fileChooserResponse.fileName : appConstant.getFileName(fileChooserResponse.uri));

						}).catch((err) =>
							console.log(err)
						);
					}
				});
				break;

			case 'Phone':
				try {
					const chooserResponse = await appFileChooser.onSelectFile();
					const data = await appFileChooser.toBase64(chooserResponse.uri);
					setFileChooser(false);
					base64(data, chooserResponse.name)


					console.log(updateResponse);
				} catch (error) {
					setFileChooser(false);
					console.log('Error onSelectFile : ', error);
				}
				break;
		}

	};
	const resp = "";

	const name = "";
	const base64 = (resp, name) => {
		setIsLoading(true)
		resp = resp;
		name = name;
		let files = filesToUpload;
		files.push({ "file_base64_encoded": resp, "file_name": name })
		setFilesToUpload(files);
		setIsLoading(false)
	}


	const api_CREATE_SERVICE_REQUEST = async () => {
		setIsLoading(true);
		try {
			const requestData = { "user_id": userId, "inventory_id": (isflatCodeSelected) ? inventory_id : invent_idatzeroindex.value, "description": description, "subject": "Service Request - Mobile App", "booking_id": (isflatCodeSelected) ? booking_id : booking_idatzeroindex, "files_attached": filesToUpload };
			const reqdata = await apiservicerequest.api_CREATE_SERVICE_REQUEST(requestData);
			console.log(requestData);
			// {"user_id":"546797","inventory_id":"262551","description":"Test SR","subject":"Service Request - Mobile App","booking_id":"288205"}


			// , "file_attached": [{ "file_base64_encoded": resp, "file_name": name }]
			if (reqdata.data.status == 200) {
				setCreateRequestResponse(reqdata.data);
				setModalview(true);
				setDescription('');
				setFilesToUpload([]);
				inputreset.current.clear()
			}
			else {
				appSnakBar.onShowSnakBar(
					reqdata.data.msg
				);
			}
			console.log(reqdata.data)
			console.log("====>uploaded")
			setIsLoading(false);
		} catch (error) {
			console.log(error);
			setIsLoading(false);
			appSnakBar.onShowSnakBar(
				appConstant.appMessage.APP_GENERIC_ERROR,
				'LONG',
			);
		}

	};


	const api_SERVICE_REQUEST_CLOSED = async () => {
		try {
			const requestData = { user_id: userId, status: "closed" };
			const reqdata = await apiservicerequest.api_SERVICE_REQUEST_CLOSED(requestData);
			setStatusClosed(reqdata.data.data)
		} catch (error) {
			console.log(error);
			setIsLoading(false);
			appSnakBar.onShowSnakBar(
				appConstant.appMessage.APP_GENERIC_ERROR,
				'LONG',
			);
		}
	};

	const api_SERVICE_REQUEST_OPEN = async () => {
		try {
			const requestData = { user_id: userId, status: "open" };
			const reqdata = await apiservicerequest.api_SERVICE_REQUEST_OPEN(requestData);
			setStatusOpen(reqdata.data.data);
		} catch (error) {
			console.log(error);
			setIsLoading(false);
			appSnakBar.onShowSnakBar(
				appConstant.appMessage.APP_GENERIC_ERROR,
				'LONG',
			);
		}
	};

	const Request = [
		{ label: 'All Requests', value: 'All Requests' },
		{ label: 'Open Requests', value: 'Open Requests' },
		{ label: 'Closed Requests', value: 'Closed Requests' },
	]
	for (const key in Request) {
		if (key == 0) {
			var Requestoptionat0 = Request[key].label
		}
	}


	const onselect_request_status = (text) => {
		setIsLoading(true)
		if (text == Request[1].value) {

			setShowCloserequest(false);
			setShowOpenrequest(true);

		}
		if (text == Request[2].value) {
			setShowCloserequest(true);
			setShowOpenrequest(false);

		}
		if (text == Request[0].value) {

			setShowOpenrequest(true);
			setShowCloserequest(true);

		}
		setIsLoading(false);

	}



	const onsearch = (text) => {
		const filteredList = [...statusOpen, ...statusClosed];
		if (text) {

			// Inserted text is not blank
			// Filter the masterDataSource and update FilteredDataSource
			const newData = filteredList.filter(
				function (item) {
					// Applying filter for the inserted text in search bar
					const itemData = item.title
						? item.title.toUpperCase()
						: ''.toUpperCase();
					const textData = text.toUpperCase();
					return itemData.indexOf(textData) > -1;
				}
			);
setshowSearchdata(newData)
			console.log(showSearchdata)
			setisSearching(true);
		} else {

			setshowSearchdata(searchdata);
			setisSearching(false);
		}

	}

	const RequestDetail = (item, status) => {
		if (status == "0") {
			for (let x in statusOpen) {
				if (statusOpen[x].id == item) {
					setRequetdetail(statusOpen[x]);
					props.navigation.navigate('ServiceRequestDetails', { requetdetail: statusOpen[x] })
				}
			}
		}
		if (status == "1") {
			for (let i in statusClosed) {
				if (statusClosed[i].id == item) {
					setRequetdetail(statusClosed[i]);
					props.navigation.navigate('ServiceRequestDetails', { requetdetail: statusClosed[i] })
				}
			}
		}


	}


	const onHandleRetryBtn = () => {
		initialcalls();
	};
	/**
	 * 
	 * @description Set current flat code  
	 */
	const setDefaultProperty = () => {
		const { route } = props;
		const currentFlatCode = route.params ? route.params.flatCode : null;

		if (currentFlatCode && flatCode.data) {

			const currentProperty = Object.keys(flatCode.data).filter((key) => {
				return flatCode.data[key] == currentFlatCode
			});
			if (currentProperty.length) {
				return currentProperty[0]
			}
			return null
		}
		else {
			if (Property_code.length) {
				return Property_code[0].value;
			}
			return null
		}
	}

	return (
		<Screen onRetry={onHandleRetryBtn}>
			<AppHeader />
			<ScrollView>
				<View style={styles.container}>
					<View style={styles.titleContainer}>
						<AppTextBold style={styles.pageTitle}>
							Write to Us
                </AppTextBold>

					</View>
					{isViewIndex != 3 &&
						<View>

							<View style={styles.tabContainer}>
								<TouchableOpacity
									activeOpacity={
										0.8
									}
									style={
										isViewIndex == 1
											? styles.buttonPlanSelected
											: styles.buttonPlan
									}
									onPress={() => {
										setViewIndex(1)
									}}>
									<AppText
										style={

											isViewIndex == 1
												? styles.textPlanSelected
												: styles.textPlan
										}>
										New Request
                              </AppText>
								</TouchableOpacity>
								<TouchableOpacity
									activeOpacity={
										0.8
									}
									style={
										isViewIndex == 2
											? styles.buttonPlanSelected
											: styles.buttonPlan
									}
									onPress={() => {
										setViewIndex(2)
										api_SERVICE_REQUEST_CLOSED();
										api_SERVICE_REQUEST_OPEN();
									}}>
									<AppText
										style={
											isViewIndex == 2
												? styles.textPlanSelected
												: styles.textPlan
										}>
										Existing Requests </AppText>
								</TouchableOpacity>
							</View>
							<View>
								{isViewIndex == 2
									&& <View>
										<TextInput
											placeholder="Search by SR number"
											style={styles.search}
											onChangeText={(text) => onsearch(text)}
										/>

										<View style={{ justifyContent: "center" }, styles.dropDownContainer}>
											<DropDownPicker
												items={Request}
												autoScrollToDefaultValue={true}
												style={styles.dropdownDocument}
												placeholderStyle={styles.dropdownPlaceholder}
												itemStyle={styles.dropDownItem}
												labelStyle={styles.label}
												dropDownMaxHeight={400}
												
												placeholder={Requestoptionat0}
												onChangeItem={(text) => onselect_request_status(text.value)}


											/>

										</View>
										
										<View>
											{isSearching==false?
											<FlatList
												data={showOpenrequest ? statusOpen : statusClosed}
												renderItem={({ item }) =>
													<TouchableOpacity onPress={(event) => RequestDetail(item.id, item.status)}>
														<View style={styles.Listitem}>
															<View style={{ width: windowWidth * 0.7 }}>
																<AppTextBold>#{item.title}</AppTextBold>
																<AppText>{item.subject}</AppText>
															</View>
															<AppText style={{ marginTop: 10 }}>{item.status_readable}</AppText>
														</View>
													</TouchableOpacity>
												}
											/>
									:
									showSearchdata!=""?  
											<FlatList
												data={ showSearchdata }
												renderItem={({ item }) =>
													<View style={styles.Listitem}>
														<View>
															<AppTextBold>#{item.title}</AppTextBold>
															<AppText>{item.subject}</AppText>
														</View>
														<TouchableOpacity onPress={(event) => RequestDetail(item.id, item.status)}>
															<AppText>{item.status_readable}</AppText>
														</TouchableOpacity>
													</View>}
											/> :<AppText style={{textAlign:'center'}}>No Record Found</AppText>
										}
										</View>		

									</View>
								}

								{isViewIndex == 1 && <View>

									<View style={{ zIndex: 90 }, styles.dropDownContainer}>
										<AppText style={{ marginTop: "5%", marginHorizontal: 5 }}> Flat Code  </AppText>

										{Property_code.length > 1 ?
											<DropDownPicker
												defaultValue={setDefaultProperty()}
												items={Property_code}
												style={{ marginBottom: "10%" }}
												autoScrollToDefaultValue={true}
												style={styles.dropdownDocument}
												placeholderStyle={styles.dropdownPlaceholder}
												itemStyle={styles.dropDownItem}
												labelStyle={styles.label}
												showArrow={Property_code.length > 1 ? true : false}
												onChangeItem={(text) => onselect_property_code(text.value)}
												placeholder={invent_idatzeroindex.label}
											/>
											: <AppText style={{ ...styles.dropdownDocument, marginLeft: 10 }}>{flatcodeat0}</AppText>}
									</View>
									<View style={{ marginBottom: "5%", marginTop: "8%" }}>
										<AppText>  Description</AppText>
									</View>

									<View>
										<TextInput
											style={styles.textArea}
											underlineColorAndroid="transparent"
											placeholder="write your description"
											numberOfLines={10}
											multiline={true}
											ref={inputreset}
											onChangeText={(description) => setDescription(description)}
										/>



										<View style={styles.attachfilebutton}>
											<AppText style={{ margin: 10 }} >
												Attach file
												</AppText>
											<TouchableOpacity style={styles.attachfilebutton} onPress={() => setFileChooser(true)}>

												<Image
													style={styles.iconAdd}
													source={require('../assets/images/attachment-icon.png')} />
											</TouchableOpacity>

										</View>

										{filesToUpload.map((item, index) => {
											return <View style={{ paddingVertical: 10 }} key={index}>
												<AppTextBoldSmall > {item.file_name}</AppTextBoldSmall>
											</View>
										})}

									</View>

									<View style={styles.appButton}>
										<AppButton title="SUBMIT" onPress={api_CREATE_SERVICE_REQUEST} />


									</View>
								</View>

								}

							</View>
						</View>
					}
				</View>
			</ScrollView>
			{modalView && <RequestCreated status={createRequestResponse.status} msg={createRequestResponse.msg} func={setModalview} />}
			<AppFooter activePage={0} isPostSales={true} />
			<AppOverlayLoader isLoading={isLoading} />
			<Modal
				isVisible={fileChooser}
				style={{
					margin: 0,
					justifyContent: 'flex-end',
				}}
				onBackButtonPress={closeActionSheet}
				onBackdropPress={closeActionSheet}
				backdropColor="rgba(0,0,0,0.5)">
				<AppFileChooser
					onPress={(clickItem) => onClickFileChooserItem(clickItem)}
				/>
			</Modal>
		</Screen>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: 20,
		paddingVertical: 10,
	},
	open: {
		flex: 1,
		justifyContent: "flex-start",
		alignItems: "flex-end"
	},
	pageTitle: {
		fontSize: appFonts.xxxlargeFontSize,
		fontFamily: appFonts.SourceSansProBold,
		color: colors.jaguar,
		textTransform: 'uppercase',
	},
	titleContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	iconAdd: {
		height: 25,
		width: 25,
		marginHorizontal: 2,
		marginBottom: 1
	},

	dropdownDocument: {
		height: 50,
		marginTop: 15,
		marginBottom: 8,
		borderRightWidth: 0,
		borderLeftWidth: 0,
		borderTopWidth: 0,
		justifyContent: 'center',
		alignItems: 'center',
	},
	dropdownPlaceholder: {
		fontSize: appFonts.largeFontSize,
		fontFamily: appFonts.SourceSansProRegular,
	},
	labelSelected: {
		fontSize: appFonts.largeFontSize,
		fontFamily: appFonts.SourceSansProSemiBold,
		marginBottom: 8,
	},
	dropdownPlaceholder: {
		fontSize: appFonts.largeFontSize,
		fontFamily: appFonts.SourceSansProRegular,
	},
	label: {
		fontSize: appFonts.largeFontSize,
		fontFamily: appFonts.SourceSansProRegular,
		marginBottom: 8,
		textAlign: 'left',
	},
	dropDownItem: {
		borderBottomWidth: 1,
		borderBottomColor: colors.lightGray,
		marginBottom: 8,
		marginTop: 4,
		justifyContent: 'flex-start',
	},
	tabContainer: {
		flexDirection: "row",
		width: "100%",
		marginTop: 10
	},
	search: {
		width: "100%",
		height: windowHeight * 0.07,
		backgroundColor: colors.LynxWhite,
		marginTop: 15,
		padding: 10,
		marginBottom: 5
	},
	buttonPlan: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		paddingHorizontal: 22,
		paddingVertical: 8,
		flexDirection: 'row',
		borderBottomColor: colors.lightGray,
		borderBottomWidth: 1,
		backgroundColor: colors.primary,
		marginVertical: 6,
	},
	textPlan: {
		fontSize: appFonts.normalFontSize,
		fontFamily: appFonts.SourceSansProRegular,
		color: colors.jaguar,
	},
	buttonPlanSelected: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		paddingHorizontal: 22,
		paddingVertical: 8,
		flexDirection: 'row',
		borderBottomColor: colors.jaguar,
		borderBottomWidth: 3,
		backgroundColor: colors.primary,
		marginVertical: 6,
	},
	textPlanSelected: {
		fontSize: appFonts.normalFontSize,
		fontFamily: appFonts.SourceSansProBold,
		color: colors.jaguar,
	},
	textArea: {
		minHeight: 100,
		justifyContent: "flex-start",
		backgroundColor: colors.LynxWhite,
		textAlignVertical: "top",
		padding: 10
	},
	appButton: {
		width: windowWidth * 0.92,
		alignSelf: 'center',
		marginTop: "8%",
		marginBottom: "5%"
	},
	textAreaContainer: {
		width: '98%',
		padding: 8,
		marginTop: 5,
		marginBottom: 25,
		marginLeft: "2%",
		borderWidth: 1,
		borderRadius: 1,
		borderColor: colors.lightGray,
		borderBottomWidth: 1,
		shadowColor: colors.lightGray,
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.7,
		shadowRadius: 3,
		elevation: 3,
		backgroundColor: colors.primary,
	},
	Listitem: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 8,
	},
	documentDoc: {
		flex: 1,
		flexDirection: "row",
		marginTop: "10%",
		marginBottom: "5%",
	},
	iconImg: {
		height: 30,
		width: 25,
	},
	iconDownload: {
		height: 18,
		width: 18,
		top: 5,
	},
	textContainer: {
		width: "82%",
		marginHorizontal: 10
	},
	listcontainer: {
		marginTop: 5,
		marginBottom: 5
	},
	attachfilebutton: {
		marginTop: 3,
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	dropDownContainer: {
		...Platform.select({ ios: { zIndex: 1000 } })

	}

})

export default ServiceRequest;