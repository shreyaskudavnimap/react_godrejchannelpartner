import React, { useState, useEffect } from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  Modal,
} from 'react-native';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import AppExpandButton from '../components/ui/AppExpandButton';
import BookingPropertyComponent from '../components/BookingPropertyComponent';
import CostSheetComponent from '../components/CostSheetComponent';
import ApplicantDetailsComponent from '../components/ApplicantDetailsComponent';
import PaymentDetailModalScreen from './modals/PaymentDetailModalScreen';
import SelectBookingPropertyModalScreen from './modals/SelectBookingPropertyModalScreen';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';

import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppBookingPropertyDetails from '../components/ui/AppBookingPropertyDetails';
import EOIBookingPropertyComponent from '../components/EOIBookingPropertyComponent';

import apiOffers from '../api/apiOffers';
import apiInvBookingJourney from '../api/apiInvBookingJourney';
import apiEoiBookingJourney from '../api/apiEoiBookingJourney';
import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';
import appCurrencyFormatter from '../utility/appCurrencyFormatter';

import useFileDownload from '../hooks/useFileDownload';
import AppProgressBar from '../components/ui/AppProgressBar';
import Orientation from 'react-native-orientation';
import { useSelector } from 'react-redux';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const expandMenu = {
  propertyDetails: false,
  propertyDetailsEOI: false,
  paymentDetails: false,
  paymentDetailsEOI: false,
  costSheetDetails: false,
  bookingSource: false,
  applicantDetails: false,
};

const BookingDetailsScreen = ({ navigation, route }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [isBookinDataLoading, setIsBookinDataLoading] = useState(false);
  const [hasError, setHasError] = useState(true);
  const [isInventory, setIsInventory] = useState(false);
  const [isShowPropertyModal, setIsShowPropertyModal] = useState(false);
  const [selectedExpandMenu, setSelectedExpandMenu] = useState(expandMenu);

  const [offersData, setOffersData] = useState([]);
  const [selectedOffersData, setSelectedOffersData] = useState();

  const [propertyDetails, setPropertyDetails] = useState({});
  const [costSheetData, setCostSheetData] = useState();
  const [selectedPaymentPlanDetails, setSelectedPaymentPlanDetails] = useState(
    null,
  );
  const [propertyDetailsEoi, setPropertyDetailsEoi] = useState({});
  const [bookingSource, setBookingSource] = useState();
  const [eoiPaymentDetails, setEoiPaymentDetals] = useState();
  const [applicantDetails, setApplicantDetails] = useState();
  const [sdrStatus, setSDRStatus] = useState(false);

  const routeParams = route.params;
  const projectId =
    routeParams && routeParams.projectId ? routeParams.projectId : '';
  const bookingId =
    routeParams && routeParams.bookingId ? routeParams.bookingId : '';
  const userId = routeParams && routeParams.userId ? routeParams.userId : '';
  const menuType = useSelector((state) => state.menuType.menuType);
  const {
    progress,
    showProgress,
    error,
    isSuccess,
    checkPermission,
  } = useFileDownload();

  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
    if (userId) {
      getOfferData(userId);
    } else {
      navigation.goBack();
    }
  }, []);

  const getOfferData = async (userId) => {
    try {
      setIsLoading(true);

      const requestData = {
        user_id: userId,
      };
      const offerDataResult = await apiOffers.apiOffers(requestData);
      // console.log('offerDataResult: ', offerDataResult.data);

      setIsLoading(false);

      if (offerDataResult.data) {
        if (
          offerDataResult.data.status &&
          offerDataResult.data.status == '200'
        ) {
          if (
            offerDataResult.data.data &&
            offerDataResult.data.data.length > 0
          ) {
            const initOfferData = offerDataResult.data.data;
            if (bookingId && bookingId != '') {
              const selectedProperty = initOfferData.find(
                (item) => item.id == bookingId,
              );
              if (selectedProperty) {
                onSelectProperty(selectedProperty);
              } else {
                onSelectProperty(initOfferData[0]);
              }
            } else {
              onSelectProperty(initOfferData[0]);
            }

            setOffersData(offerDataResult.data.data);
          } else {
            setOffersData([]);
          }
        } else {
          navigation.goBack();
          appSnakBar.onShowSnakBar(
            offerDataResult.data.msg
              ? offerDataResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      } else {
        navigation.goBack();
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      setIsLoading('LONG');
      navigation.goBack();
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const getSourceSelectionType = (source) => {
    let sourceSelectionType = '';
    const selectionSource =
      source && source.source_selection_source
        ? source.source_selection_source.toLowerCase()
        : '';

    if (selectionSource == 'direct') {
      sourceSelectionType = "Godrej Properties' advertisement";
    } else if (selectionSource == 'loyalty') {
      sourceSelectionType = "Existing Godrej Properties' customer";
    } else if (selectionSource == 'referral') {
      sourceSelectionType = 'Referred by an existing customer';
    } else if (selectionSource == 'partner') {
      sourceSelectionType = 'Referred by a Channel Partner';
    }
    return sourceSelectionType;
  };

  const onSelectProperty = (property) => {
    setSelectedExpandMenu(expandMenu);

    setPropertyDetails({});
    setCostSheetData({});
    setSelectedPaymentPlanDetails(null);
    setEoiPaymentDetals({});
    setBookingSource({});
    setPropertyDetailsEoi({});
    setApplicantDetails({});

    setSelectedOffersData(property);

    setIsInventory(property.type === 'inventory_booking' ? true : false);
    if (property.type === 'inventory_booking') {
      getBookingConfirmationData(property.id, userId);
    } else if (property.type === 'eoi_booking') {
      getEoiBookingConfirmationData(property.id, userId);
    }

    setIsShowPropertyModal(false);
  };

  const getBookingConfirmationData = async (bookingId, userId) => {
    try {
      setIsBookinDataLoading(true);

      const requestData = {
        booking_id: bookingId,
        user_id: userId,
      };
      const bookingConfirmationResult = await apiInvBookingJourney.apiBookingConfirmationData(
        requestData,
      );

      setIsBookinDataLoading(false);

      if (bookingConfirmationResult.data) {
        if (
          bookingConfirmationResult.data.status &&
          bookingConfirmationResult.data.status == '200' &&
          bookingConfirmationResult.data.data
        ) {
          setHasError(false);

          const resultData = bookingConfirmationResult.data.data;

          setSDRStatus(bookingConfirmationResult.data.sdr_status);

          setInventoryProperyDetails(resultData);
          setCostSheetDataDetails(resultData);
          setPaymentDetailsData(
            resultData,
            bookingConfirmationResult.data.sdr_status,
            bookingConfirmationResult.data.disclaimer,
          );
          setApplicantBookingSource(resultData);
          setApplicantDetailsData(resultData);
        } else {
          setHasError(true);
          appSnakBar.onShowSnakBar(
            bookingConfirmationResult.data.msg
              ? bookingConfirmationResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      } else {
        setHasError(true);
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      setHasError(true);
      setIsBookinDataLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const getEoiBookingConfirmationData = async (bookingId, userId) => {
    try {
      setIsBookinDataLoading(true);

      const requestData = {
        booking_id: bookingId,
        user_id: userId,
      };
      const bookingConfirmationResult = await apiEoiBookingJourney.apiEoiBookingConfirmationData(
        requestData,
      );

      setIsBookinDataLoading(false);

      if (bookingConfirmationResult.data) {
        if (
          bookingConfirmationResult.data.status &&
          bookingConfirmationResult.data.status == '200' &&
          bookingConfirmationResult.data.data
        ) {
          setHasError(false);

          const resultData = bookingConfirmationResult.data.data;

          setEoiProperyDetails(resultData);
          setEoiPaymentDetailsData(resultData);
          setApplicantBookingSource(resultData);
          setApplicantDetailsData(resultData);
        } else {
          setHasError(true);
          appSnakBar.onShowSnakBar(
            bookingConfirmationResult.data.msg
              ? bookingConfirmationResult.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR,
            'LONG',
          );
        }
      } else {
        setHasError(true);
        appSnakBar.onShowSnakBar(
          appConstant.appMessage.APP_GENERIC_ERROR,
          'LONG',
        );
      }
    } catch (error) {
      console.log(error);
      setHasError(true);
      setIsBookinDataLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const setInventoryProperyDetails = (resultData) => {
    if (resultData) {
      setPropertyDetails({
        proj_name: resultData.proj_name ? resultData.proj_name : '',
        plan_image: resultData.plan_image ? resultData.plan_image : '',
        flat_type: resultData.unit_code ? resultData.unit_code : '',
        tower_name: resultData.tower_name ? resultData.tower_name : '',
        wing: resultData.wing ? resultData.wing : '',
        floor_no: resultData.floor_no ? resultData.floor_no : '',
        unit_no: resultData.unit_no ? resultData.unit_no : '',
        open_balcony_area_sq_mt: resultData.open_balcony_area_sq_mt
          ? resultData.open_balcony_area_sq_mt
          : '',
        open_balcony_area_sq_ft: resultData.open_balcony_area_sq_ft
          ? resultData.open_balcony_area_sq_ft
          : '',
      });
    }
  };

  const setEoiProperyDetails = (resultData) => {
    if (resultData) {
      setPropertyDetailsEoi({
        isPlotProject: resultData.field_external_project_id
          ? appConstant.isPlotProject(resultData.field_external_project_id)
          : false,
        proj_name: resultData.proj_name ? resultData.proj_name : '',
        plan_image: resultData.plan_image ? resultData.plan_image : '',
        unit_code: resultData.unit_code ? resultData.unit_code : '',
        flat_type: resultData.typology ? resultData.typology : '',
        tower_name: resultData.tower_name ? resultData.tower_name : '',
        floor_band: resultData.floor ? resultData.floor : '',
      });
    }
  };

  const setCostSheetDataDetails = (resultData) => {
    if (resultData) {
      if (resultData.field_costsheet_data) {
        setCostSheetData({
          data: resultData.field_costsheet_data,
          costsheet_file_url: resultData.costsheet_file_url
            ? resultData.costsheet_file_url
            : '',
          costsheet_file_name: resultData.costsheet_file_name
            ? resultData.costsheet_file_name
            : '',
        });
      }
    }
  };

  const setPaymentDetailsData = (resultData, sdrStatus, disclaimer) => {
    if (
      resultData &&
      resultData.field_costsheet_plan_data &&
      resultData.field_costsheet_plan_data.curl_response &&
      resultData.field_costsheet_plan_data.curl_response.milestones
    ) {
      const planDetails =
        resultData.field_costsheet_plan_data.curl_response.milestones;
      if (planDetails && planDetails.length > 0) {
        const planDetailsObj = {};

        let noteMsg = '';
        let noteMsgAmount = '';
        if (
          resultData.field_costsheet_plan_data.curl_response.booking_amount &&
          resultData.field_costsheet_plan_data.curl_response.booking_amount !=
          '' &&
          resultData.field_costsheet_plan_data.curl_response.booking_amount != 0
        ) {
          noteMsg = `${resultData.field_costsheet_plan_data.curl_response
              .booking_days_label
              ? resultData.field_costsheet_plan_data.curl_response
                .booking_days_label
              : ''
            }`;
          noteMsgAmount = `${resultData.field_costsheet_plan_data.curl_response.currency
              ? resultData.field_costsheet_plan_data.curl_response.currency
              : ''
            } ${resultData.field_costsheet_plan_data.curl_response.booking_amount
              ? appCurrencyFormatter.getIndianCurrencyFormat(
                resultData.field_costsheet_plan_data.curl_response
                  .booking_amount,
              )
              : ''
            }`;
        } else {
          noteMsg = '';
          noteMsgAmount = '';
        }

        const plans = [];
        plans.push({
          title: 'Payment Milestone',
          interest: '%',
          amount: 'Amount (INR)',
          gst: 'GST (INR)',
          total: 'Total',
        });

        planDetails.map((planDetail, index) => {
          plans.push({
            title: planDetail.milestones,
            interest: planDetail.milestone_pct,
            amount: appCurrencyFormatter.getIndianCurrencyFormat(
              planDetail.amount,
            ),
            gst: appCurrencyFormatter.getIndianCurrencyFormat(planDetail.gst),
            total: appCurrencyFormatter.getIndianCurrencyFormat(
              planDetail.total,
            ),
            completed: planDetail.completed,
          });
        });

        if (
          resultData.field_costsheet_plan_data.curl_response
            .registration_amount &&
          resultData.field_costsheet_plan_data.curl_response
            .registration_amount != ''
        ) {
          plans.push({
            title: resultData.field_costsheet_plan_data.curl_response
              .registration_label
              ? resultData.field_costsheet_plan_data.curl_response
                .registration_label
              : '',
            interest: '',
            amount: '',
            gst: '',
            total: appCurrencyFormatter.getIndianCurrencyFormat(
              resultData.field_costsheet_plan_data.curl_response
                .registration_amount,
            ),
          });
        }

        plans.push({
          title: 'Total',
          interest: '',
          amount: resultData.field_costsheet_plan_data.curl_response
            .grand_amount
            ? appCurrencyFormatter.getIndianCurrencyFormat(
              resultData.field_costsheet_plan_data.curl_response.grand_amount,
            )
            : 0,
          gst: resultData.field_costsheet_plan_data.curl_response.grand_gst
            ? appCurrencyFormatter.getIndianCurrencyFormat(
              resultData.field_costsheet_plan_data.curl_response.grand_gst,
            )
            : 0,
          total: resultData.field_costsheet_plan_data.curl_response.grand_total
            ? appCurrencyFormatter.getIndianCurrencyFormat(
              resultData.field_costsheet_plan_data.curl_response.grand_total,
            )
            : 0,
        });

        planDetailsObj['message'] = noteMsg.trim();
        planDetailsObj['messageAmount'] = noteMsgAmount.trim();
        planDetailsObj['plans'] = plans;
        planDetailsObj['payment_plan_name'] =
          resultData.field_costsheet_plan_data.curl_response.payment_plan_name;
        planDetailsObj['cip_pp_name'] =
          resultData.field_costsheet_plan_data.curl_response.cip_pp_name;
        planDetailsObj['payment_plan_id'] =
          resultData.field_costsheet_plan_data.curl_response.payment_plan_id;
        planDetailsObj['project_id'] =
          resultData.field_costsheet_plan_data.curl_response.project_id;

        planDetailsObj['sdr_status'] = sdrStatus ? sdrStatus : false;
        planDetailsObj['disclaimer'] = disclaimer ? disclaimer : '';

        setSelectedPaymentPlanDetails(planDetailsObj);
      }
    }
  };

  const setEoiPaymentDetailsData = (resultData) => {
    if (resultData) {
      setEoiPaymentDetals({
        eoi_starting_price: resultData.eoi_starting_price
          ? resultData.eoi_starting_price
          : '',
        eoi_token_amount: resultData.eoi_token_amount
          ? resultData.eoi_token_amount
          : '',
        field_booking_token_type: resultData.field_booking_token_type
          ? resultData.field_booking_token_type
          : '',
        field_booking_number: resultData.field_booking_number
          ? resultData.field_booking_number
          : '',
      });
    }
  };

  const setApplicantBookingSource = (resultData) => {
    if (resultData) {
      setBookingSource({
        source_selection_source: resultData.source_selection_source
          ? resultData.source_selection_source
          : '',
        source_selection_answer: resultData.source_selection_answer
          ? resultData.source_selection_answer
          : '',
      });
    }
  };

  const setApplicantDetailsData = (resultData) => {
    if (resultData) {
      const applicantDetails = [
        {
          id: '1',
          title: 'Nationality',
          detail: resultData.field_residance_type
            ? resultData.field_residance_type
            : '',
        },
        {
          id: '2',
          title: 'Full Name',
          detail: resultData.field_name ? resultData.field_name : '',
        },
        {
          id: '3',
          title: 'Mobile Number',
          detail: resultData.field_mobile_no ? resultData.field_mobile_no : '',
        },
        {
          id: '4',
          title: 'Email',
          detail: resultData.field_email ? resultData.field_email : '',
        },
        {
          id: '5',
          title: 'Date of Birth',
          detail: resultData.field_dob
            ? resultData.field_dob
            : resultData.field_dob,
        },
        {
          id: '6',
          title: 'Permanent Account Number (PAN)',
          detail: resultData.pan_no
            ? resultData.field_residance_type &&
              resultData.field_residance_type === 'Indian'
              ? resultData.pan_no
              : ''
            : '',
        },
        {
          id: '7',
          title: 'Passport Number',
          detail: resultData.passport_no
            ? resultData.field_residance_type &&
              resultData.field_residance_type != 'Indian'
              ? resultData.passport_no
              : ''
            : '',
        },
        {
          id: '8',
          title: 'Purpose of Purchase',
          detail: resultData.field_applicant_purpose
            ? resultData.field_applicant_purpose
            : '',
        },
        {
          id: '9',
          title: 'Uploaded',
          docType: 'Document Type',
          detail: resultData.field_id_proof_file_type
            ? resultData.field_id_proof_file_type
            : '',
          file_format: resultData.id_proof_name
            ? resultData.id_proof_name.indexOf('.pdf') > -1
              ? 'pdf'
              : 'image'
            : '',
          file_name: resultData.id_proof_name ? resultData.id_proof_name : '',
          file_link: resultData.id_proof_url ? resultData.id_proof_url : '',
        },
        {
          id: '10',
          title: 'Uploaded',
          docType: 'Document Type',
          detail: resultData.field_address_proof_file_type
            ? resultData.field_address_proof_file_type
            : '',
          file_format: resultData.address_proof_name
            ? resultData.address_proof_name.indexOf('.pdf') > -1
              ? 'pdf'
              : 'image'
            : '',
          file_name: resultData.address_proof_name
            ? resultData.address_proof_name
            : '',
          file_link: resultData.address_proof_url
            ? resultData.address_proof_url
            : '',
        },
      ];

      const permanentAddress = [
        {
          id: '1',
          title: 'Address',
          detail: resultData.field_permanent_address
            ? resultData.field_permanent_address
            : '',
        },
        {
          id: '2',
          title: 'Pin code',
          detail: resultData.field_permanent_pincode
            ? resultData.field_permanent_pincode
            : '',
        },
        {
          id: '3',
          title: 'City',
          detail: resultData.field_permanent_city
            ? resultData.field_permanent_city
            : '',
        },
        {
          id: '4',
          title: 'State',
          detail: resultData.field_permanent_state
            ? resultData.field_permanent_state
            : '',
        },
        {
          id: '5',
          title: 'Country',
          detail: resultData.field_permanent_country
            ? resultData.field_permanent_country
            : '',
        },
      ];

      const communicationAddress = [
        {
          id: '1',
          title: 'Address',
          detail: resultData.field_address ? resultData.field_address : '',
        },
        {
          id: '2',
          title: 'Pin code',
          detail: resultData.field_pincode ? resultData.field_pincode : '',
        },
        {
          id: '3',
          title: 'City',
          detail: resultData.field_city ? resultData.field_city : '',
        },
        {
          id: '4',
          title: 'State',
          detail: resultData.field_state ? resultData.field_state : '',
        },
        {
          id: '5',
          title: 'Country',
          detail: resultData.field_country ? resultData.field_country : '',
        },
      ];

      setApplicantDetails({
        applicantDetails,
        permanentAddress,
        communicationAddress,
      });
    }
  };

  const onDownloadCostSheet = () => {
    if (
      costSheetData &&
      costSheetData.costsheet_file_url &&
      costSheetData.costsheet_file_url != '' &&
      costSheetData.costsheet_file_name &&
      costSheetData.costsheet_file_name != ''
    ) {
      onDownloadFile(
        costSheetData.costsheet_file_name,
        costSheetData.costsheet_file_url,
      );
    } else {
      appSnakBar.onShowSnakBar('File not found', 'LONG');
    }
  };

  const onDownloadFile = (fileName, fileUrl) => {
    if (fileName && fileName != '' && fileUrl && fileUrl != '') {
      const ext = fileName.split('.')[0];
      if (ext) {
        downloadFile(fileUrl, fileName, ext);
      }
    }
  };

  const downloadFile = async (imgPath, fileName, fileExtension) => {
    try {
      await checkPermission(imgPath, fileName, fileExtension);
    } catch (error) {
      appSnakBar.onShowSnakBar(error.message, 'LONG');
    }
  };

  const onExpandSelect = (type, isSelected) => {
    const selectedMenuTimeout = { ...selectedExpandMenu };

    if (type === 'propertyDetails') {
      selectedMenuTimeout.propertyDetails = isSelected;
    } else if (type === 'propertyDetailsEOI') {
      selectedMenuTimeout.propertyDetailsEOI = isSelected;
    } else if (type === 'paymentDetails') {
      selectedMenuTimeout.paymentDetails = isSelected;
    } else if (type === 'paymentDetailsEOI') {
      selectedMenuTimeout.paymentDetailsEOI = isSelected;
    } else if (type === 'costSheetDetails') {
      selectedMenuTimeout.costSheetDetails = isSelected;
    } else if (type === 'bookingSource') {
      selectedMenuTimeout.bookingSource = isSelected;
    } else if (type === 'applicantDetails') {
      selectedMenuTimeout.applicantDetails = isSelected;
    } else {
      selectedMenuTimeout.propertyDetails = false;
      selectedMenuTimeout.propertyDetailsEOI = false;
      selectedMenuTimeout.paymentDetails = false;
      selectedMenuTimeout.paymentDetailsEOI = false;
      selectedMenuTimeout.costSheetDetails = false;
      selectedMenuTimeout.bookingAmount = false;
      selectedMenuTimeout.bookingSource = false;
      selectedMenuTimeout.applicantDetails = false;
    }

    setSelectedExpandMenu(selectedMenuTimeout);

    // const selectedMenu = {...selectedExpandMenu};

    // selectedMenu.propertyDetails =
    //   type === 'propertyDetails' ? isSelected : false;
    // selectedMenu.propertyDetailsEOI =
    //   type === 'propertyDetailsEOI' ? isSelected : false;
    // selectedMenu.paymentDetails =
    //   type === 'paymentDetails' ? isSelected : false;
    // selectedMenu.paymentDetailsEOI =
    //   type === 'paymentDetailsEOI' ? isSelected : false;
    // selectedMenu.costSheetDetails =
    //   type === 'costSheetDetails' ? isSelected : false;
    // selectedMenu.bookingSource = type === 'bookingSource' ? isSelected : false;
    // selectedMenu.applicantDetails =
    //   type === 'applicantDetails' ? isSelected : false;

    // setSelectedExpandMenu(selectedMenu);
  };
  const onHandleRetryBtn = () => {
    if (userId) {
      getOfferData(userId);
    } else {
      navigation.goBack();
    }
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />

      {isLoading ? (
        <View style={styles.container}></View>
      ) : (
          <>
            {offersData && offersData.length > 0 ? (
              <>
                <View style={styles.dropdownContainer}>
                  <AppText style={styles.dropdownLabel}>Properties</AppText>
                  <TouchableWithoutFeedback
                    onPress={() => {
                      if (offersData.length > 1) {
                        setIsShowPropertyModal(true);
                      }
                    }}>
                    <View style={styles.itemContainer}>
                      <AppText style={styles.propertyText} numberOfLines={1}>
                        {selectedOffersData.title}
                      </AppText>
                      {offersData.length > 1 && (
                        <Image
                          style={styles.arrow}
                          source={require('./../assets/images/down-icon-b.png')}
                        />
                      )}
                    </View>
                  </TouchableWithoutFeedback>
                </View>

                {!isBookinDataLoading && !hasError ? (
                  <ScrollView>
                    <View style={styles.container}>
                      <AppTextBold style={styles.pageTitle}>
                        {isInventory ? `BOOKING DETAILS` : `PRE-BOOKING DETAILS`}
                      </AppTextBold>

                      {isInventory ? (
                        <>
                          {selectedExpandMenu.propertyDetails ? (
                            <BookingPropertyComponent
                              propertyDetails={propertyDetails}
                              onPress={() =>
                                onExpandSelect('propertyDetails', false)
                              }
                            />
                          ) : (
                              <AppExpandButton
                                title="Property Details"
                                onPress={() =>
                                  onExpandSelect('propertyDetails', true)
                                }
                              />
                            )}
                        </>
                      ) : null}

                      {!isInventory ? (
                        <>
                          {selectedExpandMenu.propertyDetailsEOI ? (
                            <EOIBookingPropertyComponent
                              propertyDetails={propertyDetailsEoi}
                              onPress={() =>
                                onExpandSelect('propertyDetailsEOI', false)
                              }
                            />
                          ) : (
                              <AppExpandButton
                                title="Unit Preference"
                                onPress={() =>
                                  onExpandSelect('propertyDetailsEOI', true)
                                }
                              />
                            )}
                        </>
                      ) : null}

                      {isInventory && selectedPaymentPlanDetails ? (
                        <>
                          {selectedExpandMenu.paymentDetails ? (
                            <View style={styles.viewContainer}>
                              <View style={styles.subTitle}>
                                <TouchableWithoutFeedback
                                  onPress={() =>
                                    onExpandSelect('paymentDetails', false)
                                  }>
                                  <View style={styles.titleLine}>
                                    <AppText
                                      style={[
                                        styles.title,
                                        {
                                          fontFamily:
                                            appFonts.SourceSansProSemiBold,
                                        },
                                      ]}>
                                      Payment Plan details
                                  </AppText>
                                    <Image
                                      source={require('./../assets/images/up-icon-b.png')}
                                      style={styles.icon}
                                    />
                                  </View>
                                </TouchableWithoutFeedback>
                              </View>
                              <PaymentDetailModalScreen
                                paymentPlanDetails={selectedPaymentPlanDetails}
                                cancel={false}
                              />
                            </View>
                          ) : (
                              <AppExpandButton
                                title="Payment Plan Details"
                                onPress={() =>
                                  onExpandSelect('paymentDetails', true)
                                }
                              />
                            )}
                        </>
                      ) : null}

                      {!isInventory ? (
                        <>
                          {selectedExpandMenu.paymentDetailsEOI ? (
                            <View style={styles.viewContainer}>
                              <View style={styles.subTitle}>
                                <TouchableWithoutFeedback
                                  onPress={() =>
                                    onExpandSelect('paymentDetailsEOI', false)
                                  }>
                                  <View style={styles.titleLine}>
                                    <AppText
                                      style={[
                                        styles.title,
                                        {
                                          fontFamily:
                                            appFonts.SourceSansProSemiBold,
                                        },
                                      ]}>
                                      Payment Details
                                  </AppText>
                                    <Image
                                      source={require('./../assets/images/up-icon-b.png')}
                                      style={styles.icon}
                                    />
                                  </View>
                                </TouchableWithoutFeedback>
                              </View>
                              <View style={{ paddingHorizontal: 25 }}>
                                {eoiPaymentDetails.eoi_starting_price ? (
                                  <AppBookingPropertyDetails
                                    label="Starting Price"
                                    value={`Rs. ${appCurrencyFormatter.getIndianCurrencyFormatWithoutDecimal(
                                      eoiPaymentDetails.eoi_starting_price,
                                    )}`}
                                  />
                                ) : null}
                                {eoiPaymentDetails.eoi_token_amount ? (
                                  <AppBookingPropertyDetails
                                    label="Token Amount"
                                    value={`Rs. ${appCurrencyFormatter.getIndianCurrencyFormatWithoutDecimal(
                                      eoiPaymentDetails.eoi_token_amount,
                                    )}`}
                                  />
                                ) : null}
                                {eoiPaymentDetails.field_booking_token_type ? (
                                  <AppBookingPropertyDetails
                                    label="Token Type"
                                    value={
                                      eoiPaymentDetails.field_booking_token_type
                                    }
                                  />
                                ) : null}
                                {eoiPaymentDetails.field_booking_number ? (
                                  <AppBookingPropertyDetails
                                    label="Booking No."
                                    value={eoiPaymentDetails.field_booking_number}
                                  />
                                ) : null}
                              </View>
                            </View>
                          ) : (
                              <AppExpandButton
                                title="Payment Details"
                                onPress={() =>
                                  onExpandSelect('paymentDetailsEOI', true)
                                }
                              />
                            )}
                        </>
                      ) : null}

                      {isInventory && costSheetData.data ? (
                        <>
                          {selectedExpandMenu.costSheetDetails ? (
                            <CostSheetComponent
                              data={costSheetData.data}
                              onPress={() =>
                                onExpandSelect('costSheetDetails', false)
                              }
                              showApplyCoupon={false}
                              isShowDisclaimer={sdrStatus ? false : true}
                              onDownloadCostSheet={() => onDownloadCostSheet()}
                            />
                          ) : (
                              <AppExpandButton
                                title="Cost Sheet Details"
                                onPress={() =>
                                  onExpandSelect('costSheetDetails', true)
                                }
                              />
                            )}
                        </>
                      ) : null}

                      {selectedExpandMenu.bookingSource ? (
                        <View style={styles.viewContainer}>
                          <View style={styles.subTitle}>
                            <TouchableWithoutFeedback
                              onPress={() =>
                                onExpandSelect('bookingSource', false)
                              }>
                              <View style={styles.titleLine}>
                                <AppText
                                  style={[
                                    styles.title,
                                    { fontFamily: appFonts.SourceSansProSemiBold },
                                  ]}>
                                  Booking Source
                              </AppText>
                                <Image
                                  source={require('./../assets/images/up-icon-b.png')}
                                  style={styles.icon}
                                />
                              </View>
                            </TouchableWithoutFeedback>
                            {getSourceSelectionType(bookingSource) &&
                              getSourceSelectionType(bookingSource) != '' ? (
                                <>
                                  <AppText style={styles.sourceType}>
                                    {getSourceSelectionType(bookingSource)}
                                  </AppText>
                                  {bookingSource.source_selection_answer &&
                                    bookingSource.source_selection_answer != '' ? (
                                      <AppText style={styles.source}>
                                        {bookingSource.source_selection_answer}
                                      </AppText>
                                    ) : (
                                      ''
                                    )}
                                </>
                              ) : (
                                <AppText style={styles.source}>
                                  Booking Source not available
                                </AppText>
                              )}
                          </View>
                        </View>
                      ) : (
                          <AppExpandButton
                            title="Booking Source"
                            onPress={() => onExpandSelect('bookingSource', true)}
                          />
                        )}

                      {selectedExpandMenu.applicantDetails ? (
                        <ApplicantDetailsComponent
                          onPress={() =>
                            onExpandSelect('applicantDetails', false)
                          }
                          applicantDetails={applicantDetails.applicantDetails}
                          permanentAddress={applicantDetails.permanentAddress}
                          communicationAddress={
                            applicantDetails.communicationAddress
                          }
                          downloadApplicantDocument={(fileLink, fileName) => {
                            onDownloadFile(fileName, fileLink);
                          }}
                        />
                      ) : (
                          <AppExpandButton
                            title="Primary Applicant Details"
                            onPress={() => onExpandSelect('applicantDetails', true)}
                          />
                        )}
                    </View>
                  </ScrollView>
                ) : (
                    <View style={styles.container}></View>
                  )}
              </>
            ) : (
                <View style={styles.container}>
                  <AppText style={styles.txtNoData}>No bookings available</AppText>
                </View>
              )}
          </>
        )}

      <AppFooter activePage={0} isPostSales={menuType == 'postsale' ? true : false} />
      <AppOverlayLoader isLoading={isLoading || isBookinDataLoading} />
      {showProgress ? <AppProgressBar progress={progress} /> : null}

      <Modal
        animationType="fade"
        transparent={true}
        visible={isShowPropertyModal}
        onRequestClose={() => {
          setIsShowPropertyModal(false);
        }}>
        <SelectBookingPropertyModalScreen
          properties={offersData}
          onClose={() => {
            setIsShowPropertyModal(false);
          }}
          onSelect={(property) => {
            onSelectProperty(property);
          }}
        />
      </Modal>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 10,
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    paddingHorizontal: 25,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: '7%',
    textTransform: 'uppercase',
  },
  viewContainer: {
    width: windowWidth,
    paddingBottom: 20,
    marginBottom: 25,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  titleLine: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize:appFonts.largeBold,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  value: {
    fontSize:appFonts.largeBold,
    color: colors.expandText,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  icon: {
    width: 16,
    height: 16,
  },
  detailsLine: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
  },
  subTitle: {
    paddingHorizontal: 25,
  },
  sourceType: {
    fontSize:appFonts.largeFontSize,
    marginBottom: 10,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  source: { fontSize:appFonts.largeBold, color: colors.jaguar },

  expandIcon: {
    width: 20,
    height: 20,
  },
  dropdownProperty: {
    borderBottomWidth: 2,
    borderBottomColor: colors.gray4,
    width: '100%',
    height: '100%',
    margin: 10,
    borderWidth: 0,
    marginBottom: 0,
    right: 20,
  },
  propertyText: {
    width: '90%',
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
  },
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: colors.gray4,
    paddingVertical: 20,
    paddingRight: 20,
  },
  arrow: {
    height: 16,
    width: 16,
  },
  dropdownContainer: {
    paddingVertical: 12,
    paddingHorizontal: 32,
    marginBottom: 15,
  },
  dropdownLabel: { color: colors.gray3, fontSize: appFonts.largeFontSize },
  txtNoData: {
    alignSelf: 'center',
    marginTop: 20,
    fontSize:appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
});

export default BookingDetailsScreen;
