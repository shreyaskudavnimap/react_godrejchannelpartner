import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

import Screen from '../components/Screen';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppFooter from '../components/ui/AppFooter';
import AppButton from '../components/ui/AppButton';
import colors from '../config/colors';
import appFonts from '../config/appFonts';
import AddSnagComponent from '../components/AddSnagComponent';
import apiRaiseASnag from '../api/apiRaiseasnag'
import RequestCreated from '../screens/modals/RequestCreated';
import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import Orientation from 'react-native-orientation';

const ViewSnagScreen = (props) => {
  const [isUpdateRequest, setIsUpdateRequest] = useState(false);
  const [snagList, setSnagList] = useState([])
  const [snagItemId, setSnagItemId] = useState('')
  const [modalView, setModalview] = useState(false);
  const [submitSuccessMsg, setSubmitSuccessMsg] = useState('')
  const [isLoading, setIsLoading] = useState(false);
  const [addMore, setAddMore] = useState(false)
  const bookingId =  props.route.params.bookingId
  const snagListId = props.route.params.snagListId
  const userId = props.route.params.userId

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);
  

  useEffect(() => {
    getSnagList()
  }, [])

  const getSnagList = async () => {
    const payload = {
      user_id: userId,
      booking_id: bookingId,
      snag_list_id: snagListId
    }
    setIsLoading(true)
    setAddMore(false)
    try {
      const result = await apiRaiseASnag.getViewSnagList(payload)
      const res = result.data
      setSnagList(res.data.snag_items || [])
      setIsLoading(false)
    } catch (err) {
      console.log(err)
      setIsLoading(false)
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  }

  const onDeleteSnagItem = async (snagItemId) => {
    const payload = {
      user_id: userId,
      booking_id: bookingId,
      snag_list_id: snagListId,
      snag_item_id: snagItemId
    }

    setIsLoading(true)

    try {
      await apiRaiseASnag.deleteSnagItem(payload)
      setIsLoading(false)
      getSnagList()
    } catch(err) {
      console.log(err)
      setIsLoading(false)
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  }

  const onSaveForLater = () => {
    props.navigation.navigate('RaiseSnagScreen', { bookingId })
  }

  const onSubmitSnagList = async () => {
    const payload = {
      user_id: userId,
      booking_id: bookingId,
      snag_list_id: snagListId
    }
    setIsLoading(true)
    try {
      const result = await apiRaiseASnag.submitSnagList(payload)
      const res= result.data
      setIsLoading(false)
      setSubmitSuccessMsg(res.msg)
      setModalview(true);
    } catch (err) {
      console.log(err)
      setIsLoading(false)
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  }

  const resquestModalClose = (flag) => {
    setModalview(flag)
    props.navigation.navigate('RaiseSnagScreen', { bookingId })
  }

  const onHandleRetryBtn = () => {
    getSnagList()
  };

  return (
    <Screen onRetry={onHandleRetryBtn}>
      <AppHeader />
      {modalView && <RequestCreated msg={submitSuccessMsg} func={resquestModalClose} />}
      <ScrollView>
        <View style={styles.container}>
          <AppTextBold style={styles.pageTitle}>{isUpdateRequest ? addMore ? 'RAISE A SNAG' : 'EDIT SNAG' : 'VIEW SNAG LIST' }</AppTextBold>
          {isUpdateRequest ? (
            <View>
              <AddSnagComponent
                bookingId={bookingId}
                snagListId={snagListId}
                snagItemId={snagItemId}
                userId={userId}
                setIsLoading={setIsLoading}
              />
            </View>
          ) : (
            <View>
              {snagList.map((item, index) => (
                <View key={item.snag_item_id} style={styles.snagBox}>
                {item.images[0] ?
                  <Image source={{uri: item.images[0]}} style={styles.images} /> :
                  <Image source={require('./../assets/images/image-plceholder.png')} style={styles.images} />
                }

                  <View style={styles.snagDetails}>
                    <AppText style={styles.titleDesign}>{item.location}</AppText>
                    <AppText style={styles.descriptionDesign}>
                      {item.description}
                    </AppText>

                    <View style={styles.editDeleteButton}>
                      <TouchableOpacity
                        onPress={() => {
                          setIsUpdateRequest(true);
                          setSnagItemId(item.snag_item_id)
                        }}>
                        <AppText style={styles.btnText}>Edit</AppText>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => { onDeleteSnagItem(item.snag_item_id)}}
                      >
                        <AppText style={[styles.btnText, {marginLeft: '50%'}]}>
                          Delete
                        </AppText>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              ))}

            <TouchableOpacity 
              onPress={() => {
                setIsUpdateRequest(true);
                setSnagItemId('')
                setAddMore(true)
              }}
              style={styles.addItems}>
              <Image
                style={styles.addIcon}
                source={require('./../assets/images/plus-icon.png')}
              />
              <AppText style={styles.addMoreItems}>Add more</AppText>
            </TouchableOpacity>

              <AppButton
                onPress={onSaveForLater}
                color="primary"
                textColor="secondary"
                title="SAVE FOR LATER"
              />
              <AppButton onPress={onSubmitSnagList} title="SUBMIT" />
            </View>
          )}
        </View>
      </ScrollView>

      <AppFooter activePage={0} isPostSales={true} />
      <AppOverlayLoader isLoading={isLoading} />
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '6%',
    paddingBottom: '6%',
  },
  pageTitle: {
    fontSize: appFonts.xxlargeFontSize,
    color: colors.secondary,
    textTransform: 'uppercase',
  },
  tabName: {
    paddingBottom: '3%',
    marginBottom: '5%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  activeTab: {
    width: '50%',
    paddingVertical: '5%',
    borderBottomWidth: 3,
    borderBottomColor: colors.gray,
    alignItems: 'center',
  },
  snagBox: {
    flexDirection: 'row',
    padding: '5%',
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
    marginBottom: 20,
  },
  images: {
    height: 100,
    width: 100,
  },
  snagDetails: {
    marginLeft: '5%',
    width: '65%',
    justifyContent: 'space-evenly',
  },
  titleDesign: {
    fontFamily: appFonts.SourceSansProBold,
    fontSize: appFonts.normalFontSize,
  },
  descriptionDesign: {
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.normalFontSize,
  },
  editDeleteButton: {
    flexDirection: 'row',
  },
  btnText: {
    fontSize: appFonts.mediumFontSize,
    fontWeight: 'bold',
  },
  noSnagItem: {
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.largeBold,
  },
  addItems: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '38%',
    marginBottom: '5%',
  },
  addIcon: {
    height: 40,
    width: 40,
  },
  addMoreItems: {
    marginLeft: '5%',
    fontFamily: appFonts.SourceSansProBold,
    fontSize: appFonts.largeBold,
  },
});
export default ViewSnagScreen;
