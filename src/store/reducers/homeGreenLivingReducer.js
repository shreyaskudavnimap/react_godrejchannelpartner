import {
  SET_GREEN_LIVING,
  UPDATE_GREEN_LIVING,
  UPDATE_GREEN_LIVING_ARR,
} from '../actions/homeGreenLivingAction';

const initialState = {
  greenLiving: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_GREEN_LIVING:
      return {greenLiving: action.data};

    case UPDATE_GREEN_LIVING:
      const greenLivingData = {...state.greenLiving};
      if (
        greenLivingData &&
        greenLivingData.data &&
        greenLivingData.data.length > 0
      ) {
        greenLivingData.data.map((mapItem) => {
          if (mapItem.proj_id == action.data) {
            console.log('findIndex: ', mapItem.proj_id + ' -- ' + action.data);
            mapItem.wishlist_status =
              mapItem.wishlist_status == '0' ? '1' : '0';
          }
        });
      }

      return {greenLiving: greenLivingData};

    case UPDATE_GREEN_LIVING_ARR:
      const greenLivingDataArr = {...state.greenLiving};
      if (
        greenLivingDataArr &&
        greenLivingDataArr.data &&
        greenLivingDataArr.data.length > 0 &&
        action.data &&
        action.data.length > 0
      ) {
        greenLivingDataArr.data.map((mapItem) => {
          action.data.map((dataItem) => {
            if (mapItem.proj_id == dataItem.proj_id) {
              console.log(
                'findIndex: ',
                mapItem.proj_id + ' -- ' + dataItem.proj_id,
              );
              mapItem.wishlist_status =
                mapItem.wishlist_status == '0' ? '1' : '0';
            }
          });
        });
      }

      return {greenLiving: greenLivingDataArr};
  }
  return state;
};
