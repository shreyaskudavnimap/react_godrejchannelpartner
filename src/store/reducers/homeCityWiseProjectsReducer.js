import {
  SET_CITY_WISE_PROJECTS,
  UPDATE_CITY_WISE_PROJECTS,
  UPDATE_CITY_WISE_PROJECTS_ARR
} from '../actions/homeCityWiseProjectsAction';

const initialState = {
  cityWiseProjects: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CITY_WISE_PROJECTS:
      return {cityWiseProjects: action.data};

    case UPDATE_CITY_WISE_PROJECTS:
      const cityWiseProjectsData = {...state.cityWiseProjects};
      if (
        cityWiseProjectsData &&
        cityWiseProjectsData.data &&
        cityWiseProjectsData.data.length > 0
      ) {
        cityWiseProjectsData.data.map((cityItem) => {
          if (cityItem.proj_list && cityItem.proj_list.length > 0) {
            cityItem.proj_list.map((mapItem) => {
              if (mapItem.proj_id == action.data) {
                console.log(
                  'findIndex: ',
                  mapItem.proj_id + ' -- ' + action.data,
                );
                mapItem.wishlist_status =
                  mapItem.wishlist_status == '0' ? '1' : '0';
              }
            });
          }
        });
      }

      return {cityWiseProjects: cityWiseProjectsData};

    case UPDATE_CITY_WISE_PROJECTS_ARR:
      const cityWiseProjectsDataArr = {...state.cityWiseProjects};
      if (
        cityWiseProjectsDataArr &&
        cityWiseProjectsDataArr.data &&
        cityWiseProjectsDataArr.data.length > 0
      ) {
        cityWiseProjectsDataArr.data.map((cityItem) => {
          if (
            cityItem.proj_list &&
            cityItem.proj_list.length > 0 &&
            action.data &&
            action.data.length > 0
          ) {
            cityItem.proj_list.map((mapItem) => {

              action.data.map((dataItem) => {
                if (mapItem.proj_id == dataItem.proj_id) {
                  console.log(
                    'findIndex: ',
                    mapItem.proj_id + ' -- ' + dataItem.proj_id,
                  );
                  mapItem.wishlist_status =
                    mapItem.wishlist_status == '0' ? '1' : '0';
                }
              });
            });
          }
        });
      }

      return {cityWiseProjects: cityWiseProjectsDataArr};
  }
  return state;
};
