import {SET_NEW_LAUNCH, UPDATE_NEW_LAUNCH, UPDATE_NEW_LAUNCH_ARR} from '../actions/homeNewLaunchAction';

const initialState = {
  newLaunch: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_NEW_LAUNCH:
      return {newLaunch: action.data};
      
      case UPDATE_NEW_LAUNCH:

        const newLaunchData = {...state.newLaunch};
        if (
          newLaunchData &&
          newLaunchData.data &&
          newLaunchData.data.length > 0
        ) {
          newLaunchData.data.map((mapItem) => {
            if (mapItem.proj_id == action.data) {
              console.log('findIndex: ', mapItem.proj_id + ' -- ' + action.data);
              mapItem.wishlist_status = 
                mapItem.wishlist_status == '0' ? '1' : '0';
            }
          });
        }

        return {newLaunch: newLaunchData};
      
        case UPDATE_NEW_LAUNCH_ARR:
  
          const newLaunchDataArr = {...state.newLaunch};
          if (
            newLaunchDataArr &&
            newLaunchDataArr.data &&
            newLaunchDataArr.data.length > 0 &&
            action.data &&
            action.data.length > 0
          ) {
            newLaunchDataArr.data.map((mapItem) => {
              action.data.map((dataItem) => {
                if (mapItem.proj_id == dataItem.proj_id) {
                  console.log(
                    'findIndex: ',
                    mapItem.proj_id + ' -- ' + dataItem.proj_id,
                  );
                  mapItem.wishlist_status =
                    mapItem.wishlist_status == '0' ? '1' : '0';
                }
              });
            });
          }
  
          return {newLaunch: newLaunchDataArr};
  }
  return state;
};
