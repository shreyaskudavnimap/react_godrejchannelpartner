import {SET_BATCH_DATA} from '../actions/badgeCountAction';

const initialState = {
  lastAccessed: '',
  wishlistCount: '',
  notificationCount: '',
  totalCount: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_BATCH_DATA:
      return {
        ...state,
        ...action.data,
      };
  }

  return state;
};
