import {SET_MENU_TYPE} from '../actions/menuTypeAction';

const initialState = {
  menuType: 'presale'
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_MENU_TYPE:
      return {
        ...state,
        ...action.data,
      };
  }

  return state;
};
