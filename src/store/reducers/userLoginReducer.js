import {USER_LOGIN, UPDATE_USER_IMAGE} from '../actions/userLoginAction';
import {AUTH} from '../../utility/ReduxEvents';

const initialState = {
  loginResponse: {isLogin: false},
  loginParam: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN:
      return {
        ...state,
        loginResponse: action.data,
      };

    case UPDATE_USER_IMAGE:
      const updatedLoginResponse = {...state.loginResponse};

      updatedLoginResponse.data.user_image = action.data;

      return {
        ...state,
        loginResponse: updatedLoginResponse,
      };

    case AUTH.SET_LOGIN_PARAM:
      return {
        ...state,
        loginParam: action.data,
      };
    case AUTH.CLEAR_LOGIN_PARAM:
      return {
        ...state,
        loginParam: '',
      };
    default:
      return state;
  }
};
