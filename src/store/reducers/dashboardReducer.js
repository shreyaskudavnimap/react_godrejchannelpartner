import {DASHBOARD} from '../../utility/ReduxEvents';

const initialState = {
  rmData: [],
  saveDate: 0,
  saveTime: 0,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case DASHBOARD.SET_RM_DATA:
      let date = new Date();
      return {
        ...state,
        rmData: action.data,
        saveDate: date.getDay(),
        saveTime: date.getHours(),
      };
    case DASHBOARD.CLEAR_RM_DATA:
      return {...initialState};
  }
  return state;
};
