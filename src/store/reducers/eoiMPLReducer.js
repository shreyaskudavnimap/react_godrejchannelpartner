import {
  SET_TYPOLOGY_LIST_EOI,
  UPDATE_TYPOLOGY_LIST_EOI,
  SET_TOWER_LIST_EOI,
  UPDATE_TOWER_LIST_EOI,
  RESET_TOWER_LIST_EOI,
  SET_FLOOR_BAND_LIST_EOI,
  UPDATE_FLOOR_BAND_LIST_EOI,
  RESET_FLOOR_BAND_LIST_EOI,
  UPDATE_FLOOR_BAND_PLAN_EOI,
  UPDATE_EOI_LIST_EOI_BOOKING,
  SET_TYPOLOGY_LIST_EOI_BOOKING,
  UPDATE_TYPOLOGY_LIST_EOI_BOOKING,
  SET_TOWER_LIST_EOI_BOOKING,
  UPDATE_TOWER_LIST_EOI_BOOKING,
  RESET_TOWER_LIST_EOI_BOOKING,
  SET_FLOOR_BAND_LIST_EOI_BOOKING,
  UPDATE_FLOOR_BAND_LIST_EOI_BOOKING,
  RESET_FLOOR_BAND_LIST_EOI_BOOKING,
  UPDATE_FLOOR_BAND_PLAN_EOI_BOOKING,
} from '../actions/eoiMPLAction';

const initialState = {
  typologyList: {},
  towerList: {},
  floorBandList: {},
  typologyListBooking: [],
  towerListBooking: [],
  floorBandListBooking: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_TYPOLOGY_LIST_EOI:
      const typologyData = action.data;

      if (typologyData.data && typologyData.data.length > 0) {
        typologyData.data.map((item, index) => {
          if (index === 0) {
            item['selectedTypology'] = true;
          } else {
            item['selectedTypology'] = false;
          }

          if (item.name) {
            let scssTypology = item.name
              .split(' ')
              .reverse()
              .join('_')
              .toLowerCase()
              .replace('rhk', 'bhk')
              .replace('bed', 'bhk')
              .replace('+', 'plus')
              .replace('-', '_')
              .replace('.', '_');

            if (scssTypology) {
              item['scssTypology'] = scssTypology;
            } else {
              item['scssTypology'] = 'secondary';
            }
          } else {
            item['scssTypology'] = 'secondary';
          }
        });
      }

      return {
        ...state,
        typologyList: typologyData,
        towerList: {},
        floorBandList: {},
      };

    case UPDATE_TYPOLOGY_LIST_EOI:
      console.log('UPDATE_TYPOLOGY_LIST_EOI');
      const preTypologyList = state.typologyList;
      const updatedTypology = action.data;

      if (preTypologyList.data && preTypologyList.data.length > 0) {
        preTypologyList.data.map((typology) => {
          if (typology.id === updatedTypology.id) {
            if (!typology.selectedTypology) typology.selectedTypology = true;
          } else {
            typology.selectedTypology = false;
          }
        });
      }

      return {
        ...state,
        typologyList: preTypologyList,
        towerList: {},
        floorBandList: {},
      };

    case SET_TOWER_LIST_EOI:
      const towerData = action.data;

      if (towerData && towerData.data && towerData.length > 0) {
        towerData.data.map((item, index) => {
          item['selectedTower'] = false;
        });
      }

      return {
        ...state,
        towerList: towerData,
        floorBandList: {},
      };

    case UPDATE_TOWER_LIST_EOI:
      const preTowerList = {...state.towerList};
      const updatedTower = action.data;

      if (preTowerList.data && preTowerList.data.length > 0) {
        preTowerList.data.map((tower) => {
          if (tower.id === updatedTower.id) {
            tower.selectedTower = !tower.selectedTower;
          } else {
            tower.selectedTower = false;
          }
        });
      }

      return {
        ...state,
        towerList: preTowerList,
        floorBandList: {},
      };

    case RESET_TOWER_LIST_EOI:
      return {
        ...state,
      };

    case SET_FLOOR_BAND_LIST_EOI:
      const floorBandData = action.data;

      if (floorBandData && floorBandData.data && floorBandData.length > 0) {
        floorBandData.data.map((item, index) => {
          item['selectedFloor'] = false;
          item['isSelectedFloorPlan'] = true;
        });
      }

      return {
        ...state,
        floorBandList: floorBandData,
      };

    case UPDATE_FLOOR_BAND_LIST_EOI:
      const preFloorBandList = {...state.floorBandList};
      const updatedFloorBand = action.data;

      if (preFloorBandList.data && preFloorBandList.data.length > 0) {
        preFloorBandList.data.map((floorBand) => {
          if (floorBand.id === updatedFloorBand.id) {
            floorBand.selectedFloor = !floorBand.selectedFloor;
            floorBand.isSelectedFloorPlan = true;
          } else {
            floorBand.selectedFloor = false;
          }
        });
      }

      return {
        ...state,
        floorBandList: preFloorBandList,
      };

    case UPDATE_FLOOR_BAND_PLAN_EOI:
      const preFloorBandButton = {...state.floorBandList};
      const index =
        preFloorBandButton && preFloorBandButton.data
          ? preFloorBandButton.data.indexOf(action.data)
          : -1;

      if (index > -1) {
        preFloorBandButton.data[index].isSelectedFloorPlan = !preFloorBandButton
          .data[index].isSelectedFloorPlan;
      }

      return {...state, floorBandList: preFloorBandButton};

    case RESET_FLOOR_BAND_LIST_EOI:
      return {
        ...state,
      };

    case UPDATE_EOI_LIST_EOI_BOOKING:
      return {
        ...state,
        typologyListBooking: getPreferencedSorted(
          action.typologyData,
          'typology',
        ),
        towerListBooking: getPreferencedSorted(action.towerData, 'tower'),
        floorBandListBooking: getPreferencedSorted(
          action.floorBandData,
          'floorBand',
        ),
      };

    case UPDATE_TYPOLOGY_LIST_EOI_BOOKING:
      const preTypologyListBooking = [...state.typologyListBooking];
      const updatedTypologyBooking = action.data;

      if (preTypologyListBooking && preTypologyListBooking.length > 0) {
        preTypologyListBooking.map((typology) => {
          if (typology.id === updatedTypologyBooking.id) {
            typology.selectedTypologyBooking = !typology.selectedTypologyBooking;
          } else {
            typology.selectedTypologyBooking = false;
          }
        });
      }

      return {
        ...state,
        typologyListBooking: getPreferencedSorted(
          preTypologyListBooking,
          'typology',
        ),
        towerListBooking: [],
        floorBandListBooking: [],
      };

    case SET_TOWER_LIST_EOI_BOOKING:
      const _TypologyData = [...state.typologyListBooking];

      if (
        action.typology_id &&
        action.typology_id != '' &&
        _TypologyData &&
        _TypologyData.length > 0
      ) {
        _TypologyData.map((typologyItem) => {
          if (typologyItem.id === action.typology_id) {
            typologyItem.selectedTypologyBooking = !typologyItem.selectedTypologyBooking;
          } else {
            typologyItem.selectedTypologyBooking = false;
          }
        });
      }

      return {
        ...state,
        typologyListBooking: getPreferencedSorted(_TypologyData, 'typology'),
        towerListBooking:
          action.data && action.data.data && action.data.data.length > 0
            ? action.data.data
            : [],
        floorBandListBooking: [],
      };

    case SET_FLOOR_BAND_LIST_EOI_BOOKING:
      const _TowerData = [...state.towerListBooking];

      if (
        action.tower_id &&
        action.tower_id != '' &&
        _TowerData &&
        _TowerData.length > 0
      ) {
        _TowerData.map((towerItem) => {
          if (towerItem.id === action.tower_id) {
            towerItem.selectedTowerBooking = !towerItem.selectedTowerBooking;
          } else {
            towerItem.selectedTowerBooking = false;
          }
        });
      }

      return {
        ...state,
        towerListBooking: getPreferencedSorted(_TowerData, 'tower'),
        floorBandListBooking:
          action.data && action.data.data && action.data.data.length > 0
            ? action.data.data
            : [],
      };

    case UPDATE_TOWER_LIST_EOI_BOOKING:
      const preTowerListBooking = [...state.towerListBooking];
      const updatedTowerBooking = action.data;

      if (preTowerListBooking && preTowerListBooking.length > 0) {
        preTowerListBooking.map((tower) => {
          if (tower.id === updatedTowerBooking.id) {
            tower.selectedTowerBooking = !tower.selectedTowerBooking;
          } else {
            tower.selectedTowerBooking = false;
          }
        });
      }

      return {
        ...state,
        towerListBooking: getPreferencedSorted(preTowerListBooking, 'tower'),
        floorBandListBooking: {},
      };

    case UPDATE_FLOOR_BAND_PLAN_EOI_BOOKING:
      const preFloorBandButtonBooking = [...state.floorBandListBooking];

      if (preFloorBandButtonBooking && preFloorBandButtonBooking.length > 0) {
        preFloorBandButtonBooking.map((floorBandItem) => {
          if (floorBandItem.id === action.data.id) {
            floorBandItem.selectedFloorBooking = !floorBandItem.selectedFloorBooking;
          } else {
            floorBandItem.selectedFloorBooking = false;
          }
        });
      }

      return {
        ...state,
        floorBandListBooking: getPreferencedSorted(
          preFloorBandButtonBooking,
          'floorBand',
        ),
      };
  }

  return state;
};

const getPreferencedSorted = (preferenceArr, preferenceType) => {
  let sortedArr = [];
  sortedArr = Object.assign([], preferenceArr);
  if (sortedArr && sortedArr.length > 0) {
    return sortedArr.sort(function (a, b) {

      // if (preferenceType === 'typology') {
      //   if (a.selectedTypologyBooking == true) return -1;
      // } else if (preferenceType === 'tower') {
      //   if (a.selectedTowerBooking == true) return -1;
      // } else if (preferenceType === 'floorBand') {
      //   if (a.selectedFloorBooking == true) return -1;
      // }

      return 0;
    });
  } else {
    return [];
  }
};
