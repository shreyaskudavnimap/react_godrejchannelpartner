import {
  SET_BOOKING_ENQUIRY_DETAILS_ARR,
  CLEAR_BOOKING_ENQUIRY_DETAILS_ARR,
} from '../actions/bookingEnquiryAction';

const initialState = {
  bookingEnquiryData: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_BOOKING_ENQUIRY_DETAILS_ARR:
      return {...state, bookingEnquiryData: action.data};

    case CLEAR_BOOKING_ENQUIRY_DETAILS_ARR:
      return {...initialState};
  }

  return state;
};
