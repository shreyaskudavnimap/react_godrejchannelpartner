import {
  SET_CONTINUE_EXPLORING,
  UPDATE_CONTINUE_EXPLORING,
  UPDATE_CONTINUE_EXPLORING_ARR,
} from '../actions/homeContinueExploringAction';

const initialState = {
  continueExploring: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CONTINUE_EXPLORING:
      return {continueExploring: action.data};

    case UPDATE_CONTINUE_EXPLORING:
      const continueExploringData = {...state.continueExploring};
      if (
        continueExploringData &&
        continueExploringData.data &&
        continueExploringData.data.length > 0
      ) {
        continueExploringData.data.map((mapItem) => {
          if (mapItem.proj_id == action.data) {
            console.log('findIndex: ', mapItem.proj_id + ' -- ' + action.data);
            mapItem.wishlist_status =
              mapItem.wishlist_status == '0' ? '1' : '0';
          }
        });
      }

      return {continueExploring: continueExploringData};

    case UPDATE_CONTINUE_EXPLORING_ARR:
      const continueExploringDataArr = {...state.continueExploring};
      if (
        continueExploringDataArr &&
        continueExploringDataArr.data &&
        continueExploringDataArr.data.length > 0 &&
        action.data &&
        action.data.length > 0
      ) {
        continueExploringDataArr.data.map((mapItem) => {
          action.data.map((dataItem) => {
            if (mapItem.proj_id == dataItem.proj_id) {
              console.log(
                'findIndex: ',
                mapItem.proj_id + ' -- ' + dataItem.proj_id,
              );
              mapItem.wishlist_status =
                mapItem.wishlist_status == '0' ? '1' : '0';
            }
          });
        });
      }

      return {continueExploring: continueExploringDataArr};
  }
  return state;
};
