import {SET_CMS_DATA} from '../actions/projectCmsAction';

const initialState = {
  cmsData: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CMS_DATA:
      return {cmsData: action.data};
  }
  return state;
};
