import {SEND_OTP} from '../actions/sendOtpAction';

const initialState = {
  sendOtpResponse: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SEND_OTP:
      return {sendOtpResponse: action.data};
  }
  return state;
};
