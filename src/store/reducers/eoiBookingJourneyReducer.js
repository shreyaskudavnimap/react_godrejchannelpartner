import {
  SET_BOOKING_PROPERTY_INFO_EOI,
  SET_BOOKING_ENQUIRY_DATA_EOI,
} from '../actions/eoiBookingJourneyAction';

const initialState = {
  bookingPropertyInfo: {},
  bookingEnquiryData: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_BOOKING_PROPERTY_INFO_EOI:
      return {...state, bookingPropertyInfo: action.data};

    case SET_BOOKING_ENQUIRY_DATA_EOI:
      return {...state, bookingEnquiryData: action.data};
  }

  return state;
};
