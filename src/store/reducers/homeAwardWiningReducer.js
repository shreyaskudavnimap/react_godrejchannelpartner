import {SET_AWARD_WINING, UPDATE_AWARD_WINING, UPDATE_AWARD_WINING_ARR} from '../actions/homeAwardWiningAction';

const initialState = {
  awardWining: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_AWARD_WINING:
      return {awardWining: action.data};
      
      case UPDATE_AWARD_WINING:

        const awardWiningData = {...state.awardWining};
        if (
          awardWiningData &&
          awardWiningData.data &&
          awardWiningData.data.length > 0
        ) {
          awardWiningData.data.map((mapItem) => {
            if (mapItem.proj_id == action.data) {
              console.log('findIndex: ', mapItem.proj_id + ' -- ' + action.data);
              mapItem.wishlist_status = 
                mapItem.wishlist_status == '0' ? '1' : '0';
            }
          });
        }

        return {awardWining: awardWiningData};
      
        case UPDATE_AWARD_WINING_ARR:
  
          const awardWiningDataArr = {...state.awardWining};
          if (
            awardWiningDataArr &&
            awardWiningDataArr.data &&
            awardWiningDataArr.data.length > 0 &&
            action.data &&
            action.data.length > 0
          ) {
            awardWiningDataArr.data.map((mapItem) => {
              action.data.map((dataItem) => {
                if (mapItem.proj_id == dataItem.proj_id) {
                  console.log(
                    'findIndex: ',
                    mapItem.proj_id + ' -- ' + dataItem.proj_id,
                  );
                  mapItem.wishlist_status =
                    mapItem.wishlist_status == '0' ? '1' : '0';
                }
              });
            });
          }
  
          return {awardWining: awardWiningDataArr};
  }
  return state;
};
