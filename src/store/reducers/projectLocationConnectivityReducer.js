import {
  SET_LOCATION_CONNECTIVITY,
  SET_NEARBY_POINTS,
  SET_NEARBY_POINTS_DETAILS,
} from '../actions/projectLocationConnectivityAction';

const initialState = {
  locationConnectivity: {},
  nearbyPoints: [],
  nearbyPointsDetails: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_LOCATION_CONNECTIVITY:
      return {...state, locationConnectivity: action.data};

    case SET_NEARBY_POINTS:
      return {...state, nearbyPoints: action.data};

    case SET_NEARBY_POINTS_DETAILS:
      return {...state, nearbyPointsDetails: action.data};
  }
  return state;
};
