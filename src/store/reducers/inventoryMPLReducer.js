import {
  SET_TYPOLOGY_LIST,
  UPDATE_TYPOLOGY_LIST,
  SET_TOWER_LIST,
  UPDATE_TOWER_LIST,
  SET_FLOOR_LIST,
  UPDATE_FLOOR_LIST,
  SET_INVENTORY_LIST,
  UPDATE_INVENTORY_LIST,
  RESET_INVENTORY_LIST,
  UPDATE_INVENTORY_PLAN,
  UPDATE_INVENTORY_WISHLIST,
  UPDATE_INVENTORY_WISHLIST_ARR,
} from '../actions/inventoryMPLAction';

const initialState = {
  typologyList: {},
  towerList: [],
  floorList: [],
  inventoryList: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_TYPOLOGY_LIST:
      const typologyData = action.data;

      if (typologyData.data && typologyData.data.length > 0) {
        typologyData.data.map((item, index) => {
          if (index === 0) {
            item['selectedTypology'] = true;
          } else {
            item['selectedTypology'] = false;
          }

          if (item.title) {
            let scssTypology = item.title
              .split(' ')
              .reverse()
              .join('_')
              .toLowerCase()
              .replace('rhk', 'bhk')
              .replace('bed', 'bhk')
              .replace('+', 'plus')
              .replace('.', '_');

            if (scssTypology) {
              item['scssTypology'] = scssTypology;
            } else {
              item['scssTypology'] = 'secondary';
            }
          } else {
            item['scssTypology'] = 'secondary';
          }
        });
      }

      return {
        ...state,
        typologyList: typologyData,
        towerList: [],
        floorList: [],
        inventoryList: [],
      };

    case UPDATE_TYPOLOGY_LIST:
      const preTypologyList = state.typologyList;
      const updatedTypology = action.data;

      preTypologyList.data.map((typology) => {
        if (
          typology.field_typology_tag === updatedTypology.field_typology_tag
        ) {
          if (!typology.selectedTypology) typology.selectedTypology = true;
        } else {
          typology.selectedTypology = false;
        }
      });

      return {
        ...state,
        typologyList: preTypologyList,
        towerList: [],
        floorList: [],
        inventoryList: [],
      };

    case SET_TOWER_LIST:
      const towerData = action.data;

      if (towerData && towerData.length > 0) {
        towerData.map((item, index) => {
          item['selectedTower'] = false;
        });
      }

      return {
        ...state,
        towerList: towerData,
        floorList: [],
        inventoryList: [],
      };

    case UPDATE_TOWER_LIST:
      const preTowerList = state.towerList;
      const updatedTower = action.data;

      preTowerList.map((tower) => {
        if (tower.id === updatedTower.id) {
          tower.selectedTower = !tower.selectedTower;
        } else {
          tower.selectedTower = false;
        }
      });

      return {
        ...state,
        towerList: preTowerList,
        floorList: [],
        inventoryList: [],
      };

    case SET_FLOOR_LIST:
      const floorData = action.data;

      if (floorData && floorData.length > 0) {
        floorData.map((item, index) => {
          item['selectedFloor'] = false;
        });
      }

      return {...state, floorList: floorData, inventoryList: []};

    case UPDATE_FLOOR_LIST:
      const preFloorList = state.floorList;
      const updatedFloor = action.data;

      preFloorList.map((floor) => {
        if (floor.id === updatedFloor.id) {
          floor.selectedFloor = !floor.selectedFloor;
        } else {
          floor.selectedFloor = false;
        }
      });

      return {
        ...state,
        floorList: preFloorList,
        inventoryList: [],
      };

    case SET_INVENTORY_LIST:
      const inventoryData = action.data;

      if (inventoryData && inventoryData.length > 0) {
        inventoryData.map((item, index) => {
          item['selectedInventory'] = false;
          item['isSelectedFloorPlan'] = true;
        });
      }

      return {...state, inventoryList: inventoryData};

    case UPDATE_INVENTORY_LIST:
      const preInventoryList = state.inventoryList;
      const updatedInventory = action.data;

      preInventoryList.map((inventory) => {
        if (inventory.id === updatedInventory.id) {
          inventory.selectedInventory = !inventory.selectedInventory;
          inventory.isSelectedFloorPlan = true;

          // inventory.show_floor_plan = '0';
          // inventory.show_unit_plan = '1';
          // inventory.floor_img =
          //   'https://dxdwcef76c2aw.cloudfront.net/sites/default/files/2020-06/02_BHK_TYPE-01_UNIT_FLOOR-2_TO_19_UNIT_NO-3B-207_TO_3B-1907_4B-205_TO_4B-1805.jpg';
          // inventory.inv_img =
          //   'https://dxdwcef76c2aw.cloudfront.net/sites/default/files/2020-07/02_BHK_TYPE-01_UNIT_FLOOR-2_TO_19_UNIT_NO-3B-207_TO_3B-1907_4B-205_TO_4B-1805.jpg';
        } else {
          inventory.selectedInventory = false;
        }
      });

      return {...state, inventoryList: preInventoryList};

    case RESET_INVENTORY_LIST:
      const resetFloorList = state.floorList;

      resetFloorList.map((floor) => {
        floor.selectedFloor = false;
      });

      return {
        ...state,
        floorList: resetFloorList,
        inventoryList: [],
      };

    case UPDATE_INVENTORY_PLAN:
      const preInventory = [...state.inventoryList];
      const index = preInventory.indexOf(action.data);

      if (index > -1) {
        preInventory[index].isSelectedFloorPlan = !preInventory[index]
          .isSelectedFloorPlan;
      }

      return {...state, inventoryList: preInventory};

    case UPDATE_INVENTORY_WISHLIST:
      const preInventoryWishlist = [...state.inventoryList];
      const indexWishlist = preInventoryWishlist.indexOf(action.data);

      if (indexWishlist > -1) {
        preInventoryWishlist[indexWishlist].wishlist_status =
          preInventoryWishlist[indexWishlist].wishlist_status == '0'
            ? '1'
            : '0';
      }

      return {...state, inventoryList: preInventoryWishlist};

    case UPDATE_INVENTORY_WISHLIST_ARR:
      const preInventoryWishlistArr = [...state.inventoryList];

      if (
        preInventoryWishlistArr &&
        preInventoryWishlistArr &&
        preInventoryWishlistArr.length > 0 &&
        action.data &&
        action.data.length > 0
      ) {
        preInventoryWishlistArr.map((mapItem) => {
          action.data.map((dataItem) => {
            if (mapItem.id == dataItem.nid) {
              console.log('findIndex: ', mapItem.id + ' -- ' + dataItem.nid);
              mapItem.wishlist_status =
                mapItem.wishlist_status == '0' ? '1' : '0';
            }
          });
        });
      }

      return {...state, inventoryList: preInventoryWishlistArr};
  }
  return state;
};
