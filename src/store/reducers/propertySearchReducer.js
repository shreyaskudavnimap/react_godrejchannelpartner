import {act} from 'react-test-renderer';
import {
  SET_PROPERTY_SEARCH_DATA,
  SET_CITY,
  SET_PROJECT_STATUS,
  SET_TYPOLOGY,
  SET_LAST_SEARCH,
  RESET_FILTERS,
  SET_BUDGET,
  SET_LAST_SEARCH_PEGINATION,
  SET_LAST_DESELECT,
  UPDATE_LAST_SEARCH,
} from '../actions/propertySearchAction';

const initialState = {
  cityTermMenu: [],
  projectStatus: [],
  typologyTermMenu: [],
  lastSearch: '',
  lastDesect: '',
  priceRange: {},
  resultPrice: [],
  projectStatus_selected: [],
  typologyData_selected: [],
  priceLimit: {},
  selected_city: [],
  searchResult: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PROPERTY_SEARCH_DATA:
      let cityTermMenu = action.data.city_term_menu;
      if (cityTermMenu && cityTermMenu.length > 0) {
        cityTermMenu = cityTermMenu.map((item) => {
          let preVal = state.cityTermMenu.find((elm) => elm.id === item.id);
          item.selected = !!(preVal && preVal.selected);
          return item;
        });
      }

      let projectStatus = action.data.project_status;
      if (projectStatus && projectStatus.length > 0) {
        projectStatus = projectStatus.map((item) => {
          let preVal = state.projectStatus.find((elm) => elm.id === item.id);
          item.selected = !!(preVal && preVal.selected);
          return item;
        });
      }

      let typologyTermMenu = action.data.typology_term_menu;
      if (typologyTermMenu && typologyTermMenu.length > 0) {
        typologyTermMenu = typologyTermMenu.map((item) => {
          let preVal = state.typologyTermMenu.find((elm) => elm.id === item.id);
          item.selected = !!(preVal && preVal.selected);
          return item;
        });
      }

      let priceRange = null;
      console.log('filter-cocus', action.isFilterFocus);
      if (action.isFilterFocus) {
        if (Object.keys(state.priceRange).length > 0) {
          priceRange = state.priceRange;
          if (
            parseInt(priceRange.min) < parseInt(action.data.budget_data.min)
          ) {
            priceRange.min = action.data.budget_data.min;
          }

          if (
            parseInt(priceRange.max) > parseInt(action.data.budget_data.max)
          ) {
            priceRange.max = action.data.budget_data.max;
          }
        } else {
          priceRange = action.data.budget_data
            ? action.data.budget_data
            : {min: 0, max: 0};
        }
      } else {
        priceRange = action.data.budget_data
          ? action.data.budget_data
          : {min: 0, max: 0};
      }

      return {
        ...state,
        cityTermMenu: cityTermMenu,
        projectStatus: projectStatus,
        typologyTermMenu: typologyTermMenu,
        priceRange: priceRange,
        resultPrice: [priceRange.min.toString(), priceRange.max.toString()],
        priceLimit:
          Object.keys(state.priceLimit).length === 0
            ? action.data.budget_data
            : state.priceLimit,
      };

    case SET_CITY:
      const updatedCity = action.data;
      const cityTermMenuUpdated = action.selected;

      let projectStatusCity = updatedCity.project_status;
      if (projectStatusCity && projectStatusCity.length > 0) {
        projectStatusCity = projectStatusCity.map((item) => {
          let preVal = state.projectStatus.find((elm) => elm.id === item.id);
          item.selected = !!(preVal && preVal.selected);
          return item;
        });
      }

      let typologyTermMenuCity = updatedCity.typology_term_menu;
      if (typologyTermMenuCity && typologyTermMenuCity.length > 0) {
        typologyTermMenuCity = typologyTermMenuCity.map((item) => {
          let preVal = state.typologyTermMenu.find((elm) => elm.id === item.id);
          item.selected = !!(preVal && preVal.selected);
          return item;
        });
      }

      let priceRangeCity = null;
      if (action.isFilterFocus) {
        if (Object.keys(state.priceRange).length > 0) {
          priceRangeCity = state.priceRange;
          if (
            parseInt(priceRangeCity.min) < parseInt(action.data.budget_data.min)
          ) {
            priceRangeCity.min = action.data.budget_data.min;
          }

          if (
            parseInt(priceRangeCity.max) > parseInt(action.data.budget_data.max)
          ) {
            priceRangeCity.max = action.data.budget_data.max;
          }
        } else {
          priceRangeCity = action.data.budget_data
            ? action.data.budget_data
            : {min: 0, max: 0};
        }
      } else {
        priceRangeCity = action.data.budget_data
          ? action.data.budget_data
          : {min: 0, max: 0};
      }

      return {
        ...state,
        cityTermMenu: cityTermMenuUpdated,
        priceRange: priceRangeCity,
        resultPrice: [
          priceRangeCity.min.toString(),
          priceRangeCity.max.toString(),
        ],
        projectStatus: projectStatusCity ? projectStatusCity : [],
        typologyTermMenu: typologyTermMenuCity ? typologyTermMenuCity : [],
      };

    case SET_PROJECT_STATUS:
      const updatedPossession = action.data;
      const possessionSelected = action.selected;

      let cityTermMenuPS = updatedPossession.city_term_menu;
      if (cityTermMenuPS && cityTermMenuPS.length > 0) {
        cityTermMenuPS = cityTermMenuPS.map((item) => {
          let preVal = state.cityTermMenu.find((elm) => elm.id === item.id);
          item.selected = !!(preVal && preVal.selected);
          return item;
        });
      }

      let typologyTermMenuPS = updatedPossession.typology_term_menu;
      if (typologyTermMenuPS && typologyTermMenuPS.length > 0) {
        typologyTermMenuPS = typologyTermMenuPS.map((item) => {
          let preVal = state.typologyTermMenu.find((elm) => elm.id === item.id);
          item.selected = !!(preVal && preVal.selected);
          return item;
        });
      }

      let priceRangePossession = !updatedPossession.budget_data
        ? {min: 0, max: 0}
        : updatedPossession.budget_data
        ? updatedPossession.budget_data
        : state.priceRange;

      return {
        ...state,
        cityTermMenu: cityTermMenuPS ? cityTermMenuPS : [],
        projectStatus: possessionSelected,
        priceRange: priceRangePossession,
        resultPrice: [
          priceRangePossession.min.toString(),
          priceRangePossession.max.toString(),
        ],
        typologyTermMenu: typologyTermMenuPS ? typologyTermMenuPS : [],
      };

    case SET_TYPOLOGY:
      const updatedTypology = action.data;
      const typologySelected = action.selected;

      let cityTermMenuTL = updatedTypology.city_term_menu;
      if (cityTermMenuTL && cityTermMenuTL.length > 0) {
        cityTermMenuTL = cityTermMenuTL.map((item) => {
          let preVal = state.cityTermMenu.find((elm) => elm.id === item.id);
          item.selected = !!(preVal && preVal.selected);
          return item;
        });
      }

      let projectStatusTL = updatedTypology.project_status;
      if (projectStatusTL && projectStatusTL.length > 0) {
        projectStatusTL = projectStatusTL.map((item) => {
          let preVal = state.projectStatus.find((elm) => elm.id === item.id);
          item.selected = !!(preVal && preVal.selected);
          return item;
        });
      }

      let priceRangeTL = !updatedTypology.budget_data
        ? {min: 0, max: 0}
        : updatedTypology.budget_data
        ? updatedTypology.budget_data
        : state.priceRange;

      return {
        ...state,
        cityTermMenu: cityTermMenuTL ? cityTermMenuTL : [],
        projectStatus: projectStatusTL ? projectStatusTL : [],
        priceRange: priceRangeTL,
        resultPrice: [priceRangeTL.min.toString(), priceRangeTL.max.toString()],
        typologyTermMenu: typologySelected,
      };
    case SET_BUDGET:
      const updatedBudget = action.data;
      const budgetSelected = action.selected;

      let cityTermMenuB = updatedBudget.city_term_menu;
      if (cityTermMenuB && cityTermMenuB.length > 0) {
        cityTermMenuB = cityTermMenuB.map((item) => {
          let preVal = state.cityTermMenu.find((elm) => elm.id === item.id);
          item.selected = !!(preVal && preVal.selected);
          return item;
        });
      }

      let projectStatusB = updatedBudget.project_status;
      if (projectStatusB && projectStatusB.length > 0) {
        projectStatusB = projectStatusB.map((item) => {
          let preVal = state.projectStatus.find((elm) => elm.id === item.id);
          item.selected = !!(preVal && preVal.selected);
          return item;
        });
      }

      let typologyTermMenuB = updatedBudget.typology_term_menu;
      if (typologyTermMenuB && typologyTermMenuB.length > 0) {
        typologyTermMenuB = typologyTermMenuB.map((item) => {
          let preVal = state.typologyTermMenu.find((elm) => elm.id === item.id);
          item.selected = !!(preVal && preVal.selected);
          return item;
        });
      }

      return {
        ...state,
        cityTermMenu: cityTermMenuB ? cityTermMenuB : [],
        projectStatus: projectStatusB ? projectStatusB : [],
        priceRange: {min: budgetSelected.min, max: budgetSelected.max},
        resultPrice: [
          budgetSelected.min.toString(),
          budgetSelected.max.toString(),
        ],
        typologyTermMenu: typologyTermMenuB ? typologyTermMenuB : [],
      };
    case SET_LAST_SEARCH:
      const lastData = action.data;
      // const possession_selected = action.possession;
      const possession_selected = action.possession.filter(
        (elm) => elm.selected === true,
      );
      const typology_selected = action.typology.filter(
        (elm) => elm.selected === true,
      );
      const result_price = [
        state.priceRange.min.toString(),
        state.priceRange.max.toString(),
      ];
      const selected_location = action.location.filter(
        (elm) => elm.selected === true,
      );
      // possession_selected.add(action.possession)

      // const typology_selected = action.typology

      return {
        ...state,
        searchResult: lastData,
        projectStatus_selected: possession_selected,
        typologyData_selected: typology_selected,
        resultPrice: result_price,
        selected_city: selected_location,
      };
    case SET_LAST_SEARCH_PEGINATION:
      const prevData = {...state.searchResult};
      let lastDataPage = [];
      if (action.data.list_data) {
        lastDataPage = prevData.list_data.concat(action.data.list_data);
      } else {
        lastDataPage = prevData.list_data;
      }
      prevData.list_data = lastDataPage;
      return {
        ...state,
        searchResult: prevData,
      };
    case SET_LAST_DESELECT:
      return {
        ...state,
        lastDesect:
          typeof action.payload !== 'string'
            ? JSON.stringify(action.payload)
            : action.payload,
      };

    case UPDATE_LAST_SEARCH:
      const prevDataObj = {...state.searchResult};

      if (
        prevDataObj &&
        prevDataObj.list_data &&
        prevDataObj.list_data.length > 0 &&
        action.data &&
        action.data.length > 0
      ) {
        prevDataObj.list_data.map((mapItem) => {
          action.data.map((dataItem) => {
            if (mapItem.nid == dataItem.proj_id) {
              console.log(
                'findIndex: ',
                mapItem.nid + ' -- ' + dataItem.proj_id,
              );
              mapItem.wishlist_status =
                mapItem.wishlist_status == '0' ? '1' : '0';
            }
          });
        });
      }

      return {
        ...state,
        searchResult: prevDataObj,
      };

    case RESET_FILTERS:
      return {...initialState};
  }
  return state;
};
