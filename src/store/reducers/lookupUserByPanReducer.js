import {LOOKUP_USER_BY_PAN} from '../actions/lookupUserByPanAction';

const initialState = {
  lookupUserPanInfo: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOOKUP_USER_BY_PAN:
      return {lookupUserPanInfo: action.data};
  }
  return state;
};
