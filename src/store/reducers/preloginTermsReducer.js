import {SET_LOGIN_TERMS_DATA} from '../actions/preloginTermsAction';

const initialState = {
  termsSelected: ''
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_LOGIN_TERMS_DATA:
      return {
        ...state,
        ...action.data,
      };
  }

  return state;
};
