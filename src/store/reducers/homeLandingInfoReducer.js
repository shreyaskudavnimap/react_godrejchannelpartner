import {SET_LANDING_INFO} from '../actions/homeLandingInfoAction';

const initialState = {
  landingInfo: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_LANDING_INFO:
      return {landingInfo: action.data};
  }
  return state;
};
