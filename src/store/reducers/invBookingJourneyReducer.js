import {
  SET_PAYMENT_PLAN,
  UPDATE_PAYMENT_PLAN,
  SET_PAYMENT_PLAN_DETAILS,
  SET_COST_SHEET,
  SET_BOOKING_PROPERTY_INFO,
  SET_SOURCE_PROTECTION,
  SET_BOOKING_ENQUIRY_DATA,
  SET_PROMO_CODE,
  SET_BOOKING_AMOUNT,
} from '../actions/invBookingJourneyAction';

const initialState = {
  paymentPlan: {},
  paymentPlanDetails: {},
  costSheet: {},
  promoCode: {
    payment_plan_id: '',
    total_new_price: '',
    total_old_price: '',
    disc_amt: '',
    disc_upto_limit: '',
    disc_type: '',
    disc_percentage: '',
    promo_id: '',
    promo_code: '',
  },
  bookingPropertyInfo: {},
  sourceProtection: {},
  bookingEnquiryData: [],
  bookingAmount: {
    booking_amount_radio: true,
    booking_amount: '',
    min_amount: '',
    max_amount: '',
    amount_paid: '',
    remaining_amount: '',
    custom_amount: '',
    payment_method: 'full', //custom/full
    is_booking_amount_error: false,
    booking_id: null,
    booking_no: null,
    res_booking_amount: null,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PAYMENT_PLAN:
      const paymentPlan = action.data;
      const plans = paymentPlan.data.curl_response.payment_plans;
      if (plans && plans.length > 0) {
        plans.map((item, index) => {
          if (index === 0) {
            item['plan_selected'] = '1';
            item['plan_recommended'] = '1';
          } else {
            item['plan_selected'] = '0';
            item['plan_recommended'] = '0';
          }
        });
      }

      return {...state, paymentPlan: paymentPlan};

    case UPDATE_PAYMENT_PLAN:
      const paymentPlanUpdate = {...state.paymentPlan};
      const plansUpdate = paymentPlanUpdate.data.curl_response.payment_plans;
      if (plansUpdate && plansUpdate.length > 0) {
        plansUpdate.map((item, index) => {
          if (item.payment_plan_id == action.data.payment_plan_id) {
            item['plan_selected'] = '1';
          } else {
            item['plan_selected'] = '0';
          }
        });
      }

      // console.log('Plan Pre: ', state.paymentPlan);
      // console.log('Plan Update: ', paymentPlanUpdate);

      return {...state, paymentPlan: paymentPlanUpdate};

    case SET_PAYMENT_PLAN_DETAILS:
      return {...state, paymentPlanDetails: action.data};

    case SET_COST_SHEET:
      return {...state, costSheet: action.data};

    case SET_BOOKING_PROPERTY_INFO:
      return {...state, bookingPropertyInfo: action.data};

    case SET_SOURCE_PROTECTION:
      return {...state, sourceProtection: action.data};

    case SET_BOOKING_ENQUIRY_DATA:
      return {...state, bookingEnquiryData: action.data};

    case SET_PROMO_CODE:
      return {...state, promoCode: action.data};

    case SET_BOOKING_AMOUNT:
      return {...state, bookingAmount: action.data};
  }

  return state;
};
