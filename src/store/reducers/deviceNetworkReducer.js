import {IS_NETWORK_CONNECTED} from '../actions/deviceNetworkAction';

const initialState = {
  isNetworkConnected: true,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case IS_NETWORK_CONNECTED:
      // console.log(action)
      return {
        ...state,
        isNetworkConnected: action.isNetworkConnected,
      };
  }

  return state;
};
