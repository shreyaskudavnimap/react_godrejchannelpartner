import {
  SET_PRROJECT_DETAILS,
  SET_PRROJECT_GALLERY,
  RESET_PRROJECT_GALLERY,
  SET_PRROJECT_FEATURED_MENU,
  UPDATE_PROJECT_WISHLIST,
  RESET_PROJECT_DETAILS,
} from '../actions/projectDetailsAction';

const initialState = {
  projectDetails: [],
  projectGallery: [],
  projectGallerySlice: [],
  projectFeaturedMenu: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case RESET_PROJECT_DETAILS:
      return {
        projectDetails: [],
        projectGallery: [],
        projectGallerySlice: [],
        projectFeaturedMenu: [],
      };

    case SET_PRROJECT_DETAILS:
      return {...state, projectDetails: action.data};

    case SET_PRROJECT_GALLERY:
      const imgData = action.data;
      const galleryImage = imgData[0]['details'];
      const galleryImageSlice = [];

      if (galleryImage && galleryImage.length > 0) {
        galleryImage.map((imgItem, index) => {
          if (index <= 4) {
            galleryImageSlice.push(imgItem);
            return;
          }
        });
      }

      return {
        ...state,
        projectGallery:
          galleryImage && galleryImage.length > 0 ? galleryImage : [],
        projectGallerySlice:
          galleryImageSlice && galleryImageSlice.length > 0
            ? galleryImageSlice
            : [],
      };

    case RESET_PRROJECT_GALLERY:
      return {
        ...state,
        projectGallery: [],
        projectGallerySlice: [],
      };

    case SET_PRROJECT_FEATURED_MENU:
      return {...state, projectFeaturedMenu: action.data};

    case UPDATE_PROJECT_WISHLIST:
      const updatedProectDetails = {...state.projectDetails};
      updatedProectDetails.wishlist_status =
        updatedProectDetails.wishlist_status == '0' ? '1' : '0';
      return {...state, projectDetails: updatedProectDetails};
  }
  return state;
};
