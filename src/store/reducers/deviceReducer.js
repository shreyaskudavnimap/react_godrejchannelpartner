import {Platform} from 'react-native';
import {SET_DEVICE_ID} from '../actions/deviceAction';

const initialState = {
  deviceOS: Platform.OS.toLowerCase(),
  deviceUniqueId: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_DEVICE_ID:
      return {
        ...state,
        deviceUniqueId: action.device_id,
      };
  }

  return state;
};
