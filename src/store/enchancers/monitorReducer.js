/*
 * -enchancres that logs the time taken by each reducers to process each action.
 *
 */

 //import {performance} from 'redux';

const round = number => Math.round(number * 100) / 100

const monitorReducerEnhancer = createStore => (
  reducer,
  initialState,
  enhancer
) => {
  const monitoredReducer = (state, action) => {
    const start = new Date().getTime();//performance.now();
    const newState = reducer(state, action);
    const end = new Date().getTime();//performance.now();
    const diff = round(end - start);

    console.log('reducer process time:', diff);

    return newState;
  }

  return createStore(monitoredReducer, initialState, enhancer);
}

export default monitorReducerEnhancer;
