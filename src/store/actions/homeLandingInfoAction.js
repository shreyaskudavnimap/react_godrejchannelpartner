import apiLandingPage from './../../api/apiLanding';

export const SET_LANDING_INFO = 'SET_LANDING_INFO';

export const getLandingInfo = (userId, deviceId, city, latitude, longitude) => {
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: deviceId,
        city: city,
        lat: latitude,
        lng: longitude,
      };
      
      const result = await apiLandingPage.apiLandingPageVideoInfo(request);

      dispatch({type: SET_LANDING_INFO, data: result.data});
    } catch (error) {
      throw error;
    }
  };
};
