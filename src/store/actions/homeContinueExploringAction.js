import apiLandingPage from './../../api/apiLanding';

export const SET_CONTINUE_EXPLORING = 'SET_CONTINUE_EXPLORING';
export const UPDATE_CONTINUE_EXPLORING = 'UPDATE_CONTINUE_EXPLORING';
export const UPDATE_CONTINUE_EXPLORING_ARR = 'UPDATE_CONTINUE_EXPLORING_ARR';

export const getContinueExpoloringProperty = (userId, deviceId) => {
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: userId === '' ? deviceId : '',
      };

      const result = await apiLandingPage.apiLandingPageContinueExpoloringInfo(
        request,
      );

      dispatch({type: SET_CONTINUE_EXPLORING, data: result.data});
    } catch (error) {
      throw error;
    }
  };
};

export const updateProjetWishlist = (projectId) => {
  return {type: UPDATE_CONTINUE_EXPLORING, data: projectId};
};

export const updateProjetWishlistArr = (projects) => {
  return {type: UPDATE_CONTINUE_EXPLORING_ARR, data: projects};
};
