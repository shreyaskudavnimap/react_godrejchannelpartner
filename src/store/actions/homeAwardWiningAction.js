import apiLandingPage from './../../api/apiLanding';

export const SET_AWARD_WINING = 'SET_AWARD_WINING';
export const UPDATE_AWARD_WINING = 'UPDATE_AWARD_WINING';
export const UPDATE_AWARD_WINING_ARR = 'UPDATE_AWARD_WINING_ARR';

export const getAwardWiningProperty = (userId, deviceId) => {
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: userId === '' ? deviceId: '',
        type: 'award_winning',
      };

      const result = await apiLandingPage.apiProjectByTypeProperty(request);

      dispatch({type: SET_AWARD_WINING, data: result.data});
    } catch (error) {
      throw error;
    }
  };
};

export const updateProjetWishlist = (projectId) => {
  return {type: UPDATE_AWARD_WINING, data: projectId};
};

export const updateProjetWishlistArr = (projects) => {
  return {type: UPDATE_AWARD_WINING_ARR, data: projects};
};