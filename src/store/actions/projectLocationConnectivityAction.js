import apiProjectDetails from '../../api/apiProjectDetails';
import appConstant from '../../utility/appConstant';

export const SET_LOCATION_CONNECTIVITY = 'SET_LOCATION_CONNECTIVITY';
export const SET_NEARBY_POINTS = 'SET_NEARBY_POINTS';
export const SET_NEARBY_POINTS_DETAILS = 'SET_NEARBY_POINTS_DETAILS';

export const getPropertyLocationConnectivity = (projId) => {
  return async (dispatch) => {
    try {
      const request = {proj_id: projId};

      const result = await apiProjectDetails.apiPropertyLocationConnectivity(request);

      if (result.data.status != '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      if (!result.data || !result.data.data) {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      dispatch({type: SET_LOCATION_CONNECTIVITY, data: result.data.data});
    } catch (error) {
      throw error;
    }
  };
};

export const getPropertyNearbyPoints = (projId) => {
  return async (dispatch) => {
    try {
      const request = {proj_id: projId};

      const result = await apiProjectDetails.apiPropertyNearbyPoints(request);

      if (result.data.status != '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      if (!result.data || !result.data.data || !result.data.data.length > 0) {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      dispatch({type: SET_NEARBY_POINTS, data: result.data.data});
    } catch (error) {
      throw error;
    }
  };
};

export const getNearbyPointsDetails = (projId) => {
  return async (dispatch) => {
    try {
      const request = {proj_id: projId};

      const result = await apiProjectDetails.apiNearbyPointsDetails(request);

      if (result.data.status != '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      if (!result.data || !result.data.data || !result.data.data.length > 0) {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      dispatch({type: SET_NEARBY_POINTS_DETAILS, data: result.data.data});
    } catch (error) {
      throw error;
    }
  };
};
