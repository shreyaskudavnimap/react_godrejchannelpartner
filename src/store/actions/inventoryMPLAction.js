import apiInventoryMLP from '../../api/apiInventoryMLP';
import appConstant from '../../utility/appConstant';

export const SET_TYPOLOGY_LIST = 'SET_TYPOLOGY_LIST';
export const UPDATE_TYPOLOGY_LIST = 'UPDATE_TYPOLOGY_LIST';
export const SET_TOWER_LIST = 'SET_TOWER_LIST';
export const UPDATE_TOWER_LIST = 'UPDATE_TOWER_LIST';
export const SET_FLOOR_LIST = 'SET_FLOOR_LIST';
export const UPDATE_FLOOR_LIST = 'UPDATE_FLOOR_LIST';
export const SET_INVENTORY_LIST = 'SET_INVENTORY_LIST';
export const UPDATE_INVENTORY_LIST = 'UPDATE_INVENTORY_LIST';
export const RESET_INVENTORY_LIST = 'RESET_INVENTORY_LIST';
export const UPDATE_INVENTORY_PLAN = 'UPDATE_INVENTORY_PLAN';
export const UPDATE_INVENTORY_WISHLIST = 'UPDATE_INVENTORY_WISHLIST';
export const UPDATE_INVENTORY_WISHLIST_ARR = 'UPDATE_INVENTORY_WISHLIST_ARR';

export const getTypologyList = (userId, deviceId, projectId) => {
  return async (dispatch) => {
    try {
      return new Promise(async (resolve, reject) => {
        const request = {
          user_id: userId,
          device_id: deviceId,
          project_id: projectId,
        };

        const result = await apiInventoryMLP.apiTypologyList(request);
        // console.log(result.data)

        if (result.data.status != '200') {
          const message =
            result && result.data && result.data.msg
              ? result.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR;
          reject({message});
        }

        if (!result.data || !result.data.data) {
          const message = appConstant.appMessage.APP_GENERIC_ERROR;
          reject({message});
        }

        dispatch({type: SET_TYPOLOGY_LIST, data: result.data});

        resolve(result.data);
      });
    } catch (error) {
      throw error;
    }
  };
};

export const updateTypologyList = (selectedTypology) => {
  return {type: UPDATE_TYPOLOGY_LIST, data: selectedTypology};
};

export const getTowerList = (userId, deviceId, projectId, typologyId) => {
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: deviceId,
        project_id: projectId,
        typology_id: typologyId,
      };

      const result = await apiInventoryMLP.apiTowerList(request);
      // console.log(request)

      if (result.data.status != '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      // if (!result.data || !result.data.data || !result.data.data.length > 0) {
      //   throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      // }

      dispatch({type: SET_TOWER_LIST, data: result.data.data});
    } catch (error) {
      throw error;
    }
  };
};

export const updateTowerList = (selectedTower) => {
  return {type: UPDATE_TOWER_LIST, data: selectedTower};
};

export const getFloorList = (
  userId,
  deviceId,
  projectId,
  typologyId,
  towerId,
) => {
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: deviceId,
        project_id: projectId,
        typology_id: typologyId,
        tower_id: towerId,
      };

      const result = await apiInventoryMLP.apiFloorList(request);
      // console.log(result.data);

      if (result.data.status != '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      // if (!result.data || !result.data.data || !result.data.data.length > 0) {
      //   throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      // }

      dispatch({type: SET_FLOOR_LIST, data: result.data.data});
    } catch (error) {
      throw error;
    }
  };
};

export const updateFloorList = (selectedFloor) => {
  return {type: UPDATE_FLOOR_LIST, data: selectedFloor};
};

export const getInventoryList = (
  userId,
  deviceId,
  projectId,
  typologyId,
  towerId,
  floorId,
) => {
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: deviceId,
        project_id: projectId,
        typology_id: typologyId,
        tower_id: towerId,
        floor_id: floorId,
      };

      const result = await apiInventoryMLP.apiInventoryList(request);
      // console.log(result.data);

      if (result.data.status == '502') {
        throw new Error(result.data.msg);
      }

      if (result.data.status != '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      // if (!result.data || !result.data.data || !result.data.data.length > 0) {
      //   throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      // }

      dispatch({type: SET_INVENTORY_LIST, data: result.data.data});
    } catch (error) {
      throw error;
    }
  };
};

export const updateInventoryList = (selectedInventory) => {
  return {type: UPDATE_INVENTORY_LIST, data: selectedInventory};
};

export const resetInventoryList = () => {
  return {type: RESET_INVENTORY_LIST};
};

export const updateInventoryPlan = (selectedInventory) => {
  return {type: UPDATE_INVENTORY_PLAN, data: selectedInventory};
};

export const updateInventoryWishlist = (selectedInventory) => {
  return {type: UPDATE_INVENTORY_WISHLIST, data: selectedInventory};
};

export const updateInventoryWishlistArr = (selectedInventoryArr) => {
  return {type: UPDATE_INVENTORY_WISHLIST_ARR, data: selectedInventoryArr};
};
