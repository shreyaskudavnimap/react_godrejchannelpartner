import apiDashboard from '../../api/apiDashboard';
import appConstant from '../../utility/appConstant';
import {DASHBOARD} from '../../utility/ReduxEvents';
import configureStore from '../Store';
import apiProjectDetails from '../../api/apiProjectDetails';

export const getRMInfo = (userId) => {
  return async (dispatch) => {
    try {
      let request = {user_id: userId};
      const result = await apiDashboard.apiGetRMInfo(request);

      if (result.data.status !== '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }
      if (!result.data || !result.data.data || !result.data.data.length > 0) {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }
      dispatch({type: DASHBOARD.SET_RM_DATA, data: result.data.data});
    } catch (error) {
      throw error;
    }
  };
};

export const getRMInfoOffline = (data) => {
  return (dispatch) => {
    dispatch({type: DASHBOARD.SET_RM_DATA, data: data});
  };
};

export const setRMImages = (
  propertyID,
  index,
  rmData,
  setLoadCount = undefined,
) => {
  return async (dispatch) => {
    try {
      const result = await apiDashboard.getPropertyGallery(propertyID);
      console.log('imgres', result);
      if (!result.data || result.data.status !== '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      } else {
        if (
          result.data['0'] &&
          result.data['0'].details &&
          result.data['0'].details.length > 0
        ) {
          if (rmData.length > 0) {
            let tempArr = [];
            let details = result.data['0'].details;
            for (let item of details) {
              tempArr.push({
                thumbUrl: item.field_gallery_thumb_image_url,
                url: item.field_gallery_image_url,
                caption: item.field_caption,
              });
            }
            if (tempArr.length > 0) {
              rmData[index].galleryImages = tempArr;
              dispatch({type: DASHBOARD.SET_RM_DATA, data: rmData});
              if (setLoadCount) {
                setTimeout(() => {
                  setLoadCount(Date.now() + (Math.random() + 1));
                });
              }
            }
          }
        }
      }
    } catch (error) {
      throw error;
    }
  };
};

export const setRMVideo = (
  userId,
  deviceID,
  propertyID,
  index,
  rmData,
  setLoadCount = undefined,
) => {
  return async (dispatch) => {
    try {
      const result = await apiProjectDetails.apiProjectDetailsInfo({
        user_id: userId,
        device_id: deviceID,
        project_id: propertyID,
      });
      if (!result.data || result.data.status !== '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      } else {
        console.log('vidss', result);
        if (result.data && result.data.data && result.data.data.length > 0) {
          if (rmData.length > 0) {
            let details = result.data.data;
            if (details[0].proj_vid) {
              rmData[index].video = {
                url: details[0].proj_vid,
                thumb_url: details[0].field_project_details_video_thum,
              };
              dispatch({type: DASHBOARD.SET_RM_DATA, data: rmData});
              if (setLoadCount) {
                setTimeout(() => {
                  setLoadCount(Date.now() + (Math.random() + 1));
                });
              }
            }
          }
        }
      }
    } catch (e) {
      throw e;
    }
  };
};
