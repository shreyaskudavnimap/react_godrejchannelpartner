import apiEoiMLP from '../../api/apiEoiMLP';
import appConstant from '../../utility/appConstant';

export const SET_TYPOLOGY_LIST_EOI = 'SET_TYPOLOGY_LIST_EOI';
export const UPDATE_TYPOLOGY_LIST_EOI = 'UPDATE_TYPOLOGY_LIST_EOI';
export const SET_TOWER_LIST_EOI = 'SET_TOWER_LIST_EOI';
export const UPDATE_TOWER_LIST_EOI = 'UPDATE_TOWER_LIST_EOI';
export const RESET_TOWER_LIST_EOI = 'RESET_TOWER_LIST_EOI';
export const SET_FLOOR_BAND_LIST_EOI = 'SET_FLOOR_BAND_LIST_EOI';
export const UPDATE_FLOOR_BAND_LIST_EOI = 'UPDATE_FLOOR_BAND_LIST_EOI';
export const UPDATE_FLOOR_BAND_PLAN_EOI = 'UPDATE_FLOOR_BAND_PLAN_EOI';
export const RESET_FLOOR_BAND_LIST_EOI = 'RESET_FLOOR_BAND_LIST_EOI';

export const UPDATE_EOI_LIST_EOI_BOOKING = 'UPDATE_EOI_LIST_EOI_BOOKING';
export const SET_TYPOLOGY_LIST_EOI_BOOKING = 'SET_TYPOLOGY_LIST_EOI_BOOKING';
export const UPDATE_TYPOLOGY_LIST_EOI_BOOKING =
  'UPDATE_TYPOLOGY_LIST_EOI_BOOKING';
export const SET_TOWER_LIST_EOI_BOOKING = 'SET_TOWER_LIST_EOI_BOOKING';
export const UPDATE_TOWER_LIST_EOI_BOOKING = '_BOOKING';
export const RESET_TOWER_LIST_EOI_BOOKING = 'RESET_TOWER_LIST_EOI_BOOKING';
export const SET_FLOOR_BAND_LIST_EOI_BOOKING =
  'SET_FLOOR_BAND_LIST_EOI_BOOKING';
export const UPDATE_FLOOR_BAND_LIST_EOI_BOOKING =
  'UPDATE_FLOOR_BAND_LIST_EOI_BOOKING';
export const UPDATE_FLOOR_BAND_PLAN_EOI_BOOKING =
  'UPDATE_FLOOR_BAND_PLAN_EOI_BOOKING';
export const RESET_FLOOR_BAND_LIST_EOI_BOOKING =
  'RESET_FLOOR_BAND_LIST_EOI_BOOKING';

export const getEoiFiltersData = (
  userId,
  deviceId,
  projectId,
  type,
  typologyId,
  towerId,
  floorId,
) => {
  return async (dispatch) => {
    try {
      return new Promise(async (resolve, reject) => {
        const request = {
          user_id: userId,
          device_id: deviceId,
          project_id: projectId,
          type: type,
          typology_id: typologyId ? typologyId : '',
          tower_id: towerId ? towerId : '',
          floor_id: floorId ? floorId : '',
        };

        const result = await apiEoiMLP.apiEoiFiltersData(request);
        // console.log(result.data)

        if (result.data.status != '200') {
          const message = appConstant.appMessage.APP_GENERIC_ERROR;
          reject({message});
        }

        if (!result.data || !result.data.data) {
          const message = appConstant.appMessage.APP_GENERIC_ERROR;
          reject({message});
        }

        let actiontype = '';
        if (type) {
          if (type === 'typology') {
            actiontype = SET_TYPOLOGY_LIST_EOI;
          } else if (type === 'tower') {
            actiontype = SET_TOWER_LIST_EOI;
          } else if (type === 'floor_band') {
            actiontype = SET_FLOOR_BAND_LIST_EOI;
          } else {
            actiontype = '';
          }
        }

        dispatch({type: actiontype, data: result.data});

        resolve(result.data);
      });
    } catch (error) {
      throw error;
    }
  };
};

export const updateTypologyList = (selectedTypology) => {
  return {type: UPDATE_TYPOLOGY_LIST_EOI, data: selectedTypology};
};

export const updateTowerList = (selectedTower) => {
  return {type: UPDATE_TOWER_LIST_EOI, data: selectedTower};
};

export const updateFloorBandList = (selectedFloor) => {
  return {type: UPDATE_FLOOR_BAND_LIST_EOI, data: selectedFloor};
};

export const updateFloorBandPlan = (selectedFloor) => {
  return {type: UPDATE_FLOOR_BAND_PLAN_EOI, data: selectedFloor};
};

export const updateEoiListBooking = (
  typologyData,
  towerData,
  floorBandData,
) => {
  return {
    type: UPDATE_EOI_LIST_EOI_BOOKING,
    typologyData: typologyData,
    towerData: towerData,
    floorBandData: floorBandData,
  };
};

export const getEoiFiltersDataBooking = (
  userId,
  deviceId,
  projectId,
  type,
  typologyId,
  towerId,
  floorId,
) => {
  return async (dispatch) => {
    try {
      return new Promise(async (resolve, reject) => {
        const request = {
          user_id: userId,
          device_id: deviceId,
          project_id: projectId,
          type: type,
          typology_id: typologyId ? typologyId : '',
          tower_id: towerId ? towerId : '',
          floor_id: floorId ? floorId : '',
        };

        const result = await apiEoiMLP.apiEoiFiltersData(request);
        // console.log(result.data)

        if (result.data.status != '200') {
          // const message = appConstant.appMessage.APP_GENERIC_ERROR;
          const message =
            result && result.data && result.data.msg
              ? result.data.msg
              : appConstant.appMessage.APP_GENERIC_ERROR;
          reject({message});
        }

        if (!result.data || !result.data.data) {
          const message = appConstant.appMessage.APP_GENERIC_ERROR;
          reject({message});
        }

        let actiontype = '';
        if (type) {
          if (type === 'typology') {
            actiontype = SET_TYPOLOGY_LIST_EOI_BOOKING;
          } else if (type === 'tower') {
            actiontype = SET_TOWER_LIST_EOI_BOOKING;
          } else if (type === 'floor_band') {
            actiontype = SET_FLOOR_BAND_LIST_EOI_BOOKING;
          } else {
            actiontype = '';
          }
        }

        dispatch({
          type: actiontype,
          data: result.data,
          typology_id: typologyId ? typologyId : '',
          tower_id: towerId ? towerId : '',
          floor_id: floorId ? floorId : '',
        });

        resolve(result.data);
      });
    } catch (error) {
      throw error;
    }
  };
};

export const updateTypologyListBooking = (selectedTypology) => {
  return {type: UPDATE_TYPOLOGY_LIST_EOI_BOOKING, data: selectedTypology};
};

export const updateTowerListBooking = (selectedTower) => {
  return {type: UPDATE_TOWER_LIST_EOI_BOOKING, data: selectedTower};
};

export const updateFloorBandListBooking = (selectedFloor) => {
  return {type: UPDATE_FLOOR_BAND_LIST_EOI_BOOKING, data: selectedFloor};
};

export const updateFloorBandPlanBooking = (selectedFloor) => {
  return {type: UPDATE_FLOOR_BAND_PLAN_EOI_BOOKING, data: selectedFloor};
};
