import apiLogin from './../../api/apiLogin';
import AsyncStorage from '@react-native-async-storage/async-storage';
import apiClient from '../../api/client';

export const USER_LOGIN = 'USER_LOGIN';

export const getUserSignupInfo = (fn, ln, email, mo, pwd, dt, di, cd, av) => {
  return async (dispatch) => {
    try {

        const request = {
          first_name:fn,
          last_name: ln,
          email: email,
          mob_no: mo,
          password: pwd,
          device_type: dt,
          device_id: di,
          user_id:'',
          country_code:cd,
          app_version:av,
          app_platform: "react-native"
        };

        const result = await apiLogin.apiForSignUpUser(request);
        console.log('signup result1 = '+JSON.stringify(result));

      if (result.data.status == 200) {
        let sessionKey = [
          ['sessionId', result.data.sessionId],
          ['sessionName', result.data.sessionName],
          ['token', result.data.token],
          ['data', JSON.stringify(result.data.data)],
          ['status', result.data.status],
          ['isLogin', 'true'],
        ];
        //Set request header
        const headers = {
          'X-CSRF-TOKEN': result.data.token ? result.data.token : '',
          'x-requested-with':
            result.data.sessionName && result.data.sessionId
              ? result.data.sessionName + '=' + result.data.sessionId
              : '',
        };

        apiClient.setHeaders(headers);
        //Set request heade
        await AsyncStorage.multiSet(sessionKey, (err, stores) => {
          // console.log('session created successfully');
        });
        dispatch({type: USER_LOGIN, data: {...result.data, isLogin: true}});
      } else {
        if(result.data.msg != null){
          let sessionKey = [
            ['error_msg', result.data.msg],
          ];
          await AsyncStorage.multiSet(sessionKey, (err, stores) => {
            // console.log('session created successfully');
          });
        }
        dispatch({type: USER_LOGIN, data: {...result.data, isLogin: false}});
      }
    } catch (error) {
      throw error;
    }
  };
};

// export const USER_LOGIN = 'USER_LOGIN';

// export const checkLogin = (response) => {
//   return async (dispatch) => {
//     const result = await response;
//     // console.log(result)

//     dispatch({type: USER_LOGIN, data: result});
//   };
// };
