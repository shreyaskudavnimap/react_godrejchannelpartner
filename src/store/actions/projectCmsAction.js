import apiProjectDetails from '../../api/apiProjectDetails';
import appConstant from '../../utility/appConstant';

export const SET_CMS_DATA = 'SET_CMS_DATA';

export const getProjectCMSData = (projId, cmsType) => {
  return async (dispatch) => {
    try {
      let request = {};

      if (cmsType == 'about_godrej') {
        request = {cms_type: cmsType};
      } else {
        request = {
          proj_id: projId,
          cms_type: cmsType,
        };
      }

      const result = await apiProjectDetails.apiProjectCMS(request);
      // console.log(result.data)

      if (result.data.status != '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      if (!result.data || !result.data.data || !result.data.data.length > 0) {
        throw new Error(appConstant.appMessage.NO_DATA);
      }

      dispatch({type: SET_CMS_DATA, data: result.data.data});
    } catch (error) {
      throw error;
    }
  };
};
