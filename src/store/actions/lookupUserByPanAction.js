import apiLogin from './../../api/apiLogin';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const LOOKUP_USER_BY_PAN = 'LOOKUP_USER_BY_PAN';

export const getUserLookupInfo = (pan_no) => {
  return async (dispatch) => {
    try {
      const request = {
        pan: pan_no,
      };
      
      const result = await apiLogin.apiLandingLookupUserByPan(request);

      if(result.data.status == 201){
        let sessionKey = [['lookdata',JSON.stringify(result.data.data)],['lookstatus',result.data.status]];
        await AsyncStorage.multiSet(sessionKey,(err,stores) => {
          console.log("lookupdata set successfully");
        })
      }
      if(result.data.status == 200){
        let sessionKey = [['lookstatus',result.data.status]];
        await AsyncStorage.multiSet(sessionKey,(err,stores) => {
          console.log("lookupdata set successfully");
        })
      }

      dispatch({type: LOOKUP_USER_BY_PAN, data: result.data});
    } catch (error) {
      throw error;
    }
  };
};
