export const SET_BATCH_DATA = 'SET_BATCH_DATA';

export const setBadgeData = (badgeData) => {
  return {type: SET_BATCH_DATA, data: badgeData};
};
