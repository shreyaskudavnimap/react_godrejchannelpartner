export const SET_LOGIN_TERMS_DATA = 'SET_LOGIN_TERMS_DATA';

export const setTermsData = (termsData) => {
  return {type: SET_LOGIN_TERMS_DATA, data: termsData};
};
