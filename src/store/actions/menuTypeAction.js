export const SET_MENU_TYPE = 'SET_MENU_TYPE';

export const setMenuData = (menuData) => {
  return {type: SET_MENU_TYPE, data: menuData};
};
