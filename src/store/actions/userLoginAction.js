import apiLogin from './../../api/apiLogin';
import AsyncStorage from '@react-native-async-storage/async-storage';
import apiClient from '../../api/client';
import {AUTH, DASHBOARD} from '../../utility/ReduxEvents';


export const USER_LOGIN = 'USER_LOGIN';
export const UPDATE_USER_IMAGE = 'UPDATE_USER_IMAGE';

export const getUserLoginInfo = (av, un, pw, di, dt, logintype) => {
  //const navigation = useNavigation();
  return async (dispatch) => {
    try {
      var result = '';
      // if (logintype === 'user_login') {
      //   const request = {
      //     username: un,
      //     password: pw
      //   };
      //   result = await apiLogin.apiLoginUser(request); 
      // }

      if (logintype === 'user_login') {
        const request = {
          app_version: av,
          username: un,
          password: pw,
          device_id: di,
          device_type: dt,
          app_platform:"react-native"
        };

        result = await apiLogin.apiLoginUserByPanNPass(request);
      }

      if (logintype === 'password' || logintype === 'otp') {
        const request = {
          app_version: av,
          username: un,
          password: pw,
          device_id: di,
          device_type: dt,
          login_type: logintype,
          app_platform:"react-native"
        };

        result = await apiLogin.apiLoginUserByMobileNPass(request);
      }

      if (result.data.status == 200) {
        
        let sessionKey = [
          ['sessionId', result.data.sessionId],
          ['sessionName', result.data.sessionName],
          ['token', result.data.token],
          ['data', JSON.stringify(result.data.data)],
          ['status', result.data.status],
          ['isLogin', 'true'],
        ];
        
        //Set request header
        const headers = {
          'X-CSRF-TOKEN': result.data.token ? result.data.token : '',
          'x-requested-with':
            result.data.sessionName && result.data.sessionId
              ? result.data.sessionName + '=' + result.data.sessionId
              : '',
        };

        apiClient.setHeaders(headers);
        //Set request heade
        await AsyncStorage.multiSet(sessionKey, (err) => {
          // console.log('session created successfully');
        });
        dispatch({type: USER_LOGIN, data: {...result.data, isLogin: true}});
      } 
      else {
        dispatch({type: USER_LOGIN, data: {...result.data, isLogin: false}});
      }
    } catch (error) {
      throw error;
    }
  };
};

export const clearRMData = () => {
  return (dispatch) => {
    dispatch({type: DASHBOARD.CLEAR_RM_DATA});
  };
};

export const setLoginParam = (data) => {
  return (dispatch) => {
    dispatch({type: AUTH.SET_LOGIN_PARAM, data: data});
  };
};

export const clearLoginParam = () => {
  return (dispatch) => {
    dispatch({type: AUTH.CLEAR_LOGIN_PARAM});
  };
};

export const updateUserImage = (userImage) => {
  return {type: UPDATE_USER_IMAGE, data: userImage};
};


// export const USER_LOGIN = 'USER_LOGIN';

// export const checkLogin = (response) => {
//   return async (dispatch) => {
//     const result = await response;
//     // console.log(result)

//     dispatch({type: USER_LOGIN, data: result});
//   };
// };

export const updateDeviceToken = (payload) => {
  return async (dispatch) => {
    try {
      var result = '';
        result = await apiLogin.apiForUpdateDeviceToken(payload);
        console.log("login device token set+++++++++++",result);
    } catch (error) {
      throw error;
    }
  };
};