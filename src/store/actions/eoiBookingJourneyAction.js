import apiEoiBookingJourney from '../../api/apiEoiBookingJourney';
import appConstant from '../../utility/appConstant';

export const SET_BOOKING_PROPERTY_INFO_EOI = 'SET_BOOKING_PROPERTY_INFO_EOI';

export const getBookingPropertyInfo = (
  userId,
  projId,
  deviceId,
  typology_id,
  tower_id,
  floor_id,
  enquiryId,
  enquiryName,
) => {
  return async (dispatch) => {
    try {
      return new Promise(async (resolve, reject) => {
        const request = {
          user_id: userId,
          project_id: projId,
          device_id: deviceId,
          typology_id: typology_id,
          tower_id: tower_id,
          floor_id: floor_id,
          enquiry_id: enquiryId,
          enquiry_name: enquiryName,
        };
        const result = await apiEoiBookingJourney.apiBookingPropertyInfo(
          request,
        );

        if (result.data.status != '200') {
          const message = result.data.msg
            ? result.data.msg
            : appConstant.appMessage.APP_GENERIC_ERROR;
          reject({message});
        } else {
          dispatch({type: SET_BOOKING_PROPERTY_INFO_EOI, data: result.data});
          resolve(result.data);
        }
      });
    } catch (error) {
      throw error;
    }
  };
};
