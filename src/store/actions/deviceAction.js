import DeviceInfo from 'react-native-device-info';

export const SET_DEVICE_ID = 'SET_DEVICE_ID';

export const getUniqueId = () => {
  return async (dispatch) => {
    const result = await DeviceInfo.syncUniqueId();
    // console.log(result)

    dispatch({type: SET_DEVICE_ID, device_id: result});
  };
};
