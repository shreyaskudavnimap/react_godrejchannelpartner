import apiLandingPage from './../../api/apiLanding';

export const SET_CITY_WISE_PROJECTS = 'SET_CITY_WISE_PROJECTS';
export const UPDATE_CITY_WISE_PROJECTS = 'UPDATE_CITY_WISE_PROJECTS';
export const UPDATE_CITY_WISE_PROJECTS_ARR = 'UPDATE_CITY_WISE_PROJECTS_ARR';

export const getCityWiseProperty = (
  userId,
  deviceId,
  city,
  latitude,
  longitude,
) => {
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: userId === '' ? deviceId: '',
        city: city,
        lat: latitude,
        lng: longitude,
      };
      
      const result = await apiLandingPage.apiLandingPageCityWiseProperty(
        request,
      );

      dispatch({type: SET_CITY_WISE_PROJECTS, data: result.data});
    } catch (error) {
      throw error;
    }
  };
};

export const updateProjetWishlist = (projectId) => {
  return {type: UPDATE_CITY_WISE_PROJECTS, data: projectId};
};

export const updateProjetWishlistArr = (projects) => {
  return {type: UPDATE_CITY_WISE_PROJECTS_ARR, data: projects};
};
