import apiLandingPage from './../../api/apiLanding';

export const SET_NEW_LAUNCH = 'SET_NEW_LAUNCH';
export const UPDATE_NEW_LAUNCH = 'UPDATE_NEW_LAUNCH';
export const UPDATE_NEW_LAUNCH_ARR = 'UPDATE_NEW_LAUNCH_ARR';

export const getNewLaunchProperty = (userId, deviceId) => {
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: userId === '' ? deviceId: '',
        type: 'new_launch',
      };
      
      const result = await apiLandingPage.apiProjectByTypeProperty(request);

      dispatch({type: SET_NEW_LAUNCH, data: result.data});
    } catch (error) {
      throw error;
    }
  };
};

export const updateProjetWishlist = (projectId) => {
  return {type: UPDATE_NEW_LAUNCH, data: projectId};
};

export const updateProjetWishlistArr = (projects) => {
  return {type: UPDATE_NEW_LAUNCH_ARR, data: projects};
};
