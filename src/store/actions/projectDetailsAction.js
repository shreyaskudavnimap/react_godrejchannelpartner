import apiProjectDetails from './../../api/apiProjectDetails';
import appConstant from '../../utility/appConstant';

export const SET_PRROJECT_DETAILS = 'SET_PRROJECT_DETAILS';
export const SET_PRROJECT_GALLERY = 'SET_PRROJECT_GALLERY';
export const RESET_PRROJECT_GALLERY = 'RESET_PRROJECT_GALLERY';
export const SET_PRROJECT_FEATURED_MENU = 'SET_PRROJECT_FEATURED_MENU';
export const UPDATE_PROJECT_WISHLIST = 'UPDATE_PROJECT_WISHLIST';
export const RESET_PROJECT_DETAILS = 'RESET_PROJECT_DETAILS';



export const resetProjectDetails = () => {
  return {type: RESET_PROJECT_DETAILS};
};

export const getProjectDetals = (userId, deviceId, projectId) => {
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: deviceId,
        project_id: projectId,
      };

      const result = await apiProjectDetails.apiProjectDetailsInfo(request);

      if (result.data.status != '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      if (!result.data || !result.data.data || !result.data.data.length > 0) {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      dispatch({type: SET_PRROJECT_DETAILS, data: result.data.data[0]});
    } catch (error) {
      console.log(error);
      throw error;
    }
  };
};

export const getProjectGallery = (projectId) => {
  return async (dispatch) => {
    try {
      const result = await apiProjectDetails.apiProjectDetailsGallery(
        projectId,
      );

      if (result.data.status != '200' || result.data == {}) {
        dispatch({type: RESET_PRROJECT_GALLERY, data: []});
      } else {
        dispatch({type: SET_PRROJECT_GALLERY, data: result.data});
      }
    } catch (error) {
      throw error;
    }
  };
};

export const getProjectFeaturedMenu = (userId, deviceId, projectId) => {
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: deviceId,
        project_id: projectId,
      };

      const result = await apiProjectDetails.apiProjectFeaturedMenuInfo(
        request,
      );
      // console.log(result)

      if (result.data.status != '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      if (!result.data || !result.data.data || !result.data.data.length > 0) {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      dispatch({type: SET_PRROJECT_FEATURED_MENU, data: result.data.data});
    } catch (error) {
      throw error;
    }
  };
};

export const updateProjetWishlist = () => {
  return {type: UPDATE_PROJECT_WISHLIST};
};
