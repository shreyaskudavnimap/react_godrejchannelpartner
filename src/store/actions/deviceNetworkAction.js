export const IS_NETWORK_CONNECTED = 'IS_NETWORK_CONNECTED';

export const setNetworkInfo = (isNetworkConnected) => {
  return {type: IS_NETWORK_CONNECTED, isNetworkConnected: isNetworkConnected};
};
