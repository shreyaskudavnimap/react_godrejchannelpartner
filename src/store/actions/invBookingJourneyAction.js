import apiInvBookingJourney from '../../api/apiInvBookingJourney';
import appConstant from '../../utility/appConstant';

export const SET_PAYMENT_PLAN = 'SET_PAYMENT_PLAN';
export const UPDATE_PAYMENT_PLAN = 'UPDATE_PAYMENT_PLAN';
export const SET_PAYMENT_PLAN_DETAILS = 'SET_PAYMENT_PLAN_DETAILS';
export const SET_COST_SHEET = 'SET_COST_SHEET';
export const SET_PROMO_CODE = 'SET_PROMO_CODE';
export const SET_BOOKING_PROPERTY_INFO = 'SET_BOOKING_PROPERTY_INFO';
export const SET_SOURCE_PROTECTION = 'SET_SOURCE_PROTECTION';
export const SET_BOOKING_ENQUIRY_DATA = 'SET_BOOKING_ENQUIRY_DATA';
export const SET_BOOKING_AMOUNT = 'SET_BOOKING_AMOUNT';

export const getBookingPropertyInfo = (
  userId,
  projId,
  deviceId,
  inventoryId,
  enquiryId,
  enquiryName,
) => {
  return async (dispatch) => {
    try {
      return new Promise(async (resolve, reject) => {
        const request = {
          user_id: userId,
          proj_id: projId,
          device_id: deviceId,
          inventory_id: inventoryId,
          enquiry_id: enquiryId,
          enquiry_name: enquiryName,
        };
        const result = await apiInvBookingJourney.apiBookingPropertyInfo(
          request,
        );

        if (result.data.status != '200') {
          const message = result.data.msg
            ? result.data.msg
            : appConstant.appMessage.APP_GENERIC_ERROR;
          reject({message});
        } else {
          dispatch({type: SET_BOOKING_PROPERTY_INFO, data: result.data});
          resolve(result.data);
        }
      });
    } catch (error) {
      throw error;
    }
  };
};

export const getPaymentPlan = (projectId, towerId, inventoryId, userId) => {
  return async (dispatch) => {
    try {
      return new Promise(async (resolve, reject) => {
        const request = {
          tower_id: towerId,
          project_id: projectId,
          inventory_id: inventoryId,
          user_id: userId,
        };
        const result = await apiInvBookingJourney.apiPaymentPlan(request);

        if (result.data.status != '200') {
          const message = result.data.msg
            ? result.data.msg
            : appConstant.appMessage.APP_GENERIC_ERROR;
          reject({message});
        } else {
          dispatch({type: SET_PAYMENT_PLAN, data: result.data});
          resolve(result.data);
        }
      });
    } catch (error) {
      throw error;
    }
  };
};

export const getPaymentPlanDetails = (
  paymentPlanId,
  towerId,
  projectId,
  userId,
  inventoryId,
  discountAmt,
) => {
  return async (dispatch) => {
    try {
      return new Promise(async (resolve, reject) => {
        const request = {
          payment_plan_id: paymentPlanId,
          tower_id: towerId,
          project_id: projectId,
          user_id: userId,
          inventory_id: inventoryId,
        };

        if (discountAmt && discountAmt != '') {
          request.discount_amt = discountAmt;
        }

        const result = await apiInvBookingJourney.apiPaymentPlanDetails(
          request,
        );

        if (result.data.status != '200') {
          const message = result.data.msg
            ? result.data.msg
            : appConstant.appMessage.APP_GENERIC_ERROR;
          reject({message});
        } else {
          dispatch({type: SET_PAYMENT_PLAN_DETAILS, data: result.data});
          resolve(result.data);
        }
      });
    } catch (error) {
      throw error;
    }
  };
};

export const getCostSheet = (
  projId,
  userId,
  deviceId,
  inventoryId,
  paymentPlanId,
  promoCode,
  totSalesAonsA,
  totalOldPrice,
) => {
  return async (dispatch) => {
    try {
      return new Promise(async (resolve, reject) => {
        const request = {
          proj_id: projId,
          user_id: userId,
          device_id: deviceId,
          inventory_id: inventoryId,
          payment_plan_id: paymentPlanId,
        };

        if (promoCode && promoCode != '') {
          request.promo_code = promoCode;
        }
        if (totSalesAonsA && totSalesAonsA != '') {
          request.tot_sales_cons_a = totSalesAonsA;
        }
        if (totalOldPrice && totalOldPrice != '') {
          request.total_old_price = totalOldPrice;
        }

        const result = await apiInvBookingJourney.apiCostSheet(request);

        if (result.data.status != '200') {
          const message = result.data.msg
            ? result.data.msg
            : appConstant.appMessage.APP_GENERIC_ERROR;
          reject({message});
        } else {
          dispatch({type: SET_COST_SHEET, data: result.data});
          resolve(result.data);
        }
      });
    } catch (error) {
      throw error;
    }
  };
};

export const getSourceControl = (userId, deviceId, sourceType) => {
  return async (dispatch) => {
    try {
      return new Promise(async (resolve, reject) => {
        const request = {
          user_id: userId,
          device_id: deviceId,
          source_type: sourceType,
        };
        const result = await apiInvBookingJourney.apiSourceControl(request);

        dispatch({type: SET_PAYMENT_PLAN, data: result.data});
        resolve(result.data);
      });
    } catch (error) {
      throw error;
    }
  };
};

export const setBookingEnquiryData = (bookingEnquiryData) => {
  return {type: SET_BOOKING_ENQUIRY_DATA, data: bookingEnquiryData};
};

export const setPromoCodeData = (promoCodeData) => {
  return {type: SET_PROMO_CODE, data: promoCodeData};
};

export const setBookingAmountData = (bookingAmountData) => {
  return {type: SET_BOOKING_AMOUNT, data: bookingAmountData};
};

export const updatePaymentPlan = (selectedPaymentPlan) => {
  return {type: UPDATE_PAYMENT_PLAN, data: selectedPaymentPlan};
};
