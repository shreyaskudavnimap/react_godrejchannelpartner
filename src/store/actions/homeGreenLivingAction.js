import apiLandingPage from './../../api/apiLanding';

export const SET_GREEN_LIVING = 'SET_GREEN_LIVING';
export const UPDATE_GREEN_LIVING = 'UPDATE_GREEN_LIVING';
export const UPDATE_GREEN_LIVING_ARR = 'UPDATE_GREEN_LIVING_ARR';

export const getGreenLivingProperty = (userId, deviceId) => {
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: userId === '' ? deviceId: '',
        type: 'green_living',
      };
      
      const result = await apiLandingPage.apiProjectByTypeProperty(request);

      dispatch({type: SET_GREEN_LIVING, data: result.data});
    } catch (error) {
      throw error;
    }
  };
};

export const updateProjetWishlist = (projectId) => {
  return {type: UPDATE_GREEN_LIVING, data: projectId};
};

export const updateProjetWishlistArr = (projects) => {
  return {type: UPDATE_GREEN_LIVING_ARR, data: projects};
};
