import apiPropertySearch from '../../api/apiPropertySearch';
import appConstant from '../../utility/appConstant';

export const SET_PROPERTY_SEARCH_DATA = 'SET_PROPERTY_SEARCH_DATA';
export const SET_CITY = 'SET_CITY';
export const SET_PROJECT_STATUS = 'SET_PROJECT_STATUS';
export const SET_TYPOLOGY = 'SET_TYPOLOGY';
export const SET_BUDGET = 'SET_BUDGET';
export const SET_LAST_SEARCH = 'SET_LAST_SEARCH';
export const SET_LAST_DESELECT = 'SET_LAST_DESELECT';
export const RESET_FILTERS = 'RESET_FILTERS';
export const SET_LAST_SEARCH_PEGINATION = 'SET_LAST_SEARCH_PEGINATION';
export const UPDATE_LAST_SEARCH = 'UPDATE_LAST_SEARCH';

export const getPropertySearchData = (
  userId,
  deviceId,
  propertyTypeTermMenu,
  cityTermMenu,
  subLocation,
  projectStatus,
  typologyTermMenu,
  minPrice,
  maxPrice,
  lastSearch,
  lastDesect,
  isFilterFocus = false

) => {
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: deviceId,
        property_type_term_menu: propertyTypeTermMenu,
        city_term_menu: cityTermMenu,
        sub_location: subLocation,
        project_status: projectStatus,
        typology_term_menu: typologyTermMenu,
        min_price: minPrice,
        max_price: maxPrice,
        lastSearch: lastSearch,
        lastDesect: lastDesect,
      };

      const result = await apiPropertySearch.apiPropertyFilterInfo(request);

      if (result.data.status.toString() !== '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      if (!result.data || !result.data.data) {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }
      dispatch({type: SET_LAST_DESELECT, payload: lastDesect});
      dispatch({
        type: SET_PROPERTY_SEARCH_DATA,
        data: result.data.data,
        isFilterFocus: isFilterFocus,
      });
    } catch (error) {
      throw error;
    }
  };
};

export const setCityTermMenu = (
  userId,
  deviceId,
  propertyTypeTermMenu,
  cityTermMenu,
  subLocation,
  projectStatus,
  typologyTermMenu,
  minPrice,
  maxPrice,
  lastSearch,
  lastDesect,
  selectedCityArray,
  isFilterFocus = false
) => {
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: deviceId,
        property_type_term_menu: propertyTypeTermMenu,
        city_term_menu: cityTermMenu,
        sub_location: subLocation,
        project_status: projectStatus,
        typology_term_menu: typologyTermMenu,
        min_price: minPrice,
        max_price: maxPrice,
        lastSearch: lastSearch,
        lastDesect: lastDesect,
      };

      console.log('cityData..//..//request', request);
      const result = await apiPropertySearch.apiPropertyFilterInfo(request);

      if (result.data.status != '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      if (!result.data || !result.data.data) {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      dispatch({type: SET_LAST_DESELECT, payload: lastDesect});
      dispatch({
        type: SET_CITY,
        data: result.data.data,
        selected: selectedCityArray,
        isFilterFocus: isFilterFocus
      });
    } catch (error) {
      throw error;
    }
  };
};

export const setProjectStatus = (
  userId,
  deviceId,
  propertyTypeTermMenu,
  cityTermMenu,
  subLocation,
  projectStatus,
  typologyTermMenu,
  minPrice,
  maxPrice,
  lastSearch,
  lastDesect,
  selectedPossesion,
) => {
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: deviceId,
        property_type_term_menu: propertyTypeTermMenu,
        city_term_menu: cityTermMenu,
        sub_location: subLocation,
        project_status: projectStatus,
        typology_term_menu: typologyTermMenu,
        min_price: minPrice,
        max_price: maxPrice,
        lastSearch: lastSearch,
        lastDesect: lastDesect,
      };
      const result = await apiPropertySearch.apiPropertyFilterInfo(request);

      if (result.data.status != '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      if (!result.data || !result.data.data) {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }
      dispatch({type: SET_LAST_DESELECT, payload: lastDesect});
      dispatch({
        type: SET_PROJECT_STATUS,
        data: result.data.data,
        selected: selectedPossesion,
      });
    } catch (error) {
      throw error;
    }
  };
};

export const setTypologyTermMenu = (
  userId,
  deviceId,
  propertyTypeTermMenu,
  cityTermMenu,
  subLocation,
  projectStatus,
  typologyTermMenu,
  minPrice,
  maxPrice,
  lastSearch,
  lastDesect,
  pro_status,
) => {
  return async (dispatch) => {
    // if(typeof(typologyTermMenu)=='object'){

    //   if(!typologyTermMenu.selected){
    //     t_status = typologyTermMenu.id
    //   }
    // } else {
    //   t_status = projectStatus
    // }
    try {
      const request = {
        user_id: userId,
        device_id: deviceId,
        property_type_term_menu: propertyTypeTermMenu,
        city_term_menu: cityTermMenu,
        sub_location: subLocation,
        project_status: projectStatus,
        typology_term_menu: typologyTermMenu,
        min_price: minPrice,
        max_price: maxPrice,
        lastSearch: lastSearch,
        lastDesect: lastDesect,
      };

      const result = await apiPropertySearch.apiPropertyFilterInfo(request);
      if (result.data.status != '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      if (!result.data || !result.data.data) {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }
      dispatch({type: SET_LAST_DESELECT, payload: lastDesect});
      dispatch({
        type: SET_TYPOLOGY,
        data: result.data.data,
        selected: pro_status,
      });
    } catch (error) {
      throw error;
    }
  };
};

export const setBudgetData = (
  userId,
  deviceId,
  propertyTypeTermMenu,
  cityTermMenu,
  subLocation,
  projectStatus,
  typologyTermMenu,
  minPrice,
  maxPrice,
  lastSearch,
  lastDesect,
  selectedBudget,
) => {
  return async (dispatch) => {
    // if(typeof(typologyTermMenu)=='object'){

    //   if(!typologyTermMenu.selected){
    //     t_status = typologyTermMenu.id
    //   }
    // } else {
    //   t_status = projectStatus
    // }
    try {
      const request = {
        user_id: userId,
        device_id: deviceId,
        property_type_term_menu: propertyTypeTermMenu,
        city_term_menu: cityTermMenu,
        sub_location: subLocation,
        project_status: projectStatus,
        typology_term_menu: typologyTermMenu,
        min_price: minPrice,
        max_price: maxPrice,
        lastSearch: lastSearch,
        lastDesect: lastDesect,
      };

      const result = await apiPropertySearch.apiPropertyFilterInfo(request);
      if (result.data.status != '200') {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }

      if (!result.data || !result.data.data) {
        throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      }
      dispatch({type: SET_LAST_DESELECT, payload: lastDesect});
      dispatch({
        type: SET_BUDGET,
        data: result.data.data,
        selected: selectedBudget,
      });
    } catch (error) {
      throw error;
    }
  };
};

export const setLastSearch = (
  userId,
  deviceId,
  project_status,
  location,
  lat,
  lng,
  minPrice,
  maxPrice,
  possession,
  typology,
  sublocation,
  page,
  onSuccess,
  onFailure,
) => {
  let selected_data = '';
  if (location.length > 0) {
    for (let data of location) {
      if (data.selected) {
        selected_data === ''
          ? (selected_data = data.id)
          : (selected_data = selected_data + ',' + data.id);
      }
    }
  }

  let selected_possession = '';
  if (possession.length > 0) {
    for (let data of possession) {
      if (data.selected) {
        selected_possession === ''
          ? (selected_possession = data.id)
          : (selected_possession = selected_possession + ',' + data.id);
      }
    }
  }

  let selected_typology = '';
  if (typology.length > 0) {
    for (let data of typology) {
      if (data.selected) {
        selected_typology === ''
          ? (selected_typology = data.id)
          : (selected_typology = selected_typology + ',' + data.id);
      }
    }
  }
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: deviceId,
        project_status: project_status,
        location: selected_data.toString(),
        lat: '',
        lng: '',
        min_price: minPrice,
        max_price: maxPrice,
        possession: selected_possession.toString(),
        typology: selected_typology.toString(),
        sub_location: sublocation,
        page: page,
      };
      console.log('request/last/from/action', request);
      const result = await apiPropertySearch.apiPropertyLastFilterInfo(request);

      // if (result.data.status != '200') {
      //   throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      // }

      // if (!result.data || !result.data.data) {
      //   throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      // }

      dispatch({
        type: SET_LAST_SEARCH,
        data: result.data,
        possession: possession,
        typology: typology,
        min_price: minPrice,
        max_price: maxPrice,
        location: location,
      });
      onSuccess();
    } catch (error) {
      onFailure(error);
    }
  };
};

export const setLastSearchPagination = (
  userId,
  deviceId,
  project_status,
  location,
  lat,
  lng,
  minPrice,
  maxPrice,
  possession,
  typology,
  sublocation,
  page,
  onSuccess,
  onFailure,
) => {
  let selected_data = '';
  if (location.length > 0) {
    for (let data of location) {
      if (data.selected) {
        selected_data === ''
          ? (selected_data = data.id)
          : (selected_data = selected_data + ',' + data.id);
      }
    }
  }

  let selected_possession = '';
  if (possession.length > 0) {
    for (let data of possession) {
      if (data.selected) {
        selected_possession === ''
          ? (selected_possession = data.id)
          : (selected_possession = selected_possession + ',' + data.id);
      }
    }
  }

  let selected_typology = '';
  if (typology.length > 0) {
    for (let data of typology) {
      if (data.selected) {
        selected_typology === ''
          ? (selected_typology = data.id)
          : (selected_typology = selected_typology + ',' + data.id);
      }
    }
  }
  return async (dispatch) => {
    try {
      const request = {
        user_id: userId,
        device_id: deviceId,
        project_status: project_status,
        location: selected_data.toString(),
        lat: '',
        lng: '',
        min_price: minPrice,
        max_price: maxPrice,
        possession: selected_possession.toString(),
        typology: selected_typology.toString(),
        sub_location: sublocation,
        page: page,
      };
      const result = await apiPropertySearch.apiPropertyLastFilterInfo(request);

      // if (result.data.status != '200') {
      //   throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      // }

      // if (!result.data || !result.data.data) {
      //   throw new Error(appConstant.appMessage.APP_GENERIC_ERROR);
      // }

      dispatch({
        type: SET_LAST_SEARCH_PEGINATION,
        data: result.data,
      });
      onSuccess();
    } catch (error) {
      console.log(error);
      onFailure(error);
    }
  };
};

export const resetPropertySearch = () => {
  return (dispatch) => {
    dispatch({type: SET_LAST_DESELECT, payload: []});
    dispatch({
      type: RESET_FILTERS,
    });
  };
};

export const setLastDesect = (payload) => {
  return (dispatch) => {
    dispatch({type: SET_LAST_DESELECT, payload});
  };
};

export const updateLastDesect = (payload) => {
  return {type: UPDATE_LAST_SEARCH, data: payload};
};

// export const setLastDesect = (lastDesect) => {
//   return{type: SET_LAST_DESELECT, data: lastDesect};
// };
