import apiLogin from './../../api/apiLogin';

export const SEND_OTP = 'SEND_OTP';

export const sendOtpForVerify = (mobile, deviceId) => {
  return async (dispatch) => {
    try {
      const request = {
          type: "mobile",
          user_id: "",
          mobile: mobile,
          device_id: deviceId,
          name: "",
      };
      
      const result = await apiLogin.apiLandingSendOtp(request);

      console.log("otp result ",result.data);

      dispatch({type: SEND_OTP, data: result.data});
    } catch (error) {
      throw error;
    }
  };
};
