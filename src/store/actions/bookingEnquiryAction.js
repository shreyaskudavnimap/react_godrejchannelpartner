
export const SET_BOOKING_ENQUIRY_DETAILS_ARR = 'SET_BOOKING_ENQUIRY_DETAILS_ARR';
export const CLEAR_BOOKING_ENQUIRY_DETAILS_ARR = 'CLEAR_BOOKING_ENQUIRY_DETAILS_ARR';

export const setBookingEnquiryData = (bookingEnquiryData) => {
  return {type: SET_BOOKING_ENQUIRY_DETAILS_ARR, data: bookingEnquiryData};
};


export const clearBookingEnquiryData = () => {
  return {type: CLEAR_BOOKING_ENQUIRY_DETAILS_ARR};
};