import apiMyAccounts from '../../api/apiMyAccounts';

export const getUserJourneyCTA = (urlFrag, userID, bookingId = null) => {
  return new Promise(async (resolve, reject) => {
    try {
      let reqObj;
      if (bookingId !== null) {
        reqObj = {
          user_id: userID,
          booking_id: bookingId
        };
      } else {
        reqObj = {
          user_id: userID,
        };
      }
      const result = await apiMyAccounts.apiUserJourneyCTA(urlFrag, reqObj);
      if (!result.data || result.data.status !== '200') {
        reject('Something wrong happened');
      } else {
        resolve(result.data.data);
      }
    } catch (e) {
      reject('Something wrong happened');
    }
  });
};

export const getUserJourney = (userID, bookingID) => {
  return new Promise(async (resolve, reject) => {
    try {
      const result = await apiMyAccounts.apiUserJourney({
        user_id: userID,
        booking_id: bookingID,
        source: 'react',
      });
      if (!result.data || result.data.status !== '200') {
        reject('Something wrong happened');
      } else {
        resolve(result.data.data);
      }
    } catch (e) {
      reject('Something wrong happened');
    }
  });
};
