import {createStore, combineReducers, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import loggerMiddleware from './middleware/logger';
import monitorReducerEnhancer from './enchancers/monitorReducer';

import deviceReducer from './reducers/deviceReducer'; //'./src/store/reducers/deviceReducer';
import homeCityWiseProjectsReducer from './reducers/homeCityWiseProjectsReducer';
import homeAwardWiningReducer from './reducers/homeAwardWiningReducer';
import homeContinueExploringReducer from './reducers/homeContinueExploringReducer';
import homeGreenLivingReducer from './reducers/homeGreenLivingReducer';
import homeLandingInfoReducer from './reducers/homeLandingInfoReducer';
import homeNewLaunchReducer from './reducers/homeNewLaunchReducer';
import projectDetailsReducer from './reducers/projectDetailsReducer';
import projectCmsReducer from './reducers/projectCmsReducer';
import projectLocationConnectivityReducer from './reducers/projectLocationConnectivityReducer';
import propertySearchReducer from './reducers/propertySearchReducer';
import inventoryMPLReducer from './reducers/inventoryMPLReducer';
import userLoginReducer from './reducers/userLoginReducer';
import lookupUserByPanReducer from './reducers/lookupUserByPanReducer';
import sendOtpReducer from './reducers/sendOtpReducer';
import dashboardReducer from './reducers/dashboardReducer';
import invBookingJourneyReducer from './reducers/invBookingJourneyReducer';
import eoiMPLReducer from './reducers/eoiMPLReducer';
import eoiBookingJourneyReducer from './reducers/eoiBookingJourneyReducer';
import bookingEnquiryReducer from './reducers/bookingEnquiryReducer';
import badgeCountReducer from './reducers/badgeCountReducer';
import preloginTermsReducer from './reducers/preloginTermsReducer';
import deviceNetworkReducer from './reducers/deviceNetworkReducer';
import menuTypeReducer from './reducers/menuTypeReducer';

const rootReducer = combineReducers({
  deviceInfo: deviceReducer,
  landingInfo: homeLandingInfoReducer,
  continueExploring: homeContinueExploringReducer,
  cityWiseProjects: homeCityWiseProjectsReducer,
  newLaunch: homeNewLaunchReducer,
  greenLiving: homeGreenLivingReducer,
  awardWining: homeAwardWiningReducer,
  projectDetails: projectDetailsReducer,
  projectCMSData: projectCmsReducer,
  projectLocationConnectivity: projectLocationConnectivityReducer,
  propertySearch: propertySearchReducer,
  loginInfo: userLoginReducer,
  lookupInfo: lookupUserByPanReducer,
  otpInfo: sendOtpReducer,
  dashboardData: dashboardReducer,
  inventoryMLP: inventoryMPLReducer,
  invBookingJourney: invBookingJourneyReducer,
  eoiMLP: eoiMPLReducer,
  eoiBookingJourney: eoiBookingJourneyReducer,
  bookingEnquiryDataArr: bookingEnquiryReducer,
  badgeCount: badgeCountReducer,
  preloginTerms: preloginTermsReducer,
  deviceNetInfo: deviceNetworkReducer,
  menuType: menuTypeReducer
});

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['loginInfo', 'bookingEnquiryDataArr','preloginTerms'],
};

export default function configureStore(preloadedState) {
  const middlewares = [loggerMiddleware, thunkMiddleware];
  const middlewareEnhancer = applyMiddleware(...middlewares);

  const enhancers = [middlewareEnhancer, monitorReducerEnhancer];
  const composedEnhancers = composeWithDevTools(...enhancers);

  const persistedReducer = persistReducer(persistConfig, rootReducer);
  const store = createStore(
    persistedReducer,
    preloadedState,
    composedEnhancers,
  );
  const persistor = persistStore(store);

  return {
    persistor,
    store,
  };
}
