import { useSelector } from 'react-redux';
import GetLocation from 'react-native-get-location';
import appUserAuthorization from '../utility/appUserAuthorization';

const userTermsAuthorization = () => {


    const postAuthorization = (moduleName) => {
        const loginUserData = useSelector((state) => state.loginInfo.loginResponse);
        const userId =
            loginUserData && loginUserData.data && loginUserData.data.userid
                ? loginUserData.data.userid
                : '';
        const storedUserData = loginUserData.data;
        const requestData = {
            deviceId: deviceUniqueId,
            devicePlatform: Platform.OS,
            userId: userId,
            customerName:
                storedUserData && storedUserData.first_name && storedUserData.last_name
                    ? `${storedUserData.first_name} ${storedUserData.last_name}`
                    : '',
            mobileNo:
                storedUserData && storedUserData.mob_no ? storedUserData.mob_no : '',
            emailId:
                storedUserData && storedUserData.email ? storedUserData.email : '',
            moduleName: moduleName,
        };
        console.log(requestData);
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        })
            .then(({ latitude, longitude }) => {
                appUserAuthorization.validateUserAuthorization({
                    ...requestData,
                    latiTude: latitude,
                    longiTude: longitude,
                });
            })
            .catch((error) => {
                appUserAuthorization.validateUserAuthorization(requestData);
            });
    };

    return { postAuthorization };

};

export default userTermsAuthorization;