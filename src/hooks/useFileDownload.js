import {useState} from 'react';

import RNFetchBlob from 'rn-fetch-blob';
import FileViewer from 'react-native-file-viewer';
import {PermissionsAndroid, Platform} from 'react-native';

const useFileDownload = () => {
  const [progress, setProgress] = useState(0);
  const [showProgress, setShowProgress] = useState(false);
  const [error, setError] = useState('');
  const [isSuccess, setIsSuccess] = useState(false);

  const checkPermission = async (downloadUrl, fileName, fileExt) => {
    setProgress(0);
    setShowProgress(false);
    setIsSuccess(false);
    setError('');

    if (Platform.OS === 'ios') {
      downloadFile(downloadUrl, fileName, fileExt);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message: 'App needs access to your storage to download files',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          downloadFile(downloadUrl, fileName, fileExt);
        } else {
          setIsSuccess(false);
          setError('Storage Permission Not Granted');
        }
      } catch (err) {
        setError(err);
        setIsSuccess(false);
      }
    }
  };

  const downloadFile = (downloadUrl, fileName, fileExt) => {
    let image_URL = downloadUrl;

    const {config, fs} = RNFetchBlob;

    const DownloadDir =
      Platform.OS === 'ios' ? fs.dirs.DocumentDir : fs.dirs.DownloadDir;

    setShowProgress(true);
    setProgress(1);

    let options = {
      fileCache: false,
      trusty: true,
      path: DownloadDir + '/GPL_' + fileName,
    };
    config(options)
      .fetch('GET', image_URL)
      .progress({ count: 100, interval: 100 },(received, total) => {
        const progress = Math.round(
          (received / total) * 100
        );
        // console.log('received: ' + received + " total: " + total + " progress : " + progress)
        setProgress(progress);
      })
      .then((res) => {
        FileViewer.open(res.data, {showOpenWithDialog: true})
          .then(() => {
            setProgress(0);
            setIsSuccess(true);
            setShowProgress(false);
          })
          .catch((error) => {
            setError(error);
            setProgress(0);
            setIsSuccess(false);
            setShowProgress(false);
          });
      })
      .catch((error) => {
        setError(error);
        setProgress(0);
        setIsSuccess(false);
        setShowProgress(false);
      });
  };

  return {progress, showProgress, isSuccess, error, checkPermission};
};

export default useFileDownload;
