import {useSelector} from 'react-redux';

export default useDevice = () => {

  /**
   * Device Name || Like - andoid | ios
   */
  const deviceOS = useSelector((state) => state.deviceInfo.deviceOS);

  /**
   * Device Unique Id
   */
  const deviceUniqueId = useSelector(
    (state) => state.deviceInfo.deviceUniqueId,
  );

  return {deviceOS, deviceUniqueId};
};
