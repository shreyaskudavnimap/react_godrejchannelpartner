import React,{Component} from 'react';
import { View } from 'react-native'
class ErrorBounday extends Component{
    state = {
        hasError:false,
        errorMesssage:''
    }
    componentDidCatch = (error, info) =>{
        this.setState({hasError:true,errorMesssage:error})
    }
    render(){
        if(this.state.hasError){
            return (
            <View>
                <Text>Please check your internet connection</Text>
            </View>
            )
        }else{
            return this.props.children
        }
    }
}