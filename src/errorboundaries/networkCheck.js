
import {useNetInfo} from "@react-native-community/netinfo";
const NetWorkCheck = {
  isNetworkConnected = () =>{
    const netInfo = useNetInfo();
  return netInfo.isConnected.toString()
  }
};
export default NetWorkCheck;