import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import DeviceConfigScreen from '../screens/DeviceConfigScreen';

const Stack = createStackNavigator();

const DeviceConfigNavigator  = () => (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="DeviceConfig" component={DeviceConfigScreen} />
    </Stack.Navigator>
  );

export default DeviceConfigNavigator;