import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {useSelector} from 'react-redux';

import navigationTheme from './navigationTheme';
import LoginScreen from '../screens/LoginScreen';
import SignupScreen from '../screens/SignupScreen';
import ForgetPwdScreen from '../screens/ForgetPasswordScreen';
import ForgortPassword from '../screens/ForgortPassword';
import OtpScreen from '../screens/OtpScreen';
import EmpanelmentRequest from '../screens/EmpanelmentRequest';
import SetPassword from '../screens/SetPassword';
import PasswordResetted from '../screens/PasswordResetted';
import AppNavigator from './AppNavigator.js';
import analytics from '@react-native-firebase/analytics';

import apiClient from './../api/client';

const Stack = createStackNavigator();
const AuthNavigator = () => {
  const navigationRef = React.useRef();
  const routeNameRef = React.useRef();
  useEffect(() => {
    setRequestHeader();
  }, [loginvalue]);

  const loginvalue = useSelector((state) => state.loginInfo.loginResponse);

  const setRequestHeader = () => {
    const headers = {
      'X-CSRF-TOKEN': loginvalue.token ? loginvalue.token : '',
      'x-requested-with':
        loginvalue.sessionName && loginvalue.sessionId
          ? loginvalue.sessionName + '=' + loginvalue.sessionId
          : '',
    };

    apiClient.setHeaders(headers);
  };

  return (
    <NavigationContainer theme={navigationTheme}
    ref={navigationRef}
      onReady={() => routeNameRef.current = navigationRef.current.getCurrentRoute().name}
      onStateChange={async () => {
        const previousRouteName = routeNameRef.current;
        const currentRouteName = ((navigationRef.current.getCurrentRoute().name).replace(/([a-zA-Z])([A-Z])([a-z])/g, '$1 $2$3')).replace('Screen','')

        if (previousRouteName !== currentRouteName) {
          // The line below uses the expo-firebase-analytics tracker
          // https://docs.expo.io/versions/latest/sdk/firebase-analytics/
          // Change this line to use another Mobile analytics SDK
          //analytics().setCurrentScreen(currentRouteName)
          await analytics().logScreenView({
            screen_name: currentRouteName,
            screen_class: currentRouteName,
          });
        }

        // Save the current route name for later comparision
        routeNameRef.current = currentRouteName;
      }}
    >
        <Stack.Navigator
          initialRouteName={!loginvalue.isLogin ? "Login" : "AppStackRoute"}
          screenOptions={{headerShown: false}}>
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Signup" component={SignupScreen} />
          <Stack.Screen name="ForgetPwd" component={ForgortPassword} />
          <Stack.Screen name="Otp" component={OtpScreen} />
          <Stack.Screen name="EmpanelmentRequest" component={EmpanelmentRequest} />
          <Stack.Screen name="SetPassword" component={SetPassword} />
          <Stack.Screen name="PasswordResetted" component={PasswordResetted} />
          <Stack.Screen name="AppStackRoute" component={AppNavigator} />
        </Stack.Navigator>
      {/* ) : (
        <Stack.Navigator
          initialRouteName=
          screenOptions={{headerShown: false}}>
          <Stack.Screen name="AppStackRoute" component={AppNavigator} />
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Signup" component={SignupScreen} />
          <Stack.Screen name="ForgetPwd" component={ForgetPwdScreen} />
          <Stack.Screen name="Otp" component={OtpScreen} />
        </Stack.Navigator>
      )} */}
    </NavigationContainer>
  );
};

export default AuthNavigator;
