import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { useDispatch, useSelector } from "react-redux";

import HomeScreen from '../screens/HomeScreen';
import ProjectDetailsScreen from '../screens/ProjectDetailsScreen';
import SearchScreen from '../screens/SearchScreen';
import ContactScreen from '../screens/ContactScreen';
import PresaleVisitScreen from '../screens/ScheduleVisit';
import VisitScreen from '../screens/MyVisits';
import ScheduleVisitConfirmation from '../screens/ThankYouScreen';
import UserDetailsScreen from '../screens/UserDetailsScreen';
import LocationConnectivityScreen from '../screens/LocationConnectivityScreen';
import GodrejHomeScreen from '../screens/GodrejHomeScreen';
import WishlistScreen from '../screens/WishlistScreen';
import NotificationsScreen from '../screens/NotificationsScreen';
import Profile from '../screens/ProfileMenu';
import UpdateProfile from '../screens/UpdateProfile';
import EmpanelmentRequest from '../screens/EmpanelmentRequest';
import EmpanelmentPan from '../screens/EmpanelmentPan';
import ProjectCmsScreen from '../screens/ProjectCmsScreen';
import DashboardScreen from '../screens/DashboardScreen';
import MyAccountScreen from '../screens/MyAccountScreen';
import RnDScreen from '../screens/RnDScreen';
import SearchFilterScreen from '../screens/SearchFilterScreen';
import MyAccoutScreen from '../screens/MyAccountScreen';
import ServiceRequest from '../screens/ServiceRequest';

import PayNowScreen from '../screens/PayNowScreen';
import LoanEnquiryScreen from '../screens/LoanEnquiryScreen';
import BankDetailsPreview from '../screens/BankDetailsPreview';
import LoanDisbursementForm from '../screens/LoanDisbursementForm';
import LoanDisbursementForm2 from '../screens/LoanDisbursementForm2';
import LoanDisbursementList from '../screens/LoanDisbursementList';
import BookingDetailsScreen from '../screens/BookingDetailsScreen';
import FloorScreen from '../screens/FloorLayout';
import BookingScreen from '../screens/BookingScreen';
import BookingConfirmation from '../screens/BookingConfirmation';
import ForgetPwd from '../screens/ForgetPasswordScreen';
import EditProfile from '../screens/EditProfile';
import EOIBookingScreen from '../screens/EOIBookingScreen';
import EOIFloorScreen from '../screens/EOIFloorLayout';
import EOIBookingDetailsScreen from '../screens/EOIBookingDetailsScreen';
import EOIBookingConfirmation from '../screens/EOIBookingConfirmation';
import ChangeUserName from '../screens/ChangeName';
import EditDateOfBirth from '../screens/EditDOB';
import ChangesPassword from '../screens/ChangesPassword';
import TestimonialsScreen from '../screens/Testimonial';
import DocumentScreen from '../screens/DocumentScreen';
import ChangesMobileNo from '../screens/ChangesMobileNumber';
import ChangeAddress from '../screens/ChangeAddress';
import OtpForEmail from '../screens/OtpForEmail';
import ChangeEmailId from '../screens/ChangeEmailId';
import ChangeEmail from '../screens/ChangeEmail';
import ChangeCommunicationAddress from '../screens/ChangeCommunicationAddress';
import ChangeRegisteredAddress from '../screens/ChangeRegisteredAddress';
import AddTestimonialsScreen from '../screens/AddTestimonials';
import ReachUsScreen from '../screens/ReachUsScreen';
import RMDetailsScreen from '../screens/RMDetailScreen';
import ConstructionStatus from '../screens/ConstructionStatus';
import ServiceRequestDetails from '../screens/ServiceRequestDetails';
import FAQScreen from '../screens/FAQScreen';
import RMDetailScreen from '../screens/RMDetailScreen';
import RaiseSnagScreen from '../screens/RaiseSnagScreen';
import ViewSnagScreen from '../screens/ViewSnagScreen';
import StampDutyScreen from '../screens/StampDutyScreen';
import BookingData from '../screens/BookingData';
const Stack = createStackNavigator();

function AppNavigator() {

  var _customer = '';

  const loginvalue = useSelector(
    (state) => state.loginInfo.loginResponse,
  );

  useEffect(() => {
    if (loginvalue.isLogin) {
      if (loginvalue.data.is_customer == 0) _customer = GodrejHomeScreen;
      if (loginvalue.data.is_customer == 1) _customer = DashboardScreen;
    } else {
      _customer = GodrejHomeScreen;
    }
  }, []);


  return (
    <Stack.Navigator
      initialRouteName={(loginvalue.isLogin) ? (loginvalue.data.is_customer == 1) ? "DashboardScreen" : "GodrejHome" : "GodrejHome"}
      screenOptions={{ headerShown: false }}>
      {/* <Stack.Screen name="Home" component={RnDScreen} /> */}
      <Stack.Screen name="MyAccoutScreen" component={MyAccoutScreen} />
      <Stack.Screen name="GodrejHome" component={GodrejHomeScreen} />
      <Stack.Screen name="ProjectDetails" component={ProjectDetailsScreen} />
      <Stack.Screen name="FloorScreen" component={FloorScreen} />
      <Stack.Screen name="EOIFloorScreen" component={EOIFloorScreen} />
      <Stack.Screen name="SearchFilter" component={SearchFilterScreen} />
      <Stack.Screen name="SearchResult" component={SearchScreen} />
      <Stack.Screen name="ContactScreen" component={ContactScreen} />
      <Stack.Screen name="VisitScreen" component={VisitScreen} />
      <Stack.Screen name="PresaleVisitScreen" component={PresaleVisitScreen} />
      <Stack.Screen name="ScheduleVisitConfirmation" component={ScheduleVisitConfirmation} />
      <Stack.Screen
        name="LocationConnectivity"
        component={LocationConnectivityScreen}
      />
      <Stack.Screen name="ProjectCms" component={ProjectCmsScreen} />
      <Stack.Screen name="UserDetailsScreen" component={UserDetailsScreen} />
      <Stack.Screen name="WishlistScreen" component={WishlistScreen} />
      <Stack.Screen name="DashboardScreen" component={DashboardScreen} />
      <Stack.Screen name="MyAccountScreen" component={MyAccountScreen} />
      <Stack.Screen name="PayNowScreen" component={PayNowScreen} />
      <Stack.Screen name="NotificationsScreen" component={NotificationsScreen} />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="EditProfile" component={EditProfile} />
      {/* Add new screen */}
      <Stack.Screen name="UpdateProfile" component={UpdateProfile} />
      <Stack.Screen name="EmpanelmentRequest" component={EmpanelmentRequest} />
      <Stack.Screen name="EmpanelmentPan" component={EmpanelmentPan} />
      <Stack.Screen name="BookingData" component={BookingData} />

      <Stack.Screen name="BookingScreen" component={BookingScreen} />
      <Stack.Screen
        name="BookingDetailsScreen"
        component={BookingDetailsScreen}
      />
      <Stack.Screen
        name="BookingConfirmation"
        component={BookingConfirmation}
      />
      <Stack.Screen name="EOIBookingScreen" component={EOIBookingScreen} />
      <Stack.Screen
        name="EOIBookingDetailsScreen"
        component={EOIBookingDetailsScreen}
      />
      <Stack.Screen
        name="EOIBookingConfirmation"
        component={EOIBookingConfirmation}
      />
      <Stack.Screen name="LoanEnquiryScreen" component={LoanEnquiryScreen} />
      <Stack.Screen name="BankDetailsPreview" component={BankDetailsPreview} />
      <Stack.Screen name="LoanDisbursementForm" component={LoanDisbursementForm} />
      <Stack.Screen name="LoanDisbursementForm2" component={LoanDisbursementForm2} />
      <Stack.Screen name="LoanDisbursementList" component={LoanDisbursementList} />
      <Stack.Screen name="ServiceRequest" component={ServiceRequest} />
      <Stack.Screen name="RnDScreen" component={RnDScreen} />
      <Stack.Screen name="ChangeNameScreen" component={ChangeUserName} />
      <Stack.Screen name="ChangeDOBScreen" component={EditDateOfBirth} />
      <Stack.Screen name="Testimonial" component={TestimonialsScreen} />
      <Stack.Screen name="DocumentScreen" component={DocumentScreen} />
      <Stack.Screen name="AddTestimonials" component={AddTestimonialsScreen} />
      <Stack.Screen name="ChangesPasswordScreen" component={ChangesPassword} />
      <Stack.Screen name="ChangeEmailScreen" component={ChangeEmail} />
      <Stack.Screen name="ChangeCommunicationAddress" component={ChangeCommunicationAddress} />
      <Stack.Screen name="ChangeRegisteredAddress" component={ChangeRegisteredAddress} />
      <Stack.Screen name="ChangesMobileNoScreen" component={ChangesMobileNo} />
      <Stack.Screen name="ChangeAddressScreen" component={ChangeAddress} />
      <Stack.Screen name="OtpForEmailScreen" component={OtpForEmail} />
      <Stack.Screen name="ChangeEmailIdScreen" component={ChangeEmailId} />
      <Stack.Screen name="ReachUs" component={ReachUsScreen} />
      <Stack.Screen name="RMDetailsScreen" component={RMDetailsScreen} />
      <Stack.Screen name="ConstructionStatus" component={ConstructionStatus} />
      <Stack.Screen name="ServiceRequestDetails" component={ServiceRequestDetails} />
      <Stack.Screen name="FAQ" component={FAQScreen} />
      <Stack.Screen name="RMDetailScreen" component={RMDetailScreen} />
      <Stack.Screen name="RaiseSnagScreen" component={RaiseSnagScreen} />
      <Stack.Screen name="ViewSnagScreen" component={ViewSnagScreen} />
      <Stack.Screen name="StampDutyScreen" component={StampDutyScreen} />
    </Stack.Navigator>
  );
}

export default AppNavigator;
