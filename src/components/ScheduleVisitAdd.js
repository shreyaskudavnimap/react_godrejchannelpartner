import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
  TouchableWithoutFeedback,
  TextInput,
  Modal,
  Text
} from 'react-native';

import Screen from '../components/Screen';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppClickableText from '../components/ui/AppClickableText';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
import AppButton from '../components/ui/AppButton';
import appFonts from '../config/appFonts';
import colors from '../config/colors';
import DropDownPicker from 'react-native-dropdown-picker';
import ScheduleVisitTermModalScreen from '../screens/modals/ScheduleVisitTermModalScreen';
import ScheduleVisitSuccessModal from '../screens/modals/ScheduleVisitSuccessModal'
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import appPhoneMap from '../utility/appPhoneMap';
import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';
import apiScheduledVisit from './../api/apiScheduledVisit';
import {Calendar} from 'react-native-calendars'
import moment from 'moment'
import HTML from 'react-native-render-html';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const contactInfo = [
  {
    id: '1',
    title: 'Saini Agarwal',
  },
  {
    id: '2',
    title: '9839093898',
  },
  {
    id: '3',
    title: 'saina@godrej.com',
  },
];

const PurposeVisit = [
  {label: 'Site visit', value: 'Site visit'},
  {label: 'Meeting with RM', value: 'Meeting with RM'},
  {label: 'Flat Possession', value: 'Flat Possession'},
];

const Persons = [
  {label: '1', value: '1'},
  {label: '2', value: '2'},
  {label: '3', value: '3'},
  {label: '4', value: '4'},
];
const VisitorsDetails = [
  {label: 'self', value: 'self'},
  {label: 'Authorized Representative', value: 'Authorized Representative'},
];

const ScheduleVisitAdd = (props) => {
  const [isTermsSelected, setIsTermsSelected] = useState(false);
  const [termsModalVisible, setTermsModalVisible ] = useState(false)
  const [scheduleDetails, setScheduleDetails] = useState({})
  const [visitTypesData, setVisitTypesData] = useState([])
  const [visitTypeList, setVisitTypeList] = useState(props.visitTypeList || [])
  const [propertyList, setPropertyList] = useState([])
  const [projectId, setProjectId] = useState('')
  const [property, setProperty] = useState({})
  const [purpose, setPurpose] = useState({})
  const [preferredDate, setPreferredDate] = useState('')
  const [slot, setSlot] = useState({})
  const [person, setPerson] = useState({})
  const [visitorType, setVisitorType] = useState({})
  const [comment, setComment] = useState('')
  const [bookingId, setBookingId] = useState(props.bookingId)
  const [slotPlaceHolder, setSlotPlaceHolder] = useState('Select Slot')
  const [daysConfig, setDaysConfig] = useState([])
  const [fromDate, setFromDate] = useState('')
  const [toDate, setToDate] = useState('')
  const [showCalendar, setShowCalendar] = useState(false)
  const [isDateAvailable, setIsDateAvailable] = useState(true)
  const [availableSlots, setAvailableSlots] = useState({})
  const [isSlotsAvailable, setIsSlotsAvailable] = useState(true)
  const [slots, setSlots] = useState([])
  const [numberOfPeople, setNumberOfPeople] = useState([])
  const [visitorTypes, setVisitorTypes] = useState([])
  const [rmInfo, setRmInfo] = useState({})
  const [isPreferredTime, setIsPreferredTime] = useState(false)
  const [termsData, setTermsData] = useState('')
  const [latitude, setLatitude] = useState(0)
  const [longitude, setLongitude] = useState(0)

  const [isSuccessModalVisible, setIsSuccessModalVisible] = useState(false)
  const [successMessage, setSuccessMessage] = useState('')

  const [isVisitAvailable, setIsVisitAvailable] = useState(false)
  const [isValidPerson, setIsValidPerson] = useState(true)

  const [isPropertyExpanded, setIsPropertyExpanded] = useState(false)
  const [isPurposeExpanded, setIsPurposeExpanded] = useState(false)
  const [isPreferredTimeExpanded, setIsPreferredTimeExpanded] = useState(false)
  const [isPersonExpanded, setIsPersonExpanded] = useState(false)
  const [isVisitorExpanded, setIsVisitorExpanded] = useState(false)
  const [isPreSelectedPurpose, setIsPreSelectedPurpose] = useState(false)
  const setLoading = props.setLoading

  const openDeviceMap = () => {
    appPhoneMap.openDeviceMap(
      latitude
        ? Number(latitude)
        : 0,
      longitude
        ? Number(longitude)
        : 0,
      rmInfo.property_name,
    );
  };

  useEffect(() => {
    getScheduleDatails()
  }, [])

  useEffect(() => {
    getScheduleDatails()
  }, [])

  const getScheduleDatails = async () => {
    const payload = {
      user_id: props.userId
    }

    setLoading(true)
    try {
      const result = await apiScheduledVisit.apiGetScheduleVisitDetails(payload)
      if (result.status == 200 && result.data) {
        const res = result.data
        const scheduleDetails = res.data
        setScheduleDetails(scheduleDetails)
        const propertyList = []
        const property = {}
        if (bookingId) {
          const objKey = Object.keys(
            scheduleDetails.inventory_project_map
            ).find(key => {
              return scheduleDetails.inventory_project_map[key].booking_nid === bookingId

            })
          if (objKey) {
            property['label'] = res.data.properties[objKey]
            property['value'] = objKey
            propertyList.push({ ...property})
          }
        } else {
          Object.keys(scheduleDetails.properties).forEach(k => {
            property['label'] = res.data.properties[k]
            property['value'] = k
            propertyList.push({ ...property})
          })
        }

        if (scheduleDetails['visitor_types']) {
          const visitorTypes = []
          Object.keys(scheduleDetails.visitor_types).forEach(k => {
            visitorTypes.push({
              label: scheduleDetails['visitor_types'][k],
              value: k
            })
          })
          setVisitorTypes(visitorTypes)
        }
        
        setPropertyList(propertyList)
        setLoading(false)
        if (propertyList.length === 1) {
          onPropertyChange(property, scheduleDetails)
        }
      }
    } catch (err) {
      setLoading(false)
      console.log(err)
      appSnakBar.onShowSnakBar(appConstant.appMessage.APP_GENERIC_ERROR, 'LONG');
    }
  }

  const onPropertyChange = (property, scheduleDetails) => {
    setProperty(property)

    if (scheduleDetails && scheduleDetails['rm_info']) {
      const rmInfo = scheduleDetails['rm_info'][property.value]
      setRmInfo(rmInfo)
      //set Lat long
      const latitude = parseFloat(scheduleDetails['rm_info'][property.value].lattitude)
      const longitude = parseFloat(scheduleDetails['rm_info'][property.value].longitude)
      setLatitude(latitude)
      setLongitude(longitude)
    }

    //set booking id
    const bookingId = scheduleDetails['rm_info'][property.value].booking_id
    setBookingId(bookingId)
    getVisitTypes(property, scheduleDetails)
  }

  const getVisitTypes = async (property, scheduleDetails) => {
    const payload = {
      inventory_id: property.value,
      user_id: props.userId
    }

    setLoading(true)
    try {
      const result = await apiScheduledVisit.apiGetVisitTypes(payload)
      const res = result.data
      const visitTypesData = res.data
      setVisitTypesData(visitTypesData)
      let updatedVisitTypeList = []
      if (visitTypeList.length > 0) {
        updatedVisitTypeList = [...visitTypeList]
      } else {
        updatedVisitTypeList = visitTypesData.map(vt => ({
          label: vt.name,
          value: vt.id
        }))
      }

      setVisitTypeList(updatedVisitTypeList)
      setIsVisitAvailable(true)
      resetValue(1)
      setLoading(false)
      if (visitTypeList.length > 0) {
        onPurposeChange(updatedVisitTypeList[0], scheduleDetails, property)
        setIsPreSelectedPurpose(true)
      }
    } catch(err) {
      setLoading(false)
      console.log(err)
      appSnakBar.onShowSnakBar(appConstant.appMessage.APP_GENERIC_ERROR, 'LONG');
    }
  }

  const onPurposeChange = (purpose, scheduleDetails, property) => {
    setPurpose(purpose)
    const isPreferredTime = purpose.value == '26' || purpose.value == '500' ? false : true
    setIsPreferredTime(isPreferredTime)
    let isDateAvailable = true
    
    let selectedPrerequisite = {}
    let prerequisite
    let projectId = ''
    let minDate, maxDate
    if (purpose && scheduleDetails && scheduleDetails['prerequisite'] && scheduleDetails['inventory_project_map']) {
      const inventoryMap = scheduleDetails["inventory_project_map"]
      prerequisite = scheduleDetails['prerequisite']

      if (inventoryMap[property.value] && inventoryMap[property.value]['project_nid']) {
        projectId = inventoryMap[property.value]["project_nid"]
        const key = `${projectId}_${purpose.value}`
        selectedPrerequisite = prerequisite[key] || null
      }

      setProjectId(projectId)
      minDate = selectedPrerequisite && selectedPrerequisite["from_date"] ? selectedPrerequisite["from_date"] : null
      maxDate = selectedPrerequisite && selectedPrerequisite["to_date"] ? selectedPrerequisite["to_date"] : null
      // doDateValidation(minDate, maxDate)
    }
    const slotPlaceHolder = purpose.value == 499 ? "00.00 hrs-00.00 hrs" : "Select Slot"
    setSlotPlaceHolder(slotPlaceHolder)
    getRMVisitDetails(projectId, property, purpose.value)
    resetValue(2)
  }

  const getRMVisitDetails = async (projectId, property, visitId) => {
    const payload = {
      project_id: projectId,
      visit_id: visitId,
      inventory_id: property.value
    }

    setLoading(true)
    try {
      const result = await apiScheduledVisit.apiGetRMVistDates(payload)
      const res = result.data
      const fromDate = res.dates[0];
      const toDate = res.dates[res.dates.length - 1];
      const dateRange = getDateRange(res.dates[0], res.dates[res.dates.length - 1])
      createConfigs(
        res.dates,
        dateRange
      )
      setFromDate(fromDate)
      setToDate(toDate)
      doDateValidation(fromDate, toDate)
      setLoading(false)
    } catch (err) {
      setLoading(false)
      console.log(err)
      appSnakBar.onShowSnakBar(appConstant.appMessage.APP_GENERIC_ERROR, 'LONG');
    }
  }

  const createConfigs = (enableDates, allDates) => {
    let isDateAvailable = false
    const dateConfigs = []
    const currentDate = new Date();
    const currentDateTime = currentDate.getTime()
    if (allDates.length && enableDates.length) {
      allDates.forEach(date => {
        const fromDate = new Date(`${date}T23:59:59`);
        const fromDateTime = fromDate.getTime();

        if (fromDateTime > currentDateTime) {
          dateConfigs.push({
            date,
            disable: !enableDates.includes(date)
          })

          isDateAvailable = !enableDates.includes(date)
        } else {
          dateConfigs.push({
            date,
            disable: true
          })
        }
      })

      setDaysConfig(dateConfigs)
    }
  }

  const getDateRange = (startDateParam, endDateParam) => {
    const startDate = new Date(startDateParam)
    const endDate = new Date(endDateParam)

    const dates = []
    let currentDate = startDate

    const addDays = function (days) {
      const date = new Date(this.valueOf())
      date.setDate(date.getDate() + days) 
      return date
    }

    while (currentDate <= endDate) {
      const dateString = currentDate.getFullYear() + 
                          "-" +
                          (currentDate.getMonth() + 1 < 10
                          ? "0" + (currentDate.getMonth() + 1)
                          : currentDate.getMonth() + 1) +
                          "-" +
                          (currentDate.getDate() < 10 
                          ? "0" + currentDate.getDate()
                          : currentDate.getDate());
      dates.push(dateString)
      currentDate = addDays.call(currentDate, 1)
    }

    return dates
  }

  const doDateValidation = (minDate, maxDate) => {
    let isDateAvailable = true
    if (minDate && maxDate) {
      const fromDate = new Date(minDate + 'T23:59:59')
      const toDate = new Date(maxDate + 'T23:59:59')
      const currentDate = new Date()

      const fromDateTime = fromDate.getTime()
      const toDateTime = toDate.getTime()

      const currentDateTime = currentDate.getTime()
      isDateAvailable = true
      if (fromDateTime < currentDateTime) {
        minDate = currentDate
      }
      if (toDateTime < currentDateTime) {
        isDateAvailable = false
      }
      minDate = moment(minDate).format('YYYY-MM-DD')
      setFromDate(minDate)
      setToDate(maxDate)
    } else {
      setFromDate(null)
      setToDate(null)
      isDateAvailable = false
    }
    setIsDateAvailable(isDateAvailable)
  }

  const getMarkedDates = (daysConfig) => {
    const markedDates = {}

    daysConfig.forEach(dc => {
      markedDates[dc.date] = { disabled: dc.disable, disableTouchEvent: dc.disable }
    })

    return markedDates
  }

  const onDateChange = (date) => {
    setPreferredDate(date)
    getAvailableSlots(date)
    resetValue(3)
  }

  const getAvailableSlots = async (date) => {
    const payload = {
      user_id: props.userId,
      inventory_nid: property.value,
      visit_type: purpose.value,
      date
    }

    setLoading(true)
    try {
      const result = await apiScheduledVisit.apiGetAvailableSlots(payload)
      const res = result.data
      if (res && res.status == 200 && res.data) {
        const isSlotsAvailable =  false
        const bookingDetilsByDate = res.data
        if (bookingDetilsByDate && bookingDetilsByDate['time_slots']) {
          const slots = bookingDetilsByDate['time_slots'] || {}
          prepareSlots(slots)
        }
      }
      setLoading(false)
    } catch (err) {
      setLoading(false)
      console.log(err)
      appSnakBar.onShowSnakBar(appConstant.appMessage.APP_GENERIC_ERROR, 'LONG');
    }
  }

  const prepareSlots = (slotsParam) => {
    const availableSlots = {}
    const slots = []
    let isSlotsAvailable = false
    for (const key in slotsParam) {
      if (slotsParam.hasOwnProperty(key)) {
        const slot = slotsParam[key]
        if (slot['max_value'] && !isNaN(slot['max_value']) && parseInt(slot['max_value']) > 0) {
          availableSlots[key] = slot
          slots.push({
            label: slot.text,
            value: key
          })
          isSlotsAvailable = true
        }
      }
    }
    setAvailableSlots(availableSlots)
    setSlots(slots)
    setIsSlotsAvailable(isSlotsAvailable)
  }

  const onSlotChange = (slot) => {
    setSlot(slot)
    if (availableSlots) {
      const slots = availableSlots[slot.value]

      if (slots && slots['max_value']) {
        preparePeopleList(slots['max_value'])
      }
    }
    resetValue(4)
  }

  const preparePeopleList = (people) => {
    const numberOfPeople = []
    if (people && !isNaN(people)) {
      people = parseInt(people)
      for (let index = 0; index < people; index++) {
        numberOfPeople.push({
          label: `${index + 1}`,
          value: index + 1
        })
      }
      setNumberOfPeople(numberOfPeople)
    }
  }

  const onPersonChange = (person) => {
    setPerson(person)
    resetValue(5)
  }

  const onPersonInputChange = (person = '') => {
    let isValidPerson = true
    const regex = /^[0-9]*$/
    const result = person.search(regex)
    if (result == -1) {
      isValidPerson = false
    } else {
      if (person.indexOf('.') > -1) {
        isValidPerson = false
      } else {
        if (Number(person) === 0 || Number(person) > 50) {
          isValidPerson = false
        } else {
          isValidPerson = true
          resetValue(5)
        }
      }
    }
    setPerson({ label: person, value: person})
    setIsValidPerson(isValidPerson)
  }

  const onVisitorChange = (visitorType) => {
    setVisitorType(visitorType)
  }

  const validateForm = () => {
    const fields = [
      property.value,
      purpose.value,
      preferredDate,
      slot.value,
      person.value,
      visitorType.value,
      isTermsSelected
    ]

    for (let index = 0; index < fields.length; index++) {
      if (!fields[index]) {
        return false;
      }
    }

    return isValidPerson && true
  }

  const saveScheduleDetails = async () => {
    const payload = {
      user_id: props.userId,
      inventory_id: property.value || null,
      project_id: projectId || null,
      schedule_type: purpose.value || null,
      rm_id: rmInfo['rm_id'] || null,
      number_of_persons: person.value,
      comment: comment,
      date_of_visit: preferredDate || null,
      lunch_option: null,
      is_accepted: isTermsSelected,
      time_slot: slot.value,
      visit_start_time: null,
      visit_end_time: null,
      visitors_details: visitorType.value,
      booking_id: bookingId
    }

    setLoading(true)
    try {
      const result = await apiScheduledVisit.apiSaveScheduleVisit(payload)
      const res = result.data
      setSuccessMessage(res.msg)
      setLoading(false)
      setIsSuccessModalVisible(true)
      setComment('')
      resetValue(1)
    } catch(err) {
      setLoading(false)
      console.log(err)
      appSnakBar.onShowSnakBar(appConstant.appMessage.APP_GENERIC_ERROR, 'LONG');
    }
  } 

  const onShowTermsModal = async () => {
    const purposeId = purpose.value
    const filteredPurpose = visitTypesData.find(vt => vt.id === purposeId) || {}
    setLoading(true)
    try {
      const termsDataResult = await apiScheduledVisit.apiGetTermData(
        purposeId,
        filteredPurpose.path_alias
      );

      if (
        termsDataResult &&
        termsDataResult.data &&
        termsDataResult.data[0] &&
        termsDataResult.data[0]['page_content'] 
      ) {
        const content = termsDataResult.data[0]['page_content']
        const updatedContent = content.replace('##Project Name##', rmInfo.property_name)
        setTermsData(updatedContent);
        setTermsModalVisible(true);
      } else {
        appSnakBar.onShowSnakBar(appConstant.appMessage.APP_GENERIC_ERROR, 'LONG');
      }
      setLoading(false)
    } catch (error) {
      setLoading(false)
      console.log(error);
      appSnakBar.onShowSnakBar(appConstant.appMessage.APP_GENERIC_ERROR, 'LONG');
    }
  };

  const onHandleTermsSelection = (isSelected) => {
    setIsTermsSelected(isSelected);
  };

  const resetValue = (index) => {
    switch (index) {
      case 0:
        setProperty({ value: null, label: null })
        setPurpose({ value: null, label: null })
        setPreferredDate('')
        setSlot({ value: null, label: null })
        setPerson({ value: null, label: null })
        setVisitorType({ value: null, label: null })
        setIsTermsSelected(false)
        setIsDateAvailable(true)
        setIsSlotsAvailable(true)
      case 1:
        setPurpose({ value: null, label: null })
        setPreferredDate('')
        setSlot({ value: null, label: null })
        setPerson({ value: null, label: null })
        setVisitorType({ value: null, label: null })
        setIsDateAvailable(true)
        setIsSlotsAvailable(true)
        break;
      case 2:
        setPreferredDate('')
        setSlot({ value: null, label: null })
        setPerson({ value: null, label: null })
        setVisitorType({ value: null, label: null })
        setIsSlotsAvailable(true)
        break;
      case 3:
        setSlot({ value: null, label: null })
        setPerson({ value: null, label: null })
        setVisitorType({ value: null, label: null })
        break;
      case 4:
        setPerson({ value: null, label: null })
        setVisitorType({ value: null, label: null })
        break;
      case 5:
        setVisitorType({ value: null, label: null })
        break;
      default:
        break;
    }
  } 

  const handlePin = (title) => {
    return (
      <View style={{ flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
          <AppText style={{padding: 5,
            fontSize: appFonts.largeBold,
            color: colors.danger,
            fontFamily: appFonts.SourceSansProSemiBold,
            backgroundColor: colors.primary,
            marginBottom: 5}}>{title}</AppText>
          <Image source={require('./../assets/images/map-marker.png')} style={{width: 38, height: 38 }} />
      </View>
      )
  }

  return (
    <Screen>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <View style={styles.infoConatiner}>
            {/* propperty */}
            <AppText >Property</AppText>
              {propertyList.length === 1 ? 
                <AppTextBold style={{margin: 10, ...styles.inputBox}}>{property.label}</AppTextBold> :
                <View>
                  <TouchableOpacity onPress={() => {setIsPropertyExpanded(!isPropertyExpanded)}}>
                    <View style={styles.button}>
                      <AppText style={property.label ? styles.label : {fontSize: appFonts.largeBold}}>
                        {property.label || 'Select Property'}
                      </AppText>

                      <Image
                        source={
                          isPropertyExpanded
                            ? require('./../assets/images/up-icon-b.png')
                            : require('./../assets/images/down-icon-b.png')
                        }
                        style={styles.icon}
                      />
                    </View>
                  </TouchableOpacity>

                  {isPropertyExpanded ?
                  propertyList.map((item, index) => (
                    <TouchableOpacity
                      onPress={() => {
                        onPropertyChange(item, scheduleDetails)
                        setIsPropertyExpanded(false)
                      }}
                    >
                      <View
                        key={index}
                        style={{
                          flexDirection: 'row',
                          ...styles.dropdownMenuItem
                        }}>
                        <View>
                          <AppText style={styles.label}>{item.label}</AppText>
                        </View>
                      </View>
                    </TouchableOpacity>
                  ))
                  : <View />
                  }
                </View>
              }

              {/* purpose */}

            <AppText style={styles.title}>Purpose of Visit</AppText>

            {isPreSelectedPurpose ? 
                <AppTextBold style={{margin: 10, ...styles.inputBox}}>{purpose.label}</AppTextBold> : 
              <View>
                <TouchableOpacity disabled={!property.value} onPress={() => {setIsPurposeExpanded(!isPurposeExpanded)}}>
                  <View style={styles.button}>
                    <AppText style={purpose.label ? styles.label : {fontSize:appFonts.largeBold}}>
                      {purpose.label || 'Select Purpose'}
                    </AppText>

                    <Image
                      source={
                        isPurposeExpanded
                          ? require('./../assets/images/up-icon-b.png')
                          : require('./../assets/images/down-icon-b.png')
                      }
                      style={styles.icon}
                    />
                  </View>
                </TouchableOpacity>

                {isPurposeExpanded ?
                visitTypeList.map((item, index) => (
                  <TouchableOpacity
                    onPress={() => {
                      onPurposeChange(item, scheduleDetails, property)
                      setIsPurposeExpanded(false)
                    }}
                  >
                    <View
                      key={index}
                      style={{
                        flexDirection: 'row',
                        ...styles.dropdownMenuItem
                      }}>
                      <View>
                        <AppText style={styles.label}>{item.label}</AppText>
                      </View>
                    </View>
                  </TouchableOpacity>
                ))
                : <View />
                }
              </View>
            }

            <View>

              <AppText style={styles.dateTimeTitle}>Preferred Date</AppText>

              <TouchableOpacity
                disabled={!purpose.value}
                onPress={() => setShowCalendar(!showCalendar)}
              >
                <View style={styles.button}>
                  {!showCalendar ? 
                  preferredDate ? <AppText style={styles.label} >{moment(preferredDate).format("DD-MMM-YYYY")}</AppText> :
                  <AppText style={{...styles.dropdownPlaceholder}}>{'DD/MM/YYYY'}</AppText> :
                  <View/>
                }

                  <Image
                    source={
                      showCalendar
                        ? require('./../assets/images/up-icon-b.png')
                        : require('./../assets/images/down-icon-b.png')
                    }
                    style={styles.icon}
                  />
                </View>
              </TouchableOpacity>
              {!isDateAvailable ?
                <View>
                  <AppText style={{ color: colors.danger }}>No date available for this visit</AppText>  
                </View> : <View />
              }
              {showCalendar ?
                <Calendar
                  minDate={fromDate}
                  maxDate={toDate}
                  onDayPress={({dateString}) => {
                    onDateChange(dateString)
                    setShowCalendar(false)
                  }}
                  markedDates={getMarkedDates(daysConfig)}
                /> :
                <View />
              }
            </View>

              {/* preffered time */}
            <View>
              <AppText style={styles.title}>Preferred Time</AppText>

              <View>
              <TouchableOpacity disabled={!preferredDate} onPress={() => {setIsPreferredTimeExpanded(!isPreferredTimeExpanded)}}>
                <View style={styles.button}>
                  <AppText style={slot.label ? styles.label : {fontSize: appFonts.largeBold}}>
                    {slot.label || 'Select Slot'}
                  </AppText>

                  <Image
                    source={
                      isPreferredTimeExpanded
                        ? require('./../assets/images/up-icon-b.png')
                        : require('./../assets/images/down-icon-b.png')
                    }
                    style={styles.icon}
                  />
                </View>
              </TouchableOpacity>

              {isPreferredTimeExpanded ?
              slots.map((item, index) => (
                <TouchableOpacity
                  onPress={() => {
                    onSlotChange(item, scheduleDetails)
                    setIsPreferredTimeExpanded(false)
                  }}
                >
                  <View
                    key={index}
                    style={{
                      flexDirection: 'row',
                      ...styles.dropdownMenuItem
                    }}>
                    <View>
                      <AppText style={styles.label}>{item.label}</AppText>
                    </View>
                  </View>
                </TouchableOpacity>
              ))
              : <View />
              }
            </View>
              {!isSlotsAvailable && isPreferredTime ?
                <View>
                  <AppText style={{ color: colors.danger }}>No Slot available please try another date</AppText>  
                </View> : <View />
              }
            </View>

              {/* persons */}
            <AppText style={styles.title}>Person(s)</AppText>

            {isPreferredTime ? 

            <View>
            <TouchableOpacity disabled={!slot.value} onPress={() => {setIsPersonExpanded(!isPersonExpanded)}}>
              <View style={styles.button}>
                <AppText style={person.label ? styles.label : {fontSize: appFonts.largeBold}}>
                  {person.label || 'Select'}
                </AppText>

                <Image
                  source={
                    isPersonExpanded
                      ? require('./../assets/images/up-icon-b.png')
                      : require('./../assets/images/down-icon-b.png')
                  }
                  style={styles.icon}
                />
              </View>
            </TouchableOpacity>

            {isPersonExpanded ?
            numberOfPeople.map((item, index) => (
              <TouchableOpacity
                onPress={() => {
                  onPersonChange(item, scheduleDetails)
                  setIsPersonExpanded(false)
                }}
              >
                <View
                  key={index}
                  style={{
                    flexDirection: 'row',
                    ...styles.dropdownMenuItem
                  }}>
                  <View>
                    <AppText style={styles.label}>{item.label}</AppText>
                  </View>
                </View>
              </TouchableOpacity>
            ))
            : <View />
            }
            </View> : 
              <View> 
                <TextInput
                  value={person.value}
                  style={styles.inputBox}
                  placeholder="Enter Person(s)"
                  onChangeText={onPersonInputChange}
                  editable={!!slot.value}
                />
                {!isValidPerson ?
                  <View>
                    <AppText style={{ color: colors.danger }}>Enter valid number (between 1 to 50)</AppText>  
                  </View> : <View />
                }
                
              </View>
            }

            {/* visitors details */}
            <AppText style={styles.title}>Visitors's details</AppText>

            <View>
                  <TouchableOpacity disabled={!person.value} onPress={() => {setIsVisitorExpanded(!isVisitorExpanded)}}>
                    <View style={styles.button}>
                      <AppText style={visitorType.label ? styles.label : {fontSize: appFonts.largeBold}}>
                        {visitorType.label || 'Select'}
                      </AppText>

                      <Image
                        source={
                          isVisitorExpanded
                            ? require('./../assets/images/up-icon-b.png')
                            : require('./../assets/images/down-icon-b.png')
                        }
                        style={styles.icon}
                      />
                    </View>
                  </TouchableOpacity>

                  {isVisitorExpanded ?
                  visitorTypes.map((item, index) => (
                    <TouchableOpacity
                      onPress={() => {
                        onVisitorChange(item, scheduleDetails)
                        setIsVisitorExpanded(false)
                      }}
                    >
                      <View
                        key={index}
                        style={{
                          flexDirection: 'row',
                          ...styles.dropdownMenuItem
                        }}>
                        <View>
                          <AppText style={styles.label}>{item.label}</AppText>
                        </View>
                      </View>
                    </TouchableOpacity>
                  ))
                  : <View />
                  }
                </View>

            <AppText style={styles.title}>
              Type your instructions/requests
            </AppText>
            <View>
              <TextInput
                value={comment}
                style={styles.comment}
                multiline={true}
                numberOfLines={5}
                placeholder="type your comment here"
                onChangeText={text => setComment(text)}
              />
            </View>
          </View>

          {!!purpose.value ? 
            <View style={styles.checkboxContainer}>
              <TouchableWithoutFeedback
                onPress={() => {
                  onHandleTermsSelection(!isTermsSelected);
                }}>
                <Image
                  style={styles.checkbox}
                  source={
                    isTermsSelected == true
                      ? require('./../assets/images/checked-icon.png')
                      : require('./../assets/images/unchecked-icon.png')
                  }
                />
              </TouchableWithoutFeedback>

              <AppText style={styles.checkboxLabel}>
                I accept &nbsp;
                <AppClickableText
                  onPress={onShowTermsModal}
                  style={{fontSize: appFonts.largeFontSize, textDecorationLine: 'underline'}}>
                  Terms &amp; Conditions
                </AppClickableText>
              </AppText>
            </View> :
            <View />
          }

          <AppButton onPress={saveScheduleDetails} color={!validateForm() ? "lightGray" : 'secondary'} disabled={!validateForm()} title="Submit" />

          {property.value ?
            <View style={styles.mapContainer}>
              <View style={styles.mapView}>
                <MapView
                  provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                  style={styles.map}
                  region={{
                    latitude: latitude
                      ? Number(latitude)
                      : 0,
                    longitude: longitude
                      ? Number(longitude)
                      : 0,
                      latitudeDelta: 0.9004,
                      longitudeDelta: 0.90121,
                  }}>
                  <Marker
                    coordinate={{
                      latitude: latitude
                        ? Number(latitude)
                        : 0,
                      longitude: longitude
                        ? Number(longitude)
                        : 0,
                    }}
                    title={rmInfo.property_name}
                  >
                    {handlePin(rmInfo.property_name)}
                  </Marker>
                </MapView>
              </View>
              <TouchableWithoutFeedback onPress={openDeviceMap}>
                <View style={styles.mapOverlayContainer}></View>
              </TouchableWithoutFeedback>
            </View> :
            <View />
          }

          {rmInfo && rmInfo.property_name ? 
            <View style={{ flexDirection: 'row'}}>
              <View style={styles.markerIcon}>
                <Image
                  style={{ width: 30, height: 30 }}
                  source={require('./../assets/images/visit-icon.png')}
                />
              </View>
              <View style={styles.locationBlock}>
                <AppTextBold style={styles.mapTitle}>{rmInfo.property_name}</AppTextBold>
                <View style={styles.rmAddress}>
                <AppText>{" "}</AppText>
                <View style={{width:"90%",right:20,justifyContent:"center",alignItems:"center"}}>
                  
                <HTML
                allowFontScaling={false} 
                  html={`<div style="font-size: 18px; color: #a9a9a9;">${rmInfo.present_address_1}</div>`}
                />
                </View>
               <AppText>{" "}</AppText>
                </View>
              </View>
            </View> : <View />
          }
            
          </View>
      </ScrollView>

      <Modal
        animationType="fade"
        transparent={true}
        visible={termsModalVisible}
        onRequestClose={() => {
          setTermsModalVisible(!termsModalVisible);
        }}>
        <ScheduleVisitTermModalScreen
          onCancel={() => {
            setTermsModalVisible(!termsModalVisible);
          }}
          onContinue={() => {
            setTermsModalVisible(!termsModalVisible);
            if (!isTermsSelected) {
              onHandleTermsSelection(true);
            }
          }}
          termsData={termsData}
        />
      </Modal>

            <Modal
                transparent={true}
                visible={isSuccessModalVisible}
                animationType={'fade'}>
                <View style={styles.centeredView}>
                    <View style={styles.modal}>
                        <View style={styles.cancelContainer}>
                            <TouchableWithoutFeedback onPress={() => {setIsSuccessModalVisible(false) }}>

                                <Image
                                    source={require('./../assets/images/cancel.png')}
                                    style={styles.icon}
                                />

                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.content}>
                            <View style={{ height: 80 }}>
                                <Image
                                    source={require('./../assets/images/tick_big.png')}
                                    style={styles.image}
                                />
                            </View>
                            <AppText>{successMessage}</AppText>
                        </View>

                    </View>
                </View>
            </Modal>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: '8%',
    marginBottom: '50%',
  },
  dropdownPreferredDate: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
    paddingLeft: 10,
  },
  down: {
    height: 9,
    width: 9,
    top: '2%',
    right: 20,
  },
  image: {
    height: 40,
    width: 40,
    borderRadius: 20,
    marginTop: '3%',
    marginBottom: '3%',
  },
  infoConatiner: {
    flex: 1,
    justifyContent: 'center',
  },
  iconPhn: {
    height: 34,
    width: 34,
    borderRadius: 2,
    marginTop: '3%',
    marginBottom: '3%',
    marginLeft: 8,
  },
  imgConatiner: {flexDirection: 'row'},
  imageTitle: {marginTop: '5%', marginLeft: 15},
  mapTitle: {marginBottom:-15,marginLeft: 10},
  card: {
    height: windowHeight * 0.32,
    width: '99%',
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
    padding: 15,
  },
  title: {
    marginTop: '3%',
    marginBottom: '5%',
  },
  dateTime: {
    borderBottomColor: colors.lightGray,
    borderBottomWidth: 1,
    width: '99%',
    height: '6.8%',
  },
  dropdownPlaceholder: {
    fontSize: appFonts.largeBold,
    fontFamily: 'SourceSansPro-Regular',
  },
  dateTimeTitle: {marginTop: '5%'},
  comment: {
    borderWidth: 2,
    borderColor: '#ededed',
    marginBottom: '5%',
    width: '100%',
    backgroundColor: '#fff',
    textAlignVertical: 'top',
  },
  inputBox: {
    borderBottomWidth: 1,
    borderBottomColor: '#E7E4E0',
    width: '100%',
    paddingBottom: 10,
    fontSize: appFonts.largeBold,
    fontFamily: 'SourceSansPro-SemiBold',
  },
  mapContainer: {
    height: windowHeight / 4,
    width: '100%',
    marginVertical: 20,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  label: {
    fontSize:appFonts.largeBold,
    fontFamily: 'SourceSansPro-SemiBold',

    marginBottom: '5%',
  },
  dropDownItem: {
    borderBottomWidth: 1,
    borderBottomColor: '#ededed',
    marginTop: '5%',
    justifyContent: 'flex-start'
  },
  mapOverlayContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
  mapView: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.jaguar,
    textTransform: 'uppercase',
    marginBottom: '10%',
  },
  dropDown: {
    // marginTop:10,
    height: '100%',
    width: windowWidth * 0.9,
    right: '5%',
    marginBottom: '5%',
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderTopWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: colors.lightGray,
  },
  checkboxContainer: {
    flexDirection: 'row',
    paddingBottom: '5%',
    alignItems: 'center',
  },
  checkboxLabel: {
    fontSize: appFonts.largeFontSize,
    color: colors.jaguar,
    margin: 8,
    fontFamily: appFonts.SourceSansProRegular,
  },
  checkbox: {
    height: 24,
    width: 24
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: windowHeight,
    width: windowWidth,
    backgroundColor: colors.blurColor,
  },
  modal: {
    backgroundColor: "white",
    width: "90%",
    padding: 20,
    paddingTop: 10,
    backgroundColor: colors.milkWhite,
    borderRadius: 6,
  },
  cancelContainer: {
    justifyContent: "flex-end",
    alignItems: "flex-end",
    marginTop: "5%",
  },
  icon: {
    width: 15,
    height: 15,
  },
  image: { 
    height: 51,
    width: 51,
    marginBottom: 10,
    padding: 10,
  },
  content: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: "5%",
    padding: 5,
    marginBottom: "3%"
  },
  markerIcon: { 
    borderColor: colors.gray,
    borderWidth: 1,
    borderRadius: 30,
    padding: 10,
    alignSelf: 'center',
    margin: 5
  },
  locationBlock: {
    marginLeft: 3
  },
  rmAddress: {
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'stretch',
    paddingHorizontal: 10,
  },
  button: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    borderBottomWidth: 0.5,
    borderColor: colors.gray2,
  },
  dropdownMenuItem: {
    width: '100%',
    height: 60,
    padding: 5,
    borderWidth: 0.7,
    borderColor: colors.gray2,
    alignItems: 'center'
  }
});

export default ScheduleVisitAdd;
