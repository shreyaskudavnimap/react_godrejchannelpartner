import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';

import AppText from '../components/ui/ AppText';

import appFonts from '../config/appFonts';
import colors from '../config/colors';

const windowWidth = Dimensions.get('window').width;

const EOIPreferenceDetailsComponent = ({
  typologyData,
  towerData,
  floorBandData,
  isPlotProject,
  preferenceCombination,
  onChangePreferenceData,
  onPress,
}) => {
  return (
    <View style={styles.viewContainer}>
      <View style={{paddingHorizontal: 25}}>
        <TouchableWithoutFeedback onPress={onPress}>
          <View style={styles.titleLine}>
            <AppText style={styles.title}>Preference Details</AppText>
            <Image
              source={require('./../assets/images/up-icon-b.png')}
              style={styles.icon}
            />
          </View>
        </TouchableWithoutFeedback>

        {typologyData && typologyData.length > 0 ? (
          <View  style={
            towerData && towerData.length > 0
              ? styles.subContainerBorder
              : styles.subContainer
          }>
            <AppText style={styles.subTitle}>
              {isPlotProject ? 'Plot Type' : 'Typology'}
            </AppText>
            <ScrollView
              style={styles.buttonRow}
              horizontal={true}
              bounces={false}
              showsHorizontalScrollIndicator={false}>
              {typologyData.map((data) => (
                <TouchableOpacity
                  key={data.id}
                  activeOpacity={0.8}
                  style={[
                    styles.button,
                    {
                      backgroundColor: data.selectedTypologyBooking
                        ? colors.secondary
                        : colors.primary,
                    },
                  ]}
                  onPress={() => onChangePreferenceData(data, 'typology')}>
                  <AppText
                    style={[
                      styles.text,
                      {
                        color: data.selectedTypologyBooking
                          ? colors.primary
                          : colors.secondary,
                      },
                    ]}>
                    {data.name}
                  </AppText>
                </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
        ) : null}

        {towerData &&
        towerData.length > 0 &&
        preferenceCombination &&
        (preferenceCombination.toLowerCase() === 'typology_tower' ||
          preferenceCombination.toLowerCase() === 'typology_tower_floor') ? (
          <View
            style={
              floorBandData && floorBandData.length > 0
                ? styles.subContainerBorder
                : styles.subContainer
            }>
            <AppText style={styles.subTitle}>
              {isPlotProject ? 'Parcel' : 'Tower'}
            </AppText>
            <ScrollView
              style={styles.buttonRow}
              horizontal={true}
              bounces={false}
              showsHorizontalScrollIndicator={false}>
              {towerData.map((data) => (
                <TouchableOpacity
                  key={data.id}
                  activeOpacity={0.8}
                  style={[
                    styles.button,
                    {
                      backgroundColor: data.selectedTowerBooking
                        ? colors.secondary
                        : colors.primary,
                    },
                  ]}
                  onPress={() => onChangePreferenceData(data, 'tower')}>
                  <AppText
                    style={[
                      styles.text,
                      {
                        color: data.selectedTowerBooking
                          ? colors.primary
                          : colors.jaguar,
                      },
                    ]}>
                    {data.name}
                  </AppText>
                </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
        ) : null}

        {floorBandData &&
        floorBandData.length > 0 &&
        preferenceCombination &&
        preferenceCombination.toLowerCase() === 'typology_tower_floor' ? (
          <View style={styles.subContainer}>
            <AppText style={styles.subTitle}>
              {isPlotProject ? 'Street' : 'Floor Band'}
            </AppText>
            <ScrollView
              style={styles.buttonRow}
              horizontal={true}
              bounces={false}
              showsHorizontalScrollIndicator={false}>
              {floorBandData.map((data) => (
                <TouchableOpacity
                  key={data.id}
                  activeOpacity={0.8}
                  style={[
                    styles.button,
                    {
                      backgroundColor: data.selectedFloorBooking
                        ? colors.secondary
                        : colors.primary,
                    },
                  ]}
                  onPress={() => onChangePreferenceData(data, 'floorBand')}>
                  <AppText
                    style={[
                      styles.text,
                      {
                        color: data.selectedFloorBooking
                          ? colors.primary
                          : colors.jaguar,
                      },
                    ]}>
                    {data.name}
                  </AppText>
                </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  viewContainer: {
    width: windowWidth,
    // paddingBottom: 20,
    marginBottom: 25,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  titleLine: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
    color: colors.jaguar,
  },
  icon: {
    width: 16,
    height: 16,
  },
  subContainer: {
    paddingBottom: 10,
    marginBottom: 20,
  },
  subContainerBorder: {
    borderBottomWidth: 1,
    borderBottomColor: colors.veryLightGray,
    paddingBottom: 10,
    marginBottom: 20,
  },

  subTitle: {
    fontSize: appFonts.largeBold,
    color: colors.jaguar,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  buttonRow: {
    flexDirection: 'row',
    paddingVertical: 10,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 16,
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
    marginVertical: 8,
    marginRight: 20,
  },
  text: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
});

export default EOIPreferenceDetailsComponent;
