import React from 'react';
import { View, Text, Image } from 'react-native';
import AppTextWhiteBold from './ui/AppTextWhiteBold';
const LivingExpComponent = (props) => {
    return (
        <View style={{ paddingTop:'4%',paddingBottom:'4%', backgroundColor: 'black', marginTop: '2%', marginBottom: '2%', flexDirection: 'row' }}>
            <View style={{ width: '75%', justifyContent: 'center', marginLeft: '5%' }}>
    <AppTextWhiteBold>{props.children}</AppTextWhiteBold>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Image source={require('../assets/images/chevron_right.png')} />
            </View>
        </View>
    )
}
export default LivingExpComponent;