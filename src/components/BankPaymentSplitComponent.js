import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';

import CheckBox from '@react-native-community/checkbox';

import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import colors from '../config/colors';
import appFonts from '../config/appFonts';

const BankPaymentSplitComponent = (props) => {
  var bankData = props.bankAccList.slice(0);
  const [selectedAll, isSelectedAll] = useState(true);
  // const [bankData, setBankData] = useState(bookingAccList.slice(0));

  // const [totalPayment, setTotalPayment] = useState(0);

  // useEffect(() => {

  //   onSelectAll(true);
  // }, []);

  const onSelectAll = (flag) => {
    isSelectedAll(flag);
    let paymentAmount = 0;
    bankData.map((accItem, index) => {
      if (flag) {
        bankData[index].isChecked = true;
        let fieldAmount = accItem.due_amount > 0 ? accItem.due_amount.toFixed(2) : "";
        bankData[index].field_amount = fieldAmount;
      } else {
        bankData[index].isChecked = false;
        bankData[index].field_amount = ""
      }

      if (accItem.due_amount > 0 && flag) {
        paymentAmount = paymentAmount + parseFloat(accItem.due_amount);
      }
    });
    props.setTotalPayment(paymentAmount.toFixed(2));
    props.setBankAccList(bankData);
    // setTotalPayment(paymentAmount.toFixed(2))
    // setBankData(bankData);
  };

  const markInput = (index, isChecked) => {
    if (isChecked) {
      bankData[index].isChecked = false;
      bankData[index].field_amount = "";
    } else {
      bankData[index].isChecked = true;
      let fieldAmount = bankData[index].due_amount > 0 ? bankData[index].due_amount.toFixed(2) : "";
      bankData[index].field_amount = fieldAmount;
    }

    let paymentAmount = 0
    let selectAll = true;
    bankData.map((accItem, index) => {
      if (accItem.field_amount > 0) {
        paymentAmount = paymentAmount + parseFloat(accItem.field_amount);
      }
      selectAll = selectAll * accItem.isChecked;
    });
    isSelectedAll(selectAll);
    console.log("selectAll", selectAll);
    console.log("bankData", bankData);
    props.setTotalPayment(paymentAmount.toFixed(2));
    props.setBankAccList(bankData);
    // setTotalPayment(paymentAmount.toFixed(2));
    // setBankData(bankData);

  }

  const updateAmount = (field_amount, index) => {
    bankData[index].field_amount = field_amount;
    let paymentAmount = 0
    bankData.map((accItem, index) => {
      if (accItem.field_amount > 0) {
        paymentAmount = paymentAmount + parseFloat(accItem.field_amount);
      }
    });
    // console.log("bankData UPDATE", bankData);
    props.setTotalPayment(paymentAmount.toFixed(2));
    props.setBankAccList(bankData);
    // setBankData(bankData);
    // setTotalPayment(paymentAmount.toFixed(2));
  }
  // console.log("bankData", bankData);
  return (
    <View style={styles.tabConatiner}>
      {bankData && bankData.length > 0 ? (
        <>
          <View style={styles.checkboxContainer}>
            <TouchableWithoutFeedback onPress={() => { onSelectAll(!selectedAll) }}>
              <Image
                style={styles.checkbox}
                source={
                  selectedAll ? require('./../assets/images/checked-icon.png')
                    : require('./../assets/images/unchecked-gray-icon.png')
                }
              />
            </TouchableWithoutFeedback>
            <AppText style={styles.checkboxLabel}>Select All</AppText>
          </View>

          {bankData.map((accItem, index) => (
            <View style={styles.amountType} key={index}>
              <View style={styles.checkboxContainer}>
                <TouchableWithoutFeedback
                  onPress={() => {
                    markInput(index, accItem.isChecked);
                  }}>
                  <Image
                    style={styles.checkbox}
                    source={
                      accItem.isChecked
                        ? require('./../assets/images/checked-icon.png')
                        : require('./../assets/images/unchecked-gray-icon.png')
                    }
                  />
                </TouchableWithoutFeedback>
                <View style={styles.item}>
                  <View>
                    <AppText style={styles.itemLabel}>Account Name</AppText>
                    <AppText style={styles.itemValue}>{accItem.field_favoring} {accItem.field_favoring}</AppText>
                    <AppText style={styles.itemLabel}>Bank Account Number</AppText>
                    <AppText style={styles.itemValue}>{accItem.field_ac_number}</AppText>
                    <View style={styles.amountBlock}>
                      <View style={{ width: '50%' }}>
                        <AppText style={styles.itemLabel}>Due Amount</AppText>
                        <AppText style={styles.itemValue}>{'\u20B9'}{accItem.field_amount_formated}</AppText>
                      </View>
                      <View style={{ width: '50%' }}>
                        <View style={styles.amountInput}>
                          <AppText >&#8377;</AppText>
                          <TextInput
                            style={styles.input}
                            placeholder="Enter Amount"
                            onChangeText={(text) => updateAmount(text, index)}
                            maxLength={10}
                            contextMenuHidden={true}
                            keyboardType={'number-pad'}
                            editable={accItem.isChecked}
                            defaultValue={accItem.field_amount}
                          />
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          ))}
        </>
      ) : (
          <View style={styles.amountType}>
            <AppText style={styles.errorBlock}>No bank details found for this booking</AppText>
          </View>
        )}

      <View style={styles.paymentContainer}>
        <View style={{ flex: 0.5, paddingHorizontal: '7%' }}>
          <AppText style={{ color: colors.gray3 }}>Total Payment</AppText>
        </View>
        <View style={{ flex: 0.5 }}>
          <AppTextBold style={{ fontSize: appFonts.largeBold }}>
            {'\u20B9'}
            {props.totalPayment}
          </AppTextBold>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  tabConatiner: {
    paddingTop: '5%',
    marginBottom: 20,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
    marginHorizontal: -15
  },
  amountType: {

  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    margin: 6
  },
  itemLabel: {
    width: '100%',
    color: colors.gray3,
    margin: 2
  },
  itemValue: {
    width: '90%',
    marginBottom: 10,
    flexWrap: 'wrap', 
    flexShrink: 1
  },
  input: {
    width: '75%',
    borderBottomWidth: 1,
    borderBottomColor: colors.gray3,
  },
  amountBlock:{
    flex: 1, 
    flexDirection: 'row', 
    width: '100%',
    alignItems: 'center', 
    justifyContent: 'space-between',
  },
  amountInput: {
    flex: 1,
    flexDirection: "row",
    alignItems: 'center',
    fontSize: appFonts.largeBold,
    marginTop:-15
  },
  text: {
    fontSize: appFonts.smallFontSize,
    color: colors.gray3,
  },
  paymentContainer: {
    width: '100%',
    height: 45,
    marginTop: '5%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: colors.whiteSmoke,
  },
  checkboxContainer: {
    flex: 1,
    flexDirection: 'row',
    // padding: '3%',
    paddingHorizontal: '3%',
    alignItems: 'flex-start'
  },
  checkboxLabel: {
    color: colors.gray3,
    margin: 6,
  },
  checkbox: { 
    height: 20, 
    width: 20,
    marginTop: 6
   },
  errorBlock: {
    padding: 15
  },
});

export default BankPaymentSplitComponent;
