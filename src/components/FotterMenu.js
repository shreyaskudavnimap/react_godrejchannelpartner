import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image, Dimensions } from 'react-native';
var { height, width } = Dimensions.get('window');
import gloalStyles from '../styles/GlobleStyle';
export default class FooterMenu extends Component {
  render() {
    return (

      <View style={gloalStyles.footerMenu}>
        <View style={{ width: '20%', alignItems: "center" }}>
          <View style={{ height: 30, width: 29, justifyContent: 'center', alignItems: "center" }}>
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('Home') }} style={{ justifyContent: 'center', alignItems: 'center', padding: '2%' }}>
              <Image source={ require('../assets/images/icon-home-active.png') } style={{ height: 24, width: 21, marginTop: 4 }} />
            </TouchableOpacity>
          </View>
          <Text>Home</Text>
        </View>

        <View style={{ width: '20%', alignItems: "center" }}>
          <View style={{ height: 30, width: 32,   justifyContent: 'center', alignItems: "center" }}>
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('Myloyalty') }} style={{ justifyContent: 'center', alignItems: 'center', padding: '2%' }}>
            <Image source={ require('../assets/images/icon-search.png') } style={{ height: 24, width: 24, marginTop: 4 }} />
            </TouchableOpacity>
          </View>
          <Text >Search</Text>
        </View>
        <View style={{ width: '20%', alignItems: "center" }}>
          <View style={{ height: 30, width: 32,   justifyContent: 'center', alignItems: "center" }}>
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('Orders') }} style={{ justifyContent: 'center', alignItems: 'center', padding: '2%' }}>
            <Image source={ require('../assets/images/icon-phone.png') } style={{ height: 24, width: 24, marginTop: 4 }} />
            </TouchableOpacity>
          </View>
          <Text >Contact</Text>
        </View>

        <View style={{ width: '20%', alignItems: "center" }}>
          <View style={{ height: 30, width: 32, justifyContent: 'center', alignItems: "center" }}>
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('Offers') }} style={{ justifyContent: 'center', alignItems: 'center', padding: '2%' }}>
            <Image source={ require('../assets/images/icon-calendar.png') } style={{ height: 24, width: 24, marginTop: 4 }} />
            </TouchableOpacity>
          </View>
          <Text >Visit</Text>
        </View>
        <View style={{ width: '20%', alignItems: "center" }}>
          <View style={{ height: 30, width: 32, justifyContent: 'center', alignItems: "center" }}>
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('Menu') }} style={{ justifyContent: 'center', alignItems: 'center', padding: '2%' }}>
            <Image source={ require('../assets/images/open-menu.png') } style={{ height: 20, width: 20, marginTop: 4 }} />
            </TouchableOpacity>
          </View>
          <Text >Menu</Text>
        </View>
      </View>

    );
  }
}
