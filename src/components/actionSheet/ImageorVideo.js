import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';
import colors from '../../config/colors';
import AppText from '../ui/ AppText';
import appFonts from '../../config/appFonts';

const ImageorVideo = ({title = 'Choose File', isShowRemoveItem = false, onPress}) => {
  return (
    <View style={styles.modalContent}>
      <AppText style={styles.title}>{title}</AppText>

      <View style={styles.itemContainer}>
        {isShowRemoveItem && (
          <TouchableWithoutFeedback onPress={()=> onPress('Remove')}>
            <View style={styles.item}>
              <Image
                style={styles.itemImg}
                source={require('./../../assets/images/delete-icon-b.png')}
              />
              <AppText style={styles.itemTxt}>Remove Photo</AppText>
            </View>
          </TouchableWithoutFeedback>
        )}

        <TouchableWithoutFeedback onPress={()=> onPress('Photos')}>
          <View style={styles.item}>
            <Image
              style={styles.itemImg}
              source={require('./../../assets/images/gallery-icon-b.png')}
            />
            <AppText style={styles.itemTxt}>Upload Photo</AppText>
          </View>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback onPress={()=> onPress('Video')}>
          <View style={styles.item}>
            <Image
              style={styles.itemImg}
              source={require('./../../assets/images/video-b.png')}
            />
            <AppText style={styles.itemTxt}>Upload Video</AppText>
          </View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  modalContent: {
    backgroundColor: colors.primary,
    paddingHorizontal: 15,
    paddingVertical: 20,
  },
  title: {
    fontFamily: appFonts.SourceSansProBold,
    fontSize: appFonts.xxlargeFontSize,
    textTransform: 'uppercase',
    marginBottom: 24,
    color: colors.jaguar,
  },
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
  item: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  itemImg: {width: 26, height: 26, marginVertical: 5},
  itemTxt: {
    fontFamily: appFonts.SourceSansProSemiBold,
    fontSize: appFonts.largeFontSize,
    textTransform: 'capitalize',
    color: colors.jaguar,
  },
});

export default ImageorVideo;
