import React from 'react';
import {View,Text} from 'react-native';
import globlaStyles from '../../styles/GlobleStyle';
const ProjectNameComponent = (props) =>{
    return(
        <View style={{marginLeft:'3%'}}>
                        <Text style={globlaStyles.headingProjectLocation}>{props.children}</Text>
                    </View>
    )
}
export default ProjectNameComponent;
