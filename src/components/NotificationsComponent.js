import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  Dimensions,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import appFonts from '../config/appFonts';
import AppText from './ui/ AppText';
import moment from 'moment';
import colors from '../config/colors';
import appConstant from '../utility/appConstant';

const windowWidth = Dimensions.get('window').width;

const NotificationsComponent = (props) => {
  const {
    title,
    field_is_message_read,
    field_created_date,
    field_created_timestamp,
  } = props.item;
  const [msgReadStatus, setMsgReadStatus] = useState(field_is_message_read);
  const navigation = useNavigation();
  const openNotification = () => {
    let item = props.item;
    console.log("item", item)
    // New Notifications
    if (item.field_is_message_read == 0) {
      props.readNotification(item.id);
      setMsgReadStatus(1);
    }

    if (item.field_mobile_app_data) {
      if (item.field_mobile_app_data.title) {
        if (item.field_mobile_app_data.title.toLowerCase() === 'birthday') {
          // -- Notification 1
          navigation.navigate('Profile');
        } else if (
          item.field_mobile_app_data.title.toLowerCase() === 'anniversary'
        ) {
          // -- Notification 2
        } else if (
          item.field_mobile_app_data.title.toLowerCase() === 'support'
        ) {
          // -- Notification 3
          navigation.navigate('ReachUs');
        } else if (
          item.field_mobile_app_data.title.toLowerCase() === 'testimonial'
        ) {
          // -- Notification 4
          navigation.navigate('Testimonial');
        } else if (
          item.field_mobile_app_data.title.toLowerCase() === 'documents' ||
          item.field_mobile_app_data.title.toLowerCase() ===
          'documents_verified_rm'
        ) {
          // -- Notification 6 // -- Notification 13
          navigation.navigate('DocumentScreen');
        } else if (
          item.field_mobile_app_data.title.toLowerCase() === 'sr_open_close'
        ) {
          // -- Notification 7
          navigation.navigate('ServiceRequest', { tabIndex: 2 });
        } else if (
          item.field_mobile_app_data.title.toLowerCase() === 'bank_details'
        ) {
          // -- Notification Bank Details Reject
          if (item.field_mobile_app_data.data.booking_id) {
            navigation.navigate('LoanEnquiryScreen', {
              bookingId: item.field_mobile_app_data.data.booking_id,
              activeTab: 'LoanDetails',
              // status: item.field_mobile_app_data.data.status
            });
          }
        } else if (
          item.field_mobile_app_data.title.toLowerCase() === 'bank_details_view'
        ) {
          // -- Notification Bank Details View
          if (item.field_mobile_app_data.data.booking_id) {
            navigation.navigate('LoanEnquiryScreen', {
              bookingId: item.field_mobile_app_data.data.booking_id,
              activeTab: 'LoanDetails',
            });
          }
        } else if (
          item.field_mobile_app_data.title.toLowerCase() ===
          'payment_realization'
        ) {
          // -- Notification 11
          if (item.field_mobile_app_data.data.booking_id) {
            // navigation.navigate("PayNowScreen", {
            //   bookingId: item.field_mobile_app_data.data.booking_id
            // });
            navigation.navigate('MyAccountScreen', {
              bookingId: item.field_mobile_app_data.data.booking_id,
            });
          }
        } else if (
          item.field_mobile_app_data.title.toLowerCase() ===
          'booking_confirmation_data'
        ) {
          // -- Notification 14
          if (item.field_mobile_app_data.data.booking_id) {
            navigation.navigate('BookingDetailsScreen', {
              bookingId: item.field_mobile_app_data.data.booking_id,
              userId: item.field_mobile_app_data.data.user_id,
            });
          }
        } else if (
          item.field_mobile_app_data.title.toLowerCase() ===
          'eoi_booking_confirmation_data'
        ) {
          // -- Notification 15
          if (item.field_mobile_app_data.data.booking_id) {
            navigation.navigate('BookingDetailsScreen', {
              bookingId: item.field_mobile_app_data.data.booking_id,
              userId: item.field_mobile_app_data.data.user_id,
            });
          }
        } else if (
          item.field_mobile_app_data.title.toLowerCase() === 'scheduled_visits'
        ) {
          // -- Notification 16
          navigation.navigate('VisitScreen');
        } else if (
          item.field_mobile_app_data.title.toLowerCase() ===
          'payment_realization_task'
        ) {
          // -- Notification 17
          if (item.field_mobile_app_data.data.booking_id) {
            // navigation.navigate("PayNowScreen", {
            //   bookingId: item.field_mobile_app_data.data.booking_id
            // });
            navigation.navigate('MyAccountScreen', {
              bookingId: item.field_mobile_app_data.data.booking_id,
            });
          }
        }
      } else if (item.field_mobile_app_data.target_link) {
        const PageApi = item.field_mobile_app_data.target_link.split('/')[1];
        if (PageApi == 'get_user_data_api.json') {
          navigation.navigate('Profile');
        } else if (PageApi == 'list_document_api.json') {
          navigation.navigate('DocumentScreen');
        } else if (PageApi == 'booking_confirmation_data.json') {
          if (item.field_mobile_app_data.data.booking_id) {
            navigation.navigate('BookingDetailsScreen', {
              bookingId: item.field_mobile_app_data.data.booking_id,
              userId: item.field_mobile_app_data.data.user_id,
            });
          }
        } else if (PageApi == 'single_service_request.json') {
          navigation.navigate('ServiceRequest', { tabIndex: 2 });
        }
      }
    }
  };
  
  return (
    <TouchableOpacity onPress={() => openNotification()}>
      <View style={styles.notificationsContainer}>
        <View style={styles.notification}>
          <View style={{ width: '100%' }}>
            <AppText
              style={
                msgReadStatus == 0 ? styles.unreadMessage : styles.readMessage
              }>
              {title}
            </AppText>
          </View>
          {/* <View>
                    <TouchableOpacity>
                        <Image
                            style = {styles.trash}
                            source={require('../assets/images/delete-icon-b.png')}
                        />
                    </TouchableOpacity>
                </View> */}
        </View>
        <View>
          <AppText style={styles.time}>
            {/* {moment(new Date(field_created_date)).fromNow()} */}
            {appConstant.timeSinceAgo((+field_created_timestamp) * 1000)}
          </AppText>
          {/* {field_created_date && <AppText style={styles.time}>
            {moment(new Date((+field_created_timestamp)*1000)).fromNow()}
          </AppText>} */}
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  notificationsContainer: {
    width: windowWidth * 0.85,
    paddingBottom: 20,
    marginTop: '5%',
    borderBottomColor: '#dedede',
    borderBottomWidth: 2,
  },
  notification: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  unreadMessage: {
    color: colors.expandText,
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
    marginTop: '1%',
    marginBottom: '1%',
    lineHeight: 27,
  },
  readMessage: {
    color: colors.expandText,
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    marginTop: '1%',
    marginBottom: '1%',
    lineHeight: 27,
  },
  time: {
    fontFamily: appFonts.RobotoRegular,
    lineHeight: 27,
  },
  trash: {
    width: '50%',
    height: '50%',
  },
});

export default NotificationsComponent;
