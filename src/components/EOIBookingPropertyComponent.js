import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';
import ImageModal from 'react-native-image-modal';

import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';

import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppBookingPropertyDetails from './ui/AppBookingPropertyDetails';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const EOIBookingPropertyComponent = ({propertyDetails, onPress}) => {
  return (
    <View style={styles.viewContainer}>
      <View style={{paddingHorizontal: 25, marginBottom: 18}}>
        <TouchableWithoutFeedback onPress={onPress}>
          <View style={styles.titleLine}>
            <AppText
              style={[
                styles.title,
                {fontFamily: appFonts.SourceSansProSemiBold},
              ]}>
              Unit Preference
            </AppText>

            <Image
              source={require('./../assets/images/up-icon-b.png')}
              style={styles.icon}
            />
          </View>
        </TouchableWithoutFeedback>

        {propertyDetails.proj_name ? (
          <AppTextBold style={styles.projectName}>
            {propertyDetails.proj_name}
          </AppTextBold>
        ) : null}

        {propertyDetails.plan_image ? (
          <View style={styles.layoutImgContainer}>
            <ImageModal
              resizeMode="contain"
              imageBackgroundColor="#ffffff"
              style={styles.layoutImg}
              source={{
                uri: propertyDetails.plan_image,
              }}
            />
          </View>
        ) : null}

        {propertyDetails.unit_code ? (
          <AppBookingPropertyDetails
            label="Sample Unit"
            value={propertyDetails.unit_code}
          />
        ) : null}

        {propertyDetails.flat_type ? (
          <AppBookingPropertyDetails
            label={!propertyDetails.isPlotProject ? 'Unit Type' : 'Plot Type'}
            value={propertyDetails.flat_type}
          />
        ) : null}

        {propertyDetails.tower_name ? (
          <AppBookingPropertyDetails
            label={!propertyDetails.isPlotProject ? 'Tower' : 'Parcel'}
            value={propertyDetails.tower_name}
          />
        ) : null}

        {propertyDetails.floor_band ? (
          <AppBookingPropertyDetails
            label={!propertyDetails.isPlotProject ? 'Floor Band' : 'Street'}
            value={propertyDetails.floor_band}
          />
        ) : null}
      </View>

      <AppText style={styles.info}>
        The mentioned Sample Unit is only to represent the Typology preference
        selected by you.
      </AppText>

      <AppText style={styles.info}>
        Actual unit is subject to change and availability, and will be allotted
        as per final selection.
      </AppText>
    </View>
  );
};

const styles = StyleSheet.create({
  viewContainer: {
    width: windowWidth,
    paddingBottom: 20,
    marginBottom: 25,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  titleLine: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  icon: {
    width: 16,
    height: 16,
  },
  layoutImgContainer: {
    width: '100%',
    height: windowHeight / 4,
    marginVertical: 12,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e1e4e8',
  },
  layoutImg: {
    width: windowWidth * 0.92,
    height: windowHeight / 4,
  },
  info: {
    fontSize: appFonts.normalFontSize,
    marginBottom: 12,
    paddingHorizontal: 25,
    textAlign: 'justify',
  },
  projectName: {marginBottom: 12, fontSize: appFonts.xxlargeFontSize, textTransform: 'uppercase'},
});

export default EOIBookingPropertyComponent;
