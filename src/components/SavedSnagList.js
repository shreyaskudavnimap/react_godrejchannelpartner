import React, { useState, useEffect } from 'react';
import {
	View,
	StyleSheet,
  Image,
  TouchableOpacity,
  FlatList
} from 'react-native';
import apiRaiseasnag from '../api/apiRaiseasnag'
import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';
import colors from '../config/colors';
import appFonts from '../config/appFonts';
import AppHeader from '../components/ui/AppHeader';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppButton from '../components/ui/AppButton';

const SavedSnagList = ({ userId, bookingId, setIsLoading, navigation}) => {
	const [snagListData, setSnagListData] = useState([]);

	useEffect(() => {
    getSavedSnagList()
  }, [])

	const getSavedSnagList = async () => {
    try {
      setIsLoading(true)
      const requestData = { user_id: userId, booking_id: bookingId };
			const snagdata = await apiRaiseasnag.getSavedSnagList(requestData);
      setSnagListData(snagdata.data && snagdata.data.data ? snagdata.data.data.filter(sld => sld.total_items > 0) : [])
      setIsLoading(false)
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
	};
	
	const onEditClick = (snag) => {
		const snagListId = snag.list_id
		navigation.navigate('ViewSnagScreen', { userId, snagListId, bookingId });
	}

	if (snagListData.length > 0) {
		return (
			<View>
				<FlatList
					data={snagListData}
					renderItem={({item}) => {
						let snag = item
						return (
							<View style={styles.card}>
								<View style={styles.snagInfo}>
									<AppText style={styles.snagInfoText}>
										{item.total_items} {item.total_items > 1 ? 'sangs' : 'snag'}
									</AppText>
								</View>
								<FlatList
									data={item.snag_items}
									renderItem={({item}) => (
										<View>
											<AppText style={{...styles.snagInfoText, paddingTop: 5, paddingLeft: 5}}>
												{snag.last_saved_on}
											</AppText>
											<View style={styles.snagItems}>
												{item.images[0] ?
													<Image source={{uri: item.images[0]}} style={styles.images} /> :
													<Image source={require('./../assets/images/image-plceholder.png')} style={styles.images} />
												}
												
			
												<View
													style={{
														marginLeft: '5%',
														justifyContent: 'flex-start',
														width: '80%'
													}}>
													<AppText style={styles.titleDesign}>
														{item.location}
													</AppText>
													<AppText style={styles.descriptionDesign}>
														{item.description}
													</AppText>
			
													<View style={styles.editDeleteButton}>
														<TouchableOpacity onPress={() => onEditClick(snag)}>
															<AppText style={styles.btnText}>Edit</AppText>
														</TouchableOpacity>
													</View>
												</View>
											</View>
										</View>
									)}
								/>
							</View>
						)
					}}
				/>
	
				{/* {snagListData.map((item) => (
					<View key={item.list_id}>
				 <View style={styles.card}>
					 <View style={styles.snagInfo}>
						 <AppText style={styles.snagInfoText}>{item.last_saved_on}</AppText>
						 <AppText style={styles.snagInfoText}>
							 {item.total_items} snag
					 </AppText>
					 </View>
					 
						 {item.subcategory.map((item,index) => (
					 <View style={styles.snagItems} key={index}>
								 <Image source={item.image} style={styles.images} />
	
								 <View
									 style={{
										 marginLeft: '5%',
										 justifyContent: 'space-evenly',
									 }}>
									 <AppText style={styles.titleDesign}>
										 {item.title}
									 </AppText>
									 <AppText style={styles.descriptionDesign}>
										 {item.Description}
									 </AppText>
	
									 <View style={styles.editDeleteButton}>
										 <TouchableOpacity>
											 <AppText style={styles.btnText}>Edit</AppText>
										 </TouchableOpacity>
										 <TouchableOpacity>
											 <AppText
												 style={[styles.btnText, { marginLeft: '50%' }]}>
												 Delete
									 </AppText>
										 </TouchableOpacity>
									 </View>
								 </View>
							 </View>
						 ))}
				
				 </View>
				 </View>
			 ))} */}
	
				{/* <TouchableOpacity style={styles.addItems}>
					<Image
						style={styles.addIcon}
						source={require('./../assets/images/plus-icon.png')}
					/>
					<AppText style={styles.addMoreItems}>Add more</AppText>
				</TouchableOpacity> */}
	
				{/* <AppButton title="SUBMIT SNAG LIST" /> */}
			</View>
		);
	} else {
		return (
			<View>
				<AppText style={styles.noSnagItem}>
					No snag item in this list, add snag item.
				</AppText>
			</View>
		)
	}

};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '6%',
    paddingBottom: '6%',
  },
  pageTitle: {
    fontSize: appFonts.xxlargeFontSize,
    color: colors.secondary,
    textTransform: 'uppercase',
  },
  tabName: {
    paddingBottom: '3%',
    marginBottom: '5%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  activeTab: {
    width: '50%',
    paddingVertical: '5%',
    borderBottomWidth: 3,
    borderBottomColor: colors.gray,
    alignItems: 'center',
  },
  snagInfo: {
    paddingHorizontal: '5%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    width: '100%',
    height: 40,
    alignItems: 'flex-end',
    borderBottomColor: colors.borderGrey,
    borderBottomWidth: 1,
    paddingBottom: 5,
    
  },
  snagInfoText: {
    fontSize: appFonts.largeFontSize,
	color: colors.gray4,
  },
  images: {
    height: 100,
    width: 100,
    marginLeft:20
  
  },
  snagItems: {
    paddingHorizontal: '3%',
    marginTop: '5%',
    flexDirection: 'row',
    // justifyContent: 'center',
    width: '75%',
  },
  card: {
    // alignItems: 'center',
    paddingBottom: '5%',
    marginBottom: 30,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  titleDesign: {
    fontFamily: appFonts.SourceSansProBold,
    fontSize: appFonts.normalFontSize,
  },
  descriptionDesign: {
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.normalFontSize,
  },
  editDeleteButton: {
    flexDirection: 'row',
  },
  btnText: {
    fontSize: appFonts.mediumFontSize,
    fontWeight: 'bold',
  },
  addItems: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '38%',
    marginBottom: '5%',
  },
  addIcon: {
    height: 40,
    width: 40,
  },
  addMoreItems: {
    marginLeft: '5%',
    fontFamily: appFonts.SourceSansProBold,
    fontSize: appFonts.largeBold,
	},
	noSnagItem: {
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.xlargeFontSize,
  },
});

export default SavedSnagList;
