import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
  TouchableWithoutFeedback,
  TextInput,
  Modal,
} from 'react-native';

import {useSelector} from 'react-redux';
import appConstant from '../utility/appConstant';
import MyJourneyMenuItem from './ui/MyJourneyMenuItem';
import {
    getUserJourneyCTA,
    getUserJourney,
  } from '../store/actions/userAccountAction';
import MultiFileDownloadModal from '../screens/modals/MultiFileDownloadModal';
import useFileDownload from '../hooks/useFileDownload';
import appSnakBar from '../utility/appSnakBar';
import App_constant from '../utility/appConstant';
import AppOverlayLoader from '../components/ui/AppOverlayLoader';
const PowerOfAttorny = (props)=>{
    const { isPowerOfAttornyDraftModalOpen, setIsPowerOfAttornyDraftModalOpen, isPowerOfAttorny,setIsShowLoader} = props;
    const [modalData, setModalData] = useState({
        visible: false,
        title: '',
        content: [],
    });
    const {
        data: {userid: userID},
    } = useSelector((state) => state.loginInfo.loginResponse);
    const {
      progress,
      showProgress,
      error,
      isSuccess,
      checkPermission,
    } = useFileDownload();
    const handleFileDownload = async (imgPath, isPdf) => {
        setIsShowLoader(true);
        let fileExt = isPdf ? 'pdf' : appConstant.getExtention(imgPath);
        console.log('fileExt',fileExt)
        let fName = appConstant.getFileName(imgPath);
        fName = fName + (isPdf ? '.pdf' : '');
        if (fileExt[0] && fName) {
          downloadFile(imgPath, fName, fileExt[0]);
        } else {
          setIsShowLoader(false);
        }
      };
      const downloadFile = async (imgPath, fileName, fileExtension) => {
        try {
          await checkPermission(imgPath, fileName, fileExtension);
          setIsShowLoader(false);
          setIsPowerOfAttornyDraftModalOpen(false)
        } catch (error) {
          appSnakBar.onShowSnakBar(error.message, 'LONG');
          setIsPowerOfAttornyDraftModalOpen(false)
          setIsShowLoader(false);
        }
      };
    
      const subStagesRecurrsion = (data)=>{
        const id = isPowerOfAttorny?App_constant.qaInstance.powerOfAttorneyStage:App_constant.qaInstance.draftAgreementStage;
        if(data.id && data.id == id){
            ctaDetails(data.cta,data.name);
            return
        }
        if(data.length){   
            data.forEach((nedData)=>{
                if(nedData['sub-stages'] && nedData['sub-stages'].length)
                {
                    
                    subStagesRecurrsion(nedData['sub-stages'])
                }
                else{
                    subStagesRecurrsion(nedData)
                }
                
            })
            
        }
      }
      useEffect(() => {
        setIsShowLoader(true)
        getUserJourney(userID, props.bookingId).then(
          
          (result) => {
            setIsShowLoader(false)
            subStagesRecurrsion(result)
          },
          (error) => {
            appSnakBar.onShowSnakBar('Could not get journey details', 'LONG');
          },
        );
      }, []);
      
    const ctaDetails = (ctaUrl, title) => {
        const finalUrl = ctaUrl.replace(
          'http://43.242.212.209/gpl-project/gpl-api/',
          '',
        );
       // props.setIsLoading(true);
        getUserJourneyCTA(finalUrl, userID).then((result) => {
          if (result.length > 0) {
            switch (result[0].type) {
              case 'screen-redirect':
                setIsShowLoader(false);
                let pageName = '';
                let params = {};
                switch (result[0].screen_id) {
                  case 'LoanManagementPage':
                    pageName = 'LoanEnquiryScreen';
                    params = {bookingId: result[0].project_id};
                    break;
                  case 'ScheduleVisitPage':
                    pageName = 'PresaleVisitScreen';
                    params = {};
                    break;
                }
    
                if (pageName !== '') {
                  //props.navigation.navigate(pageName, params);
                }
    
                break;
              case 'download':
                handleFileDownload(result[0].url, false);
                break;
              case 'multiple-download':
                setIsShowLoader(false);
                setModalData({
                  title: title,
                  content: result[0].data,
                  visible: isPowerOfAttornyDraftModalOpen,
                });
                break;
            }
          }
        });
      };
    return (
    <>
    {
      <MultiFileDownloadModal
        modalData={modalData}
        setModalData={setModalData}
        handleFileDownload={handleFileDownload}
        setIsPowerOfAttornyDraftModalOpen = {setIsPowerOfAttornyDraftModalOpen}
      />
    }
    
    </>
    )
}

export default PowerOfAttorny