import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableNativeFeedback,
} from 'react-native';

import appFonts from '../config/appFonts';
import AppText from './ui/ AppText';
import AppTextBold from './ui/AppTextBold';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const SearchListComponent = ({onPress, item, onPressWishlist}) => {
  return (
    <View style={styles.searchListContainer}>
      <TouchableWithoutFeedback
        onPress={() => {
          onPress(item);
        }}>
        <View>
          {item.city ? (
            <AppTextBold>{item.city.toUpperCase()}</AppTextBold>
          ) : null}
          <View style={styles.viewItem}>
            <View style={styles.imgContainer}>
              <Image
                style={styles.viewImg}
                source={{
                  uri: item.imageUri,
                }}
              />
            </View>
            <View style={styles.viewTxtContainer}>
              <AppText numberOfLines={1} style={styles.viewTopTxt}>
                <AppText style={styles.priceTxt}>{item.title},</AppText>
                {item.address}
              </AppText>
              <AppText numberOfLines={2} style={styles.viewBottomTxt}>
                {item.details}
              </AppText>
            </View>
            <TouchableNativeFeedback onPress={onPressWishlist}>
              <View style={styles.wishlistContainer}>
                <Image
                  style={styles.wishlist}
                  source={
                    item.wishlist_status === '1'
                      ? require('./../assets/images/thumb-fill-w.png')
                      : require('../assets/images/thumb-line.png')
                  }
                />
              </View>
            </TouchableNativeFeedback>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  searchListContainer: {marginBottom: 20},
  viewItem: {
    width: '100%',
    // height: windowHeight / 1.65,
    marginBottom: 25,
    marginTop: 5,
  },
  imgContainer: {
    backgroundColor: '#e1e4e8',
    width: windowWidth,
    height: windowWidth * 0.9,
    resizeMode: 'cover',
  },
  viewImg: {width: '100%', height: '100%'},
  viewTxtContainer: {
    width: windowWidth,
    // height: windowWidth * 0.1,
    justifyContent: 'center',
    paddingTop: 20,
  },
  viewTopTxt: {
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.largeFontSize,
    marginBottom: 1,
  },
  viewBottomTxt: {
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.mediumFontSize,
    marginTop: 4,
    color: 'rgb(78,78,78)',
  },
  priceTxt: {fontFamily: appFonts.SourceSansProBold, fontSize: appFonts.largeBold},
  wishlistContainer: {
    height: 40,
    width: 40,
    position: 'absolute',
    top: 20,
    right: 20,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'rgba(0,0,0,0.6)',
    borderRadius: 20,
  },
  wishlist: {height: 24, width: 24},
});

export default SearchListComponent;
