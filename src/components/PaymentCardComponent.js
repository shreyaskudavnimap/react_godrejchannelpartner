import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';

import CheckBox from '@react-native-community/checkbox';

import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import colors from '../config/colors';
import appFonts from '../config/appFonts';

const PaymentCardComponent = (props) => {
  const [isSelectedAll, setSelectedAll] = useState(false);
  const [isSelectedInvoice, setIsSelectedInvoice] = useState(false);
  const [isSelectedInterest, setIsSelectedInterest] = useState(false);

  const [invoiceAmount, setInvoiceAmount] = useState(0);
  const [interestAmount, setInterestAmount] = useState(0);

  const totalPayment = Number(invoiceAmount) + Number(interestAmount);

  const onSelectAll = () => {
    setSelectedAll(!isSelectedAll);
    setIsSelectedInvoice(!isSelectedInvoice);
    setIsSelectedInterest(!isSelectedInterest);
  };

  return (
    <View style={styles.tabConatiner}>
      <View style={styles.propertyDetail}>
        <AppTextBold style={styles.propertyName}>
          {props.property.propertyName}
        </AppTextBold>
        <AppText style={styles.bank}>Bank A/c No. 50200030236327</AppText>
      </View>

      <View style={styles.checkboxContainer}>
        <TouchableWithoutFeedback onPress={onSelectAll}>
          <Image
            style={styles.checkbox}
            source={
              isSelectedAll == true
                ? require('./../assets/images/checked-icon.png')
                : require('./../assets/images/unchecked-gray-icon.png')
            }
          />
        </TouchableWithoutFeedback>
        <AppText style={styles.checkboxLabel}>Select All</AppText>
      </View>
      <View style={styles.amountType}>
        <View style={styles.checkboxContainer}>
          <TouchableWithoutFeedback
            onPress={() => {
              setIsSelectedInvoice(!isSelectedInvoice);
            }}>
            <Image
              style={styles.checkbox}
              source={
                isSelectedInvoice == true
                  ? require('./../assets/images/checked-icon.png')
                  : require('./../assets/images/unchecked-gray-icon.png')
              }
            />
          </TouchableWithoutFeedback>
          <AppText style={styles.checkboxLabel}>Invoice Amount</AppText>
        </View>
        <View style={{flex: 0.5}}>
          <TextInput
            style={styles.input}
            placeholder="Enter Amount"
            onChangeText={(text) => setInvoiceAmount(text)}
            keyboardType="numeric"
          />
        </View>
      </View>
      <View style={styles.amountType}>
        <View style={styles.checkboxContainer}>
          <TouchableWithoutFeedback
            onPress={() => {
              setIsSelectedInterest(!isSelectedInterest);
            }}>
            <Image
              style={styles.checkbox}
              source={
                isSelectedInterest == true
                  ? require('./../assets/images/checked-icon.png')
                  : require('./../assets/images/unchecked-gray-icon.png')
              }
            />
          </TouchableWithoutFeedback>
          <AppText style={styles.checkboxLabel}>Interest Amount</AppText>
        </View>
        <View style={{flex: 0.5}}>
          <TextInput
            style={styles.input}
            placeholder="Enter Amount"
            onChangeText={(text) => setInterestAmount(text)}
            keyboardType="numeric"
          />
          <AppText style={styles.text}>Amt. inclusive of taxes</AppText>
        </View>
      </View>
      <View style={styles.paymentContainer}>
        <View style={{flex: 0.5, paddingHorizontal: '7%'}}>
          <AppText style={{color: colors.gray3}}>Total Payment</AppText>
        </View>
        <View style={{flex: 0.5}}>
          <AppTextBold style={{fontSize: appFonts.largeBold}}>
            {'\u20B9'}
            {totalPayment}
          </AppTextBold>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  tabConatiner: {
    paddingTop: '5%',
    marginBottom: 20,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  propertyDetail: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  propertyName: {
    fontSize: appFonts.largeFontSize,
  },
  bank: {
    fontSize: appFonts.normalFontSize,
    color: colors.gray3,
  },
  amountType: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  input: {
    width: '85%',
    borderBottomWidth: 2,
    borderBottomColor: colors.gray3,
  },
  text: {
    fontSize:appFonts.normalFontSize,
    color: colors.gray3,
  },
  paymentContainer: {
    width: '100%',
    height: 80,
    marginTop: '5%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: colors.whiteSmoke,
  },
  checkboxContainer: {
    flex: 0.5,
    flexDirection: 'row',
    padding: '5%',
    alignItems: 'center',
    marginLeft: 5,
  },
  checkboxLabel: {
    color: colors.gray3,
    margin: 6,
  },
  checkbox: {height: 22, width: 22},
});

export default PaymentCardComponent;
