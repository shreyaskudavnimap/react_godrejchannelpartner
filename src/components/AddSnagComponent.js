import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Alert,
  Dimensions,
} from 'react-native';
import Modal from 'react-native-modal';
import AppText from '../components/ui/ AppText';
import AppButton from '../components/ui/AppButton';
import AppFileChooser from '../components/actionSheet/AppFileChooser';
import appFileChooser from '../utility/appFileChooser';
import colors from '../config/colors';
import appFonts from '../config/appFonts';
import Screen from '../components/Screen';
import apiRaiseASnag from '../api/apiRaiseasnag';
import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const AddSnagComponent = ({
  bookingId,
  userId,
  snagItemId,
  snagListId,
  setIsLoading,
}) => {
  const [location, setLocation] = useState('');
  const [description, setDescription] = useState('');
  const [fileChooserResponse, setFileChooserResponse] = useState([]);
  const [fileChooser, setFileChooser] = useState(false);
  const closeActionSheet = () => setFileChooser(false);

  useEffect(() => {
    if (snagItemId) {
      getSnagItemToEdit();
    }
  }, []);

  const getSnagItemToEdit = async () => {
    const payload = {
      user_id: userId,
      booking_id: bookingId,
      snag_list_id: snagListId,
      snag_item_id: snagItemId,
    };

    setIsLoading(true);

    try {
      const result = await apiRaiseASnag.getSnagItemToEdit(payload);
      const res = result.data;
      const snagItem = res.data.snag_item;
      const images = snagItem.images.map((img) => ({data: img}));
      setLocation(snagItem.location);
      setDescription(snagItem.description);
      setFileChooserResponse(images);
      setIsLoading(false);
    } catch (err) {
      setIsLoading(false);
      console.log(err);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  const onClickFileChooserItem = async (clickItem) => {
    const fileChooserArr = [...fileChooserResponse];

    switch (clickItem) {
      case 'Remove':
        setFileChooser(false);
        setFileChooserResponse([]);
        break;

      case 'Gallery':
        appFileChooser.onLaunchImageLibrary(async (response) => {
          setFileChooser(false);
          if (response) {
            if (response.didCancel) {
              // setFileChooserResponse([]);
            } else {
              const fileName = response.name
                ? response.name
                : response.uri
                ? response.uri.split('/').pop()
                : '';

              if (fileName) {
                const re = /(?:\.([^.]+))?$/;
                const ext = re.exec(fileName)[1];

                if (
                  typeof ext !== undefined &&
                  ext &&
                  (ext.toLowerCase() == 'jpg' ||
                    ext.toLowerCase() == 'jpeg' ||
                    ext.toLowerCase() == 'png')
                ) {
                  if (response && response.fileSize) {
                    if (validateFileSize(response.fileSize)) {
                      const dataUriResponse = await appFileChooser.toBase64(
                        response.uri,
                      );
                      response.data = `data:image/jpeg;base64,${dataUriResponse}`;
                      setFileChooser(false);
                      fileChooserArr.push(response);
                      setFileChooserResponse(fileChooserArr);
                    } else {
                      invalidFile();
                    }
                  }
                } else {
                  invalidFileFormat();
                }
              }
            }
          }
        });
        break;

      case 'Camera':
        appFileChooser.onLaunchCamera(async (response) => {
          setFileChooser(false);

          if (response && response.didCancel) {
            // setFileChooserResponse([]);
          } else {
            if (response && response.fileSize) {
              if (validateFileSize(response.fileSize)) {
                const dataUriResponse = await appFileChooser.toBase64(
                  response.uri,
                );
                response.data = `data:image/jpeg;base64,${dataUriResponse}`;
                fileChooserArr.push(response);
                setFileChooserResponse(fileChooserArr);
              } else {
                invalidFile();
              }
            }
          }
        });
        break;

      case 'Phone':
        try {
          const chooserResponse = await appFileChooser.onSelectFile();
          const dataUriResponse = await appFileChooser.toBase64(
            chooserResponse.uri,
          );

          chooserResponse.data = `data:image/jpeg;base64,${dataUriResponse}`;
          setFileChooser(false);

          if (chooserResponse) {
            const fileName = chooserResponse.name
              ? chooserResponse.name
              : chooserResponse.uri
              ? chooserResponse.uri.split('/').pop()
              : '';
            if (fileName) {
              const re = /(?:\.([^.]+))?$/;
              const ext = re.exec(fileName)[1];

              if (
                typeof ext !== undefined &&
                ext &&
                (ext.toLowerCase() == 'jpg' ||
                  ext.toLowerCase() == 'jpeg' ||
                  ext.toLowerCase() == 'png')
              ) {
                if (chooserResponse && chooserResponse.size) {
                  if (validateFileSize(chooserResponse.size)) {
                    fileChooserArr.push(chooserResponse);
                    setFileChooserResponse(fileChooserArr);
                  } else {
                    invalidFile();
                  }
                }
              } else {
                invalidFileFormat();
              }
            }
          }
        } catch (error) {
          console.log('Error onSelectFile : ', error);
          // setFileChooserResponse([]);
          setFileChooser(false);
        }
        break;
    }
  };

  const validateFileSize = (fileSize) => {
    let isValidFile = true;
    appFileChooser.validateFileSize(fileSize, (fileChooserResponse) => {
      isValidFile = fileChooserResponse ? true : false;
    });
    return isValidFile;
  };

  const invalidFile = () => {
    Alert.alert(
      'Oops!',
      'Invalid file size. File size should be less than 5MB',
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  };

  const invalidFileFormat = () => {
    Alert.alert(
      'Oops!',
      'Invalid file format. Allowed document type - jpg, jpeg, png',
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  };

  const validateForm = () => {
    const fields = [location, description];

    for (let index = 0; index < fields.length; index++) {
      if (!fields[index]) {
        return false;
      }
    }
    return true;
  };

  const saveSnag = async () => {
    const payload = {
      user_id: userId,
      booking_id: bookingId,
      location,
      description,
    };

    if (snagListId) {
      payload['snag_list_id'] = snagListId;
    }
    if (snagItemId) {
      payload['snag_item_id'] = snagItemId;
    }

    payload['images'] = fileChooserResponse.map((fcr) =>
      (fcr.data || '').replace('data:image/jpeg;base64,', ''),
    );
    setIsLoading(true);
    try {
      const result = await apiRaiseASnag.addSnagItem(payload);
      setIsLoading(false);
      setLocation('');
      setDescription('');
      setFileChooserResponse([]);
      const snagMsg = snagItemId
        ? 'Snag has been updated.'
        : '1 snag has been added to your list.';
      appSnakBar.onShowSnakBar(snagMsg, 'LONG');
    } catch (err) {
      setIsLoading(false);
      console.log(err);
      appSnakBar.onShowSnakBar(
        appConstant.appMessage.APP_GENERIC_ERROR,
        'LONG',
      );
    }
  };

  return (
    <Screen>
      <ScrollView>
        <View>
          {fileChooserResponse.length > 0 ? (
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                margin: 2,
                justifyContent: 'space-evenly',
              }}>
              {fileChooserResponse.map((fcr, index) =>
                fcr ? (
                  <Image
                    key={index}
                    style={{
                      width: windowWidth / 3,
                      height: windowWidth / 3,
                      margin: 5,
                      marginBottom: 10
                    }}
                    source={{uri: fcr.data}}
                  />
                ) : null,
              )}
              {fileChooserResponse.length < 5 ? (
                <TouchableOpacity
                  style={styles.imageContainerMore}
                  onPress={() => setFileChooser(true)}>
                  <Image
                    source={require('./../assets/images/camera-icon-b.png')}
                    style={styles.cameraIcon}
                  />
                  <AppText>+ Add Images</AppText>
                </TouchableOpacity>
              ) : (
                <View
                  style={{
                    width: windowWidth / 3,
                    height: windowWidth / 3,
                    margin: 5,
                  }}
                />
              )}
            </View>
          ) : (
            <TouchableOpacity
              style={styles.imageContainer}
              onPress={() => setFileChooser(true)}>
              <Image
                source={require('./../assets/images/camera-icon-b.png')}
                style={styles.cameraIcon}
              />
              <AppText>+ Add Images</AppText>
            </TouchableOpacity>
          )}

          <View style={styles.locationCon}>
            <AppText style={styles.locationText}>Location</AppText>
            <TextInput
              style={styles.locationInput}
              onChangeText={(text) => setLocation(text)}
              value={location}
              placeholder="eg. Master Bedroom"
            />
          </View>

          <TextInput
            style={styles.descriptionBox}
            onChangeText={(text) => setDescription(text)}
            multiline={true}
            numberOfLines={5}
            value={description}
            placeholder="Description"
          />

          <AppButton
            onPress={saveSnag}
            color={!validateForm() ? 'lightGray' : 'secondary'}
            disabled={!validateForm()}
            title="ADD TO SNAG LIST"
          />
        </View>
      </ScrollView>

      {fileChooser ? (
        <Modal
          isVisible={fileChooser}
          style={{
            margin: 0,
            justifyContent: 'flex-end',
          }}
          onBackButtonPress={closeActionSheet}
          onBackdropPress={closeActionSheet}
          backdropColor="rgba(0,0,0,0.5)">
          <AppFileChooser
            cameraOnly={true}
            onPress={(clickItem) => onClickFileChooserItem(clickItem)}
          />
        </Modal>
      ) : (
        <View />
      )}
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabName: {
    paddingBottom: '3%',
    marginBottom: '5%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  activeTab: {
    width: '50%',
    paddingVertical: '5%',
    borderBottomWidth: 3,
    borderBottomColor: colors.gray,
    alignItems: 'center',
  },
  imageContainer: {
    height: 150,
    width: 150,
    borderRadius: 3,
    borderWidth: 1,
    borderColor: colors.gray4,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
  imageContainerMore: {
    height: 150,
    width: 150,
    borderRadius: 3,
    borderWidth: 1,
    borderColor: colors.gray4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cameraIcon: {
    width: 35,
    height: 35,
  },
  locationCon: {
    marginVertical: '5%',
  },
  locationText: {
    fontSize: appFonts.xxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
  },
  locationInput: {
    lineHeight: 25,
   fontSize: appFonts.largeBold,
    borderBottomWidth: 1,
    borderBottomColor: colors.gray5,
  },
  descriptionBox: {
    marginVertical: '3%',
    height: 150,
    lineHeight: 25,
    fontSize:appFonts.xlargeFontSize,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: colors.gray5,
    textAlignVertical: 'top',
  },
});

export default AddSnagComponent;
