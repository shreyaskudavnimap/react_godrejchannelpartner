import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TextInput,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Modal,
  Platform,
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';

import AppText from '../components/ui/ AppText';

import appFonts from '../config/appFonts';
import colors from '../config/colors';

import AutoSearchInputModalScreen from '../screens/modals/AutoSearchInputModalScreen';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const BookingSourceComponent = ({
  onPress,
  sourceProtectionData,
  bookingSource,
  onClickRadioItem,
  sourceSelectedValue: value,
  sourcePartner,
  sourceReferral,
  sourceLoyalty,
  sourceDirect,
  changeSourceSelection,
  selectedValue,
  disabledFiled,
  isShowError,
  setDropSource,
}) => {
  const [showAutoSearchText, setShowAutoSearchText] = useState(false);
  const [showDropdown, setShowDropdown] = useState(false);

  const onSelectDefaultValue = () => {
    if (selectedValue && selectedValue != '') {
      if (sourceDirect && sourceDirect.length > 0) {
        const selectedItem = sourceDirect.find(
          (item) => item.value === selectedValue,
        );
        return selectedItem && selectedItem.value ? selectedItem.value : '';
      } else {
        return `Select`;
      }
    } else {
      return `Select`;
    }
  };

  const onShowAutoSearchText = () => {
    if (!disabledFiled) {
      setShowAutoSearchText(!showAutoSearchText);
    }
  };

  const ErrorView = (errorMsg) => {
    return (
      <View style={{marginBottom: 20, marginTop: 5}}>
        <AppText style={{color: colors.danger}}>{errorMsg}</AppText>
      </View>
    );
  };

  const [dynamicBottomPadding, setDynamicBottomPadding] = useState(0);

  return (
    <View style={[styles.viewContainer, {paddingBottom: dynamicBottomPadding}]}>
      <View style={{paddingHorizontal: 25}}>
        <TouchableWithoutFeedback onPress={onPress}>
          <View style={styles.titleLine}>
            <AppText
              style={[
                styles.title,
                {fontFamily: appFonts.SourceSansProSemiBold},
              ]}>
              How did you hear about us?
            </AppText>
            <Image
              source={require('./../assets/images/up-icon-b.png')}
              style={styles.icon}
            />
          </View>
        </TouchableWithoutFeedback>

        <View style={{paddingVertical: 10}}>
          {bookingSource &&
            bookingSource.length > 0 &&
            bookingSource.map((data) => (
              <View key={data.key} style={styles.buttonContainer}>
                <View>
                  <TouchableOpacity
                    activeOpacity={0.9}
                    style={styles.circle}
                    onPress={() => {
                      setDynamicBottomPadding(0);
                      onClickRadioItem(data.key);
                    }}>
                    {value === data.key && (
                      <View style={styles.checkedCircle} />
                    )}
                  </TouchableOpacity>
                </View>
                <View>
                  <TouchableOpacity
                    activeOpacity={0.9}
                    onPress={() => {
                      setDynamicBottomPadding(0);
                      onClickRadioItem(data.key);
                    }}>
                    <AppText style={{fontSize: appFonts.largeFontSize, zIndex: 1}}>
                      {data.title}
                    </AppText>
                  </TouchableOpacity>
                  {value === data.key &&
                    (value === '4' ? (
                      <View
                        style={{
                          ...(Platform.OS !== 'android' && {
                            zIndex: 10,
                          }),
                        }}>
                        {sourceDirect && sourceDirect.length > 0 && (
                          <DropDownPicker
                            items={
                              sourceDirect && sourceDirect.length > 0
                                ? sourceDirect
                                : []
                            }
                            defaultValue={onSelectDefaultValue()}
                            autoScrollToDefaultValue={true}
                            style={styles.dropdownDocument}
                            placeholderStyle={styles.dropdownPlaceholder}
                            itemStyle={styles.dropDownItem}
                            labelStyle={styles.label}
                            activeLabelStyle={styles.labelSelected}
                            selectedLabelStyle={styles.labelSelected}
                            // onChangeItem={(item) => {
                            //   if (item.value != 'Select') {
                            //     changeSourceSelection(item.value);
                            //   }
                            // }}
                            onChangeItem={(item) => {
                              changeSourceSelection(item.value);
                            }}
                            onOpen={() => {
                              setShowDropdown(true);
                              setDynamicBottomPadding(120);
                            }}
                            onClose={() => {
                              setShowDropdown(false);
                              setDynamicBottomPadding(0);
                            }}
                            placeholder={data.placeholderTxt}
                            disabled={disabledFiled}
                            zIndex={13000}
                            controller={(instance) => {
                              setDropSource(instance);
                            }}
                          />
                        )}
                      </View>
                    ) : (
                      <>
                        {value === '2' || value === '3' ? (
                          <View>
                            <TextInput
                              style={styles.txtInput}
                              placeholder={data.placeholderTxt}
                              value={selectedValue ? selectedValue : ''}
                              autoCapitalize="none"
                              autoCorrect={false}
                              keyboardType="default"
                              autoFocus={true}
                              editable={disabledFiled ? false : true}
                              onChangeText={(text) =>
                                changeSourceSelection(text)
                              }
                            />
                          </View>
                        ) : (
                          <View style={styles.txtAutoComplete}>
                            <TouchableOpacity
                              activeOpacity={0.9}
                              onPress={onShowAutoSearchText}>
                              <AppText
                                style={{
                                  fontSize: appFonts.largeFontSize,
                                  color: selectedValue
                                    ? colors.jaguar
                                    : colors.gray,
                                }}>
                                {selectedValue
                                  ? selectedValue
                                  : data.placeholderTxt}
                              </AppText>
                            </TouchableOpacity>
                          </View>
                        )}
                      </>
                    ))}

                  {isShowError &&
                    value &&
                    value === data.key &&
                    (!selectedValue || selectedValue.trim() == '' || selectedValue.trim() =='Select') && (
                      <>
                        {value === '1' &&
                          ErrorView('Channel Partner name is required')}
                        {value === '2' &&
                          ErrorView('Customer name is required')}
                        {value === '3' &&
                          ErrorView('Please enter project name')}
                        {value === '4' &&
                          !showDropdown &&
                          ErrorView('Please select one')}
                      </>
                    )}
                </View>
              </View>
            ))}
        </View>

        {isShowError &&
          !value &&
          !showDropdown &&
          ErrorView('Please choose your preference')}
      </View>

      <Modal
        animationType="fade"
        transparent={true}
        visible={showAutoSearchText}
        onRequestClose={onShowAutoSearchText}>
        <AutoSearchInputModalScreen
          onCancelPress={onShowAutoSearchText}
          fieldData={sourcePartner}
          searchData={selectedValue ? selectedValue : ''}
          onSelectPartnerData={(selectedData) => {
            onShowAutoSearchText();
            changeSourceSelection(selectedData);
          }}
        />
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  viewContainer: {
    width: windowWidth,
    marginBottom: 25,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
    zIndex: 10,
  },
  titleLine: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize:  appFonts.largeBold,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  icon: {
    width: 16,
    height: 16,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginBottom: 25,
  },
  circle: {
    marginRight: 10,
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: colors.secondaryLight,
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkedCircle: {
    width: 12,
    height: 12,
    borderRadius: 7,
    backgroundColor: colors.secondaryLight,
  },
  txtInput: {
    borderBottomColor: colors.gray5,
    borderBottomWidth: 1,
    fontSize: appFonts.largeFontSize,
    marginTop: 15,
    marginBottom: 8,
    width: windowWidth * 0.75,
    height: 50,
    fontFamily: appFonts.SourceSansProRegular,
  },
  dropdownDocument: {
    height: 50,
    width: windowWidth * 0.75,
    marginTop: 15,
    marginBottom: 8,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderTopWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropdownPlaceholder: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
  },
  labelSelected: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
    marginBottom: 8,
  },
  label: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    marginBottom: 8,
    textAlign: 'left',
  },
  dropDownItem: {
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGray,
    marginBottom: 8,
    marginTop: 4,
    justifyContent: 'flex-start',
  },
  txtAutoComplete: {
    borderBottomColor: colors.gray5,
    borderBottomWidth: 1,
    marginTop: 15,
    marginBottom: 8,
    width: windowWidth * 0.75,
    height: 50,
    justifyContent: 'center',
  },
});

export default BookingSourceComponent;
