import React from 'react';
import { View, Text, Image } from 'react-native';
import globlaStyles from '../styles/GlobleStyle';
import AppTextBOld from '../components/ui/AppTextBold';
import AppTextGray from '../components/ui/AppTextGray';

const VideoDiscription = () => {
    return (
        <>
            <View style={{ alignItems: 'center' }}>
                <AppTextGray>---- Featured Residences ----</AppTextGray>
            </View>
            <View style={{alignItems:'center'}}>
            
                <AppTextBOld >GODREJ LAKE GARDENS</AppTextBOld>
                <AppTextGray>Sarajapur, Bangalore</AppTextGray>
            </View>
        </>
    )
}
export default VideoDiscription;