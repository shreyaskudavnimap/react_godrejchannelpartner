import React, {useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  Image,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {StackActions, useNavigation} from '@react-navigation/native';

import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppText from '../components/ui/ AppText';
import appCurrencyFormatter from './../utility/appCurrencyFormatter';
import {useDispatch, useSelector} from 'react-redux';
import {clearLoginParam, clearRMData} from '../store/actions/userLoginAction';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const logOut = async (navigation, dispatch) => {
  await AsyncStorage.removeItem('sessionId');
  await AsyncStorage.removeItem('sessionName');
  await AsyncStorage.removeItem('token');
  await AsyncStorage.removeItem('data');
  await AsyncStorage.removeItem('status');
  await AsyncStorage.removeItem('isLogin');
  await AsyncStorage.removeItem('rmData');
  dispatch(clearRMData());

  navigation.dispatch(
    StackActions.push('Login', {
      backRoute: 'AppStackRoute',
      action: 'wishlist_set',
      screen: 'GodrejHome',
    }),
  );
};

const AppLandingViewItemJustLaunch = ({
  title = '',
  projectData = null,
  loginParam,
  onPressWishlist,
}) => {
  const isLoggedIn = useSelector(
    (state) => state.loginInfo.loginResponse.isLogin,
  );

  const dispatch = useDispatch();

  useEffect(() => {
    if (loginParam) {
      if (loginParam === 'wishlist_set') {
        // Do some action
        console.log('Rou par', loginParam);
        console.log('Wishlist Resumed');
        dispatch(clearLoginParam());
      }
    }
  }, []);

  const navigation = useNavigation();

  const goToProjectDetails = (item) => {
    // navigation.navigate('ProjectDetails', {item});
    navigation.dispatch(StackActions.push('ProjectDetails', {item}));
  };

  const addToWishList = (wishlistItem) => {
    if (isLoggedIn) {
      onPressWishlist(wishlistItem);
    } else {
      logOut(navigation, dispatch);
    }
  };

  // console.log('AppLandingViewItemJustLaunch: ', projectData);

  return (
    <View style={styles.viewContainer}>
      <AppText style={styles.viewTitle}>{title}</AppText>

      <TouchableWithoutFeedback onPress={() => goToProjectDetails(projectData)}>
        <View style={[styles.viewItem]}>
          <View style={styles.imgContainer}>
            <Image
              style={styles.viewImg}
              source={{
                uri: projectData.field_app_thumbnail_image,
              }}
            />
          </View>
          <View style={styles.viewTxtContainer}>
            <AppText numberOfLines={1} style={styles.viewTopTxt}>
              <AppText style={styles.priceTxt}>{projectData.title},</AppText>
              &nbsp;{projectData.field_city}
            </AppText>
            <AppText numberOfLines={1} style={styles.viewBottomTxt}>
              ₹
              {appCurrencyFormatter.landingScreenCurrencyFormat(
                projectData.starting_price,
              )}
            </AppText>
          </View>

          <View style={styles.wishlistContainer}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => addToWishList(projectData)}>
              <Image
                style={styles.wishlist}
                source={
                  projectData.wishlist_status == '0'
                    ? require('./../assets/images/thumb-line.png')
                    : require('./../assets/images/thumb-fill-w.png')
                }
              />
            </TouchableOpacity>
          </View>

          {/* <View style={styles.greenContainer}>
            <Image
              style={styles.greenCertificate}
              source={require('./../assets/images/procertificate.png')}
            />
          </View> */}
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  viewContainer: {
    paddingHorizontal: 18,
    paddingVertical: 10,
    marginBottom: 15,
    justifyContent: 'center',
  },
  viewTitle: {
    width: '100%',
    fontFamily: appFonts.RobotoBold,
    fontSize: appFonts.largeBold,
    color: colors.jaguar,
    textTransform: 'uppercase',
    marginBottom: 15,
    marginTop: 10,
  },
  viewItem: {
    width: '100%',
    height: windowHeight / 1.65,
  },
  imgContainer: {backgroundColor: '#e1e4e8', width: '100%', height: '85%'},
  viewImg: {width: '100%', height: '100%'},
  viewTxtContainer: {
    width: '100%',
    marginTop: 5,
    justifyContent: 'center',
    paddingTop: 10,
  },
  viewTopTxt: {
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.largeBold,
  },
  viewBottomTxt: {
    fontFamily: appFonts.SourceSansProRegular,
    fontSize: appFonts.largeBold,
    marginTop: 2,
  },
  priceTxt: {
    fontFamily: appFonts.SourceSansProBold,
    fontSize: appFonts.largeBold,
  },
  wishlistContainer: {
    height: 34,
    width: 34,
    position: 'absolute',
    top: 5,
    bottom: 0,
    right: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255,255,255,0.1)',
    borderRadius: 17,
  },
  wishlist: {height: 22, width: 22},
  greenContainer: {
    height: 52,
    width: 52,
    position: 'absolute',
    top: 5,
    bottom: 0,
    left: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  greenCertificate: {height: 50, width: 50},
});

export default AppLandingViewItemJustLaunch;
