import React from 'react';
import { Text, View, Dimensions, Image } from 'react-native';
const { width, height } = Dimensions.get('window');
import VideoPlayer from 'react-native-video-player'
const VideoComponent = (props) => {
    return (
        <View style={{width:width,height:height}}>
            {props.videoData && <VideoPlayer
                video={{ uri: 'https://customercare.godrejproperties.com/sites/default/files/2020-09/GGC2.mp4' }}
                style={{height: '100%', width: '100%'}}
                thumbnail={{uri: 'https://i.picsum.photos/id/866/1600/900.jpg'}}
                muted={true}
                disableSeek={true}
                autoplay={true}
                loop={true}
                hideControlsOnStart={true}
                resizeMode="stretch"
                disableControlsAutoHide={true}
                customStyles={{controls: {opacity: 0}}}
                // thumbnail={{ uri: props.videoData.data.field_featured_media_thumb_url }}
            />}
        </View>
    )
}
export default VideoComponent;