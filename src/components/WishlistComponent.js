import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  ImageBackground,
  Image,
} from 'react-native';
import { useNavigation, StackActions } from '@react-navigation/native';
import CURRENCY_FORMAT from '../utility/appCurrencyFormatter';
import appFonts from '../config/appFonts';
import AppText from './ui/ AppText';
import AppTextBold from './ui/AppTextBold';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../config/colors';

const WishlistComponent = (props) => {
  const {
    cities,
    possession,
    proj_img,
    proj_name,
    typologies,
    min_price,
    isSameCity,
    unit_no,
    appurtenant_area_in_sqr_me,
    floor,
    typology_tags,
    unit,
  } = props.item;
  console.log('props.item', props.item);

  const navigation = useNavigation();

  /**
   *
   * @description navigate different screens
   * @returns {void}
   */
  const navigate = () => {
    if (props.isProject) {
      // navigation.navigate('ProjectDetails', {item: props.item});
      navigation.dispatch(
        StackActions.push('ProjectDetails', { item: props.item }),
      );
    } else {
      // navigation.navigate('FloorScreen', {item: props.item});
      navigation.dispatch(StackActions.push('FloorScreen', { item: props.item }));
    }
  };

  /**
   * @description return project detail string in formatted form
   * @returns { Jsx }
   */
  const projectDetail = () => {
    const projectDetail = `${possession ? `${possession}  | ` : ''} ${typologies ? `${typologies} | ` : ''
      }`;
    return (
      <Text>
        {projectDetail}
        <Text>{'  '}</Text>
        {CURRENCY_FORMAT.landingScreenCurrencyFormat(min_price) && (
          <Text>
            <Icon name="rupee" style={{ fontWeight: 'normal' }} size={12}></Icon>
            {'' + CURRENCY_FORMAT.landingScreenCurrencyFormat(min_price)}
          </Text>
        )}
      </Text>
    );
  };
  /**
   * @description return project detail string in formatted form
   * @returns { Jsx }
   */
  const inventoryDetail = () => {
    const projectDetail = `${unit_no ? `Unit ${unit_no}  | ` : ''} ${appurtenant_area_in_sqr_me
      ? `${appurtenant_area_in_sqr_me}${unit}  | `
      : ''
      }${floor ? `${floor}  | ` : ''} ${typology_tags ? `${typology_tags}` : ''}`;
    return <Text> {projectDetail} </Text>;
  };
  /**
   *
   * @description Return the current cityname
   */
  const cityName = () => {
    const newCity = cities.split(',');
    return newCity.length > 1
      ? newCity[1].toUpperCase()
      : newCity[0].toUpperCase();
  };
  return (
    <TouchableOpacity onPress={navigate}>
      <View style={styles.wishlistContainer}>
        {cities && !isSameCity ? <AppTextBold>{cityName()}</AppTextBold> : null}
        {/* {cities && <AppTextBold>{cityName()}</AppTextBold>} */}
        <ImageBackground
          style={styles.imageContainer}
          transition={false}
          source={{ uri: proj_img }}>
          <TouchableOpacity
            onPress={() =>
              props.isProject
                ? props.deleteWishList(props.item)
                : props.deleteInventoryList(props.item)
            }
            style={styles.likeBtn}>
            <Image
              style={{ height: 22, width: 23 }}
              source={require('../assets/images/thumb-fill.png')}></Image>
          </TouchableOpacity>
        </ImageBackground>
        <View style={styles.details}>
          <AppText style={styles.projectName}
            textBreakStrategy="simple"
            ellipsizeMode="tail">
            {proj_name}
            {props.item.sub_location ? `, ` : ''}
          </AppText>
          <AppText style={styles.address}
            textBreakStrategy="simple"
            ellipsizeMode="tail">
            {props.item.sub_location}
          </AppText>
        </View>
        <View>
          <AppText style={styles.projectDetail}
            textBreakStrategy="simple"
            ellipsizeMode="tail">
            {props.isProject ? projectDetail() : inventoryDetail()}
          </AppText>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  wishlistContainer: {
    width: '95%',
    paddingBottom: 20,
    marginBottom: '10%',
  },
  imageContainer: {
    marginTop: '2%',
    width: '100%',
    height: 460,
    flexDirection: 'row-reverse',
  },
  likeBtn: {
    width: '7%',
    height: '7%',
    marginHorizontal: '7%',
    marginVertical: '7%',
  },
  details: {
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: '3%',
  },
  address: {
    color: colors.gray3,
    fontSize: appFonts.largeFontSize,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
  },
  projectName: {
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: appFonts.largeBold,
    fontWeight: 'bold',
  },
  projectDetail: {
    lineHeight: 24,
    // color: 'rgb(138,138,140)' }, 
    fontSize: appFonts.normalFontSize,
    color: colors.gray3,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  }
});

export default WishlistComponent;
