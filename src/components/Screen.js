import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';
import {useNetInfo} from '@react-native-community/netinfo';

import colors from '../config/colors';
import DeviceConfigScreen from '../screens/DeviceConfigScreen';

import {useSelector, useDispatch} from 'react-redux';
import * as deviceNetworkAction from './../store/actions/deviceNetworkAction';

const Screen = ({children, style, onRetry, firstTime = true}) => {
  const netInfo = useNetInfo();

  // const [networkAvailable, setNetworkAvailable] = useState(true);
  const [isShowRetryBtn, setIsShowRetryBtn] = useState(false);

  const networkAvailable = useSelector(
    (state) => state.deviceNetInfo.isNetworkConnected,
  );
  // console.log('isNetworkConnected: netInfo:: ', netInfo);
  // console.log('isNetworkConnected: ', networkAvailable);

  const dispatch = useDispatch();

  useEffect(() => {
    if (
      netInfo.details &&
      netInfo.isConnected !== null
      // &&
      // netInfo.isInternetReachable !== null
    ) {
      // setIsShowRetryBtn(netInfo.isConnected && netInfo.isInternetReachable);
      setIsShowRetryBtn(netInfo.isConnected);

      if (!netInfo.isConnected) {
        // dispatch(
        //   deviceNetworkAction.setNetworkInfo(
        //     netInfo.isConnected && netInfo.isInternetReachable,
        //   ),
        // );
        dispatch(deviceNetworkAction.setNetworkInfo(netInfo.isConnected));
      }
    }
  }, [netInfo]);

  const onHandleRetryBtn = () => {
    dispatch(deviceNetworkAction.setNetworkInfo(true));
    setIsShowRetryBtn(false);

    try {
      if (typeof onRetry === 'function') {
        onRetry();
      }
    } catch (error) {
      console.log('Internet Connection Retry Error: ', error);
    }
  };

  return (
    <SafeAreaView style={[styles.container, style]}>
      {networkAvailable ? (
        <View style={[styles.view, style]}>{children}</View>
      ) : (
        <DeviceConfigScreen
          deviceConfig="internet"
          showRetryBtn={isShowRetryBtn}
          onPress={onHandleRetryBtn}
        />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  view: {
    flex: 1,
    backgroundColor: colors.primary,
  },
});

export default Screen;
