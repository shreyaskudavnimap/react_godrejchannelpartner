import React, { useState, useEffect } from 'react';
import {
	View,
	Text,
	ScrollView,
	StyleSheet,
	Image,
	TouchableOpacity,
	Alert,
	TextInput
} from 'react-native';
import moment from 'moment';
import apiScheduledVisit from '../api/apiScheduledVisit';
import appSnakBar from '../utility/appSnakBar';
import appFonts from '../config/appFonts';
import {
	Menu,
	MenuOptions,
	MenuOption,
	MenuTrigger,
	MenuProvider 
  } from 'react-native-popup-menu';

const getTimeSlot = (activeVisit) => {
	let timeSlot;
	 if ((activeVisit.schedule_type_name =='Meeting with RM' && activeVisit.status === 'Requested') ||
	 (activeVisit.schedule_type_name =='Meeting with RM' && (activeVisit.status === 'Scheduled' || activeVisit.status === 'Rescheduled')) ||
	 (activeVisit.schedule_type_name !='Meeting with RM' && activeVisit.status === 'Requested')
	 ) {
		 timeSlot = activeVisit.time_slot
	 }
	 if ((activeVisit.schedule_type_name !='Meeting with RM' && (activeVisit.status === 'Scheduled' || activeVisit.status === 'Rescheduled')) && (activeVisit.rm_from_time_slot && activeVisit.rm_to_time_slot && activeVisit.rm_from_time_slot != '' && activeVisit.rm_to_time_slot != '')) {
		 timeSlot = `${activeVisit.rm_from_time_slot} - ${activeVisit.rm_to_time_slot}`
	 }

	 return timeSlot
} 

const formatVisitDate = (date) => {
	return moment(date).format("DD-MM-YYYY").toString()
}

const getTypeTextColor = (item, type) => {
	let color 
	if (type === 'COMPLETED') {
		color = '#c3c3c3'
	} else {
		color = type === 'ACTIVE' && item.status === 'Confirmed' ? '#b4b400' : '#F5CE2E'
	}
	return color
}

const ScheduledVisit = ({userId, callGetScheduleVisit, setApiIsLoading, handleReschedulePress}) => {
	const [isCompletedOpen, setCompletedOpen] = useState(false)
	const [showComments, setShowComments] = useState(false)
	const [showCommentInput, setShowCommentInput] = useState(false)
	const [commentText, setCommentText] = useState('')
	const [selectedItem, setSelectedItem] = useState('')
	const [activeVisits, setActiveVisits] = useState([]);
	const [closedVisits, setClosedVisits] = useState([]);

	useEffect(() => {
		getScheduledVisitList();
	}, [callGetScheduleVisit]);

	const getScheduledVisitList = async () => {
		setApiIsLoading(true)
		try {
		  const payload = {
			user_id: userId,
		  };
		  const result = await apiScheduledVisit.apiGetScheduledVisitList(payload);
		  const {scheduled_visit, previous_visit} = result.data;
		  setActiveVisits(scheduled_visit);
		  setClosedVisits(previous_visit);
		  setApiIsLoading(false)
		} catch (err) {
		  setApiIsLoading(false)
		  console.log('SCHEDULED VISIT ERROR: ', error);
		}
	  };

	const renderOptions = (options, item) => {
		const filteredOptions = options.filter(o => !item.comment && o.title === 'View Comments' ? false : true )
		if (item.status === 'Cancelled' || item.status === 'Rejected') {
			return (
				<Image
					style={styles.icons}
					source={require('../assets/images/ellipsis-icon.png')}
				/>
			)
		}
		return (
			<Menu onSelect={value => {
				options.find(o => o.value === value).onPress(item)
			}}>
				<MenuTrigger>
					<Image
						style={styles.icons}
						source={require('../assets/images/ellipsis-icon-black.png')}
					/>
				</MenuTrigger>
				<MenuOptions>
					{filteredOptions.map((option, index) => (
						<MenuOption
							key={index}
							value={option.value}
							text={option.title}
						/>
					))}
				</MenuOptions>
			</Menu>
		)
	}

	const onReschedulePress = (item) => {
		handleReschedulePress(item)
	}

	const onCancelPress = (item) => {
		renderCancelAlert(item.visit_id)
	}

	const onViewCommentsPress = (item) => {
		setSelectedItem(item.visit_id)
		setShowComments(true)
		setShowCommentInput(false)
	}

	const onAddCommentsPress = (item) => {
		setSelectedItem(item.visit_id)
		setShowComments(false)
		setShowCommentInput(true)
	}

	const onConfirmCancel = async (visitId) => {
		const payload = {
			user_id: userId,
			visit_id: visitId,
		};

		const result = await apiScheduledVisit.apiDeleteScheduleVisit(payload);

		if (result.status === 200) {
			getScheduledVisitList()
		}
	}

	const onSaveComment = async(item) => {
		const commentTxt = commentText.trim()

		if(commentTxt) {
			setApiIsLoading(true)
			const payload ={
				user_id: userId,
        visit_id: item.visit_id,
        comment: commentTxt,
        schedule_type: item.schedule_type_id,
			}
			const result = await apiScheduledVisit.apiEditScheduleVisit(payload)
			if (result.status === 200) {
				setCommentText('')
				getScheduledVisitList()
				appSnakBar.onShowSnakBar('Comment added successfully', 'LONG');
			}
			setApiIsLoading(false)
		}
	}

	const options = [
		{title: 'Reschedule', value: 'RESCHEDULE', onPress: onReschedulePress},
		{title: 'Cancel', value: 'CANCEL', onPress: onCancelPress},
		{title: 'View Comments', value: 'VIEW_COMMENTS', onPress: onViewCommentsPress},
		{title: 'Add Comments', value: 'ADD_COMMENTS', onPress: onAddCommentsPress},
	]

	const renderCancelAlert = (visitId) => {
		Alert.alert(
			"Confirm Cancellation",
			"Do you want to cancel this?",
			[
				{text: 'No', onPress: () => {}},
				{text: 'Yes', onPress: () => onConfirmCancel(visitId)}
			]
		)
	}

	const renderList = (item, type) => {
		const timeSlot = getTimeSlot(item)

		const { comment } = item

		return(
			<>
				<View style={styles.cardContainer}>
					<View style={{ padding: 5 }}>

						<View style={styles.titleRow}>
							<Text style={styles.propertyNameText}>{item.project_name}</Text> 
							{type === 'ACTIVE' ? renderOptions(options, item) : <View />}
						</View>
						<Text
							style={{...styles.typeText, color: getTypeTextColor(item, type)}}
						>
							{item.schedule_type_name} {item.status}
						</Text>
						<View style={styles.detailsRow}>
							<View style={styles.iconWrap}>
								<Image
									style={styles.icons}
									source={
											require('./../assets/images/date-icon.png')
										}
									/>
								<Text>{formatVisitDate(item.date_of_visit)}</Text>
							</View>
							<View style={styles.iconWrap}>
								{timeSlot ?
									<Image
										style={styles.icons}
										source={
												require('./../assets/images/time-icon.png')
											}
									/> : <View />
								}
								<Text>{timeSlot}</Text>
							</View>
							<View style={styles.iconWrap}>
								<Image
									style={styles.icons}
									source={
											require('./../assets/images/people-icon.png')
										}
								/>
								<Text>{`${item.number_of_persons} ${item.number_of_persons > 1 ? 'persons' : 'person'}`}</Text>
							</View>
						</View>
					</View>

					{selectedItem === item.visit_id && showComments ?
						<View style={styles.commentWrap}>
								<View style={styles.closeCommentWrap}>
									<TouchableOpacity onPress={() => {
										setShowComments(false)
									}}>
										<Text>X</Text>
									</TouchableOpacity>
								</View>
								{comment && comment.length > 0 ?
							<View style={{height: comment.length > 1 ? 90 : 40}}>
							<ScrollView showsVerticalScrollIndicator={false} nestedScrollEnabled={true}>
								{comment.map((c, index) => (
									<Text key={index} style={{ ...styles.commentText, paddingTop: index === 0 ? 0 : 15 }}>{c.details}</Text>
								))}
							</ScrollView>
						</View>: <View/>	
							}
						</View> : <View />}

					{selectedItem === item.visit_id && showCommentInput ? 
						<View style={styles.commentWrap}>
							<View style={{ flexDirection: 'row' }}>
								<TextInput
									style={styles.commentInput}
									placeholder="Add your comment here..."
									value={commentText}
									onChangeText={text => setCommentText(text)}
								/>
								<TouchableOpacity onPress={() => { onSaveComment(item)}}>
									<Image
										style={{...styles.icons, ...styles.sendCommentIcon}}
										source={
											require('./../assets/images/send-icon.png')
										}
									/>
								</TouchableOpacity>
							</View>
						</View>
						: <View />
					}
				</View>
			</>
		)
	}

  return (
	<MenuProvider>
      <ScrollView showsVerticalScrollIndicator={false}>
				<View>
					<Text style={styles.typeHeadTxt}>Upcoming</Text>
					{activeVisits && activeVisits.length > 0 ?activeVisits.map((item, index) => (
						<View key={index}>
							{renderList(item, 'ACTIVE')}
						</View>
					)) : <Text style={styles.noDataTxt}>No Record Found</Text>}
				</View>

				<View>
					<TouchableOpacity
						style={{justifyContent: 'center'}}
						onPress={() => {
							setCompletedOpen(!isCompletedOpen)
						}}>
						<View style={styles.completedDropdownWrap}>
							<Text style={styles.typeHeadTxt}>Completed</Text>
							<Image
								style={{...styles.icons, marginTop: 13, marginLeft: 15}}
								source={
										isCompletedOpen ? 
										require('./../assets/images/up-icon-b.png') :
										require('./../assets/images/down-icon-b.png')
									}
							/>
						</View>
          </TouchableOpacity>
					{isCompletedOpen ? 
						closedVisits && closedVisits.length > 0 ? closedVisits.map((item, index) => (
							<View key={index}>
								{renderList(item, 'COMPLETED')}
							</View>
						)) : <Text style={styles.noDataTxt}>No Record Found</Text>
						: <View />
					}
				</View>
      </ScrollView>
	  </MenuProvider>
  );
};

export default ScheduledVisit;

const styles = StyleSheet.create({
  container: {
	},
	cardContainer: {
		padding: 5,
		marginTop: 10,
		marginBottom: 10,
		borderWidth: 1,
		borderColor: '#D3D1CD',
	},
	propertyNameText: {
		fontSize:appFonts.xxxlargeFontSize,
		margin: 0
	},
	typeText: {
		paddingTop: 5,
		paddingBottom: 5
	},
	timeText: {},
	noOfVisitorsText: {},
	detailsRow: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingTop: 10
	},
	titleRow: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	icons: {
		height: 17,
		width: 17,
		marginRight: 5,
		marginLeft: 5
	},
	iconWrap: {
		flexDirection: 'row'
	},
	noDataTxt: {
		padding: 10,
		fontSize: appFonts.largeFontSize
	},
	completedDropdownWrap: {
		flexDirection: 'row'
	},
	typeHeadTxt: {
		fontSize: appFonts.largeFontSize,
		marginTop: 10
	},
	commentWrap: {
		marginTop: 10,
		backgroundColor: '#F7F5EC',
		borderWidth: 1,
		borderColor: '#D3D1CD'
	},
	commentInput: {
		width: '90%'
	},
	sendCommentIcon: {
		marginTop: 15,
		width: 18,
	},
	commentText: {
		fontSize: appFonts.largeFontSize,
		paddingTop: 15,
		paddingBottom: 15,
		marginLeft: 10,
		marginRight: 10,
		borderBottomWidth: 1,
		borderColor: '#D3D1CD'
	},
	closeCommentWrap: {
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: 10
	}
});
