import React from 'react';
import {StatusBar,Platform,View} from 'react-native';


const StatusBarComponent = ()=>{
    return(
        <View
                style={{
                    backgroundColor: '#fff',
                    height: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,
                }}>
                <StatusBar
                    translucent
                    backgroundColor="#fff"
                    barStyle="dark-content"
                />
            </View>
    )
}
export default StatusBarComponent;