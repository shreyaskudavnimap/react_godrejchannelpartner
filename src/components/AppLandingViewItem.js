import React, {useEffect} from 'react';
import {View, StyleSheet, Dimensions, FlatList} from 'react-native';
import {StackActions, useNavigation} from '@react-navigation/native';

import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppText from '../components/ui/ AppText';
import LandingPageViewItem from '../components/ui/LandingPageViewItem';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {clearLoginParam, clearRMData} from '../store/actions/userLoginAction';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const logOut = async (navigation, dispatch, wishlistItem, onPressWishlist) => {
  await AsyncStorage.removeItem('sessionId');
  await AsyncStorage.removeItem('sessionName');
  await AsyncStorage.removeItem('token');
  await AsyncStorage.removeItem('data');
  await AsyncStorage.removeItem('status');
  await AsyncStorage.removeItem('isLogin');
  await AsyncStorage.removeItem('rmData');
  dispatch(clearRMData());

  navigation.dispatch(
    StackActions.push('Login', {
      backRoute: 'AppStackRoute',
      action: 'wishlist_set',
      screen: 'GodrejHome',
      // params: {
      //   screen: 'GodrejHome',
      //   actionToPerform: () => onPressWishlist(wishlistItem)
      // }
    }),
  );
};

const AppLandingViewItem = ({
  title = '',
  projectData = null,
  isCityWise = false,
  loginParam,
  onPressWishlist,
}) => {
  const isLoggedIn = useSelector(
    (state) => state.loginInfo.loginResponse.isLogin,
  );

  const dispatch = useDispatch();

  useEffect(() => {
    if (loginParam) {
      if (loginParam === 'wishlist_set') {
        // Do some action
        console.log('Rou par', loginParam);
        console.log('Wishlist Resumed');
        dispatch(clearLoginParam());
      }
    }
  }, []);

  const navigation = useNavigation();

  const goToProjectDetails = (item) => {
    // navigation.navigate('ProjectDetails', {item});
    navigation.dispatch(StackActions.push('ProjectDetails', {item}));
  };

  const addToWishList = (isLoggedIn, navigation, dispatch, wishlistItem) => {
    if (isLoggedIn) {
      onPressWishlist(wishlistItem);
    } else {
      logOut(navigation, dispatch, wishlistItem, onPressWishlist);
    }
  };

  if (!projectData || !projectData.length > 0) {
    return null;
  }

  return (
    <View style={styles.viewContainer}>
      <AppText style={styles.viewTitle}>{title}</AppText>

      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={projectData}
        renderItem={({item, index}) => (
          <LandingPageViewItem
            itemIndex={index}
            totalItem={projectData.length}
            item={item}
            isLoggedIn={isLoggedIn}
            navigation={navigation}
            dispatch={dispatch}
            addToWishList={addToWishList}
            isCityWise={isCityWise}
            onPress={(item) => goToProjectDetails(item)}
          />
        )}
        keyExtractor={(item) => item.proj_id}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  viewContainer: {
    paddingHorizontal: 18,
    paddingVertical: 10,
    marginBottom: 15,
    justifyContent: 'center',
  },
  viewTitle: {
    width: '100%',
    fontFamily: appFonts.RobotoBold,
    fontSize: appFonts.largeBold,
    color: colors.jaguar,
    textTransform: 'uppercase',
    marginBottom: 15,
    marginTop: 10,
  },
});

export default AppLandingViewItem;
