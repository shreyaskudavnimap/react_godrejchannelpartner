import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';

import AppText from '../components/ui/ AppText';

import appFonts from '../config/appFonts';
import colors from '../config/colors';
import appCurrencyFormatter from '../utility/appCurrencyFormatter';

const windowWidth = Dimensions.get('window').width;

const EOIPaymentDetailsComponent = ({
  onPress,
  refundStaus,
  tokenTypeData: tokenType,
  onChangeTokenType,
  isShowTokenSelection,
  isPlotProject,
  selectedPreference,
  tokenAmount,
  startingPrice,
}) => {
  // console.log('EOIPaymentDetailsComponent: ', selectedPreference)

  return (
    <View style={styles.viewContainer}>
      <View style={{paddingHorizontal: 25}}>
        <TouchableWithoutFeedback onPress={onPress}>
          <View style={styles.titleLine}>
            <AppText
              style={[
                styles.title,
                {fontFamily: appFonts.SourceSansProSemiBold},
              ]}>
              Payment Details
            </AppText>
            <Image
              source={require('./../assets/images/up-icon-b.png')}
              style={styles.icon}
            />
          </View>
        </TouchableWithoutFeedback>

        {selectedPreference &&
        selectedPreference['typologyName'] &&
        selectedPreference['typologyName'] != '' ? (
          <View style={styles.subContainer}>
            <AppText style={styles.subTitle}>
              {isPlotProject ? 'Plot Type' : 'Typology'} Selected
            </AppText>
            <AppText style={styles.value}>
              {selectedPreference['typologyName']}
            </AppText>
          </View>
        ) : null}

        {isShowTokenSelection ? (
          <View style={{paddingBottom: 10}}>
            <AppText style={styles.subTitle}>Token Type</AppText>

            <View style={styles.tokenContainer}>
              {tokenType &&
                tokenType.length > 0 &&
                tokenType.map((data) => (
                  <View key={data.key} style={styles.buttonContainer}>
                    <View>
                      <TouchableOpacity
                        activeOpacity={0.8}
                        style={styles.circle}
                        onPress={() => onChangeTokenType(data.title)}>
                        {data.selected && <View style={styles.checkedCircle} />}
                      </TouchableOpacity>
                    </View>

                    <TouchableOpacity
                      activeOpacity={0.8}
                      onPress={() => onChangeTokenType(data.title)}>
                      <AppText style={{fontSize: appFonts.largeFontSize}}>{data.title}</AppText>
                    </TouchableOpacity>
                  </View>
                ))}
            </View>
          </View>
        ) : null}

        <View style={styles.subContainer}>
          <AppText style={styles.subTitle}>Starting Price</AppText>
          {startingPrice && startingPrice != '' ? (
            <AppText style={styles.value}>
              {'\u20B9'}&nbsp;
              {appCurrencyFormatter.getPriceInWord(startingPrice)}
            </AppText>
          ) : (
            <AppText style={styles.value}>
              {'\u20B9'}&nbsp;
              {'0'}
            </AppText>
          )}
        </View>

        <View style={[styles.subContainer, {marginBottom: 5}]}>
          <AppText style={styles.subTitle}>Token Amount</AppText>
          {tokenAmount && tokenAmount != '' ? (
            <AppText style={styles.value}>
              {'\u20B9'}&nbsp;{appCurrencyFormatter.getPriceInWord(tokenAmount)}
            </AppText>
          ) : (
            <AppText style={styles.value}>
              {'\u20B9'}&nbsp;{'0'}
            </AppText>
          )}
        </View>

        {refundStaus && refundStaus.toLowerCase() == 'both' ? null : (
          <>
            {refundStaus ? (
              <AppText style={styles.valueToken}>{refundStaus}</AppText>
            ) : null}
          </>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  viewContainer: {
    width: windowWidth,
    paddingBottom: 20,
    marginBottom: 25,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  titleLine: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  icon: {
    width: 16,
    height: 16,
  },
  subContainer: {
    borderBottomWidth: 1,
    borderBottomColor: colors.veryLightGray,
    paddingBottom: 10,
    marginBottom: 20,
  },
  subTitle: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
    color: colors.jaguar,
    marginBottom: 10,
  },
  value: {fontSize: appFonts.xxlargeFontSize, color: colors.gray6},
  valueToken: {
    fontSize: appFonts.normalFontSize,
    color: colors.jaguar,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  tokenContainer: {flexDirection: 'row', paddingVertical: 10},
  buttonContainer: {
    flexDirection: 'row',
    marginRight: 25,
  },
  circle: {
    marginRight: 10,
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: colors.secondaryLight,
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkedCircle: {
    width: 12,
    height: 12,
    borderRadius: 7,
    backgroundColor: colors.secondaryLight,
  },
});

export default EOIPaymentDetailsComponent;
