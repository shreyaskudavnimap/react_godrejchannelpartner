import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, TextInput, Text } from 'react-native';
import colors from '../config/colors';
import appFonts from '../config/appFonts';


const ShowPassword = (props) => {
     const [showPassword, setPassword] = useState(true);
    return (
        <View>

            <View style={styles.container}>
                <View style={styles.textBoxContainer}>
                    <TextInput secureTextEntry={showPassword} style={{color:props.colorText, fontSize: appFonts.normalFontSize,
            alignSelf: 'stretch',
            height: 48,
            marginTop: 50,
            borderBottomWidth: 2,
            paddingVertical: 0,
            borderBottomColor: colors.borderGrey,
            width: '100%',}} 


            placeholder={props.title}
                        placeholderTextColor={props.colorText}
                        onChangeText={props.onChangeText}
                        onTouchStart={props.onTouchStart}
                        showSoftInputOnFocus={props.showSoftInputOnFocus}
                        value={props.value} />
                    <TouchableOpacity activeOpacity={0.8} style={styles.touachableButton} onPress={() => {
                        //console.log(showPassword)
                        {if(showPassword==false ){ setPassword(true);}else{setPassword(false);}}

                    }}>
                       {props.colorText=="white" && <Image source={(showPassword) ? require('./../assets/images/hide-icon.png') : require('./../assets/images/show-icon.png')} style={styles.buttonImage} />}
                      {props.colorText=="black" && <Image
                      source={(showPassword) ? require('./../assets/images/hide-icon-b.png') : require('./../assets/images/show-icon-b.png')}
                      style={styles.buttonImage} />}
                    </TouchableOpacity>
                    {/* <Text style={{color:'#fc3131', fontSize:13}}>{props.error}</Text> */}
                    {(props.error) ? <Text style={{color:'#fc3131', fontSize:appFonts.mediumFontSize}}>{props.error}</Text> : null}
                </View>
                
            </View>
            
        </View>
    );
};


const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            //  justifyContent: "center",
            // alignItems: "center",

        },
        headerText: {
            fontSize: appFonts.xxxlargeFontSize,
            textAlign: "center",
            margin: 10,
            color: 'black',
            fontWeight: "bold"
        },
        textBoxContainer: {
            
            justifyContent: 'center',
        },
        textBox: {
            fontSize: appFonts.normalFontSize,
            alignSelf: 'stretch',
            height: 48,
            marginTop: 50,
            borderBottomWidth: 2,
            paddingVertical: 0,
            borderBottomColor: colors.borderGrey,
            width: 355,


        },
        touachableButton: {
            position: 'absolute',
            right: 3,
            top: 8,
            height: 40,
            width: 35,
            padding: 5,
        },
        buttonImage: {
            resizeMode: 'contain',
            height: '100%',
            width: '100%',
        }

    });
export default ShowPassword;
