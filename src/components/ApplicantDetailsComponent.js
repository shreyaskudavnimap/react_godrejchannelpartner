import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';

import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';

import appFonts from '../config/appFonts';
import colors from '../config/colors';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const ApplicantDetailsComponent = ({
  onPress,
  applicantDetails,
  permanentAddress,
  communicationAddress,
  downloadApplicantDocument,
}) => {
  return (
    <View style={styles.viewContainer}>
      <View style={styles.subContainer}>
        <TouchableOpacity onPress={onPress}>
          <View style={styles.titleLine}>
            <AppText
              style={[
                styles.title,
                {fontFamily: appFonts.SourceSansProSemiBold},
              ]}>
              Primary Applicant Details
            </AppText>
            <Image
              source={require('./../assets/images/up-icon-b.png')}
              style={styles.icon}
            />
          </View>
        </TouchableOpacity>
        <AppText style={styles.info}>
          The information provided below will be used to process your
          application booking form. Once confirmed, these details will be used
          to create the purchase agreement for the given unit. Incase of any
          changes required, kindly get in touch with us.
        </AppText>
        <AppTextBold style={styles.infoTitle}>PERSONAL INFO</AppTextBold>

        {applicantDetails &&
          applicantDetails.map((data) => (
            <View key={data.id}>
              {data.detail != '' ? (
                <>
                  <View style={styles.detailsLine}>
                    <View style={styles.detailView}>
                      <AppText style={styles.detailTitle}>
                        {data.title === 'Uploaded' ? data.docType : data.title}
                      </AppText>
                    </View>
                    <View style={styles.detailView}>
                      <AppText style={styles.value}>{data.detail}</AppText>
                    </View>
                  </View>
                  {data.title === 'Uploaded' &&
                    data.file_format &&
                    data.file_name &&
                    data.file_link && (
                      <TouchableWithoutFeedback
                        onPress={() => {
                          downloadApplicantDocument(
                            data.file_link,
                            data.file_name,
                          );
                        }}>
                        <View style={styles.docImageContainer}>
                          {data.file_format === 'image' ? (
                            <Image
                              resizeMode="contain"
                              style={styles.docImage}
                              source={{uri: data.file_link}}
                            />
                          ) : (
                            <Image
                            resizeMode="contain"
                              style={styles.docImagePdf}
                              source={require('./../assets/images/pdf_icon.png')}
                            />
                          )}
                        </View>
                      </TouchableWithoutFeedback>
                    )}
                </>
              ) : null}
            </View>
          ))}

        <View style={{paddingVertical: 20}}>
          <AppTextBold style={{fontSize: appFonts.xlargeFontSize}}>PERMANENT ADDRESS</AppTextBold>
          {permanentAddress &&
            permanentAddress.map((data) => (
              <View key={data.id}>
                {data.detail != '' ? (
                  <View style={styles.detailsLine}>
                    <View style={styles.detailView}>
                      <AppText style={styles.detailTitle}>{data.title}</AppText>
                    </View>
                    <View style={styles.detailView}>
                      <AppText style={styles.value}>{data.detail}</AppText>
                    </View>
                  </View>
                ) : null}
              </View>
            ))}
        </View>

        <View style={{paddingBottom: 20}}>
          <AppTextBold style={{fontSize: appFonts.xlargeFontSize}}>
            COMMUNICATION ADDRESS
          </AppTextBold>
          {communicationAddress.map((data) => (
            <View key={data.id}>
              {data.detail != '' ? (
                <View style={styles.detailsLine}>
                  <View style={styles.detailView}>
                    <AppText style={styles.detailTitle}>{data.title}</AppText>
                  </View>
                  <View style={styles.detailView}>
                    <AppText style={styles.value}>{data.detail}</AppText>
                  </View>
                </View>
              ) : null}
            </View>
          ))}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  viewContainer: {
    width: windowWidth,
    marginBottom: 20,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  subContainer: {paddingHorizontal: 25},
  titleLine: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  info: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
    marginBottom: 15,
    lineHeight: 20,
    textAlign: 'justify'
  },
  infoTitle: {
    fontSize: appFonts.xlargeFontSize,
    paddingVertical: 10,
    lineHeight: 30,
  },
  value: {
    fontSize: appFonts.largeBold,
    color: colors.expandText,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  icon: {
    width: 16,
    height: 16,
  },
  attachIcon: {
    width: 20,
    height: 20,
  },
  detailsLine: {
    flexDirection: 'row',
    // alignItems: 'center',
    paddingVertical: 12,
  },
  detailView: {flex: 0.5},
  detailTitle: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  docImageContainer: {
    marginVertical: 10,
    alignItems: 'center'
  },
  docImagePdf: {height: windowHeight / 6},
  docImage: {width: '100%', height: windowHeight / 4},
});

export default ApplicantDetailsComponent;
