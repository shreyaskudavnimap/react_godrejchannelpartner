import React from 'react';
import {
  View,
  Image,
  ImageBackground,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';

import AppText from '../components/ui/ AppText';

import appFonts from '../config/appFonts';
import colors from '../config/colors';

const applicantDetails = [
  {
    id: '1',
    title: 'Name 1',
    detail: '1st Applicant',
  },
  {
    id: '2',
    title: 'Name 2',
    detail: '2nd Applicant',
  },
];

const propertyDetails = [
  {
    id: '1',
    title: '28,96,985.00',
    detail: 'Total Value',
  },
  {
    id: '2',
    title: 'NA',
    detail: 'Carpet Area',
  },
  {
    id: '3',
    title: 'NA',
    detail: 'Exclusive Area',
  },
  {
    id: '4',
    title: '4.50 sq. mt',
    detail: 'Total Area',
  },
  {
    id: '5',
    title: 'July-2021',
    detail: 'Planned Handover Date',
  },
];

const OverviewComponent = (props) => {
  return (
    <View style={styles.container}>
      <ImageBackground
        source={{
          uri:
            'https://customercare.godrejproperties.com/sites/default/files/2020-04/GreenGlades%2CAhd_ImageGallery_3.JPG',
        }}
        style={{height: 200}}>
        <AppText style={styles.location}>Mumbai</AppText>
      </ImageBackground>

      <View style={styles.buttonContainer}>
        <View style={styles.button}>
          <AppText style={styles.buttonText}>P517000244042</AppText>
          <AppText style={[styles.buttonText, {color: colors.gray}]}>
            RERA Registration Number
          </AppText>
        </View>
      </View>

      <View style={styles.buttonContainer}>
        {applicantDetails.map((data) => (
          <View
            key={data.id}
            style={[
              styles.button,
              {
                borderTopWidth: data.id === '1' ? 0 : 1,
                borderTopColor: colors.veryLightGray,
              },
            ]}>
            <AppText style={styles.buttonText}>{data.title}</AppText>
            <AppText style={[styles.buttonText, {color: colors.gray}]}>
              {data.detail}
            </AppText>
          </View>
        ))}
      </View>

      <View style={styles.buttonContainer}>
        {propertyDetails.map((data) => (
          <View
            key={data.id}
            style={[
              styles.button,
              {
                borderTopWidth: data.id === '1' ? 0 : 1,
                borderTopColor: colors.veryLightGray,
              },
            ]}>
            <AppText style={styles.buttonText}>{data.title}</AppText>
            <AppText style={[styles.buttonText, {color: colors.gray}]}>
              {data.detail}
            </AppText>
          </View>
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: '5%',
  },
  buttonContainer: {
    marginTop: 20,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  button: {
    height: 60,
    justifyContent: 'center',
    marginHorizontal: 20,
  },
  buttonText: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
    color: colors.secondary,
  },
  location: {
    padding: 10,
    fontSize: appFonts.xlargeFontSize,
    color: colors.whiteSmoke,
  },
});

export default OverviewComponent;
