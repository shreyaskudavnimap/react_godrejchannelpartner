import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';

import Screen from '../components/Screen';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';

import appFonts from '../config/appFonts';
import colors from '../config/colors';
import appCurrencyFormatter from '../utility/appCurrencyFormatter';
import AppNotePink from './ui/AppNotePink';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const PaymentPlanComponent = ({
  onPress,
  data,
  onPressInfo,
  onSelectPaymentPlan,
  onPressPaymentPlanDetails,
}) => {
  const [planData, setPlanData] = useState(data);

  const onSelectPlan = (plan) => {
    if (plan.plan_selected === '0') {
      const pData = {...planData};
      pData.data.curl_response.payment_plans.map((data, index) => {
        if (data.payment_plan_id === plan.payment_plan_id) {
          data.plan_selected = '1';
        } else {
          data.plan_selected = '0';
        }
      });
      setPlanData(pData);
      onSelectPaymentPlan(plan);
    }
  };

  return (
    <Screen>
      <View style={styles.container}>
        <View style={{paddingHorizontal: 25}}>
          <TouchableWithoutFeedback onPress={onPress}>
            <View style={styles.titleLine}>
              <AppText
                style={[
                  styles.title,
                  {fontFamily: appFonts.SourceSansProSemiBold},
                ]}>
                Payment Plan
              </AppText>
              <Image
                source={require('./../assets/images/up-icon-b.png')}
                style={styles.icon}
              />
            </View>
          </TouchableWithoutFeedback>

          <AppTextBold style={styles.pageTitle}>
            CHOOSE A PAYMENT PLAN
          </AppTextBold>

          {planData.data.curl_response.tower_completion_label &&
          planData.data.curl_response.tower_completion_label !== '' &&
          planData.data.curl_response.tower_completion_percentege &&
          planData.data.curl_response.tower_completion_percentege !== '' ? (
            <AppNotePink
              label={`${planData.data.curl_response.tower_completion_label} ${planData.data.curl_response.tower_completion_percentege}`}
            />
          ) : null}

          <AppText style={styles.planMsg}>
            You can change the payment plan before confirming the booking.
          </AppText>

          {planData.data.curl_response.payment_plans &&
            planData.data.curl_response.payment_plans.length > 0 &&
            planData.data.curl_response.payment_plans.map((data, index) => (
              <TouchableWithoutFeedback
                onPress={() => onSelectPlan(data)}
                key={index}>
                <View
                  style={[
                    styles.viewContainer,
                    {
                      backgroundColor:
                        data.plan_selected === '1'
                          ? colors.whiteSmoke
                          : colors.primary,
                      borderColor:
                        data.plan_selected === '1'
                          ? colors.darkGray
                          : colors.lightGray,
                    },
                  ]}>
                  <View style={styles.pmtTitleContainer}>
                    <AppText style={styles.pmtTitle}>
                      {data.cip_pp_name}
                    </AppText>

                    <TouchableOpacity activeOpacity={0.8} onPress={onPressInfo}>
                      <View style={{paddingVertical: 8, paddingLeft: 8}}>
                      <Image
                        source={require('./../assets/images/information-icon-big.png')}
                        style={styles.info}
                      />
                      </View>
                    </TouchableOpacity>
                  </View>

                  <View style={styles.priceContainer}>
                    <AppText style={styles.priceLabel}>
                      Total amount to be paid
                    </AppText>
                    <AppText style={styles.priceValue}>
                      {'\u20B9'}&nbsp;
                      {appCurrencyFormatter.getIndianCurrencyFormat(
                        data.payment_plan_total,
                      )}
                    </AppText>
                  </View>

                  <View style={styles.bottomContainer}>
                    <TouchableOpacity
                      onPress={() => {
                        onPressPaymentPlanDetails(data);
                      }}>
                      <AppText style={styles.txtViewPlanDetals}>
                        View Plan Details
                      </AppText>
                    </TouchableOpacity>

                    {data.plan_recommended === '1' ? (
                      <View style={styles.recmContainer}>
                        <AppText style={styles.recmTxt}>Recommended</AppText>
                      </View>
                    ) : null}
                  </View>
                </View>
              </TouchableWithoutFeedback>
            ))}
        </View>
      </View>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    width: windowWidth,
    paddingBottom: 20,
    marginBottom: 25,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  pageTitle: {
    fontSize: appFonts.xxlargeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    textTransform: 'uppercase',
  },
  viewContainer: {
    padding: 20,
    marginVertical: 10,
    borderWidth: 1,
    borderRadius: 1,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
  },
  titleLine: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  info: {
    width: 18,
    height: 18,
  },
  icon: {
    width: 16,
    height: 16,
  },
  planMsg: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
    marginBottom: 15,
  },
  pmtTitleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15,
  },
  pmtTitle: {
    flex: 1,
    marginRight: 10,
    color: colors.jaguar,
    fontFamily: appFonts.SourceSansProSemiBold,
    fontSize: appFonts.largeBoldx,
  },
  priceContainer: {
    marginBottom: 16,
  },
  priceLabel: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.secondary,
    marginBottom: 3,
  },
  priceValue: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.jaguar,
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  txtViewPlanDetals: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.secondary,
    textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
    textDecorationColor: '#000',
  },
  recmContainer: {
    backgroundColor: colors.veryLightGray,
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderRadius: 5,
  },
  recmTxt: {
    fontSize: appFonts.normalFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
    color: colors.jaguar,
  },
});

export default PaymentPlanComponent;
