import React from 'react';
import {View, StyleSheet} from 'react-native';
import AppText from './ AppText';
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';

const AppNotePink = ({showNote = false, label, labelAmount}) => {
  return (
    <View style={styles.container}>
      <AppText style={styles.txt}>
        {showNote && <AppText style={styles.txtNote}>Note:&nbsp;</AppText>}
        {label}
        <AppText style={styles.txtNote}>
          &nbsp;{labelAmount ? labelAmount : ''}
        </AppText>
      </AppText>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 12,
    paddingVertical: 15,
    backgroundColor: colors.noteBg,
    marginVertical: 15,
    borderRadius: 5,
  },
  txtNote: {
    color: colors.jaguar,
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
    lineHeight: 24,
  },
  txt: {color: colors.jaguar, lineHeight: 22, fontSize: appFonts.largeFontSize},
});

export default AppNotePink;
