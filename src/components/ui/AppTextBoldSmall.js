import React from 'react';
import {StyleSheet, Text} from 'react-native';
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
const AppTextBoldSmall = ({style, children, ...otherProps}) => {
  return (
    <Text style={[styles.text, style]} {...otherProps}>
      {children}
    </Text>
  );
};

const styles = StyleSheet.create({
  text: {
    color: colors.secondary,
    fontSize: appFonts.normalFontSize,
    fontWeight:'bold',
    marginTop:'1%',
    marginBottom:"1%"
  },
});

export default AppTextBoldSmall;
