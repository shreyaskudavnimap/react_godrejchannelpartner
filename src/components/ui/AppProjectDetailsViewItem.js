import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';

import colors from '../../config/colors';
import AppText from './ AppText';
import appFonts from '../../config/appFonts';
const AppProjectDetailsViewItem = ({
  containerStyle,
  imgSource,
  title,
  onPress,
}) => {
  return (
    <TouchableOpacity
    activeOpacity={0.8}
      style={[styles.container, containerStyle]}
      onPress={onPress}>
      <Image style={styles.img} source={imgSource} />
      <AppText style={styles.title}>{title}</AppText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '48%',
    backgroundColor: colors.secondary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {width: 24, height: 24, marginBottom: 10},
  title: {fontSize: appFonts.normalFontSize, color: colors.primary, textAlign: 'center'},
});

export default AppProjectDetailsViewItem;
