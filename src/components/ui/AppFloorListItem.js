import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';

import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import AppText from './ AppText';

const AppFloorListItem = ({
  menuItem,
  onPress,
  color = 'LynxWhite',
  textColor = 'jaguar',
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[styles.button, {backgroundColor: colors[color]}]}
      onPress={onPress}>
      <View style={styles.itemContainer}>
        <AppText
          style={[styles.text, {color: colors[textColor]}]}
          numberOfLines={1}>
          {menuItem.title}
        </AppText>

        {menuItem.selectedFloor ? (
          <Image
            style={styles.selectIcon}
            source={require('./../../assets/images/down-icon-b.png')}
          />
        ) : (
          <Image
            style={styles.icon}
            source={require('./../../assets/images/arrow-back.png')}
          />
        )}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    padding: 20,
    flexDirection: 'row',
    width: '100%',
    marginVertical: 9,
    backgroundColor: colors.LynxWhite,
  },
  itemContainer: {flexDirection: 'row', alignItems: 'center'},
  text: {
    flex: 1,
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  icon: {
    width: 8,
    height: 16,
  },
  selectIcon: {
    width: 14,
    height: 14,
  },
});

export default AppFloorListItem;
