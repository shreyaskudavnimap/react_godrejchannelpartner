import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';

import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import AppText from './ AppText';

const AppTowerListItem = ({
  menuItem,
  onPress,
  color = 'primary',
  textColor = 'secondary',
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[styles.button, {backgroundColor: colors[color]}]}
      onPress={onPress}>
      <View style={styles.itemContainer}>
        <AppText
          style={[styles.text, {color: colors[textColor]}]}
          numberOfLines={1}>
          {menuItem.title}
        </AppText>
        <AppText style={styles.txtFilling} numberOfLines={1}>
          {menuItem.filling_fast}
          {menuItem.filling_fast ? '!' : ''}
        </AppText>

        <Image
          style={styles.icon}
          source={require('./../../assets/images/arrow-back.png')}
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    padding: 20,
    flexDirection: 'row',
    width: '100%',
    marginVertical: 9,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  itemContainer: {flexDirection: 'row', alignItems: 'center'},
  text: {
    flex: 2,
    fontSize: appFonts.largeBold,
    textTransform: 'capitalize',
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  txtFilling: {
    flex: 1,
    marginLeft: 20,
    fontSize: appFonts.normalFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  icon: {
    width: 8,
    height: 16,
  },
});

export default AppTowerListItem;
