import React, {useState} from 'react';
import {View, StyleSheet, Image, TouchableWithoutFeedback} from 'react-native';

import AppText from './ AppText';
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';

const AppExpandButton = ({title, onPress}) => {
  return (
    <View style = {{paddingHorizontal: 25}}>
        <View style={styles.buttonConatainer}>
            <TouchableWithoutFeedback onPress = {onPress}>
                <View style = {styles.button}>
                <AppText style = {styles.buttonText}>{title}</AppText>
                    <Image 
                        source={require('./../../assets/images/down-icon-b.png')}
                        style={styles.icon} 
                    />
                </View>
            </TouchableWithoutFeedback>
        </View>
    </View>
  );
};

const styles = StyleSheet.create({
    buttonConatainer: {
        marginBottom: 20,
        borderWidth: 1,
        borderRadius: 1,
        borderColor: colors.lightGray,
        borderBottomWidth: 1,
        shadowColor: colors.lightGray,
        shadowOffset: {width: 2, height: 2},
        shadowOpacity: 0.7,
        shadowRadius: 3,
        elevation: 3,
        backgroundColor: colors.primary,
    },
    button: {
        height : 60, 
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems : 'center',
        paddingHorizontal: 20
    },
    buttonText: {
        fontSize: appFonts.largeBold,
        fontFamily: appFonts.SourceSansProSemiBold,
        color: colors.secondary
    },
    icon: {
        width: 16,
        height: 16,
    }, 
});

export default AppExpandButton;
