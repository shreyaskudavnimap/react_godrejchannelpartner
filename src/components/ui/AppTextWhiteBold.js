import React from 'react';
import {StyleSheet, Text} from 'react-native';
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
const AppTextWhiteBold= ({style, children, ...otherProps}) => {
  return (
    <Text style={[styles.text, style]} {...otherProps}>
      {children}
    </Text>
  );
};

const styles = StyleSheet.create({
  text: {
    color: colors.primary,
    fontSize: appFonts.xxxlargeFontSize,
    fontWeight:'bold'
  },
});

export default AppTextWhiteBold;
