import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
  Dimensions,
  Alert,
} from 'react-native';
import ImageModal from 'react-native-image-modal';

import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import AppText from './ AppText';
import appCurrencyFormatter from '../../utility/appCurrencyFormatter';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const EOIInventoryList = ({menuItem, onPress, onSelectionPlanImg}) => {
  const onClickPriceInfo = () => {
    Alert.alert(
      'Price Info',
      'Exclusive of Other Charges and All Govt.Taxes',
      [{text: 'GOT IT', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  };

  const isShowPlanImg = (floorBandItem) => {
    if (floorBandItem.show_floor_plan && floorBandItem.show_unit_plan) {
      if (
        (floorBandItem.show_floor_plan == '1' &&
          floorBandItem.floor_img &&
          floorBandItem.show_unit_plan == '1' &&
          floorBandItem.unit_img) ||
        (floorBandItem.show_floor_plan == '1' &&
          floorBandItem.floor_img &&
          floorBandItem.show_unit_plan != '1') ||
        (floorBandItem.show_floor_plan != '1' &&
          floorBandItem.show_unit_plan == '1' &&
          floorBandItem.unit_img)
      ) {
        return true;
      }
    }

    return false;
  };

  const getPlanImg = (floorBandItem) => {
    console.log(floorBandItem);
    if (floorBandItem) {
      if (
        floorBandItem.show_floor_plan == '1' &&
        floorBandItem.show_unit_plan == '1' &&
        floorBandItem.floor_img &&
        floorBandItem.unit_img
      ) {
        return !floorBandItem.isSelectedFloorPlan
          ? floorBandItem.floor_img
          : floorBandItem.unit_img;
      } else if (
        floorBandItem.show_floor_plan == '1' &&
        floorBandItem.show_unit_plan == '0' &&
        floorBandItem.floor_img
      ) {
        return floorBandItem.floor_img;
      } else if (
        floorBandItem.show_floor_plan == '0' &&
        floorBandItem.show_unit_plan == '1' &&
        floorBandItem.unit_img
      ) {
        return floorBandItem.unit_img;
      }
    }

    return null;
  };

  return (
    <View>
      <TouchableOpacity
        style={[
          styles.contaner,
          menuItem.selectedFloor ? styles.clicked : styles.notClicked,
        ]}
        activeOpacity={0.8}
        onPress={onPress}>
        <View style={styles.textContainer}>
          {menuItem.name ? (
            <AppText style={styles.txtTitle}>{menuItem.name}</AppText>
          ) : null}

          {menuItem.starting_price ? (
            <View style={styles.priceInfo}>
              <AppText style={styles.txtPrice}>
                Starting Price:&nbsp;₹&nbsp;
                {appCurrencyFormatter.getSearchCurrencyFormat(
                  menuItem.starting_price,
                )}
              </AppText>
              <TouchableWithoutFeedback onPress={onClickPriceInfo}>
                <View
                  style={styles.infoImgContainer}>
                  <Image
                    style={styles.infoImg}
                    source={require('./../../assets/images/information-icon-big.png')}
                  />
                </View>
              </TouchableWithoutFeedback>
            </View>
          ) : null}

          {menuItem.token_amount ? (
            <View style={styles.priceInfo}>
              <AppText style={styles.txtPrice}>
                Token Amount:&nbsp;₹&nbsp;
                {appCurrencyFormatter.getSearchCurrencyFormat(
                  menuItem.token_amount,
                )}
              </AppText>
            </View>
          ) : null}
        </View>
      </TouchableOpacity>

      {menuItem.selectedFloor && isShowPlanImg(menuItem) && (
        <View>
          <View style={{flexDirection: 'row'}}>
            {menuItem.show_floor_plan == '1' &&
            menuItem.show_unit_plan == '1' &&
            menuItem.floor_img &&
            menuItem.unit_img ? (
              <>
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={
                    menuItem.isSelectedFloorPlan
                      ? styles.buttonPlanSelected
                      : styles.buttonPlan
                  }
                  onPress={() => {
                    if (!menuItem.isSelectedFloorPlan) onSelectionPlanImg();
                  }}>
                  <AppText
                    style={
                      menuItem.isSelectedFloorPlan
                        ? styles.textPlanSelected
                        : styles.textPlan
                    }>
                    Floor Plan
                  </AppText>
                </TouchableOpacity>

                <TouchableOpacity
                  activeOpacity={0.8}
                  style={
                    !menuItem.isSelectedFloorPlan
                      ? styles.buttonPlanSelected
                      : styles.buttonPlan
                  }
                  onPress={() => {
                    if (menuItem.isSelectedFloorPlan) onSelectionPlanImg();
                  }}>
                  <AppText
                    style={
                      !menuItem.isSelectedFloorPlan
                        ? styles.textPlanSelected
                        : styles.textPlan
                    }>
                    Unit Plan
                  </AppText>
                </TouchableOpacity>
              </>
            ) : (
              <>
                {menuItem.show_floor_plan == '1' &&
                menuItem.show_unit_plan == '0' &&
                menuItem.floor_img ? (
                  <View style={styles.buttonPlanSelectedSingle}>
                    <AppText
                      style={
                        menuItem.isSelectedFloorPlan
                          ? styles.textPlanSelected
                          : styles.textPlan
                      }>
                      Floor Plan
                    </AppText>
                  </View>
                ) : null}
                {menuItem.show_floor_plan == '0' &&
                menuItem.show_unit_plan == '1' &&
                menuItem.unit_img ? (
                  <View style={styles.buttonPlanSelectedSingle}>
                    <AppText
                      style={
                        menuItem.isSelectedFloorPlan
                          ? styles.textPlanSelected
                          : styles.textPlan
                      }>
                      Unit Plan
                    </AppText>
                  </View>
                ) : null}
              </>
            )}
          </View>

          {getPlanImg(menuItem) && (
            <View style={styles.pImgContainer}>
              <ImageModal
                resizeMode="contain"
                imageBackgroundColor="#ffffff"
                style={styles.planImage}
                source={{
                  uri: getPlanImg(menuItem),
                }}
              />
            </View>
          )}
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  contaner: {
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: 'row',
    width: '100%',
    marginVertical: 9,
    backgroundColor: colors.primary,
    borderColor: colors.jaguar,
    borderWidth: 1,
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  clicked: {borderWidth: 1.5, borderColor: colors.jaguar},
  notClicked: {borderWidth: 1, borderColor: colors.lightGray},
  like: {height: 24, width: 24},
  txtTitle: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProBold,
    marginBottom: 5,
  },
  infoContainer: {
    flexDirection: 'row',
    marginVertical: 5,
    marginRight: 5,
    alignItems: 'center',
  },
  buttonPlan: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 22,
    paddingVertical: 8,
    flexDirection: 'row',
    borderBottomColor: colors.lightGray,
    borderBottomWidth: 1,
    backgroundColor: colors.primary,
    marginVertical: 6,
  },
  textPlan: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  buttonPlanSelected: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 22,
    paddingVertical: 8,
    flexDirection: 'row',
    borderBottomColor: colors.jaguar,
    borderBottomWidth: 3,
    backgroundColor: colors.primary,
    marginVertical: 6,
  },
  buttonPlanSelectedSingle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
    paddingVertical: 8,
    borderBottomColor: colors.jaguar,
    borderBottomWidth: 3,
    backgroundColor: colors.primary,
    marginVertical: 6,
  },
  textPlanSelected: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.jaguar,
  },
  pImgContainer: {
    marginVertical: 22,
    width: windowWidth * 0.8,
    height: windowHeight / 4,
    // backgroundColor: '#e1e4e8',
    alignItems: 'center',
    justifyContent: 'center',
  },

  planImage: {
    width: windowWidth * 0.8,
    height: windowHeight / 4,
  },
  infoTxt: {marginRight: 3, fontSize: 16},
  infoTxtSept: {marginRight: 3},
  priceInfo: {flexDirection: 'row', alignItems: 'center', marginBottom: 4},
  txtPrice: {fontSize: 16},
  infoImgContainer: {paddingLeft: 5, paddingRight: 12, paddingVertical: 4},
  infoImg: {height: 12, width: 12},
});

export default EOIInventoryList;
