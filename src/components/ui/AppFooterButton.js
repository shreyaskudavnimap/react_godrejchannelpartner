import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';

import colors from '../../config/colors';
import AppText from '../ui/ AppText';
import appFonts from '../../config/appFonts';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const AppFooterButton = ({
  title,
  onPress,
  color = 'gray',
  imgSource,
  imgSize = 22,
  isCountShow = false,
  countTxt = '',
  isPostSales,
}) => {

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.item}>
        <Image
          style={[styles.itemImg, {height: imgSize, width: imgSize}]}
          source={imgSource}
        />
        <AppText style={[styles.itemTxt, {color: colors[color]}]}>
          {title}
        </AppText>

        {isCountShow && countTxt !== '' && (
          <View
            style={[
              styles.count,
              {
                right:
                  windowWidth > 360
                    ? isPostSales
                      ? 24
                      : 15
                    : isPostSales
                    ? 15
                    : 10,
              },
            ]}>
            <AppText style={styles.countTxt}>{countTxt}</AppText>
          </View>
        )}
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  item: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  itemTxt: {
    fontFamily: appFonts.SourceSansProSemiBold,
    fontSize: appFonts.largeFontSize,
    marginTop: 4,
    marginBottom: 7,
    textAlign: 'center'
  },
  count: {
    height: 24,
    width: 24,
    backgroundColor: colors.secondaryDark,
    position: 'absolute',
    top: -10,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
  },
  countTxt: {
    color: colors.primary,
    fontSize: appFonts.smallFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
});

export default AppFooterButton;
