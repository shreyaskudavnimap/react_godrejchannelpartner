import React, {useState, useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import {
  useNavigation,
  StackActions,
  CommonActions,
} from '@react-navigation/native';
import {useIsFocused} from '@react-navigation/native';

import colors from '../../config/colors';
import AppFooterButton from './AppFooterButton';
import {useDispatch, useSelector} from 'react-redux';
import * as menuTypeAction from './../../store/actions/menuTypeAction';

const preSalesFooter = [
  {
    id: 1,
    title: 'Home',
    img: require('./../../assets/menu/icon-home.png'),
    activeImg: require('./../../assets/menu/icon-home-a.png'),
    isActive: false,
    screen: 'GodrejHome',
  },
  {
    id: 2,
    title: 'Search',
    img: require('./../../assets/menu/icon-search.png'),
    activeImg: require('./../../assets/menu/icon-search-a.png'),
    isActive: false,
    screen: 'SearchFilter',
  },
  {
    id: 3,
    title: 'Contact',
    img: require('./../../assets/menu/icon-contact.png'),
    activeImg: require('./../../assets/menu/icon-contact-a.png'),
    isActive: false,
    screen: 'ContactScreen',
  },
  // {
  //   id: 4,
  //   title: 'Visit',
  //   img: require('./../../assets/menu/icon-visit.png'),
  //   activeImg: require('./../../assets/menu/icon-visit-a.png'),
  //   isActive: false,
  //   screen: 'PresaleVisitScreen',
  // },
  {
    id: 7,
    title: 'Menu',
    img: require('./../../assets/menu/open-menu.png'),
    activeImg: require('./../../assets/menu/open-menu-a.png'),
    isActive: false,
    screen: 'UserDetailsScreen',
  },
];

const preSaleFooterCustomer = [
  {
    id: 1,
    title: 'Home',
    img: require('./../../assets/menu/icon-home.png'),
    activeImg: require('./../../assets/menu/icon-home-a.png'),
    isActive: false,
    screen: 'GodrejHome',
  },
  {
    id: 2,
    title: 'Search',
    img: require('./../../assets/menu/icon-search.png'),
    activeImg: require('./../../assets/menu/icon-search-a.png'),
    isActive: false,
    screen: 'SearchFilter',
  },
  {
    id: 3,
    title: 'Contact',
    img: require('./../../assets/menu/icon-contact.png'),
    activeImg: require('./../../assets/menu/icon-contact-a.png'),
    isActive: false,
    screen: 'ContactScreen',
  },
  {
    id: 8,
    // title: 'Projects',
    title: 'My Properties',
    img: require('./../../assets/menu/icon-projects.png'),
    activeImg: require('./../../assets/menu/icon-projects-a.png'),
    isActive: false,
    screen: 'DashboardScreen',
  },
  {
    id: 7,
    title: 'Menu',
    img: require('./../../assets/menu/open-menu.png'),
    activeImg: require('./../../assets/menu/open-menu-a.png'),
    isActive: false,
    screen: 'UserDetailsScreen',
  },
];

const postSalesFooter = [
  {
    id: 1,
    title: 'Home',
    img: require('./../../assets/menu/icon-home.png'),
    activeImg: require('./../../assets/menu/icon-home-a.png'),
    isActive: false,
    screen: 'DashboardScreen',
  },
  {
    id: 5,
    title: 'FAQ ',
    img: require('./../../assets/menu/icon-faq.png'),
    activeImg: require('./../../assets/menu/icon-faq-a.png'),
    isActive: false,
    screen: 'FAQ',
  },
  {
    id: 6,
    title: 'Reach Us',
    img: require('./../../assets/menu/icon-contact.png'),
    activeImg: require('./../../assets/menu/icon-contact-a.png'),
    isActive: false,
    screen: 'ReachUs',
  },
  {
    id: 7,
    title: 'Menu',
    img: require('./../../assets/menu/open-menu.png'),
    activeImg: require('./../../assets/menu/open-menu-a.png'),
    isActive: false,
    screen: 'UserDetailsScreen',
  },
];

const AppFooter = ({
  isPostSales = false,
  activePage = '0',
  isGoBackLink = false,
}) => {
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const loginvalue = useSelector((state) => state.loginInfo.loginResponse);
  const isCustomer =
    typeof loginvalue.data !== 'undefined' ? loginvalue.data.is_customer : 0;
  // console.log("isCustomer", isCustomer)

  const menuType = useSelector((state) => state.menuType.menuType);
  // console.log('menuType:: ', menuType);
  // console.log('isPostSales:: ', isPostSales);

  const selectedPreference = isPostSales
    ? postSalesFooter
    : isCustomer == 1 && menuType == 'presale'
    ? preSaleFooterCustomer
    : preSalesFooter;

  const [footerMenu, setFooterMenu] = useState(selectedPreference.slice(0));

  const badgeData = useSelector((state) => state.badgeCount);
  const notificationCount =
    badgeData && badgeData.notificationCount ? badgeData.notificationCount : '';

  useEffect(() => {
    setFooterMenu(selectedPreference.slice(0));
  }, [dispatch, menuType, isPostSales]);

  const handleMenuType = async () => {
    dispatch(
      menuTypeAction.setMenuData({
        menuType: 'postsale',
      }),
    );
  };

  const onPressFooerMenu = (menu) => {
    // console.log('menu', menu)

    // if (menu.title == 'Projects' || menu.title == 'My Properties') {
    //   handleMenuType();
    // }

    if (menu.title == 'My Properties') {
      handleMenuType();
    }

    try {
      if (activePage == menu.id) {
        // console.log('activePage:: ', activePage);
        // console.log('activePage:: ', menu);
        // console.log('activePage:: ', navigation);
        if (activePage == '7') {
          // navigation.goBack();
          navigation.dispatch(StackActions.pop());
        } else if (activePage == '2' && isGoBackLink) {
          navigation.goBack();
        }
      } else {
        // if (activePage == '7') {
        //   navigation.reset({
        //     routes: [{name: 'UserDetailsScreen'}],
        //   });
        // } else {
        //   onHandleFooterMenu(menu.id);
        //   navigation.navigate(menu.screen);
        // }
        onHandleFooterMenu(menu.id);
        // navigation.navigate(menu.screen);

        if (menu.id == '8') {
          navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [{name: 'DashboardScreen'}],
            }),
          );
        } else if (menu.id == '1') {
          navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [{name: menu.screen}],
            }),
          );
        } else {
          navigation.dispatch(StackActions.push(menu.screen));
        }
      }

      // if (activePage == '6') {
      //   // navigation.navigate({
      //   //   routes: [{name: 'ReachUs'}],
      //   // });
      //   onHandleFooterMenu(menu.id);
      //   navigation.navigate(menu.screen);
      // }
      // if (activePage == '5') {
      //   // navigation.navigate({
      //   //   routes: [{name: 'FAQ'}],
      //   // });
      //   onHandleFooterMenu(menu.id);
      //   navigation.navigate(menu.screen);
      // }
    } catch (error) {
      console.log('onPressFooerMenu: error:: ', error);
    }
  };

  const onHandleFooterMenu = (activeMenu) => {
    const footer = [...footerMenu];
    footer.map((menu) => {
      if (menu.id == activeMenu.id) {
        menu.isActive = true;
      } else {
        menu.isActive = false;
      }
    });
    setFooterMenu(footer);
  };

  return (
    <View style={styles.container}>
      {footerMenu.map((menu) => (
        <AppFooterButton
          key={menu.id}
          title={menu.title}
          imgSource={menu.id == activePage ? menu.activeImg : menu.img}
          color={menu.id == activePage ? 'jaguar' : 'gray'}
          onPress={() => onPressFooerMenu(menu)}
          isCountShow={
            menu.id === 7 ? (menuType == 'presale' ? true : false) : false
          }
          countTxt={menuType == 'presale' ? notificationCount : ''}
          isPostSales={isPostSales}
        />
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    borderWidth: 1,
    borderColor: 'rgba(255,255,255,0.1)',
    borderTopColor: colors.lightGray,
    backgroundColor: colors.primary,
    paddingHorizontal: 5,
    paddingTop: 7,
  },
});

export default AppFooter;
