import React from 'react';
import {View, StyleSheet, Image, TouchableWithoutFeedback} from 'react-native';

import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import AppText from './ AppText';

const AppUserChoice = ({
  title,
  onPress
}) => {
  return (
    <TouchableWithoutFeedback
      style={styles.button}
      onPress={onPress}>
      <View style={styles.itemContainer}>
        <AppText style={styles.text}>{title}</AppText>

        <Image
          style={styles.icon}
          source={require('./../../assets/images/next-icon.png')}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    marginBottom: 20,
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.jaguar,
    paddingHorizontal: 18,
    paddingVertical: 28,
  },
  text: {
    flex: 1,
    fontSize: appFonts.xlargeFontSize,
    fontFamily: appFonts.RobotoBold,
    color: colors.primary,
    lineHeight: 32,
    marginRight: 20,
  },
  icon: {
    width: 20,
    height: 20,
  },
});

export default AppUserChoice;
