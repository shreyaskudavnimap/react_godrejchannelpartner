import React from 'react';
import {View, StyleSheet} from 'react-native';
import Loader from 'react-native-three-dots-loader';

import colors from '../../config/colors';

const AppOverlayLoader = ({isLoading = false, isWhiteBackground = false, isZindex= false}) => {
  if (!isLoading) return null;

  return (
    <View
      style={[
        styles.loadinContainer,
        {
          backgroundColor: isWhiteBackground
            ? colors.primary
            : 'rgba(255,255,255,0.5)',
            zIndex: isZindex ? 90 : 0
        },
      ]}>
      <Loader
        size={8}
        dotMargin={6}
        background={colors.secondaryLight}
        activeBackground={colors.secondaryDark}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  loadinContainer: {
    flex: 1,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: 'rgba(255,255,255,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default AppOverlayLoader;
