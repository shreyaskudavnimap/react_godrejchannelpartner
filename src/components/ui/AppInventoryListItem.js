import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';

import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import AppText from './ AppText';
import appCurrencyFormatter from '../../utility/appCurrencyFormatter';

const AppInventoryListItem = ({
  menuItem,
  onPress,
  onClickPriceInfo,
  isSqFt = false,
  onWishlistPress,
}) => {
  return (
    <View
      style={[
        styles.contaner,
        {
          borderWidth: menuItem.selectedInventory ? 1.5 : 1,
          borderColor: menuItem.selectedInventory ? colors.jaguar : colors.gray,
        },
      ]}>
      <TouchableOpacity
        style={styles.textContainer}
        activeOpacity={0.8}
        onPress={onPress}>
        {menuItem.unit_no ? (
          <AppText style={styles.txtTitle}>{menuItem.unit_no}</AppText>
        ) : null}
        <View style={styles.infoContainer}>
          {isSqFt ? (
            <View>
              {menuItem.carpet_area_sq_ft ? (
                <AppText style={styles.infoTxt}>
                  {menuItem.carpet_area_sq_ft}
                </AppText>
              ) : null}
            </View>
          ) : (
            <View>
              {menuItem.carpet_area ? (
                <AppText style={styles.infoTxt}>{menuItem.carpet_area}</AppText>
              ) : null}
            </View>
          )}

          {(menuItem.carpet_area_sq_ft || menuItem.carpet_area) &&
          menuItem.floor_title ? (
            <AppText style={styles.infoTxtSept}>|</AppText>
          ) : null}

          {menuItem.floor_title ? (
            <>
              {/* <AppText style={styles.infoTxtSept}>|</AppText> */}
              <AppText style={styles.infoTxt}>{menuItem.floor_title}</AppText>
            </>
          ) : null}
{/* 
          {menuItem.typology_title ? (
            <>
              <AppText style={styles.infoTxtSept}>|</AppText>
              <AppText style={styles.infoTxt}>
                {menuItem.typology_title}
              </AppText>
            </>
          ) : null}

          {menuItem.field_wing ? (
            <>
              <AppText style={styles.infoTxtSept}>|</AppText>
              <AppText style={styles.infoTxt}>
                Wing {menuItem.field_wing}
              </AppText>
            </>
          ) : null}
           */}
        </View>


        {/* {menuItem.inventory_price_formatted ? (
          <View style={styles.priceInfo}>
            <AppText style={styles.txtPrice}>
              ₹&nbsp;
              {menuItem.inventory_price_formatted}
            </AppText>
            <TouchableWithoutFeedback onPress={onClickPriceInfo}>
              <View style={styles.infoImgContainer}>
                <Image
                  style={styles.infoImg}
                  source={require('./../../assets/images/information-icon-big.png')}
                />
              </View>
            </TouchableWithoutFeedback>
          </View>
        ) : null} */}
      </TouchableOpacity>

      {/* <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => onWishlistPress(menuItem)}
        style={styles.wishlistContainer}>
        <Image
          style={styles.like}
          source={
            menuItem.wishlist_status && menuItem.wishlist_status == '1'
              ? require('./../../assets/images/thumb-fill.png')
              : require('./../../assets/images/thumb-line.png')
          }
        />
      </TouchableOpacity> */}
    </View>
  );
};

const styles = StyleSheet.create({
  contaner: {
    alignItems: 'center',
    paddingLeft: 12,
    paddingRight: 5,
    paddingVertical: 12,
    flexDirection: 'row',
    width: '100%',
    marginVertical: 9,
    backgroundColor: colors.primary,
    borderColor: colors.jaguar,
    borderWidth: 1,
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  wishlistContainer: {paddingVertical: 12, paddingHorizontal: 8, marginLeft: 3},
  like: {height: 22, width: 22},
  txtTitle: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProBold,
  },
  infoContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginVertical: 5,
    marginRight: 3,
    alignItems: 'center',
  },
  infoTxt: {marginRight: 4, fontSize: appFonts.normalFontSize},
  infoTxtSept: {
    marginRight: 4,
    fontSize: appFonts.smallFontSize,
    fontFamily: appFonts.SourceSansProBold,
  },
  priceInfo: {flexDirection: 'row', alignItems: 'center'},
  txtPrice: {fontSize: appFonts.normalFontSize},
  infoImgContainer: {paddingLeft: 5, paddingRight: 12, paddingVertical: 4},
  infoImg: {height: 12, width: 12},
});

export default AppInventoryListItem;
