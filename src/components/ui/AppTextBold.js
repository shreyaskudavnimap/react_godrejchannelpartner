import React from 'react';
import {StyleSheet, Text} from 'react-native';
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
const AppTextBold = ({style, children, ...otherProps}) => {
  return (
    <Text style={[styles.text, style]} {...otherProps}>
      {children}
    </Text>
  );
};

const styles = StyleSheet.create({
  text: {
    color: colors.secondary,
    fontSize: appFonts.xlargeFontSize,
    fontWeight:'bold',
    marginTop:'3%',
    marginBottom:"3%"
  },
});

export default AppTextBold;
