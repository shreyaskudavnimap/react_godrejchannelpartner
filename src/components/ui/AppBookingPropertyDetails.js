import React from 'react';
import {View, StyleSheet} from 'react-native';
import appFonts from '../../config/appFonts';
import colors from '../../config/colors';
import AppText from './ AppText';

const AppBookingPropertyDetails = ({label, value}) => {
  return (
    <View style={styles.container}>
      <AppText style={styles.label}>{label}</AppText>
      <AppText style={styles.value}>{value}</AppText>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 12,
  },
  label: {
    flex: 1,
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  value: {
    flex: 1,
    fontSize: appFonts.largeFontSize,
    color: colors.expandText,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
});

export default AppBookingPropertyDetails;
