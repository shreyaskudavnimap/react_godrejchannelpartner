import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import AppText from './ AppText';
import appFonts from '../../config/appFonts';

import appCurrencyFormatter from './../../utility/appCurrencyFormatter';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const LandingPageViewItem = ({
  itemIndex,
  totalItem,
  onPress,
  item,
  isCityWise,
  addToWishList,
  navigation,
  dispatch,
  isLoggedIn,
}) => {
  // console.log(
  //   'LandingPageViewItem: ' + (item.proj_name ? item.proj_name : item.title) + ' wishlist_status: ' + item.wishlist_status,
  // );
  
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        onPress(item);
      }}>
      <View
        style={[
          styles.viewItem,
          {marginRight: itemIndex === totalItem - 1 ? 0 : 20},
        ]}>
        <View style={styles.imgContainer}>
          <Image
            style={styles.viewImg}
            source={{
              uri: item.field_app_thumbnail_image,
            }}
          />
        </View>
        <View style={styles.viewTxtContainer}>
          <AppText numberOfLines={1} style={styles.viewTopTxt}>
            <AppText style={styles.priceTxt}>
              {item.proj_name ? item.proj_name : item.title},
            </AppText>
            &nbsp;
            {item.cities
              ? item.cities
              : isCityWise
              ? item.sub_location
              : item.field_city}
          </AppText>
          <AppText numberOfLines={1} style={styles.viewBottomTxt}>
          ₹{appCurrencyFormatter.landingScreenCurrencyFormat(
              item.min_price ? item.min_price : item.starting_price,
            )}
          </AppText>
        </View>

        {item.wishlist_status && <View style={styles.wishlistContainer}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              addToWishList(isLoggedIn, navigation, dispatch, item)
            }>
            <Image
              style={styles.wishlist}
              source={
                item.wishlist_status == '0'
                  ? require('./../../assets/images/thumb-line.png')
                  : require('./../../assets/images/thumb-fill-w.png')
              }
            />
          </TouchableOpacity>
        </View>}

        {/* <View style={styles.greenContainer}>
          <Image
            style={styles.greenCertificate}
            source={require('./../../assets/images/procertificate.png')}
          />
        </View> */}
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  viewItem: {
    width: windowWidth / 1.7,
    height: windowHeight / 2.25,
  },
  imgContainer: {backgroundColor: '#e1e4e8', width: '100%', height: '80%'},
  viewImg: {width: '100%', height: '100%'},
  viewTxtContainer: {
    width: '100%',
    height: '20%',
    justifyContent: 'center',
    paddingTop: 10,
  },
  viewTopTxt: {fontFamily: appFonts.SourceSansProRegular, fontSize:appFonts.largeBold},
  viewBottomTxt: {
    fontFamily: appFonts.SourceSansProRegular,
    fontSize:appFonts.largeBold,
    marginTop: 2,
  },
  priceTxt: {fontFamily: appFonts.SourceSansProBold, fontSize: appFonts.largeBold},
  wishlistContainer: {
    height: 34,
    width: 34,
    position: 'absolute',
    top: 5,
    bottom: 0,
    right: 5,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'rgba(255,255,255,0.1)',
    // borderRadius: 17,
  },
  wishlist: {height: 22, width: 22},
  greenContainer: {
    height: 52,
    width: 52,
    position: 'absolute',
    top: 5,
    bottom: 0,
    left: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  greenCertificate: {height: 50, width: 50},
});

export default LandingPageViewItem;
