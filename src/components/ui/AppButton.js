import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import AppText from './ AppText';

const AppButton = ({
  title,
  onPress,
  color = 'secondary',
  textColor = 'primary',
  opacity = 1,
  disabled = false
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[
        styles.button,
        {
          backgroundColor: colors[color],
          opacity: opacity,
          zIndex: 99,
          position: 'relative',
        },
      ]}
      onPress={onPress}
      disabled={disabled}
      >
      <AppText style={[styles.text, {color: colors[textColor]}]}>
        {title}
      </AppText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 17,
    flexDirection: 'row',
    width: '100%',
    marginVertical: 9,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  text: {
    fontSize: appFonts.largeBold,
    //textTransform: 'uppercase',
    fontFamily: appFonts.SourceSansProBold,
  },
});

export default AppButton;
