import React from 'react';
import { Image, TouchableWithoutFeedback, View, StyleSheet } from "react-native";
import AppText from "./ AppText";
import colors from "../../config/colors";
import appFonts from '../../config/appFonts';
export default ({ctaDetails, item}) => (
  <TouchableWithoutFeedback
    onPress={() => {
      if (item.cta) {
        ctaDetails(item.cta,item.name,item.id);
      }
    }}
    disabled={item.access === false}
  >
    <View
      style={[styles.button, {marginHorizontal: 10}]}>
      <AppText
        style={[
          styles.text,
          item.access === false
            ? styles.buttonTextDisabled
            : null,
        ]}>
        {item.name}
      </AppText>
      {item.is_downloadable === 1 ? (
        <Image
          source={require('../../assets/images/download-b.png')}
          style={styles.subIcon}
        />
      ) : null}
    </View>
  </TouchableWithoutFeedback>
)

const styles = StyleSheet.create({
  button: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 25,
  },
  text: {
    fontSize:appFonts.largeBold,
  },
  buttonTextDisabled: {color: colors.lightGray},
  subIcon: {
    width: 14,
    height: 14,
  },
});
