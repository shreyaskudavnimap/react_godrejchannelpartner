import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import AppText from './ AppText';

const AppSearchFilterListItem = ({
  title,
  onPress,
  color = 'primary',
  textColor = 'jaguar',
  index,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[
        styles.button,
        {backgroundColor: colors[color], marginLeft: index !== 0 ? 18 : 0},
      ]}
      onPress={onPress}>
      <AppText style={[styles.text, {color: colors[textColor]}]}>
        {title}
      </AppText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 14,
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
    marginVertical: 10,
  },
  text: {
    fontSize: appFonts.normalFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
});

export default AppSearchFilterListItem;
