import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {IMAGE_PLACEHOLDER} from '../../utility/appConstant';

import colors from '../../config/colors';
import AppText from './ AppText';
// import {OfflineImage} from 'react-native-image-offline';
import FastImage from 'react-native-fast-image';
const AppProjectDetailsViewImgItemWithCache = ({
  containerStyle,
  imgSource,
  onPress,
  viewIndex = 0,
  viewItemLength = 0,
}) => {
  const [isImageLoading, setIsImageLoading] = useState(false);

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[
        styles.container,
        containerStyle,
        {marginBottom: viewIndex !== viewItemLength ? 5 : 0},
      ]}
      onPress={onPress}>
      <View style={styles.imgContainer}>
        {/*<OfflineImage*/}
        {/*  key={Date.now().toString()}*/}
        {/*  style={styles.img}*/}
        {/*  resizeMode="cover"*/}
        {/*  fallbackSource={{uri: IMAGE_PLACEHOLDER}}*/}
        {/*  source={{uri: imgSource}}*/}
        {/*/>*/}
        <FastImage
          style={styles.img}
          source={{
            uri: imgSource,
            cache: 'immutable',
            priority: 'high',
          }}
          fallback={IMAGE_PLACEHOLDER}
          onLoadStart={() => {
            setIsImageLoading(true);
          }}
          onLoadEnd={() => {
            setIsImageLoading(false);
          }}
        />
        {isImageLoading ? (
          <View
            style={{
              height: '100%',
              position: 'absolute',
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              backgroundColor: '#ffffff',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator
              color={colors.jaguar}
              size="small"
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            />
          </View>
        ) : null}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '48%',
    backgroundColor: colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgContainer: {backgroundColor: '#e1e4e8', width: '100%', height: '100%'},
  img: {width: '100%', height: '100%'},
});

export default AppProjectDetailsViewImgItemWithCache;
