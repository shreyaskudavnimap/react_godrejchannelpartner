import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';

import colors from '../../config/colors';
import AppText from './ AppText';

const AppProjectDetailsViewImgItem = ({
  containerStyle,
  imgSource,
  onPress,
  viewIndex=0,
  viewItemLength = 0
}) => {
  return (
    <TouchableOpacity
    activeOpacity={0.8}
      style={[styles.container, containerStyle, {marginBottom : viewIndex != viewItemLength ? 5: 0}]}
      onPress={onPress}>
        <View style={styles.imgContainer}>
      <Image style={styles.img} source={{uri: imgSource}} />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '48%',
    backgroundColor: colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgContainer: {backgroundColor: '#e1e4e8', width: '100%', height: '100%'},
  img: {width: '100%', height: '100%'}
});

export default AppProjectDetailsViewImgItem;
