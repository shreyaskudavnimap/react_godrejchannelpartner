import React from "react";
import { StyleSheet, TouchableWithoutFeedback } from "react-native";

import colors from "../../config/colors";
import appFonts from "../../config/appFonts";
import AppText from "./ AppText";

const AppClickableText = ({ style, children, onPress }) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <AppText style={[styles.text, style]}>{children}</AppText>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  text: {
    color: colors.secondary,
    fontSize: appFonts.largeBold,
    padding: 6,
    fontFamily: appFonts.SourceSansProBold,
  },
});

export default AppClickableText;