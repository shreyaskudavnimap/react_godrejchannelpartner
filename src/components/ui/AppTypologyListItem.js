import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';

import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import AppText from './ AppText';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const AppTypologyListItem = ({
  menuItem,
  index,
  color = 'primary',
  textColor = 'jaguar',
  borderColor = 'jaguar',
  onPress,
  isSqFt = false,
}) => {
  // console.log('color: ', color)
  // console.log('textColor: ', textColor)
  // console.log('borderColor: ', borderColor)

  return (
    <TouchableOpacity
      style={[
        styles.contaner,
        {
          //backgroundColor: colors[color] ? colors[color] : colors['primary'],
          // borderColor: colors[borderColor]
          //   ? colors[borderColor]
          //   : colors['jaguar'],
          marginLeft: index != 0 ? 12 : 0,
        },
      ]}
      activeOpacity={0.8}
      onPress={onPress}>
      <View style={styles.textContainer}>
        {menuItem && menuItem.title ? (
          <AppText
            style={[
              styles.txtTitle,
              //{color: colors[textColor] ? colors[textColor] : colors['jaguar']},
            ]}>
            {menuItem.title}
          </AppText>
        ) : null}
        {menuItem &&
        (menuItem.field_floor_size_square_foot ||
          menuItem.field_floor_size_square_meter) ? (
          <AppText
            style={[
              styles.txtPrice,
              //{color: colors[textColor] ? colors[textColor] : colors['jaguar']},
            ]}>
            {isSqFt
              ? menuItem.field_floor_size_square_foot
              : menuItem.field_floor_size_square_meter}
          </AppText>
        ) : null}
        {menuItem && menuItem.field_floor_price_range ? (
          <AppText
            style={[
              styles.txtPrice,
              //{color: colors[textColor] ? colors[textColor] : colors['jaguar']},
            ]}>
            ₹&nbsp;{menuItem.field_floor_price_range}
          </AppText>
        ) : null}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  contaner: {
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: windowWidth / 2,
    flexDirection: 'row',
    marginVertical: 9,
    borderColor: colors.jaguar,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  like: {height: 24, width: 24},
  txtTitle: {fontSize: appFonts.largeBold, fontFamily: appFonts.SourceSansProBold},
  txtPrice: {fontSize: appFonts.largeFontSize, marginTop: 4},
});

export default AppTypologyListItem;
