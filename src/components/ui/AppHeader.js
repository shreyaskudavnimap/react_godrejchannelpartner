import React from 'react';
import { View, StyleSheet, Image, TouchableWithoutFeedback } from 'react-native';
import colors from '../../config/colors';
import { useLinkProps, useNavigation } from '@react-navigation/native';
import AppText from './ AppText';
import appFonts from '../../config/appFonts';

const AppHeader = ({
  onPress,
  selectionIconSource,
  isShowUnitSwitch = false,
  isSqFt = false,
  changeUnit,
  onWishlistPress,
  bankDetailsEditForm = false,
  isShowBankDetailView,
  isBankDetailsEditForm
}) => {
  const navigation = useNavigation();

  const goBack = () => {
    if (!bankDetailsEditForm) {
      navigation.goBack();
    }
    if (bankDetailsEditForm) {
      isShowBankDetailView(true);
      isBankDetailsEditForm(false)
    }
  };

  return (
    <View style={styles.container}>
      <TouchableWithoutFeedback onPress={goBack}>
        <Image
          style={styles.backIcon}
          source={require('./../../assets/images/btn-header-back.png')}
        />
      </TouchableWithoutFeedback>

      {selectionIconSource && (
        <TouchableWithoutFeedback onPress={onWishlistPress}>
          <Image style={styles.selectionIcon} source={selectionIconSource} />
        </TouchableWithoutFeedback>
      )}

      {isShowUnitSwitch && (
        <TouchableWithoutFeedback onPress={changeUnit}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <AppText
              style={[
                styles.switchTxt,
                {
                  color: isSqFt ? colors.secondary : colors.gray3,
                  fontFamily: isSqFt
                    ? appFonts.SourceSansProSemiBold
                    : appFonts.SourceSansProRegular,
                },
              ]}>
              sqft
            </AppText>
            <Image
              style={styles.switch}
              source={
                isSqFt
                  ? require('./../../assets/images/toggle-on.png')
                  : require('./../../assets/images/toggle-off.png')
              }
            />
            <AppText
              style={[
                styles.switchTxt,
                {
                  color: !isSqFt ? colors.secondary : colors.gray3,
                  fontFamily: !isSqFt
                    ? appFonts.SourceSansProSemiBold
                    : appFonts.SourceSansProRegular,
                },
              ]}>
              sqmt
            </AppText>
          </View>
        </TouchableWithoutFeedback>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 18,
    paddingVertical: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: colors.primary,
  },
  backIcon: {
    height: 22,
    width: 65,
  },
  selectionIcon: {
    height: 24,
    width: 24,
  },
  switchTxt: {
    fontSize: appFonts.largeFontSize,
  },
  switch: {
    height: 16,
    width: 32,
    marginHorizontal: 8,
  },
});

export default AppHeader;
