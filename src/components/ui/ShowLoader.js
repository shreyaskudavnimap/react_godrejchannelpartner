import React from 'react';
import { ActivityIndicator, View, Modal, StyleSheet} from 'react-native';
import colors from '../../config/colors';
import Loader from 'react-native-three-dots-loader';

const ShowLoader = () => {
  
    return (
        <View style={styles.container}>
            <Modal
            animationType = {"fade"}
            transparent = {true}
            visible = {true}>
            <View style = {styles.modelContainer}>
                <ActivityIndicator size="large" color={colors.secondaryLight} />
            </View>
            </Modal>
        </View>
    );
  };


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'rgba(255,255,255,0.5)',
    },
    modelContainer:{
      flex:1,
      justifyContent:'center',
      alignItems:'center',
      backgroundColor:'rgba(255,255,255,0.5)'
    },
  });

  export default ShowLoader;