import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';

import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import AppText from './ AppText';
import appCurrencyFormatter from '../../utility/appCurrencyFormatter';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const EOITypologyListItem = ({
  menuItem,
  index,
  color = 'primary',
  textColor = 'jaguar',
  borderColor = 'jaguar',
  onPress,
}) => {

  // console.log('index: ', index);
  // console.log('color: ', colors[color]);
  // console.log('textColor: ', textColor);
  // console.log('borderColor: ', borderColor);
  // console.log('***************');

  return (
    <TouchableOpacity
      style={[
        styles.contaner,
        {
          backgroundColor: colors[color] ? colors[color] : colors['primary'],
          borderColor: colors[borderColor] ? colors[borderColor] : colors['jaguar'],
          marginLeft: index != 0 ? 12 : 0,
        },
      ]}
      activeOpacity={0.8}
      onPress={onPress}>
      <View style={styles.textContainer}>
        {menuItem && menuItem.name ? (
          <AppText style={[styles.txtTitle, {color: colors[textColor] ? colors[textColor] : colors['jaguar']}]}>
            {menuItem.name}
          </AppText>
        ) : null}

        {menuItem && menuItem.starting_price ? (
          <AppText style={[styles.txtPrice, {color: colors[textColor] ? colors[textColor] : colors['jaguar']}]}>
            ₹&nbsp;
            {appCurrencyFormatter.getSearchCurrencyFormat(
              menuItem.starting_price,
            )}
          </AppText>
        ) : null}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  contaner: {
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: windowWidth / 2,
    flexDirection: 'row',
    marginVertical: 9,
    borderColor: colors.jaguar,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingVertical: 8,
  },
  txtTitle: {fontSize:appFonts.largeBold, fontFamily: appFonts.SourceSansProBold},
  txtPrice: {fontSize: appFonts.largeFontSize, marginTop: 4},
});

export default EOITypologyListItem;
