import React from 'react';
import * as Progress from 'react-native-progress';
import {Dimensions, View, StyleSheet} from 'react-native';

import AppText from './ AppText';
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const AppProgressBar = ({progress}) => {
  return (
    <View style={styles.container}>
      <View style={styles.popupContainer}>
        <AppText style={styles.msg}>Your file is downloading...</AppText>

        <Progress.Bar
          progress={progress / 100}
          width={windowWidth / 1.5}
          height={4}
          color={colors.jaguar}
        />

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            width: windowWidth / 1.5,
            paddingVertical: 10,
          }}>
          <AppText style={[styles.percentage, {textAlign: 'left'}]}></AppText>
          <AppText style={[styles.percentage, {textAlign: 'right'}]}>
            {Math.round(progress)}%
          </AppText>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  popupContainer: {
    backgroundColor: colors.primary,
    paddingVertical: 22,
    width: windowWidth / 1.15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    borderColor: colors.jaguar,
    borderWidth: 1,
  },
  msg: {
    width: windowWidth / 1.5,
    fontSize: appFonts.largeBold,
    paddingVertical: 12,
    marginBottom: 8,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  percentage: {flex: 1, textAlign: 'right', fontSize: appFonts.largeFontSize, fontFamily: appFonts.SourceSansProSemiBold},
});

export default AppProgressBar;
