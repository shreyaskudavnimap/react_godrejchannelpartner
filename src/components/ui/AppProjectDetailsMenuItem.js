import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';

import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import AppText from './ AppText';

const AppProjectDetailsMenuItem = ({
  title,
  onPress,
  color = 'primary',
  textColor = 'secondary',
  isMenuOpen = false
}) => {

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[styles.button, { backgroundColor: colors[color] }]}
      onPress={onPress}>
      <View style={styles.itemContainer}>
        <AppText
          style={[styles.text, { color: colors[textColor] }]}
          numberOfLines={1}>
          {title}
        </AppText>
        {isMenuOpen ? (
          <Image
            style={styles.iconY}
            source={require('./../../assets/images/arrow-back-Y.png')}
          />
        ) : (
            <Image
              style={styles.icon}
              source={require('./../../assets/images/arrow-back.png')}
            />
          )}

      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    padding: 20,
    flexDirection: 'row',
    width: '100%',
    marginVertical: 9,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  itemContainer: { flexDirection: 'row', alignItems: 'center' },
  text: {
    flex: 1,
    fontSize:appFonts.largeBold,
   
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  icon: {
    width: 8,
    height: 16,
  },
  iconY: {
    width: 16,
    height: 8,
  },
});

export default AppProjectDetailsMenuItem;
