import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  ImageBackground,
  TouchableWithoutFeedback,
} from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const AppVideoThumbView = ({imageUri, onPress}) => {
  return (
    <View style={styles.videoContainer}>
      <ImageBackground
        style={styles.videoThumbImg}
        source={{
          uri: imageUri,
        }}>
        <TouchableWithoutFeedback onPress={onPress}>
          <View style={styles.videoPlayContainer}>
            <Image source={require('./../../assets/images/play-w-fill.png')} />
          </View>
        </TouchableWithoutFeedback>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  videoContainer: {
    height: windowHeight / 3,
    width: '100%',
    marginTop: 20,
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  videoThumbImg: {
    flexGrow: 1,
    height: null,
    width: null,
    alignItems: 'center',
    justifyContent: 'center',
  },
  videoPlayContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
});

export default AppVideoThumbView;
