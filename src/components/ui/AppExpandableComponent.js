import React, {useEffect, useState} from 'react';
import {StyleSheet, View, TouchableOpacity, Image} from 'react-native';
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import AppText from './ AppText';

const AppExpandableComponent = ({item, onPress, onChildPress}) => {
  const [layoutHeight, setLayoutHeight] = useState(0);

  useEffect(() => {
    if (item.isExpanded) {
      setLayoutHeight(null);
    } else {
      setLayoutHeight(0);
    }
  }, [item.isExpanded]);

  return (
    <View style={styles.container}>
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={onPress}
        style={styles.header}>
        <View style={styles.headerItemContainer}>
          <AppText style={styles.headerText} numberOfLines={1}>
            {item.place_type} 
            {/* {item.details.length} */}
          </AppText>

          <Image
            style={styles.icon}
            source={
              item.isExpanded
                ? require('./../../assets/images/up-icon-b.png')
                : require('./../../assets/images/down-icon-b.png')
            }
          />
        </View>
      </TouchableOpacity>

      <View
        style={{
          height: layoutHeight,
          overflow: 'hidden',
        }}>
        {item.details &&
          item.details.length > 0 &&
          item.details.map((item, key) => (
            <TouchableOpacity
              activeOpacity={0.9}
              key={key}
              style={styles.content}
              onPress={() => onChildPress(item.title)}>
              <View style={styles.separator} />

              <View style={styles.childTextContainer}>
                <AppText style={styles.childTitle} numberOfLines={1}>
                  {item.title}
                </AppText>
                <AppText style={styles.childDistance} numberOfLines={1}>
                  {item.description}
                </AppText>
              </View>
            </TouchableOpacity>
          ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 22,
    flexDirection: 'column',
    width: '100%',
    marginVertical: 10,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  header: {
    paddingVertical: 20,
  },
  headerItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    flex: 1,
    fontSize: appFonts.xlargeFontSize,
    textTransform: 'capitalize',
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  icon: {
    width: 14,
    height: 14,
  },
  separator: {
    height: 1,
    backgroundColor: colors.lightGray,
  },
  childTextContainer: {
    flex: 1,
    paddingVertical: 20,
  },
  childTitle: {
    color: colors.expandText,
    fontSize: appFonts.largeBoldx,
    marginBottom: 10,
    textTransform: 'capitalize',
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  childDistance: {
    color: colors.secondary,
    fontSize: appFonts.largeFontSize,
    textTransform: 'capitalize',
    fontFamily: appFonts.SourceSansProRegular,
  },
});

export default AppExpandableComponent;
