import React, { useState } from 'react';
import {View, StyleSheet, Image, Dimensions, ActivityIndicator} from 'react-native';

import VideoPlayer from 'react-native-video-player';
import ImageZoom from 'react-native-image-pan-zoom';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const AppGalleryItem = ({item, index}) => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
              }}>
      {!item.field_media_type ||
      item.field_media_type === '' ||
      item.field_media_type === 'image' ? (
        <ImageZoom
          cropWidth={windowWidth}
          cropHeight={windowHeight}
          imageHeight={windowHeight / 3}
          imageWidth={windowWidth}
          enableSwipeDown={true}
          useHardwareTextureAndroid={true}
          pinchToZoom={true}>
          <Image
            style={{height: '100%', width: '100%'}}
            source={{
              uri: item.field_gallery_image_url
                ? item.field_gallery_image_url
                : item.banner_image_url
                ? item.banner_image_url
                : item.field_gallery_thumb_image_url,
            }}
          />
        </ImageZoom>
      ) : (
        <View
          style={{
            height: windowHeight / 4,
            width: windowWidth * 0.8,
            backgroundColor: 'floralwhite',
          }}>
          {item.field_media_type === 'video' && (
            <VideoPlayer
              video={{
                uri: item.field_gallery_image_url,
              }}
              thumbnail={{
                uri: item.field_gallery_thumb_image_url,
              }}
              autoplay={true}
              videoHeight={windowHeight / 4}
              videoWidth={windowWidth * 0.8}
              resizeMode="stretch"
              loop={false}
              defaultMuted={true}
            />
          )}
        </View>
      )}
      <ActivityIndicator
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            zIndex: -1
          }}
          size="large"
          animating={true}
        />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
});

export default AppGalleryItem;
