import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';

import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import AppText from './ AppText';

const AppMenuItemDownload = ({
  title,
  onPress,
  color = 'primary',
  textColor = 'secondary',
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[styles.button, {backgroundColor: colors[color]}]}
      onPress={onPress}>
      <View style={styles.itemContainer}>
      <Image
          style={styles.iconPdf}
          source={require('./../../assets/images/pdf-docment-b.png')}
        />

        <AppText
          style={[styles.text, {color: colors[textColor]}]}
          numberOfLines={1}>
          {title}
        </AppText>

        <Image
          style={styles.icon}
          source={require('./../../assets/images/download-b.png')}
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    padding: 15,
    flexDirection: 'row',
    width: '100%',
    marginVertical: 9,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  itemContainer: {flexDirection: 'row', alignItems: 'center'},
  text: {
    flex: 1,
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
    marginHorizontal: 10
  },
  iconPdf: {
    width: 29,
    height: 28,
  },
  icon: {
    width: 16,
    height: 16,
  },
});

export default AppMenuItemDownload;
