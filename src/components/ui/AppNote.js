import React from 'react';
import {View, StyleSheet} from 'react-native';
import AppText from './ AppText';
import colors from '../../config/colors';

const AppNote = ({label}) => {
  return (
    <View style={styles.container}>
      <AppText style={styles.txt}>{label}</AppText>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    paddingHorizontal: 12,
    paddingVertical: 18,
    backgroundColor: colors.LynxWhite,
    marginTop: 15,
    marginBottom: 15,
  },
  txt: {color: colors.jaguar, fontSize: 15, lineHeight: 24},
});

export default AppNote;
