import React from 'react';
import {StyleSheet, TouchableOpacity, View, Image} from 'react-native';
import colors from '../../config/colors';
import appFonts from '../../config/appFonts';
import AppText from './ AppText';

const AppSearchListFilterListItem = ({title, onPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={styles.button}
      onPress={onPress}>
      <View style={styles.itemContainer}>
        <AppText style={styles.text}>{title}</AppText>
        <Image
          style={styles.arrow}
          source={require('./../../assets/images/down-icon-b.png')}
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
    marginVertical: 10,
    marginRight: 18,
    paddingHorizontal: 20,
  },
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  arrow: {height: 12, width: 12, marginLeft: 10},
  text: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
    color: colors.jaguar,
  },
});

export default AppSearchListFilterListItem;
