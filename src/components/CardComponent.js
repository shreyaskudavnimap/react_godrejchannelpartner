import React from 'react';
import {View, Image, ImageBackground, TouchableOpacity} from 'react-native';
import AppTextBoldSmall from './ui/AppTextBoldSmall';
import AppTextGray from './ui/AppTextGray';

const CardComponent = (props) => {
  return (
    <View style={{width: 200, marginRight: 30}}>
      <View>
        <ImageBackground
          source={{uri: props.image}}
          style={{height: 200, width: '100%'}}>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-end',
              zIndex: 99,
            }}>
            <Image source={require('../assets/images/like.png')} />
          </TouchableOpacity>
        </ImageBackground>
      </View>

      <View style={{flexDirection: 'row', marginTop: '3%'}}>
        <AppTextBoldSmall>{props.name} </AppTextBoldSmall>
        <AppTextGray>{props.city}</AppTextGray>
      </View>
      <AppTextGray>$62 Lack onwards </AppTextGray>
    </View>
  );
};
export default CardComponent;
