import React, { useState } from "react";
import {
    Modal,
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableOpacity, FlatList, Image, TextInput, SafeAreaView, ScrollView 
} from "react-native";
import colors from '../config/colors';
import AppTextBold from "./ui/AppTextBold";
import { SvgUri } from 'react-native-svg';
import appFonts from '../config/appFonts';
var { height, width } = Dimensions.get('window');

let countryCodes = [
    {
        name: "India",
        callingCodes: "+91",
        alpha3Code: "IND",
        flag: "https://restcountries.eu/data/ind.svg",
    },
    {
        name: "Afghanistan",
        callingCodes: "+93",
        alpha3Code: "AFG",
        flag: "https://restcountries.eu/data/afg.svg",
    },
    {
        name: "Åland Islands",
        callingCodes: "+358",
        alpha3Code: "ALA",
        flag: "https://restcountries.eu/data/ala.svg",
    },
    {
        name: "Albania",
        callingCodes: "+355",
        alpha3Code: "ALB",
        flag: "https://restcountries.eu/data/alb.svg",
    },
    {
        name: "Algeria",
        callingCodes: "+213",
        alpha3Code: "DZA",
        flag: "https://restcountries.eu/data/dza.svg",
    },
    {
        name: "American Samoa",
        callingCodes: "+1684",
        alpha3Code: "ASM",
        flag: "https://restcountries.eu/data/asm.svg",
    },
    {
        name: "Andorra",
        callingCodes: "+376",
        alpha3Code: "AND",
        flag: "https://restcountries.eu/data/and.svg",
    },
    {
        name: "Angola",
        callingCodes: "+244",
        alpha3Code: "AGO",
        flag: "https://restcountries.eu/data/ago.svg",
    },
    {
        name: "Anguilla",
        callingCodes: "+1264",
        alpha3Code: "AIA",
        flag: "https://restcountries.eu/data/aia.svg",
    },
    {
        name: "Antarctica",
        callingCodes: "+672",
        alpha3Code: "ATA",
        flag: "https://restcountries.eu/data/ata.svg",
    },
    {
        name: "Antigua and Barbuda",
        callingCodes: "+1268",
        alpha3Code: "ATG",
        flag: "https://restcountries.eu/data/atg.svg",
    },

    {
        name: "Argentina",
        callingCodes: "+54",
        alpha3Code: "ARG",
        flag: "https://restcountries.eu/data/arg.svg",
    },
    {
        name: "Armenia",
        callingCodes: "+374",
        alpha3Code: "ARM",
        flag: "https://restcountries.eu/data/arm.svg",
    },
    {
        name: "Aruba",
        callingCodes: "+297",
        alpha3Code: "ABW",
        flag: "https://restcountries.eu/data/abw.svg",
    },
    {
        name: "Australia",
        callingCodes: "+61",
        alpha3Code: "AUS",
        flag: "https://restcountries.eu/data/aus.svg",
    },
    {
        name: "Austria",
        callingCodes: "+43",
        alpha3Code: "AUT",
        flag: "https://restcountries.eu/data/aut.svg",
    },
    {
        name: "Azerbaijan",
        callingCodes: "+994",
        alpha3Code: "AZE",
        flag: "https://restcountries.eu/data/aze.svg",
    },
    {
        name: "Bahamas",
        callingCodes: "+1242",
        alpha3Code: "BHS",
        flag: "https://restcountries.eu/data/bhs.svg",
    },
    {
        name: "Bahrain",
        callingCodes: "+973",
        alpha3Code: "BHR",
        flag: "https://restcountries.eu/data/bhr.svg",
    },
    {
        name: "Bangladesh",
        callingCodes: "+880",
        alpha3Code: "BGD",
        flag: "https://restcountries.eu/data/bgd.svg",
    },
    {
        name: "Barbados",
        callingCodes: "+1246",
        alpha3Code: "BRB",
        flag: "https://restcountries.eu/data/brb.svg",
    },
    {
        name: "Belarus",
        callingCodes: "+375",
        alpha3Code: "BLR",
        flag: "https://restcountries.eu/data/blr.svg",
    },
    {
        name: "Belgium",
        callingCodes: "+32",
        alpha3Code: "BEL",
        flag: "https://restcountries.eu/data/bel.svg",
    },
    {
        name: "Belize",
        callingCodes: "+501",
        alpha3Code: "BLZ",
        flag: "https://restcountries.eu/data/blz.svg",
    },
    {
        name: "Benin",
        callingCodes: "+229",
        alpha3Code: "BEN",
        flag: "https://restcountries.eu/data/ben.svg",
    },
    {
        name: "Bermuda",
        callingCodes: "+1441",
        alpha3Code: "BMU",
        flag: "https://restcountries.eu/data/bmu.svg",
    },
    {
        name: "Bhutan",
        callingCodes: "+975",
        alpha3Code: "BTN",
        flag: "https://restcountries.eu/data/btn.svg",
    },
    {
        name: "Bolivia (Plurinational State of)",
        callingCodes: "+591",
        alpha3Code: "BOL",
        flag: "https://restcountries.eu/data/bol.svg",
    },
    {
        name: "Bonaire, Sint Eustatius and Saba",
        callingCodes: "+5997",
        alpha3Code: "BES",
        flag: "https://restcountries.eu/data/bes.svg",
    },
    {
        name: "Bosnia and Herzegovina",
        callingCodes: "+387",
        alpha3Code: "BIH",
        flag: "https://restcountries.eu/data/bih.svg",
    },
    {
        name: "Botswana",
        callingCodes: "+267",
        alpha3Code: "BWA",
        flag: "https://restcountries.eu/data/bwa.svg",
    },
    {
        name: "Brazil",
        callingCodes: "+55",
        alpha3Code: "BRA",
        flag: "https://restcountries.eu/data/bra.svg",
    },
    {
        name: "British Indian Ocean Territory",
        callingCodes: "+246",
        alpha3Code: "IOT",
        flag: "https://restcountries.eu/data/iot.svg",
    },
    {
        name: "Virgin Islands (British)",
        callingCodes: "+1284",
        alpha3Code: "VGB",
        flag: "https://restcountries.eu/data/vgb.svg",
    },
    {
        name: "Virgin Islands (U.S.)",
        callingCodes: "+1 340",
        alpha3Code: "VIR",
        flag: "https://restcountries.eu/data/vir.svg",
    },
    {
        name: "Brunei Darussalam",
        callingCodes: "+673",
        alpha3Code: "BRN",
        flag: "https://restcountries.eu/data/brn.svg",
    },
    {
        name: "Bulgaria",
        callingCodes: "+359",
        alpha3Code: "BGR",
        flag: "https://restcountries.eu/data/bgr.svg",
    },
    {
        name: "Burkina Faso",
        callingCodes: "+226",
        alpha3Code: "BFA",
        flag: "https://restcountries.eu/data/bfa.svg",
    },
    {
        name: "Burundi",
        callingCodes: "+257",
        alpha3Code: "BDI",
        flag: "https://restcountries.eu/data/bdi.svg",
    },
    {
        name: "Cambodia",
        callingCodes: "+855",
        alpha3Code: "KHM",
        flag: "https://restcountries.eu/data/khm.svg",
    },
    {
        name: "Cameroon",
        callingCodes: "+237",
        alpha3Code: "CMR",
        flag: "https://restcountries.eu/data/cmr.svg",
    },
    {
        name: "Canada",
        callingCodes: "+1",
        alpha3Code: "CAN",
        flag: "https://restcountries.eu/data/can.svg",
    },
    {
        name: "Cabo Verde",
        callingCodes: "238",
        alpha3Code: "CPV",
        flag: "https://restcountries.eu/data/cpv.svg",
    },
    {
        name: "Cayman Islands",
        callingCodes: "+1345",
        alpha3Code: "CYM",
        flag: "https://restcountries.eu/data/cym.svg",
    },
    {
        name: "Central African Republic",
        callingCodes: "+236",
        alpha3Code: "CAF",
        flag: "https://restcountries.eu/data/caf.svg",
    },
    {
        name: "Chad",
        callingCodes: "+235",
        alpha3Code: "TCD",
        flag: "https://restcountries.eu/data/tcd.svg",
    },
    {
        name: "Chile",
        callingCodes: "+56",
        alpha3Code: "CHL",
        flag: "https://restcountries.eu/data/chl.svg",
    },
    {
        name: "China",
        callingCodes: "+86",
        alpha3Code: "CHN",
        flag: "https://restcountries.eu/data/chn.svg",
    },
    {
        name: "Christmas Island",
        callingCodes: "+61",
        alpha3Code: "CXR",
        flag: "https://restcountries.eu/data/cxr.svg",
    },
    {
        name: "Colombia",
        callingCodes: "+57",
        alpha3Code: "COL",
        flag: "https://restcountries.eu/data/col.svg",
    },
    {
        name: "Comoros",
        callingCodes: "+269",
        alpha3Code: "COM",
        flag: "https://restcountries.eu/data/com.svg",
    },
    {
        name: "Congo",
        callingCodes: "+242",
        alpha3Code: "COG",
        flag: "https://restcountries.eu/data/cog.svg",
    },
    {
        name: "Congo (Democratic Republic of the)",
        callingCodes: "+243",
        alpha3Code: "COD",
        flag: "https://restcountries.eu/data/cod.svg",
    },
    {
        name: "Cook Islands",
        callingCodes: "+682",
        alpha3Code: "COK",
        flag: "https://restcountries.eu/data/cok.svg",
    },
    {
        name: "Costa Rica",
        callingCodes: "+506",
        alpha3Code: "CRI",
        flag: "https://restcountries.eu/data/cri.svg",
    },
    {
        name: "Croatia",
        callingCodes: "+385",
        alpha3Code: "HRV",
        flag: "https://restcountries.eu/data/hrv.svg",
    },
    {
        name: "Cuba",
        callingCodes: "+53",
        alpha3Code: "CUB",
        flag: "https://restcountries.eu/data/cub.svg",
    },
    {
        name: "Curaçao",
        callingCodes: "+599",
        alpha3Code: "CUW",
        flag: "https://restcountries.eu/data/cuw.svg",
    },
    {
        name: "Cyprus",
        callingCodes: "+357",
        alpha3Code: "CYP",
        flag: "https://restcountries.eu/data/cyp.svg",
    },
    {
        name: "Czech Republic",
        callingCodes: "+420",
        alpha3Code: "CZE",
        flag: "https://restcountries.eu/data/cze.svg",
    },
    {
        name: "Denmark",
        callingCodes: "+45",
        alpha3Code: "DNK",
        flag: "https://restcountries.eu/data/dnk.svg",
    },
    {
        name: "Djibouti",
        callingCodes: "+253",
        alpha3Code: "DJI",
        flag: "https://restcountries.eu/data/dji.svg",
    },
    {
        name: "Dominica",
        callingCodes: "+1767",
        alpha3Code: "DMA",
        flag: "https://restcountries.eu/data/dma.svg",
    },
    {
        name: "Dominican Republic",
        callingCodes: "+1809",
        alpha3Code: "DOM",
        flag: "https://restcountries.eu/data/dom.svg",
    },
    {
        name: "Ecuador",
        callingCodes: "+593",
        alpha3Code: "ECU",
        flag: "https://restcountries.eu/data/ecu.svg",
    },
    {
        name: "Egypt",
        callingCodes: "+20",
        alpha3Code: "EGY",
        flag: "https://restcountries.eu/data/egy.svg",
    },
    {
        name: "El Salvador",
        callingCodes: "+503",
        alpha3Code: "SLV",
        flag: "https://restcountries.eu/data/slv.svg",
    },
    {
        name: "Equatorial Guinea",
        callingCodes: "+240",
        alpha3Code: "GNQ",
        flag: "https://restcountries.eu/data/gnq.svg",
    },
    {
        name: "Eritrea",
        callingCodes: "+291",
        alpha3Code: "ERI",
        flag: "https://restcountries.eu/data/eri.svg",
    },
    {
        name: "Estonia",
        callingCodes: "+372",
        alpha3Code: "EST",
        flag: "https://restcountries.eu/data/est.svg",
    },
    {
        name: "Ethiopia",
        callingCodes: "+251",
        alpha3Code: "ETH",
        flag: "https://restcountries.eu/data/eth.svg",
    },
    {
        name: "Falkland Islands (Malvinas)",
        callingCodes: "+500",
        alpha3Code: "FLK",
        flag: "https://restcountries.eu/data/flk.svg",
    },
    {
        name: "Faroe Islands",
        callingCodes: "+298",
        alpha3Code: "FRO",
        flag: "https://restcountries.eu/data/fro.svg",
    },
    {
        name: "Fiji",
        callingCodes: "+679",
        alpha3Code: "FJI",
        flag: "https://restcountries.eu/data/fji.svg",
    },
    {
        name: "Finland",
        callingCodes: "+358",
        alpha3Code: "FIN",
        flag: "https://restcountries.eu/data/fin.svg",
    },
    {
        name: "France",
        callingCodes: "+33",
        alpha3Code: "FRA",
        flag: "https://restcountries.eu/data/fra.svg",
    },
    {
        name: "French Guiana",
        callingCodes: "+594",
        alpha3Code: "GUF",
        flag: "https://restcountries.eu/data/guf.svg",
    },
    {
        name: "French Polynesia",
        callingCodes: "+689",
        alpha3Code: "PYF",
        flag: "https://restcountries.eu/data/pyf.svg",
    },

    {
        name: "Gabon",
        callingCodes: "241",
        alpha3Code: "GAB",
        flag: "https://restcountries.eu/data/gab.svg",
    },
    {
        name: "Gambia",
        callingCodes: "+220",
        alpha3Code: "GMB",
        flag: "https://restcountries.eu/data/gmb.svg",
    },
    {
        name: "Georgia",
        callingCodes: "+995",
        alpha3Code: "GEO",
        flag: "https://restcountries.eu/data/geo.svg",
    },
    {
        name: "Germany",
        callingCodes: "+49",
        alpha3Code: "DEU",
        flag: "https://restcountries.eu/data/deu.svg",
    },
    {
        name: "Ghana",
        callingCodes: "+233",
        alpha3Code: "GHA",
        flag: "https://restcountries.eu/data/gha.svg",
    },
    {
        name: "Gibraltar",
        callingCodes: "+350",
        alpha3Code: "GIB",
        flag: "https://restcountries.eu/data/gib.svg",
    },
    {
        name: "Greece",
        callingCodes: "+30",
        alpha3Code: "GRC",
        flag: "https://restcountries.eu/data/grc.svg",
    },
    {
        name: "Greenland",
        callingCodes: "+299",
        alpha3Code: "GRL",
        flag: "https://restcountries.eu/data/grl.svg",
    },
    {
        name: "Grenada",
        callingCodes: "+1473",
        alpha3Code: "GRD",
        flag: "https://restcountries.eu/data/grd.svg",
    },
    {
        name: "Guadeloupe",
        callingCodes: "590",
        alpha3Code: "GLP",
        flag: "https://restcountries.eu/data/glp.svg",
    },
    {
        name: "Guam",
        callingCodes: "+1671",
        alpha3Code: "GUM",
        flag: "https://restcountries.eu/data/gum.svg",
    },
    {
        name: "Guatemala",
        callingCodes: "+502",
        alpha3Code: "GTM",
        flag: "https://restcountries.eu/data/gtm.svg",
    },
    {
        name: "Guernsey",
        callingCodes: "+44",
        alpha3Code: "GGY",
        flag: "https://restcountries.eu/data/ggy.svg",
    },
    {
        name: "Guinea",
        callingCodes: "+224",
        alpha3Code: "GIN",
        flag: "https://restcountries.eu/data/gin.svg",
    },
    {
        name: "Guinea-Bissau",
        callingCodes: "+245",
        alpha3Code: "GNB",
        flag: "https://restcountries.eu/data/gnb.svg",
    },
    {
        name: "Guyana",
        callingCodes: "+592",
        alpha3Code: "GUY",
        flag: "https://restcountries.eu/data/guy.svg",
    },
    {
        name: "Haiti",
        callingCodes: "+509",
        alpha3Code: "HTI",
        flag: "https://restcountries.eu/data/hti.svg",
    },

    {
        name: "Holy See",
        callingCodes: "+379",
        alpha3Code: "VAT",
        flag: "https://restcountries.eu/data/vat.svg",
    },
    {
        name: "Honduras",
        callingCodes: "+504",
        alpha3Code: "HND",
        flag: "https://restcountries.eu/data/hnd.svg",
    },
    {
        name: "Hong Kong",
        callingCodes: "+852",
        alpha3Code: "HKG",
        flag: "https://restcountries.eu/data/hkg.svg",
    },
    {
        name: "Hungary",
        callingCodes: "+36",
        alpha3Code: "HUN",
        flag: "https://restcountries.eu/data/hun.svg",
    },
    {
        name: "Iceland",
        callingCodes: "+354",
        alpha3Code: "ISL",
        flag: "https://restcountries.eu/data/isl.svg",
    },

    {
        name: "Indonesia",
        callingCodes: "+62",
        alpha3Code: "IDN",
        flag: "https://restcountries.eu/data/idn.svg",
    },
    {
        name: "Côte d'Ivoire",
        callingCodes: "+225",
        alpha3Code: "CIV",
        flag: "https://restcountries.eu/data/civ.svg",
    },
    {
        name: "Iran (Islamic Republic of)",
        callingCodes: "+98",
        alpha3Code: "IRN",
        flag: "https://restcountries.eu/data/irn.svg",
    },
    {
        name: "Iraq",
        callingCodes: "+964",
        alpha3Code: "IRQ",
        flag: "https://restcountries.eu/data/irq.svg",
    },
    {
        name: "Ireland",
        callingCodes: "+353",
        alpha3Code: "IRL",
        flag: "https://restcountries.eu/data/irl.svg",
    },
    {
        name: "Isle of Man",
        callingCodes: "+44",
        alpha3Code: "IMN",
        flag: "https://restcountries.eu/data/imn.svg",
    },
    {
        name: "Israel",
        callingCodes: "+972",
        alpha3Code: "ISR",
        flag: "https://restcountries.eu/data/isr.svg",
    },
    {
        name: "Italy",
        callingCodes: "+39",
        alpha3Code: "ITA",
        flag: "https://restcountries.eu/data/ita.svg",
    },
    {
        name: "Jamaica",
        callingCodes: "+1876",
        alpha3Code: "JAM",
        flag: "https://restcountries.eu/data/jam.svg",
    },
    {
        name: "Japan",
        callingCodes: "+81",
        alpha3Code: "JPN",
        flag: "https://restcountries.eu/data/jpn.svg",
    },
    {
        name: "Jersey",
        callingCodes: "+44",
        alpha3Code: "JEY",
        flag: "https://restcountries.eu/data/jey.svg",
    },
    {
        name: "Jordan",
        callingCodes: "+962",
        alpha3Code: "JOR",
        flag: "https://restcountries.eu/data/jor.svg",
    },
    {
        name: "Kazakhstan",
        callingCodes: "+76",
        alpha3Code: "KAZ",
        flag: "https://restcountries.eu/data/kaz.svg",
    },
    {
        name: "Kenya",
        callingCodes: "+254",
        alpha3Code: "KEN",
        flag: "https://restcountries.eu/data/ken.svg",
    },
    {
        name: "Kiribati",
        callingCodes: "+686",
        alpha3Code: "KIR",
        flag: "https://restcountries.eu/data/kir.svg",
    },
    {
        name: "Kuwait",
        callingCodes: "+965",
        alpha3Code: "KWT",
        flag: "https://restcountries.eu/data/kwt.svg",
    },
    {
        name: "Kyrgyzstan",
        callingCodes: "+996",
        alpha3Code: "KGZ",
        flag: "https://restcountries.eu/data/kgz.svg",
    },
    {
        name: "Lao People's Democratic Republic",
        callingCodes: "+856",
        alpha3Code: "LAO",
        flag: "https://restcountries.eu/data/lao.svg",
    },
    {
        name: "Latvia",
        callingCodes: "+371",
        alpha3Code: "LVA",
        flag: "https://restcountries.eu/data/lva.svg",
    },
    {
        name: "Lebanon",
        callingCodes: "+961",
        alpha3Code: "LBN",
        flag: "https://restcountries.eu/data/lbn.svg",
    },
    {
        name: "Lesotho",
        callingCodes: "+266",
        alpha3Code: "LSO",
        flag: "https://restcountries.eu/data/lso.svg",
    },
    {
        name: "Liberia",
        callingCodes: "+231",
        alpha3Code: "LBR",
        flag: "https://restcountries.eu/data/lbr.svg",
    },
    {
        name: "Libya",
        callingCodes: "+218",
        alpha3Code: "LBY",
        flag: "https://restcountries.eu/data/lby.svg",
    },
    {
        name: "Liechtenstein",
        callingCodes: "+423",
        alpha3Code: "LIE",
        flag: "https://restcountries.eu/data/lie.svg",
    },
    {
        name: "Lithuania",
        callingCodes: "+370",
        alpha3Code: "LTU",
        flag: "https://restcountries.eu/data/ltu.svg",
    },
    {
        name: "Luxembourg",
        callingCodes: "+352",
        alpha3Code: "LUX",
        flag: "https://restcountries.eu/data/lux.svg",
    },
    {
        name: "Macao",
        callingCodes: "+853",
        alpha3Code: "MAC",
        flag: "https://restcountries.eu/data/mac.svg",
    },
    {
        name: "Macedonia (the former Yugoslav Republic of)",
        callingCodes: "+389",
        alpha3Code: "MKD",
        flag: "https://restcountries.eu/data/mkd.svg",
    },
    {
        name: "Madagascar",
        callingCodes: "+261",
        alpha3Code: "MDG",
        flag: "https://restcountries.eu/data/mdg.svg",
    },
    {
        name: "Malawi",
        callingCodes: "+265",
        alpha3Code: "MWI",
        flag: "https://restcountries.eu/data/mwi.svg",
    },
    {
        name: "Malaysia",
        callingCodes: "+60",
        alpha3Code: "MYS",
        flag: "https://restcountries.eu/data/mys.svg",
    },
    {
        name: "Maldives",
        callingCodes: "+960",
        alpha3Code: "MDV",
        flag: "https://restcountries.eu/data/mdv.svg",
    },
    {
        name: "Mali",
        callingCodes: "+223",
        alpha3Code: "MLI",
        flag: "https://restcountries.eu/data/mli.svg",
    },
    {
        name: "Malta",
        callingCodes: "+356",
        alpha3Code: "MLT",
        flag: "https://restcountries.eu/data/mlt.svg",
    },
    {
        name: "Marshall Islands",
        callingCodes: "+692",
        alpha3Code: "MHL",
        flag: "https://restcountries.eu/data/mhl.svg",
    },
    {
        name: "Martinique",
        callingCodes: "+596",
        alpha3Code: "MTQ",
        flag: "https://restcountries.eu/data/mtq.svg",
    },
    {
        name: "Mauritania",
        callingCodes: "+222",
        alpha3Code: "MRT",
        flag: "https://restcountries.eu/data/mrt.svg",
    },
    {
        name: "Mauritius",
        callingCodes: "+230",
        alpha3Code: "MUS",
        flag: "https://restcountries.eu/data/mus.svg",
    },
    {
        name: "Mayotte",
        callingCodes: "+262",
        alpha3Code: "MYT",
        flag: "https://restcountries.eu/data/myt.svg",
    },
    {
        name: "Mexico",
        callingCodes: "+52",
        alpha3Code: "MEX",
        flag: "https://restcountries.eu/data/mex.svg",
    },
    {
        name: "Micronesia (Federated States of)",
        callingCodes: "+691",
        alpha3Code: "FSM",
        flag: "https://restcountries.eu/data/fsm.svg",
    },
    {
        name: "Moldova (Republic of)",
        callingCodes: "+373",
        alpha3Code: "MDA",
        flag: "https://restcountries.eu/data/mda.svg",
    },
    {
        name: "Monaco",
        callingCodes: "+377",
        alpha3Code: "MCO",
        flag: "https://restcountries.eu/data/mco.svg",
    },
    {
        name: "Mongolia",
        callingCodes: "+976",
        alpha3Code: "MNG",
        flag: "https://restcountries.eu/data/mng.svg",
    },
    {
        name: "Montenegro",
        callingCodes: "+382",
        alpha3Code: "MNE",
        flag: "https://restcountries.eu/data/mne.svg",
    },
    {
        name: "Montserrat",
        callingCodes: "+1664",
        alpha3Code: "MSR",
        flag: "https://restcountries.eu/data/msr.svg",
    },
    {
        name: "Morocco",
        callingCodes: "+212",
        alpha3Code: "MAR",
        flag: "https://restcountries.eu/data/mar.svg",
    },
    {
        name: "Mozambique",
        callingCodes: "+258",
        alpha3Code: "MOZ",
        flag: "https://restcountries.eu/data/moz.svg",
    },
    {
        name: "Myanmar",
        callingCodes: "+95",
        alpha3Code: "MMR",
        flag: "https://restcountries.eu/data/mmr.svg",
    },
    {
        name: "Namibia",
        callingCodes: "+264",
        alpha3Code: "NAM",
        flag: "https://restcountries.eu/data/nam.svg",
    },
    {
        name: "Nauru",
        callingCodes: "+674",
        alpha3Code: "NRU",
        flag: "https://restcountries.eu/data/nru.svg",
    },
    {
        name: "Nepal",
        callingCodes: "+977",
        alpha3Code: "NPL",
        flag: "https://restcountries.eu/data/npl.svg",
    },
    {
        name: "Netherlands",
        callingCodes: "+31",
        alpha3Code: "NLD",
        flag: "https://restcountries.eu/data/nld.svg",
    },
    {
        name: "New Caledonia",
        callingCodes: "+687",
        alpha3Code: "NCL",
        flag: "https://restcountries.eu/data/ncl.svg",
    },
    {
        name: "New Zealand",
        callingCodes: "+64",
        alpha3Code: "NZL",
        flag: "https://restcountries.eu/data/nzl.svg",
    },
    {
        name: "Nicaragua",
        callingCodes: "+505",
        alpha3Code: "NIC",
        flag: "https://restcountries.eu/data/nic.svg",
    },
    {
        name: "Niger",
        callingCodes: "+227",
        alpha3Code: "NER",
        flag: "https://restcountries.eu/data/ner.svg",
    },
    {
        name: "Nigeria",
        callingCodes: "+234",
        alpha3Code: "NGA",
        flag: "https://restcountries.eu/data/nga.svg",
    },
    {
        name: "Niue",
        callingCodes: "+683",
        alpha3Code: "NIU",
        flag: "https://restcountries.eu/data/niu.svg",
    },
    {
        name: "Norfolk Island",
        callingCodes: "+672",
        alpha3Code: "NFK",
        flag: "https://restcountries.eu/data/nfk.svg",
    },
    {
        name: "Korea (Democratic People's Republic of)",
        callingCodes: "+850",
        alpha3Code: "PRK",
        flag: "https://restcountries.eu/data/prk.svg",
    },
    {
        name: "Northern Mariana Islands",
        callingCodes: "+1670",
        alpha3Code: "MNP",
        flag: "https://restcountries.eu/data/mnp.svg",
    },
    {
        name: "Norway",
        callingCodes: "+47",
        alpha3Code: "NOR",
        flag: "https://restcountries.eu/data/nor.svg",
    },
    {
        name: "Oman",
        callingCodes: "+968",
        alpha3Code: "OMN",
        flag: "https://restcountries.eu/data/omn.svg",
    },
    {
        name: "Pakistan",
        callingCodes: "+92",
        alpha3Code: "PAK",
        flag: "https://restcountries.eu/data/pak.svg",
    },
    {
        name: "Palau",
        callingCodes: "+680",
        alpha3Code: "PLW",
        flag: "https://restcountries.eu/data/plw.svg",
    },
    {
        name: "Palestine, State of",
        callingCodes: "+970",
        alpha3Code: "PSE",
        flag: "https://restcountries.eu/data/pse.svg",
    },
    {
        name: "Panama",
        callingCodes: "+507",
        alpha3Code: "PAN",
        flag: "https://restcountries.eu/data/pan.svg",
    },
    {
        name: "Papua New Guinea",
        callingCodes: "+675",
        alpha3Code: "PNG",
        flag: "https://restcountries.eu/data/png.svg",
    },
    {
        name: "Paraguay",
        callingCodes: "+595",
        alpha3Code: "PRY",
        flag: "https://restcountries.eu/data/pry.svg",
    },
    {
        name: "Peru",
        callingCodes: "+51",
        alpha3Code: "PER",
        flag: "https://restcountries.eu/data/per.svg",
    },
    {
        name: "Philippines",
        callingCodes: "+63",
        alpha3Code: "PHL",
        flag: "https://restcountries.eu/data/phl.svg",
    },
    {
        name: "Pitcairn",
        callingCodes: "+64",
        alpha3Code: "PCN",
        flag: "https://restcountries.eu/data/pcn.svg",
    },
    {
        name: "Poland",
        callingCodes: "+48",
        alpha3Code: "POL",
        flag: "https://restcountries.eu/data/pol.svg",
    },
    {
        name: "Portugal",
        callingCodes: "+351",
        alpha3Code: "PRT",
        flag: "https://restcountries.eu/data/prt.svg",
    },
    {
        name: "Puerto Rico",
        callingCodes: "+1787",
        alpha3Code: "PRI",
        flag: "https://restcountries.eu/data/pri.svg",
    },
    {
        name: "Qatar",
        callingCodes: "+974",
        alpha3Code: "QAT",
        flag: "https://restcountries.eu/data/qat.svg",
    },
    {
        name: "Republic of Kosovo",
        callingCodes: "+383",
        alpha3Code: "KOS",
        flag: "https://restcountries.eu/data/kos.svg",
    },
    {
        name: "Réunion",
        callingCodes: "+262",
        alpha3Code: "REU",
        flag: "https://restcountries.eu/data/reu.svg",
    },
    {
        name: "Romania",
        callingCodes: "+40",
        alpha3Code: "ROU",
        flag: "https://restcountries.eu/data/rou.svg",
    },
    {
        name: "Russian Federation",
        callingCodes: "+7",
        alpha3Code: "RUS",
        flag: "https://restcountries.eu/data/rus.svg",
    },
    {
        name: "Rwanda",
        callingCodes: "+250",
        alpha3Code: "RWA",
        flag: "https://restcountries.eu/data/rwa.svg",
    },
    {
        name: "Saint Barthélemy",
        callingCodes: "+590",
        alpha3Code: "BLM",
        flag: "https://restcountries.eu/data/blm.svg",
    },
    {
        name: "Saint Helena, Ascension and Tristan da Cunha",
        callingCodes: "+290",
        alpha3Code: "SHN",
        flag: "https://restcountries.eu/data/shn.svg",
    },
    {
        name: "Saint Kitts and Nevis",
        callingCodes: "+1869",
        alpha3Code: "KNA",
        flag: "https://restcountries.eu/data/kna.svg",
    },
    {
        name: "Saint Lucia",
        callingCodes: "+1758",
        alpha3Code: "LCA",
        flag: "https://restcountries.eu/data/lca.svg",
    },
    {
        name: "Saint Martin (French part)",
        callingCodes: "+590",
        alpha3Code: "MAF",
        flag: "https://restcountries.eu/data/maf.svg",
    },
    {
        name: "Saint Pierre and Miquelon",
        callingCodes: "+508",
        alpha3Code: "SPM",
        flag: "https://restcountries.eu/data/spm.svg",
    },
    {
        name: "Saint Vincent and the Grenadines",
        callingCodes: "+1784",
        alpha3Code: "VCT",
        flag: "https://restcountries.eu/data/vct.svg",
    },
    {
        name: "Samoa",
        callingCodes: "+685",
        alpha3Code: "WSM",
        flag: "https://restcountries.eu/data/wsm.svg",
    },
    {
        name: "San Marino",
        callingCodes: "+378",
        alpha3Code: "SMR",
        flag: "https://restcountries.eu/data/smr.svg",
    },
    {
        name: "Sao Tome and Principe",
        callingCodes: "+239",
        alpha3Code: "STP",
        flag: "https://restcountries.eu/data/stp.svg",
    },
    {
        name: "Saudi Arabia",
        callingCodes: "+966",
        alpha3Code: "SAU",
        flag: "https://restcountries.eu/data/sau.svg",
    },
    {
        name: "Senegal",
        callingCodes: "+221",
        alpha3Code: "SEN",
        flag: "https://restcountries.eu/data/sen.svg",
    },
    {
        name: "Serbia",
        callingCodes: "+381",
        alpha3Code: "SRB",
        flag: "https://restcountries.eu/data/srb.svg",
    },
    {
        name: "Seychelles",
        callingCodes: "+248",
        alpha3Code: "SYC",
        flag: "https://restcountries.eu/data/syc.svg",
    },
    {
        name: "Sierra Leone",
        callingCodes: "+232",
        alpha3Code: "SLE",
        flag: "https://restcountries.eu/data/sle.svg",
    },
    {
        name: "Singapore",
        callingCodes: "+65",
        alpha3Code: "SGP",
        flag: "https://restcountries.eu/data/sgp.svg",
    },
    {
        name: "Sint Maarten (Dutch part)",
        callingCodes: "+1721",
        alpha3Code: "SXM",
        flag: "https://restcountries.eu/data/sxm.svg",
    },
    {
        name: "Slovakia",
        callingCodes: "+421",
        alpha3Code: "SVK",
        flag: "https://restcountries.eu/data/svk.svg",
    },
    {
        name: "Slovenia",
        callingCodes: "+386",
        alpha3Code: "SVN",
        flag: "https://restcountries.eu/data/svn.svg",
    },
    {
        name: "Solomon Islands",
        callingCodes: "+677",
        alpha3Code: "SLB",
        flag: "https://restcountries.eu/data/slb.svg",
    },
    {
        name: "Somalia",
        callingCodes: "+252",
        alpha3Code: "SOM",
        flag: "https://restcountries.eu/data/som.svg",
    },
    {
        name: "South Africa",
        callingCodes: "+27",
        alpha3Code: "ZAF",
        flag: "https://restcountries.eu/data/zaf.svg",
    },
    {
        name: "South Georgia and the South Sandwich Islands",
        callingCodes: "+500",
        alpha3Code: "SGS",
        flag: "https://restcountries.eu/data/sgs.svg",
    },
    {
        name: "Korea (Republic of)",
        callingCodes: "+82",
        alpha3Code: "KOR",
        flag: "https://restcountries.eu/data/kor.svg",
    },
    {
        name: "South Sudan",
        callingCodes: "+211",
        alpha3Code: "SSD",
        flag: "https://restcountries.eu/data/ssd.svg",
    },
    {
        name: "Spain",
        callingCodes: "+34",
        alpha3Code: "ESP",
        flag: "https://restcountries.eu/data/esp.svg",
    },
    {
        name: "Sri Lanka",
        callingCodes: "+94",
        alpha3Code: "LKA",
        flag: "https://restcountries.eu/data/lka.svg",
    },
    {
        name: "Sudan",
        callingCodes: "+249",
        alpha3Code: "SDN",
        flag: "https://restcountries.eu/data/sdn.svg",
    },
    {
        name: "Suriname",
        callingCodes: "+597",
        alpha3Code: "SUR",
        flag: "https://restcountries.eu/data/sur.svg",
    },
    {
        name: "Svalbard and Jan Mayen",
        callingCodes: "+4779",
        alpha3Code: "SJM",
        flag: "https://restcountries.eu/data/sjm.svg",
    },
    {
        name: "Swaziland",
        callingCodes: "+268",
        alpha3Code: "SWZ",
        flag: "https://restcountries.eu/data/swz.svg",
    },
    {
        name: "Sweden",
        callingCodes: "+46",
        alpha3Code: "SWE",
        flag: "https://restcountries.eu/data/swe.svg",
    },
    {
        name: "Switzerland",
        callingCodes: "+41",
        alpha3Code: "CHE",
        flag: "https://restcountries.eu/data/che.svg",
    },
    {
        name: "Syrian Arab Republic",
        callingCodes: "+963",
        alpha3Code: "SYR",
        flag: "https://restcountries.eu/data/syr.svg",
    },
    {
        name: "Taiwan",
        callingCodes: "+886",
        alpha3Code: "TWN",
        flag: "https://restcountries.eu/data/twn.svg",
    },
    {
        name: "Tajikistan",
        callingCodes: "+992",
        alpha3Code: "TJK",
        flag: "https://restcountries.eu/data/tjk.svg",
    },
    {
        name: "Tanzania, United Republic of",
        callingCodes: "+255",
        alpha3Code: "TZA",
        flag: "https://restcountries.eu/data/tza.svg",
    },
    {
        name: "Thailand",
        callingCodes: "+66",
        alpha3Code: "THA",
        flag: "https://restcountries.eu/data/tha.svg",
    },
    {
        name: "Timor-Leste",
        callingCodes: "+670",
        alpha3Code: "TLS",
        flag: "https://restcountries.eu/data/tls.svg",
    },
    {
        name: "Togo",
        callingCodes: "+228",
        alpha3Code: "TGO",
        flag: "https://restcountries.eu/data/tgo.svg",
    },
    {
        name: "Tokelau",
        callingCodes: "+690",
        alpha3Code: "TKL",
        flag: "https://restcountries.eu/data/tkl.svg",
    },
    {
        name: "Tonga",
        callingCodes: "+676",
        alpha3Code: "TON",
        flag: "https://restcountries.eu/data/ton.svg",
    },
    {
        name: "Trinidad and Tobago",
        callingCodes: "+1868",
        alpha3Code: "TTO",
        flag: "https://restcountries.eu/data/tto.svg",
    },
    {
        name: "Tunisia",
        callingCodes: "+216",
        alpha3Code: "TUN",
        flag: "https://restcountries.eu/data/tun.svg",
    },
    {
        name: "Turkey",
        callingCodes: "+90",
        alpha3Code: "TUR",
        flag: "https://restcountries.eu/data/tur.svg",
    },
    {
        name: "Turkmenistan",
        callingCodes: "+993",
        alpha3Code: "TKM",
        flag: "https://restcountries.eu/data/tkm.svg",
    },
    {
        name: "Turks and Caicos Islands",
        callingCodes: "+1649",
        alpha3Code: "TCA",
        flag: "https://restcountries.eu/data/tca.svg",
    },
    {
        name: "Tuvalu",
        callingCodes: "+688",
        alpha3Code: "TUV",
        flag: "https://restcountries.eu/data/tuv.svg",
    },
    {
        name: "Uganda",
        callingCodes: "+256",
        alpha3Code: "UGA",
        flag: "https://restcountries.eu/data/uga.svg",
    },
    {
        name: "Ukraine",
        callingCodes: "+380",
        alpha3Code: "UKR",
        flag: "https://restcountries.eu/data/ukr.svg",
    },
    {
        name: "United Arab Emirates",
        callingCodes: "+971",
        alpha3Code: "ARE",
        flag: "https://restcountries.eu/data/are.svg",
    },
    {
        name: "United Kingdom of Great Britain and Northern Ireland",
        callingCodes: "+44",
        alpha3Code: "GBR",
        flag: "https://restcountries.eu/data/gbr.svg",
    },
    {
        name: "United States of America",
        callingCodes: "+1",
        alpha3Code: "USA",
        flag: "https://restcountries.eu/data/usa.svg",
    },
    {
        name: "Uruguay",
        callingCodes: "+598",
        alpha3Code: "URY",
        flag: "https://restcountries.eu/data/ury.svg",
    },
    {
        name: "Uzbekistan",
        callingCodes: "+998",
        alpha3Code: "UZB",
        flag: "https://restcountries.eu/data/uzb.svg",
    },
    {
        name: "Vanuatu",
        callingCodes: "+678",
        alpha3Code: "VUT",
        flag: "https://restcountries.eu/data/vut.svg",
    },
    {
        name: "Venezuela (Bolivarian Republic of)",
        callingCodes: "+58",
        alpha3Code: "VEN",
        flag: "https://restcountries.eu/data/ven.svg",
    },
    {
        name: "Viet Nam",
        callingCodes: "+84",
        alpha3Code: "VNM",
        flag: "https://restcountries.eu/data/vnm.svg",
    },
    {
        name: "Wallis and Futuna",
        callingCodes: "+681",
        alpha3Code: "WLF",
        flag: "https://restcountries.eu/data/wlf.svg",
    },
    {
        name: "Western Sahara",
        callingCodes: "+212",
        alpha3Code: "ESH",
        flag: "https://restcountries.eu/data/esh.svg",
    },
    {
        name: "Yemen",
        callingCodes: "+967",
        alpha3Code: "YEM",
        flag: "https://restcountries.eu/data/yem.svg",
    },
    {
        name: "Zambia",
        callingCodes: "+260",
        alpha3Code: "ZMB",
        flag: "https://restcountries.eu/data/zmb.svg",
    },
    {
        name: "Zimbabwe",
        callingCodes: "+263",
        alpha3Code: "ZWE",
        flag: "https://restcountries.eu/data/zwe.svg",
    },
];

const App = (props) => {
    const [modalVisible, setModalVisible] = useState(props.visible);
    const [searchCountryCodes, setSearchCountryCodes] = useState(countryCodes);

    const _searchListData = (text) => {
        const newData = countryCodes.filter(item => {
            const itemData = item.name.toUpperCase();
            console.log('itemData = '+itemData);
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1
        });
        setSearchCountryCodes(newData);
    };

    return (
        <SafeAreaView>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
            >
                <ScrollView>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <View style={styles.title}>
                            <AppTextBold>Country List</AppTextBold>

                            <TouchableOpacity onPress={() => {
                                setModalVisible(false);
                                props.func("IND","+91","https://restcountries.eu/data/ind.svg");
                            }}>
                                <AppTextBold style={styles.close}> X </AppTextBold>
                            </TouchableOpacity>
                        </View>

                        <TextInput 
                            style={styles.searchBg} 
                            placeholder="SEARCH"
                            onChangeText={(text) => _searchListData(text)}
                        />

                        <View>
                            <FlatList
                                data={searchCountryCodes}
                                renderItem={({ item }) => (
                                    <TouchableOpacity onPress={() => {
                                        props.func(item.alpha3Code, item.callingCodes, item.flag);
                                        setModalVisible(false);

                                    }} >
                                        <View style={styles.listStyle}>
                                            <View style={styles.flag}>
                                                <SvgUri
                                                    width="100%"
                                                    height="80%"
                                                    uri={item.flag}
                                                />
                                            </View>
                                            <View style={styles.nameContainer}>
                                                <Text style={styles.name}>{item.name}</Text>
                                            </View>
                                            <View style={styles.numberContainer}>
                                                <Text style={styles.number}>{item.callingCodes}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )}
                            />

                        </View>

                    </View>
                </View>
                </ScrollView>
            </Modal>

        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        height: height,
        width: width
    },
    number: { fontSize:  appFonts.largeBold, fontWeight: 'bold' },
    nameContainer: { marginLeft: 25, width: "50%" },

    modalView: {
        margin: 20,
        backgroundColor: "white",
        height: height,
        width: width,
        padding: 20
    },
    numberContainer: { width: "18%", marginRight: 15, marginBottom: 15,  justifyContent: "flex-end", alignItems: "flex-end"},
    name: { fontSize: appFonts.largeBold, fontWeight: 'bold', marginBottom: 10 },
    flag: { height: 25, width: 30 },
    listStyle: { flexDirection: 'row', justifyContent: "space-between", marginTop: 20, borderBottomWidth: 2, borderBottomColor: colors.borderGrey },

    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    title: { flexDirection: "row", justifyContent: "space-between", marginBottom: 15 },
    close: { fontWeight: "bold", top: 10 },
    searchBg: { backgroundColor: colors.LynxWhite, marginBottom: "5%", height: "7%" }
});

export default App;