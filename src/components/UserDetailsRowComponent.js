import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';

import AppText from '../components/ui/ AppText';

import appFonts from '../config/appFonts';
import colors from '../config/colors';

const windowHeight = Dimensions.get('window').height;

const UserDetailsRowComponent = ({title, number, onPress}) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.buttonContainer}>
        <AppText style={styles.text}>{title}</AppText>
        {number > 0 ? (
          <View style={styles.number}>
            <AppText style={styles.txtNumber}>
              {number > 99 ? '99+' : number}
            </AppText>
          </View>
        ) : null}
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: colors.veryLightGray,
    borderBottomWidth: 1,
    height: windowHeight / 12,
  },
  text: {
    flex: 1,
    fontSize: appFonts.largeBold,
    marginLeft: 2,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  number: {
    width: 32,
    height: 32,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.jaguar,
    borderRadius: 16,
  },
  txtNumber: {
    color: colors.primary,
    fontSize: appFonts.mediumFontSize,
    fontFamily: appFonts.SourceSansProBold,
  },
});

export default UserDetailsRowComponent;
