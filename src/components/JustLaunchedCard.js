import React from 'react';
import {View, Text, Image, ImageBackground, Dimensions} from 'react-native';
import AppTextBoldSmall from './ui/AppTextBoldSmall';
import AppTextGray from './ui/AppTextGray';
const {width, height} = Dimensions.get('window');

const JustCardComponent = (props) => {
  return (
    <View style={{width: width, marginRight: 30}}>
      <View>
        <ImageBackground
          source={{uri: props.image}}
          style={{height: 350, width: '100%'}}>
          {/* <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
                   <Image source ={require('../assets/images/like.png')} />
               </View> */}
        </ImageBackground>
      </View>

      <View style={{flexDirection: 'row', marginTop: '3%'}}>
        <AppTextBoldSmall>{props.name} </AppTextBoldSmall>
        <AppTextGray>{props.city}</AppTextGray>
      </View>
      <AppTextGray>$62 Lack onwards </AppTextGray>
    </View>
  );
};
export default JustCardComponent;
