import React, { Component, Fragment } from "react";
import VideoPlayer from "react-native-video-player";
import { ActivityIndicator, View } from "react-native";
import colors from "../config/colors";

export class VideoThumbnailPlayer extends Component {
  constructor() {
    super();
    this._videRef = null;
    this.state = {
      isVideoLoading: false
    }
  }
  render() {
    return (
      <Fragment>
      <VideoPlayer
      video={{
        uri: this.props.url,
      }}
      ref={ r => this._videRef = r}
      style={{height: '100%', width: '100%'}}
      thumbnail={{
        uri: 'https://i.picsum.photos/id/866/1600/900.jpg',
      }}
      muted={true}
      disableSeek={true}
      autoplay={true}
      loop
      hideControlsOnStart={true}
      resizeMode="stretch"
      disableControlsAutoHide={true}
      customStyles={{controls: {opacity: 0}}}
      onLoadStart={() => {
        this.setState({isVideoLoading: true})
      }}
      onLoad={() => {
        this.setState({isVideoLoading: false})
      }}
      onEnd={(event) => {
     this._videRef.stop();
     setTimeout(() => {
       this._videRef.resume();
     }, 250)
      }}
    />
        {this.state.isVideoLoading ? (
          <View
            style={{
              height: '100%',
              position: 'absolute',
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              backgroundColor: '#ffffff',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator
              color={colors.jaguar}
              size="small"
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            />
          </View>
        ) : null}
      </Fragment>
    )
  }
}
