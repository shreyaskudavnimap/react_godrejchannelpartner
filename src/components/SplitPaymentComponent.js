import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Image,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';

import CheckBox from '@react-native-community/checkbox';
import appFonts from '../config/appFonts';
import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import colors from '../config/colors';
const windowWidth = Dimensions.get('window').width;

const SplitPaymentComponent = (props) => {
  var bookingItemsAccList = props.bookingItemsAccList;
  // console.log('bookingItemsAcc++++++', bookingItemsAccList);
  const totalPayment = props.totalPayment;
  var subTotal = 0;
  return (
    <View>
      {/* {bookingItemsAccList.map((bookingItem, bookingIndex) => { */}
      {Object.keys(bookingItemsAccList).map((bKey) => {
        subTotal = 0;
        return (
          <View key={bKey}>
            <View style={styles.tabConatiner}>
              <View style={styles.propertyDetail}>
                <AppTextBold style={styles.propertyName}
                  textBreakStrategy="simple"
                  ellipsizeMode="tail">
                  {bookingItemsAccList[bKey][0].project_name}
                </AppTextBold>
              </View>
              {bookingItemsAccList[bKey].map((bookingItem, bookingIndex) => {
                // console.log("bookingItem.bank_list", bookingItem.bank_list);
                // subTotal = 0;
                return (
                  <View key={bookingIndex}>
                    <View style={styles.propertyDetail}>
                      <AppTextBold style={styles.propertyName}
                        textBreakStrategy="simple"
                        ellipsizeMode="tail">
                        {bookingItem.inventory_name}
                      </AppTextBold>
                    </View>

                    {Object.keys(bookingItem.bank_list).length > 0 ? (
                      <>
                        {Object.keys(bookingItem.bank_list).map((key) => (
                          <View key={key}>
                            <AppText style={styles.accNo}>
                              Bank A/c No. {key}
                            </AppText>
                            {bookingItem.bank_list[key].map(
                              (accItem, accIndex) => {
                                if (accItem.field_amount > 0) {
                                  subTotal =
                                    subTotal + parseFloat(accItem.field_amount);
                                }
                                return (
                                  <View
                                    style={styles.checkboxContainer}
                                    key={accIndex}>
                                    <TouchableWithoutFeedback
                                      onPress={() => {
                                        props.markInput(
                                          bKey,
                                          bookingIndex,
                                          key,
                                          accIndex,
                                          accItem.is_checked,
                                        );
                                      }}>
                                      <Image
                                        style={styles.checkbox}
                                        source={
                                          accItem.is_checked == true
                                            ? require('./../assets/images/checked-icon.png')
                                            : require('./../assets/images/unchecked-gray-icon.png')
                                        }
                                      />
                                    </TouchableWithoutFeedback>
                                    <View style={styles.item}>
                                      <View style={styles.amountBlock}>
                                        <View style={{ flex: 0.6 }}>
                                          <View style={styles.amountInput}>
                                            <AppText style={styles.itemLabel}>
                                              {accItem.lineItemName}
                                            </AppText>
                                            {/* <AppText style={styles.itemValue}>{'\u20B9'}{accItem.field_amount_formated}</AppText> */}
                                          </View>
                                        </View>
                                        <View
                                          style={{ flex: 0.4, marginTop: -15 }}>
                                          <View style={styles.amountInput}>
                                            <AppText>{'\u20B9'}</AppText>
                                            <TextInput
                                              style={styles.input}
                                              placeholder="0.00"
                                              onChangeText={(value) =>
                                                props.updateAmount(
                                                  bKey,
                                                  bookingIndex,
                                                  key,
                                                  accIndex,
                                                  value,
                                                )
                                              }
                                              maxLength={10}
                                              contextMenuHidden={true}
                                              keyboardType={'number-pad'}
                                              editable={accItem.is_checked}
                                              value={
                                                accItem.field_amount
                                              }
                                              defaultValue={
                                                accItem.field_amount
                                              }
                                            />
                                          </View>
                                        </View>
                                      </View>
                                    </View>
                                  </View>
                                );
                              },
                            )}
                          </View>
                        ))}
                      </>
                    ) : (
                        <View style={styles.amountType}>
                          <AppText style={styles.errorBlock}>
                            No bank details found for this booking
                        </AppText>
                        </View>
                      )}
                  </View>
                );
              })}
              <View style={styles.subTotalContainer}>
                <View style={{ flex: 0.55, paddingHorizontal: '7%' }}>
                  <AppText style={{ color: colors.gray3 }}>Total Payment</AppText>
                </View>
                <View style={{ flex: 0.45 }}>
                  <AppTextBold style={{ fontSize: appFonts.largeBold }}>
                    {'\u20B9'}
                    {subTotal.toFixed(2)}
                  </AppTextBold>
                </View>
              </View>
            </View>
          </View>
        );
      })}

      <View style={styles.paymentContainer}>
        <View style={{ flex: 0.6, paddingHorizontal: '7%' }}>
          <AppText style={{ color: colors.gray3 }}>
            Grand Total Payable Amount
          </AppText>
        </View>
        <View style={{ flex: 0.4 }}>
          <AppTextBold style={{ fontSize: appFonts.largeBold }}>
            {'\u20B9'}
            {totalPayment}
          </AppTextBold>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  tabConatiner: {
    paddingTop: '5%',
    marginBottom: 20,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  propertyDetail: {
    alignSelf: "center"
  },
  propertyName: {
    width: '100%',
    fontSize: appFonts.largeFontSize,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bank: {
    fontSize: appFonts.mediumFontSize,
    color: colors.gray3,
  },
  accNo: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    marginBottom: 10,
  },
  amountType: {},
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    marginHorizontal: 5,
  },
  itemLabel: {
    width: '90%',
    color: colors.gray3,
    // marginBottom: 4,
    height: 40,
    flexWrap: 'wrap',
    flexShrink: 1,
  },
  itemValue: {
    width: windowWidth / 1.4,
    marginBottom: 10,
    flexWrap: 'wrap',
    flexShrink: 1,
    marginBottom: 15,
  },
  input: {
    flex: 0.7,
    flexDirection: 'row',
    width: '90%',
    borderBottomWidth: 1,
    paddingVertical: 0,
    borderBottomColor: colors.gray3,
  },
  amountBlock: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  amountInput: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    // fontSize: 18,
    // marginTop: -15,
    lineHeight: 40,
    height: 40,
    includeFontPadding: false,
  },
  paymentContainer: {
    width: '100%',
    height: 50,
    marginVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: colors.whiteSmoke,
  },
  subTotalContainer: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: colors.whiteSmoke,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  checkboxContainer: {
    flex: 0.5,
    flexDirection: 'row',
    // padding: '3%',
    // paddingVertical: 3,
    alignItems: 'flex-start',
    marginLeft: 5,
  },
  checkboxLabel: {
    color: colors.gray3,
    margin: 6,
  },
  checkbox: { height: 22, width: 22 },
  errorBlock: {
    padding: 15,
    textAlign: 'center',
  },
});

export default SplitPaymentComponent;
