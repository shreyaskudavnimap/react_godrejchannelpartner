import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TextInput,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';

import AppText from '../components/ui/ AppText';
import AppButton from '../components/ui/AppButton';

import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppNote from './ui/AppNote';
import appCurrencyFormatter from '../utility/appCurrencyFormatter';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const CostSheetComponent = ({
  data,
  onPress,
  showApplyCoupon,
  isCouponApplied,
  onCouponApplied,
  promoCodeData,
  onDownloadCostSheet,
  isShowDisclaimer = true,
}) => {
  
  const [costSheetData, setCostSheetData] = useState(data);
  const [isShowCoupon, setIsShowCoupon] = useState(false);
  const [couponCode, setCouponCode] = useState(
    promoCodeData && promoCodeData.promo_code ? promoCodeData.promo_code : '',
  );
  const [showError, setShowError] = useState(false);

  useEffect(() => {
    setCostSheetData(data);
  }, [data]);

  const onSelectProjectBreakup = (projectPriceIndex) => {
    const data = {...costSheetData};
    data.curl_response.project_price[projectPriceIndex].isSelected = !data
      .curl_response.project_price[projectPriceIndex].isSelected;
    setCostSheetData(data);
  };

  const onChangeCouponText = (text) => {
    setCouponCode(text);
    setShowError(false);
  };

  const onHandleCouponApplied = () => {
    if (isCouponApplied) {
      onChangeCouponText('');
      onCouponApplied('');
    } else {
      if (couponCode && couponCode != '') {
        onCouponApplied(couponCode);
      } else {
        setShowError(true);
      }
    }
  };

  return (
    <View style={styles.viewContainer}>
      <View style={{paddingHorizontal: 25}}>
        <TouchableWithoutFeedback onPress={onPress}>
          <View style={styles.titleLine}>
            <AppText
              style={[
                styles.title,
                {fontFamily: appFonts.SourceSansProSemiBold},
              ]}>
              Cost Sheet Details
            </AppText>
            <Image
              source={require('./../assets/images/up-icon-b.png')}
              style={styles.icon}
            />
          </View>
        </TouchableWithoutFeedback>

        {costSheetData.curl_response &&
          costSheetData.curl_response.project_price &&
          costSheetData.curl_response.project_price.length > 0 && (
            <>
              {costSheetData.curl_response.project_price.map(
                (projectPrice, projectPriceIndex) => (
                  <View
                    style={styles.priceMainContainer}
                    key={projectPriceIndex}>
                    <TouchableWithoutFeedback
                      onPress={() => onSelectProjectBreakup(projectPriceIndex)}>
                      <View style={styles.clikContainer}>
                        <View style={styles.priceLbContainer}>
                          <AppText style={styles.priceTxt}>
                            {projectPrice.title}
                          </AppText>
                          <AppText style={styles.priceLb}>
                            {'\u20B9'}&nbsp;
                            {appCurrencyFormatter.getIndianCurrencyFormat(
                              projectPrice.value,
                            )}
                          </AppText>
                        </View>
                        <Image
                          source={
                            projectPrice.isSelected
                              ? require('./../assets/images/up-icon-b.png')
                              : require('./../assets/images/down-icon-b.png')
                          }
                          style={styles.iconPrice}
                        />
                      </View>
                    </TouchableWithoutFeedback>

                    {projectPrice.isSelected &&
                      projectPrice.breakups &&
                      projectPrice.breakups.length > 0 && (
                        <View>
                          {projectPrice.breakups.map(
                            (breakupItem, breakupIndex) => (
                              <View key={breakupIndex}>
                                {breakupItem &&
                                breakupItem.title &&
                                breakupItem.value ? (
                                  <View style={styles.priceValContainer}>
                                    <AppText style={styles.txtPriceLb}>
                                      {breakupItem.title}
                                    </AppText>

                                    <AppText style={styles.txtPrice}>
                                      {'\u20B9'}&nbsp;
                                    </AppText>

                                    <AppText style={styles.totalPriceValue}>
                                      {appCurrencyFormatter.getIndianCurrencyFormat(
                                        breakupItem.value,
                                      )}
                                    </AppText>
                                  </View>
                                ) : null}
                              </View>
                            ),
                          )}
                        </View>
                      )}
                  </View>
                ),
              )}
            </>
          )}
        {costSheetData &&
        costSheetData.curl_response &&
        costSheetData.curl_response.project_stock &&
        costSheetData.curl_response.project_stock.total_price ? (
          <View style={styles.totalPriceContainer}>
            <View style={styles.totalPriceLbContainer}>
              <AppText style={styles.totalPriceLb}>
                Total Sales Price (A+B+C)
              </AppText>
              {isCouponApplied ? (
                <AppText style={styles.txtDiscount}>After Discount</AppText>
              ) : null}
            </View>

            <AppText style={styles.txtPrice}>{'\u20B9'}&nbsp;</AppText>

            <AppText style={styles.totalPriceValue}>
              {appCurrencyFormatter.getIndianCurrencyFormat(
                costSheetData.curl_response.project_stock.total_price,
              )}
            </AppText>
          </View>
        ) : null}

        {isShowDisclaimer ? (
          <AppNote label={`Exclusive of Stamp Duty & Registration Charges`} />
        ) : null}

        {showApplyCoupon ? (
          <View style={styles.couponContainer}>
            <TouchableWithoutFeedback
              onPress={() => setIsShowCoupon(!isShowCoupon)}>
              <View style={styles.applyCouponTitle}>
                <AppText style={styles.couponTxt}>Apply Coupon</AppText>
                <Image
                  source={
                    isShowCoupon
                      ? require('./../assets/images/up-icon-b.png')
                      : require('./../assets/images/down-icon-b.png')
                  }
                  style={styles.iconPrice}
                />
              </View>
            </TouchableWithoutFeedback>

            {isShowCoupon && (
              <View>
                <View style={styles.txtCouponLine}>
                  <AppText style={styles.couponText}>Coupon Applied</AppText>
                  <AppText style={styles.couponText}>
                    {'\u20B9'}&nbsp;
                    {promoCodeData.disc_amt && promoCodeData.disc_amt != ''
                      ? appCurrencyFormatter.getIndianCurrencyFormat(
                          promoCodeData.disc_amt,
                        )
                      : 0.0}
                  </AppText>
                </View>
                <View style={styles.applyCouponLine}>
                  <View style={{flex: 1}}>
                    <TextInput
                      style={styles.txtInput}
                      placeholder="Add Coupon"
                      onChangeText={(text) => onChangeCouponText(text)}
                      value={couponCode}
                    />
                  </View>

                  <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={onHandleCouponApplied}>
                    <View style={styles.applyBtn}>
                      <AppText style={{color: colors.primary}}>
                        {isCouponApplied ? 'REMOVE' : 'APPLY'}
                      </AppText>
                    </View>
                  </TouchableOpacity>
                </View>

                {showError ? (
                  <AppText style={styles.txtErrorCoupon}>Enter Coupon</AppText>
                ) : null}
              </View>
            )}
          </View>
        ) : null}

        <AppButton
          color="primary"
          textColor="secondary"
          title="Download Cost Sheet"
          onPress={() => onDownloadCostSheet()}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  viewContainer: {
    width: windowWidth,
    paddingBottom: 20,
    marginBottom: 25,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  clikContainer: {
    flexDirection: 'row',
  },
  titleLine: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize:  appFonts.largeBold,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  icon: {
    width: 16,
    height: 16,
  },
  priceMainContainer: {
    paddingHorizontal: 16,
    paddingVertical: 18,
    marginTop: 15,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  couponContainer: {
    // flexDirection: 'row',
    paddingHorizontal: 16,
    paddingVertical: 18,
    marginVertical: 15,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
    // alignItems: 'flex-start',
  },
  priceLbContainer: {flex: 1},
  iconPrice: {
    width: 14,
    height: 14,
    marginTop: 5,
  },
  priceTxt: {fontSize: appFonts.largeFontSize},
  priceLb: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
    marginTop: 3,
  },
  couponTxt: {
    fontSize: appFonts.largeFontSize,
    flex: 1,
    color: colors.jaguar,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  totalPriceContainer: {
    flexDirection: 'row',
    paddingHorizontal: 12,
    paddingVertical: 18,
    backgroundColor: colors.LynxWhite,
    marginVertical: 15,
  },
  totalPriceLbContainer: {flex: 1.7},
  totalPriceLb: {fontSize: appFonts.largeFontSize},
  txtDiscount: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
    marginTop: 4,
  },
  totalPriceValue: {
    flex: 1,
    textAlign: 'right',
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  txtPrice: {textAlign: 'center', fontSize: appFonts.largeFontSize},
  priceValContainer: {
    flexDirection: 'row',
    marginTop: 15,
  },
  txtPriceLb: {flex: 1.7, marginRight: 5, fontSize: appFonts.largeFontSize},
  applyCouponContainer: {marginTop: 15},
  applyCouponTitle: {flexDirection: 'row'},
  txtCouponLine: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  applyCouponLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  couponText: {fontSize: appFonts.largeFontSize},
  txtInput: {
    flex: 1,
    borderBottomColor: colors.gray5,
    borderBottomWidth: 1,
    paddingVertical: 5,
  },
  applyBtn: {
    borderRadius: 5,
    backgroundColor: colors.secondaryLight,
    alignItems: 'center',
    paddingVertical: 10,
    width: 90,
  },
  apply: {
    borderRadius: 5,
    backgroundColor: colors.secondaryLight,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtErrorCoupon: {color: colors.danger, fontSize: appFonts.normalFontSize, marginTop: 5},
});

export default CostSheetComponent;
