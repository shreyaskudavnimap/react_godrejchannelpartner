import React, {useState} from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';

import AppText from '../components/ui/ AppText';

import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppTextBold from './ui/AppTextBold';
import appCurrencyFormatter from '../utility/appCurrencyFormatter';

const windowWidth = Dimensions.get('window').width;

const amountType = [
  {
    key: '1',
    title: 'Pay Booking Amount',
  },
  {
    key: '2',
    title: 'Pay Custom Amount',
  },
];

const BookingAmountComponent = ({
  bookingAmountData,
  onPress,
  customAmountInputError,
  invalidCustomBookingAmount,
  remainingBookingAmount,
  customAmountInputPlacehlder,
  onClickRadioButton,
  onChangePriceInput,
}) => {
  // console.log(
  //   'BookingAmountComponent - bookingAmountData: ',
  //   bookingAmountData,
  // );

  const [value, setValue] = useState(
    bookingAmountData.payment_method === 'full' ? '1' : '2',
  );

  const onClickBookingAmountType = (type) => {
    setValue(type);
    onClickRadioButton(type);
  };

  return (
    <View style={styles.viewContainer}>
      <View style={{paddingHorizontal: 25}}>
        <TouchableWithoutFeedback onPress={onPress}>
          <View style={styles.titleLine}>
            <AppText
              style={[
                styles.title,
                {fontFamily: appFonts.SourceSansProSemiBold},
              ]}>
              Booking Amount
            </AppText>
            <Image
              source={require('./../assets/images/up-icon-b.png')}
              style={styles.icon}
            />
          </View>
        </TouchableWithoutFeedback>

        {!bookingAmountData.booking_amount_radio ? (
          <View style={{paddingVertical: 10}}>
            <View style={styles.txtContainerSingle}>
              <AppText style={styles.txtValue}>Pay Booking Amount</AppText>
              <AppText
                style={[
                  styles.txtValue,
                  {marginTop: 10, fontFamily: appFonts.SourceSansProSemiBold},
                ]}>
                {'\u20B9'}&nbsp;
                {appCurrencyFormatter.getIndianCurrencyFormat(
                  bookingAmountData.max_amount,
                )}
              </AppText>
            </View>
          </View>
        ) : null}

        {bookingAmountData.booking_amount_radio ? (
          <View style={{paddingVertical: 10}}>
            {amountType.map((data) => (
              <View key={data.key} style={styles.buttonContainer}>
                <View style={{marginTop: 8}}>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.circle}
                    onPress={() => onClickBookingAmountType(data.key)}>
                    {value === data.key && (
                      <View style={styles.checkedCircle} />
                    )}
                  </TouchableOpacity>
                </View>
                <View style={{marginTop: 8}}>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => onClickBookingAmountType(data.key)}>
                    <AppText style={{fontSize: appFonts.largeFontSize}}>{data.title}</AppText>
                  </TouchableOpacity>
                  {value === data.key && (
                    <>
                      {data.key === '2' ? (
                        <View>
                          <TextInput
                            style={[
                              styles.txtInput,
                              {
                                fontSize:
                                  bookingAmountData.custom_amount.length > 0
                                    ? appFonts.largeBold
                                    : 12,
                                fontFamily:
                                  bookingAmountData.custom_amount.length > 0
                                    ? appFonts.SourceSansProSemiBold
                                    : appFonts.SourceSansProRegular,
                              },
                            ]}
                            placeholder={customAmountInputPlacehlder}
                            placeholderTextColor={colors.gray}
                            autoCapitalize="none"
                            autoCorrect={false}
                            keyboardType="number-pad"
                            editable={true}
                            value={
                              bookingAmountData.custom_amount
                                ? bookingAmountData.custom_amount
                                : ''
                            }
                            onChangeText={(value) => {
                              onChangePriceInput(value);
                            }}
                          />

                          {bookingAmountData.is_booking_amount_error &&
                          bookingAmountData.custom_amount != '' ? (
                            <View style={{paddingBottom: 10, paddingRight: 20}}>
                              {!invalidCustomBookingAmount ||
                              invalidCustomBookingAmount == '' ? (
                                <AppText style={{color: colors.danger}}>
                                  {customAmountInputError}
                                </AppText>
                              ) : (
                                <>
                                  {invalidCustomBookingAmount &&
                                  invalidCustomBookingAmount != '' ? (
                                    <AppText style={{color: colors.danger}}>
                                      {invalidCustomBookingAmount}
                                    </AppText>
                                  ) : null}
                                </>
                              )}
                            </View>
                          ) : null}

                          {!bookingAmountData.is_booking_amount_error &&
                          bookingAmountData.custom_amount != '' ? (
                            <View style={{paddingBottom: 10, paddingRight: 20}}>
                              <AppText style={{color: colors.success}}>
                                Your remaining booking amount to be paid is
                                &nbsp;{'\u20B9'}&nbsp;
                                {appCurrencyFormatter.getIndianCurrencyFormat(
                                  remainingBookingAmount,
                                )}
                              </AppText>
                            </View>
                          ) : null}
                        </View>
                      ) : (
                        <View style={styles.txtContainer}>
                          <AppText
                            style={[
                              styles.txtValue,
                              {fontFamily: appFonts.SourceSansProSemiBold},
                            ]}>
                            {'\u20B9'}&nbsp;
                            {appCurrencyFormatter.getIndianCurrencyFormat(
                              bookingAmountData.max_amount,
                            )}
                          </AppText>
                        </View>
                      )}
                    </>
                  )}
                </View>
              </View>
            ))}
          </View>
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  viewContainer: {
    width: windowWidth,
    marginBottom: 25,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  titleLine: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  icon: {
    width: 16,
    height: 16,
  },
  txtInput: {
    borderBottomColor: colors.lightGray,
    borderBottomWidth: 1,
    marginBottom: 2,
    fontFamily: appFonts.SourceSansProSemiBold,
    fontSize: appFonts.largeBold,
    marginTop: 4,
    paddingVertical: 10,
    width: windowWidth / 1.35,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  circle: {
    marginRight: 10,
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: colors.secondaryLight,
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkedCircle: {
    width: 12,
    height: 12,
    borderRadius: 7,
    backgroundColor: colors.secondaryLight,
  },
  txtContainerSingle: {
    borderBottomColor: colors.lightGray,
    borderBottomWidth: 1,
    paddingVertical: 10,
    width: windowWidth / 1.25,
  },
  txtContainer: {
    borderBottomColor: colors.lightGray,
    borderBottomWidth: 1,
    paddingVertical: 10,
    width: windowWidth / 1.35,
  },
  txtValue: {
    fontSize:  appFonts.largeBold,
  },
});

export default BookingAmountComponent;
