import React, {useState, useRef} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
  TextInput,
  Alert,
  Platform,
} from 'react-native';
import Modal from 'react-native-modal';
import DropDownPicker from 'react-native-dropdown-picker';
import {DateInput} from 'react-native-date-input';
import DateTimePicker from '@react-native-community/datetimepicker';
import dayjs from 'dayjs';

import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';
import AppButton from '../components/ui/AppButton';
import appFonts from '../config/appFonts';
import colors from '../config/colors';

import AppFileChooser from './../components/actionSheet/AppFileChooser';
import appFileChooser from './../utility/appFileChooser';
import appSnakBar from '../utility/appSnakBar';
import appConstant from '../utility/appConstant';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const PrimaryApplicant = ({
  onPress,
  fieldInputs: inputs,
  onInputChange,
  onSelectPermanentCountry,
  onSelectCommunicationCountry,
  isSameAddress,
  onSelectSameAddress,
  idProofDocType,
  addressProofDocType,
  purposeOfPurchase,
  isNriCustomer,
  isDobEditable,
  attachedFileIdProof,
  attachedFileAddressProof,
  downloadApplicantDocument,
  onUploadFileSelect,
  setDropPurpose,
  setDropDocumentId,
  setDropDocumentAddress,
}) => {
  const [fileChooserResponse, setFileChooserResponse] = useState(null);
  const [fileChooser, setFileChooser] = useState(false);
  const [fileChooserFor, setFileChooserFor] = useState('');
  const [showDatePicker, setShowDatePicker] = useState(false);

  const [
    dynamicBottomPaddingPurpose,
    setDynamicBottomPaddingPurpose,
  ] = useState(0);

  const [dynamicBottomPaddingDocId, setDynamicBottomPaddingDocId] = useState(0);

  const [dynamicBottomPaddingDocAdd, setDynamicBottomPaddingDocAdd] = useState(
    0,
  );

  const onShowDatePicker = () => {
    setShowDatePicker(true);
  };

  const onCloseDatePicker = () => {
    setShowDatePicker(false);
  };

  const closeActionSheet = () => setFileChooser(false);

  const onChooseFileUpload = (type) => {
    setFileChooserFor(type);
    setFileChooser(true);
  };

  const onClickFileChooserItem = async (clickItem) => {
    console.log('fileChooserFor: ', fileChooserFor);
    switch (clickItem) {
      case 'Remove':
        setFileChooser(false);
        break;

      case 'Gallery':
        appFileChooser.onLaunchImageLibrary((fileChooserResponse) => {
          setFileChooser(false);

          if (fileChooserResponse) {
            const fileName = fileChooserResponse.name
              ? fileChooserResponse.name
              : fileChooserResponse.uri
              ? fileChooserResponse.uri.split('/').pop()
              : '';
            if (fileName) {
              const re = /(?:\.([^.]+))?$/;
              const ext = re.exec(fileName)[1];

              if (
                typeof ext !== undefined &&
                ext &&
                (ext.toLowerCase() == 'jpg' ||
                  ext.toLowerCase() == 'jpeg' ||
                  ext.toLowerCase() == 'png' ||
                  ext.toLowerCase() == 'pdf')
              ) {
                if (fileChooserResponse && fileChooserResponse.fileSize) {
                  if (
                    fileChooserResponse.fileSize &&
                    validateFileSize(fileChooserResponse.fileSize)
                  ) {
                    onUploadFileSelect(fileChooserFor, fileChooserResponse);
                  } else {
                    invalidFile();
                  }
                }
              } else {
                invalidFileFormat();
              }
            }
          }
        });
        break;

      case 'Camera':
        appFileChooser.onLaunchCamera((fileChooserResponse) => {
          setFileChooser(false);

          if (fileChooserResponse && fileChooserResponse.fileSize) {
            if (
              fileChooserResponse.fileSize &&
              validateFileSize(fileChooserResponse.fileSize)
            ) {
              onUploadFileSelect(fileChooserFor, fileChooserResponse);
            } else {
              invalidFile();
            }
          }
        });
        break;

      case 'Phone':
        try {
          const chooserResponse = await appFileChooser.onSelectFilePdf();
          const dataUriResponse = await appFileChooser.toBase64(
            chooserResponse.uri,
          );
          chooserResponse.data = dataUriResponse;
          setFileChooser(false);

          if (chooserResponse) {
            const fileName = chooserResponse.name
              ? chooserResponse.name
              : chooserResponse.uri
              ? chooserResponse.uri.split('/').pop()
              : '';
            if (fileName) {
              const re = /(?:\.([^.]+))?$/;
              const ext = re.exec(fileName)[1];

              if (
                typeof ext !== undefined &&
                ext &&
                (ext.toLowerCase() == 'jpg' ||
                  ext.toLowerCase() == 'jpeg' ||
                  ext.toLowerCase() == 'png' ||
                  ext.toLowerCase() == 'pdf')
              ) {
                if (chooserResponse && chooserResponse.size) {
                  if (
                    chooserResponse.size &&
                    validateFileSize(chooserResponse.size)
                  ) {
                    onUploadFileSelect(fileChooserFor, chooserResponse);
                  } else {
                    invalidFile();
                  }
                }
              } else {
                invalidFileFormat();
              }
            }
          }
        } catch (error) {
          setFileChooser(false);
          console.log('Error onSelectFile : ', error);
        }
        break;
    }
  };

  const invalidFile = () => {
    Alert.alert(
      'Oops!',
      'Invalid file size. File size should be less than 5MB',
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  };

  const invalidFileFormat = () => {
    Alert.alert(
      'Oops!',
      'Invalid file format. Allowed document type - jpg, jpeg, png, pdf',
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  };

  const validateFileSize = (fileSize) => {
    let isValidFile = true;
    appFileChooser.validateFileSize(fileSize, (fileChooserResponse) => {
      isValidFile = fileChooserResponse ? true : false;
    });
    return isValidFile;
  };

  const renderError = (id) => {
    if (inputs && inputs[id].errorLabel && inputs[id].showErr) {
      return (
        <View style={styles.txtErrorContainer}>
          <AppText style={styles.txtError}>{inputs[id].errorLabel}</AppText>
        </View>
      );
    }
    return null;
  };

  const renderErrorDropdown = (id) => {
    if (inputs && inputs[id].errorLabel && inputs[id].showErr) {
      return (
        <View>
          <AppText style={styles.txtErrorDoc}>{inputs[id].errorLabel}</AppText>
        </View>
      );
    }
    return null;
  };

  return (
    <View style={styles.viewContainer}>
      <View style={{paddingHorizontal: 25}}>
        <TouchableOpacity onPress={onPress}>
          <View style={styles.titleLine}>
            <AppText
              style={[
                styles.title,
                {fontFamily: appFonts.SourceSansProSemiBold},
              ]}>
              Primary Applicant Details
            </AppText>
            <Image
              source={require('./../assets/images/up-icon-b.png')}
              style={styles.icon}
            />
          </View>
        </TouchableOpacity>
        <AppText
          style={[
            styles.title,
            {
              fontSize: appFonts.largeFontSize,
              marginBottom: 20,
              lineHeight: 22,
              textAlign: 'justify',
            },
          ]}>
          The information provided below will be used to process your
          application booking form. Once confirmed, these details will be used
          to create the purchase agreement for the given unit. Incase of any
          changes required, kindly get in touch with us.
        </AppText>

        <AppTextBold style={styles.parts}>Personal Info</AppTextBold>

        <View>
          <AppText>Nationality</AppText>
          <View style={styles.btnContainer}>
            <View style={styles.btnInd}>
              <AppButton
                color={!isNriCustomer ? 'secondary' : 'primary'}
                textColor={!isNriCustomer ? 'primary' : 'secondary'}
                title="Indian"
              />
            </View>
            <View style={styles.btnNri}>
              <AppButton
                color={isNriCustomer ? 'secondary' : 'primary'}
                textColor={isNriCustomer ? 'primary' : 'secondary'}
                title="NRI"
              />
            </View>
          </View>
        </View>

        <View style={{marginTop: 12}}>
          <AppText>Name *</AppText>
          <TextInput
            numberOfLines={1}
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="default"
            editable={false}
            value={
              inputs && inputs['name'] && inputs['name'].value
                ? inputs['name'].value
                : ''
            }
            onChangeText={(value) => {
              onInputChange('name', value);
            }}
          />
          {renderError('name')}
        </View>
        <View>
          <AppText>Mobile Number *</AppText>
          <TextInput
            numberOfLines={1}
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="default"
            editable={false}
            value={
              inputs && inputs['mobNo'] && inputs['mobNo'].value
                ? inputs['mobNo'].value
                : ''
            }
            onChangeText={(value) => {
              onInputChange('mobNo', value);
            }}
          />
          {renderError('mobNo')}
        </View>
        <View>
          <AppText>Email *</AppText>
          <TextInput
            numberOfLines={1}
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="default"
            editable={false}
            value={
              inputs && inputs['email'] && inputs['email'].value
                ? inputs['email'].value
                : ''
            }
            onChangeText={(value) => {
              onInputChange('email', value);
            }}
          />
          {renderError('email')}
        </View>
        <View>
          {isDobEditable ? (
            <>
              {Platform.OS === 'ios' ? (
                <View>
                  <AppText>Date of Birth *</AppText>
                  <DateInput
                    inputProps={{
                      style: styles.input,
                    }}
                    dateFormat={'DD MMM YYYY'}
                    defaultDate={new Date(dayjs().format('DD MMM YYYY'))}
                    defaultValue={
                      inputs && inputs['dob'] && inputs['dob'].value
                        ? dayjs(inputs['dob'].value).format('DD MMM YYYY')
                        : ''
                    }
                    maximumDate={
                      new Date(
                        dayjs().subtract(18, 'year').format('DD MMM YYYY'),
                      )
                    }
                    handleChange={(date) => {
                      onInputChange(
                        'dob',
                        date ? dayjs(date).format('YYYY/MM/DD') : '',
                      );
                    }}
                  />
                </View>
              ) : (
                <TouchableOpacity
                  onPress={onShowDatePicker}
                  activeOpacity={0.7}>
                  <View>
                    <AppText>Date of Birth *</AppText>
                    <TextInput
                      pointerEvents="none"
                      numberOfLines={1}
                      style={styles.input}
                      autoCapitalize="none"
                      autoCorrect={false}
                      keyboardType="default"
                      editable={false}
                      value={
                        inputs && inputs['dob'] && inputs['dob'].value
                          ? dayjs(inputs['dob'].value).format('DD MMM YYYY')
                          : ''
                      }
                    />

                    {showDatePicker && (
                      <DateTimePicker
                        testID="dateTimePicker"
                        value={new Date(dayjs().format('DD MMM YYYY'))}
                        mode="date"
                        display={
                          Platform.OS === 'android' ? 'calendar' : 'compact'
                        }
                        onChange={(e, date) => {
                          onCloseDatePicker();
                          onInputChange(
                            'dob',
                            date ? dayjs(date).format('YYYY/MM/DD') : '',
                          );
                        }}
                        maximumDate={
                          new Date(
                            dayjs().subtract(18, 'year').format('DD MMM YYYY'),
                          )
                        }
                      />
                    )}
                  </View>
                </TouchableOpacity>
              )}
            </>
          ) : (
            <TextInput
              numberOfLines={1}
              style={styles.input}
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="default"
              editable={false}
              value={
                inputs && inputs['dob'] && inputs['dob'].value
                  ? dayjs(inputs['dob'].value).format('DD MMM YYYY')
                  : ''
              }
            />
          )}

          {renderError('dob')}
        </View>

        <AppTextBold style={styles.parts}>Permanent Address</AppTextBold>

        <View>
          <AppText>House No. *</AppText>
          <TextInput
            numberOfLines={1}
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="default"
            editable={true}
            value={
              inputs && inputs['house_no'] && inputs['house_no'].value
                ? inputs['house_no'].value
                : ''
            }
            onChangeText={(value) => {
              onInputChange('house_no', value);
            }}
          />
          {renderError('house_no')}
        </View>
        <View>
          <AppText>Street *</AppText>
          <TextInput
            numberOfLines={1}
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="default"
            editable={true}
            value={
              inputs && inputs['street'] && inputs['street'].value
                ? inputs['street'].value
                : ''
            }
            onChangeText={(value) => {
              onInputChange('street', value);
            }}
          />
          {renderError('street')}
        </View>
        <View>
          <AppText>Locality *</AppText>
          <TextInput
            numberOfLines={1}
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="default"
            editable={true}
            value={
              inputs && inputs['locality'] && inputs['locality'].value
                ? inputs['locality'].value
                : ''
            }
            onChangeText={(value) => {
              onInputChange('locality', value);
            }}
          />
          {renderError('locality')}
        </View>
        <View>
          <AppText>Pincode *</AppText>
          <TextInput
            numberOfLines={1}
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="default"
            editable={true}
            value={
              inputs && inputs['pincode'] && inputs['pincode'].value
                ? inputs['pincode'].value
                : ''
            }
            onChangeText={(value) => {
              onInputChange('pincode', value);
            }}
          />
          {renderError('pincode')}
        </View>
        <View>
          <AppText>City *</AppText>
          <TextInput
            numberOfLines={1}
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="default"
            editable={true}
            value={
              inputs && inputs['city'] && inputs['city'].value
                ? inputs['city'].value
                : ''
            }
            onChangeText={(value) => {
              onInputChange('city', value);
            }}
          />
          {renderError('city')}
        </View>
        <View>
          <AppText>State *</AppText>
          <TextInput
            numberOfLines={1}
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="default"
            editable={true}
            value={
              inputs && inputs['state'] && inputs['state'].value
                ? inputs['state'].value
                : ''
            }
            onChangeText={(value) => {
              onInputChange('state', value);
            }}
          />
          {renderError('state')}
        </View>

        <TouchableOpacity
          onPress={onSelectPermanentCountry}
          activeOpacity={0.7}>
          <View>
            <AppText>Country *</AppText>
            <TextInput
              numberOfLines={1}
              style={styles.input}
              autoCapitalize="none"
              editable={false}
              autoCorrect={false}
              keyboardType="default"
              value={
                inputs && inputs['country'] && inputs['country'].value
                  ? inputs['country'].value
                  : ''
              }
              pointerEvents="none"
            />

            {renderError('country')}
          </View>
        </TouchableOpacity>

        <View style={styles.checkboxContainer}>
          <TouchableWithoutFeedback
            onPress={() => onSelectSameAddress(!isSameAddress)}>
            <Image
              style={styles.checkbox}
              source={
                isSameAddress == true
                  ? require('./../assets/images/checked-icon.png')
                  : require('./../assets/images/unchecked-icon.png')
              }
            />
          </TouchableWithoutFeedback>
          <AppText style={styles.checkboxLabel}>
            Communication Address same as Permanent Address
          </AppText>
        </View>

        {!isSameAddress && (
          <>
            <AppTextBold style={styles.parts}>
              Communication Address
            </AppTextBold>
            <View>
              <AppText>House No. *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={
                  inputs &&
                  inputs['comm_house_no'] &&
                  inputs['comm_house_no'].value
                    ? inputs['comm_house_no'].value
                    : ''
                }
                onChangeText={(value) => {
                  onInputChange('comm_house_no', value);
                }}
              />
              {renderError('comm_house_no')}
            </View>
            <View>
              <AppText>Street *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={
                  inputs && inputs['comm_street'] && inputs['comm_street'].value
                    ? inputs['comm_street'].value
                    : ''
                }
                onChangeText={(value) => {
                  onInputChange('comm_street', value);
                }}
              />
              {renderError('comm_street')}
            </View>
            <View>
              <AppText>Locality *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={
                  inputs &&
                  inputs['comm_locality'] &&
                  inputs['comm_locality'].value
                    ? inputs['comm_locality'].value
                    : ''
                }
                onChangeText={(value) => {
                  onInputChange('comm_locality', value);
                }}
              />
              {renderError('comm_locality')}
            </View>
            <View>
              <AppText>Pincode *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={
                  inputs &&
                  inputs['comm_pincode'] &&
                  inputs['comm_pincode'].value
                    ? inputs['comm_pincode'].value
                    : ''
                }
                onChangeText={(value) => {
                  onInputChange('comm_pincode', value);
                }}
              />
              {renderError('comm_pincode')}
            </View>
            <View>
              <AppText>City *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={
                  inputs && inputs['comm_city'] && inputs['comm_city'].value
                    ? inputs['comm_city'].value
                    : ''
                }
                onChangeText={(value) => {
                  onInputChange('comm_city', value);
                }}
              />
              {renderError('comm_city')}
            </View>
            <View>
              <AppText>State *</AppText>
              <TextInput
                numberOfLines={1}
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                editable={true}
                value={
                  inputs && inputs['comm_state'] && inputs['comm_state'].value
                    ? inputs['comm_state'].value
                    : ''
                }
                onChangeText={(value) => {
                  onInputChange('comm_state', value);
                }}
              />
              {renderError('comm_state')}
            </View>

            <TouchableOpacity
              onPress={onSelectCommunicationCountry}
              activeOpacity={0.7}>
              <View>
                <AppText>Country *</AppText>
                <TextInput
                  numberOfLines={1}
                  style={styles.input}
                  autoCapitalize="none"
                  autoCorrect={false}
                  editable={false}
                  keyboardType="default"
                  value={
                    inputs &&
                    inputs['comm_country'] &&
                    inputs['comm_country'].value
                      ? inputs['comm_country'].value
                      : ''
                  }
                  pointerEvents="none"
                />

                {renderError('comm_country')}
              </View>
            </TouchableOpacity>
          </>
        )}

        {!isNriCustomer ? (
          <View>
            <AppText>PAN Number *</AppText>
            <TextInput
              numberOfLines={1}
              style={styles.input}
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType={
                Platform.OS === 'android' ? 'visible-password' : 'default'
              }
              editable={true}
              value={
                inputs && inputs['pan_number'] && inputs['pan_number'].value
                  ? inputs['pan_number'].value
                  : ''
              }
              onChangeText={(value) => {
                onInputChange('pan_number', value);
              }}
            />
            {renderError('pan_number')}
          </View>
        ) : (
          <View>
            <AppText>Passport Number *</AppText>
            <TextInput
              numberOfLines={1}
              style={styles.input}
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType={
                Platform.OS === 'android' ? 'visible-password' : 'default'
              }
              editable={true}
              value={
                inputs &&
                inputs['passport_number'] &&
                inputs['passport_number'].value
                  ? inputs['passport_number'].value
                  : ''
              }
              onChangeText={(value) => {
                onInputChange('passport_number', value);
              }}
            />
            {renderError('passport_number')}
          </View>
        )}

        {purposeOfPurchase && purposeOfPurchase.length > 0 && (
          <>
            <AppText>Purpose of Purchase *</AppText>
            <DropDownPicker
              items={purposeOfPurchase ? purposeOfPurchase : []}
              autoScrollToDefaultValue={true}
              // defaultValue={
              //   inputs &&
              //   inputs['purpose_of_purchase'] &&
              //   inputs['purpose_of_purchase'].value
              //     ? inputs['purpose_of_purchase'].value
              //     : ''
              // }
              defaultValue={
                inputs &&
                inputs['purpose_of_purchase'] &&
                inputs['purpose_of_purchase'].value
                  ? inputs['purpose_of_purchase'].value
                  : ''
              }
              style={styles.dropdownDocument}
              placeholderStyle={styles.dropdownPlaceholder}
              itemStyle={styles.dropDownItem}
              labelStyle={styles.label}
              activeLabelStyle={styles.labelSelected}
              selectedLabelStyle={styles.labelSelected}
              onChangeItem={(item) =>
                onInputChange('purpose_of_purchase', item.value)
              }
              placeholder="Select Purpose of Purchase"
              controller={(instance) => {
                setDropPurpose(instance);
              }}
              onOpen={() => {
                // setDynamicBottomPaddingDocId(0);
                // setDynamicBottomPaddingDocAdd(0);

                setDynamicBottomPaddingPurpose(130);
              }}
              onClose={() => {
                setDynamicBottomPaddingPurpose(0);
              }}
            />
            {renderErrorDropdown('purpose_of_purchase')}
          </>
        )}

        <View style={{paddingBottom: dynamicBottomPaddingPurpose}}></View>

        <AppTextBold style={styles.parts}>Upload Documents</AppTextBold>

        <AppText>Document Type *</AppText>
        <DropDownPicker
          items={idProofDocType ? idProofDocType : []}
          autoScrollToDefaultValue={true}
          // defaultValue={
          //   inputs && inputs['id_proof_doc'] && inputs['id_proof_doc'].value
          //     ? inputs['id_proof_doc'].value
          //     : ''
          // }
          defaultValue={
            inputs && inputs['id_proof_doc'] && inputs['id_proof_doc'].value
              ? inputs['id_proof_doc'].value
              : ''
          }
          style={styles.dropdownDocument}
          placeholderStyle={styles.dropdownPlaceholder}
          itemStyle={styles.dropDownItem}
          labelStyle={styles.label}
          activeLabelStyle={styles.labelSelected}
          selectedLabelStyle={styles.labelSelected}
          onChangeItem={(item) => onInputChange('id_proof_doc', item.value)}
          placeholder="Select Document Type"
          controller={(instance) => {
            setDropDocumentId(instance);
          }}
          onOpen={() => {
            // setDynamicBottomPaddingPurpose(0);
            // setDynamicBottomPaddingDocAdd(0);

            setDynamicBottomPaddingDocId(
              idProofDocType && idProofDocType.length > 1 ? 120 : 60,
            );
          }}
          onClose={() => {
            setDynamicBottomPaddingDocId(0);
          }}
        />
        {renderErrorDropdown('id_proof_doc')}

        <View style={{paddingBottom: dynamicBottomPaddingDocId}}></View>

        <View style={styles.uploadContainer}>
          <AppText style={{fontSize: appFonts.normalFontSize}}>
            (Upload pdf, jpg, jpeg, png under 5 MB)
          </AppText>
          <View style={styles.uploadItemContainer}>
            <View style={styles.document}>
              <AppText style={styles.uploadedDocumentName} numberOfLines={1}>
                {attachedFileIdProof && attachedFileIdProof.file_name
                  ? attachedFileIdProof.file_name
                  : ''}
              </AppText>
            </View>
            <View style={styles.remove}>
              <AppButton
                title="Upload"
                onPress={() => onChooseFileUpload('idProof')}
              />
            </View>
          </View>

          {renderErrorDropdown('id_proof')}

          {attachedFileIdProof &&
          attachedFileIdProof.file_format &&
          attachedFileIdProof.file ? (
            <TouchableWithoutFeedback
              onPress={() => {
                if (attachedFileIdProof.fid && attachedFileIdProof.fid != '') {
                  downloadApplicantDocument(
                    attachedFileIdProof && attachedFileIdProof.file_name
                      ? attachedFileIdProof.file_name
                      : '',
                    attachedFileIdProof.file,
                  );
                }
              }}>
              <View style={styles.docImageContainer}>
                {attachedFileIdProof.file_format === 'image' ? (
                  <Image
                    resizeMode="contain"
                    style={styles.docImage}
                    source={{uri: attachedFileIdProof.file}}
                  />
                ) : (
                  <Image
                    resizeMode="contain"
                    style={styles.docImagePdf}
                    source={require('./../assets/images/pdf_icon.png')}
                  />
                )}
              </View>
            </TouchableWithoutFeedback>
          ) : null}
        </View>

        <AppText>Document Type *</AppText>
        <DropDownPicker
          items={addressProofDocType ? addressProofDocType : []}
          autoScrollToDefaultValue={true}
          // defaultValue={
          //   inputs &&
          //   inputs['address_proof_doc'] &&
          //   inputs['address_proof_doc'].value
          //     ? inputs['address_proof_doc'].value
          //     : ''
          // }
          defaultValue={
            inputs &&
            inputs['address_proof_doc'] &&
            inputs['address_proof_doc'].value
              ? inputs['address_proof_doc'].value
              : ''
          }
          style={styles.dropdownDocument}
          placeholderStyle={styles.dropdownPlaceholder}
          itemStyle={styles.dropDownItem}
          labelStyle={styles.label}
          activeLabelStyle={styles.labelSelected}
          selectedLabelStyle={styles.labelSelected}
          onChangeItem={(item) =>
            onInputChange('address_proof_doc', item.value)
          }
          placeholder="Select Document Type"
          controller={(instance) => {
            setDropDocumentAddress(instance);
          }}
          onOpen={() => {
            // setDynamicBottomPaddingPurpose(0);
            // setDynamicBottomPaddingDocId(0);
            setDynamicBottomPaddingDocAdd(
              idProofDocType && idProofDocType.length > 1 ? 120 : 60,
            );
          }}
          onClose={() => {
            setDynamicBottomPaddingDocAdd(0);
          }}
        />
        {renderErrorDropdown('address_proof_doc')}

        <View style={{paddingBottom: dynamicBottomPaddingDocAdd}}></View>

        <View style={styles.uploadContainer}>
          <AppText style={{fontSize: appFonts.normalFontSize}}>
            (Upload pdf, jpg, jpeg, png under 5 MB)
          </AppText>
          <View style={styles.uploadItemContainer}>
            <View style={styles.document}>
              <AppText style={styles.uploadedDocumentName} numberOfLines={1}>
                {attachedFileAddressProof && attachedFileAddressProof.file_name
                  ? attachedFileAddressProof.file_name
                  : ''}
              </AppText>
            </View>
            <View style={styles.remove}>
              <AppButton
                title="Upload"
                onPress={() => onChooseFileUpload('addressProof')}
              />
            </View>
          </View>

          {renderErrorDropdown('address_proof')}

          {attachedFileAddressProof &&
          attachedFileAddressProof.file_format &&
          attachedFileAddressProof.file ? (
            <TouchableWithoutFeedback
              onPress={() => {
                if (
                  attachedFileAddressProof.fid &&
                  attachedFileAddressProof.fid != ''
                ) {
                  downloadApplicantDocument(
                    attachedFileAddressProof &&
                      attachedFileAddressProof.file_name
                      ? attachedFileAddressProof.file_name
                      : '',
                    attachedFileAddressProof.file,
                  );
                }
              }}>
              <View style={styles.docImageContainer}>
                {attachedFileAddressProof.file_format === 'image' ? (
                  <Image
                    resizeMode="contain"
                    style={styles.docImage}
                    source={{uri: attachedFileAddressProof.file}}
                  />
                ) : (
                  <Image
                    resizeMode="contain"
                    style={styles.docImagePdf}
                    source={require('./../assets/images/pdf_icon.png')}
                  />
                )}
              </View>
            </TouchableWithoutFeedback>
          ) : null}
        </View>
      </View>

      <Modal
        isVisible={fileChooser}
        style={{
          margin: 0,
          justifyContent: 'flex-end',
        }}
        onBackButtonPress={closeActionSheet}
        onBackdropPress={closeActionSheet}
        backdropColor="rgba(0,0,0,0.5)">
        <AppFileChooser
          onPress={(clickItem) => onClickFileChooserItem(clickItem)}
        />
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    marginBottom: 20,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
    zIndex: 5,
  },
  document: {
    backgroundColor: colors.primary,
    width: '62%',
    height: '75%',
    marginTop: '3%',
    alignItems: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGray,
  },
  uploadContainer: {
    // marginTop: '2%',
    marginBottom: '5%',
  },
  uploadItemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // marginTop: '2%',
  },
  parts: {
    fontSize: appFonts.xlargeFontSize,
    paddingVertical: 10,
    lineHeight: 30,
  },
  uploadedDocumentName: {
    marginTop: '10%',
    borderBottomColor: colors.gray5,
    borderBottomWidth: 1,
  },

  btnContainer: {
    flexDirection: 'row',
    marginTop: 8,
    marginBottom: 8,
  },
  btnInd: {width: '30%', marginRight: '5%'},
  btnNri: {width: '30%'},
  inputContainer: {marginTop: '2%'},
  input: {
    borderBottomColor: colors.lightGray,
    borderBottomWidth: 1,
    marginBottom: '5%',
    fontFamily: appFonts.SourceSansProSemiBold,
    fontSize: appFonts.largeBold,
    marginTop: 2,
    paddingVertical: 8,
    color: colors.jaguar,
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
  },
  buttonText: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProSemiBold,
    color: colors.secondary,
  },
  remove: {width: '32%'},
  icon: {
    width: 16,
    height: 16,
  },
  container: {
    flex: 1,
    paddingVertical: 10,
  },
  pageTitle: {
    fontSize: appFonts.xxxlargeFontSize,
    paddingHorizontal: 25,
    fontFamily: appFonts.SourceSansProBold,
    color: colors.secondary,
    marginBottom: '7%',
    textTransform: 'uppercase',
  },
  viewContainer: {
    width: windowWidth,
    paddingBottom: 20,
    marginBottom: 25,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  titleLine: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  value: {
    fontSize: appFonts.largeBold,
    color: colors.expandText,
    fontFamily: appFonts.SourceSansProSemiBold,
  },
  icon: {
    width: 16,
    height: 16,
  },
  layoutImg: {
    width: '90%',
    marginVertical: 20,
  },
  detailsLine: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
  },
  checkboxContainer: {
    flexDirection: 'row',
    paddingBottom: '5%',
    alignItems: 'center',
  },
  checkboxLabel: {
    fontSize: appFonts.largeFontSize,
    color: colors.secondary,
    margin: 8,
    fontFamily: appFonts.SourceSansProRegular,
  },
  checkbox: {height: 24, width: 24},
  dropdownDocument: {
    height: 50,
    marginTop: 15,
    marginBottom: 8,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderTopWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropdownPlaceholder: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
  },
  labelSelected: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProSemiBold,
    marginBottom: 8,
  },
  label: {
    fontSize: appFonts.largeFontSize,
    fontFamily: appFonts.SourceSansProRegular,
    marginBottom: 8,
    textAlign: 'left',
  },
  dropDownItem: {
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGray,
    marginBottom: 6,
    marginTop: 6,
    justifyContent: 'flex-start',
  },
  docImageContainer: {
    marginVertical: 24,
    alignItems: 'center',
  },

  docImagePdf: {height: windowHeight / 6},
  docImage: {width: '100%', height: windowHeight / 4},
  txtErrorContainer: {
    marginTop: -10,
    marginBottom: 20,
  },
  txtError: {
    // position: 'absolute',
    // bottom: -12,
    color: colors.danger,
    fontSize: appFonts.largeFontSize,
    // marginBottom: 20,
  },
  txtErrorDoc: {
    color: colors.danger,
    fontSize: appFonts.largeFontSize,
    marginBottom: 20,
  },
});

export default PrimaryApplicant;
