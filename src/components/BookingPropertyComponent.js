import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';
import ImageModal from 'react-native-image-modal';

import AppText from '../components/ui/ AppText';
import AppTextBold from '../components/ui/AppTextBold';

import appFonts from '../config/appFonts';
import colors from '../config/colors';
import AppBookingPropertyDetails from './ui/AppBookingPropertyDetails';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const BookingPropertyComponent = ({propertyDetails, onPress}) => {
  return (
    <View style={styles.viewContainer}>
      <View style={{paddingHorizontal: 25}}>
        <TouchableWithoutFeedback onPress={onPress}>
          <View style={styles.titleLine}>
            <AppText
              style={[
                styles.title,
                {fontFamily: appFonts.SourceSansProSemiBold},
              ]}>
              Property Details
            </AppText>

            <Image
              source={require('./../assets/images/up-icon-b.png')}
              style={styles.icon}
            />
          </View>
        </TouchableWithoutFeedback>

        {propertyDetails.proj_name? <AppTextBold style={styles.projectName}>
          {propertyDetails.proj_name}
        </AppTextBold>: null}

        {propertyDetails.plan_image ? <View style={styles.layoutImgContainer}>
          <ImageModal
            resizeMode="contain"
            imageBackgroundColor="#ffffff"
            style={styles.layoutImg}
            source={{
              uri: propertyDetails.plan_image,
            }}
          />
        </View>: null}

        {propertyDetails.flat_type ? (
          <AppBookingPropertyDetails
            label="Unit Type"
            value={propertyDetails.flat_type}
          />
        ) : null}

        {propertyDetails.tower_name ? (
          <AppBookingPropertyDetails
            label="Tower"
            value={propertyDetails.tower_name}
          />
        ) : null}

        {propertyDetails.wing ? (
          <AppBookingPropertyDetails
            label="Wing"
            value={propertyDetails.wing}
          />
        ) : null}

        {propertyDetails.floor_no ? (
          <AppBookingPropertyDetails
            label="Floor"
            value={propertyDetails.floor_no}
          />
        ) : null}

        {propertyDetails.unit_no ? (
          <AppBookingPropertyDetails
            label="Unit No."
            value={propertyDetails.unit_no}
          />
        ) : null}

        {propertyDetails.open_balcony_area_sq_mt ? (
          <AppBookingPropertyDetails
            label="RERA Total Area (sqmt)"
            value={propertyDetails.open_balcony_area_sq_mt}
          />
        ) : null}

        {propertyDetails.open_balcony_area_sq_ft ? (
          <AppBookingPropertyDetails
            label="RERA Total Area (sqft)"
            value={propertyDetails.open_balcony_area_sq_ft}
          />
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  viewContainer: {
    width: windowWidth,
    paddingBottom: 20,
    marginBottom: 25,
    borderTopWidth: 1,
    borderColor: colors.lightGray,
    borderBottomWidth: 1,
    shadowColor: colors.lightGray,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.7,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: colors.primary,
  },
  titleLine: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: appFonts.largeBold,
    fontFamily: appFonts.SourceSansProRegular,
    color: colors.jaguar,
  },
  icon: {
    width: 16,
    height: 16,
  },
  projectName: {marginBottom: 12, fontSize: appFonts.xxlargeFontSize, textTransform: 'uppercase'},
  layoutImgContainer: {
    width: '100%',
    height: windowHeight / 4,
    marginVertical: 12,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e1e4e8',
  },
  layoutImg: {
    width: windowWidth * 0.92,
    height: windowHeight / 4,
  },
});

export default BookingPropertyComponent;
